<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltExperience" %>
<%/*

    filename: out\jsp\tExperience_form.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tExperience")%>



<%
//initial declaration of list class and parentID
    Integer        iExperienceID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection7", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iExperienceID")) 
    {
        iExperienceID        =    (Integer)pageControllerHash.get("iExperienceID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tExperience_form.jsp");
//initial declaration of list class and parentID

    bltExperience        Experience        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        Experience        =    new    bltExperience(iExperienceID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        Experience        =    new    bltExperience(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tExperience_form_instructions.jsp" %>
        <form action="tExperience_form_sub.jsp" name="tExperience_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (Experience.isRequired("TypeOfExperience",UserSecurityGroupID))&&(!Experience.isComplete("TypeOfExperience")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("TypeOfExperience",expiredDays))&&(Experience.isExpiredCheck("TypeOfExperience",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("TypeOfExperience",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Experience Type&nbsp;</b></p></td><td valign=top><p><select   name="TypeOfExperience" ><jsp:include page="../generic/tExperienceTypeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=Experience.getTypeOfExperience()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TypeOfExperience&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("TypeOfExperience")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("TypeOfExperience",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Experience Type&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tExperienceTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=Experience.getTypeOfExperience()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TypeOfExperience&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("TypeOfExperience")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (Experience.isRequired("Other",UserSecurityGroupID))&&(!Experience.isComplete("Other")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("Other",expiredDays))&&(Experience.isExpiredCheck("Other",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("Other",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>If other, list here&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Other" value="<%=Experience.getOther()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Other&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Other")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("Other",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>If other, list here&nbsp;</b></p></td><td valign=top><p><%=Experience.getOther()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Other&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Other")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("Specialty",UserSecurityGroupID))&&(!Experience.isComplete("Specialty")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("Specialty",expiredDays))&&(Experience.isExpiredCheck("Specialty",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("Specialty",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Specialty, Focus, Course Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Specialty" value="<%=Experience.getSpecialty()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Specialty&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Specialty")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("Specialty",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Specialty, Focus, Course Name&nbsp;</b></p></td><td valign=top><p><%=Experience.getSpecialty()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Specialty&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Specialty")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("DateFrom",UserSecurityGroupID))&&(!Experience.isComplete("DateFrom")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("DateFrom",expiredDays))&&(Experience.isExpiredCheck("DateFrom",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((Experience.isWrite("DateFrom",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20 type=text size="80" name="DateFrom" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(Experience.getDateFrom())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateFrom&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("DateFrom")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("DateFrom",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(Experience.getDateFrom())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateFrom&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("DateFrom")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("DateTo",UserSecurityGroupID))&&(!Experience.isComplete("DateTo")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("DateTo",expiredDays))&&(Experience.isExpiredCheck("DateTo",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((Experience.isWrite("DateTo",UserSecurityGroupID)))
            {
                        %>
<tr><td colspan=2>&nbsp;</td></tr>
<tr>
                  <td colspan=2 class=instructions>If you finished this program, please enter the date you finished. If you are still 
                    currently in this program, please enter "Current" otherwise, 
                    enter &quot;n/a&quot;</td>
                </tr>

                        <tr><td valign=top><p class=<%=theClass%> ><b>End Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20 type=text size="80" name="DateTo" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(Experience.getDateTo())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateTo&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("DateTo")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("DateTo",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>End Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(Experience.getDateTo())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateTo&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("DateTo")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("NoComplete",UserSecurityGroupID))&&(!Experience.isComplete("NoComplete")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("NoComplete",expiredDays))&&(Experience.isExpiredCheck("NoComplete",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("NoComplete",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=Experience.getEnglish("NoComplete")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="NoComplete" cols="40" maxlength=200><%=Experience.getNoComplete()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NoComplete&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("NoComplete")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("NoComplete",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=Experience.getEnglish("NoComplete")%>&nbsp;</b></p></td><td valign=top><p><%=Experience.getNoComplete()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NoComplete&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("NoComplete")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("Name",UserSecurityGroupID))&&(!Experience.isComplete("Name")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("Name",expiredDays))&&(Experience.isExpiredCheck("Name",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("Name",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Institution&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Name" value="<%=Experience.getName()%>">&nbsp;<a href="#" onClick="window.open('LI_tHospitalLI_Search.jsp?OFieldName=Name','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Name")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("Name",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Institution&nbsp;</b></p></td><td valign=top><p><%=Experience.getName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Name")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("Address1",UserSecurityGroupID))&&(!Experience.isComplete("Address1")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("Address1",expiredDays))&&(Experience.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("Address1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Address1" value="<%=Experience.getAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Address1")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("Address1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><%=Experience.getAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Address1")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("Address2",UserSecurityGroupID))&&(!Experience.isComplete("Address2")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("Address2",expiredDays))&&(Experience.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("Address2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="Address2" value="<%=Experience.getAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Address2")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("Address2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=Experience.getAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Address2")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("City",UserSecurityGroupID))&&(!Experience.isComplete("City")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("City",expiredDays))&&(Experience.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("City",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Institution City&nbsp;</b></p></td><td valign=top><p><input maxlength="30" type=text size="80" name="City" value="<%=Experience.getCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("City")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("City",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Institution City&nbsp;</b></p></td><td valign=top><p><%=Experience.getCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("City")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("StateID",UserSecurityGroupID))&&(!Experience.isComplete("StateID")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("StateID",expiredDays))&&(Experience.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><select   name="StateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=Experience.getStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("StateID")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=Experience.getStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("StateID")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (Experience.isRequired("Province",UserSecurityGroupID))&&(!Experience.isComplete("Province")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("Province",expiredDays))&&(Experience.isExpiredCheck("Province",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("Province",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Province" value="<%=Experience.getProvince()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Province")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("Province",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><%=Experience.getProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Province")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("ZIP",UserSecurityGroupID))&&(!Experience.isComplete("ZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("ZIP",expiredDays))&&(Experience.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("ZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ZIP" value="<%=Experience.getZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("ZIP")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("ZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=Experience.getZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("ZIP")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("CountryID",UserSecurityGroupID))&&(!Experience.isComplete("CountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("CountryID",expiredDays))&&(Experience.isExpiredCheck("CountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><select   name="CountryID" ><jsp:include page="../generic/tCountryLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=Experience.getCountryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("CountryID")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=Experience.getCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("CountryID")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (Experience.isRequired("Phone",UserSecurityGroupID))&&(!Experience.isComplete("Phone")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("Phone",expiredDays))&&(Experience.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("Phone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Phone" value="<%=Experience.getPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Phone")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("Phone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=Experience.getPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Phone")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("Fax",UserSecurityGroupID))&&(!Experience.isComplete("Fax")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("Fax",expiredDays))&&(Experience.isExpiredCheck("Fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("Fax",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Fax" value="<%=Experience.getFax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Fax")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("Fax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=Experience.getFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Fax")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("ContactName",UserSecurityGroupID))&&(!Experience.isComplete("ContactName")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("ContactName",expiredDays))&&(Experience.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("ContactName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name/Program Director&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ContactName" value="<%=Experience.getContactName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("ContactName")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("ContactName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name/Program Director&nbsp;</b></p></td><td valign=top><p><%=Experience.getContactName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("ContactName")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("ContactEmail",UserSecurityGroupID))&&(!Experience.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("ContactEmail",expiredDays))&&(Experience.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><input maxlength="75" type=text size="80" name="ContactEmail" value="<%=Experience.getContactEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=Experience.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (Experience.isRequired("Comments",UserSecurityGroupID))&&(!Experience.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((Experience.isExpired("Comments",expiredDays))&&(Experience.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((Experience.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=Experience.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=Experience.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Comments")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((Experience.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=Experience.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=Experience.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tExperience&amp;sRefID=<%=Experience.getExperienceID()%>&amp;sFieldNameDisp=<%=Experience.getEnglish("Comments")%>&amp;sTableNameDisp=tExperience','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tExperience")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tExperience")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
