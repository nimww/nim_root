<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltAdminPhysicianLU,com.winstaff.bltAdminPhysicianLU_List_LU_PhysicianID" %>
<%/*
    filename: out\jsp\tAdminPhysicianLU_main2_LU_PhysicianID_form_authorize.jsp
    JSP AutoGen on Mar/26/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }
    bltAdminPhysicianLU_List_LU_PhysicianID        bltAdminPhysicianLU_List_LU_PhysicianID        =    new    bltAdminPhysicianLU_List_LU_PhysicianID(iPhysicianID,"LookupID="+requestID,"");

//declaration of Enumeration
    bltAdminPhysicianLU        working_bltAdminPhysicianLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPhysicianLU_List_LU_PhysicianID.elements();
    %>
    <%
    if (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltAdminPhysicianLU  = (bltAdminPhysicianLU) leCurrentElement.getObject();
        pageControllerHash.put("iLookupID",working_bltAdminPhysicianLU.getLookupID());
        pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tAdminPhysicianLU_form2.jsp?nullParam=null"+parameterPassString    ;
        if (request.getParameter("EDIT").equalsIgnoreCase("del"))
        {
            targetRedirect = "tAdminPhysicianLU_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
        }
        response.sendRedirect(targetRedirect);
    }
    else
    {
   response.sendRedirect("/errorhandler.jsp");
    }

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>




