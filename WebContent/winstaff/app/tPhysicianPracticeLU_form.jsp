<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltPhysicianMaster ,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPhysicianPracticeLU" %>
<%/*

    filename: out\jsp\tPhysicianPracticeLU_form.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tPhysicianPracticeLU")%>



<%
//initial declaration of list class and parentID
    Integer        iLookupID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iLookupID")) 
    {
        iLookupID        =    (Integer)pageControllerHash.get("iLookupID");
        accessValid = true;
        if (pageControllerHash.containsKey("sKeyMasterReference"))
        {
            sKeyMasterReference = (String)pageControllerHash.get("sKeyMasterReference");
        }
        if (sKeyMasterReference!=null)
        {
            if (sKeyMasterReference.equalsIgnoreCase("p"))
            {
                if (!pageControllerHash.containsKey("iPhysicianID")) 
                {
                    accessValid=false;
                }
            }
            else if (sKeyMasterReference.equalsIgnoreCase("s"))
            {
                if (!pageControllerHash.containsKey("iPracticeID")) 
                {
                    accessValid=false;
                }
            }
            else
            {
                accessValid=false;
            }
        }
        else 
        {
            accessValid=false;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tPhysicianPracticeLU_form.jsp");
//initial declaration of list class and parentID

    bltPhysicianPracticeLU        PhysicianPracticeLU        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PhysicianPracticeLU        =    new    bltPhysicianPracticeLU(iLookupID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PhysicianPracticeLU        =    new    bltPhysicianPracticeLU(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tPhysicianPracticeLU_form_instructions.jsp" %>
        <form action="tPhysicianPracticeLU_form_sub.jsp" name="tPhysicianPracticeLU_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
bltPhysicianMaster pm = new bltPhysicianMaster (PhysicianPracticeLU.getPhysicianID());
%>
          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>
            <p class=tdBase><b>Physician: </b> <%=pm.getLastName() + ", " + pm.getFirstName() %></p>
            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (PhysicianPracticeLU.isRequired("StartDate",UserSecurityGroupID))&&(!PhysicianPracticeLU.isComplete("StartDate")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianPracticeLU.isExpired("StartDate",expiredDays))&&(PhysicianPracticeLU.isExpiredCheck("StartDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((PhysicianPracticeLU.isWrite("StartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="StartDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianPracticeLU.getStartDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("StartDate")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PhysicianPracticeLU.isRead("StartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianPracticeLU.getStartDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("StartDate")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PhysicianPracticeLU.isRequired("EndDate",UserSecurityGroupID))&&(!PhysicianPracticeLU.isComplete("EndDate")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianPracticeLU.isExpired("EndDate",expiredDays))&&(PhysicianPracticeLU.isExpiredCheck("EndDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((PhysicianPracticeLU.isWrite("EndDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>End Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="EndDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianPracticeLU.getEndDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("EndDate")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PhysicianPracticeLU.isRead("EndDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>End Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PhysicianPracticeLU.getEndDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("EndDate")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PhysicianPracticeLU.isRequired("isPrimaryOffice",UserSecurityGroupID))&&(!PhysicianPracticeLU.isComplete("isPrimaryOffice")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianPracticeLU.isExpired("isPrimaryOffice",expiredDays))&&(PhysicianPracticeLU.isExpiredCheck("isPrimaryOffice",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PhysicianPracticeLU.isWrite("isPrimaryOffice",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is this your primary office?&nbsp;</b></p></td><td valign=top><p><select   name="isPrimaryOffice" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PhysicianPracticeLU.getisPrimaryOffice()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isPrimaryOffice&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("isPrimaryOffice")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PhysicianPracticeLU.isRead("isPrimaryOffice",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is this your primary office?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PhysicianPracticeLU.getisPrimaryOffice()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isPrimaryOffice&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("isPrimaryOffice")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PhysicianPracticeLU.isRequired("isAdministrativeOffice",UserSecurityGroupID))&&(!PhysicianPracticeLU.isComplete("isAdministrativeOffice")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianPracticeLU.isExpired("isAdministrativeOffice",expiredDays))&&(PhysicianPracticeLU.isExpiredCheck("isAdministrativeOffice",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PhysicianPracticeLU.isWrite("isAdministrativeOffice",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is this your administrative office?&nbsp;</b></p></td><td valign=top><p><select   name="isAdministrativeOffice" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PhysicianPracticeLU.getisAdministrativeOffice()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isAdministrativeOffice&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("isAdministrativeOffice")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PhysicianPracticeLU.isRead("isAdministrativeOffice",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is this your administrative office?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PhysicianPracticeLU.getisAdministrativeOffice()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isAdministrativeOffice&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("isAdministrativeOffice")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PhysicianPracticeLU.isRequired("CoverageHours",UserSecurityGroupID))&&(!PhysicianPracticeLU.isComplete("CoverageHours")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianPracticeLU.isExpired("CoverageHours",expiredDays))&&(PhysicianPracticeLU.isExpiredCheck("CoverageHours",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PhysicianPracticeLU.isWrite("CoverageHours",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=PhysicianPracticeLU.getEnglish("CoverageHours")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="CoverageHours" cols="40" maxlength=200><%=PhysicianPracticeLU.getCoverageHours()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoverageHours&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("CoverageHours")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PhysicianPracticeLU.isRead("CoverageHours",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PhysicianPracticeLU.getEnglish("CoverageHours")%>&nbsp;</b></p></td><td valign=top><p><%=PhysicianPracticeLU.getCoverageHours()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoverageHours&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("CoverageHours")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PhysicianPracticeLU.isRequired("Comments",UserSecurityGroupID))&&(!PhysicianPracticeLU.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((PhysicianPracticeLU.isExpired("Comments",expiredDays))&&(PhysicianPracticeLU.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PhysicianPracticeLU.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=PhysicianPracticeLU.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=PhysicianPracticeLU.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("Comments")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PhysicianPracticeLU.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PhysicianPracticeLU.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=PhysicianPracticeLU.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPhysicianPracticeLU&amp;sRefID=<%=PhysicianPracticeLU.getLookupID()%>&amp;sFieldNameDisp=<%=PhysicianPracticeLU.getEnglish("Comments")%>&amp;sTableNameDisp=tPhysicianPracticeLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tPhysicianPracticeLU")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tPhysicianPracticeLU")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
