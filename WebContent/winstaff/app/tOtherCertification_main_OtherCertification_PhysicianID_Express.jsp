<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltOtherCertification,com.winstaff.bltOtherCertification_List" %>
<%/*
    filename: tOtherCertification_main_OtherCertification_PhysicianID.jsp
    Created on Oct/22/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("EXPRESSTopControl","tOtherCertification",iExpressMode)%>



<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection5", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID") ) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tOtherCertification_main_OtherCertification_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iOtherCertID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltOtherCertification_List        bltOtherCertification_List        =    new    bltOtherCertification_List(iPhysicianID);

//declaration of Enumeration
    bltOtherCertification        OtherCertification;
    ListElement         leCurrentElement;
    Enumeration eList = bltOtherCertification_List.elements();
    %>
        <%@ include file="tOtherCertification_main_OtherCertification_PhysicianID_instructions.jsp" %>

        <form action="tOtherCertification_main_OtherCertification_PhysicianID_Express_sub.jsp" name="tOtherCertification_main_OtherCertification_PhysicianID1" method="POST">
    <%
    int iExpress=0;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tOtherCertification"))
    {
       iExpress++;
         %>
         <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        OtherCertification  = (bltOtherCertification) leCurrentElement.getObject();
      }
      else
      {
        OtherCertification  = new bltOtherCertification();
        isNewRecord= true;
      }
        OtherCertification.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tOtherCertification")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

  {

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (OtherCertification.isRequired("CertificationType",UserSecurityGroupID))&&(!OtherCertification.isComplete("CertificationType")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("CertificationType",expiredDays))&&(OtherCertification.isExpiredCheck("CertificationType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("CertificationType",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Type&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="CertificationType_<%=iExpress%>" value="<%=OtherCertification.getCertificationType()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertificationType&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("CertificationType")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("CertificationType",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Type&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getCertificationType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertificationType&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("CertificationType")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("CertificationNumber",UserSecurityGroupID))&&(!OtherCertification.isComplete("CertificationNumber")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("CertificationNumber",expiredDays))&&(OtherCertification.isExpiredCheck("CertificationNumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((OtherCertification.isWrite("CertificationNumber",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Certificate/Document Number&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="CertificationNumber_<%=iExpress%>" value="<%=OtherCertification.getCertificationNumber()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertificationNumber&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("CertificationNumber")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("CertificationNumber",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Certificate/Document Number&nbsp;</b></p></td><td valign=top><p><%=OtherCertification.getCertificationNumber()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertificationNumber&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("CertificationNumber")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("ExpirationDate",UserSecurityGroupID))&&(!OtherCertification.isComplete("ExpirationDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("ExpirationDate",expiredDays))&&(OtherCertification.isExpiredCheck("ExpirationDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((OtherCertification.isWrite("ExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="80" name="ExpirationDate_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(OtherCertification.getExpirationDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpirationDate&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("ExpirationDate")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((OtherCertification.isRead("ExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(OtherCertification.getExpirationDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpirationDate&amp;sTableName=tOtherCertification&amp;sRefID=<%=OtherCertification.getOtherCertID()%>&amp;sFieldNameDisp=<%=OtherCertification.getEnglish("ExpirationDate")%>&amp;sTableNameDisp=tOtherCertification','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (OtherCertification.isRequired("Name",UserSecurityGroupID))&&(!OtherCertification.isComplete("Name")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("Name",expiredDays))&&(OtherCertification.isExpiredCheck("Name",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Name_<%=iExpress%>" value="<%=OtherCertification.getName()%>">

            <%
            if ( (OtherCertification.isRequired("Address1",UserSecurityGroupID))&&(!OtherCertification.isComplete("Address1")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("Address1",expiredDays))&&(OtherCertification.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Address1_<%=iExpress%>" value="<%=OtherCertification.getAddress1()%>">

            <%
            if ( (OtherCertification.isRequired("Address2",UserSecurityGroupID))&&(!OtherCertification.isComplete("Address2")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("Address2",expiredDays))&&(OtherCertification.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Address2_<%=iExpress%>" value="<%=OtherCertification.getAddress2()%>">

            <%
            if ( (OtherCertification.isRequired("City",UserSecurityGroupID))&&(!OtherCertification.isComplete("City")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("City",expiredDays))&&(OtherCertification.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="City_<%=iExpress%>" value="<%=OtherCertification.getCity()%>">

            <%
            if ( (OtherCertification.isRequired("StateID",UserSecurityGroupID))&&(!OtherCertification.isComplete("StateID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("StateID",expiredDays))&&(OtherCertification.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="StateID_<%=iExpress%>" value="<%=OtherCertification.getStateID()%>">

            <%
            if ( (OtherCertification.isRequired("Province",UserSecurityGroupID))&&(!OtherCertification.isComplete("Province")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("Province",expiredDays))&&(OtherCertification.isExpiredCheck("Province",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Province_<%=iExpress%>" value="<%=OtherCertification.getProvince()%>">

            <%
            if ( (OtherCertification.isRequired("ZIP",UserSecurityGroupID))&&(!OtherCertification.isComplete("ZIP")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("ZIP",expiredDays))&&(OtherCertification.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ZIP_<%=iExpress%>" value="<%=OtherCertification.getZIP()%>">

            <%
            if ( (OtherCertification.isRequired("CountryID",UserSecurityGroupID))&&(!OtherCertification.isComplete("CountryID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("CountryID",expiredDays))&&(OtherCertification.isExpiredCheck("CountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="CountryID_<%=iExpress%>" value="<%=OtherCertification.getCountryID()%>">

            <%
            if ( (OtherCertification.isRequired("Phone",UserSecurityGroupID))&&(!OtherCertification.isComplete("Phone")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("Phone",expiredDays))&&(OtherCertification.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Phone_<%=iExpress%>" value="<%=OtherCertification.getPhone()%>">

            <%
            if ( (OtherCertification.isRequired("Fax",UserSecurityGroupID))&&(!OtherCertification.isComplete("Fax")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("Fax",expiredDays))&&(OtherCertification.isExpiredCheck("Fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Fax_<%=iExpress%>" value="<%=OtherCertification.getFax()%>">

            <%
            if ( (OtherCertification.isRequired("ContactName",UserSecurityGroupID))&&(!OtherCertification.isComplete("ContactName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("ContactName",expiredDays))&&(OtherCertification.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ContactName_<%=iExpress%>" value="<%=OtherCertification.getContactName()%>">

            <%
            if ( (OtherCertification.isRequired("ContactEmail",UserSecurityGroupID))&&(!OtherCertification.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("ContactEmail",expiredDays))&&(OtherCertification.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ContactEmail_<%=iExpress%>" value="<%=OtherCertification.getContactEmail()%>">

            <%
            if ( (OtherCertification.isRequired("DocuLinkID",UserSecurityGroupID))&&(!OtherCertification.isComplete("DocuLinkID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("DocuLinkID",expiredDays))&&(OtherCertification.isExpiredCheck("DocuLinkID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="DocuLinkID_<%=iExpress%>" value="<%=OtherCertification.getDocuLinkID()%>">

            <%
            if ( (OtherCertification.isRequired("Comments",UserSecurityGroupID))&&(!OtherCertification.isComplete("Comments")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((OtherCertification.isExpired("Comments",expiredDays))&&(OtherCertification.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="Comments_<%=iExpress%>" value="<%=OtherCertification.getComments()%>">


            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>

            <input type=hidden name=routePageReference value="sParentReturnPage">

        </td></tr></table>
        </td></tr></table>
        <%
  }%>

        </td></tr></table></table><br>        <%
    }
    %>
        <input <%=HTMLFormStyleButton%> onClick="document.forms[0].nextPage.value='<%=ConfigurationMessages.getExpressLinkRaw("tOtherCertification","next",iExpressMode)%>'" type=Submit value="Continue" name=Submit>
        <input type=hidden name=nextPage value="">
        </form>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
