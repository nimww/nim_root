<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.ConfigurationInformation,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPeerReference" %>
<%/*

    filename: out\jsp\tPeerReference_form.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tPeerReference")%>



<%
//initial declaration of list class and parentID
    Integer        iReferenceID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection11", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iReferenceID")) 
    {
        iReferenceID        =    (Integer)pageControllerHash.get("iReferenceID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tPeerReference_form.jsp");
//initial declaration of list class and parentID

    bltPeerReference        PeerReference        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PeerReference        =    new    bltPeerReference(iReferenceID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PeerReference        =    new    bltPeerReference(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tPeerReference_form_instructions.jsp" %>
        <form action="tPeerReference_form_sub.jsp" name="tPeerReference_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (PeerReference.isRequired("Salutation",UserSecurityGroupID))&&(!PeerReference.isComplete("Salutation")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("Salutation",expiredDays))&&(PeerReference.isExpiredCheck("Salutation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Salutation",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Salutation&nbsp;</b></p></td><td valign=top><p><select   name="Salutation" ><jsp:include page="../generic/tSalutationLIShort.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PeerReference.getSalutation()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Salutation&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Salutation")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Salutation",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Salutation&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tSalutationLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PeerReference.getSalutation()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Salutation&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Salutation")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PeerReference.isRequired("Title",UserSecurityGroupID))&&(!PeerReference.isComplete("Title")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("Title",expiredDays))&&(PeerReference.isExpiredCheck("Title",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Title",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Title&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Title" value="<%=PeerReference.getTitle()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Title&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Title")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Title",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Title&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getTitle()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Title&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Title")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PeerReference.isRequired("FirstName",UserSecurityGroupID))&&(!PeerReference.isComplete("FirstName")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("FirstName",expiredDays))&&(PeerReference.isExpiredCheck("FirstName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("FirstName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>First Name&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="FirstName" value="<%=PeerReference.getFirstName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FirstName&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("FirstName")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("FirstName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>First Name&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getFirstName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FirstName&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("FirstName")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("LastName",UserSecurityGroupID))&&(!PeerReference.isComplete("LastName")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("LastName",expiredDays))&&(PeerReference.isExpiredCheck("LastName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("LastName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Last Name&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="LastName" value="<%=PeerReference.getLastName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LastName&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("LastName")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("LastName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Last Name&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getLastName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LastName&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("LastName")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PeerReference.isRequired("Specialty",UserSecurityGroupID))&&(!PeerReference.isComplete("Specialty")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("Specialty",expiredDays))&&(PeerReference.isExpiredCheck("Specialty",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Specialty",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Specialty&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Specialty" value="<%=PeerReference.getSpecialty()%>">&nbsp;<a href="#" onClick="window.open('LI_tSpecialtyLI_Search.jsp?OFieldName=Specialty','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Specialty&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Specialty")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Specialty",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Specialty&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getSpecialty()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Specialty&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Specialty")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PeerReference.isRequired("HospitalAffiliation",UserSecurityGroupID))&&(!PeerReference.isComplete("HospitalAffiliation")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("HospitalAffiliation",expiredDays))&&(PeerReference.isExpiredCheck("HospitalAffiliation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("HospitalAffiliation",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>HospitalAffiliation&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="HospitalAffiliation" value="<%=PeerReference.getHospitalAffiliation()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HospitalAffiliation&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("HospitalAffiliation")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("HospitalAffiliation",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>HospitalAffiliation&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getHospitalAffiliation()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HospitalAffiliation&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("HospitalAffiliation")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("HospitalDepartment",UserSecurityGroupID))&&(!PeerReference.isComplete("HospitalDepartment")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("HospitalDepartment",expiredDays))&&(PeerReference.isExpiredCheck("HospitalDepartment",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("HospitalDepartment",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>HospitalDepartment&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="HospitalDepartment" value="<%=PeerReference.getHospitalDepartment()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HospitalDepartment&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("HospitalDepartment")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("HospitalDepartment",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>HospitalDepartment&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getHospitalDepartment()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HospitalDepartment&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("HospitalDepartment")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PeerReference.isRequired("Address1",UserSecurityGroupID))&&(!PeerReference.isComplete("Address1")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("Address1",expiredDays))&&(PeerReference.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Address1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Address1" value="<%=PeerReference.getAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Address1")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Address1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Address1")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("Address2",UserSecurityGroupID))&&(!PeerReference.isComplete("Address2")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("Address2",expiredDays))&&(PeerReference.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Address2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="Address2" value="<%=PeerReference.getAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Address2")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Address2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Address2")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("City",UserSecurityGroupID))&&(!PeerReference.isComplete("City")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("City",expiredDays))&&(PeerReference.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("City",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td valign=top><p><input maxlength="30" type=text size="80" name="City" value="<%=PeerReference.getCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("City")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("City",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("City")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("StateID",UserSecurityGroupID))&&(!PeerReference.isComplete("StateID")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("StateID",expiredDays))&&(PeerReference.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><select   name="StateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PeerReference.getStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("StateID")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PeerReference.getStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("StateID")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PeerReference.isRequired("Province",UserSecurityGroupID))&&(!PeerReference.isComplete("Province")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("Province",expiredDays))&&(PeerReference.isExpiredCheck("Province",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Province",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Province" value="<%=PeerReference.getProvince()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Province")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Province",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Province")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("ZIP",UserSecurityGroupID))&&(!PeerReference.isComplete("ZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("ZIP",expiredDays))&&(PeerReference.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("ZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ZIP" value="<%=PeerReference.getZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("ZIP")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("ZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("ZIP")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("CountryID",UserSecurityGroupID))&&(!PeerReference.isComplete("CountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("CountryID",expiredDays))&&(PeerReference.isExpiredCheck("CountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><select   name="CountryID" ><jsp:include page="../generic/tCountryLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PeerReference.getCountryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("CountryID")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PeerReference.getCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("CountryID")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PeerReference.isRequired("Phone",UserSecurityGroupID))&&(!PeerReference.isComplete("Phone")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("Phone",expiredDays))&&(PeerReference.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Phone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Phone" value="<%=PeerReference.getPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Phone")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Phone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Phone")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("Fax",UserSecurityGroupID))&&(!PeerReference.isComplete("Fax")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("Fax",expiredDays))&&(PeerReference.isExpiredCheck("Fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Fax",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="Fax" value="<%=PeerReference.getFax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Fax")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Fax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Fax")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("ContactEmail",UserSecurityGroupID))&&(!PeerReference.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("ContactEmail",expiredDays))&&(PeerReference.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><input maxlength="75" type=text size="80" name="ContactEmail" value="<%=PeerReference.getContactEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("YearsAssociated",UserSecurityGroupID))&&(!PeerReference.isComplete("YearsAssociated")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("YearsAssociated",expiredDays))&&(PeerReference.isExpiredCheck("YearsAssociated",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("YearsAssociated",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Years associated&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="YearsAssociated" value="<%=PeerReference.getYearsAssociated()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=YearsAssociated&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("YearsAssociated")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("YearsAssociated",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Years associated&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getYearsAssociated()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=YearsAssociated&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("YearsAssociated")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("DocuLinkID",UserSecurityGroupID))&&(!PeerReference.isComplete("DocuLinkID")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("DocuLinkID",expiredDays))&&(PeerReference.isExpiredCheck("DocuLinkID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("DocuLinkID",UserSecurityGroupID)))
            {
                        if (PeerReference.getDocuLinkID().intValue()>0)
                        {
                            bltDocumentManagement myDoc = new bltDocumentManagement(PeerReference.getDocuLinkID());
                            if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                            {
				    pageControllerHash.put("sFileName",ConfigurationInformation.sLinkedPDFDirectory + "\\" + myDoc.getDocumentFileName());
				    pageControllerHash.put("sDownloadName",myDoc.getDocumentFileName());
				    pageControllerHash.put("bDownload",new Boolean(false));
				    session.setAttribute("pageControllerHash",pageControllerHash);
                            %>
                            <tr><td valign=top><p class=<%=theClass%> ><b>Attached Document:&nbsp;</p></td><td valign=top><p><a target=_blank href="fileRetrieve.jsp">view</a></b></p></td></tr>
                            <%
                           }
                            else
                            {
                            %>

                            <%
                            }
                        }
            }
            else if ((PeerReference.isRead("DocuLinkID",UserSecurityGroupID)))
            {
                        bltDocumentManagement myDoc = new bltDocumentManagement(PeerReference.getDocuLinkID());
                            if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                            {
				    pageControllerHash.put("sFileName",ConfigurationInformation.sLinkedPDFDirectory + "\\" + myDoc.getDocumentFileName());
				    pageControllerHash.put("sDownloadName",myDoc.getDocumentFileName());
				    pageControllerHash.put("bDownload",new Boolean(false));
				    session.setAttribute("pageControllerHash",pageControllerHash);
                            %>
                            <tr><td valign=top><p class=<%=theClass%> ><b>Attached Document:&nbsp;</p></td><td valign=top><p><a target=_blank href="fileRetrieve.jsp">view</a></b></p></td></tr>
                            <%
                           }
                        else
                        {
                        %>

                        <%
                        }
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PeerReference.isRequired("Comments",UserSecurityGroupID))&&(!PeerReference.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((PeerReference.isExpired("Comments",expiredDays))&&(PeerReference.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=PeerReference.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=PeerReference.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Comments")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PeerReference.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Comments")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tPeerReference")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tPeerReference")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
