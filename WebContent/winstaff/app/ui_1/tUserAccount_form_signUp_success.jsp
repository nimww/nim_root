<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltUserAccount" %>
<%/*

    filename: out\jsp\tUserAccount_form.jsp
    JSP AutoGen on Mar/25/2002
*/%>
<%@ include file="../../generic/generalDisplay.jsp" %>

<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<link rel="stylesheet" href="style_PhysicianID.css" type="text/css">



    
<script language="JavaScript">
<!--
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
<table cellpadding=0 cellspacing=0 border=0 width=700>
    <tr><td width=10>&nbsp;</td>
    <td> <br>


<%
//initial declaration of list class and parentID
    boolean accessValid = false;
    // required for Type2
    {
        accessValid = true;    
		}
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");


//initial declaration of list class and parentID

//this is changed manually to allow a company to create user accounts.  This one is trickier, because data collection must come before accounts can be created
    bltUserAccount        UserAccount        =    new    bltUserAccount();


//fields
        %><p class=tdHeader>Sign Up Successful.</p>
      <p class=tdHeader>Please continue and enter your UserName and Password to 
        access your account.<br>
        <input style="font-family: Arial; font-size: 10px; background-color: #C0C0C0"  type="button" name="Continue" value="Continue" onClick="MM_goToURL('parent','../signIn.jsp');return document.MM_returnValue">
        <%
  }
  else
  {
   out.println("illegal");
  }

%>
        <br>
      </p>	
      </td></tr></table>
<table width="500" border="0" cellspacing="0" cellpadding="0" bgcolor=#FFFFFF>
  <tr>
    <td bgcolor="#FFFFFF"><img src="images/bot-nav_PhysicianID.gif" ></td>
  </tr>
</table>

