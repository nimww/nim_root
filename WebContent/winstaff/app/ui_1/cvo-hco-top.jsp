<%@page contentType="text/html" language="java" import="com.winstaff.*" %>

<%@ include file="../../generic/CheckLogin.jsp" %>

<%
//initial declaration of list class and parentID
    Integer        iHCOID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
	CVOPhysician_ControlType CPCT = new CVOPhysician_ControlType();
    if (pageControllerHash.containsKey("iHCOID")&&pageControllerHash.containsKey("iCVOHCOControlStatusID")&&pageControllerHash.containsKey("iCVOHCOPriorityStatusID")&&pageControllerHash.containsKey("iCVOHCOLUID")) 
    {
        iHCOID        =    (Integer)pageControllerHash.get("iHCOID");
		CPCT.setControlType((Integer)pageControllerHash.get("iCVOHCOControlStatusID"));
		CPCT.setPriorityType((Integer)pageControllerHash.get("iCVOHCOPriorityStatusID"));
		CPCT.setCVOHCOLUID((Integer)pageControllerHash.get("iCVOHCOLUID"));
		accessValid=true;
    }
  //page security
  if (accessValid)
  {
  bltHCOMaster hm = new bltHCOMaster(iHCOID);
%>
<html>
<head>
<title>side-nav</title>
<meta http-equiv="Content-Type" content="text/html;">
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Oct 07 16:23:07 GMT-0700 (Pacific Daylight Time) 2002-->
<link rel="stylesheet" href="style_SideNav.css" type="text/css">
<script language="JavaScript">
<!--
function MM_popupMsg(msg) { //v1.0
  alert(msg);
}
//-->
</script>
</head>
<base target="hcoMain">
<body bgcolor="#999999" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" text="#000000" link="#000000" vlink="#000000" alink="#000000">
<table border="2" cellspacing="0" cellpadding="0" bgcolor="#999999" bordercolor="#666666" width="700">
  <tr> 
    <td rowspan="2" width="75" height="105"> <img src="images/HCO_Tools1.jpg" width="75" height="105"></td>
    <td> 
      <table border="0" cellspacing="0" cellpadding="5" bgcolor="#999999" width="100%">
        <tr bgcolor="#999999" valign="bottom"> 
          <td nowrap> 
            <div align="center"><a href="../pro-file_HCO-Status.jsp"><img src="sid-nav_Images/home_icon1.gif" border="0" width="47" height="39"><br>
              HCO Home</a></div>
          </td>
          <td nowrap> 
            <div align="center"><a href="../HCO-query_HCOID.jsp"><img src="sid-nav_Images/practitioners_icon2-search.gif" border="0" width="48" height="41"><br>
              Search HCO Authorized Practitioners</a></div>
          </td>
          <td nowrap> 
            <div align="center"><a href="../HCO-query_view.jsp"><img src="sid-nav_Images/practitioners_icon1.gif" border="0"><br>
              Practitioner View</a></div>
          </td>
<%
if ( (CurrentUserAccount.getAccessType().intValue()==2||CurrentUserAccount.getAccessType().intValue()==4) )
{
%>
          <td nowrap> 
            <div align="center"><a href="../reports_HCOID.jsp"><img src="sid-nav_Images/forms_icon1.gif" border="0" ><br>
              HCO Tools</a></div>
          </td>
<%
}
%>

        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td> 
      <table width="100%" border="1" cellspacing="0" cellpadding="2" bgcolor="#CCCCCC">
        <tr class=tdBase> 
          <td nowrap><b><%=hm.getName()%></b>&nbsp;[<%=hm.getHCOID()%>]</td><td>Priority:&nbsp;<b><%=CPCT.getPriorityType()%></b></td><td>Access 
            Level:&nbsp;<b><%=CPCT.getControlType()%></b></td>
<%
if (false)
{
%>
          <td nowrap align="right"><a href="#" onClick="MM_popupMsg('Coming Soon\r')">Edit 
            HCO Info</a>&nbsp;</td>
          <td nowrap align="right" width="31"><a href="#" onClick="MM_popupMsg('Coming Soon\r')"><img src="../images/icon_edit.gif" width="31" height="24"></a></td>
<%
}
%>		  
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
<%
}
%>