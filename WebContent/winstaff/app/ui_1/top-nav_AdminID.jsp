
<%@page contentType="text/html" language="java" import="com.winstaff.bltStateLI, com.winstaff.bltAdminMaster" %>

<%@ include file="../../generic/CheckLogin.jsp" %>

 
<%
    Integer        iAdminID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;
    }
	bltAdminMaster pm = new bltAdminMaster (iAdminID);

%>
<link rel="stylesheet" href="ui_<%=thePLCID%>\style_AdminID.css" type="text/css">
<script language="JavaScript">
<!--
function fwLoadMenus() {
  if (window.fw_menu_0) return;
  window.fw_menu_0 = new Menu("root",120,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");

  fw_menu_0.addMenuItem("Admin Status","location='pro-file-manager_Status.jsp'");
  fw_menu_0.addMenuItem("Options","location='Admin_options.jsp'");
  fw_menu_0.addMenuItem("Queries & Reports","location='QR_Admin_main.jsp'");
  fw_menu_0.addMenuItem("Sign Off","location='signOff.jsp'");
   fw_menu_0.fontWeight="bold";
   fw_menu_0.hideOnMouseOut=true;

  window.fw_menu_1 = new Menu("root",100,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_1.addMenuItem("List/Manage","location='AdminPhysician_query.jsp?PhysicianID=&LastName=&FirstName=&SSN=&HomeCity=&HomeStateID=0&HomeZIP=&orderBy=PhysicianID&startID=0&maxResults=50&Submit2=Submit'");
  fw_menu_1.addMenuItem("Search/Manage","location='AdminPhysician_query.jsp'");
   fw_menu_1.fontWeight="bold";
   fw_menu_1.hideOnMouseOut=true;

  window.fw_menu_2 = new Menu("root",100,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_2.addMenuItem("List/Manage","location='AdminPractice_query.jsp?PracticeID=&PracticeName=&OfficeCity=&OfficeStateID=0&OfficeZip=&orderBy=PracticeID&startID=0&maxResults=50&Submit2=Submit'");
  fw_menu_2.addMenuItem("Search/Manage","location='AdminPractice_query.jsp'");
   fw_menu_2.fontWeight="bold";
   fw_menu_2.hideOnMouseOut=true;

  window.fw_menu_3 = new Menu("root",161,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_3.addMenuItem("General Help","window.open('help_general.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
  fw_menu_3.addMenuItem("Frequently Asked Questions","window.open('help_faqs.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
  fw_menu_3.addMenuItem("Contact Us","window.open('ui_1/contact_us.jsp', '_blank','scrollbars=yes,status=yes, width=400,height=630,resizable=yes');");
  fw_menu_3.addMenuItem("About","window.open('ui_1/about.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
   fw_menu_3.fontWeight="bold";
   fw_menu_3.hideOnMouseOut=true;

  fw_menu_3.writeMenus();
} 
// fwLoadMenus()

//-->
</script>
<script language="JavaScript1.2" src="ui_1/fw_menu.js"></script>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor=#666666>
  <tr> 
    <td bgcolor="#FFFF00">Gen Admin Tools:  <a href="AdminPracticeAll_query.jsp">Return to Main Search</a></td></tr>
  <tr>
    <td>
      <script language="JavaScript1.2">fwLoadMenus();</script>
      <table border="0" cellpadding="0" cellspacing="0" width="700">
        <!-- fwtable fwsrc="top-nav2_AdminID.png" fwbase="top-nav_AdminID.jpg" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
        <tr> 
          <td><img src="ui_1/images/spacer.gif" width="135" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="92" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="116" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="3" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="93" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="59" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="7" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="77" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="108" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="1" border="0"></td>
        </tr>
        <tr> 
          <td colspan="11"><img name="topnav_AdminID_r1_c1" src="ui_1/images/top-nav_AdminID_r1_c1.jpg" width="700" height="66" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="66" border="0"></td>
        </tr>
        <tr> 
          <td rowspan="2"><img name="topnav_AdminID_r2_c1" src="ui_1/images/top-nav_AdminID_r2_c1.jpg" width="135" height="34" border="0"></td>
          <td><a href="pro-file-manager_Status.jsp" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_0,135,91);" ><img name="topnav_AdminID_r2_c2" src="ui_1/images/top-nav_AdminID_r2_c2.jpg"border="0"></a></td>
          <td rowspan="2"><img name="topnav_AdminID_r2_c3" src="ui_1/images/top-nav_AdminID_r2_c3.jpg" width="5" height="34" border="0"></td>
          <td><a href="AdminPhysician_query.jsp?PhysicianID=&LastName=&FirstName=&SSN=&HomeCity=&HomeStateID=0&HomeZIP=&orderBy=PhysicianID&startID=0&maxResults=50&Submit2=Submit" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_1,231,91);" ><img name="topnav_AdminID_r2_c4" src="ui_1/images/top-nav_AdminID_r2_c4.jpg"  border="0"></a></td>
          <td rowspan="2"><img name="topnav_AdminID_r2_c5" src="ui_1/images/top-nav_AdminID_r2_c5.jpg" width="3" height="34" border="0"></td>
          <td><a href="AdminPractice_query.jsp?PracticeID=&PracticeName=&OfficeCity=&OfficeStateID=0&OfficeZip=&orderBy=PracticeID&startID=0&maxResults=50&Submit2=Submit" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_2,353,91);" ><img name="topnav_AdminID_r2_c4" src="ui_1/images/top-nav_AdminID_r2_c6.jpg"  border="0"></a></td>
          <td rowspan="2"><img name="topnav_AdminID_r2_c7" src="ui_1/images/top-nav_AdminID_r2_c7.jpg" width="5" height="34" border="0"></td>
          <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_3,449,91);" ><img name="topnav_AdminID_r2_c8" src="ui_1/images/top-nav_AdminID_r2_c8.jpg" border="0"></a></td>
          <td rowspan="2"><img name="topnav_AdminID_r2_c9" src="ui_1/images/top-nav_AdminID_r2_c9.jpg" width="7" height="34" border="0"></td>
          <td><a href="signOff.jsp"><img name="topnav_AdminID_r2_c10" src="ui_1/images/top-nav_AdminID_r2_c10.jpg" width="77" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_AdminID_r2_c11" src="ui_1/images/top-nav_AdminID_r2_c11.jpg" width="108" height="34" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="25" border="0"></td>
        </tr>
        <tr> 
          <td><img name="topnav_AdminID_r3_c2" src="ui_1/images/top-nav_AdminID_r3_c2.jpg" width="92" height="9" border="0"></td>
          <td><img name="topnav_AdminID_r3_c4" src="ui_1/images/top-nav_AdminID_r3_c4.jpg" width="116" height="9" border="0"></td>
          <td><img name="topnav_AdminID_r3_c6" src="ui_1/images/top-nav_AdminID_r3_c6.jpg" width="93" height="9" border="0"></td>
          <td><img name="topnav_AdminID_r3_c8" src="ui_1/images/top-nav_AdminID_r3_c8.jpg" width="59" height="9" border="0"></td>
          <td><img name="topnav_AdminID_r3_c10" src="ui_1/images/top-nav_AdminID_r3_c10.jpg" width="77" height="9" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="9" border="0"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table width="700" border="1" cellspacing="0" cellpadding="0" bordercolor="#999999">
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="1" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
              <tr>
                <td class=tdHeaderAlt nowrap>Admin Group:</td>
                <td class=tdHeader nowrap width="99%">&nbsp;<%=pm.getName()%></td>
                <td class=tdHeader nowrap>ID:&nbsp;<%=pm.getAdminID()%></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
	</table>
<br>
