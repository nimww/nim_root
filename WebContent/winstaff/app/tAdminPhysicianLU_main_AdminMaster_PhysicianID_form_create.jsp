<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltAdminMaster,com.winstaff.bltAdminPhysicianLU,com.winstaff.bltAdminPhysicianLU_List_LU_PhysicianID" %>
<%/*
    filename: out\jsp\tAdminPhysicianLU_main_AdminMaster_PhysicianID_form_create.jsp
    Created on Mar/21/2003
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Company1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }

//declaration of Enumeration
    bltAdminMaster        working_bltAdminMaster = new bltAdminMaster();
    working_bltAdminMaster.setUniqueCreateDate(new java.util.Date());
    working_bltAdminMaster.setUniqueModifyDate(new java.util.Date());
    working_bltAdminMaster.commitDataForced();
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltAdminMaster.setUniqueModifyComments(""+UserLogonDescription);
    }

    bltAdminPhysicianLU        working_bltAdminPhysicianLU = new bltAdminPhysicianLU();
    working_bltAdminPhysicianLU.setPhysicianID(iPhysicianID);
    working_bltAdminPhysicianLU.setAdminID(working_bltAdminMaster.getUniqueID());
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltAdminPhysicianLU.setUniqueModifyComments(""+UserLogonDescription);
    }

    working_bltAdminPhysicianLU.commitData();
    {
        pageControllerHash.put("iAdminID",working_bltAdminMaster.getUniqueID());
        pageControllerHash.put("sKeyMasterReference","p");
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tAdminMaster_form.jsp?nullParam=null"+parameterPassString    ;

        {
            targetRedirect = "tAdminMaster_form.jsp?EDITID="+working_bltAdminPhysicianLU.getUniqueID()+"&routePageReference=sParentReturnPage&EDIT=edit";
        }
        response.sendRedirect(targetRedirect);
    }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

