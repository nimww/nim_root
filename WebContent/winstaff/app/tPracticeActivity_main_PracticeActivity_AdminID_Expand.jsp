<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.*, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltPracticeActivity, com.winstaff.bltPracticeActivity_List_AdminID" %>
<%/*
    filename: tPracticeActivity_main_PracticeActivity_AdminID.jsp
    Created on Nov/04/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_1/style.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_AdminID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tPracticeActivity")%>

    <br><a href="tPracticeActivity_main_PracticeActivity_AdminID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tPracticeActivity_main_PracticeActivity_AdminID_Expand.jsp");
    pageControllerHash.remove("iPracticeActivityID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltPracticeActivity_List_AdminID        bltPracticeActivity_List_AdminID        =    new    bltPracticeActivity_List_AdminID(iAdminID);

//declaration of Enumeration
    bltPracticeActivity        working_bltPracticeActivity;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPracticeActivity_List_AdminID.elements();
    %>
        <%@ include file="tPracticeActivity_main_PracticeActivity_AdminID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <input type=button onClick = "this.disabled=true;document.location ='tPracticeActivity_main_PracticeActivity_AdminID_form_create.jsp?EDIT=new&KM=p'" value="Create">
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltPracticeActivity  = (bltPracticeActivity) leCurrentElement.getObject();
        working_bltPracticeActivity.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltPracticeActivity.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltPracticeActivity.getPracticeActivityID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPracticeActivity.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPracticeActivity.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltPracticeActivity.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            <td width="50%" bgColor=#ffffff> 
        <input type=button onClick = "this.disabled=true;document.location ='tPracticeActivity_main_PracticeActivity_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltPracticeActivity.getPracticeActivityID()%>&KM=p';" value="Edit">


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
       <br> <input type=button onClick = "confirmDelete2('','tPracticeActivity_main_PracticeActivity_AdminID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltPracticeActivity.getPracticeActivityID()%>&KM=p',this)" value="Delete">
        <br>
        <% 
            if (working_bltPracticeActivity.getDocuLinkID().intValue()>0)
            {
              bltDocumentManagement myDoc = new bltDocumentManagement(working_bltPracticeActivity.getDocuLinkID());
              if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
              {
              %>
                      <a class=linkBase target=_blank href = "pdf/<%=myDoc.getDocumentFileName()%>">View Document</a>

              <%
              }
              else
              {
              %>

                      <a class=linkBase target=_blank href = "#" onClick="window.open('tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=faxCover&amp;EDITID=<%=working_bltPracticeActivity.getDocuLinkID()%>','DocPDF','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=500');return false;" >Print Cover Sheet</a>

              <%
              }
            }
            else
            {
             if (working_bltPracticeActivity.isComplete())
             {
               %>
                      <a target=_blank class=linkBase href = "tPracticeActivity_ModifyDocument.jsp?EDITID=<%=working_bltPracticeActivity.getPracticeActivityID()%>&EDIT=CREATE&dType=tPracticeActivity">Create Document</a>
               <%
             }
             else
             {
               %>
                      <span class=tdBase>You must complete this item before you can attach a document</span>
               <%
             }
            }
            %>

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("AdminID",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("AdminID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("AdminID"))&&(!working_bltPracticeActivity.isComplete("AdminID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>AdminID:&nbsp;</b><%=working_bltPracticeActivity.getAdminID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("Name",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("Name"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("Name"))&&(!working_bltPracticeActivity.isComplete("Name")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Name:&nbsp;</b><%=working_bltPracticeActivity.getName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("ActivityType",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("ActivityType"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("ActivityType"))&&(!working_bltPracticeActivity.isComplete("ActivityType")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ActivityType:&nbsp;</b><%=working_bltPracticeActivity.getActivityType()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("isCompleted",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("isCompleted"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("isCompleted"))&&(!working_bltPracticeActivity.isComplete("isCompleted")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>isCompleted:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPracticeActivity.getisCompleted()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("StartDate",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("StartDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("StartDate"))&&(!working_bltPracticeActivity.isComplete("StartDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>StartDate:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPracticeActivity.getStartDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("EndDate",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("EndDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("EndDate"))&&(!working_bltPracticeActivity.isComplete("EndDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>EndDate:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPracticeActivity.getEndDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("RemindDate",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("RemindDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("RemindDate"))&&(!working_bltPracticeActivity.isComplete("RemindDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>RemindDate:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPracticeActivity.getRemindDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("Description",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("Description"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("Description"))&&(!working_bltPracticeActivity.isComplete("Description")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Description:&nbsp;</b><%=working_bltPracticeActivity.getDescription()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("TaskFileReference",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("TaskFileReference"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("TaskFileReference"))&&(!working_bltPracticeActivity.isComplete("TaskFileReference")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>TaskFileReference:&nbsp;</b><%=working_bltPracticeActivity.getTaskFileReference()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("TaskLogicReference",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("TaskLogicReference"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("TaskLogicReference"))&&(!working_bltPracticeActivity.isComplete("TaskLogicReference")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>TaskLogicReference:&nbsp;</b><%=working_bltPracticeActivity.getTaskLogicReference()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("Summary",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("Summary"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("Summary"))&&(!working_bltPracticeActivity.isComplete("Summary")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Summary:&nbsp;</b><%=working_bltPracticeActivity.getSummary()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("Comments",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("Comments"))&&(!working_bltPracticeActivity.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Comments:&nbsp;</b><%=working_bltPracticeActivity.getComments()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("ReferenceID",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("ReferenceID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("ReferenceID"))&&(!working_bltPracticeActivity.isComplete("ReferenceID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ReferenceID:&nbsp;</b><%=working_bltPracticeActivity.getReferenceID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPracticeActivity.isExpired("DoculinkID",expiredDays))&&(working_bltPracticeActivity.isExpiredCheck("DoculinkID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPracticeActivity.isRequired("DoculinkID"))&&(!working_bltPracticeActivity.isComplete("DoculinkID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>DoculinkID:&nbsp;</b><%=working_bltPracticeActivity.getDoculinkID()%></p>

        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_AdminID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>