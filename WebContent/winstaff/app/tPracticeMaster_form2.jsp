<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPracticeMaster" %>
<%/*

    filename: out\jsp\tPracticeMaster_form.jsp
    Created on Mar/03/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tPracticeMaster2")%>



<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tAlliedHealthProfessional_main_AlliedHealthProfessional_PracticeID.jsp");
    pageControllerHash.put("sParentReturnPage","tAlliedHealthProfessional_main_AlliedHealthProfessional_PracticeID.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID

    bltPracticeMaster        PracticeMaster        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PracticeMaster        =    new    bltPracticeMaster(iPracticeID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PracticeMaster        =    new    bltPracticeMaster(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tPracticeMaster_form_instructions.jsp" %>
        <form action="tPracticeMaster_form_sub.jsp" name="tPracticeMaster_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>
            <table cellpadding=0 cellspacing=0 width=100%>
<tr class=title><td colspan=2>Services Provided:</td></tr>

            <%
            if ( (PracticeMaster.isRequired("AnesthesiaLocal",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AnesthesiaLocal")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AnesthesiaLocal",expiredDays))&&(PracticeMaster.isExpiredCheck("AnesthesiaLocal",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("AnesthesiaLocal",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Anesthesia Local&nbsp;</b></p></td><td valign=top><p><select   name="AnesthesiaLocal" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAnesthesiaLocal()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnesthesiaLocal&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnesthesiaLocal")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("AnesthesiaLocal",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Anesthesia Local&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAnesthesiaLocal()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnesthesiaLocal&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnesthesiaLocal")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("AnesthesiaRegional",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AnesthesiaRegional")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AnesthesiaRegional",expiredDays))&&(PracticeMaster.isExpiredCheck("AnesthesiaRegional",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("AnesthesiaRegional",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Anesthesia Regional&nbsp;</b></p></td><td valign=top><p><select   name="AnesthesiaRegional" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAnesthesiaRegional()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnesthesiaRegional&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnesthesiaRegional")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("AnesthesiaRegional",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Anesthesia Regional&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAnesthesiaRegional()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnesthesiaRegional&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnesthesiaRegional")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("AnesthesiaConscious",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AnesthesiaConscious")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AnesthesiaConscious",expiredDays))&&(PracticeMaster.isExpiredCheck("AnesthesiaConscious",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("AnesthesiaConscious",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Anesthesia Conscious&nbsp;</b></p></td><td valign=top><p><select   name="AnesthesiaConscious" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAnesthesiaConscious()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnesthesiaConscious&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnesthesiaConscious")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("AnesthesiaConscious",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Anesthesia Conscious&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAnesthesiaConscious()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnesthesiaConscious&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnesthesiaConscious")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("AnesthesiaGeneral",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AnesthesiaGeneral")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AnesthesiaGeneral",expiredDays))&&(PracticeMaster.isExpiredCheck("AnesthesiaGeneral",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("AnesthesiaGeneral",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Anesthesia General&nbsp;</b></p></td><td valign=top><p><select   name="AnesthesiaGeneral" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAnesthesiaGeneral()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnesthesiaGeneral&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnesthesiaGeneral")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("AnesthesiaGeneral",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Anesthesia General&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAnesthesiaGeneral()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnesthesiaGeneral&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnesthesiaGeneral")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("MinorSurgery",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MinorSurgery")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MinorSurgery",expiredDays))&&(PracticeMaster.isExpiredCheck("MinorSurgery",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("MinorSurgery",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Minor Surgery&nbsp;</b></p></td><td valign=top><p><select   name="MinorSurgery" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMinorSurgery()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MinorSurgery&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MinorSurgery")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("MinorSurgery",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Minor Surgery&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMinorSurgery()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MinorSurgery&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MinorSurgery")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("Gynecology",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Gynecology")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Gynecology",expiredDays))&&(PracticeMaster.isExpiredCheck("Gynecology",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("Gynecology",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Gynecology&nbsp;</b></p></td><td valign=top><p><select   name="Gynecology" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getGynecology()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Gynecology&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Gynecology")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("Gynecology",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Gynecology&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getGynecology()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Gynecology&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Gynecology")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("XRayProcedures",UserSecurityGroupID))&&(!PracticeMaster.isComplete("XRayProcedures")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("XRayProcedures",expiredDays))&&(PracticeMaster.isExpiredCheck("XRayProcedures",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("XRayProcedures",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>X-ray Procedures&nbsp;</b></p></td><td valign=top><p><select   name="XRayProcedures" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getXRayProcedures()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=XRayProcedures&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("XRayProcedures")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("XRayProcedures",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>X-ray Procedures&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getXRayProcedures()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=XRayProcedures&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("XRayProcedures")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("DrawBlood",UserSecurityGroupID))&&(!PracticeMaster.isComplete("DrawBlood")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("DrawBlood",expiredDays))&&(PracticeMaster.isExpiredCheck("DrawBlood",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("DrawBlood",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Draw Blood&nbsp;</b></p></td><td valign=top><p><select   name="DrawBlood" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDrawBlood()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DrawBlood&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DrawBlood")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("DrawBlood",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Draw Blood&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDrawBlood()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DrawBlood&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DrawBlood")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("BasicLab",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BasicLab")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BasicLab",expiredDays))&&(PracticeMaster.isExpiredCheck("BasicLab",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("BasicLab",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Basic Lab (CBC, H&H, UA)&nbsp;</b></p></td><td valign=top><p><select   name="BasicLab" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getBasicLab()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BasicLab&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BasicLab")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("BasicLab",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Basic Lab (CBC, H&H, UA)&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getBasicLab()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BasicLab&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BasicLab")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("EKG",UserSecurityGroupID))&&(!PracticeMaster.isComplete("EKG")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("EKG",expiredDays))&&(PracticeMaster.isExpiredCheck("EKG",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("EKG",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>EKG&nbsp;</b></p></td><td valign=top><p><select   name="EKG" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getEKG()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EKG&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("EKG")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("EKG",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>EKG&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getEKG()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EKG&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("EKG")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("MinorLacerations",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MinorLacerations")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MinorLacerations",expiredDays))&&(PracticeMaster.isExpiredCheck("MinorLacerations",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("MinorLacerations",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Minor Lacerations&nbsp;</b></p></td><td valign=top><p><select   name="MinorLacerations" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMinorLacerations()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MinorLacerations&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MinorLacerations")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("MinorLacerations",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Minor Lacerations&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMinorLacerations()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MinorLacerations&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MinorLacerations")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("Pulmonary",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Pulmonary")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Pulmonary",expiredDays))&&(PracticeMaster.isExpiredCheck("Pulmonary",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("Pulmonary",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Pulmonary&nbsp;</b></p></td><td valign=top><p><select   name="Pulmonary" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getPulmonary()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Pulmonary&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Pulmonary")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("Pulmonary",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Pulmonary&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getPulmonary()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Pulmonary&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Pulmonary")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("Allergy",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Allergy")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Allergy",expiredDays))&&(PracticeMaster.isExpiredCheck("Allergy",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("Allergy",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Allergy&nbsp;</b></p></td><td valign=top><p><select   name="Allergy" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAllergy()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Allergy&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Allergy")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("Allergy",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Allergy&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAllergy()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Allergy&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Allergy")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("VisualScreen",UserSecurityGroupID))&&(!PracticeMaster.isComplete("VisualScreen")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("VisualScreen",expiredDays))&&(PracticeMaster.isExpiredCheck("VisualScreen",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("VisualScreen",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Visual Screen&nbsp;</b></p></td><td valign=top><p><select   name="VisualScreen" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getVisualScreen()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=VisualScreen&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("VisualScreen")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("VisualScreen",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Visual Screen&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getVisualScreen()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=VisualScreen&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("VisualScreen")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("Audiometry",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Audiometry")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Audiometry",expiredDays))&&(PracticeMaster.isExpiredCheck("Audiometry",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("Audiometry",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Audiometry&nbsp;</b></p></td><td valign=top><p><select   name="Audiometry" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAudiometry()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Audiometry&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Audiometry")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("Audiometry",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Audiometry&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAudiometry()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Audiometry&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Audiometry")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("Sigmoidoscopy",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Sigmoidoscopy")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Sigmoidoscopy",expiredDays))&&(PracticeMaster.isExpiredCheck("Sigmoidoscopy",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("Sigmoidoscopy",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Sigmoidoscopy&nbsp;</b></p></td><td valign=top><p><select   name="Sigmoidoscopy" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getSigmoidoscopy()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Sigmoidoscopy&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Sigmoidoscopy")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("Sigmoidoscopy",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Sigmoidoscopy&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getSigmoidoscopy()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Sigmoidoscopy&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Sigmoidoscopy")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("Immunizations",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Immunizations")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Immunizations",expiredDays))&&(PracticeMaster.isExpiredCheck("Immunizations",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("Immunizations",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Immunizations&nbsp;</b></p></td><td valign=top><p><select   name="Immunizations" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getImmunizations()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Immunizations&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Immunizations")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("Immunizations",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Immunizations&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getImmunizations()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Immunizations&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Immunizations")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("Asthma",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Asthma")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Asthma",expiredDays))&&(PracticeMaster.isExpiredCheck("Asthma",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("Asthma",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Asthma&nbsp;</b></p></td><td valign=top><p><select   name="Asthma" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAsthma()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Asthma&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Asthma")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("Asthma",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Asthma&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAsthma()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Asthma&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Asthma")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("IVTreatment",UserSecurityGroupID))&&(!PracticeMaster.isComplete("IVTreatment")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("IVTreatment",expiredDays))&&(PracticeMaster.isExpiredCheck("IVTreatment",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("IVTreatment",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>IV Treatment&nbsp;</b></p></td><td valign=top><p><select   name="IVTreatment" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getIVTreatment()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IVTreatment&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("IVTreatment")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("IVTreatment",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>IV Treatment&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getIVTreatment()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IVTreatment&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("IVTreatment")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("Osteopathic",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Osteopathic")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Osteopathic",expiredDays))&&(PracticeMaster.isExpiredCheck("Osteopathic",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("Osteopathic",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Osteopathic&nbsp;</b></p></td><td valign=top><p><select   name="Osteopathic" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOsteopathic()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Osteopathic&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Osteopathic")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("Osteopathic",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Osteopathic&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOsteopathic()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Osteopathic&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Osteopathic")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("Hydration",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Hydration")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Hydration",expiredDays))&&(PracticeMaster.isExpiredCheck("Hydration",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("Hydration",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Hydration&nbsp;</b></p></td><td valign=top><p><select   name="Hydration" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHydration()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Hydration&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Hydration")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("Hydration",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Hydration&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHydration()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Hydration&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Hydration")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("CardiacStress",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CardiacStress")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CardiacStress",expiredDays))&&(PracticeMaster.isExpiredCheck("CardiacStress",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("CardiacStress",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Cardiac Stress&nbsp;</b></p></td><td valign=top><p><select   name="CardiacStress" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCardiacStress()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CardiacStress&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CardiacStress")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("CardiacStress",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Cardiac Stress&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCardiacStress()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CardiacStress&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CardiacStress")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("PhysicalTherapy",UserSecurityGroupID))&&(!PracticeMaster.isComplete("PhysicalTherapy")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("PhysicalTherapy",expiredDays))&&(PracticeMaster.isExpiredCheck("PhysicalTherapy",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("PhysicalTherapy",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Physical Therapy&nbsp;</b></p></td><td valign=top><p><select   name="PhysicalTherapy" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getPhysicalTherapy()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PhysicalTherapy&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PhysicalTherapy")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("PhysicalTherapy",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Physical Therapy&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getPhysicalTherapy()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PhysicalTherapy&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PhysicalTherapy")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("MaternalHealth",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MaternalHealth")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MaternalHealth",expiredDays))&&(PracticeMaster.isExpiredCheck("MaternalHealth",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("MaternalHealth",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Maternal Health&nbsp;</b></p></td><td valign=top><p><select   name="MaternalHealth" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMaternalHealth()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MaternalHealth&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MaternalHealth")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("MaternalHealth",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Maternal Health&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMaternalHealth()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MaternalHealth&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MaternalHealth")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("CHDP",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CHDP")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CHDP",expiredDays))&&(PracticeMaster.isExpiredCheck("CHDP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("CHDP",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>CHDP Coordination & Education&nbsp;</b></p></td><td valign=top><p><select   name="CHDP" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCHDP()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CHDP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CHDP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("CHDP",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>CHDP Coordination & Education&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCHDP()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CHDP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CHDP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

            <%
            if ( (PracticeMaster.isRequired("DiagnosticUltraSound",UserSecurityGroupID))&&(!PracticeMaster.isComplete("DiagnosticUltraSound")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("DiagnosticUltraSound",expiredDays))&&(PracticeMaster.isExpiredCheck("DiagnosticUltraSound",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("DiagnosticUltraSound",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Diagnostic UltraSound&nbsp;</b></p></td><td valign=top><p><select   name="DiagnosticUltraSound" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDiagnosticUltraSound()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DiagnosticUltraSound&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DiagnosticUltraSound")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("DiagnosticUltraSound",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Diagnostic UltraSound&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDiagnosticUltraSound()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DiagnosticUltraSound&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DiagnosticUltraSound")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("Endoscopy",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Endoscopy")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Endoscopy",expiredDays))&&(PracticeMaster.isExpiredCheck("Endoscopy",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("Endoscopy",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Endoscopy&nbsp;</b></p></td><td valign=top><p><select   name="Endoscopy" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getEndoscopy()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Endoscopy&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Endoscopy")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("Endoscopy",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Endoscopy&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getEndoscopy()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Endoscopy&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Endoscopy")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeMaster.isRequired("OfficeServicesComments",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeServicesComments")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeServicesComments",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeServicesComments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("OfficeServicesComments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("OfficeServicesComments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this)" rows="2" name="OfficeServicesComments" cols="40" maxlength=200><%=PracticeMaster.getOfficeServicesComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeServicesComments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeServicesComments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("OfficeServicesComments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("OfficeServicesComments")%>&nbsp;</b></p></td><td valign=top><p><%=PracticeMaster.getOfficeServicesComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeServicesComments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeServicesComments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

<tr class=title><td colspan=2><br><hr>Facility Information:</td></tr>


            <%
            if ( (PracticeMaster.isRequired("LicenseDisplayed",UserSecurityGroupID))&&(!PracticeMaster.isComplete("LicenseDisplayed")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("LicenseDisplayed",expiredDays))&&(PracticeMaster.isExpiredCheck("LicenseDisplayed",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("LicenseDisplayed",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are all licenses & certification for all current staff displayed or available?&nbsp;</b></p></td><td valign=top><p><select   name="LicenseDisplayed" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getLicenseDisplayed()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LicenseDisplayed&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("LicenseDisplayed")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("LicenseDisplayed",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are all licenses & certification for all current staff displayed or available?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getLicenseDisplayed()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LicenseDisplayed&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("LicenseDisplayed")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("CPRPresent",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CPRPresent")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CPRPresent",expiredDays))&&(PracticeMaster.isExpiredCheck("CPRPresent",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("CPRPresent",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is a staff person or physician currently CPR Certified and in attendance during office hours?&nbsp;</b></p></td><td valign=top><p><select   name="CPRPresent" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCPRPresent()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CPRPresent&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CPRPresent")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("CPRPresent",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is a staff person or physician currently CPR Certified and in attendance during office hours?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCPRPresent()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CPRPresent&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CPRPresent")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("AmbuBagAvailable",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AmbuBagAvailable")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AmbuBagAvailable",expiredDays))&&(PracticeMaster.isExpiredCheck("AmbuBagAvailable",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("AmbuBagAvailable",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Ambu Bag, Airways, Suction Devices, Adrenalin present?&nbsp;</b></p></td><td valign=top><p><select   name="AmbuBagAvailable" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAmbuBagAvailable()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AmbuBagAvailable&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AmbuBagAvailable")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("AmbuBagAvailable",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Ambu Bag, Airways, Suction Devices, Adrenalin present?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAmbuBagAvailable()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AmbuBagAvailable&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AmbuBagAvailable")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("OxygenAvailable",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OxygenAvailable")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OxygenAvailable",expiredDays))&&(PracticeMaster.isExpiredCheck("OxygenAvailable",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("OxygenAvailable",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is oxygen available,  clearly marked & secured?&nbsp;</b></p></td><td valign=top><p><select   name="OxygenAvailable" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOxygenAvailable()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OxygenAvailable&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OxygenAvailable")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("OxygenAvailable",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is oxygen available,  clearly marked & secured?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOxygenAvailable()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OxygenAvailable&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OxygenAvailable")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("SurgicalSuite",UserSecurityGroupID))&&(!PracticeMaster.isComplete("SurgicalSuite")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("SurgicalSuite",expiredDays))&&(PracticeMaster.isExpiredCheck("SurgicalSuite",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("SurgicalSuite",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Surgical Suite&nbsp;</b></p></td><td valign=top><p><select   name="SurgicalSuite" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getSurgicalSuite()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SurgicalSuite&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("SurgicalSuite")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("SurgicalSuite",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Surgical Suite&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getSurgicalSuite()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SurgicalSuite&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("SurgicalSuite")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("CertType",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CertType")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CertType",expiredDays))&&(PracticeMaster.isExpiredCheck("CertType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("CertType",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Surgical Suite Certification Type&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="CertType" value="<%=PracticeMaster.getCertType()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertType&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CertType")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("CertType",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Surgical Suite Certification Type&nbsp;</b></p></td><td valign=top><p><%=PracticeMaster.getCertType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CertType&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CertType")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeMaster.isRequired("LabOnSite",UserSecurityGroupID))&&(!PracticeMaster.isComplete("LabOnSite")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("LabOnSite",expiredDays))&&(PracticeMaster.isExpiredCheck("LabOnSite",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("LabOnSite",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have a lab on-site?&nbsp;</b></p></td><td valign=top><p><select   name="LabOnSite" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getLabOnSite()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LabOnSite&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("LabOnSite")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("LabOnSite",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have a lab on-site?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getLabOnSite()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LabOnSite&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("LabOnSite")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("CLIANumber",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CLIANumber")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CLIANumber",expiredDays))&&(PracticeMaster.isExpiredCheck("CLIANumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("CLIANumber",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>CLIA Number&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="CLIANumber" value="<%=PracticeMaster.getCLIANumber()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CLIANumber&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CLIANumber")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("CLIANumber",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>CLIA Number&nbsp;</b></p></td><td valign=top><p><%=PracticeMaster.getCLIANumber()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CLIANumber&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CLIANumber")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeMaster.isRequired("CLIAWaiver",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CLIAWaiver")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CLIAWaiver",expiredDays))&&(PracticeMaster.isExpiredCheck("CLIAWaiver",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("CLIAWaiver",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>CLIA Waiver Number&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="CLIAWaiver" value="<%=PracticeMaster.getCLIAWaiver()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CLIAWaiver&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CLIAWaiver")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("CLIAWaiver",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>CLIA Waiver Number&nbsp;</b></p></td><td valign=top><p><%=PracticeMaster.getCLIAWaiver()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CLIAWaiver&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CLIAWaiver")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (PracticeMaster.isRequired("CLIAExpirationDate",UserSecurityGroupID))&&(!PracticeMaster.isComplete("CLIAExpirationDate")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("CLIAExpirationDate",expiredDays))&&(PracticeMaster.isExpiredCheck("CLIAExpirationDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((PracticeMaster.isWrite("CLIAExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>CLIA Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="CLIAExpirationDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeMaster.getCLIAExpirationDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CLIAExpirationDate&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CLIAExpirationDate")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("CLIAExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>CLIA Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeMaster.getCLIAExpirationDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CLIAExpirationDate&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CLIAExpirationDate")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("ADAAccessibility",UserSecurityGroupID))&&(!PracticeMaster.isComplete("ADAAccessibility")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("ADAAccessibility",expiredDays))&&(PracticeMaster.isExpiredCheck("ADAAccessibility",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("ADAAccessibility",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have ADA Accessibility?&nbsp;</b></p></td><td valign=top><p><select   name="ADAAccessibility" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getADAAccessibility()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ADAAccessibility&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("ADAAccessibility")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("ADAAccessibility",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have ADA Accessibility?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getADAAccessibility()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ADAAccessibility&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("ADAAccessibility")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("HandicapAccessBuilding",UserSecurityGroupID))&&(!PracticeMaster.isComplete("HandicapAccessBuilding")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("HandicapAccessBuilding",expiredDays))&&(PracticeMaster.isExpiredCheck("HandicapAccessBuilding",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("HandicapAccessBuilding",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have Handicap Building Access with Ramp?&nbsp;</b></p></td><td valign=top><p><select   name="HandicapAccessBuilding" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessBuilding()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessBuilding&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessBuilding")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("HandicapAccessBuilding",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have Handicap Building Access with Ramp?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessBuilding()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessBuilding&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessBuilding")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("HandicapAccessRampRail",UserSecurityGroupID))&&(!PracticeMaster.isComplete("HandicapAccessRampRail")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("HandicapAccessRampRail",expiredDays))&&(PracticeMaster.isExpiredCheck("HandicapAccessRampRail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("HandicapAccessRampRail",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are there handrails on both sides of the ramp?&nbsp;</b></p></td><td valign=top><p><select   name="HandicapAccessRampRail" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessRampRail()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessRampRail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessRampRail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("HandicapAccessRampRail",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are there handrails on both sides of the ramp?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessRampRail()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessRampRail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessRampRail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("HandicapAccessParking",UserSecurityGroupID))&&(!PracticeMaster.isComplete("HandicapAccessParking")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("HandicapAccessParking",expiredDays))&&(PracticeMaster.isExpiredCheck("HandicapAccessParking",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("HandicapAccessParking",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have Handicap Parking?&nbsp;</b></p></td><td valign=top><p><select   name="HandicapAccessParking" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessParking()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessParking&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessParking")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("HandicapAccessParking",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have Handicap Parking?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessParking()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessParking&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessParking")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("HandicapAccessWheel",UserSecurityGroupID))&&(!PracticeMaster.isComplete("HandicapAccessWheel")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("HandicapAccessWheel",expiredDays))&&(PracticeMaster.isExpiredCheck("HandicapAccessWheel",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("HandicapAccessWheel",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have Wheelchair Access?&nbsp;</b></p></td><td valign=top><p><select   name="HandicapAccessWheel" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessWheel()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessWheel&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessWheel")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("HandicapAccessWheel",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have Wheelchair Access?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessWheel()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessWheel&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessWheel")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("HandicapAccessRail",UserSecurityGroupID))&&(!PracticeMaster.isComplete("HandicapAccessRail")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("HandicapAccessRail",expiredDays))&&(PracticeMaster.isExpiredCheck("HandicapAccessRail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("HandicapAccessRail",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have Handicap Rails in the Bathroom?&nbsp;</b></p></td><td valign=top><p><select   name="HandicapAccessRail" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessRail()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessRail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessRail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("HandicapAccessRail",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have Handicap Rails in the Bathroom?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessRail()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessRail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessRail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("HandicapAccessElevator",UserSecurityGroupID))&&(!PracticeMaster.isComplete("HandicapAccessElevator")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("HandicapAccessElevator",expiredDays))&&(PracticeMaster.isExpiredCheck("HandicapAccessElevator",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("HandicapAccessElevator",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have Elevator available if office is above the first floor?&nbsp;</b></p></td><td valign=top><p><select   name="HandicapAccessElevator" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessElevator()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessElevator&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessElevator")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("HandicapAccessElevator",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have Elevator available if office is above the first floor?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessElevator()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessElevator&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessElevator")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("HandicapAccessBraille",UserSecurityGroupID))&&(!PracticeMaster.isComplete("HandicapAccessBraille")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("HandicapAccessBraille",expiredDays))&&(PracticeMaster.isExpiredCheck("HandicapAccessBraille",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("HandicapAccessBraille",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have Elevator Brail indicators in building with more than one story?&nbsp;</b></p></td><td valign=top><p><select   name="HandicapAccessBraille" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessBraille()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessBraille&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessBraille")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("HandicapAccessBraille",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have Elevator Brail indicators in building with more than one story?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapAccessBraille()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapAccessBraille&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapAccessBraille")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("HandicapFountainPhone",UserSecurityGroupID))&&(!PracticeMaster.isComplete("HandicapFountainPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("HandicapFountainPhone",expiredDays))&&(PracticeMaster.isExpiredCheck("HandicapFountainPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("HandicapFountainPhone",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have water fountains and telephones at proper height for wheelchair patients?&nbsp;</b></p></td><td valign=top><p><select   name="HandicapFountainPhone" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapFountainPhone()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapFountainPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapFountainPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("HandicapFountainPhone",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have water fountains and telephones at proper height for wheelchair patients?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getHandicapFountainPhone()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HandicapFountainPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("HandicapFountainPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("NearPublicTransportation",UserSecurityGroupID))&&(!PracticeMaster.isComplete("NearPublicTransportation")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("NearPublicTransportation",expiredDays))&&(PracticeMaster.isExpiredCheck("NearPublicTransportation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("NearPublicTransportation",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are you near public transportation?&nbsp;</b></p></td><td valign=top><p><select   name="NearPublicTransportation" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getNearPublicTransportation()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NearPublicTransportation&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("NearPublicTransportation")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("NearPublicTransportation",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are you near public transportation?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getNearPublicTransportation()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NearPublicTransportation&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("NearPublicTransportation")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("DisabledTTY",UserSecurityGroupID))&&(!PracticeMaster.isComplete("DisabledTTY")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("DisabledTTY",expiredDays))&&(PracticeMaster.isExpiredCheck("DisabledTTY",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("DisabledTTY",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you provide Text Telephony - TTY&nbsp;</b></p></td><td valign=top><p><select   name="DisabledTTY" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDisabledTTY()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DisabledTTY&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DisabledTTY")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("DisabledTTY",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you provide Text Telephony - TTY&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDisabledTTY()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DisabledTTY&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DisabledTTY")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("DisabledASL",UserSecurityGroupID))&&(!PracticeMaster.isComplete("DisabledASL")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("DisabledASL",expiredDays))&&(PracticeMaster.isExpiredCheck("DisabledASL",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("DisabledASL",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you provide American Sign Language - ASL&nbsp;</b></p></td><td valign=top><p><select   name="DisabledASL" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDisabledASL()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DisabledASL&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DisabledASL")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("DisabledASL",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you provide American Sign Language - ASL&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDisabledASL()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DisabledASL&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DisabledASL")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("ChildcareServices",UserSecurityGroupID))&&(!PracticeMaster.isComplete("ChildcareServices")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("ChildcareServices",expiredDays))&&(PracticeMaster.isExpiredCheck("ChildcareServices",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("ChildcareServices",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you provide childcare services&nbsp;</b></p></td><td valign=top><p><select   name="ChildcareServices" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getChildcareServices()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ChildcareServices&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("ChildcareServices")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("ChildcareServices",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you provide childcare services&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getChildcareServices()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ChildcareServices&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("ChildcareServices")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("SafetyFireExtinguisher",UserSecurityGroupID))&&(!PracticeMaster.isComplete("SafetyFireExtinguisher")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("SafetyFireExtinguisher",expiredDays))&&(PracticeMaster.isExpiredCheck("SafetyFireExtinguisher",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("SafetyFireExtinguisher",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is the fire extinguisher accessible?&nbsp;</b></p></td><td valign=top><p><select   name="SafetyFireExtinguisher" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getSafetyFireExtinguisher()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SafetyFireExtinguisher&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("SafetyFireExtinguisher")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("SafetyFireExtinguisher",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is the fire extinguisher accessible?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getSafetyFireExtinguisher()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SafetyFireExtinguisher&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("SafetyFireExtinguisher")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("SafetyExtinguisherInspected",UserSecurityGroupID))&&(!PracticeMaster.isComplete("SafetyExtinguisherInspected")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("SafetyExtinguisherInspected",expiredDays))&&(PracticeMaster.isExpiredCheck("SafetyExtinguisherInspected",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("SafetyExtinguisherInspected",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are the extinguisher inspected and logged routinely?&nbsp;</b></p></td><td valign=top><p><select   name="SafetyExtinguisherInspected" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getSafetyExtinguisherInspected()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SafetyExtinguisherInspected&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("SafetyExtinguisherInspected")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("SafetyExtinguisherInspected",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are the extinguisher inspected and logged routinely?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getSafetyExtinguisherInspected()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SafetyExtinguisherInspected&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("SafetyExtinguisherInspected")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("SafetySmokeDetectors",UserSecurityGroupID))&&(!PracticeMaster.isComplete("SafetySmokeDetectors")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("SafetySmokeDetectors",expiredDays))&&(PracticeMaster.isExpiredCheck("SafetySmokeDetectors",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("SafetySmokeDetectors",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are there smoke detectors or a sprinkler system on premises?&nbsp;</b></p></td><td valign=top><p><select   name="SafetySmokeDetectors" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getSafetySmokeDetectors()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SafetySmokeDetectors&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("SafetySmokeDetectors")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("SafetySmokeDetectors",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are there smoke detectors or a sprinkler system on premises?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getSafetySmokeDetectors()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SafetySmokeDetectors&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("SafetySmokeDetectors")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("SafetyCorridorsClear",UserSecurityGroupID))&&(!PracticeMaster.isComplete("SafetyCorridorsClear")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("SafetyCorridorsClear",expiredDays))&&(PracticeMaster.isExpiredCheck("SafetyCorridorsClear",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("SafetyCorridorsClear",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are the corridors and hallways clear?&nbsp;</b></p></td><td valign=top><p><select   name="SafetyCorridorsClear" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getSafetyCorridorsClear()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SafetyCorridorsClear&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("SafetyCorridorsClear")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("SafetyCorridorsClear",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are the corridors and hallways clear?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getSafetyCorridorsClear()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SafetyCorridorsClear&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("SafetyCorridorsClear")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("InfectionControlHandwashing",UserSecurityGroupID))&&(!PracticeMaster.isComplete("InfectionControlHandwashing")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("InfectionControlHandwashing",expiredDays))&&(PracticeMaster.isExpiredCheck("InfectionControlHandwashing",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("InfectionControlHandwashing",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is there access for hand washing in exam room or proximate to exam room?&nbsp;</b></p></td><td valign=top><p><select   name="InfectionControlHandwashing" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getInfectionControlHandwashing()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InfectionControlHandwashing&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("InfectionControlHandwashing")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("InfectionControlHandwashing",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is there access for hand washing in exam room or proximate to exam room?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getInfectionControlHandwashing()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InfectionControlHandwashing&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("InfectionControlHandwashing")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("InfectionControlGloves",UserSecurityGroupID))&&(!PracticeMaster.isComplete("InfectionControlGloves")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("InfectionControlGloves",expiredDays))&&(PracticeMaster.isExpiredCheck("InfectionControlGloves",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("InfectionControlGloves",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are gloves changed between patients?&nbsp;</b></p></td><td valign=top><p><select   name="InfectionControlGloves" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getInfectionControlGloves()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InfectionControlGloves&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("InfectionControlGloves")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("InfectionControlGloves",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are gloves changed between patients?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getInfectionControlGloves()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InfectionControlGloves&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("InfectionControlGloves")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("InfectionControlBloodCleaned",UserSecurityGroupID))&&(!PracticeMaster.isComplete("InfectionControlBloodCleaned")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("InfectionControlBloodCleaned",expiredDays))&&(PracticeMaster.isExpiredCheck("InfectionControlBloodCleaned",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("InfectionControlBloodCleaned",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are surfaces exposed to blood or body fluids cleansed with appropriate disinfectant?&nbsp;</b></p></td><td valign=top><p><select   name="InfectionControlBloodCleaned" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getInfectionControlBloodCleaned()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InfectionControlBloodCleaned&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("InfectionControlBloodCleaned")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("InfectionControlBloodCleaned",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are surfaces exposed to blood or body fluids cleansed with appropriate disinfectant?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getInfectionControlBloodCleaned()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InfectionControlBloodCleaned&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("InfectionControlBloodCleaned")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("FacilityComments",UserSecurityGroupID))&&(!PracticeMaster.isComplete("FacilityComments")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("FacilityComments",expiredDays))&&(PracticeMaster.isExpiredCheck("FacilityComments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("FacilityComments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("FacilityComments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this)" rows="2" name="FacilityComments" cols="40" maxlength=200><%=PracticeMaster.getFacilityComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityComments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("FacilityComments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("FacilityComments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("FacilityComments")%>&nbsp;</b></p></td><td valign=top><p><%=PracticeMaster.getFacilityComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityComments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("FacilityComments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

<tr class=title><td colspan=2><br><hr>Medical Records Information:</td></tr>


            <%
            if ( (PracticeMaster.isRequired("MedicalRecordsIndividual",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MedicalRecordsIndividual")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MedicalRecordsIndividual",expiredDays))&&(PracticeMaster.isExpiredCheck("MedicalRecordsIndividual",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("MedicalRecordsIndividual",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Does each patient have an individual medical record?&nbsp;</b></p></td><td valign=top><p><select   name="MedicalRecordsIndividual" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsIndividual()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsIndividual&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsIndividual")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("MedicalRecordsIndividual",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Does each patient have an individual medical record?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsIndividual()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsIndividual&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsIndividual")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("MedicalRecordsInaccessible",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MedicalRecordsInaccessible")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MedicalRecordsInaccessible",expiredDays))&&(PracticeMaster.isExpiredCheck("MedicalRecordsInaccessible",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("MedicalRecordsInaccessible",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are medical records kept in an area which is inaccessible to patients?&nbsp;</b></p></td><td valign=top><p><select   name="MedicalRecordsInaccessible" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsInaccessible()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsInaccessible&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsInaccessible")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("MedicalRecordsInaccessible",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are medical records kept in an area which is inaccessible to patients?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsInaccessible()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsInaccessible&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsInaccessible")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("MedicalRecordsUniform",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MedicalRecordsUniform")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MedicalRecordsUniform",expiredDays))&&(PracticeMaster.isExpiredCheck("MedicalRecordsUniform",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("MedicalRecordsUniform",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are medical records maintained in a  uniform format?&nbsp;</b></p></td><td valign=top><p><select   name="MedicalRecordsUniform" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsUniform()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsUniform&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsUniform")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("MedicalRecordsUniform",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Are medical records maintained in a  uniform format?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsUniform()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsUniform&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsUniform")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("MedicalRecordsPatientsNamePerPage",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MedicalRecordsPatientsNamePerPage")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MedicalRecordsPatientsNamePerPage",expiredDays))&&(PracticeMaster.isExpiredCheck("MedicalRecordsPatientsNamePerPage",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("MedicalRecordsPatientsNamePerPage",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is there a place on each page for the patient's name?&nbsp;</b></p></td><td valign=top><p><select   name="MedicalRecordsPatientsNamePerPage" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsPatientsNamePerPage()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsPatientsNamePerPage&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsPatientsNamePerPage")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("MedicalRecordsPatientsNamePerPage",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is there a place on each page for the patient's name?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsPatientsNamePerPage()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsPatientsNamePerPage&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsPatientsNamePerPage")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("MedicalRecordsSignatureAuthor",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MedicalRecordsSignatureAuthor")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MedicalRecordsSignatureAuthor",expiredDays))&&(PracticeMaster.isExpiredCheck("MedicalRecordsSignatureAuthor",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("MedicalRecordsSignatureAuthor",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is there a place for signature of each author?&nbsp;</b></p></td><td valign=top><p><select   name="MedicalRecordsSignatureAuthor" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsSignatureAuthor()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsSignatureAuthor&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsSignatureAuthor")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("MedicalRecordsSignatureAuthor",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is there a place for signature of each author?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsSignatureAuthor()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsSignatureAuthor&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsSignatureAuthor")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("MedicalRecordsEntryDate",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MedicalRecordsEntryDate")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MedicalRecordsEntryDate",expiredDays))&&(PracticeMaster.isExpiredCheck("MedicalRecordsEntryDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("MedicalRecordsEntryDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is there a place for each entry to be dated?&nbsp;</b></p></td><td valign=top><p><select   name="MedicalRecordsEntryDate" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsEntryDate()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsEntryDate&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsEntryDate")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("MedicalRecordsEntryDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is there a place for each entry to be dated?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsEntryDate()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsEntryDate&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsEntryDate")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("MedicalRecordsHistory",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MedicalRecordsHistory")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MedicalRecordsHistory",expiredDays))&&(PracticeMaster.isExpiredCheck("MedicalRecordsHistory",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("MedicalRecordsHistory",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is there a place for medical history in the record?&nbsp;</b></p></td><td valign=top><p><select   name="MedicalRecordsHistory" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsHistory()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsHistory&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsHistory")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("MedicalRecordsHistory",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is there a place for medical history in the record?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsHistory()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsHistory&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsHistory")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("MedicalRecordsAdverse",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MedicalRecordsAdverse")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MedicalRecordsAdverse",expiredDays))&&(PracticeMaster.isExpiredCheck("MedicalRecordsAdverse",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("MedicalRecordsAdverse",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is there a place for allergies and adverse reactions to medications to be prominently noted in the record?&nbsp;</b></p></td><td valign=top><p><select   name="MedicalRecordsAdverse" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsAdverse()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsAdverse&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsAdverse")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("MedicalRecordsAdverse",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is there a place for allergies and adverse reactions to medications to be prominently noted in the record?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMedicalRecordsAdverse()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsAdverse&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsAdverse")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PracticeMaster.isRequired("MedicalRecordsComments",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MedicalRecordsComments")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MedicalRecordsComments",expiredDays))&&(PracticeMaster.isExpiredCheck("MedicalRecordsComments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("MedicalRecordsComments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("MedicalRecordsComments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this)" rows="2" name="MedicalRecordsComments" cols="40" maxlength=200><%=PracticeMaster.getMedicalRecordsComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsComments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsComments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("MedicalRecordsComments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("MedicalRecordsComments")%>&nbsp;</b></p></td><td valign=top><p><%=PracticeMaster.getMedicalRecordsComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicalRecordsComments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicalRecordsComments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

<tr class=title><td colspan=2><br><hr>Additional Procedures:</td></tr>


            <%
            if ( (PracticeMaster.isRequired("AcceptReferralsComments",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AcceptReferralsComments")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AcceptReferralsComments",expiredDays))&&(PracticeMaster.isExpiredCheck("AcceptReferralsComments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("AcceptReferralsComments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("AcceptReferralsComments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="AcceptReferralsComments" cols="40" maxlength=200><%=PracticeMaster.getAcceptReferralsComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AcceptReferralsComments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AcceptReferralsComments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("AcceptReferralsComments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("AcceptReferralsComments")%>&nbsp;</b></p></td><td valign=top><p><%=PracticeMaster.getAcceptReferralsComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AcceptReferralsComments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AcceptReferralsComments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeMaster.isRequired("PatientBringComments",UserSecurityGroupID))&&(!PracticeMaster.isComplete("PatientBringComments")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("PatientBringComments",expiredDays))&&(PracticeMaster.isExpiredCheck("PatientBringComments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("PatientBringComments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("PatientBringComments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="PatientBringComments" cols="40" maxlength=200><%=PracticeMaster.getPatientBringComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientBringComments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PatientBringComments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("PatientBringComments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("PatientBringComments")%>&nbsp;</b></p></td><td valign=top><p><%=PracticeMaster.getPatientBringComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientBringComments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PatientBringComments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PracticeMaster.isRequired("PatientReferralQuestions",UserSecurityGroupID))&&(!PracticeMaster.isComplete("PatientReferralQuestions")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("PatientReferralQuestions",expiredDays))&&(PracticeMaster.isExpiredCheck("PatientReferralQuestions",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PracticeMaster.isWrite("PatientReferralQuestions",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("PatientReferralQuestions")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="PatientReferralQuestions" cols="40" maxlength=200><%=PracticeMaster.getPatientReferralQuestions()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientReferralQuestions&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PatientReferralQuestions")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PracticeMaster.isRead("PatientReferralQuestions",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("PatientReferralQuestions")%>&nbsp;</b></p></td><td valign=top><p><%=PracticeMaster.getPatientReferralQuestions()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientReferralQuestions&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PatientReferralQuestions")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>





            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tPracticeMaster")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tPracticeMaster")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PracticeID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
