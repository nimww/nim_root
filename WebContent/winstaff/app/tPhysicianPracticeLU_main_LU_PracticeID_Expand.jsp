<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPhysicianPracticeLU,com.winstaff.bltPhysicianPracticeLU_List_LU_PracticeID" %>
<%/*
    filename: tPhysicianPracticeLU_main_LU_PracticeID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tPhysicianPracticeLU")%>

    <br><a href="tPhysicianPracticeLU_main_LU_PracticeID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tPhysicianPracticeLU_main_LU_PracticeID_Expand.jsp");
    pageControllerHash.remove("iLookupID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltPhysicianPracticeLU_List_LU_PracticeID        bltPhysicianPracticeLU_List_LU_PracticeID        =    new    bltPhysicianPracticeLU_List_LU_PracticeID(iPracticeID);

//declaration of Enumeration
    bltPhysicianPracticeLU        working_bltPhysicianPracticeLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPhysicianPracticeLU_List_LU_PracticeID.elements();
    %>
        <%@ include file="tPhysicianPracticeLU_main_LU_PracticeID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tPhysicianPracticeLU_main_LU_PracticeID_form_create.jsp?EDIT=new&KM=s"><img border=0 src="ui_<%=thePLCID%>/icons/create_PracticeID.gif"></a>
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltPhysicianPracticeLU  = (bltPhysicianPracticeLU) leCurrentElement.getObject();
        working_bltPhysicianPracticeLU.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltPhysicianPracticeLU.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltPhysicianPracticeLU.getLookupID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianPracticeLU.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianPracticeLU.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltPhysicianPracticeLU.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%" bgColor=#ffffff> 
        <a class=linkBase href = "tPhysicianPracticeLU_main_LU_PracticeID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltPhysicianPracticeLU.getLookupID()%>&KM=s"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PracticeID.gif"></a>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <br><a class=linkBase  onClick="return confirmDelete()"  href = "tPhysicianPracticeLU_main_LU_PracticeID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltPhysicianPracticeLU.getLookupID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PracticeID.gif"></a>

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianPracticeLU.isExpired("PracticeID",expiredDays))&&(working_bltPhysicianPracticeLU.isExpiredCheck("PracticeID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianPracticeLU.isRequired("PracticeID"))&&(!working_bltPhysicianPracticeLU.isComplete("PracticeID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>PracticeID:&nbsp;</b><%=working_bltPhysicianPracticeLU.getPracticeID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianPracticeLU.isExpired("StartDate",expiredDays))&&(working_bltPhysicianPracticeLU.isExpiredCheck("StartDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianPracticeLU.isRequired("StartDate"))&&(!working_bltPhysicianPracticeLU.isComplete("StartDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Start Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianPracticeLU.getStartDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPhysicianPracticeLU.isExpired("EndDate",expiredDays))&&(working_bltPhysicianPracticeLU.isExpiredCheck("EndDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianPracticeLU.isRequired("EndDate"))&&(!working_bltPhysicianPracticeLU.isComplete("EndDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>End Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltPhysicianPracticeLU.getEndDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltPhysicianPracticeLU.isExpired("isPrimaryOffice",expiredDays))&&(working_bltPhysicianPracticeLU.isExpiredCheck("isPrimaryOffice"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianPracticeLU.isRequired("isPrimaryOffice"))&&(!working_bltPhysicianPracticeLU.isComplete("isPrimaryOffice")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Is this your primary office?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianPracticeLU.getisPrimaryOffice()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianPracticeLU.isExpired("isAdministrativeOffice",expiredDays))&&(working_bltPhysicianPracticeLU.isExpiredCheck("isAdministrativeOffice"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianPracticeLU.isRequired("isAdministrativeOffice"))&&(!working_bltPhysicianPracticeLU.isComplete("isAdministrativeOffice")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Is this your administrative office?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltPhysicianPracticeLU.getisAdministrativeOffice()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianPracticeLU.isExpired("CoverageHours",expiredDays))&&(working_bltPhysicianPracticeLU.isExpiredCheck("CoverageHours"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianPracticeLU.isRequired("CoverageHours"))&&(!working_bltPhysicianPracticeLU.isComplete("CoverageHours")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Coverage Hours:&nbsp;</b><%=working_bltPhysicianPracticeLU.getCoverageHours()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltPhysicianPracticeLU.isExpired("Comments",expiredDays))&&(working_bltPhysicianPracticeLU.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPhysicianPracticeLU.isRequired("Comments"))&&(!working_bltPhysicianPracticeLU.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltPhysicianPracticeLU.getComments()%></p>

        </td></tr></table></table><br>        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PracticeID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
