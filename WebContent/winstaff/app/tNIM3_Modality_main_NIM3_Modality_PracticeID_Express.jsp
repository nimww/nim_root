<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.*, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltNIM3_Modality, com.winstaff.bltNIM3_Modality_List" %>
<%/*
    filename: tNIM3_Modality_main_NIM3_Modality_PracticeID.jsp
    Created on Oct/28/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

<link href="ui_1/style.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("EXPRESSTopControl","tNIM3_Modality",iExpressMode)%>



<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeID") ) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;
    }  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_Modality_main_NIM3_Modality_PracticeID_Expand.jsp");
    pageControllerHash.remove("iModalityID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltNIM3_Modality_List        bltNIM3_Modality_List        =    new    bltNIM3_Modality_List(iPracticeID);

//declaration of Enumeration
    bltNIM3_Modality        NIM3_Modality;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Modality_List.elements();
    %>
        <%@ include file="tNIM3_Modality_main_NIM3_Modality_PracticeID_instructions.jsp" %>

        <form action="tNIM3_Modality_main_NIM3_Modality_PracticeID_Express_sub.jsp" name="tNIM3_Modality_main_NIM3_Modality_PracticeID1" method="POST">
         <table border="0" bordercolor="333333" cellpadding="0"  cellspacing="0" width="100%">        <tr><td>
        <input <%=HTMLFormStyleButton%> onClick="this.disable=true;document.forms[0].nextPage.value='<%=ConfigurationMessages.getExpressLinkRaw("tNIM3_Modality","next",iExpressMode)%>'" type=Submit value="Save & Continue" name=Submit>
        </td></tr>
    <%
    int iExpress=0;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tNIM3_Modality"))
    {
       iExpress++;
         %>
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        NIM3_Modality  = (bltNIM3_Modality) leCurrentElement.getObject();
      }
      else
      {
        NIM3_Modality  = new bltNIM3_Modality();
        isNewRecord= true;
      }
        NIM3_Modality.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tNIM3_Modality")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

  {

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase><tr><td>
        <table cellpadding=0 border=0 cellspacing=0 width=100%>




            <%
            if ( (NIM3_Modality.isRequired("ModalityTypeID",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("ModalityTypeID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((NIM3_Modality.isExpired("ModalityTypeID",expiredDays))&&(NIM3_Modality.isExpiredCheck("ModalityTypeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("ModalityTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ModalityTypeID&nbsp;</b></p></td><td valign=top><p><select   name="ModalityTypeID_<%=iExpress%>" ><jsp:include page="../generic/tModalityTypeLIShort.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getModalityTypeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ModalityTypeID&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("ModalityTypeID")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("ModalityTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ModalityTypeID&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tModalityTypeLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getModalityTypeID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ModalityTypeID&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("ModalityTypeID")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("ModalityModelID",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("ModalityModelID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((NIM3_Modality.isExpired("ModalityModelID",expiredDays))&&(NIM3_Modality.isExpiredCheck("ModalityModelID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("ModalityModelID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ModalityModelID&nbsp;</b></p></td><td valign=top><p><select   name="ModalityModelID_<%=iExpress%>" ><jsp:include page="../generic/tMRI_ModelLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getModalityModelID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ModalityModelID&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("ModalityModelID")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("ModalityModelID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ModalityModelID&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tMRI_ModelLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getModalityModelID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ModalityModelID&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("ModalityModelID")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("SoftwareVersion",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("SoftwareVersion")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((NIM3_Modality.isExpired("SoftwareVersion",expiredDays))&&(NIM3_Modality.isExpiredCheck("SoftwareVersion",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="SoftwareVersion_<%=iExpress%>" value="<%=NIM3_Modality.getSoftwareVersion()%>">

            <%
            if ( (NIM3_Modality.isRequired("LastServiceDate",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("LastServiceDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((NIM3_Modality.isExpired("LastServiceDate",expiredDays))&&(NIM3_Modality.isExpiredCheck("LastServiceDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="LastServiceDate_<%=iExpress%>" value="<%=NIM3_Modality.getLastServiceDate()%>">

            <%
            if ( (NIM3_Modality.isRequired("SmallPartSpecialtyCoil",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("SmallPartSpecialtyCoil")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((NIM3_Modality.isExpired("SmallPartSpecialtyCoil",expiredDays))&&(NIM3_Modality.isExpiredCheck("SmallPartSpecialtyCoil",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("SmallPartSpecialtyCoil",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>SmallPartSpecialtyCoil&nbsp;</b></p></td><td valign=top><p><select   name="SmallPartSpecialtyCoil_<%=iExpress%>" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getSmallPartSpecialtyCoil()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SmallPartSpecialtyCoil&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("SmallPartSpecialtyCoil")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("SmallPartSpecialtyCoil",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>SmallPartSpecialtyCoil&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getSmallPartSpecialtyCoil()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SmallPartSpecialtyCoil&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("SmallPartSpecialtyCoil")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("IsACR",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("IsACR")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((NIM3_Modality.isExpired("IsACR",expiredDays))&&(NIM3_Modality.isExpiredCheck("IsACR",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("IsACR",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>IsACR&nbsp;</b></p></td><td valign=top><p><select   name="IsACR_<%=iExpress%>" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getIsACR()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsACR&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("IsACR")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("IsACR",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>IsACR&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getIsACR()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsACR&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("IsACR")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("ACRExpiration",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("ACRExpiration")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((NIM3_Modality.isExpired("ACRExpiration",expiredDays))&&(NIM3_Modality.isExpiredCheck("ACRExpiration",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ACRExpiration_<%=iExpress%>" value="<%=NIM3_Modality.getACRExpiration()%>">

            <%
            if ( (NIM3_Modality.isRequired("IsACRVerified",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("IsACRVerified")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((NIM3_Modality.isExpired("IsACRVerified",expiredDays))&&(NIM3_Modality.isExpiredCheck("IsACRVerified",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("IsACRVerified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>IsACRVerified&nbsp;</b></p></td><td valign=top><p><select   name="IsACRVerified_<%=iExpress%>" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getIsACRVerified()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsACRVerified&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("IsACRVerified")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("IsACRVerified",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>IsACRVerified&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Modality.getIsACRVerified()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsACRVerified&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("IsACRVerified")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Modality.isRequired("ACRVerifiedDate",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("ACRVerifiedDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((NIM3_Modality.isExpired("ACRVerifiedDate",expiredDays))&&(NIM3_Modality.isExpiredCheck("ACRVerifiedDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                     <input type=hidden name="ACRVerifiedDate_<%=iExpress%>" value="<%=NIM3_Modality.getACRVerifiedDate()%>">

            <%
            if ( (NIM3_Modality.isRequired("Comments",UserSecurityGroupID))&&(!NIM3_Modality.isComplete("Comments")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((NIM3_Modality.isExpired("Comments",expiredDays))&&(NIM3_Modality.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Modality.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Comments&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Comments_<%=iExpress%>" value="<%=NIM3_Modality.getComments()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Modality.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Comments&nbsp;</b></p></td><td valign=top><p><%=NIM3_Modality.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Modality&amp;sRefID=<%=NIM3_Modality.getModalityID()%>&amp;sFieldNameDisp=<%=NIM3_Modality.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Modality','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>






            <input type=hidden name=routePageReference value="sParentReturnPage">

        </table>
        </td></tr></table>
        </td></tr>
        <tr><td>&nbsp;</td></tr>
        <%
  }%>

        <%
    }
    %>
        <tr><td>
        <input <%=HTMLFormStyleButton%> onClick="this.disable=true;document.forms[0].nextPage.value='<%=ConfigurationMessages.getExpressLinkRaw("tNIM3_Modality","next",iExpressMode)%>'" type=Submit value="Save & Continue" name=Submit>
        <input type=hidden name=nextPage value="">
        </td></tr> </table>
        </form>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PracticeID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>