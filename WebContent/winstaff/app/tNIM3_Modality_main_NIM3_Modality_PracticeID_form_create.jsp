<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tNIM3_Modality_main_NIM3_Modality_PracticeID_form_create.jsp
    Created on Oct/28/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
        }

//declaration of Enumeration
    bltNIM3_Modality        working_bltNIM3_Modality = new bltNIM3_Modality();
    working_bltNIM3_Modality.setPracticeID(iPracticeID);
    working_bltNIM3_Modality.setUniqueCreateDate(new java.util.Date());
    working_bltNIM3_Modality.setUniqueModifyDate(new java.util.Date());
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltNIM3_Modality.setUniqueModifyComments(""+UserLogonDescription);
    }

            working_bltNIM3_Modality.commitData();
        {
            {
                pageControllerHash.put("iModalityID",working_bltNIM3_Modality.getUniqueID());
                pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
                session.setAttribute("pageControllerHash",pageControllerHash);
                //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
                String targetRedirect = "tNIM3_Modality_form.jsp?nullParam=null"+parameterPassString    ;

                {
                    targetRedirect = "tNIM3_Modality_form.jsp?EDITID="+working_bltNIM3_Modality.getUniqueID()+"&routePageReference=sParentReturnPage&EDIT=edit"+parameterPassString    ;
                }
                response.sendRedirect(targetRedirect);
            }
        }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>





