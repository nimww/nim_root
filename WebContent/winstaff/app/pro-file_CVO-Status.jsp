 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltCVOMaster" %>
<%/*
    filename: out\jsp\tCVOMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>
<%@ include file="../generic/CheckLogin.jsp" %>
<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_CVOID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iCVOID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iCVOID")) 
    {
        iCVOID        =    (Integer)pageControllerHash.get("iCVOID");
        accessValid = true;    
   }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

bltCVOMaster        CVOMaster        =   new  bltCVOMaster (iCVOID);
    pageControllerHash.put("sLocalChildReturnPage","pro-file_Status.jsp");
    pageControllerHash.put("sParentReturnPage","pro-file_Status.jsp");
    pageControllerHash.put("iAdminID",CVOMaster.getMasterAdminID());
    session.setAttribute("pageControllerHash",pageControllerHash);

%>
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p class=title>PRO-FILE CVO Status</p>
      <p class=title>Welcome <%=CurrentUserAccount.getLogonUserName()%>:</p>
      <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
        <tr> 
          <td> 
            <table width="100%" border="1" cellspacing="0" cellpadding="3" bordercolor="#999999">
              <tr class=tdHeaderAlt> 
                <td nowrap>Action</td>
                <td width="90%">Description</td>
              </tr>
              <tr class=tdBase> 
                <td nowrap><a href="CVO-query.jsp">Search Practitioners</a></td>
                <td width="90%">Search entire database of practitioners</td>
              </tr>
              <tr class="tdBaseAlt">
                <td nowrap="nowrap"><a href="tCVOHCOLU_main_HCOMaster_CVOID.jsp">View Groups</a></td>
                <td>Search only those practitioners who have authorized 
                  your Healthcare Organization.</td>
              </tr>
              <tr class="tdBaseAlt">
                <td nowrap="nowrap"><a href="#" onClick="window.open('file:///C|/winstaff/alexian/reports.mdb','QR1','status=yes,scrollbars=yes,resizable=yes,width=200,height=200')">Run Reports </a></td>
                <td>Launch Reports </td>
              </tr>
              <tr class=tdBaseAlt> 
                <td nowrap><a href="QR_Admin_main.jsp">Run Sync </a></td>
                <td width="90%">&nbsp;</td>
              </tr>
<%
if (false)
{
%>
              <tr class=tdBase> 
                <td nowrap><a href="reports_CVOID.jsp">Run Reports</a> </td>
                <td width="90%">Allows reports to be generated</td>
              </tr>
<%
}
%>
            </table>
          </td>
        </tr>
      </table>
      <br>
      <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
        <tr> 
          <td> 
            <table width="100%" border="1" cellspacing="0" cellpadding="2" bordercolor="#CCCCCC">
              <tr> 
                <td class=tdHeader width="30%" valign="top"> 
                  <div align="right">CVO Information:&nbsp;</div>
                </td>
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"> Name:&nbsp;</td>
                      <td><%=CVOMaster.getCVOName()%></td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"> Address:&nbsp;</td>
                      <td><%=CVOMaster.getAddress1()%>&nbsp;<%=CVOMaster.getAddress2()%><br>
                        <%=CVOMaster.getCity()%>&nbsp; 
                        <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=CVOMaster.getStateID()%>" />
                        </jsp:include>
                        &nbsp;<%=CVOMaster.getZIP()%></td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top">Email:&nbsp;</td>
                      <td><a href="mailto:<%=CVOMaster.getContactEmail()%>" ><%=CVOMaster.getContactEmail()%></a></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class=tdHeader width="30%" valign="top"> 
                  <div align="right">User Information:&nbsp;</div>
                </td>
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top">User Logon Name:&nbsp;</td>
                      <td><%=CurrentUserAccount.getLogonUserName()%></td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top">User Access:&nbsp;</td>
                      <td> 
                        <jsp:include page="../generic/tUserAccessTypeLILong_translate.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=CurrentUserAccount.getAccessType()%>" />
                        </jsp:include>
                      </td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top">Contact Email:&nbsp;</td>
                      <td><a href="mailto:<%=CurrentUserAccount.getContactEmail()%>" ><%=CurrentUserAccount.getContactEmail()%></a></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
</table>
<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_CVOID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
