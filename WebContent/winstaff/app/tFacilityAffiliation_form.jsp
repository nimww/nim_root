<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltFacilityAffiliation" %>
<%/*

    filename: out\jsp\tFacilityAffiliation_form.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tFacilityAffiliation")%>



<%
//initial declaration of list class and parentID
    Integer        iAffiliationID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection10", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iAffiliationID")) 
    {
        iAffiliationID        =    (Integer)pageControllerHash.get("iAffiliationID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tFacilityAffiliation_form.jsp");
//initial declaration of list class and parentID

    bltFacilityAffiliation        FacilityAffiliation        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        FacilityAffiliation        =    new    bltFacilityAffiliation(iAffiliationID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        FacilityAffiliation        =    new    bltFacilityAffiliation(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tFacilityAffiliation_form_instructions.jsp" %>
        <form action="tFacilityAffiliation_form_sub.jsp" name="tFacilityAffiliation_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (FacilityAffiliation.isRequired("IsPrimary",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("IsPrimary")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("IsPrimary",expiredDays))&&(FacilityAffiliation.isExpiredCheck("IsPrimary",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("IsPrimary",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Appointment Type&nbsp;</b></p></td><td valign=top><p><select   name="IsPrimary" ><jsp:include page="../generic/tAppointmentTypeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getIsPrimary()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsPrimary&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("IsPrimary")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("IsPrimary",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Appointment Type&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tAppointmentTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getIsPrimary()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsPrimary&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("IsPrimary")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("AppointmentLevel",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("AppointmentLevel")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("AppointmentLevel",expiredDays))&&(FacilityAffiliation.isExpiredCheck("AppointmentLevel",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("AppointmentLevel",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Appointment/status level&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="AppointmentLevel" value="<%=FacilityAffiliation.getAppointmentLevel()%>">&nbsp;<a href="#" onClick="window.open('LI_tAppointmentStatusLI_Search.jsp?OFieldName=AppointmentLevel','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AppointmentLevel&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("AppointmentLevel")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("AppointmentLevel",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Appointment/status level&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getAppointmentLevel()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AppointmentLevel&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("AppointmentLevel")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("StartDate",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("StartDate")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("StartDate",expiredDays))&&(FacilityAffiliation.isExpiredCheck("StartDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((FacilityAffiliation.isWrite("StartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Start Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><input maxlength=20  type=text size="80" name="StartDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(FacilityAffiliation.getStartDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("StartDate")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("StartDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Start Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(FacilityAffiliation.getStartDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StartDate&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("StartDate")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("EndDate",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("EndDate")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("EndDate",expiredDays))&&(FacilityAffiliation.isExpiredCheck("EndDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((FacilityAffiliation.isWrite("EndDate",UserSecurityGroupID)))
            {
                        %>
<tr><td colspan=2>&nbsp;</td></tr>
<tr>
                  <td colspan=2 class=instructions>Please enter the date you terminated 
                    your affiliation. If you are still currently affiliated, please 
                    enter "Current"</td>
                </tr>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>End Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><input maxlength=20  type=text size="80" name="EndDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(FacilityAffiliation.getEndDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("EndDate")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("EndDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>End Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(FacilityAffiliation.getEndDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EndDate&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("EndDate")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("PendingDate",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("PendingDate")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("PendingDate",expiredDays))&&(FacilityAffiliation.isExpiredCheck("PendingDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((FacilityAffiliation.isWrite("PendingDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>If this appointment is pending, 
                      please enter the date you submitted your application&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><input maxlength=20  type=text size="80" name="PendingDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(FacilityAffiliation.getPendingDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PendingDate&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("PendingDate")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("PendingDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>If this appointment is pending, 
                      please enter the date you submitted your application&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(FacilityAffiliation.getPendingDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PendingDate&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("PendingDate")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityName",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityName")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("FacilityName",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Facility Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="FacilityName" value="<%=FacilityAffiliation.getFacilityName()%>">&nbsp;<a href="#" onClick="window.open('LI_tHospitalLI_Search.jsp?OFieldName=FacilityName','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityName&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityName")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Name&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityName&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityName")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityDepartment",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityDepartment")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("FacilityDepartment",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityDepartment",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityDepartment",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Facility Department&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="FacilityDepartment" value="<%=FacilityAffiliation.getFacilityDepartment()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityDepartment&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityDepartment")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityDepartment",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Department&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityDepartment()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityDepartment&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityDepartment")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityAddress1",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("FacilityAddress1",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityAddress1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Facility Address&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="FacilityAddress1" value="<%=FacilityAffiliation.getFacilityAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityAddress1&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityAddress1")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityAddress1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Address&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityAddress1&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityAddress1")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityAddress2",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("FacilityAddress2",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityAddress2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="FacilityAddress2" value="<%=FacilityAffiliation.getFacilityAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityAddress2&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityAddress2")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityAddress2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityAddress2&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityAddress2")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityCity",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityCity")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("FacilityCity",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityCity",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>City, Town, Province&nbsp;</b></p></td><td valign=top><p><input maxlength="30" type=text size="80" name="FacilityCity" value="<%=FacilityAffiliation.getFacilityCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityCity&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityCity")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityCity",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>City, Town, Province&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityCity&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityCity")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityStateID",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("FacilityStateID",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><select   name="FacilityStateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getFacilityStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityStateID&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityStateID")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getFacilityStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityStateID&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityStateID")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("FacilityProvince",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityProvince")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("FacilityProvince",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityProvince",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityProvince",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="FacilityProvince" value="<%=FacilityAffiliation.getFacilityProvince()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityProvince&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityProvince")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityProvince",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityProvince&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityProvince")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityZIP",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("FacilityZIP",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="FacilityZIP" value="<%=FacilityAffiliation.getFacilityZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityZIP&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityZIP")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityZIP&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityZIP")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityCountryID",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityCountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("FacilityCountryID",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityCountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityCountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><select   name="FacilityCountryID" ><jsp:include page="../generic/tCountryLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getFacilityCountryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityCountryID&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityCountryID")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityCountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getFacilityCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityCountryID&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityCountryID")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("FacilityPhone",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("FacilityPhone",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityPhone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Facility Phone Number (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="FacilityPhone" value="<%=FacilityAffiliation.getFacilityPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityPhone&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityPhone")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityPhone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Phone Number (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityPhone&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityPhone")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("FacilityFax",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("FacilityFax")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("FacilityFax",expiredDays))&&(FacilityAffiliation.isExpiredCheck("FacilityFax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("FacilityFax",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Facility Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="FacilityFax" value="<%=FacilityAffiliation.getFacilityFax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityFax&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityFax")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("FacilityFax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Facility Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getFacilityFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FacilityFax&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("FacilityFax")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("ContactName",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("ContactName")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("ContactName",expiredDays))&&(FacilityAffiliation.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("ContactName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ContactName" value="<%=FacilityAffiliation.getContactName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("ContactName")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("ContactName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getContactName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("ContactName")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("ContactEmail",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("ContactEmail",expiredDays))&&(FacilityAffiliation.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><input maxlength="75" type=text size="80" name="ContactEmail" value="<%=FacilityAffiliation.getContactEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("ReasonForLeaving",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("ReasonForLeaving")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("ReasonForLeaving",expiredDays))&&(FacilityAffiliation.isExpiredCheck("ReasonForLeaving",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("ReasonForLeaving",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=FacilityAffiliation.getEnglish("ReasonForLeaving")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="ReasonForLeaving" cols="40" maxlength=200><%=FacilityAffiliation.getReasonForLeaving()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReasonForLeaving&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("ReasonForLeaving")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("ReasonForLeaving",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=FacilityAffiliation.getEnglish("ReasonForLeaving")%>&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getReasonForLeaving()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReasonForLeaving&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("ReasonForLeaving")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("AdmissionPriviledges",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("AdmissionPriviledges")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("AdmissionPriviledges",expiredDays))&&(FacilityAffiliation.isExpiredCheck("AdmissionPriviledges",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("AdmissionPriviledges",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have admission privileges?&nbsp;</b></p></td><td valign=top><p><select   name="AdmissionPriviledges" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getAdmissionPriviledges()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AdmissionPriviledges&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("AdmissionPriviledges")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("AdmissionPriviledges",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have admission privileges?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getAdmissionPriviledges()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AdmissionPriviledges&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("AdmissionPriviledges")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("AdmissionArrangements",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("AdmissionArrangements")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("AdmissionArrangements",expiredDays))&&(FacilityAffiliation.isExpiredCheck("AdmissionArrangements",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("AdmissionArrangements",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>If no, Please explain your admission arrangements&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="AdmissionArrangements" value="<%=FacilityAffiliation.getAdmissionArrangements()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AdmissionArrangements&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("AdmissionArrangements")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("AdmissionArrangements",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>If no, Please explain your admission arrangements&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getAdmissionArrangements()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AdmissionArrangements&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("AdmissionArrangements")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("UnrestrictedAdmission",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("UnrestrictedAdmission")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("UnrestrictedAdmission",expiredDays))&&(FacilityAffiliation.isExpiredCheck("UnrestrictedAdmission",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("UnrestrictedAdmission",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Do you have unrestricted admission 
                      privileges&nbsp;</b></p>
                  </td><td valign=top><p><select   name="UnrestrictedAdmission" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getUnrestrictedAdmission()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=UnrestrictedAdmission&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("UnrestrictedAdmission")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("UnrestrictedAdmission",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Do you have unrestricted admission 
                      privileges&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getUnrestrictedAdmission()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=UnrestrictedAdmission&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("UnrestrictedAdmission")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("TempPriviledges",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("TempPriviledges")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("TempPriviledges",expiredDays))&&(FacilityAffiliation.isExpiredCheck("TempPriviledges",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("TempPriviledges",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have temporary privileges?&nbsp;</b></p></td><td valign=top><p><select   name="TempPriviledges" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getTempPriviledges()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TempPriviledges&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("TempPriviledges")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("TempPriviledges",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you have temporary privileges?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getTempPriviledges()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TempPriviledges&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("TempPriviledges")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("InpatientCare",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("InpatientCare")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("InpatientCare",expiredDays))&&(FacilityAffiliation.isExpiredCheck("InpatientCare",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("InpatientCare",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you provide inpatient care?&nbsp;</b></p></td><td valign=top><p><select   name="InpatientCare" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getInpatientCare()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InpatientCare&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("InpatientCare")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("InpatientCare",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you provide inpatient care?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=FacilityAffiliation.getInpatientCare()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InpatientCare&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("InpatientCare")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (FacilityAffiliation.isRequired("PercentAdmissions",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("PercentAdmissions")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("PercentAdmissions",expiredDays))&&(FacilityAffiliation.isExpiredCheck("PercentAdmissions",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("PercentAdmissions",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Percent Admissions&nbsp;</b></p></td><td valign=top><p><input maxlength="15" type=text size="80" name="PercentAdmissions" value="<%=FacilityAffiliation.getPercentAdmissions()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PercentAdmissions&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("PercentAdmissions")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("PercentAdmissions",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Percent Admissions&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getPercentAdmissions()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PercentAdmissions&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("PercentAdmissions")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (FacilityAffiliation.isRequired("Comments",UserSecurityGroupID))&&(!FacilityAffiliation.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((FacilityAffiliation.isExpired("Comments",expiredDays))&&(FacilityAffiliation.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((FacilityAffiliation.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=FacilityAffiliation.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=FacilityAffiliation.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("Comments")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((FacilityAffiliation.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=FacilityAffiliation.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=FacilityAffiliation.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tFacilityAffiliation&amp;sRefID=<%=FacilityAffiliation.getAffiliationID()%>&amp;sFieldNameDisp=<%=FacilityAffiliation.getEnglish("Comments")%>&amp;sTableNameDisp=tFacilityAffiliation','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tFacilityAffiliation")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tFacilityAffiliation")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
