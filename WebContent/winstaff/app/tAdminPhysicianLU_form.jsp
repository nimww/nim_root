<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltAdminPhysicianLU" %>
<%/*

    filename: out\jsp\tAdminPhysicianLU_form.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_AdminID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tAdminPhysicianLU")%>



<%
//initial declaration of list class and parentID
    Integer        iLookupID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("Admin1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iLookupID")) 
    {
        iLookupID        =    (Integer)pageControllerHash.get("iLookupID");
        accessValid = true;
        if (pageControllerHash.containsKey("sKeyMasterReference"))
        {
            sKeyMasterReference = (String)pageControllerHash.get("sKeyMasterReference");
        }
        if (sKeyMasterReference!=null)
        {
            if (sKeyMasterReference.equalsIgnoreCase("p"))
            {
                if (!pageControllerHash.containsKey("iAdminID")) 
                {
                    accessValid=false;
                }
            }
            else if (sKeyMasterReference.equalsIgnoreCase("s"))
            {
                if (!pageControllerHash.containsKey("iPhysicianID")) 
                {
                    accessValid=false;
                }
            }
            else
            {
                accessValid=false;
            }
        }
        else 
        {
            accessValid=false;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tAdminPhysicianLU_form.jsp");
//initial declaration of list class and parentID

    bltAdminPhysicianLU        AdminPhysicianLU        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        AdminPhysicianLU        =    new    bltAdminPhysicianLU(iLookupID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        AdminPhysicianLU        =    new    bltAdminPhysicianLU(UserSecurityGroupID, true);
    }

//fields
        %>
        <%@ include file="tAdminPhysicianLU_form_instructions.jsp" %>
        <form action="tAdminPhysicianLU_form_sub.jsp" name="tAdminPhysicianLU_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>
            <%
            if (sKeyMasterReference.equalsIgnoreCase("p"))
            {
            %>
            <p class=tdBase><b>AdminID</b> <%=AdminPhysicianLU.getAdminID()%></p>
            <p class=tdBaseAlt><b>PhysicianID</b><input   type=text name="PhysicianID" value="<%=AdminPhysicianLU.getPhysicianID()%>"></p>
            <%
            }
            else if (sKeyMasterReference.equalsIgnoreCase("s"))
            {
            %>
            <p class=tdBase><b>PhysicianID</b> <%=AdminPhysicianLU.getPhysicianID()%></p>
            <p class=tdBaseAlt><b>AdminID</b><input   type=text name="AdminID" value="<%=AdminPhysicianLU.getAdminID()%>"></p>
            <%
            }
            %>
            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (AdminPhysicianLU.isRequired("Comments",UserSecurityGroupID))&&(!AdminPhysicianLU.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminPhysicianLU.isExpired("Comments",expiredDays))&&(AdminPhysicianLU.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((AdminPhysicianLU.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=AdminPhysicianLU.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=AdminPhysicianLU.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tAdminPhysicianLU&amp;sRefID=<%=AdminPhysicianLU.getLookupID()%>&amp;sFieldNameDisp=<%=AdminPhysicianLU.getEnglish("Comments")%>&amp;sTableNameDisp=tAdminPhysicianLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((AdminPhysicianLU.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=AdminPhysicianLU.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=AdminPhysicianLU.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tAdminPhysicianLU&amp;sRefID=<%=AdminPhysicianLU.getLookupID()%>&amp;sFieldNameDisp=<%=AdminPhysicianLU.getEnglish("Comments")%>&amp;sTableNameDisp=tAdminPhysicianLU','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tAdminPhysicianLU")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tAdminPhysicianLU")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_AdminID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
