<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltProfessionalEducation,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tProfessionalEducation_form_sub.jsp
    Created on Sep/11/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iProfessionalEducationID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection6", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iProfessionalEducationID")) 
    {
        iProfessionalEducationID        =    (Integer)pageControllerHash.get("iProfessionalEducationID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltProfessionalEducation        ProfessionalEducation        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        ProfessionalEducation        =    new    bltProfessionalEducation(iProfessionalEducationID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        ProfessionalEducation        =    new    bltProfessionalEducation(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !ProfessionalEducation.getUniqueCreateDate().equals(testObj)  )
    {
         ProfessionalEducation.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !ProfessionalEducation.getUniqueModifyDate().equals(testObj)  )
    {
         ProfessionalEducation.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !ProfessionalEducation.getUniqueModifyComments().equals(testObj)  )
    {
         ProfessionalEducation.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID")) ;
    if ( !ProfessionalEducation.getPhysicianID().equals(testObj)  )
    {
         ProfessionalEducation.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DegreeID")) ;
    if ( !ProfessionalEducation.getDegreeID().equals(testObj)  )
    {
         ProfessionalEducation.setDegreeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation DegreeID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Other")) ;
    if ( !ProfessionalEducation.getOther().equals(testObj)  )
    {
         ProfessionalEducation.setOther( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation Other not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("StartDate"))) ;
    if ( !ProfessionalEducation.getStartDate().equals(testObj)  )
    {
         ProfessionalEducation.setStartDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation StartDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateOfGraduation"))) ;
    if ( !ProfessionalEducation.getDateOfGraduation().equals(testObj)  )
    {
         ProfessionalEducation.setDateOfGraduation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation DateOfGraduation not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("EndDate"))) ;
    if ( !ProfessionalEducation.getEndDate().equals(testObj)  )
    {
         ProfessionalEducation.setEndDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation EndDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Focus")) ;
    if ( !ProfessionalEducation.getFocus().equals(testObj)  )
    {
         ProfessionalEducation.setFocus( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation Focus not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolName")) ;
    if ( !ProfessionalEducation.getSchoolName().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolAddress1")) ;
    if ( !ProfessionalEducation.getSchoolAddress1().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolAddress2")) ;
    if ( !ProfessionalEducation.getSchoolAddress2().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolCity")) ;
    if ( !ProfessionalEducation.getSchoolCity().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SchoolStateID")) ;
    if ( !ProfessionalEducation.getSchoolStateID().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolProvince")) ;
    if ( !ProfessionalEducation.getSchoolProvince().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolProvince not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolZIP")) ;
    if ( !ProfessionalEducation.getSchoolZIP().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SchoolCountryID")) ;
    if ( !ProfessionalEducation.getSchoolCountryID().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolCountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolPhone")) ;
    if ( !ProfessionalEducation.getSchoolPhone().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolFax")) ;
    if ( !ProfessionalEducation.getSchoolFax().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName")) ;
    if ( !ProfessionalEducation.getContactName().equals(testObj)  )
    {
         ProfessionalEducation.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !ProfessionalEducation.getContactEmail().equals(testObj)  )
    {
         ProfessionalEducation.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !ProfessionalEducation.getComments().equals(testObj)  )
    {
         ProfessionalEducation.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tProfessionalEducation","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	ProfessionalEducation.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    ProfessionalEducation.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        ProfessionalEducation.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
