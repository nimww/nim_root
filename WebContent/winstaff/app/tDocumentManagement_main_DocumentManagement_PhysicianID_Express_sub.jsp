<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltDocumentManagement,com.winstaff.bltDocumentManagement_List,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: tDocumentManagement_main_DocumentManagement_PhysicianID.jsp
    Created on Nov/12/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

    <table cellpadding=0 cellspacing=0 border=0 width=800 >
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("DocumentManagement1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }

  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tDocumentManagement_main_DocumentManagement_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iDocumentID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltDocumentManagement_List        bltDocumentManagement_List        =    new    bltDocumentManagement_List(iPhysicianID);

//declaration of Enumeration
    bltDocumentManagement        DocumentManagement;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltDocumentManagement_List.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tDocumentManagement"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        DocumentManagement  = (bltDocumentManagement) leCurrentElement.getObject();
      }
      else
      {
        DocumentManagement  = new bltDocumentManagement();
        DocumentManagement.setPhysicianID(iPhysicianID);
        isNewRecord= true;
      }
        DocumentManagement.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !DocumentManagement.getUniqueCreateDate().equals(testObj)  )
    {
         DocumentManagement.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !DocumentManagement.getUniqueModifyDate().equals(testObj)  )
    {
         DocumentManagement.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !DocumentManagement.getUniqueModifyComments().equals(testObj)  )
    {
         DocumentManagement.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID_"+iExpress)) ;
    if ( !DocumentManagement.getPhysicianID().equals(testObj)  )
    {
         DocumentManagement.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DocumentTypeID_"+iExpress)) ;
    if ( !DocumentManagement.getDocumentTypeID().equals(testObj)  )
    {
         DocumentManagement.setDocumentTypeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement DocumentTypeID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DocumentName_"+iExpress)) ;
    if ( !DocumentManagement.getDocumentName().equals(testObj)  )
    {
         DocumentManagement.setDocumentName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement DocumentName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DocumentFileName_"+iExpress)) ;
    if ( !DocumentManagement.getDocumentFileName().equals(testObj)  )
    {
         DocumentManagement.setDocumentFileName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement DocumentFileName not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateReceived_"+iExpress))) ;
    if ( !DocumentManagement.getDateReceived().equals(testObj)  )
    {
         DocumentManagement.setDateReceived( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement DateReceived not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateOfExpiration_"+iExpress))) ;
    if ( !DocumentManagement.getDateOfExpiration().equals(testObj)  )
    {
         DocumentManagement.setDateOfExpiration( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement DateOfExpiration not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateAlertSent_"+iExpress))) ;
    if ( !DocumentManagement.getDateAlertSent().equals(testObj)  )
    {
         DocumentManagement.setDateAlertSent( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement DateAlertSent not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Archived_"+iExpress)) ;
    if ( !DocumentManagement.getArchived().equals(testObj)  )
    {
         DocumentManagement.setArchived( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement Archived not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !DocumentManagement.getComments().equals(testObj)  )
    {
         DocumentManagement.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("DocumentManagement Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tDocumentManagement","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	DocumentManagement.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    DocumentManagement.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
	        DocumentManagement.commitData();
	        if (!DocumentManagement.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }
    }
}       }
    }//while

if (errorRouteMeTotal)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else 
{
String nextPage = request.getParameter("nextPage");
if (nextPage==null||nextPage.equalsIgnoreCase(""))
{
	nextPage = ConfigurationMessages.getExpressLinkRaw("tDocumentManagement","next",iExpressMode);
}
if (!isAllComplete)
{
	%>
	<script language=Javascript>
	      if ( confirm("<%=ConfigurationMessages.getMessage("ConfirmReqFieldsReturn")%>") )
	      {
	          document.location="<%=ConfigurationMessages.getDataCategoryLink("tDocumentManagement","Express")%>";
	      }
	      else 
	      {
	          document.location="<%=nextPage%>";
	      }
	</script>
  <%
}
else
{
	%>
	<script language=Javascript>
	      document.location="<%=nextPage%>";
	</script>
  <%
}
}
       
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
