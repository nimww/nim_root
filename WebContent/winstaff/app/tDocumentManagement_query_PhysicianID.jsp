<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.DataControlUtils,com.winstaff.SecurityCheck,com.winstaff.ConfigurationMessages,com.winstaff.bltYesNoLI,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils,com.winstaff.bltDocumentManagement,com.winstaff.bltDocumentManagement_List,com.winstaff.searchDB2,com.winstaff.bltDocumentTypeLI" %>
<%/*
    filename: tDocumentManagement_query_PhysicianID.jsp
    Created on Apr/23/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%>>
    <tr><td width=10>&nbsp;</td>
	<td>
 <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tDocumentManagement")%><br>
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("DocumentManagement1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tDocumentManagement_query_PhysicianID.jsp");
    pageControllerHash.remove("iDocumentID");
    session.setAttribute("pageControllerHash",pageControllerHash);

%>


<table width="<%=MasterTableWidth%>" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
<%
try
{

String myDocumentID = "";
String myDocumentTypeID = "0";
String myDocumentName = "";
String myArchived = "0";
String myDateOfExpiration = "1800-01-01";
String orderBy = "firstname";
int startID = 0;
int maxResults = 50;

boolean firstDisplay = false;

try
{
maxResults = Integer.parseInt(request.getParameter("maxResults"));
startID = Integer.parseInt(request.getParameter("startID"));
myDocumentID = request.getParameter("DocumentID");
myDocumentTypeID = request.getParameter("DocumentTypeID");
myDocumentName = request.getParameter("DocumentName");
myArchived = request.getParameter("Archived");
myDateOfExpiration = request.getParameter("DateOfExpiration");
orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myDocumentID = "";
myDocumentTypeID = "0";
myDocumentName = "";
myArchived = "0";
myDateOfExpiration = "1800-01-01";
orderBy = "DocumentID";
}
if (orderBy == null)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myDocumentID = "";
myDocumentTypeID = "0";
myDocumentName = "";
myArchived = "0";
myDateOfExpiration = "1800-01-01";
orderBy = "DocumentID";
}


if (firstDisplay)
{
%>

<table cellpadding="2" cellspacing="0" border="1" bordercolor="#666666">
        <tr>
          <td width=70% class=instructions> 
                  <p>You can search below for any documents that are already attached 
                    to your account. Documents that have already been created, 
                    but not yet fax and processed will also show up.</p>
                  <p><b><u>Print Cover</u></b> - For documents that have not yet been 
                    faxed in and processed, the only option you have is &quot;Print 
                    Cover&quot; which will print out just the cover sheet that 
                    must be attached to each individual document before it can 
                    be faxed.</p>
                  <p><b><u>Print</u></b> - Documents that have been faxed in and 
                    processed can now be viewed and printed by clicking on the 
                    &quot;Print&quot; action. This will open the document using 
                    the Adobe Acrobat plug-in. Once open, you can view/print/save 
                    the document by clicking on the appropriate button or menu 
                    item within Adobe Acrobat.</p>
            </td>
          <td valign="top">
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tDocumentManagement_main_DocumentManagement_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a><br>
            <span class=instructions>If you would like to attach a document that 
            you do not see listed here, then click create. Each document must 
            have its own cover sheet attacked to it before you fax it in to us.</span> 
            <%}%>
          </td>
</tr>
</table>
<br>

<table width = <%=MasterTableWidth%> cellpadding=2 cellspacing=2>
<tr>
<td width=10>&nbsp;</td>
<td class=tdBase><p>
Please enter your search criteria here:</p>
</td>
</tr>
<tr>
<td width=10>&nbsp;</td>

<td>
<form name="form1" method="get" action="tDocumentManagement_query_PhysicianID.jsp">
  <table width="400" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#003333">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
        <tr>
                              <td class=tdHeaderAlt> Document ID </td>
         <td>
         <input type=text name="DocumentID">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Type
         </td>
         <td>
              <select name="DocumentTypeID">
                <jsp:include page="../generic/tDocumentTypeLILong.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="0" />
                </jsp:include>
              </select>
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Name
         </td>
         <td>
         <input type=text name="DocumentName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Expiration
         </td>
         <td>
         <input type=text name="DateOfExpiration">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Archived
         </td>
         <td>
              <select name="Archived">
                <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="2" />
                </jsp:include>
              </select>
         </td>
        </tr>

            <tr>
            <td class=tdHeaderAlt><p>Sort by:</p></td>
            <td> 
              <select name=orderBy>
                                  <option value="DocumentID">Document ID</option>
                <option value="DocumentTypeID">Type</option>
                <option value="DocumentName">Name</option>
                <option value="DateOfExpiration">Expiration</option>
              </select>
            </td>
          </tr>
          <tr class=tdHeaderAlt> 
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td> 
              <input type=hidden name="startID" value=0>
              <input type=hidden name="maxResults" value=50>
              <input type="submit" name="Submit2" value="Submit">
            </td>
            <td>&nbsp;</td>
          </tr>
        </table>
</td>
    </tr>
  </table>
  <p>&nbsp;</p>
  </form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p> 
  <%
}
else
{

%>
                  <table cellpadding="2" cellspacing="0" border="1" bordercolor="#666666">
                    <tr valign="top"> 
                      <td width=50% class=instructions> 
                        <p>You can search below for any documents that are already 
                          attached to your account. Documents that have already 
                          been created, but not yet fax and processed will also 
                          show up.</p>
            </td>
                      <td> 
                        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
                        <a class=linkBase href = "tDocumentManagement_main_DocumentManagement_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a><br>
                        <span class=instructions>If you would like to attach a 
                        document that you do not see listed here, then click create. 
                        </span> 
                        <%}%>
                      </td>
</tr>
</table>



<%



String myWhere = "where (physicianID="+iPhysicianID + " ";
boolean theFirst = false;

try
{
if (!myDocumentID.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "DocumentID LIKE '%" + myDocumentID +"%'";
theFirst = false;
}
if (!myDocumentTypeID.equalsIgnoreCase("0"))
{
if (!myDocumentTypeID.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "DocumentTypeID = " + myDocumentTypeID +"";
theFirst = false;
}
}
if (!myDocumentName.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "DocumentName LIKE '%" + DataControlUtils.fixApostrophe(myDocumentName) +"%'";
theFirst = false;
}
if (!myArchived.equalsIgnoreCase("0"))
{
if (!myArchived.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "Archived = " + myArchived +"";
theFirst = false;
}
}
if (!myDateOfExpiration.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "DateOfExpiration LIKE '%" + myDateOfExpiration +"%'";
theFirst = false;
}
myWhere += ")";

//System.out.println(myWhere);
if (theFirst||myWhere.equalsIgnoreCase(")"))
{
myWhere = "";
}


}
catch(Exception e)
{
out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
myRS = mySS.executeStatement("select * from tDocumentManagement " + myWhere + " order by " + orderBy);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
cnt++;
if (cnt>=startID&&cnt<=startID+maxResults)
{
cnt2++;









String myClass = "tdBase";
if (cnt2%2==0)
{
myClass = "tdBaseAlt";
}


myMainTable +="<tr class="+myClass+"><td  nowrap>";

bltDocumentManagement dm = new bltDocumentManagement(new Integer(myRS.getString("DocumentID")));

boolean isCompany1 = false;
    if (pageControllerHash.containsKey("iCompanyID")) 
    {
        Integer iCompanyID        =    (Integer)pageControllerHash.get("iCompanyID");
        if (iCompanyID.intValue()==1);
	{
	        isCompany1 = true;
	}
    }

if (isCompany1)
{
	myMainTable+=	"<a class=linkBase target=\"_blank\" href=\"GSN-DocumentManagement_form.jsp?iDocumentID="+dm.getDocumentID()+"\">GSN EDIT</a><br>"; 
}

if (dm.isComplete("DocumentFileName"))
{
	myMainTable+=	"<a class=linkBase  href = \"#\" onClick=\"MM_openBrWindow('tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=print&amp;EDITID="+dm.getDocumentID()+"','DocPDF','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=500')\">Print</a>"; 
}
else
{
	myMainTable+=	"Not-Processed"; 
}
	myMainTable+=	"<br><a class=linkBase  href = \"#\" onClick=\"MM_openBrWindow('tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=faxCover&amp;EDITID="+dm.getDocumentID()+"','DocPDF','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=500')\">Print Cover</a>"; 
	myMainTable+=	"<br><a class=linkBase  href =\"tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=del&amp;EDITID="+dm.getDocumentID()+"\" >Remove</a>"; 
myMainTable +="</td>";
myMainTable +="<td valign=top>";
myMainTable +=myRS.getString("DocumentID")+"";
myMainTable +="</td>";
myMainTable +="<td valign=top>";
myMainTable +=(new bltDocumentTypeLI (new Integer(myRS.getString("DocumentTypeID")) ).getDocumentTypeLong()+"");
myMainTable +="</td>";
myMainTable +="<td valign=top>";
myMainTable +=myRS.getString("DocumentName")+"";
myMainTable +="</td>";
myMainTable +="<td valign=top>";
myMainTable +=(new bltYesNoLI (new Integer(myRS.getString("Archived")) ).getYNLong()+"");
myMainTable +="</td>";
myMainTable +="<td nowrap valign=top>";
myMainTable +="Exp: <b>" + PLCUtils.getDisplayDate(dbdf.parse(myRS.getString("DateOfExpiration")),false)+"</b>";
if (dm.isComplete("DocumentFileName"))
{
	myMainTable +="<br>Rec: <b>" +PLCUtils.getDisplayDate(dbdf.parse(myRS.getString("UniqueModifyDate")),false)+"</b>";
}
else
{
	myMainTable +="<br>Rec: Pending";
}
myMainTable +="</td>";
myMainTable +="</tr>";
}
   }
mySS.closeAll();
endCount = cnt;



if (startID>=endCount)
{
startID = endCount-maxResults;

}

if (maxResults<=0)
{
maxResults=10;
}


if (startID<=0)
{
startID=0;
}




%>
  <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</p>
<table width=100% border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
<tr>
<td width=10>&nbsp;</td>
<td>
<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
<tr>
<td>

<form name="selfForm" method="get" action="tDocumentManagement_query_PhysicianID.jsp">
  <table border=0 cellspacing=1 width='100%' cellpadding=2>
    <tr > 
      <td colspan=2 class=tdHeader><p>Results:</p></td>
      <td colspan=1 class=tdBase align=right>
        <%if (startID>0)
{%><a href="tDocumentManagement_query_PhysicianID.jsp?startID=<%=(startID-maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&DocumentID=<%=myDocumentID%>&DocumentTypeID=<%=myDocumentTypeID%>&DocumentName=<%=myDocumentName%>&Archived=<%=myArchived%>&DateOfExpiration=<%=myDateOfExpiration%>"><< previous <%=maxResults%></a> 
          <%
}
else
{
%><p>&nbsp;</p><%
}
%></td>
      <td colspan=1 class=tdBase> 
        <%
if ((startID+maxResults)<endCount)
{
%>
<a href="tDocumentManagement_query_PhysicianID.jsp?startID=<%=(startID+maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&DocumentID=<%=myDocumentID%>&DocumentTypeID=<%=myDocumentTypeID%>&DocumentName=<%=myDocumentName%>&Archived=<%=myArchived%>&DateOfExpiration=<%=myDateOfExpiration%>"> next >> <%=maxResults%></a>         
        <%
}
else
{
%><p>&nbsp;</p><%
}


}
catch(Exception e)
{
out.println("PrevNext:"+e);
}




try{

%>
      </td>
      <td colspan=1 class=tdHeader nowrap> 
<input type=hidden name=maxResults value=<%=maxResults%> >
<input type=hidden name=startID value=0 ><p>Sort:
<select name=orderBy>
      <option value=DocumentID <%if (orderBy.equalsIgnoreCase("DocumentID")){out.println(" selected");}%> >Doc ID</option>
      <option value=DocumentTypeID <%if (orderBy.equalsIgnoreCase("DocumentTypeID")){out.println(" selected");}%> >Type</option>
      <option value=DocumentName <%if (orderBy.equalsIgnoreCase("DocumentName")){out.println(" selected");}%> >Name</option>
      <option value=Archived <%if (orderBy.equalsIgnoreCase("Archived")){out.println(" selected");}%> >Archived</option>
      <option value=DateOfExpiration <%if (orderBy.equalsIgnoreCase("DateOfExpiration")){out.println(" selected");}%> >Expiration</option>
</select></p>
      </td> 
      <td class=tdHeader colspan=1> 
        <p  align="center"> 
          <input type="Submit" name="Submit" value="Submit" align="middle">
        </p>
      </td>
    </tr>
    <tr class=tdHeader> 
      <td  colspan=1 width=50>&nbsp; 

      </td>
      <td colspan=1> 
        <input type="text" name="DocumentID" value="<%=myDocumentID%>" size="10">
      </td>
      <td colspan=1> 
              <select name="DocumentTypeID">
                <jsp:include page="../generic/tDocumentTypeLILong.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myDocumentTypeID%>" />
                </jsp:include>
              </select>
      </td>
      <td colspan=1> 
        <input type="text" name="DocumentName" value="<%=myDocumentName%>" size="10">
      </td>
      <td colspan=1> 
              <select name="Archived">
                <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myArchived%>" />
                </jsp:include>
              </select>
      </td>
      <td colspan=1> 
        <input type="text" name="DateOfExpiration" value="<%=myDateOfExpiration%>" size="10">
      </td>
    </tr>
    <tr class=tdHeader> 
    <td>#</td>
                                  <td colspan=1> Document ID </td>
      <td colspan=1> 
        Type
      </td>
      <td colspan=1> 
        Name
      </td>
      <td colspan=1> 
        Archived
      </td>
                                  <td colspan=1> Dates</td>
    </tr>
    <%=myMainTable%>



</table> 


<%

}
catch(Exception e)
{
out.println("Display:"+e);
}


































}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>



  </table>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </td>
  </tr>
</table>

    <%
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
