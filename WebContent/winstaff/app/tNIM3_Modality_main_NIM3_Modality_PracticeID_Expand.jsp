<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.*, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltNIM3_Modality, com.winstaff.bltNIM3_Modality_List" %>
<%/*
    filename: tNIM3_Modality_main_NIM3_Modality_PracticeID.jsp
    Created on Oct/28/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_1/style.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM3_Modality")%>

    <br><a href="tNIM3_Modality_main_NIM3_Modality_PracticeID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_Modality_main_NIM3_Modality_PracticeID_Expand.jsp");
    pageControllerHash.remove("iModalityID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltNIM3_Modality_List        bltNIM3_Modality_List        =    new    bltNIM3_Modality_List(iPracticeID);

//declaration of Enumeration
    bltNIM3_Modality        working_bltNIM3_Modality;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Modality_List.elements();
    %>
        <%@ include file="tNIM3_Modality_main_NIM3_Modality_PracticeID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <input type=button onClick = "this.disabled=true;document.location ='tNIM3_Modality_main_NIM3_Modality_PracticeID_form_create.jsp?EDIT=new&KM=p'" value="Create">
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_Modality  = (bltNIM3_Modality) leCurrentElement.getObject();
        working_bltNIM3_Modality.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltNIM3_Modality.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltNIM3_Modality.getModalityID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Modality.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Modality.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltNIM3_Modality.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            <td width="50%" bgColor=#ffffff> 
        <input type=button onClick = "this.disabled=true;document.location ='tNIM3_Modality_main_NIM3_Modality_PracticeID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltNIM3_Modality.getModalityID()%>&KM=p';" value="Edit">


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
       <br> <input type=button onClick = "confirmDelete2('','tNIM3_Modality_main_NIM3_Modality_PracticeID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltNIM3_Modality.getModalityID()%>&KM=p',this)" value="Delete">

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltNIM3_Modality.isExpired("ModalityTypeID",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("ModalityTypeID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltNIM3_Modality.isRequired("ModalityTypeID"))&&(!working_bltNIM3_Modality.isComplete("ModalityTypeID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ModalityTypeID:&nbsp;</b><jsp:include page="../generic/tModalityTypeLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Modality.getModalityTypeID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltNIM3_Modality.isExpired("ModalityModelID",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("ModalityModelID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltNIM3_Modality.isRequired("ModalityModelID"))&&(!working_bltNIM3_Modality.isComplete("ModalityModelID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ModalityModelID:&nbsp;</b><jsp:include page="../generic/tMRI_ModelLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Modality.getModalityModelID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltNIM3_Modality.isExpired("SoftwareVersion",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("SoftwareVersion"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltNIM3_Modality.isRequired("SoftwareVersion"))&&(!working_bltNIM3_Modality.isComplete("SoftwareVersion")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>SoftwareVersion:&nbsp;</b><%=working_bltNIM3_Modality.getSoftwareVersion()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltNIM3_Modality.isExpired("LastServiceDate",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("LastServiceDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltNIM3_Modality.isRequired("LastServiceDate"))&&(!working_bltNIM3_Modality.isComplete("LastServiceDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>LastServiceDate:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Modality.getLastServiceDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltNIM3_Modality.isExpired("SmallPartSpecialtyCoil",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("SmallPartSpecialtyCoil"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltNIM3_Modality.isRequired("SmallPartSpecialtyCoil"))&&(!working_bltNIM3_Modality.isComplete("SmallPartSpecialtyCoil")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>SmallPartSpecialtyCoil:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Modality.getSmallPartSpecialtyCoil()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltNIM3_Modality.isExpired("IsACR",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("IsACR"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltNIM3_Modality.isRequired("IsACR"))&&(!working_bltNIM3_Modality.isComplete("IsACR")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>IsACR:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Modality.getIsACR()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltNIM3_Modality.isExpired("ACRExpiration",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("ACRExpiration"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltNIM3_Modality.isRequired("ACRExpiration"))&&(!working_bltNIM3_Modality.isComplete("ACRExpiration")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>ACRExpiration:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Modality.getACRExpiration())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltNIM3_Modality.isExpired("IsACRVerified",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("IsACRVerified"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltNIM3_Modality.isRequired("IsACRVerified"))&&(!working_bltNIM3_Modality.isComplete("IsACRVerified")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>IsACRVerified:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Modality.getIsACRVerified()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltNIM3_Modality.isExpired("ACRVerifiedDate",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("ACRVerifiedDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltNIM3_Modality.isRequired("ACRVerifiedDate"))&&(!working_bltNIM3_Modality.isComplete("ACRVerifiedDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>ACRVerifiedDate:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Modality.getACRVerifiedDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltNIM3_Modality.isExpired("Comments",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltNIM3_Modality.isRequired("Comments"))&&(!working_bltNIM3_Modality.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Comments:&nbsp;</b><%=working_bltNIM3_Modality.getComments()%></p>

        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PracticeID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>