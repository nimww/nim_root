<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils,com.winstaff.bltAdminMaster" %>
<%/*
    filename: out\jsp\tAdminMaster_form_sub.jsp
    Created on Apr/23/2002
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltAdminMaster        AdminMaster        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        AdminMaster        =    new    bltAdminMaster(iAdminID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        AdminMaster        =    new    bltAdminMaster();
    }

String testChangeID = "0";

try
{
    String testObj = new String(request.getParameter("AlertEmail")) ;
    if ( !AdminMaster.getAlertEmail().equals(testObj)  )
    {
         AdminMaster.setAlertEmail( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("AdminMaster AlertEmail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AlertDays")) ;
    if ( !AdminMaster.getAlertDays().equals(testObj)  )
    {
         AdminMaster.setAlertDays( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("AdminMaster AlertDays not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("LastImportDate"))) ;
    if (testObj.after(displayDateSDF1.parse("5/5/1950")))
    {
	    if ( !AdminMaster.getLastImportDate().equals(testObj)  )
	    {
	         AdminMaster.setLastImportDate( testObj,UserSecurityGroupID   );
	         testChangeID = "1";
	    }
    }

}
catch(Exception e)
{
     //out.println("AppointmentStatusLI UniqueModifyDate not set. this is ok-not an error");
}


// If an edit, update information; if new, append information
if (testChangeID.equalsIgnoreCase("1"))
{
	AdminMaster.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    AdminMaster.setUniqueModifyComments(""+UserLogonDescription);
	    AdminMaster.commitData();
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
}
if (nextPage!=null)
{
%>
<script language = javascript>
alert("Your information has been saved");
document.location = "<%=nextPage%>?EDIT=edit";
</script>
<%
//    response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>
Successful save

  <%
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
  %>
