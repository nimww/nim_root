<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltPeerReference, com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tPeerReference_form_sub.jsp
    Created on Feb/27/2004
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iReferenceID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection11", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iReferenceID")) 
    {
        iReferenceID        =    (Integer)pageControllerHash.get("iReferenceID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltPeerReference        PeerReference        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PeerReference        =    new    bltPeerReference(iReferenceID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PeerReference        =    new    bltPeerReference(UserSecurityGroupID,true);
    }

String testChangeID = "0";



try
{
    Integer testObj = new Integer(request.getParameter("Salutation")) ;
    if ( !PeerReference.getSalutation().equals(testObj)  )
    {
         PeerReference.setSalutation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Salutation not set. this is ok-not an error");
}
try
{
    String testObj = new String(request.getParameter("Specialty")) ;
    if ( !PeerReference.getSpecialty().equals(testObj)  )
    {
         PeerReference.setSpecialty( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Specialty not set. this is ok-not an error");
}
try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !PeerReference.getUniqueCreateDate().equals(testObj)  )
    {
         PeerReference.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !PeerReference.getUniqueModifyDate().equals(testObj)  )
    {
         PeerReference.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !PeerReference.getUniqueModifyComments().equals(testObj)  )
    {
         PeerReference.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID")) ;
    if ( !PeerReference.getPhysicianID().equals(testObj)  )
    {
         PeerReference.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Salutation")) ;
    if ( !PeerReference.getSalutation().equals(testObj)  )
    {
         PeerReference.setSalutation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Salutation not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FirstName")) ;
    if ( !PeerReference.getFirstName().equals(testObj)  )
    {
         PeerReference.setFirstName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference FirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LastName")) ;
    if ( !PeerReference.getLastName().equals(testObj)  )
    {
         PeerReference.setLastName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference LastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Specialty")) ;
    if ( !PeerReference.getSpecialty().equals(testObj)  )
    {
         PeerReference.setSpecialty( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Specialty not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address1")) ;
    if ( !PeerReference.getAddress1().equals(testObj)  )
    {
         PeerReference.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2")) ;
    if ( !PeerReference.getAddress2().equals(testObj)  )
    {
         PeerReference.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City")) ;
    if ( !PeerReference.getCity().equals(testObj)  )
    {
         PeerReference.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("StateID")) ;
    if ( !PeerReference.getStateID().equals(testObj)  )
    {
         PeerReference.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference StateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province")) ;
    if ( !PeerReference.getProvince().equals(testObj)  )
    {
         PeerReference.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP")) ;
    if ( !PeerReference.getZIP().equals(testObj)  )
    {
         PeerReference.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID")) ;
    if ( !PeerReference.getCountryID().equals(testObj)  )
    {
         PeerReference.setCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone")) ;
    if ( !PeerReference.getPhone().equals(testObj)  )
    {
         PeerReference.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Fax")) ;
    if ( !PeerReference.getFax().equals(testObj)  )
    {
         PeerReference.setFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Fax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !PeerReference.getContactEmail().equals(testObj)  )
    {
         PeerReference.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("YearsAssociated")) ;
    if ( !PeerReference.getYearsAssociated().equals(testObj)  )
    {
         PeerReference.setYearsAssociated( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference YearsAssociated not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DocuLinkID")) ;
    if ( !PeerReference.getDocuLinkID().equals(testObj)  )
    {
         PeerReference.setDocuLinkID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference DocuLinkID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !PeerReference.getComments().equals(testObj)  )
    {
         PeerReference.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Comments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Title")) ;
    if ( !PeerReference.getTitle().equals(testObj)  )
    {
         PeerReference.setTitle( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference Title not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HospitalAffiliation")) ;
    if ( !PeerReference.getHospitalAffiliation().equals(testObj)  )
    {
         PeerReference.setHospitalAffiliation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference HospitalAffiliation not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("HospitalDepartment")) ;
    if ( !PeerReference.getHospitalDepartment().equals(testObj)  )
    {
         PeerReference.setHospitalDepartment( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PeerReference HospitalDepartment not set. this is ok-not an error");
}


boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tPeerReference","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	PeerReference.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    PeerReference.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        PeerReference.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }	//Doculink Addition
	//Doculink Does Exist, prompt to Modify
	Integer iPhysicianID = PeerReference.getPhysicianID();
     if (!errorRouteMe&&PeerReference.getDocuLinkID().intValue()>0)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement(PeerReference.getDocuLinkID());
            if (myDoc.getDocumentFileName().equalsIgnoreCase("")) 
            {
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tPeerReference";
                myDUtils.setDocumentType(myDType, PeerReference.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
            }
            else
            {
                //archive then create new
                myDoc.setArchived(new Integer("1"));
                myDoc.commitData();
                myDoc = new bltDocumentManagement();
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tPeerReference";
                myDUtils.setDocumentType(myDType, PeerReference.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
                PeerReference.setDocuLinkID(myDoc.getUniqueID());
                PeerReference.commitData();
            }
     }
     else if (!errorRouteMe)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement();
            DocumentManagerUtils myDUtils = new DocumentManagerUtils();
            String myDType = "tPeerReference";
            myDUtils.setDocumentType(myDType, PeerReference.getUniqueID());
            myDoc.setDocumentName(myDUtils.getDocumentName());
            myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
            myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
            myDoc.setArchived(new Integer(2));
            myDoc.commitData();
            PeerReference.setDocuLinkID(myDoc.getUniqueID());
            PeerReference.commitData();
     }

    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
