
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tAdminPhysicianLU_main_LU_AdminID.jsp
    JSP AutoGen on Mar/13/2002
    Type: 1-n main class file
*/%>
                    <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_AdminID_nim.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table cellpadding=0 cellspacing=0 border=0 width=700>
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p><span class=title>Practitioners</span></p>
      <%@ include file="AdminPhysician_query_instructions.jsp" %>
      <a href = "tAdminPhysician_MasterAdd.jsp"&EDIT=edit"><img border=0 src="ui_<%=thePLCID%>/icons/add_AdminID.gif"></a> 
      <p> 
        <%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdf2 = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","QR_AdminPhysicianMaster_query.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

%>
      <table width="700" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width=10>&nbsp;</td>
          <td> 
            <%
try
{

String myPhysicianID = "";
String myLastName = "";
String myFirstName = "";
String mySSN = "";
String myIsAttested = "0";
String myAttestDate = "";
String myDOBm = "";
String myLastModifiedDate = "";
String orderBy = "firstname";
int startID = 0;
int maxResults = 50;

boolean firstDisplay = false;

try
{
maxResults = Integer.parseInt(request.getParameter("maxResults"));
startID = Integer.parseInt(request.getParameter("startID"));
myPhysicianID = request.getParameter("PhysicianID");
myLastName = request.getParameter("LastName");
myFirstName = request.getParameter("FirstName");
mySSN = request.getParameter("SSN");
myIsAttested = request.getParameter("IsAttested");
myAttestDate = request.getParameter("AttestDate");
myDOBm = request.getParameter("DOBm");
myLastModifiedDate = request.getParameter("LastModifiedDate");
orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myPhysicianID = "";
myLastName = "";
myFirstName = "";
mySSN = "";
myIsAttested = "0";
myAttestDate = "";
myDOBm = "";
myLastModifiedDate = "";
orderBy = "PhysicianID";
}
if (orderBy == null)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myPhysicianID = "";
myLastName = "";
myFirstName = "";
mySSN = "";
myIsAttested = "0";
myAttestDate = "";
myDOBm = "";
myLastModifiedDate = "";
orderBy = "PhysicianID";
}

if (orderBy.equalsIgnoreCase("LastModifiedDate")||orderBy.equalsIgnoreCase("AttestDate"))
{
orderBy = orderBy + " DESC";
}


if (firstDisplay)
{
%>
            <table width = 735 cellpadding=2 cellspacing=2>
              <tr> 
                <td width=10>&nbsp;</td>
                <td class=tdBase> 
                  <p> Enter Search Criteria:</p>
                </td>
              </tr>
              <tr> 
                <td width=10>&nbsp;</td>
                <td> 
                  <form name="form1" method="get" action="AdminPhysician_query.jsp">
                    <table width="400" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#003333">
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
                            <tr> 
                              <td class=tdHeaderAlt> ID </td>
                              <td> 
                                <input type=text name="PhysicianID">
                              </td>
                            </tr>
                            <tr> 
                              <td class=tdHeaderAlt> Last Name </td>
                              <td> 
                                <input type=text name="LastName">
                              </td>
                            </tr>
                            <tr> 
                              <td class=tdHeaderAlt> First Name </td>
                              <td> 
                                <input type=text name="FirstName">
                              </td>
                            </tr>
                            <tr> 
                              <td class=tdHeaderAlt> SSN </td>
                              <td> 
                                <input type=text name="SSN">
                              </td>
                            </tr>
                            <tr> 
                              <td class=tdHeaderAlt> Attested </td>
                              <td> 
                                <select name="IsAttested">
                                  <jsp:include page="../generic/tYesNoLIShortSpecial.jsp" flush="true" > 
                                  <jsp:param name="CurrentSelection" value="0" />
                                  </jsp:include>
                                </select>
                              </td>
                            </tr>
                            <tr> 
                              <td class=tdHeaderAlt> Month of Birth: </td>
                              <td> 
                                <select name="DOBm">
                                  <jsp:include page="../generic/tMonthTypeLILong.jsp" flush="true" > 
                                  <jsp:param name="CurrentSelection" value="0" />
                                  </jsp:include>
                                </select>
                              </td>
                            </tr>
                            <tr> 
                              <td class=tdHeaderAlt> Attest Date<br>
                                <a href="#" onClick="MM_openBrWindow('QR-DateQuickPick.jsp?OFieldName=AttestDate','QPDate','status=yes,scrollbars=yes,resizable=yes,width=400,height=400')">Quick 
                                Pick </a></td>
                              <td> 
                                <input type=text value="<%=displayDateSDF1.format(dateUtils.dateChange(new java.util.Date(),-30))%>" name="AttestDate">
                              </td>
                            </tr>
                            <tr> 
                              <td class=tdHeaderAlt> Last Modified <br>
                                <a href="#" onClick="MM_openBrWindow('QR-DateQuickPick.jsp?OFieldName=LastModifiedDate','QPDate','status=yes,scrollbars=yes,resizable=yes,width=400,height=400')">Quick 
                                Pick </a> </td>
                              <td> 
                                <input type=text name="LastModifiedDate">
                              </td>
                            </tr>
                            <tr> 
                              <td class=tdHeaderAlt> 
                                <p>Sort by:</p>
                              </td>
                              <td> 
                                <select name=orderBy>
                                  <option value="PhysicianID">ID</option>
                                  <option value="LastName">Last Name</option>
                                  <option value="FirstName">First Name</option>
                                  <option value="SSN">SSN</option>
                                  <option value="IsAttested">Attested</option>
                                  <option value="AttestDate">Attest Date</option>
                                  <option value="LastModifiedDate">Last Modified</option>
                                </select>
                              </td>
                            </tr>
                            <tr bgcolor="#CCCCCC"> 
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td> 
                                <input type=hidden name="startID" value=0>
                                <input type=hidden name="maxResults" value=50>
                                <input type="submit" name="Submit2" value="Submit">
                              </td>
                              <td>&nbsp;</td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    <p>&nbsp;</p>
                  </form>
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <p> 
                    <%
}
else
{


String myWhere = "where  (tAdminPhysicianLU.adminID="+iAdminID+") and (tAdminPhysicianLU.physicianID=tPhysicianMaster.physicianID) and (tPhysicianMaster.physicianID>0";
boolean theFirst = false;

try
{
if (!myPhysicianID.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "tPhysicianMaster.PhysicianID LIKE '%" + myPhysicianID +"%'";
theFirst = false;
}
if (!myLastName.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "LastName LIKE '%" + DataControlUtils.fixApostrophe(myLastName) +"%'";
theFirst = false;
}
if (!myFirstName.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "FirstName LIKE '%" + DataControlUtils.fixApostrophe(myFirstName) +"%'";
theFirst = false;
}
if (!mySSN.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "SSN LIKE '%" + mySSN +"%'";
theFirst = false;
}
if (!myIsAttested.equalsIgnoreCase("0"))
{
if (!myIsAttested.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "IsAttested = " + myIsAttested +"";
theFirst = false;
}
}
if (!myAttestDate.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "AttestDate > '" + dbdf.format(PLCUtils.getSubDate(myAttestDate)) +"'";
theFirst = false;
}
if (!myLastModifiedDate.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "LastModifiedDate > '" + dbdf.format(PLCUtils.getSubDate(myLastModifiedDate)) +"'";
theFirst = false;
}
if (!myDOBm.equalsIgnoreCase("0"))
{
if (!myDOBm.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "Month(DateOfBirth) = " + myDOBm +"";
theFirst = false;
}
}



}
catch(Exception e)
{
out.println("FFF:"+e);
}

myWhere += ")";


//System.out.println(myWhere);
if (theFirst||myWhere.equalsIgnoreCase(")"))
{
myWhere = "";
}


searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
myRS = mySS.executeStatement("select  tPhysicianMaster.DateOfBirth, tPhysicianMaster.FirstName,tPhysicianMaster.IsAttested,tPhysicianMaster.IsAttestedPending,tPhysicianMaster.LastName,tPhysicianMaster.SSN,tPhysicianMaster.LastModifiedDate,tPhysicianMaster.AttestDate, tAdminPhysicianLU.PhysicianID, tAdminPhysicianLU.LookupID from tAdminPhysicianLU,tPhysicianMaster " + myWhere + " order by tPhysicianMaster." + orderBy);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
cnt++;
if (cnt>=startID&&cnt<=startID+maxResults)
{
cnt2++;

String myClass = "tdBase";
if (cnt2%2==0)
{
myClass = "tdBaseAlt";
}
String pmPhysicianID = myRS.getString("PhysicianID");
myMainTable +="<tr class="+myClass+">";
myMainTable +="<td>"+cnt+"</td>";
myMainTable +="<td>";
myMainTable +="<a href = \"tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID="+pmPhysicianID+"&EDIT=edit\"><img border=0 src=\"ui_"+thePLCID+"/icons/edit_AdminID.gif\"></a>        <a class=linkBase  onClick=\"return confirmDelete()\"   href = \"tAdminPhysicianLU_main_LU_AdminID_form_authorize.jsp?EDIT=del&EDITID="+myRS.getString("LookupID")+"&KM=p\"><img border=0 src=\"ui_"+thePLCID+"/icons/remove_AdminID.gif\">";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=pmPhysicianID+ "";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +="<b>"+myRS.getString("LastName")+"</b>";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +="<b>"+myRS.getString("FirstName")+"</b>";
myMainTable +="</td>";
myMainTable +="<td nowrap>";
myMainTable +=myRS.getString("SSN")+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=(new bltYesNoLI (new Integer(myRS.getString("IsAttested")) ).getYNLong()+"");
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=PLCUtils.getDisplayDate(  dbdf2.parse(myRS.getString("DateOfBirth")),false)+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=PLCUtils.getDisplayDate(  dbdf2.parse(myRS.getString("AttestDate")),true)+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=PLCUtils.getDisplayDate(  dbdf2.parse(myRS.getString("LastModifiedDate")),true)+"";
myMainTable +="</td>";
myMainTable +="</tr>";
}
   }
mySS.closeAll();
endCount = cnt;



if (startID>=endCount)
{
startID = endCount-maxResults;

}

if (maxResults<=0)
{
maxResults=10;
}


if (startID<=0)
{
startID=0;
}




%>
                  </p>
                  <table width=100% border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
                    <tr> 
                      <td> 
                        <table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
                          <tr> 
                            <td> <form name="selfForm" method="get" action="AdminPhysician_query.jsp"> 
                              <table border=0 cellspacing=1 width='100%' cellpadding=3>
                                <tr > 
                                  <td colspan=4 class=tdHeader> 
                                    <p>Enter Search Criteria:</p>
                                  </td>
                                  <td colspan=1 class=tdBase align=right nowrap> 
                                    <%if (startID>0)
{%>
                                    <a href="AdminPhysician_query.jsp?startID=<%=(startID-maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&PhysicianID=<%=myPhysicianID%>&LastName=<%=myLastName%>&FirstName=<%=myFirstName%>&SSN=<%=mySSN%>&IsAttested=<%=myIsAttested%>&AttestDate=<%=myAttestDate%>&LastModifiedDate=<%=myLastModifiedDate%>&DOBm=<%=myDOBm%>"><< 
                                    previous <%=maxResults%></a> 
                                    <%
}
else
{
%>
                                    <p>&nbsp;</p>
                                    <%
}
%>
                                  </td>
                                  <td colspan=1 class=tdBase> 
                                    <%
if ((startID+maxResults)<endCount)
{
%>
                                    <a href="AdminPhysician_query.jsp?startID=<%=(startID+maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&PhysicianID=<%=myPhysicianID%>&LastName=<%=myLastName%>&FirstName=<%=myFirstName%>&SSN=<%=mySSN%>&IsAttested=<%=myIsAttested%>&AttestDate=<%=myAttestDate%>&LastModifiedDate=<%=myLastModifiedDate%>&DOBm=<%=myDOBm%>"> 
                                    next >> <%=maxResults%></a> 
                                    <%
}
else
{
%>
                                    <p>&nbsp;</p>
                                    <%
}


}
catch(Exception e)
{
out.println("PrevNext:"+e);
}




try{

%>
                                  </td>
                                  <td colspan=3 class=tdHeader nowrap> 
                                    <input type=hidden name=maxResults value=<%=maxResults%> >
                                    <input type=hidden name=startID value=0 >
                                    <p>Sort: 
                                      <select name=orderBy>
                                        <option value=PhysicianID <%if (orderBy.equalsIgnoreCase("PhysicianID")){out.println(" selected");}%> >ID</option>
                                        <option value=LastName <%if (orderBy.equalsIgnoreCase("LastName")){out.println(" selected");}%> >Last 
                                        Name</option>
                                        <option value=FirstName <%if (orderBy.equalsIgnoreCase("FirstName")){out.println(" selected");}%> >First 
                                        Name</option>
                                        <option value=SSN <%if (orderBy.equalsIgnoreCase("SSN")){out.println(" selected");}%> >SSN</option>
                                        <option value=IsAttested <%if (orderBy.equalsIgnoreCase("IsAttested")){out.println(" selected");}%> >Attested</option>
                                        <option value=AttestDate <%if (orderBy.equalsIgnoreCase("AttestDate")){out.println(" selected");}%> >Attest 
                                        Date</option>
                                        <option value=LastModifiedDate <%if (orderBy.equalsIgnoreCase("LastModifiedDate")){out.println(" selected");}%> >Last 
                                        Modified</option>
                                      </select>
                                    </p>
                                  </td>
                                  <td class=tdHeader colspan=1> 
                                    <p  align="center"> 
                                      <input type="Submit" name="Submit" value="Submit" align="middle">
                                    </p>
                                  </td>
                                </tr>
                                <tr class=tdHeader valign="bottom"> 
                                  <td  colspan=2 nowrap align="center" valign="middle" ><img src="images/image_dr_coat.jpg"></td>
                                  <td colspan=1 nowrap> 
                                    <input type="text" name="PhysicianID" value="<%=myPhysicianID%>" size="4">
                                  </td>
                                  <td colspan=1 nowrap> 
                                    <input type="text" name="LastName" value="<%=myLastName%>" size="8">
                                  </td>
                                  <td colspan=1 nowrap> 
                                    <input type="text" name="FirstName" value="<%=myFirstName%>" size="8">
                                  </td>
                                  <td colspan=1 nowrap> 
                                    <input type="text" name="SSN" value="<%=mySSN%>" size="10">
                                  </td>
                                  <td colspan=1 nowrap> 
                                    <select name="IsAttested">
                                      <jsp:include page="../generic/tYesNoLIShortSpecial.jsp" flush="true" > 
                                      <jsp:param name="CurrentSelection" value="<%=myIsAttested%>" />
                                      </jsp:include>
                                    </select>
                                  </td>
                                  <td colspan=1 bgcolor="#CCCCCC" nowrap> Choose<br>
                                    Month: <br>
                                    <select name="DOBm">
                                      <jsp:include page="../generic/tMonthTypeLILong.jsp" flush="true" > 
                                      <jsp:param name="CurrentSelection" value="<%=myDOBm%>" />
                                      </jsp:include>
                                    </select>
                                  </td>
                                  <td colspan=1 bgcolor="#CCCCCC" nowrap> Later 
                                    than:<br>
                                    <input type="text" name="AttestDate" value="<%=myAttestDate%>" size="9">
                                    <br>
                                    <a href="#" onClick="MM_openBrWindow('QR-DateQuickPick.jsp?OFieldName=AttestDate','QPDate','status=yes,scrollbars=yes,resizable=yes,width=400,height=400')">Quick 
                                    Pick </a></td>
                                  <td colspan=1 bgcolor="#CCCCCC" nowrap> Later 
                                    than:<br>
                                    <input type="text" name="LastModifiedDate" value="<%=myLastModifiedDate%>" size="9">
                                    <br>
                                    <a href="#" onClick="MM_openBrWindow('QR-DateQuickPick.jsp?OFieldName=LastModifiedDate','QPDate','status=yes,scrollbars=yes,resizable=yes,width=400,height=400')">Quick 
                                    Pick </a></td>
                                </tr>
                                <tr class=tdHeader bgcolor="#333333"> 
                                  <td nowrap><font color="#FFFFFF">#</font></td>
                                  <td colspan=1 nowrap><font color="#FFFFFF"> 
                                    Actions </font></td>
                                  <td colspan=1 nowrap><font color="#FFFFFF"> 
                                    ID </font></td>
                                  <td colspan=1 nowrap><font color="#FFFFFF"> 
                                    Last Name </font></td>
                                  <td colspan=1 nowrap><font color="#FFFFFF"> 
                                    First Name </font></td>
                                  <td colspan=1 nowrap><font color="#FFFFFF"> 
                                    SSN </font></td>
                                  <td colspan=1 nowrap><font color="#FFFFFF"> 
                                    Attested </font></td>
                                  <td colspan=1 nowrap><font color="#FFFFFF"> 
                                    DOB</font></td>
                                  <td colspan=1 nowrap><font color="#FFFFFF"> 
                                    Attest Date </font></td>
                                  <td colspan=1 nowrap><font color="#FFFFFF"> 
                                    Last Modified </font></td>
                                </tr>
                                <%=myMainTable%> 
                              </table>
                              <%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>
                        </table></form>
                        </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
          </td>
        </tr>
      </table>
      <%


  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>
      <p>&nbsp;</p>
    </td>
  </tr>
</table>
