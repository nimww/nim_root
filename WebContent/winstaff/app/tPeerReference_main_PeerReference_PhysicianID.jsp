<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltSalutationLI, com.winstaff.ConfigurationInformation,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPeerReference,com.winstaff.bltPeerReference_List" %>
<%/*
    filename: tPeerReference_main_PeerReference_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tPeerReference")%>



<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection11", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tPeerReference_main_PeerReference_PhysicianID.jsp");
    pageControllerHash.remove("iReferenceID");
    pageControllerHash.put("sINTNext","tPeerReference_main_PeerReference_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltPeerReference_List        bltPeerReference_List        =    new    bltPeerReference_List(iPhysicianID);

//declaration of Enumeration
    bltPeerReference        working_bltPeerReference;
    ListElement         leCurrentElement;
    Enumeration eList = bltPeerReference_List.elements();
    %>
        <%@ include file="tPeerReference_main_PeerReference_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tPeerReference_main_PeerReference_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
         
      <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
        <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltPeerReference  = (bltPeerReference) leCurrentElement.getObject();
        working_bltPeerReference.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltPeerReference.isComplete())
        {
            theClass = "incompleteItem";
        %>
        <tr class=incompleteItem><td><b>Not Complete</b><br>
        <%
        }
        else
        {
        %>
        <tr class=<%=theClass%> ><td> 
        <%
        }
        %>

              <b>Item ID:&nbsp;</b><%=working_bltPeerReference.getReferenceID()%></td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltPeerReference.isExpired("FirstName",expiredDays))&&(working_bltPeerReference.isExpiredCheck("FirstName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltPeerReference.isRequired("FirstName"))&&(!working_bltPeerReference.isComplete("FirstName")) ){theClassF = "requiredFieldMain";}%>
            
          <td valign="top"> 
            <p class=<%=theClassF%> ><b><%=new bltSalutationLI(working_bltPeerReference.getSalutation()).getSalutationShort()%>&nbsp;<%=working_bltPeerReference.getFirstName()%>&nbsp;<%=working_bltPeerReference.getLastName()%>,&nbsp;<%=working_bltPeerReference.getTitle()%></b><br><%=working_bltPeerReference.getPhone()%><br><%=working_bltPeerReference.getContactEmail()%></p>
          </td>

<%theClassF = "textBase";%>
            
          <td valign="top"> 
            <p class=<%=theClassF%> ><b><%=working_bltPeerReference.getHospitalAffiliation()%></b><br><%=working_bltPeerReference.getHospitalDepartment()%></p></td>

            <td > 
        <a class=linkBase href = "tPeerReference_main_PeerReference_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltPeerReference.getReferenceID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase  onClick="return confirmDelete()"  href = "tPeerReference_main_PeerReference_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltPeerReference.getReferenceID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        <br>
        <% 
            if (working_bltPeerReference.getDocuLinkID().intValue()>0)
            {
              bltDocumentManagement myDoc = new bltDocumentManagement(working_bltPeerReference.getDocuLinkID());
              if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
              {
              %>
                      <a target=_blank href="tPeerReference_main_PeerReference_PhysicianID_form_authorize.jsp?EDIT=print&EDITID=<%=working_bltPeerReference.getReferenceID()%>&KM=p">View Document</a>
              <%
              }
              else
              {
              %>

                      <a class=linkBase target=_blank href = "#" onClick="window.open('tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=faxCover&amp;EDITID=<%=working_bltPeerReference.getDocuLinkID()%>','DocPDF','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=500');return false;" >Print Cover Sheet</a>

              <%
              }
            }
            else
            {
             if (working_bltPeerReference.isComplete())
             {
               %>
                      <a target=_blank class=linkBase href = "tPeerReference_ModifyDocument.jsp?EDITID=<%=working_bltPeerReference.getReferenceID()%>&EDIT=CREATE&dType=tPeerReference">Create Document</a>
               <%
             }
             else
             {
               %>
                      <span class=tdBase>You must complete this item before you can attach a document</span>
               <%
             }
            }
            %>
        <% }%>
                  </td></tr>
        <%
    }//end while
       }//end of if
       else 
       {
           %>
           <tr><td><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tPeerReference")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tPeerReference")%>"))
           {
               document.location="tPeerReference_main_PeerReference_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
