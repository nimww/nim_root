<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.searchDB2, com.winstaff.bltPracticeMaster ,com.winstaff.bltAdminPracticeLU,com.winstaff.bltAdminPracticeLU_List_LU_AdminID,com.winstaff.bltPhysicianMaster ,com.winstaff.bltAdminPhysicianLU,com.winstaff.bltAdminPhysicianLU_List_LU_AdminID,com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltAdminMaster" %>
<%/*
    filename: out\jsp\tAdminMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>
<%@ include file="../generic/CheckLogin.jsp" %>
<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_AdminID_nim.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;    
   }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","pro-file-manager_Status.jsp");
    pageControllerHash.put("sParentReturnPage","pro-file-manager_Status.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);
bltAdminMaster        AdminMaster        =   new  bltAdminMaster (iAdminID);

%>
<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<%
if (CurrentUserAccount.getPLCID().intValue()==111&&isBasic)
{
%>
<script language="javascript">
alert("*** Change of Service Notice ***\n\nDear CredentialPlus User:\n\nAs you may have heard, there has been an announcement by an organization called Health Care Administrative Solutions (HCAS) that will be providing credentialing services to several large insurers in the state of Massachusetts. Those plans include Blue Cross Blue Shield of Massachusetts, Fallon Community Health Plan, Harvard Pilgrim Health Care, Health New England, Neighborhood Health Plan, Network Health, and Tufts Health Plan. Trizetto/NCVO has announced that they will terminate their credentialing services for the above health plans as of July 31, 2006.  We offered to continue providing your application information electronically to HCAS, but they declined our requests.\n\nFor the Basic user, this means you will need to complete another application for HCAS for the above health plans. Because Trizetto/NCVO will no longer be providing credentialing services to the above health plans, they have ceased the financial support of Basic users as of March 31, 2006. If you wish to continue using CredentialPlus for your other health plans and hospital applications, you will need to upgrade to the Premium service. If you continue to enter information into CredentialPlus, it will not be electronically received by any healthcare organizations.\n\nFor Premium users, you will still have the capability to manage and organize all your credentialing data on-line and print out application forms as needed for other health plans and hospitals, but electronic applications to the above health plans will not be available after March 31, 2006.  You can still submit applications without filling out paper to the above health plans by faxing the Integrated Massachusetts Credentialing Application (completed and printed by our system).  There are two hospitals that continue to accept electronic applications through the CredentialPlus Premium service: UMass Memorial Hospital and St. Vincent Hospital.  CMIPA physicians / users are Premium users.\n\nIf you have any questions about credentialing with the above health plans, please contact them directly.\n\n\n\nSincerely,\n\n\n\nThe CredentialPlus Team");
</script>
<%
}
%>
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p class=title>Account  Home<br>
      </p>
      <%
String helpIncludeFN = "ui_"+thePLCID+"\\help_AdminID.jsp";
%>
      <jsp:include page="<%=helpIncludeFN%>" flush="true" ></jsp:include>
      <table cellpadding="0" cellspacing="0" border="1"  width="100%" bordercolor="#333333">
        <tr> 
          <td> 
            <table cellpadding="0" cellspacing="0" border="0" class=tdBase width="100%" bordercolor="#999999">
              <tr > 
                <td height="100%" valign=top > 
                  <table cellpadding="5" cellspacing="0" border="1" class=tdBase width="100%" bordercolor="#999999" height="100%">
                    <tr > 
                      <td class = tdHeaderAlt height="100%"> Welcome to the PRO-FILE 
                        On-Line Credentialing System</td >
                    </tr>
                    <tr> 
                      <td height="100%" > 
                        <%
searchDB2 mySDB = new searchDB2();
java.sql.ResultSet myRS = mySDB.executeStatement("select tAdminPhysicianLU.PhysicianID from tAdminPhysicianLU where adminid = '"+iAdminID+"'");
int myCount = 0;
Integer iPhys = null;
while (myRS.next())
{
  myCount++;
  iPhys = new Integer( myRS.getString("PhysicianID"));
}

%>


                        <table class=tdBase cellpadding="2" cellspacing="0" border="0" width="100%">
                          <tr>
                            <td>&nbsp;</td>
                            <td><input type="button" class="tdBaseAltGreen" onclick="this.disabled=false;document.location='mailto:<%=AdminMaster.getAlertEmail()%>';" value = "Send Emailt to: <%=AdminMaster.getAlertEmail()%>" /></td>
                          </tr>
                          <tr>
                            <td bgcolor="#D0F4CE"><span class="instructions"><img src="images/icon_AdminID.gif" width="51" height="25" border="0" /></span></td>
                            <td bgcolor="#D0F4CE"><input type="button" class="tdBaseAltGreen" onclick="this.disabled=true;document.location='tAdminMaster_form.jsp?EDIT=edit';" value = "Update Account Information Here" /></td>
                          </tr>
                          <tr>
                            <td bgcolor="#D0F4CE"><span class="instructions"><img src="images/icon_AdminID.gif" width="51" height="25" border="0" /></span></td>
                            <td bgcolor="#D0F4CE"><input type="button" class="tdBaseAltGreen" onclick="this.disabled=true;document.location='tPracticeActivity_main_PracticeActivity_AdminID.jsp';" value = "View Activities" /></td>
                          </tr>
                          </table>


<%
if (false)
{
if (myCount==1)
{ 
%>
                        <table class=tdBase cellpadding="2" cellspacing="0" border="0" width="100%">
                          <tr> 
                            <td class=instructions><a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=iPhys%>"><img src="images/continue_practitioner.gif" width="158" height="43" border="0"></a></td>
                            <td> <a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=iPhys%>"><b>Continue 
                              to Edit Existing Practitioner</b></a>- This will 
                              continue the data entry process for your Practitioner. 
                            </td>
                          </tr>
                        </table>
                        <%
}
else if (myCount==0)
{
%>
                        <table class=tdBase cellpadding="2" cellspacing="0" border="0" width="100%">
                          <tr> 
                            <td class=instructions><a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_create.jsp?EDIT=new"><img src="images/continue_hand.gif" width="37" height="23" border="0"></a></td>
                            <td><a href = "tAdminPhysician_MasterAdd.jsp"&EDIT=edit"><b>Start 
                              Here</b></a> - This will create your first practitioner 
                              and guide you through the data entry process.</td>
                          </tr>
                        </table>
                        <%
}
else
{
%>
                        <table class=tdBase cellpadding="2" cellspacing="0" border="0" width="100%">
                          <tr> 
                            <td class=instructions><img src="images/continue_hand.gif" width="37" height="23" border="0"></td>
                            <td>Please <a href="#Manage"><b>scroll down</b></a> 
                              and select the Practitioner or Practice that you 
                              would like to edit.</td>
                          </tr>
                        </table>
                        <%
}
mySDB.closeAll();
%>
                        <table class=tdBase cellpadding="2" cellspacing="0" border="0" width="100%">
<%
if (!isBasic&&false)
{
%>
                          <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><span class="instructions"><img src="images/continue_hand.gif" width="37" height="23" border="0"></span></td>
                            <td><a href="billing.jsp"><strong>Update Billing Information Here</strong></a> </td>
                          </tr>
<%
}
%>						  
                          <tr> 
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr> 
                            <td class=instructions><a href="tAdminPhysician_MasterAdd.jsp"><img src="images/findadd-small.gif" width="34" height="31" border="0"></a></td>
                            <td> <a href="tAdminPhysician_MasterAdd.jsp"><b>Add 
                              a New Practitioner</b></a> - This feature allows 
                              you to find a practitioner whose data is already 
                              in this database. </td>
                          </tr>
                          <tr> 
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr> 
                            <td><a href="#" onClick="MM_openBrWindow('flashoverview.html','FlashOverview','status=yes,width=556,height=406')"><img src="images/guide.gif" width="37" height="35" border="0"></a></td>
                            <td bgcolor="#FFFFCC"> <font color="#00CC66"><b><font color="#00CC66"><b><a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank"><img src="/winstaff/app/images/getacro.gif" border="1" align="right"></a></b></font><font color="#009900">Are 
                              you a new user? View the Quick Start Guide:</font><font color="#009900"><br>
                              View online - <a href="help_images/guide.htm" target="_blank">click 
                              here</a><br>
                              Print -</font><font color="#009933"> <a href="help_images/QuickStartGuideMaster.pdf" target="_blank">click 
                              here</a></font></b></font></td>
                          </tr>
                        </table>
<%
}
%>
                      </td>
                    </tr>
                  </table>
                </td >
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr class=title> 
          <td align=Center width="50%"><a name="Manage"></a>Practitioners </td>
          <td align=Center width="50%">Practices</td>
        </tr>
        <tr class=instructions> 
          <td align=Center width="50%">(only the first 5 recently modified are 
            shown)<br>
            (Click on Manage to see more)</td>
          <td align=Center width="50%">(only the first 5 are 
            shown)<br>
            (Click on Manage to see more)</td>
        </tr>
      </table>
      <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
        <tr> 
          <td width="50%" valign=top> 
            <table width="100%" border=1 cellspacing="0" cellpadding="2" bordercolor="#999999">
              <tr class=tdHeader> 
                <td colspan=4> <a href = "tAdminPhysician_MasterAdd.jsp"&EDIT=edit"><img border=0 src="ui_<%=thePLCID%>/icons/add_AdminID.gif"></a>&nbsp; 
                  <a href = "AdminPhysician_query.jsp?maxResults=50&startID=0&orderBy=LastName&Submit=Submit&PhysicianID=&LastName=&FirstName=&SSN=&IsAttested=0&DOBm=0&AttestDate=&LastModifiedDate="><img border=0 src="ui_<%=thePLCID%>/icons/manage_AdminID.gif"></a></td>
              </tr>
              <tr class=tdHeaderAlt> 
                <td nowrap>ID</td>
                <td nowrap>Last Name</td>
                <td nowrap>First Name</td>
                <td nowrap>Modified</td>
              </tr>
              <%
if (true)
{
	try
	{
		myRS = mySDB.executeStatement("select tPhysicianMaster.firstname, tPhysicianMaster.lastname, tPhysicianMaster.physicianID, tPhysicianMaster.LastModifiedDate from tPhysicianMaster where tPhysicianMaster.physicianID in (select tadminphysicianLU.physicianID from tAdminPhysicianLU where adminid = "+iAdminID+") order by tPhysicianMaster.LastModifiedDate DESC limit 5");
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
    int cnt=0;
    while (myRS!=null&&myRS.next())
    {
	cnt++;
	String myClass = "tdBaseAlt";
	if (cnt%2!=0)
	{
		myClass = "tdBase";
	}
String getPhysicianID = myRS.getString("PhysicianID");
String getLastName = myRS.getString("LastName");
String getFirstName = myRS.getString("FirstName");
String theDate = "";
if (myRS.getDate("LastModifiedDate")!=null)
{
	theDate = displayDateSDF.format(myRS.getDate("LastModifiedDate"));
}
String getLastModifiedDate = theDate;

%>
              <tr class=<%=myClass%>> 
                <td><a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=getPhysicianID%>"><%=getPhysicianID%></a></td>
                <td><a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=getPhysicianID%>"><%=getLastName%></a></td>
                <td><a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=getPhysicianID%>"><%=getFirstName%></a></td>
                <td><a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=getPhysicianID%>"><%=getLastModifiedDate%></a></td>
              </tr>
              <%
	}
}
	%>
            </table>
          </td>
          <td width="50%" valign=top> 
            <table width="100%" border=1 cellspacing="0" cellpadding="2" bordercolor="#999999">
              <tr class=tdHeader> 
                <td colspan=3> <a href = "tAdminPracticeLU_main_PracticeMaster_AdminID_form_create.jsp?EDIT=new"&EDIT=edit"><img border=0 src="ui_<%=thePLCID%>/icons/add_AdminID.gif"></a> 
                  <a href = "AdminPractice_query.jsp?PracticeID=&PracticeName=&OfficeCity=&OfficeStateID=0&RelationshipTypeID=-1&OfficeZip=&orderBy=PracticeID&startID=0&maxResults=50&Submit2=Submit"&EDIT=edit"><img border=0 src="ui_<%=thePLCID%>/icons/manage_AdminID.gif"></a></td>
              </tr>
              <tr class=tdHeaderAlt> 
                <td nowrap>ID</td>
                <td nowrap>Practice Name</td>
                <td nowrap>Modified</td>
              </tr>
              <%
if (true)
{
	try
	{
		myRS = mySDB.executeStatement("select tpracticeMaster.practicename,tpracticeMaster.OfficeCity, tpracticeMaster.LastModifiedDate,tpracticeMaster.officeZIP, tpracticeMaster.practiceID from tpracticeMaster where tpracticeMaster.practiceID in (select tadminpracticeLU.practiceID from tAdminpracticeLU where adminid = "+iAdminID+")  order by tPracticeMaster.PracticeID limit 5");
//ms		myRS = mySDB.executeStatement("select top 5 tpracticeMaster.practicename,tpracticeMaster.OfficeCity, tpracticeMaster.LastModifiedDate,tpracticeMaster.officeZIP, tpracticeMaster.practiceID from tpracticeMaster where tpracticeMaster.practiceID in (select tadminpracticeLU.practiceID from tAdminpracticeLU where adminid = "+iAdminID+")  order by tPracticeMaster.PracticeID");		
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
    int cnt=0;
    while (myRS!=null&&myRS.next())
    {
	cnt++;
	String myClass = "tdBaseAlt";
	if (cnt%2!=0)
	{
		myClass = "tdBase";
	}
String getPracticeID = myRS.getString("practiceID");
String getPracticeName = myRS.getString("PracticeName");
if (getPracticeName.equalsIgnoreCase(""))
{
	getPracticeName = myRS.getString("OfficeCity");
}
String getOfficeZIP = myRS.getString("OfficeZIP");
String theDate = "";
if (myRS.getDate("LastModifiedDate")!=null)
{
	theDate = displayDateSDF.format(myRS.getDate("LastModifiedDate"));
}
String getLastModifiedDate = theDate;

%>
              <tr class=<%=myClass%>> 
                <td><a href="tAdminPracticeLU_main_PracticeMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=getPracticeID%>"><%=getPracticeID%></a></td>
                <td><a href="tAdminPracticeLU_main_PracticeMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=getPracticeID%>"><%=getPracticeName%>&nbsp;(<%=getOfficeZIP%>)</a></td>
                <td><a href="tAdminPracticeLU_main_PracticeMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID=<%=getPracticeID%>"><%=getLastModifiedDate%></a></td>
              </tr>
              <%
	}
}
mySDB.closeAll();
	%>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr class=instructions> 
    <td width=10>&nbsp;</td>
    <td colspan=2>&nbsp;</td>
  </tr>
<%
if (false)
{
%>
  <tr class=instructions> 
    <td width=10>&nbsp;</td>
    <td colspan=2>To run reports, <a href="reports_AdminID.jsp">click here</a></td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<%
}%>

  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
        <tr> 
          <td> 
            <table width="100%" border="1" cellspacing="0" cellpadding="2" bordercolor="#999999">
              <tr> 
                <td class=tdHeader width="30%" valign="top"> 
                  <div align="right">Account  Group Information:&nbsp;</div>
                </td>
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top" height="20"> 
                        <b>Name</b>:&nbsp;</td>
                      <td height="20"><%=AdminMaster.getName()%></td>
                    </tr>
<%if (false)
{
%>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"> <b>Address</b>:&nbsp;</td>
                       <td><%=AdminMaster.getAddress1()%>&nbsp;<%=AdminMaster.getAddress2()%><br>
                        <%=AdminMaster.getCity()%>&nbsp; 
                        <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=AdminMaster.getStateID()%>" />
                        </jsp:include>
                        &nbsp;<%=AdminMaster.getZIP()%></td>
                    </tr>
<%
}
%>					
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"><b>Email</b>:&nbsp;</td>
                      <td><a href="mailto:<%=AdminMaster.getAlertEmail()%>" ><%=AdminMaster.getAlertEmail()%></a></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class=tdHeader width="30%" valign="top"> 
                  <div align="right">User Information:&nbsp;</div>
                </td>
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"><b>User Logon 
                        Name</b>:&nbsp;</td>
                      <td><%=CurrentUserAccount.getLogonUserName()%></td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"><b>User Access</b>:&nbsp;</td>
                      <td> 
                        <jsp:include page="../generic/tUserAccessTypeLILong_translate.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=CurrentUserAccount.getAccessType()%>" />
                        </jsp:include>
                      </td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"><b>Contact Email</b>:&nbsp;</td>
                      <td><a href="mailto:<%=CurrentUserAccount.getAlertEmail()%>" ><%=CurrentUserAccount.getAlertEmail()%></a></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class=tdHeader width="30%" valign="top"> 
                  <div align="right">Alert Status:&nbsp;</div>
                </td>
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class=tdBase> 
                      <td width="30%" valign="top"> 
                        <%
					  if (AdminMaster.getAlertDays().intValue()>0)
					  {
					  %>
                        &nbsp;&nbsp;&nbsp;You are currently set to receive Alerts 
                        at <a href="mailto:<%=AdminMaster.getAlertEmail()%>" ><%=AdminMaster.getAlertEmail()%></a> 
                        <%
					  }
					  else
					  {
					  %>
                        &nbsp;&nbsp;&nbsp;You are NOT configured to receive Alert 
                        Emails 
                        <%
					  }
					  %>
                      </td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" valign="top">&nbsp;&nbsp;&nbsp;To change 
                        your Alert Options, <a href="Admin_options.jsp">click 
                        here</a></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td width=10>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>