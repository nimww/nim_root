<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*, java.util.Enumeration" %>
<%/*
    filename: out\jsp\cl_Practice_Physician_via_CVO.jsp
    Created on Feb/17/2003
    Type: cl main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
  <tr> 
    <td width=10>&nbsp;</td>
    <td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","Add_Practice")%>
	 <br>
      <a href="tPhysicianPracticeLU_PhysicianID.jsp">return</a> 
      <%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    Integer        iCVOID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if ((pageControllerHash.containsKey("iPhysicianID"))&&(pageControllerHash.containsKey("iCVOID"))) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        iCVOID        =    (Integer)pageControllerHash.get("iCVOID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateLongSDF = new java.text.SimpleDateFormat("MMMM dd yyyy");

%>
      <br>
      <br>
      <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
        <tr> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bordercolor="#CCCCCC">
              <tr class=tdHeaderAlt> 
                <td width="50%" class=title> 
                  <div align="center">Option 1</div>
                </td>
                <td width="10" bgcolor="#333333"> 
                  <div align="center"></div>
                </td>
                <td width="50%" class=title> 
                  <div align="center">Option 2</div>
                </td>
              </tr>
              <tr class=tdBase> 
                <td width="50%"> 
                  <div align="center">Select an existing practice from the list 
                    below</div>
                </td>
                <td width="10" bgcolor="#333333">&nbsp;</td>
                <td width="50%"> 
                  <div align="center">Create a new practice<br>
                    <a href="tPhysicianPracticeLU_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"><img border=0 src="tPhysicianPracticeLU_PhysicianID_form_create-PhysOnly.jsp"></a> 
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <p class=instructions>If you do not want to add a practice, <a href="tPhysicianPracticeLU_PhysicianID.jsp">click 
        here</a> to return.</p>
      <%




    session.setAttribute("pageControllerHash",pageControllerHash);
	searchDB2 mySS = new searchDB2();
	java.sql.ResultSet myRS = null;;
	try
	{
		myRS = mySS.executeStatement("SELECT  tPracticeMaster.PracticeID FROM tPracticeMaster INNER JOIN tPhysicianPracticeLU ON tPhysicianPracticeLU.PracticeID = tPracticeMaster.PracticeID INNER JOIN tHCOPhysicianLU ON tPhysicianPracticeLU.PhysicianID = tHCOPhysicianLU.PhysicianID INNER JOIN  tCVOHCOLU ON tCVOHCOLU.HCOID = tHCOPhysicianLU.HCOID WHERE  (tCVOHCOLU.CVOID = "+iCVOID+") GROUP BY tPracticeMaster.PracticeID, tCVOHCOLU.CVOID ORDER BY tPracticeMaster.PracticeID");
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
	try
	{
		%>
		<p class=title>List of Existing Practices:</p>
        <table width="100%" border="1" cellspacing="0" cellpadding=0 bordercolor="#333333">
        <%
		int endCount = 0;
		int cnt=0;
		int cnt2=0;
	   if (myRS!=null)
	   {
 	   while (myRS!=null&&myRS.next())
 	   {
		cnt++;
		Integer iPracticeID = new Integer(myRS.getString("PracticeID"));        
        bltPracticeMaster working_bltPracticeMaster  = new bltPracticeMaster(iPracticeID);



        String theClass = "tdHeader";
        %>
        <tr> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding=0>
              <tr> 
                <td> 
                  <%String theClassF = "textBase";%>
                  <table width="100%" border="0" cellspacing="0" cellpadding="3">
                    <tr class=tdHeaderAlt> 
                      <td width=40%> 
                        <%theClassF = "textBase";%>
                        <%if ((working_bltPracticeMaster.isExpired("PracticeName",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("PracticeName"))){theClassF = "expiredFieldMain";}%>
                        <%if ( (working_bltPracticeMaster.isRequired("PracticeName"))&&(!working_bltPracticeMaster.isComplete("PracticeName")) ){theClassF = "requiredFieldMain";}%>
                        <p class=<%=theClassF%> ><%=working_bltPracticeMaster.getPracticeName()%></p>
                      </td>
                      <td width=40%> 
                        <p> 
                          <%theClassF = "textBase";%>
                          <%if ((working_bltPracticeMaster.isExpired("OfficeAddress1",expiredDays))&&(working_bltPracticeMaster.isExpiredCheck("OfficeAddress1"))){theClassF = "expiredFieldMain";}%>
                          <%if ( (working_bltPracticeMaster.isRequired("OfficeAddress1"))&&(!working_bltPracticeMaster.isComplete("OfficeAddress1")) ){theClassF = "requiredFieldMain";}%>
                        </p>
                        <p class=<%=theClassF%> ><%=working_bltPracticeMaster.getOfficeAddress1()%></p>
                      </td>
                      <td width=20%> 
                        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
                        {


							bltPhysicianPracticeLU_List_LU_PhysicianID        bltPhysicianPracticeLU_List_LU_PhysicianID        =    new    bltPhysicianPracticeLU_List_LU_PhysicianID(iPhysicianID,"PracticeID="+working_bltPracticeMaster.getPracticeID(),"");
							java.util.Enumeration eList2 = bltPhysicianPracticeLU_List_LU_PhysicianID.elements();
							if (eList2.hasMoreElements())
							{
							%>
									<p align=right>You have already added this practice.</p>
									<%
							}
							else
							{
							%>
									<p><a class=linkBase href = "cl_Practice_Physician_via_CVO_create.jsp?EDITID=<%=working_bltPracticeMaster.getPracticeID()%>"><img border=0 src="ui_<%=thePLCID%>/icons/add_PhysicianID.gif" ></a><br>click to add this practice</p>
									<%
							}
						}
			%>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <%
    }//while
%>
      </table>
      <%

    }
	else
    {
	%>
      <p class=instructions>There are currently no practices. Please <a class=linkBase href = "tPhysicianPracticeLU_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes">click 
        here</a> to create one.</p>
      <script language=javascript>
           alert("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements_PracticeSpecial","tPhysicianPracticeLU")%>");
           document.location="tPhysicianPracticeLU_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
      </script>
      <%
    }


   }
   catch(Exception e55)
   {
   }

    %>
</table>
<br>
<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%></td></tr></table>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
