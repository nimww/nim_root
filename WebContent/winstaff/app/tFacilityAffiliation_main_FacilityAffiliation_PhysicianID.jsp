<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltFacilityAffiliation,com.winstaff.bltFacilityAffiliation_List" %>
<%/*
    filename: tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tFacilityAffiliation")%>



<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection10", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp");
    pageControllerHash.remove("iAffiliationID");
    pageControllerHash.put("sINTNext","tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltFacilityAffiliation_List        bltFacilityAffiliation_List        =    new    bltFacilityAffiliation_List(iPhysicianID,"","StartDate DESC");

//declaration of Enumeration
    bltFacilityAffiliation        working_bltFacilityAffiliation;
    ListElement         leCurrentElement;
    Enumeration eList = bltFacilityAffiliation_List.elements();
    %>
        <%@ include file="tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltFacilityAffiliation  = (bltFacilityAffiliation) leCurrentElement.getObject();
        working_bltFacilityAffiliation.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltFacilityAffiliation.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <tr class=incompleteItem><td><b>Not Complete</b><br>
        <%
        }
        else
        {
        %>
        <tr class=<%=theClass%> ><td> 
        <%
        }
        %>

              <b>Item ID:&nbsp;</b><%=working_bltFacilityAffiliation.getAffiliationID()%></td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
            <td>
            <p class=<%=theClassF%> ><b><%=working_bltFacilityAffiliation.getFacilityName()%></b><br><b>Dept:</b> <%=working_bltFacilityAffiliation.getFacilityDepartment()%></p>
          </td>

<%theClassF = "textBase";%>
<%if ((working_bltFacilityAffiliation.isExpired("AppointmentLevel",expiredDays))&&(working_bltFacilityAffiliation.isExpiredCheck("AppointmentLevel"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltFacilityAffiliation.isRequired("AppointmentLevel"))&&(!working_bltFacilityAffiliation.isComplete("AppointmentLevel")) ){theClassF = "requiredFieldMain";}%>
            <td>
            <p class=<%=theClassF%> ><b>Status:&nbsp;</b><%=working_bltFacilityAffiliation.getAppointmentLevel()%>
<br>
<b>Term:</b> <%=PLCUtils.getDisplayDate(working_bltFacilityAffiliation.getStartDate(),false)%> to <%=PLCUtils.getDisplayDate(working_bltFacilityAffiliation.getEndDate(),false)%>			
			
			</p>
          </td>

            <td > 
        <a class=linkBase href = "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltFacilityAffiliation.getAffiliationID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase  onClick="return confirmDelete()"  href = "tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltFacilityAffiliation.getAffiliationID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        <% }%>
                  </td></tr>
        <%
    }//end while
       }//end of if
       else 
       {
           %>
           <tr><td><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tFacilityAffiliation")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tFacilityAffiliation")%>"))
           {
               document.location="tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
