<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltLicenseRegistration,com.winstaff.bltLicenseRegistration_List" %>
<%/*
    filename: tLicenseRegistration_main_LicenseRegistration_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tLicenseRegistration")%>

    <br><a href="tLicenseRegistration_main_LicenseRegistration_PhysicianID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection3", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tLicenseRegistration_main_LicenseRegistration_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iLicenseRegistrationID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltLicenseRegistration_List        bltLicenseRegistration_List        =    new    bltLicenseRegistration_List(iPhysicianID);

//declaration of Enumeration
    bltLicenseRegistration        working_bltLicenseRegistration;
    ListElement         leCurrentElement;
    Enumeration eList = bltLicenseRegistration_List.elements();
    %>
        <%@ include file="tLicenseRegistration_main_LicenseRegistration_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tLicenseRegistration_main_LicenseRegistration_PhysicianID_form_create.jsp?EDIT=new&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltLicenseRegistration  = (bltLicenseRegistration) leCurrentElement.getObject();
        working_bltLicenseRegistration.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltLicenseRegistration.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltLicenseRegistration.getLicenseRegistrationID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltLicenseRegistration.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltLicenseRegistration.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltLicenseRegistration.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%" bgColor=#ffffff> 
        <a class=linkBase href = "tLicenseRegistration_main_LicenseRegistration_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltLicenseRegistration.getLicenseRegistrationID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <br><a class=linkBase  onClick="return confirmDelete()"  href = "tLicenseRegistration_main_LicenseRegistration_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltLicenseRegistration.getLicenseRegistrationID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        <br>
        <% 
            if (working_bltLicenseRegistration.getDocuLinkID().intValue()>0)
            {
              bltDocumentManagement myDoc = new bltDocumentManagement(working_bltLicenseRegistration.getDocuLinkID());
              if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
              {
              %>
                      <a class=linkBase target=_blank href = "pdf/<%=myDoc.getDocumentFileName()%>">View Document</a>

              <%
              }
              else
              {
              %>

                      <a class=linkBase target=_blank href = "#" onClick="window.open('tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=faxCover&amp;EDITID=<%=working_bltLicenseRegistration.getDocuLinkID()%>','DocPDF','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=500');return false;" >Print Cover Sheet</a>

              <%
              }
            }
            else
            {
             if (working_bltLicenseRegistration.isComplete())
             {
               %>
                      <a target=_blank class=linkBase href = "tLicenseRegistration_ModifyDocument.jsp?EDITID=<%=working_bltLicenseRegistration.getLicenseRegistrationID()%>&EDIT=CREATE&dType=tLicenseRegistration">Create Document</a>
               <%
             }
             else
             {
               %>
                      <span class=tdBase>You must complete this item before you can attach a document</span>
               <%
             }
            }
            %>

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("IsCurrent",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("IsCurrent"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("IsCurrent"))&&(!working_bltLicenseRegistration.isComplete("IsCurrent")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Is this a current license?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltLicenseRegistration.getIsCurrent()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("LicenseType",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("LicenseType"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("LicenseType"))&&(!working_bltLicenseRegistration.isComplete("LicenseType")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Type:&nbsp;</b><jsp:include page="../generic/tLicenseTypeLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltLicenseRegistration.getLicenseType()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("StateID",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("StateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("StateID"))&&(!working_bltLicenseRegistration.isComplete("StateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltLicenseRegistration.getStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("IsPractice",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("IsPractice"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("IsPractice"))&&(!working_bltLicenseRegistration.isComplete("IsPractice")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Do you currently practice in this state?:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltLicenseRegistration.getIsPractice()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("LicenseDocumentNumber",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("LicenseDocumentNumber"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("LicenseDocumentNumber"))&&(!working_bltLicenseRegistration.isComplete("LicenseDocumentNumber")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Document Number:&nbsp;</b><%=working_bltLicenseRegistration.getLicenseDocumentNumber()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("IssueDate",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("IssueDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("IssueDate"))&&(!working_bltLicenseRegistration.isComplete("IssueDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Issue Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltLicenseRegistration.getIssueDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("ExpirationDate",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("ExpirationDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("ExpirationDate"))&&(!working_bltLicenseRegistration.isComplete("ExpirationDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Expiration Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltLicenseRegistration.getExpirationDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("Name",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("Name"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("Name"))&&(!working_bltLicenseRegistration.isComplete("Name")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Licensing Organization:&nbsp;</b><%=working_bltLicenseRegistration.getName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("Address1",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("Address1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("Address1"))&&(!working_bltLicenseRegistration.isComplete("Address1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address:&nbsp;</b><%=working_bltLicenseRegistration.getAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("Address2",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("Address2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("Address2"))&&(!working_bltLicenseRegistration.isComplete("Address2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address 2:&nbsp;</b><%=working_bltLicenseRegistration.getAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("City",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("City"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("City"))&&(!working_bltLicenseRegistration.isComplete("City")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Institution City:&nbsp;</b><%=working_bltLicenseRegistration.getCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("OrgStateID",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("OrgStateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("OrgStateID"))&&(!working_bltLicenseRegistration.isComplete("OrgStateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltLicenseRegistration.getOrgStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("Province",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("Province"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("Province"))&&(!working_bltLicenseRegistration.isComplete("Province")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Non-US Province, District, State:&nbsp;</b><%=working_bltLicenseRegistration.getProvince()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("ZIP",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("ZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("ZIP"))&&(!working_bltLicenseRegistration.isComplete("ZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ZIP:&nbsp;</b><%=working_bltLicenseRegistration.getZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("CountryID",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("CountryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("CountryID"))&&(!working_bltLicenseRegistration.isComplete("CountryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Country:&nbsp;</b><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltLicenseRegistration.getCountryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("Phone",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("Phone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("Phone"))&&(!working_bltLicenseRegistration.isComplete("Phone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltLicenseRegistration.getPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("Fax",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("Fax"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("Fax"))&&(!working_bltLicenseRegistration.isComplete("Fax")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Fax (XXX-XXX-XXXX):&nbsp;</b><%=working_bltLicenseRegistration.getFax()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("ContactName",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("ContactName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("ContactName"))&&(!working_bltLicenseRegistration.isComplete("ContactName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact Name:&nbsp;</b><%=working_bltLicenseRegistration.getContactName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("ContactEmail",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("ContactEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("ContactEmail"))&&(!working_bltLicenseRegistration.isComplete("ContactEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact E-mail:&nbsp;</b><%=working_bltLicenseRegistration.getContactEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("DocuLinkID",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("DocuLinkID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("DocuLinkID"))&&(!working_bltLicenseRegistration.isComplete("DocuLinkID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>DocuLink:&nbsp;</b><%=working_bltLicenseRegistration.getDocuLinkID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltLicenseRegistration.isExpired("Comments",expiredDays))&&(working_bltLicenseRegistration.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltLicenseRegistration.isRequired("Comments"))&&(!working_bltLicenseRegistration.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltLicenseRegistration.getComments()%></p>

        </td></tr></table></table><br>        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
