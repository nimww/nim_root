<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltDocumentManagement,com.winstaff.bltDocumentManagement_List" %>
<%/*
    filename: tDocumentManagement_main_DocumentManagement_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tDocumentManagement")%>

    <br><a href="tDocumentManagement_main_DocumentManagement_PhysicianID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("DocumentManagement1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tDocumentManagement_main_DocumentManagement_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iDocumentID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltDocumentManagement_List        bltDocumentManagement_List        =    new    bltDocumentManagement_List(iPhysicianID);

//declaration of Enumeration
    bltDocumentManagement        working_bltDocumentManagement;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltDocumentManagement_List.elements();
    %>
        <%@ include file="tDocumentManagement_main_DocumentManagement_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tDocumentManagement_main_DocumentManagement_PhysicianID_form_create.jsp?EDIT=new&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltDocumentManagement  = (bltDocumentManagement) leCurrentElement.getObject();
        working_bltDocumentManagement.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltDocumentManagement.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltDocumentManagement.getDocumentID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltDocumentManagement.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltDocumentManagement.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltDocumentManagement.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%" bgColor=#ffffff> 
        <a class=linkBase href = "tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltDocumentManagement.getDocumentID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <br><a class=linkBase  onClick="return confirmDelete()"  href = "tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltDocumentManagement.getDocumentID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltDocumentManagement.isExpired("DocumentTypeID",expiredDays))&&(working_bltDocumentManagement.isExpiredCheck("DocumentTypeID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltDocumentManagement.isRequired("DocumentTypeID"))&&(!working_bltDocumentManagement.isComplete("DocumentTypeID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Document Type:&nbsp;</b><jsp:include page="../generic/tDocumentTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltDocumentManagement.getDocumentTypeID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltDocumentManagement.isExpired("DocumentName",expiredDays))&&(working_bltDocumentManagement.isExpiredCheck("DocumentName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltDocumentManagement.isRequired("DocumentName"))&&(!working_bltDocumentManagement.isComplete("DocumentName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Document Name:&nbsp;</b><%=working_bltDocumentManagement.getDocumentName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltDocumentManagement.isExpired("DocumentFileName",expiredDays))&&(working_bltDocumentManagement.isExpiredCheck("DocumentFileName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltDocumentManagement.isRequired("DocumentFileName"))&&(!working_bltDocumentManagement.isComplete("DocumentFileName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>File Name:&nbsp;</b><%=working_bltDocumentManagement.getDocumentFileName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltDocumentManagement.isExpired("DateReceived",expiredDays))&&(working_bltDocumentManagement.isExpiredCheck("DateReceived"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltDocumentManagement.isRequired("DateReceived"))&&(!working_bltDocumentManagement.isComplete("DateReceived")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Date Received:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltDocumentManagement.getDateReceived())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltDocumentManagement.isExpired("DateOfExpiration",expiredDays))&&(working_bltDocumentManagement.isExpiredCheck("DateOfExpiration"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltDocumentManagement.isRequired("DateOfExpiration"))&&(!working_bltDocumentManagement.isComplete("DateOfExpiration")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Expiration Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltDocumentManagement.getDateOfExpiration())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltDocumentManagement.isExpired("DateAlertSent",expiredDays))&&(working_bltDocumentManagement.isExpiredCheck("DateAlertSent"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltDocumentManagement.isRequired("DateAlertSent"))&&(!working_bltDocumentManagement.isComplete("DateAlertSent")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Date Alert Sent:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltDocumentManagement.getDateAlertSent())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltDocumentManagement.isExpired("Archived",expiredDays))&&(working_bltDocumentManagement.isExpiredCheck("Archived"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltDocumentManagement.isRequired("Archived"))&&(!working_bltDocumentManagement.isComplete("Archived")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Archive:&nbsp;</b><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltDocumentManagement.getArchived()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltDocumentManagement.isExpired("Comments",expiredDays))&&(working_bltDocumentManagement.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltDocumentManagement.isRequired("Comments"))&&(!working_bltDocumentManagement.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltDocumentManagement.getComments()%></p>

        </td></tr></table></table><br>        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
