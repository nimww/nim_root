<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tAdminPhysicianLU_main_AdminMaster_PhysicianID_form_authorize.jsp
    Created on Mar/21/2003
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iGenAdminID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("company1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iGenAdminID")) 
    {
        iGenAdminID        =    (Integer)pageControllerHash.get("iGenAdminID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        Integer request2ID = null;
        Integer request3ID = null;
        String sEDIT = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }
        if (request.getParameter("EDIT2ID")!=null)
        {
            request2ID = new Integer(request.getParameter("EDIT2ID"));
            out.println(request2ID);
        }
        if (request.getParameter("EDIT3ID")!=null)
        {
            request3ID = new Integer(request.getParameter("EDIT3ID"));
            out.println(request3ID);
        }
        if (request.getParameter("EDIT")!=null)
        {
            sEDIT = (request.getParameter("EDIT"));
            out.println(requestID);
        }
		pageControllerHash.remove("iAdminID");
		pageControllerHash.remove("iPracticeID");
    if (true)
    {
		if (sEDIT.equalsIgnoreCase("owner"))
		{
			pageControllerHash.put("iAdminID",requestID);
			session.setAttribute("pageControllerHash",pageControllerHash);
			//Parameter Pass Code here
			String parameterPassString ="";
			java.util.Enumeration myParameterPassList = request.getParameterNames();
			while (myParameterPassList.hasMoreElements())
			{
				String myName = (String)myParameterPassList.nextElement();
				String myS = (String) request.getParameter(myName);
				parameterPassString+="&"+myName + "=" + myS;
			}
			response.sendRedirect("pro-file-manager_Status.jsp?nullParam=null"+parameterPassString);
		}
		else if (sEDIT.equalsIgnoreCase("practice"))
		{
			pageControllerHash.put("iAdminID",request2ID);
			pageControllerHash.put("iPracticeID",requestID);
			session.setAttribute("pageControllerHash",pageControllerHash);
			//Parameter Pass Code here
			String parameterPassString ="";
			java.util.Enumeration myParameterPassList = request.getParameterNames();
			while (myParameterPassList.hasMoreElements())
			{
				String myName = (String)myParameterPassList.nextElement();
				String myS = (String) request.getParameter(myName);
				parameterPassString+="&"+myName + "=" + myS;
			}
			response.sendRedirect("practice_Status.jsp?nullParam=null"+parameterPassString);
		}
		else if (sEDIT.equalsIgnoreCase("activity_admin"))
		{
			pageControllerHash.put("iAdminID",request2ID);
			pageControllerHash.put("iPracticeActivityID",request3ID);
			session.setAttribute("pageControllerHash",pageControllerHash);
			//Parameter Pass Code here
			String parameterPassString ="";
			java.util.Enumeration myParameterPassList = request.getParameterNames();
			while (myParameterPassList.hasMoreElements())
			{
				String myName = (String)myParameterPassList.nextElement();
				String myS = (String) request.getParameter(myName);
				parameterPassString+="&"+myName + "=" + myS;
			}
			response.sendRedirect("tPracticeActivity_main_PracticeActivity_AdminID_form_authorize.jsp?&KM=p&EDIT=edit&EDITID=" + request3ID);
		}
		else if (sEDIT.equalsIgnoreCase("activity_practice"))
		{
			pageControllerHash.put("iPracticeID",requestID);
			pageControllerHash.put("iAdminID",request2ID);
			pageControllerHash.put("iPracticeActivityID",request3ID);
			session.setAttribute("pageControllerHash",pageControllerHash);
			//Parameter Pass Code here
			String parameterPassString ="";
			java.util.Enumeration myParameterPassList = request.getParameterNames();
			while (myParameterPassList.hasMoreElements())
			{
				String myName = (String)myParameterPassList.nextElement();
				String myS = (String) request.getParameter(myName);
				parameterPassString+="&"+myName + "=" + myS;
			}
			response.sendRedirect("tPracticeActivity_main_PracticeActivity_PracticeID_form_authorize.jsp?&KM=p&EDIT=edit&EDITID=" + request3ID);
		}
    }
    else
    {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORInvalidQuery")+"</p>");
    }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>


