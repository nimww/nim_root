<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltProfessionalEducation,com.winstaff.bltProfessionalEducation_List,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: tProfessionalEducation_main_ProfessionalEducation_PhysicianID.jsp
    Created on Nov/12/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

    <table cellpadding=0 cellspacing=0 border=0 width=800 >
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection6", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }

  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tProfessionalEducation_main_ProfessionalEducation_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iProfessionalEducationID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltProfessionalEducation_List        bltProfessionalEducation_List        =    new    bltProfessionalEducation_List(iPhysicianID);

//declaration of Enumeration
    bltProfessionalEducation        ProfessionalEducation;
    ListElement         leCurrentElement;
    Enumeration eList = bltProfessionalEducation_List.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tProfessionalEducation"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        ProfessionalEducation  = (bltProfessionalEducation) leCurrentElement.getObject();
      }
      else
      {
        ProfessionalEducation  = new bltProfessionalEducation();
        ProfessionalEducation.setPhysicianID(iPhysicianID);
        isNewRecord= true;
      }
        ProfessionalEducation.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !ProfessionalEducation.getUniqueCreateDate().equals(testObj)  )
    {
         ProfessionalEducation.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !ProfessionalEducation.getUniqueModifyDate().equals(testObj)  )
    {
         ProfessionalEducation.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !ProfessionalEducation.getUniqueModifyComments().equals(testObj)  )
    {
         ProfessionalEducation.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID_"+iExpress)) ;
    if ( !ProfessionalEducation.getPhysicianID().equals(testObj)  )
    {
         ProfessionalEducation.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DegreeID_"+iExpress)) ;
    if ( !ProfessionalEducation.getDegreeID().equals(testObj)  )
    {
         ProfessionalEducation.setDegreeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation DegreeID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Other_"+iExpress)) ;
    if ( !ProfessionalEducation.getOther().equals(testObj)  )
    {
         ProfessionalEducation.setOther( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation Other not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("StartDate_"+iExpress))) ;
    if ( !ProfessionalEducation.getStartDate().equals(testObj)  )
    {
         ProfessionalEducation.setStartDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation StartDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateOfGraduation_"+iExpress))) ;
    if ( !ProfessionalEducation.getDateOfGraduation().equals(testObj)  )
    {
         ProfessionalEducation.setDateOfGraduation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation DateOfGraduation not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("EndDate_"+iExpress))) ;
    if ( !ProfessionalEducation.getEndDate().equals(testObj)  )
    {
         ProfessionalEducation.setEndDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation EndDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Focus_"+iExpress)) ;
    if ( !ProfessionalEducation.getFocus().equals(testObj)  )
    {
         ProfessionalEducation.setFocus( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation Focus not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolName_"+iExpress)) ;
    if ( !ProfessionalEducation.getSchoolName().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolAddress1_"+iExpress)) ;
    if ( !ProfessionalEducation.getSchoolAddress1().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolAddress2_"+iExpress)) ;
    if ( !ProfessionalEducation.getSchoolAddress2().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolCity_"+iExpress)) ;
    if ( !ProfessionalEducation.getSchoolCity().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SchoolStateID_"+iExpress)) ;
    if ( !ProfessionalEducation.getSchoolStateID().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolProvince_"+iExpress)) ;
    if ( !ProfessionalEducation.getSchoolProvince().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolProvince not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolZIP_"+iExpress)) ;
    if ( !ProfessionalEducation.getSchoolZIP().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SchoolCountryID_"+iExpress)) ;
    if ( !ProfessionalEducation.getSchoolCountryID().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolCountryID( testObj,UserSecurityGroupID   );
         //testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolCountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolPhone_"+iExpress)) ;
    if ( !ProfessionalEducation.getSchoolPhone().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SchoolFax_"+iExpress)) ;
    if ( !ProfessionalEducation.getSchoolFax().equals(testObj)  )
    {
         ProfessionalEducation.setSchoolFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation SchoolFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName_"+iExpress)) ;
    if ( !ProfessionalEducation.getContactName().equals(testObj)  )
    {
         ProfessionalEducation.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail_"+iExpress)) ;
    if ( !ProfessionalEducation.getContactEmail().equals(testObj)  )
    {
         ProfessionalEducation.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !ProfessionalEducation.getComments().equals(testObj)  )
    {
         ProfessionalEducation.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("ProfessionalEducation Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tProfessionalEducation","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	ProfessionalEducation.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    ProfessionalEducation.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
	        ProfessionalEducation.commitData();
	        if (!ProfessionalEducation.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }
    }
}       }
    }//while

if (errorRouteMeTotal)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else 
{
String nextPage = request.getParameter("nextPage");
if (nextPage==null||nextPage.equalsIgnoreCase(""))
{
	nextPage = ConfigurationMessages.getExpressLinkRaw("tProfessionalEducation","next",iExpressMode);
}
if (!isAllComplete)
{
	%>
	<script language=Javascript>
	      if ( confirm("<%=ConfigurationMessages.getMessage("ConfirmReqFieldsReturn")%>") )
	      {
	          document.location="<%=ConfigurationMessages.getDataCategoryLink("tProfessionalEducation","Express")%>";
	      }
	      else 
	      {
	          document.location="<%=nextPage%>";
	      }
	</script>
  <%
}
else
{
	%>
	<script language=Javascript>
	      document.location="<%=nextPage%>";
	</script>
  <%
}
}
       
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
