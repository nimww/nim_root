<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltLicenseRegistration" %>
<%/*

    filename: out\jsp\tLicenseRegistration_form.jsp
    Created on Aug/26/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
  <tr>
    <td width=10>&nbsp;</td>
    <td> <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tLicenseRegistration")%> 
      <%
//initial declaration of list class and parentID
    Integer        iLicenseRegistrationID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection3", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iLicenseRegistrationID")) 
    {
        iLicenseRegistrationID        =    (Integer)pageControllerHash.get("iLicenseRegistrationID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tLicenseRegistration_form.jsp");
//initial declaration of list class and parentID

    bltLicenseRegistration        LicenseRegistration        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        LicenseRegistration        =    new    bltLicenseRegistration(iLicenseRegistrationID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        LicenseRegistration        =    new    bltLicenseRegistration(UserSecurityGroupID, true);
    }

//fields
        %>
      <%@ include file="tLicenseRegistration_form_instructions.jsp" %>
      <form action="tLicenseRegistration_form_sub.jsp" name="tLicenseRegistration_form1" method="POST">
        <%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >
        <%
    }
%>
        <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
          <tr>
            <td class=tableColor> 
              <table cellpadding=0 cellspacing=0 width=100%>
                <%
            if ( (LicenseRegistration.isRequired("IsCurrent",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("IsCurrent")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("IsCurrent",expiredDays))&&(LicenseRegistration.isExpiredCheck("IsCurrent",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("IsCurrent",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Is this a current license?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="IsCurrent" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getIsCurrent()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsCurrent&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("IsCurrent")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("IsCurrent",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Is this a current license?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getIsCurrent()%>" />
                      </jsp:include>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsCurrent&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("IsCurrent")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("LicenseType",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("LicenseType")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("LicenseType",expiredDays))&&(LicenseRegistration.isExpiredCheck("LicenseType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("LicenseType",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Type&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="LicenseType" >
                        <jsp:include page="../generic/tLicenseTypeLIShort.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getLicenseType()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LicenseType&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("LicenseType")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("LicenseType",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Type&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tLicenseTypeLIShort_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getLicenseType()%>" />
                      </jsp:include>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LicenseType&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("LicenseType")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("SubType",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("SubType")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("SubType",expiredDays))&&(LicenseRegistration.isExpiredCheck("SubType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("SubType",UserSecurityGroupID)))
            {
                        %>
                <tr class=instructions> 
                  <td colspan=2> <br>
                    For DEA, indicate DEA-schedule; Otherwise please indicate 
                    sub-type or description such as: <b>IVID</b>, <b>DO</b>,<b> 
                    DDS</b>, <b>SOCIAL</b> <b>WORKER</b>, <b>RN</b>, etc.</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Description (schedule/sub-type)</b></p>
                  </td>
                  <td valign=top>
                    <p> 
                      <input onKeyDown="textAreaStop(this,90)" maxlength="100" type=text size="40" name="SubType" value="<%=LicenseRegistration.getSubType()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SubType&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("SubType")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%> Quick Add:
					  <select name=quickSubType onChange="javascript:document.forms[0].SubType.value = document.forms[0].quickSubType.value;textAreaStop(document.forms[0].SubType,90)">
<option value="">DEA Schedule</option>
<option value=",,,,,,,">,,,,,,,</option>
<option value=",,,,,,,5">,,,,,,,5</option>
<option value=",,,,,,4,5">,,,,,,4,5</option>
<option value=",,,,,3N,,">,,,,,3N,,</option>
<option value=",,,,3,,,">,,,,3,,,</option>
<option value=",,,,3,3N,4,5">,,,,3,3N,4,5</option>
<option value=",,,2N,,3N,,">,,,2N,,3N,,</option>
<option value=",,,2N,,3N,4,">,,,2N,,3N,4,</option>
<option value=",,,2N,,3N,4,5">,,,2N,,3N,4,5</option>
<option value=",,,2N,3,,4,5">,,,2N,3,,4,5</option>
<option value=",,,2N,3,3N,4,5">,,,2N,3,3N,4,5</option>
<option value=",,2,,,,,">,,2,,,,,</option>
<option value=",,2,,,,4,">,,2,,,,4,</option>
<option value=",,2,,,,4,5">,,2,,,,4,5</option>
<option value=",,2,,3,,,">,,2,,3,,,</option>
<option value=",,2,,3,,4,">,,2,,3,,4,</option>
<option value=",,2,,3,,4,5">,,2,,3,,4,5</option>
<option value=",,2,,3,3N,4,5">,,2,,3,3N,4,5</option>
<option value=",,2,2N,,,,">,,2,2N,,,,</option>
<option value=",,2,2N,,,,5">,,2,2N,,,,5</option>
<option value=",,2,2N,,,4,5">,,2,2N,,,4,5</option>
<option value=",,2,2N,,3N,,">,,2,2N,,3N,,</option>
<option value=",,2,2N,,3N,4,">,,2,2N,,3N,4,</option>
<option value=",,2,2N,,3N,4,5">,,2,2N,,3N,4,5</option>
<option value=",,2,2N,3,,,">,,2,2N,3,,,</option>
<option value=",,2,2N,3,,4,5">,,2,2N,3,,4,5</option>
<option value=",,2,2N,3,3N,,">,,2,2N,3,3N,,</option>
<option value=",,2,2N,3,3N,,5">,,2,2N,3,3N,,5</option>
<option value=",,2,2N,3,3N,4,">,,2,2N,3,3N,4,</option>
<option value=",,2,2N,3,3N,4,5">,,2,2N,3,3N,4,5</option>
					  </select>
                    </p>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("SubType",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Description (schedule/sub-type)</b></p>
                  </td>
                  <td valign=top>
                    <p><%=LicenseRegistration.getSubType()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SubType&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("SubType")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("StateID",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("StateID")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("StateID",expiredDays))&&(LicenseRegistration.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("StateID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="StateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getStateID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("StateID")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("StateID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getStateID()%>" />
                      </jsp:include>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("StateID")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
						
						
                <%
            }
            %>
			

            <%
            if ( (LicenseRegistration.isRequired("NPDBFOL",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("NPDBFOL")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("NPDBFOL",expiredDays))&&(LicenseRegistration.isExpiredCheck("NPDBFOL",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("NPDBFOL",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Field of Licensure&nbsp;</b></p></td><td valign=top><p><select   name="NPDBFOL" ><jsp:include page="../generic/tNPDBFOLTypeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getNPDBFOL()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NPDBFOL&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("NPDBFOL")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("NPDBFOL",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Field of Licensure&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tNPDBFOLTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getNPDBFOL()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NPDBFOL&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("NPDBFOL")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

			
			
			
                <%
            if ( (LicenseRegistration.isRequired("IsPractice",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("IsPractice")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("IsPractice",expiredDays))&&(LicenseRegistration.isExpiredCheck("IsPractice",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("IsPractice",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you currently practice in this 
                      state?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="IsPractice" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getIsPractice()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsPractice&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("IsPractice")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("IsPractice",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Do you currently practice in this 
                      state?&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getIsPractice()%>" />
                      </jsp:include>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsPractice&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("IsPractice")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("LicenseDocumentNumber",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("LicenseDocumentNumber")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("LicenseDocumentNumber",expiredDays))&&(LicenseRegistration.isExpiredCheck("LicenseDocumentNumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("LicenseDocumentNumber",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Document Number&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="60" type=text size="80" name="LicenseDocumentNumber" value="<%=LicenseRegistration.getLicenseDocumentNumber()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LicenseDocumentNumber&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("LicenseDocumentNumber")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("LicenseDocumentNumber",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Document Number&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=LicenseRegistration.getLicenseDocumentNumber()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LicenseDocumentNumber&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("LicenseDocumentNumber")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("IssueDate",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("IssueDate")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("IssueDate",expiredDays))&&(LicenseRegistration.isExpiredCheck("IssueDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("IssueDate",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Issue Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength=20 type=text size="80" name="IssueDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(LicenseRegistration.getIssueDate())%>" /></jsp:include>' >
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IssueDate&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("IssueDate")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("IssueDate",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Issue Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(LicenseRegistration.getIssueDate())%>" />
                      </jsp:include>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IssueDate&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("IssueDate")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("ExpirationDate",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("ExpirationDate")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("ExpirationDate",expiredDays))&&(LicenseRegistration.isExpiredCheck("ExpirationDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("ExpirationDate",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength=20 type=text size="80" name="ExpirationDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(LicenseRegistration.getExpirationDate())%>" /></jsp:include>' >
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpirationDate&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ExpirationDate")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("ExpirationDate",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(LicenseRegistration.getExpirationDate())%>" />
                      </jsp:include>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpirationDate&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ExpirationDate")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("Name",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Name")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("Name",expiredDays))&&(LicenseRegistration.isExpiredCheck("Name",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("Name",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Licensing Organization&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="100" type=text size="80" name="Name" value="<%=LicenseRegistration.getName()%>">
                      &nbsp;<a href="#" onClick="window.open('LI_tLicenseBoardLI_Search.jsp?OFieldName=Name','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Name")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("Name",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Licensing Organization&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=LicenseRegistration.getName()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Name")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("Address1",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Address1")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("Address1",expiredDays))&&(LicenseRegistration.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("Address1",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="Address1" value="<%=LicenseRegistration.getAddress1()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Address1")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("Address1",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=LicenseRegistration.getAddress1()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Address1")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("Address2",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Address2")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("Address2",expiredDays))&&(LicenseRegistration.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("Address2",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="20" type=text size="80" name="Address2" value="<%=LicenseRegistration.getAddress2()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Address2")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("Address2",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=LicenseRegistration.getAddress2()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Address2")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("City",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("City")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("City",expiredDays))&&(LicenseRegistration.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("City",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Institution City&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="30" type=text size="80" name="City" value="<%=LicenseRegistration.getCity()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("City")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("City",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Institution City&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=LicenseRegistration.getCity()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("City")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("OrgStateID",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("OrgStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("OrgStateID",expiredDays))&&(LicenseRegistration.isExpiredCheck("OrgStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("OrgStateID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="OrgStateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getOrgStateID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OrgStateID&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("OrgStateID")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("OrgStateID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getOrgStateID()%>" />
                      </jsp:include>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OrgStateID&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("OrgStateID")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("Province",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Province")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("Province",expiredDays))&&(LicenseRegistration.isExpiredCheck("Province",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("Province",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Non-US Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="100" type=text size="80" name="Province" value="<%=LicenseRegistration.getProvince()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Province")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("Province",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Non-US Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=LicenseRegistration.getProvince()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Province")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("ZIP",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("ZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("ZIP",expiredDays))&&(LicenseRegistration.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("ZIP",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="ZIP" value="<%=LicenseRegistration.getZIP()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ZIP")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("ZIP",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=LicenseRegistration.getZIP()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ZIP")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("CountryID",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("CountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("CountryID",expiredDays))&&(LicenseRegistration.isExpiredCheck("CountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("CountryID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select   name="CountryID" >
                        <jsp:include page="../generic/tCountryLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getCountryID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("CountryID")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("CountryID",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getCountryID()%>" />
                      </jsp:include>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("CountryID")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("Phone",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Phone")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("Phone",expiredDays))&&(LicenseRegistration.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("Phone",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="Phone" value="<%=LicenseRegistration.getPhone()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Phone")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("Phone",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=LicenseRegistration.getPhone()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Phone")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("Fax",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Fax")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("Fax",expiredDays))&&(LicenseRegistration.isExpiredCheck("Fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("Fax",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="Fax" value="<%=LicenseRegistration.getFax()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Fax")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("Fax",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=LicenseRegistration.getFax()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Fax")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("ContactName",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("ContactName")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("ContactName",expiredDays))&&(LicenseRegistration.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("ContactName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="50" type=text size="80" name="ContactName" value="<%=LicenseRegistration.getContactName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ContactName")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("ContactName",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=LicenseRegistration.getContactName()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ContactName")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("ContactEmail",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("ContactEmail",expiredDays))&&(LicenseRegistration.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input maxlength="75" type=text size="80" name="ContactEmail" value="<%=LicenseRegistration.getContactEmail()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=LicenseRegistration.getContactEmail()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("DocuLinkID",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("DocuLinkID")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("DocuLinkID",expiredDays))&&(LicenseRegistration.isExpiredCheck("DocuLinkID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("DocuLinkID",UserSecurityGroupID)))
            {
                        if (LicenseRegistration.getDocuLinkID().intValue()>0)
                        {
                            bltDocumentManagement myDoc = new bltDocumentManagement(LicenseRegistration.getDocuLinkID());
                            if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                            {
                            %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Attached Document:&nbsp;</p>
                  </td>
                  <td valign=top>
                    <p><a href="pdf/<%=myDoc.getDocumentFileName()%>">view</a></b></p>
                  </td>
                </tr>
                <%
                           }
                            else
                            {
                            %>
                <%
                            }
                        }
            }
            else if ((LicenseRegistration.isRead("DocuLinkID",UserSecurityGroupID)))
            {
                        bltDocumentManagement myDoc = new bltDocumentManagement(LicenseRegistration.getDocuLinkID());
                        if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                        {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Attached Document:&nbsp;</p>
                  </td>
                  <td valign=top>
                    <p><a href="pdf/<%=myDoc.getDocumentFileName()%>">view</a></b></p>
                  </td>
                </tr>
                <%
                        }
                        else
                        {
                        %>
                <%
                        }
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (LicenseRegistration.isRequired("Comments",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((LicenseRegistration.isExpired("Comments",expiredDays))&&(LicenseRegistration.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((LicenseRegistration.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b><%=LicenseRegistration.getEnglish("Comments")%>&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=LicenseRegistration.getComments()%></textarea>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Comments")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((LicenseRegistration.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=LicenseRegistration.getEnglish("Comments")%>&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p><%=LicenseRegistration.getComments()%>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Comments")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr>
                  <td width=40%>&nbsp;</td>
                  <td width=60%>&nbsp;</td>
                </tr>
              </table>
              <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
              <input type=hidden name=routePageReference value="sParentReturnPage">
              <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
              <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                <tr class=requiredField>
                  <td> 
                    <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>
                    &nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tLicenseRegistration")%> 
                    <br>
                    <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">
                    &nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tLicenseRegistration")%> 
                  </td>
                </tr>
              </table>
              <br>
              <br>
              <br>
              <%
              }
              %>
              <p>
                <input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit>
              </p>
              <%}%>
            </td>
          </tr>
        </table>
      </form>
      <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
    </td>
  </tr>
</table>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" >
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
