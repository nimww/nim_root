<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltFacilityAffiliation,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tFacilityAffiliation_form_sub.jsp
    Created on Sep/11/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iAffiliationID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection10", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iAffiliationID")) 
    {
        iAffiliationID        =    (Integer)pageControllerHash.get("iAffiliationID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltFacilityAffiliation        FacilityAffiliation        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        FacilityAffiliation        =    new    bltFacilityAffiliation(iAffiliationID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        FacilityAffiliation        =    new    bltFacilityAffiliation(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !FacilityAffiliation.getUniqueCreateDate().equals(testObj)  )
    {
         FacilityAffiliation.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !FacilityAffiliation.getUniqueModifyDate().equals(testObj)  )
    {
         FacilityAffiliation.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !FacilityAffiliation.getUniqueModifyComments().equals(testObj)  )
    {
         FacilityAffiliation.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID")) ;
    if ( !FacilityAffiliation.getPhysicianID().equals(testObj)  )
    {
         FacilityAffiliation.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsPrimary")) ;
    if ( !FacilityAffiliation.getIsPrimary().equals(testObj)  )
    {
         FacilityAffiliation.setIsPrimary( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation IsPrimary not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AppointmentLevel")) ;
    if ( !FacilityAffiliation.getAppointmentLevel().equals(testObj)  )
    {
         FacilityAffiliation.setAppointmentLevel( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation AppointmentLevel not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("StartDate"))) ;
    if ( !FacilityAffiliation.getStartDate().equals(testObj)  )
    {
         FacilityAffiliation.setStartDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation StartDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("EndDate"))) ;
    if ( !FacilityAffiliation.getEndDate().equals(testObj)  )
    {
         FacilityAffiliation.setEndDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation EndDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("PendingDate"))) ;
    if ( !FacilityAffiliation.getPendingDate().equals(testObj)  )
    {
         FacilityAffiliation.setPendingDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation PendingDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityName")) ;
    if ( !FacilityAffiliation.getFacilityName().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityDepartment")) ;
    if ( !FacilityAffiliation.getFacilityDepartment().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityDepartment( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityDepartment not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityAddress1")) ;
    if ( !FacilityAffiliation.getFacilityAddress1().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityAddress2")) ;
    if ( !FacilityAffiliation.getFacilityAddress2().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityCity")) ;
    if ( !FacilityAffiliation.getFacilityCity().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("FacilityStateID")) ;
    if ( !FacilityAffiliation.getFacilityStateID().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityProvince")) ;
    if ( !FacilityAffiliation.getFacilityProvince().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityProvince not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityZIP")) ;
    if ( !FacilityAffiliation.getFacilityZIP().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("FacilityCountryID")) ;
    if ( !FacilityAffiliation.getFacilityCountryID().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityCountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityPhone")) ;
    if ( !FacilityAffiliation.getFacilityPhone().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityFax")) ;
    if ( !FacilityAffiliation.getFacilityFax().equals(testObj)  )
    {
         FacilityAffiliation.setFacilityFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation FacilityFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName")) ;
    if ( !FacilityAffiliation.getContactName().equals(testObj)  )
    {
         FacilityAffiliation.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !FacilityAffiliation.getContactEmail().equals(testObj)  )
    {
         FacilityAffiliation.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReasonForLeaving")) ;
    if ( !FacilityAffiliation.getReasonForLeaving().equals(testObj)  )
    {
         FacilityAffiliation.setReasonForLeaving( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation ReasonForLeaving not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AdmissionPriviledges")) ;
    if ( !FacilityAffiliation.getAdmissionPriviledges().equals(testObj)  )
    {
         FacilityAffiliation.setAdmissionPriviledges( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation AdmissionPriviledges not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AdmissionArrangements")) ;
    if ( !FacilityAffiliation.getAdmissionArrangements().equals(testObj)  )
    {
         FacilityAffiliation.setAdmissionArrangements( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation AdmissionArrangements not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("UnrestrictedAdmission")) ;
    if ( !FacilityAffiliation.getUnrestrictedAdmission().equals(testObj)  )
    {
         FacilityAffiliation.setUnrestrictedAdmission( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation UnrestrictedAdmission not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TempPriviledges")) ;
    if ( !FacilityAffiliation.getTempPriviledges().equals(testObj)  )
    {
         FacilityAffiliation.setTempPriviledges( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation TempPriviledges not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("InpatientCare")) ;
    if ( !FacilityAffiliation.getInpatientCare().equals(testObj)  )
    {
         FacilityAffiliation.setInpatientCare( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation InpatientCare not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PercentAdmissions")) ;
    if ( !FacilityAffiliation.getPercentAdmissions().equals(testObj)  )
    {
         FacilityAffiliation.setPercentAdmissions( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation PercentAdmissions not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !FacilityAffiliation.getComments().equals(testObj)  )
    {
         FacilityAffiliation.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("FacilityAffiliation Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tFacilityAffiliation","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	FacilityAffiliation.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    FacilityAffiliation.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        FacilityAffiliation.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
