<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tPracticeActivity_main_PracticeActivity_PracticeID_form_create.jsp
    Created on Oct/30/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
        }

//declaration of Enumeration
    bltPracticeActivity        working_bltPracticeActivity = new bltPracticeActivity();
    working_bltPracticeActivity.setPracticeID(iPracticeID);
    working_bltPracticeActivity.setUniqueCreateDate(new java.util.Date());
    working_bltPracticeActivity.setUniqueModifyDate(new java.util.Date());
    working_bltPracticeActivity.setAssignedToID(CurrentUserAccount.getUserID());
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltPracticeActivity.setUniqueModifyComments(""+UserLogonDescription);
    }

            working_bltPracticeActivity.commitData();
        {
            {
                pageControllerHash.put("iPracticeActivityID",working_bltPracticeActivity.getUniqueID());
                pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
                session.setAttribute("pageControllerHash",pageControllerHash);
                //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
                String targetRedirect = "tPracticeActivity_form.jsp?nullParam=null"+parameterPassString    ;

                {
                    targetRedirect = "tPracticeActivity_form.jsp?EDITID="+working_bltPracticeActivity.getUniqueID()+"&routePageReference=sParentReturnPage&EDIT=edit"+parameterPassString    ;
                }
                response.sendRedirect(targetRedirect);
            }
        }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>





