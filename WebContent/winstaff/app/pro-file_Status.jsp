

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tPhysicianMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>
<%@ include file="../generic/CheckLogin.jsp" %>
<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;    
   }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","pro-file_Status.jsp");
    pageControllerHash.put("sParentReturnPage","pro-file_Status.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

//stepVerify_PhysicianID mySV = new stepVerify_PhysicianID();
//mySV.setPhysicianID(iPhysicianID);
//mySV.setSecurityID(UserSecurityGroupID);
bltPhysicianMaster        PhysicianMaster        =   new  bltPhysicianMaster (iPhysicianID);

//verify data
boolean ApplicationDataCore = false;
boolean ApplicationDataExtra = false;
/*
if (mySV.isAllCore())
{
	ApplicationDataCore = true;
}
if (mySV.isAllExtra())
{
	ApplicationDataExtra = true;
}
*/
boolean ApplicationDataAll = (ApplicationDataExtra&&ApplicationDataCore);
//verify documents are complete:


boolean SupportingDocuments = true;
{
    bltDocumentManagement_List        bltDocumentManagement_List        =    new    bltDocumentManagement_List(iPhysicianID,"Archived=2","");
	//declaration of Enumeration
    bltDocumentManagement        working_bltDocumentManagement;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltDocumentManagement_List.elements();
    while (eList.hasMoreElements()&&SupportingDocuments)
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltDocumentManagement  = (bltDocumentManagement) leCurrentElement.getObject();
		if (!working_bltDocumentManagement.isComplete())
		{
			SupportingDocuments = false;
		}
	}
}
	//check Attestation Account Status
	boolean AttestationAccount = false;
	boolean AttestationAccountCreated = false;
{
/*
bltPhysicianUserAccountLU_List_LU_PhysicianID        bltPhysicianUserAccountLU_List_LU_PhysicianID        =    new    bltPhysicianUserAccountLU_List_LU_PhysicianID(iPhysicianID);
	//declaration of Enumeration
    bltPhysicianUserAccountLU        working_bltPhysicianUserAccountLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPhysicianUserAccountLU_List_LU_PhysicianID.elements();
    if (eList.hasMoreElements())
    {        
 	     AttestationAccountCreated = true;
         leCurrentElement    = (ListElement) eList.nextElement();
         working_bltPhysicianUserAccountLU  = (bltPhysicianUserAccountLU) leCurrentElement.getObject();
         bltUserAccount working_bltUserAccount  = new bltUserAccount(working_bltPhysicianUserAccountLU.getUserID());
		 if (working_bltUserAccount.getStatus().intValue()==2)
		 {
	 	     AttestationAccount = true;
		 }
    }
*/
}
//check attest status
boolean Attestation = false;
if (PhysicianMaster.getIsAttested().intValue()==1)
{
	Attestation = true;
}


//check HCO Auth Status
boolean HCOAuth = false;
/*
{
    bltHCOPhysicianLU_List_LU_PhysicianID        bltHCOPhysicianLU_List_LU_PhysicianID        =    new    bltHCOPhysicianLU_List_LU_PhysicianID(iPhysicianID);

//declaration of Enumeration
    bltHCOPhysicianLU        working_bltHCOPhysicianLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltHCOPhysicianLU_List_LU_PhysicianID.elements();
    if (eList.hasMoreElements())
    {
         HCOAuth = true;
    }

}
*/




%>
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <table width="100%%" border="0" cellspacing="0" cellpadding="5">
        <tr class=tdHeaderAlt> 
          <td class=title>Practitioner Home</td>
        </tr>
        <tr class=tdBase>
          <td class=title>
            <table border="0" cellpadding="0" cellspacing="0" width="650">
              <!-- fwtable fwsrc="dataprocess1.png" fwbase="dataprocess-nav.gif" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
              <tr> 
                <td><img src="images/dataprocess/spacer.gif" width="6" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="70" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="8" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="139" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="15" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="77" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="6" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="121" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="2" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="121" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="6" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="62" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="8" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="8" height="1" border="0" name="undefined_3"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="1" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td colspan="15"><img name="dataprocessnav_r1_c1_2" src="images/dataprocess/dataprocess-nav_r1_c1.gif" width="650" height="13" border="0"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="13" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td rowspan="2" colspan="3"><img name="dataprocessnav_r2_c1_2" src="images/dataprocess/dataprocess-nav_r2_c1.gif" width="84" height="49" border="0"></td>
          <%
if (ApplicationDataCore)
{
%>
                <td colspan="2"><a href="pro-file_Status_Express.jsp"><img name="dataprocessnav_r2_c4_2" src="images/dataprocess/dataprocess-nav_r2_c4_f3.gif" width="140" height="43" border="0"></a></td>
          <%
}
else
{
%>
                <td colspan="2"><a href="pro-file_Status_Express.jsp"><img name="dataprocessnav_r2_c4_2" src="images/dataprocess/dataprocess-nav_r2_c4.gif" width="140" height="43" border="0"></a></td>
          <%
}
%>
                <td rowspan="6"><img name="dataprocessnav_r2_c6_2" src="images/dataprocess/dataprocess-nav_r2_c6.gif" width="15" height="187" border="0"></td>
                <td rowspan="2" colspan="8"><a href="pro-file_Status_Express.jsp"><img name="dataprocessnav_r2_c7" src="images/dataprocess/dataprocess-nav_r2_c7.gif" width="403" height="49" border="0"></a></td>
                <td rowspan="6"><img name="dataprocessnav_r2_c15" src="images/dataprocess/dataprocess-nav_r2_c15.gif" width="8" height="187" border="0"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="43" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td rowspan="3" colspan="2"><img name="dataprocessnav_r3_c4_2" src="images/dataprocess/dataprocess-nav_r3_c4.gif" width="140" height="83" border="0"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="6" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td rowspan="4"><img name="dataprocessnav_r4_c1_2" src="images/dataprocess/dataprocess-nav_r4_c1.gif" width="6" height="138" border="0"></td>
                <td><a href="pro-file_Status_Express.jsp"><img name="dataprocessnav_r4_c2_2" src="images/dataprocess/dataprocess-nav_r4_c2_f2.gif" width="70" height="72" border="0"></a></td>
                <td rowspan="4"><img name="dataprocessnav_r4_c3_2" src="images/dataprocess/dataprocess-nav_r4_c3.gif" width="8" height="138" border="0"></td>
          <%
if (ApplicationDataExtra)
{
%>
                <td><a href="ExtraInformation_PhysicianID.jsp"><img name="dataprocessnav_r4_c7_2" src="images/dataprocess/dataprocess-nav_r4_c7_f3.gif" width="77" height="72" border="0"></a></td>
          <%
}
else
{
%>
                <td><a href="ExtraInformation_PhysicianID.jsp"><img name="dataprocessnav_r4_c7_2" src="images/dataprocess/dataprocess-nav_r4_c7.gif" width="77" height="72" border="0"></a></td>
          <%
}
%>
                <td><img name="dataprocessnav_r4_c8_2" src="images/dataprocess/dataprocess-nav_r4_c8.gif" width="6" height="72" border="0"></td>
          <%
if (PhysicianMaster.getIsAttested().intValue()==1)
{
%>
                <td><a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp"><img name="dataprocessnav_r4_c9_2" src="images/dataprocess/dataprocess-nav_r4_c9_f3.gif" width="121" height="72" border="0"></a></td>
          <%
}
else
{
%>
                <td><a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp"><img name="dataprocessnav_r4_c9_2" src="images/dataprocess/dataprocess-nav_r4_c9.gif" width="121" height="72" border="0"></a></td>
          <%
}
%>
                <td><img name="dataprocessnav_r4_c10_2" src="images/dataprocess/dataprocess-nav_r4_c10.gif" width="2" height="72" border="0"></td>
          <%
if (SupportingDocuments)
{
%>
                <td><a href="tDocumentManagement_PhysicianID.jsp"><img name="dataprocessnav_r4_c11_2" src="images/dataprocess/dataprocess-nav_r4_c11_f3.gif" width="121" height="72" border="0"></a></td>
          <%
}
else
{
%>
                <td><a href="tDocumentManagement_PhysicianID.jsp"><img name="dataprocessnav_r4_c11_2" src="images/dataprocess/dataprocess-nav_r4_c11.gif" width="121" height="72" border="0"></a></td>
          <%
}
%>
                <td><img name="dataprocessnav_r4_c12_2" src="images/dataprocess/dataprocess-nav_r4_c12.gif" width="6" height="72" border="0"></td>
                <td colspan="2"><a href="FinishedCheckList_PhysicianID.jsp"><img name="dataprocessnav_r4_c13_2" src="images/dataprocess/dataprocess-nav_r4_c13.gif" width="70" height="72" border="0"></a></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="72" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td rowspan="3"><img name="dataprocessnav_r5_c2_2" src="images/dataprocess/dataprocess-nav_r5_c2.gif" width="70" height="66" border="0"></td>
                <td rowspan="2" colspan="7"><img name="dataprocessnav_r5_c7_2" src="images/dataprocess/dataprocess-nav_r5_c7.gif" width="395" height="48" border="0"></td>
                <td rowspan="3"><img name="dataprocessnav_r5_c14" src="images/dataprocess/dataprocess-nav_r5_c14.gif" width="8" height="66" border="0"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="5" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td rowspan="2"><img name="dataprocessnav_r6_c4_2" src="images/dataprocess/dataprocess-nav_r6_c4.gif" width="1" height="61" border="0"></td>
          <%
if (ApplicationDataCore)
{
%>
                <td><a href="pro-file_Status_Advanced.jsp"><img name="dataprocessnav_r6_c5_2" src="images/dataprocess/dataprocess-nav_r6_c5_f3.gif" width="139" height="43" border="0"></a></td>
          <%
}
else
{
%>
                <td><a href="pro-file_Status_Advanced.jsp"><img name="dataprocessnav_r6_c5_2" src="images/dataprocess/dataprocess-nav_r6_c5.gif" width="139" height="43" border="0"></a></td>
          <%
}
%>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="43" border="0" name="undefined_3"></td>
              </tr>
              <tr> 
                <td><img name="dataprocessnav_r7_c5_2" src="images/dataprocess/dataprocess-nav_r7_c5.gif" width="139" height="18" border="0"></td>
                <td colspan="7"><img name="dataprocessnav_r7_c7" src="images/dataprocess/dataprocess-nav_r7_c7.gif" width="395" height="18" border="0"></td>
                <td><img src="images/dataprocess/spacer.gif" width="1" height="18" border="0" name="undefined_3"></td>
              </tr>
            </table>
</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <table width="100%%" border="0" cellspacing="0" cellpadding="5" bordercolor="#000000">
        <tr valign="top"> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bordercolor="#333333">
              <tr> 
                <td> 
                  <table width="100%%" border="1" cellspacing="0" cellpadding="5" bordercolor="#999999" class=tdBase>
                    <tr class=tdHeaderAlt> 
                      <td colspan="2">Application Status</td>
                    </tr>
                    <tr valign="top"> 
                      <td>Data Collection </td>
                      <td> 
                        <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=(ApplicationDataAll)%>" />
                        </jsp:include>
                        <br>
                        <%
if (!ApplicationDataAll)
{
%>
                        Please begin the data entry process by clicking on either 
                        <a href="pro-file_Status_Express.jsp">Express Mode</a> 
                        or <a href="pro-file_Status_Advanced.jsp">Advanced Mode</a>.<br>
                        <br>
                        If you have already gone through the data entry process 
                        and just need to see what data is still incomplete, please 
                        <a href="FinishedCheckList_PhysicianID.jsp">click here</a> 
                        to review your application.<br>
                        <%
}
%>
                      </td>
                    </tr>
                    <tr valign="top"> 
                      <td>Application Signed</td>
                      <td> 
                        <%
int anyAttest = 2;
if (true||PhysicianMaster.getIsAttested().intValue()==1)
{
%>
                        Yes<br>
                        <%
}
else if (PhysicianMaster.getIsAttestedPending().intValue()==1)
{
%>
                        Pending*<br>
                        <br>
                        <span class=instructions>*You still need to fax in your 
                        release form - <a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp">click 
                        here</a> to print it.</span> 
                        <%
}
else if (PhysicianMaster.getAttestDate().after(dbdf.parse("1810-01-01") ) )
{
%>
                        Not Signed<br>
                        You data has been updated, you need to sign your application 
                        again <a href="pro-file_Attestation.jsp">here</a> 
                        <%
}
else
{
%>
                        Not Signed<br>
                        You must complete your application then sign it <a href="pro-file_Attestation.jsp">here</a> 
                        <%
}

%>
                      </td>
                    </tr>
                    <tr> 
                      <td> Documents Attached<br>
                      </td>
                      <td> 
                        <jsp:include page="../generic/TFCompleted_translate.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=SupportingDocuments%>" />
                        </jsp:include>
                        &nbsp;<br>
                        <%
if (!ApplicationDataAll)
{
%>
                        Please complete the data entry process before sending 
                        in your documents. 
                        <%
}
else
{
%>
                        Please <a href="tDocumentManagement_PhysicianID.jsp">click 
                        here</a> to process and print out your fax coversheets. 
                        These act as a checklist of the documents that you need 
                        to fax in to us. 
                        <%
}
%>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
          <td> 
            <table width="100%%" border="0" cellspacing="0" cellpadding="0" bordercolor="#000000">
              <tr> 
                <td> 
                  <table width="100%%" border="1" cellspacing="0" cellpadding="5" bordercolor="#999999" class=tdBase>
                    <tr class=tdHeaderAlt> 
                      <td>Begin the data collection Process</td>
                    </tr>
                    <tr> 
                      <td bgcolor="#CCFFCC" align="right"><a href="pro-file_Status_Express.jsp"><img src="express/expressButton1.gif" width="300" height="75" border="0"></a></td>
                    </tr>
                    <tr> 
                      <td bgcolor="#CCFFCC">A simple form to quickly enter your 
                        core credentialing information.</td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                    </tr>
                    <tr> 
                      <td bgcolor="#FFCCCC" align="right"><a href="pro-file_Status_Advanced.jsp"><img src="express/advancedButton1.gif" width="300" height="75" border="0"></a></td>
                    </tr>
                    <tr> 
                      <td bgcolor="#FFCCCC">A comprehensive form to store all 
                        your credentialing data. This is useful for completing 
                        various application forms.</td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p>&nbsp;</p>
    </td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td class=tdHeader> 
      <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
        <tr> 
          <td> 
            <table width="100%" border="1" cellspacing="0" cellpadding="3" bordercolor="#999999">
              <tr class=tdHeaderAlt> 
                <td nowrap>Other Actions</td>
                <td width="90%">Description</td>
              </tr>
              <tr class=tdBase> 
                <td nowrap><a href="Physician_HCOAuth.jsp">HCO Authorization</a> 
                </td>
                <td width="90%">Authorize participating HCOs to have electronic 
                  access to your credentialing information.</td>
              </tr>
              <tr class=tdBaseAlt> 
                <td nowrap><a href="populateForms.jsp">Populate Forms</a></td>
                <td width="90%">Print HCO application forms with your data pre-populated.</td>
              </tr>
              <tr class=tdBase> 
                <td nowrap><a href="reports_PhysicianID.jsp">Run Reports</a> </td>
                <td width="90%">View and Print helpful reports</td>
              </tr>
              <tr class=tdBaseAlt> 
                <td nowrap><a href="tDocumentManagement_PhysicianID.jsp">Document 
                  Management</a> </td>
                <td width="90%">On-line storage of copies of your credentialing 
                  documents</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td class=tdHeader>&nbsp;</td>
  </tr>
</table>
<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
