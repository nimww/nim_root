<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltAdminPracticeLU" %>
<%/*

    filename: out\jsp\tAdminPracticeLU_form2.jsp
    JSP AutoGen on Mar/26/2002
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_CompanyID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>


    <table cellpadding=0 cellspacing=0 border=0 width=700>
    <tr><td width=10>&nbsp;</td><td>
    <span class=title>File: tAdminPracticeLU_form2.jsp</span><br>


<%
//initial declaration of list class and parentID
    Integer        iLookupID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iLookupID")) 
    {
        iLookupID        =    (Integer)pageControllerHash.get("iLookupID");
        accessValid = true;
        if (pageControllerHash.containsKey("sKeyMasterReference"))
        {
            sKeyMasterReference = (String)pageControllerHash.get("sKeyMasterReference");
        }
        if (sKeyMasterReference!=null)
        {
            if (sKeyMasterReference.equalsIgnoreCase("p"))
            {
                if (!pageControllerHash.containsKey("iAdminID")) 
                {
                    accessValid=false;
                }
            }
            else if (sKeyMasterReference.equalsIgnoreCase("s"))
            {
                if (!pageControllerHash.containsKey("iPracticeID")) 
                {
                    accessValid=false;
                }
            }
            else
            {
                accessValid=false;
            }
        }
        else 
        {
            accessValid=false;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tAdminPracticeLU_form2.jsp");
//initial declaration of list class and parentID

    bltAdminPracticeLU        AdminPracticeLU        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        AdminPracticeLU        =    new    bltAdminPracticeLU(iLookupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        AdminPracticeLU        =    new    bltAdminPracticeLU();
    }

//fields
        %>
        <%@ include file="tAdminPracticeLU_form_instructions.jsp" %>
        <form method=POST action="tAdminPracticeLU_form_sub.jsp" name="tAdminLU_form1">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>        <p class=tdHeader><b>UniqueID</b>[<i>LookupID</i>]<%=AdminPracticeLU.getLookupID()%></p>
        <p class=tdBase><b>This Item was Created on:</b> <%=displayDateSDF.format(AdminPracticeLU.getUniqueCreateDate())%></p>
        <p class=tdBase><b>This Item was Last Modified on:</b> <%=displayDateSDF.format(AdminPracticeLU.getUniqueModifyDate())%></p>
        <p class=tdBase><b>Modification Comments:</b> <%=(AdminPracticeLU.getUniqueModifyComments())%></p>
          <%  String theClass ="tdBase";%>
            <%
            if (sKeyMasterReference.equalsIgnoreCase("p"))
            {
            %>
            <p class=tdBase><b>AdminID</b> <%=AdminPracticeLU.getAdminID()%></p>
            <p class=tdBaseAlt><b>PracticeID</b><input type=text name="PracticeID" value="<%=AdminPracticeLU.getPracticeID()%>"></p>
            <%
            }
            else if (sKeyMasterReference.equalsIgnoreCase("s"))
            {
            %>
            <p class=tdBase><b>PracticeID</b> <%=AdminPracticeLU.getPracticeID()%></p>
            <p class=tdBaseAlt><b>AdminID</b><input type=text onFocus="javascript:this.blur();" name="AdminID" value="<%=AdminPracticeLU.getAdminID()%>">
          <input type="button" name="changePassword2" value="Change..." onClick="MM_openBrWindow('Admin_via_CompanyID.jsp','changeReferenceID','status=yes,scrollbars=yes,resizable=yes,width=700,height=500')" style="font-family: Arial; font-size: 10px">			
			
			</p>
            <%
            }
            %>
            <%
            if ( (AdminPracticeLU.isRequired("Comments"))&&(!AdminPracticeLU.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminPracticeLU.isExpired("Comments",expiredDays))&&(AdminPracticeLU.isExpiredCheck("Comments")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Extra Comments</b><input type=text size="100" name="Comments" value="<%=AdminPracticeLU.getComments()%>"></p>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <p><select name=routePageReference>
            <option value="sParentReturnPage">Save & Return</option>
            <option value="sLocalChildReturnPage">Save & Stay</option>
            </select></p>
            <p><input type=Submit value=Submit name=Submit></p>
        <%}%>
        </form>
        <%
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_CompanyID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
