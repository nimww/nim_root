<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltWorkHistory,com.winstaff.bltWorkHistory_List" %>
<%/*
    filename: tWorkHistory_main_WorkHistory_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tWorkHistory")%>



<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection8", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tWorkHistory_main_WorkHistory_PhysicianID.jsp");
    pageControllerHash.remove("iWorkHistoryID");
    pageControllerHash.put("sINTNext","tWorkHistory_main_WorkHistory_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltWorkHistory_List        bltWorkHistory_List        =    new    bltWorkHistory_List(iPhysicianID);

//declaration of Enumeration
    bltWorkHistory        working_bltWorkHistory;
    ListElement         leCurrentElement;
    Enumeration eList = bltWorkHistory_List.elements();
    %>
        <%@ include file="tWorkHistory_main_WorkHistory_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tWorkHistory_main_WorkHistory_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltWorkHistory  = (bltWorkHistory) leCurrentElement.getObject();
        working_bltWorkHistory.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltWorkHistory.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <tr class=incompleteItem><td><b>Not Complete</b><br>
        <%
        }
        else
        {
        %>
        <tr class=<%=theClass%> ><td> 
        <%
        }
        %>

              <b>Item ID:&nbsp;</b><%=working_bltWorkHistory.getWorkHistoryID()%></td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("WorkHistoryTypeID",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("WorkHistoryTypeID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("WorkHistoryTypeID"))&&(!working_bltWorkHistory.isComplete("WorkHistoryTypeID")) ){theClassF = "requiredFieldMain";}%>
            <td><p class=<%=theClassF%> ><b>Work Type:&nbsp;</b><jsp:include page="../generic/tWorkHistoryTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltWorkHistory.getWorkHistoryTypeID()%>" /></jsp:include></p></td>

<%theClassF = "textBase";%>
<%if ((working_bltWorkHistory.isExpired("Name",expiredDays))&&(working_bltWorkHistory.isExpiredCheck("Name"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltWorkHistory.isRequired("Name"))&&(!working_bltWorkHistory.isComplete("Name")) ){theClassF = "requiredFieldMain";}%>
            <td><p class=<%=theClassF%> ><b>Company:&nbsp;</b><%=working_bltWorkHistory.getName()%></p></td>

            <td > 
        <a class=linkBase href = "tWorkHistory_main_WorkHistory_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltWorkHistory.getWorkHistoryID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase  onClick="return confirmDelete()"  href = "tWorkHistory_main_WorkHistory_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltWorkHistory.getWorkHistoryID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        <% }%>
                  </td></tr>
        <%
    }//end while
       }//end of if
       else 
       {
           %>
           <tr><td><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tWorkHistory")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tWorkHistory")%>"))
           {
               document.location="tWorkHistory_main_WorkHistory_PhysicianID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
