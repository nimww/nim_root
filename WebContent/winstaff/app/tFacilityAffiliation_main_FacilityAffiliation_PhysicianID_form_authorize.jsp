<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltFacilityAffiliation,com.winstaff.bltFacilityAffiliation_List" %>
<%/*
    filename: out\jsp\tFacilityAffiliation_main_FacilityAffiliation_PhysicianID_form_authorize.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection10", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }
    bltFacilityAffiliation_List        bltFacilityAffiliation_List        =    new    bltFacilityAffiliation_List(iPhysicianID,"AffiliationID="+requestID,"");

//declaration of Enumeration
    bltFacilityAffiliation        working_bltFacilityAffiliation;
    ListElement         leCurrentElement;
    Enumeration eList = bltFacilityAffiliation_List.elements();
    %>
    <%
    if (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltFacilityAffiliation  = (bltFacilityAffiliation) leCurrentElement.getObject();
        pageControllerHash.put("iAffiliationID",working_bltFacilityAffiliation.getAffiliationID());
        pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tFacilityAffiliation_form.jsp?nullParam=null"+parameterPassString    ;
        if (request.getParameter("EDIT").equalsIgnoreCase("del"))
        {
            targetRedirect = "tFacilityAffiliation_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
        }
        response.sendRedirect(targetRedirect);
    }
    else
    {
   out.println("invalid where query");
    }

  }
  else
  {
   out.println("illegal");
  }
}
else
{
out.println("Your Security Level does not permit you to View this.");
}
%>




