<!-- it is transitional only because of using width='xx%' property in table example -->
<html xmlns='http://www.w3.org/1999/xhtml'><head>

<!-- meta http-equiv="Content-Type" content="text/html; charset=windows-1251" / -->
<link rel='stylesheet' href='resizable.css' type='text/css' media='screen' />
<title>Resizable Table Columns Demo</title>

</head><body>

<script type='text/javascript' src='js/resizable-tables.js'></script>
<table class='resizable' width="100%" border="1" height="100%">
  <tr height="40%">
    <th height="40%" colspan="2"><iframe name="queryResults" src="AdminPracticeAll_query.jsp" width="100%" height="100%" scrolling="auto"></iframe></th>
  </tr>
  <tr height="60%">
    <th width='50%' height="60%"><iframe name="mapResults" width="100%" height="100%" scrolling="auto"></iframe></th>
    <th width='50%' height="60%"><iframe name="editResults"  width="100%" height="100%" scrolling="auto"></iframe></th>
    </tr>
</table>
</body></html>
