<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tAdminPracticeLU_main_LU_AdminID.jsp
    Created on Apr/23/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0>
    <tr><td width=10>&nbsp;</td>
    <td>
<p> <br />
   <span class=title>See NetDev Response</span><p>


    <%
//initial declaration of list class and parentID
    Integer        iGenAdminID        =    null;
    Integer        iCommTrackID        =    null;
    boolean accessValid = false;
    if ( (isScheduler3||pageControllerHash.containsKey("iGenAdminID"))&&request.getParameter("EDITID")!=null) 
    {
        iCommTrackID        =    new Integer(request.getParameter("EDITID"));
//        iGenAdminID        =    (Integer)pageControllerHash.get("iGenAdminID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat dbdf_day = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1Full);
	  
	  
    bltNIM3_CommTrack        NIM3_CommTrack        =    null;
	NIM3_CommTrack        =    new    bltNIM3_CommTrack(iCommTrackID,UserSecurityGroupID);
boolean isOpenSave = false;
if (NIM3_CommTrack.getAlertStatusCode()==0||isScheduler3)
{
	isOpenSave = true;
}
String testChangeID = "0";

try
{
    Integer testObj = new Integer(request.getParameter("AlertStatusCode")) ;
    if ( !NIM3_CommTrack.getAlertStatusCode().equals(testObj)  )
    {
         NIM3_CommTrack.setAlertStatusCode( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack AlertStatusCode not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !NIM3_CommTrack.getComments().equals(testObj)  )
    {
         NIM3_CommTrack.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack Comments not set. this is ok-not an error");
}

int switchCourtesy = -1;
try
{
    Integer testObj = new Integer(request.getParameter("isCourtesy")) ;
    if ( testObj !=-1 )
    {
//		NIM3_CommTrack.setAlertStatusCode( 1  );
		switchCourtesy = testObj.intValue();
		testChangeID = "1";
    }
}
catch(Exception e)
{
     //out.println("NIM3_CommTrack AlertStatusCode not set. this is ok-not an error");
}




if (testChangeID.equalsIgnoreCase("1")&&isOpenSave)
{
	NIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
	NIM3_CommTrack.setUniqueModifyComments(UserLogonDescription);
	if (NIM3_CommTrack.getAlertStatusCode()==1||NIM3_CommTrack.getAlertStatusCode()==2||NIM3_CommTrack.getAlertStatusCode()==3)
	{
		NIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(true));
		bltNIM3_Encounter myE = new bltNIM3_Encounter(NIM3_CommTrack.getEncounterID());
		myE.setSeeNetDev_Waiting(new Integer(2));
		if (NIM3_CommTrack.getAlertStatusCode()==3)
		{
			myE.setisCourtesy(new Integer(4));
		}
		if (switchCourtesy==1||switchCourtesy==3||switchCourtesy==2)
		{
			myE.setisCourtesy(new Integer(switchCourtesy));
			bltNIM3_CommTrack        working_bltNIM3_CommTrack = new bltNIM3_CommTrack();
			working_bltNIM3_CommTrack.setUniqueModifyComments("NIMUtils:processNetDevWorklist:System Generated [ULD:"+UserLogonDescription+"]");
			working_bltNIM3_CommTrack.setEncounterID(myE.getEncounterID());
			working_bltNIM3_CommTrack.setMessageName(CurrentUserAccount.getLogonUserName());
//			working_bltNIM3_CommTrack.setMessageCompany(myPM.getPayerName());
			working_bltNIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
			working_bltNIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
			working_bltNIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
			working_bltNIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
			working_bltNIM3_CommTrack.setUniqueModifyComments(""+UserLogonDescription);
			if (working_bltNIM3_CommTrack.getAlertStatusCode()==0)
			{
				//working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
			}
			if (switchCourtesy==1)
			{
				working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
				working_bltNIM3_CommTrack.setMessageText("Courtesy Schedule Approved");
			}
			else if (switchCourtesy==3)
			{
				working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
				working_bltNIM3_CommTrack.setMessageText("Courtesy Schedule Denied - Please Decline Case");
			}
			else if (switchCourtesy==2)
			{
				working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
				working_bltNIM3_CommTrack.setMessageText("Courtesy Schedule Removed. This case is no longer a courtesy.");
			}
			working_bltNIM3_CommTrack.commitData();
   		}
		myE.commitData();
		bltNIM3_Referral myR = new bltNIM3_Referral(myE.getReferralID());
		bltNIM3_CaseAccount myCA = new bltNIM3_CaseAccount(myR.getCaseID());
		bltUserAccount myUA = new bltUserAccount(myCA.getAssignedToID());
		emailType_V3 myEM = new emailType_V3();
		myEM.setFrom("server@nextimagemedical.com");
		myEM.setSubject("Response to: See NetDev - Request: SP: " + myE.getScanPass() + "[" + myCA.getPatientLastName() + "]");
		myEM.setBody("Response to: See NetDev - Request: SP: " + myE.getScanPass() + "[" + myCA.getPatientLastName() + "]");
		myEM.setTo(myUA.getContactEmail());
		myEM.isSendMail();
		//temp send email to bob
		if (NIM3_CommTrack.getAlertStatusCode()==3)
		{
			myEM.setSubject("Courtesy Review - Response to: See NetDev - Request: SP: " + myE.getScanPass() + "[" + myCA.getPatientLastName() + "]");
			myEM.setTo("Carol.Hosmer@nextimagemedical.com");
			myEM.isSendMail();
		}
	}
	NIM3_CommTrack.commitData();
	    %><script language=Javascript>
			alert("Your information has been updated");
			document.location="SeeND_Edit.jsp?EDITID=<%=NIM3_CommTrack.getCommTrackID()%>" ;
	      </script><%
}
else if (!isOpenSave)
{
	    %><script language=Javascript>
			alert("This CommTrack is no longer open and can't be updated.");
			document.location="SeeND_Edit.jsp?EDITID=<%=NIM3_CommTrack.getCommTrackID()%>" ;
	      </script><%
	
}
else
{
	    %><script language=Javascript>
			alert("Nothing Changed...");
			document.location="SeeND_Edit.jsp?EDITID=<%=NIM3_CommTrack.getCommTrackID()%>" ;
	      </script><%
	
}



}
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
%>    