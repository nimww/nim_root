<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltAdminMaster" %>
<%/*

    filename: out\jsp\tAdminMaster_form.jsp
    JSP AutoGen on Mar/26/2002
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_CompanyID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=700>
    <tr><td width=10>&nbsp;</td><td>
    <span class=title>File: tAdminMaster_form.jsp</span><br>


<%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iAdminID")) 
    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tAdminMaster_form.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID

    bltAdminMaster        AdminMaster        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        AdminMaster        =    new    bltAdminMaster(iAdminID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        AdminMaster        =    new    bltAdminMaster();
    }

//fields
        %>
        <%@ include file="tAdminMaster_form_instructions.jsp" %>
        <form method=POST action="tAdminMaster_form_sub.jsp" name="tAdminMaster_form1">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>        <p class=tdHeader><b>UniqueID</b>[<i>AdminID</i>]<%=AdminMaster.getAdminID()%></p>
        <p class=tdBase><b>This Item was Created on:</b> <%=displayDateSDF.format(AdminMaster.getUniqueCreateDate())%></p>
        <p class=tdBase><b>This Item was Last Modified on:</b> <%=displayDateSDF.format(AdminMaster.getUniqueModifyDate())%></p>
        <p class=tdBase><b>Modification Comments:</b> <%=(AdminMaster.getUniqueModifyComments())%></p>
          <%  String theClass ="tdBase";%>
            <%
            if ( (AdminMaster.isRequired("Name"))&&(!AdminMaster.isComplete("Name")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("Name",expiredDays))&&(AdminMaster.isExpiredCheck("Name")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Organization Name</b><input type=text size="50" name="Name" value="<%=AdminMaster.getName()%>"></p>

            <%
            if ( (AdminMaster.isRequired("Address1"))&&(!AdminMaster.isComplete("Address1")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("Address1",expiredDays))&&(AdminMaster.isExpiredCheck("Address1")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Organization Address</b><input type=text size="25" name="Address1" value="<%=AdminMaster.getAddress1()%>"></p>

            <%
            if ( (AdminMaster.isRequired("Address2"))&&(!AdminMaster.isComplete("Address2")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("Address2",expiredDays))&&(AdminMaster.isExpiredCheck("Address2")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Organization Address 2</b><input type=text size="10" name="Address2" value="<%=AdminMaster.getAddress2()%>"></p>

            <%
            if ( (AdminMaster.isRequired("City"))&&(!AdminMaster.isComplete("City")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("City",expiredDays))&&(AdminMaster.isExpiredCheck("City")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Organization City</b><input type=text size="15" name="City" value="<%=AdminMaster.getCity()%>"></p>

            <%
            if ( (AdminMaster.isRequired("StateID"))&&(!AdminMaster.isComplete("StateID")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("StateID",expiredDays))&&(AdminMaster.isExpiredCheck("StateID")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Organization State</b><select name="StateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=AdminMaster.getStateID()%>" /></jsp:include></select></p>

            <%
            if ( (AdminMaster.isRequired("ZIP"))&&(!AdminMaster.isComplete("ZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ZIP",expiredDays))&&(AdminMaster.isExpiredCheck("ZIP")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Organization ZIP</b><input type=text size="5" name="ZIP" value="<%=AdminMaster.getZIP()%>"></p>

            <%
            if ( (AdminMaster.isRequired("Phone"))&&(!AdminMaster.isComplete("Phone")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("Phone",expiredDays))&&(AdminMaster.isExpiredCheck("Phone")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Organization Phone (XXX-XXX-XXXX)</b><input type=text size="10" name="Phone" value="<%=AdminMaster.getPhone()%>"></p>

            <%
            if ( (AdminMaster.isRequired("AlertEmail"))&&(!AdminMaster.isComplete("AlertEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("AlertEmail",expiredDays))&&(AdminMaster.isExpiredCheck("AlertEmail")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Alert Email</b><input type=text size="25" name="AlertEmail" value="<%=AdminMaster.getAlertEmail()%>"></p>

            <%
            if ( (AdminMaster.isRequired("AlertDays"))&&(!AdminMaster.isComplete("AlertDays")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("AlertDays",expiredDays))&&(AdminMaster.isExpiredCheck("AlertDays")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Alert Days</b><input type=text size="10" name="AlertDays" value="<%=AdminMaster.getAlertDays()%>"></p>

            <%
            if ( (AdminMaster.isRequired("ContactFirstName"))&&(!AdminMaster.isComplete("ContactFirstName")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactFirstName",expiredDays))&&(AdminMaster.isExpiredCheck("ContactFirstName")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Contact First Name</b><input type=text size="25" name="ContactFirstName" value="<%=AdminMaster.getContactFirstName()%>"></p>

            <%
            if ( (AdminMaster.isRequired("ContactLastName"))&&(!AdminMaster.isComplete("ContactLastName")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactLastName",expiredDays))&&(AdminMaster.isExpiredCheck("ContactLastName")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Contact Last Name</b><input type=text size="25" name="ContactLastName" value="<%=AdminMaster.getContactLastName()%>"></p>

            <%
            if ( (AdminMaster.isRequired("ContactEmail"))&&(!AdminMaster.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactEmail",expiredDays))&&(AdminMaster.isExpiredCheck("ContactEmail")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Contact Name</b><input type=text size="37" name="ContactEmail" value="<%=AdminMaster.getContactEmail()%>"></p>

            <%
            if ( (AdminMaster.isRequired("ContactAddress1"))&&(!AdminMaster.isComplete("ContactAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactAddress1",expiredDays))&&(AdminMaster.isExpiredCheck("ContactAddress1")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Contact Address</b><input type=text size="25" name="ContactAddress1" value="<%=AdminMaster.getContactAddress1()%>"></p>

            <%
            if ( (AdminMaster.isRequired("ContactAddress2"))&&(!AdminMaster.isComplete("ContactAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactAddress2",expiredDays))&&(AdminMaster.isExpiredCheck("ContactAddress2")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Contact Address 2</b><input type=text size="10" name="ContactAddress2" value="<%=AdminMaster.getContactAddress2()%>"></p>

            <%
            if ( (AdminMaster.isRequired("ContactCity"))&&(!AdminMaster.isComplete("ContactCity")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactCity",expiredDays))&&(AdminMaster.isExpiredCheck("ContactCity")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Contact City</b><input type=text size="15" name="ContactCity" value="<%=AdminMaster.getContactCity()%>"></p>

            <%
            if ( (AdminMaster.isRequired("ContactStateID"))&&(!AdminMaster.isComplete("ContactStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactStateID",expiredDays))&&(AdminMaster.isExpiredCheck("ContactStateID")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Contact State</b><select name="ContactStateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=AdminMaster.getContactStateID()%>" /></jsp:include></select></p>

            <%
            if ( (AdminMaster.isRequired("ContactZIP"))&&(!AdminMaster.isComplete("ContactZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactZIP",expiredDays))&&(AdminMaster.isExpiredCheck("ContactZIP")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Contact ZIP</b><input type=text size="5" name="ContactZIP" value="<%=AdminMaster.getContactZIP()%>"></p>

            <%
            if ( (AdminMaster.isRequired("ContactPhone"))&&(!AdminMaster.isComplete("ContactPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("ContactPhone",expiredDays))&&(AdminMaster.isExpiredCheck("ContactPhone")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Contact Phone (XXX-XXX-XXXX)</b><input type=text size="10" name="ContactPhone" value="<%=AdminMaster.getContactPhone()%>"></p>

            <%
            if ( (AdminMaster.isRequired("Comments"))&&(!AdminMaster.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((AdminMaster.isExpired("Comments",expiredDays))&&(AdminMaster.isExpiredCheck("Comments")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Extra Comments</b><input type=text size="100" name="Comments" value="<%=AdminMaster.getComments()%>"></p>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <p><select name=routePageReference>
            <option value="sParentReturnPage">Save & Return</option>
            <option value="sLocalChildReturnPage">Save & Stay</option>
            </select></p>
            <p><input type=Submit value=Submit name=Submit></p>
        <%}%>
        </form>
        <%
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_CompanyID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
