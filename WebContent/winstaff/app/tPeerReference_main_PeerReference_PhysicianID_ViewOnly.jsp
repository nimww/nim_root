<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltPeerReference, com.winstaff.bltPeerReference_List" %>
<%/*
    filename: tPeerReference_main_PeerReference_PhysicianID.jsp
    Created on May/26/2004
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    String MasterTableWidthVO = "100%";
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    if (pageControllerHash.containsKey("MasterTableWidthVO")) 
    {
        MasterTableWidthVO = (String)pageControllerHash.get("MasterTableWidthVO");
    }
    %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidthVO%> >
    <tr><td width=10>&nbsp;</td><td>




<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection11", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID") ) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tPeerReference_main_PeerReference_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iReferenceID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltPeerReference_List        bltPeerReference_List        =    new    bltPeerReference_List(iPhysicianID);

//declaration of Enumeration
    bltPeerReference        PeerReference;
    ListElement         leCurrentElement;
    Enumeration eList = bltPeerReference_List.elements();
    %>
        <%@ include file="tPeerReference_main_PeerReference_PhysicianID_instructions.jsp" %>


    <%
    int iExpress=0;
    while (eList.hasMoreElements())
    {
       iExpress++;
         %>
         <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        PeerReference  = (bltPeerReference) leCurrentElement.getObject();
      }
      else
      {
        PeerReference  = new bltPeerReference();
        isNewRecord= true;
      }
        PeerReference.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tPeerReference")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

  {

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (PeerReference.isRequired("Salutation",UserSecurityGroupID))&&(!PeerReference.isComplete("Salutation")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("Salutation",expiredDays))&&(PeerReference.isExpiredCheck("Salutation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Salutation",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Salutation&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tSalutationLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PeerReference.getSalutation()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Salutation&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Salutation")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Salutation",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Salutation&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tSalutationLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PeerReference.getSalutation()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Salutation&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Salutation")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PeerReference.isRequired("FirstName",UserSecurityGroupID))&&(!PeerReference.isComplete("FirstName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("FirstName",expiredDays))&&(PeerReference.isExpiredCheck("FirstName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("FirstName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>First Name&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getFirstName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FirstName&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("FirstName")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("FirstName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>First Name&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getFirstName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=FirstName&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("FirstName")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("LastName",UserSecurityGroupID))&&(!PeerReference.isComplete("LastName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("LastName",expiredDays))&&(PeerReference.isExpiredCheck("LastName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("LastName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Last Name&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getLastName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LastName&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("LastName")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("LastName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Last Name&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getLastName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LastName&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("LastName")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("Specialty",UserSecurityGroupID))&&(!PeerReference.isComplete("Specialty")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("Specialty",expiredDays))&&(PeerReference.isExpiredCheck("Specialty",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Specialty",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Specialty&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getSpecialty()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Specialty&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Specialty")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Specialty",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Specialty&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getSpecialty()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Specialty&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Specialty")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("Address1",UserSecurityGroupID))&&(!PeerReference.isComplete("Address1")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("Address1",expiredDays))&&(PeerReference.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Address1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Address1")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Address1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Address1")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("Address2",UserSecurityGroupID))&&(!PeerReference.isComplete("Address2")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("Address2",expiredDays))&&(PeerReference.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Address2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Address2")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Address2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Address2")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("City",UserSecurityGroupID))&&(!PeerReference.isComplete("City")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("City",expiredDays))&&(PeerReference.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("City",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("City")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("City",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("City")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("StateID",UserSecurityGroupID))&&(!PeerReference.isComplete("StateID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("StateID",expiredDays))&&(PeerReference.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PeerReference.getStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("StateID")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PeerReference.getStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("StateID")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PeerReference.isRequired("Province",UserSecurityGroupID))&&(!PeerReference.isComplete("Province")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("Province",expiredDays))&&(PeerReference.isExpiredCheck("Province",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Province",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Province")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Province",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Province")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("ZIP",UserSecurityGroupID))&&(!PeerReference.isComplete("ZIP")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("ZIP",expiredDays))&&(PeerReference.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("ZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("ZIP")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("ZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("ZIP")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("CountryID",UserSecurityGroupID))&&(!PeerReference.isComplete("CountryID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("CountryID",expiredDays))&&(PeerReference.isExpiredCheck("CountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PeerReference.getCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("CountryID")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PeerReference.getCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("CountryID")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PeerReference.isRequired("Phone",UserSecurityGroupID))&&(!PeerReference.isComplete("Phone")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("Phone",expiredDays))&&(PeerReference.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Phone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Phone")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Phone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Phone")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("Fax",UserSecurityGroupID))&&(!PeerReference.isComplete("Fax")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("Fax",expiredDays))&&(PeerReference.isExpiredCheck("Fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Fax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Fax")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Fax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Fax")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("ContactEmail",UserSecurityGroupID))&&(!PeerReference.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("ContactEmail",expiredDays))&&(PeerReference.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("YearsAssociated",UserSecurityGroupID))&&(!PeerReference.isComplete("YearsAssociated")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("YearsAssociated",expiredDays))&&(PeerReference.isExpiredCheck("YearsAssociated",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("YearsAssociated",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Years associated&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getYearsAssociated()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=YearsAssociated&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("YearsAssociated")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("YearsAssociated",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Years associated&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getYearsAssociated()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=YearsAssociated&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("YearsAssociated")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("DocuLinkID",UserSecurityGroupID))&&(!PeerReference.isComplete("DocuLinkID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("DocuLinkID",expiredDays))&&(PeerReference.isExpiredCheck("DocuLinkID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("DocuLinkID",UserSecurityGroupID)))
            {
                        if (PeerReference.getDocuLinkID().intValue()>0)
                        {
                            bltDocumentManagement myDoc = new bltDocumentManagement(PeerReference.getDocuLinkID());
                            if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                            {
                            %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Attached Document:&nbsp;</p></td><td valign=top><p><a href="pdf/<%=myDoc.getDocumentFileName()%>">view</a></b></p></td></tr>
                            <%
                           }
                            else
                            {
                            %>

                            <%
                            }
                        }
            }
            else if ((PeerReference.isRead("DocuLinkID",UserSecurityGroupID)))
            {
                        bltDocumentManagement myDoc = new bltDocumentManagement(PeerReference.getDocuLinkID());
                        if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                        {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Attached Document:&nbsp;</p></td><td valign=top><p><a href="pdf/<%=myDoc.getDocumentFileName()%>">view</a></b></p></td></tr>
                        <%
                        }
                        else
                        {
                        %>

                        <%
                        }
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (PeerReference.isRequired("Comments",UserSecurityGroupID))&&(!PeerReference.isComplete("Comments")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("Comments",expiredDays))&&(PeerReference.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PeerReference.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Comments")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=PeerReference.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Comments")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("Title",UserSecurityGroupID))&&(!PeerReference.isComplete("Title")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("Title",expiredDays))&&(PeerReference.isExpiredCheck("Title",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("Title",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Title&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getTitle()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Title&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Title")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("Title",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Title&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getTitle()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Title&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("Title")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("HospitalAffiliation",UserSecurityGroupID))&&(!PeerReference.isComplete("HospitalAffiliation")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("HospitalAffiliation",expiredDays))&&(PeerReference.isExpiredCheck("HospitalAffiliation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("HospitalAffiliation",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>HospitalAffiliation&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getHospitalAffiliation()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HospitalAffiliation&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("HospitalAffiliation")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("HospitalAffiliation",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>HospitalAffiliation&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getHospitalAffiliation()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HospitalAffiliation&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("HospitalAffiliation")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (PeerReference.isRequired("HospitalDepartment",UserSecurityGroupID))&&(!PeerReference.isComplete("HospitalDepartment")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((PeerReference.isExpired("HospitalDepartment",expiredDays))&&(PeerReference.isExpiredCheck("HospitalDepartment",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((PeerReference.isWrite("HospitalDepartment",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>HospitalDepartment&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getHospitalDepartment()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HospitalDepartment&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("HospitalDepartment")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((PeerReference.isRead("HospitalDepartment",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>HospitalDepartment&nbsp;</b></p></td><td valign=top><p><%=PeerReference.getHospitalDepartment()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HospitalDepartment&amp;sTableName=tPeerReference&amp;sRefID=<%=PeerReference.getReferenceID()%>&amp;sFieldNameDisp=<%=PeerReference.getEnglish("HospitalDepartment")%>&amp;sTableNameDisp=tPeerReference','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>



        </td></tr></table>
        </td></tr></table>
        <%
  }%>

        </td></tr></table></table><br>        <%
    }
    %>




    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


