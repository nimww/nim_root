<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltAdminPhysicianLU,com.winstaff.bltAdminPhysicianLU_List_LU_PhysicianID" %>
<%/*
    filename: tAdminPhysicianLU_main2_LU_PhysicianID.jsp
    JSP AutoGen on Mar/26/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_CompanyID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=700>
    <tr><td width=10>&nbsp;</td><td>
    <span class=title>File: tAdminPhysicianLU_main2_LU_PhysicianID.jsp</span><br>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tAdminPhysicianLU_main2_LU_PhysicianID.jsp");
    pageControllerHash.remove("iLookupID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltAdminPhysicianLU_List_LU_PhysicianID        bltAdminPhysicianLU_List_LU_PhysicianID        =    new    bltAdminPhysicianLU_List_LU_PhysicianID(iPhysicianID);

//declaration of Enumeration
    bltAdminPhysicianLU        working_bltAdminPhysicianLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltAdminPhysicianLU_List_LU_PhysicianID.elements();
    %>
        <%@ include file="tAdminPhysicianLU_main2_LU_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tAdminPhysicianLU_main2_LU_PhysicianID_form_create.jsp?EDIT=new&KM=s"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>

    <%
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltAdminPhysicianLU  = (bltAdminPhysicianLU) leCurrentElement.getObject();
        String theClass = "tdHeader";
        if (!working_bltAdminPhysicianLU.isComplete())
        {
            theClass = "incompleteItem";
        %>
            <p class=incompleteItem><b>This item is incomplete</b></p>
        <%
        }
        %>
        <p class=<%=theClass%> ><b>UniqueID</b>[<i>LookupID</i>]<%=working_bltAdminPhysicianLU.getLookupID()%> </p>
        <a class=linkBase href = "tAdminPhysicianLU_main2_LU_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltAdminPhysicianLU.getLookupID()%>&KM=s"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tAdminPhysicianLU_main2_LU_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltAdminPhysicianLU.getLookupID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        </p>
        <%}%>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPhysicianLU.isExpired("UniqueCreateDate",expiredDays))&&(working_bltAdminPhysicianLU.isExpiredCheck("UniqueCreateDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPhysicianLU.isRequired("UniqueCreateDate"))&&(!working_bltAdminPhysicianLU.isComplete("UniqueCreateDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Item Create Date</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltAdminPhysicianLU.getUniqueCreateDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltAdminPhysicianLU.isExpired("UniqueModifyDate",expiredDays))&&(working_bltAdminPhysicianLU.isExpiredCheck("UniqueModifyDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPhysicianLU.isRequired("UniqueModifyDate"))&&(!working_bltAdminPhysicianLU.isComplete("UniqueModifyDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Item Modify Date</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltAdminPhysicianLU.getUniqueModifyDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltAdminPhysicianLU.isExpired("UniqueModifyComments",expiredDays))&&(working_bltAdminPhysicianLU.isExpiredCheck("UniqueModifyComments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPhysicianLU.isRequired("UniqueModifyComments"))&&(!working_bltAdminPhysicianLU.isComplete("UniqueModifyComments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Item Modification Comments</b><%=working_bltAdminPhysicianLU.getUniqueModifyComments()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPhysicianLU.isExpired("AdminID",expiredDays))&&(working_bltAdminPhysicianLU.isExpiredCheck("AdminID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPhysicianLU.isRequired("AdminID"))&&(!working_bltAdminPhysicianLU.isComplete("AdminID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>AdminID</b><%=working_bltAdminPhysicianLU.getAdminID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPhysicianLU.isExpired("PhysicianID",expiredDays))&&(working_bltAdminPhysicianLU.isExpiredCheck("PhysicianID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPhysicianLU.isRequired("PhysicianID"))&&(!working_bltAdminPhysicianLU.isComplete("PhysicianID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>PhysicianID</b><%=working_bltAdminPhysicianLU.getPhysicianID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltAdminPhysicianLU.isExpired("Comments",expiredDays))&&(working_bltAdminPhysicianLU.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltAdminPhysicianLU.isRequired("Comments"))&&(!working_bltAdminPhysicianLU.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments</b><%=working_bltAdminPhysicianLU.getComments()%></p>
        <%
    }
    %>
    <%
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_CompanyID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
