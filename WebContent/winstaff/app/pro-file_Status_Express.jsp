

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.ConfigurationMessages,com.winstaff.bltHCOPhysicianLU_List_LU_PhysicianID,com.winstaff.bltHCOPhysicianLU, com.winstaff.bltPhysicianUserAccountLU_List_LU_PhysicianID, com.winstaff.bltPhysicianUserAccountLU, com.winstaff.bltDocumentManagement_List,com.winstaff.bltDocumentManagement,com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltPhysicianMaster,com.winstaff.stepVerify_PhysicianID" %>
<%/*
    filename: out\jsp\tPhysicianMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>
<%@ include file="../generic/CheckLogin.jsp" %>
<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;    
   }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","pro-file_Status.jsp");
    pageControllerHash.put("sParentReturnPage","pro-file_Status.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

stepVerify_PhysicianID mySV = new stepVerify_PhysicianID();
mySV.setPhysicianID(iPhysicianID);
mySV.setSecurityID(UserSecurityGroupID);
bltPhysicianMaster        PhysicianMaster        =   new  bltPhysicianMaster (iPhysicianID);

//verify data
boolean ApplicationDataCore = false;
if (mySV.isAllCore())
{
	ApplicationDataCore = true;
}
boolean ApplicationDataExtra = false;
if (mySV.isAllExtra())
{
	ApplicationDataExtra = true;
}
boolean ApplicationDataAll = (ApplicationDataExtra&&ApplicationDataCore);
//verify documents are complete:


boolean SupportingDocuments = true;
{
    bltDocumentManagement_List        bltDocumentManagement_List        =    new    bltDocumentManagement_List(iPhysicianID,"Archived=2","");
	//declaration of Enumeration
    bltDocumentManagement        working_bltDocumentManagement;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltDocumentManagement_List.elements();
    while (eList.hasMoreElements()&&SupportingDocuments)
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltDocumentManagement  = (bltDocumentManagement) leCurrentElement.getObject();
		if (!working_bltDocumentManagement.isComplete())
		{
			SupportingDocuments = false;
		}
	}
}
	//check Attestation Account Status
	boolean AttestationAccount = false;
	boolean AttestationAccountCreated = false;
{
    bltPhysicianUserAccountLU_List_LU_PhysicianID        bltPhysicianUserAccountLU_List_LU_PhysicianID        =    new    bltPhysicianUserAccountLU_List_LU_PhysicianID(iPhysicianID);
	//declaration of Enumeration
    bltPhysicianUserAccountLU        working_bltPhysicianUserAccountLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltPhysicianUserAccountLU_List_LU_PhysicianID.elements();
    if (eList.hasMoreElements())
    {        
 	     AttestationAccountCreated = true;
         leCurrentElement    = (ListElement) eList.nextElement();
         working_bltPhysicianUserAccountLU  = (bltPhysicianUserAccountLU) leCurrentElement.getObject();
         bltUserAccount working_bltUserAccount  = new bltUserAccount(working_bltPhysicianUserAccountLU.getUserID());
		 if (working_bltUserAccount.getStatus().intValue()==2)
		 {
	 	     AttestationAccount = true;
		 }
    }
}

//check attest status
boolean Attestation = false;
if (PhysicianMaster.getIsAttested().intValue()==1)
{
	Attestation = true;
}


//check HCO Auth Status
boolean HCOAuth = false;
{
    bltHCOPhysicianLU_List_LU_PhysicianID        bltHCOPhysicianLU_List_LU_PhysicianID        =    new    bltHCOPhysicianLU_List_LU_PhysicianID(iPhysicianID);

//declaration of Enumeration
    bltHCOPhysicianLU        working_bltHCOPhysicianLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltHCOPhysicianLU_List_LU_PhysicianID.elements();
    if (eList.hasMoreElements())
    {
         HCOAuth = true;
    }

}





%>
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <div align="right"> 
        <table width="100%%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td> 
              <table border="0" cellpadding="0" cellspacing="0" width="690">
                <!-- fwtable fwsrc="expressTop.png" fwbase="express-nav.gif" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
                <tr> 
                  <td><img src="express/nav-images/spacer.gif" width="468" height="1" border="0" name="undefined_2"></td>
                  <td><img src="express/nav-images/spacer.gif" width="211" height="1" border="0" name="undefined_2"></td>
                  <td><img src="express/nav-images/spacer.gif" width="11" height="1" border="0" name="undefined_2"></td>
                  <td><img src="express/nav-images/spacer.gif" width="1" height="1" border="0" name="undefined_2"></td>
                </tr>
                <tr> 
                  <td colspan="3"><img name="expressnav_r1_c1" src="express/nav-images/express-nav_r1_c1.gif" width="690" height="11" border="0"></td>
                  <td><img src="express/nav-images/spacer.gif" width="1" height="11" border="0" name="undefined_2"></td>
                </tr>
                <tr> 
                  <td rowspan="2"><img name="expressnav_r2_c1" src="express/nav-images/express-nav_r2_c1.gif" width="468" height="39" border="0"></td>
                  <td bgcolor="#ffffff" valign="top"><a href="#" onClick='document.forms[0].submit()'><img src="images/continue.gif" width="136" height="23" border="0"></a></td>
                  <td rowspan="2"><img name="expressnav_r2_c3" src="express/nav-images/express-nav_r2_c3.gif" width="11" height="39" border="0"></td>
                  <td><img src="express/nav-images/spacer.gif" width="1" height="29" border="0" name="undefined_2"></td>
                </tr>
                <tr> 
                  <td><img name="expressnav_r3_c2" src="express/nav-images/express-nav_r3_c2.gif" width="211" height="10" border="0"></td>
                  <td><img src="express/nav-images/spacer.gif" width="1" height="10" border="0" name="undefined_2"></td>
                </tr>
              </table>
            </td>
            <td> 
              <div align="right"></div>
            </td>
          </tr>
        </table>
        <table width="100%%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
          <tr> 
            <td> 
              <table width="100%%" border="0" cellspacing="0" cellpadding="5">
                <tr class=tdBase> 
                  <td align="right"> 
                    <form name="form1" method="post" action="pro-file_Status_Express_sub.jsp">
<%
if (false)
{
%>
                      Audio Help 
                      <input type="checkbox" name="isAudio" value="checkbox" checked>
                      Smart Continue 
                      <input type="checkbox" name="isSmart" value="checkbox" checked>
                      <br>
<%
}
%>

					  <%
					  if (isBasic)
					  {
					  %>
                      <input type=hidden name="sExpressMode" value="2">
					  <%
					  }
					  else
					  {
					  %>
                      <select name="sExpressMode">
                        <option value="2">Interplan Recred</option>
                        <option value="1" selected>Full Credentialing</option>
                      </select>
					  <%
					  }
					  %>
                      <br>
                    </form>
                  </td>
                </tr>
                <tr class=tdHeaderAlt> 
                  <td align="center"> 
                    <div align="left">Data Collection Process:</div>
                  </td>
                </tr>
                <tr> 
                  <td align="center"> 
                    <table border="0" cellpadding="0" cellspacing="0" width="650">
                      <!-- fwtable fwsrc="dataprocess1.png" fwbase="dataprocess-nav.gif" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
                      <tr> 
                        <td><img src="images/dataprocess/spacer.gif" width="6" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="70" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="8" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="1" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="139" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="15" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="77" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="6" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="121" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="2" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="121" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="6" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="62" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="8" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="8" height="1" border="0" name="undefined_3"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="1" height="1" border="0" name="undefined_3"></td>
                      </tr>
                      <tr> 
                        <td colspan="15"><img name="dataprocessnav_r1_c1_2" src="images/dataprocess/dataprocess-nav_r1_c1.gif" width="650" height="13" border="0"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="1" height="13" border="0" name="undefined_3"></td>
                      </tr>
                      <tr> 
                        <td rowspan="2" colspan="3"><img name="dataprocessnav_r2_c1_2" src="images/dataprocess/dataprocess-nav_r2_c1.gif" width="84" height="49" border="0"></td>
                        <%
if (ApplicationDataCore)
{
%>
                        <td colspan="2"><a href="#" onClick='document.forms[0].submit()'><img name="dataprocessnav_r2_c4_2" src="images/dataprocess/dataprocess-nav_r2_c4_f2.gif" width="140" height="43" border="0"></a></td>
                        <%
}
else
{
%>
                        <td colspan="2"><a href="#" onClick='document.forms[0].submit()'><img name="dataprocessnav_r2_c4_2" src="images/dataprocess/dataprocess-nav_r2_c4_f2.gif" width="140" height="43" border="0"></a></td>
                        <%
}
%>
                        <td rowspan="6"><img name="dataprocessnav_r2_c6_2" src="images/dataprocess/dataprocess-nav_r2_c6.gif" width="15" height="187" border="0"></td>
                        <td rowspan="2" colspan="8"><a href="pro-file_Status_Express.jsp"><img name="dataprocessnav_r2_c7" src="images/dataprocess/dataprocess-nav_r2_c7_f2.gif" width="403" height="49" border="0"></a></td>
                        <td rowspan="6"><img name="dataprocessnav_r2_c15" src="images/dataprocess/dataprocess-nav_r2_c15.gif" width="8" height="187" border="0"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="1" height="43" border="0" name="undefined_3"></td>
                      </tr>
                      <tr> 
                        <td rowspan="3" colspan="2"><img name="dataprocessnav_r3_c4_2" src="images/dataprocess/dataprocess-nav_r3_c4.gif" width="140" height="83" border="0"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="1" height="6" border="0" name="undefined_3"></td>
                      </tr>
                      <tr> 
                        <td rowspan="4"><img name="dataprocessnav_r4_c1_2" src="images/dataprocess/dataprocess-nav_r4_c1.gif" width="6" height="138" border="0"></td>
                        <td><a href="pro-file_Status.jsp" ><img name="dataprocessnav_r4_c2_2" src="images/dataprocess/dataprocess-nav_r4_c2_f4.gif" width="70" height="72" border="0"></a></td>
                        <td rowspan="4"><img name="dataprocessnav_r4_c3_2" src="images/dataprocess/dataprocess-nav_r4_c3.gif" width="8" height="138" border="0"></td>
                        <%
if (ApplicationDataExtra)
{
%>
                        <td><a href="ExtraInformation_PhysicianID.jsp"><img name="dataprocessnav_r4_c7_2" src="images/dataprocess/dataprocess-nav_r4_c7_f3.gif" width="77" height="72" border="0"></a></td>
                        <%
}
else
{
%>
                        <td><a href="ExtraInformation_PhysicianID.jsp"><img name="dataprocessnav_r4_c7_2" src="images/dataprocess/dataprocess-nav_r4_c7.gif" width="77" height="72" border="0"></a></td>
                        <%
}
%>
                        <td><img name="dataprocessnav_r4_c8_2" src="images/dataprocess/dataprocess-nav_r4_c8.gif" width="6" height="72" border="0"></td>
                        <%
if (PhysicianMaster.getIsAttested().intValue()==1)
{
%>
                        <td><a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp"><img name="dataprocessnav_r4_c9_2" src="images/dataprocess/dataprocess-nav_r4_c9_f3.gif" width="121" height="72" border="0"></a></td>
                        <%
}
else
{
%>
                        <td><a href="tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp"><img name="dataprocessnav_r4_c9_2" src="images/dataprocess/dataprocess-nav_r4_c9.gif" width="121" height="72" border="0"></a></td>
                        <%
}
%>
                        <td><img name="dataprocessnav_r4_c10_2" src="images/dataprocess/dataprocess-nav_r4_c10.gif" width="2" height="72" border="0"></td>
                        <%
if (SupportingDocuments)
{
%>
                        <td><a href="tDocumentManagement_PhysicianID.jsp"><img name="dataprocessnav_r4_c11_2" src="images/dataprocess/dataprocess-nav_r4_c11_f3.gif" width="121" height="72" border="0"></a></td>
                        <%
}
else
{
%>
                        <td><a href="tDocumentManagement_PhysicianID.jsp"><img name="dataprocessnav_r4_c11_2" src="images/dataprocess/dataprocess-nav_r4_c11.gif" width="121" height="72" border="0"></a></td>
                        <%
}
%>
                        <td><img name="dataprocessnav_r4_c12_2" src="images/dataprocess/dataprocess-nav_r4_c12.gif" width="6" height="72" border="0"></td>
                        <td colspan="2"><a href="FinishedCheckList_PhysicianID.jsp"><img name="dataprocessnav_r4_c13_2" src="images/dataprocess/dataprocess-nav_r4_c13.gif" width="70" height="72" border="0"></a></td>
                        <td><img src="images/dataprocess/spacer.gif" width="1" height="72" border="0" name="undefined_3"></td>
                      </tr>
                      <tr> 
                        <td rowspan="3"><img name="dataprocessnav_r5_c2_2" src="images/dataprocess/dataprocess-nav_r5_c2.gif" width="70" height="66" border="0"></td>
                        <td rowspan="2" colspan="7"><img name="dataprocessnav_r5_c7_2" src="images/dataprocess/dataprocess-nav_r5_c7.gif" width="395" height="48" border="0"></td>
                        <td rowspan="3"><img name="dataprocessnav_r5_c14" src="images/dataprocess/dataprocess-nav_r5_c14.gif" width="8" height="66" border="0"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="1" height="5" border="0" name="undefined_3"></td>
                      </tr>
                      <tr> 
                        <td rowspan="2"><img name="dataprocessnav_r6_c4_2" src="images/dataprocess/dataprocess-nav_r6_c4.gif" width="1" height="61" border="0"></td>
                        <%
if (ApplicationDataCore)
{
%>
                        <td><a href="pro-file_Status_Advanced.jsp"><img name="dataprocessnav_r6_c5_2" src="images/dataprocess/dataprocess-nav_r6_c5_f3.gif" width="139" height="43" border="0"></a></td>
                        <%
}
else
{
%>
                        <td><a href="pro-file_Status_Advanced.jsp"><img name="dataprocessnav_r6_c5_2" src="images/dataprocess/dataprocess-nav_r6_c5_f4.gif" width="139" height="43" border="0"></a></td>
                        <%
}
%>
                        <td><img src="images/dataprocess/spacer.gif" width="1" height="43" border="0" name="undefined_3"></td>
                      </tr>
                      <tr> 
                        <td><img name="dataprocessnav_r7_c5_2" src="images/dataprocess/dataprocess-nav_r7_c5.gif" width="139" height="18" border="0"></td>
                        <td colspan="7"><img name="dataprocessnav_r7_c7" src="images/dataprocess/dataprocess-nav_r7_c7.gif" width="395" height="18" border="0"></td>
                        <td><img src="images/dataprocess/spacer.gif" width="1" height="18" border="0" name="undefined_3"></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr class=tdBase> 
                  <td align="center"> 
                    <div align="left"> 
                      <p class=title>Tips:</p>
                    </div>
                    <ul>
                      <li> 
                        <div align="left"><b>Express Mode</b> will guide you through 
                          the collection of your Core Practitioner Data.<br>
                          <br>
                        </div>
                      </li>
                      <li> 
                        <div align="left">Simply click the Continue Icon <img src="images/continue.gif" width="136" height="23" align="absmiddle"> 
                          at the top of each page or the 
                          <input  <%=HTMLFormStyleButton%>  type="button" name="Button" value="Continue">
                          Button at the bottom of each page to navigate through 
                          the various sections.<br>
                          <br>
                        </div>
                      </li>
                      <li> 
                        <div align="left">Make sure you fill out each section 
                          completely.</div>
                      </li>
                    </ul>
                  </td>
                </tr>
                <tr> 
                  <td align="center">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td class=tdHeader>&nbsp;</td>
  </tr>
</table>
<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
