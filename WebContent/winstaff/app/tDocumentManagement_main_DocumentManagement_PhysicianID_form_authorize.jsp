	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.SecurityCheck,com.winstaff.ConfigurationInformation,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils,com.winstaff.bltDocumentManagement,com.winstaff.bltDocumentManagement_List" %>
<%/*
    filename: out\jsp\tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp
    Created on Apr/23/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("DocumentManagement1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
//            out.println(requestID);
        }
    bltDocumentManagement_List        bltDocumentManagement_List        =    new    bltDocumentManagement_List(iPhysicianID,"DocumentID="+requestID,"");

//declaration of Enumeration
    bltDocumentManagement        working_bltDocumentManagement;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltDocumentManagement_List.elements();
    %>
    <%
    if (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltDocumentManagement  = (bltDocumentManagement) leCurrentElement.getObject();
        pageControllerHash.put("iDocumentID",working_bltDocumentManagement.getDocumentID());
//        pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
	String targetRedirect = "";
        if (request.getParameter("EDIT").equalsIgnoreCase("del"))
        {
            targetRedirect = "tDocumentManagement_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString;
            response.sendRedirect(targetRedirect);
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("print"))
        {
			    pageControllerHash.put("sFileName",ConfigurationInformation.sLinkedPDFDirectory + "\\" + working_bltDocumentManagement.getDocumentFileName());
			    pageControllerHash.put("sDownloadName",working_bltDocumentManagement.getDocumentFileName());
			    pageControllerHash.put("bDownload",new Boolean(false));
			    session.setAttribute("pageControllerHash",pageControllerHash);
		%>
		<script language=JavaScript>
		document.location="fileRetrieve.jsp";
		</script>
		<%
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("faxCover"))
         {
              targetRedirect = "../formPop/formPop_DocFaxCover.jsp";
		%>
		<script language=JavaScript>
		document.location="../formPop/formPop_DocFaxCover.jsp";
		</script>
		<%
        }
    }
    else
    {
//   response.sendRedirect("/errorhandler.jsp");
    }

  }
  else
  {
//   response.sendRedirect("/errorhandler.jsp");
  }
}
else
{
out.println("Your Security Level does not permit you to View this.");
}

%>
