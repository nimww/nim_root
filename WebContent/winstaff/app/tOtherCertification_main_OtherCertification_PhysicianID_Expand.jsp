<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltOtherCertification,com.winstaff.bltOtherCertification_List" %>
<%/*
    filename: tOtherCertification_main_OtherCertification_PhysicianID.jsp
    Created on Mar/21/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tOtherCertification")%>

    <br><a href="tOtherCertification_main_OtherCertification_PhysicianID.jsp">Compact</a>

<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection5", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tOtherCertification_main_OtherCertification_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iOtherCertID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltOtherCertification_List        bltOtherCertification_List        =    new    bltOtherCertification_List(iPhysicianID);

//declaration of Enumeration
    bltOtherCertification        working_bltOtherCertification;
    ListElement         leCurrentElement;
    Enumeration eList = bltOtherCertification_List.elements();
    %>
        <%@ include file="tOtherCertification_main_OtherCertification_PhysicianID_instructions.jsp" %>

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <a class=linkBase href = "tOtherCertification_main_OtherCertification_PhysicianID_form_create.jsp?EDIT=new&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/create_PhysicianID.gif"></a>
        <%}%>
    <%
    while (eList.hasMoreElements())
    {
         %>
         <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="3" class=tdHeaderAlt cellspacing="0" width="100%">
                     <tr> 
                       <td>
         <%

        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltOtherCertification  = (bltOtherCertification) leCurrentElement.getObject();
        working_bltOtherCertification.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        if (!working_bltOtherCertification.isComplete())
        {
            theClass = "incompleteItem";
        %>
                <span class=incompleteItem><b>Not Complete</b></span><br>
        <%
        }
        %>
               <span class=<%=theClass%> ><b>ID:&nbsp;</b><%=working_bltOtherCertification.getOtherCertID()%> </span>
                  </td></tr>
                  <tr><td><b>Item Create Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltOtherCertification.getUniqueCreateDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltOtherCertification.getUniqueModifyDate())%>" /></jsp:include></td></tr>
                  <tr><td><b>Item Modify Comments:&nbsp;</b><%=working_bltOtherCertification.getUniqueModifyComments()%></td></tr>
                  </td></tr></table>
            </td><td width="50%" bgColor=#ffffff> 
        <a class=linkBase href = "tOtherCertification_main_OtherCertification_PhysicianID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltOtherCertification.getOtherCertID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/edit_PhysicianID.gif"></a>


        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <br><a class=linkBase  onClick="return confirmDelete()"  href = "tOtherCertification_main_OtherCertification_PhysicianID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltOtherCertification.getOtherCertID()%>&KM=p"><img border=0 src="ui_<%=thePLCID%>/icons/delete_PhysicianID.gif"></a>
        <br>
        <% 
            if (working_bltOtherCertification.getDocuLinkID().intValue()>0)
            {
              bltDocumentManagement myDoc = new bltDocumentManagement(working_bltOtherCertification.getDocuLinkID());
              if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
              {
              %>
                      <a class=linkBase target=_blank href = "pdf/<%=myDoc.getDocumentFileName()%>">View Document</a>

              <%
              }
              else
              {
              %>

                      <a class=linkBase target=_blank href = "#" onClick="window.open('tDocumentManagement_main_DocumentManagement_PhysicianID_form_authorize.jsp?EDIT=faxCover&amp;EDITID=<%=working_bltOtherCertification.getDocuLinkID()%>','DocPDF','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=500');return false;" >Print Cover Sheet</a>

              <%
              }
            }
            else
            {
             if (working_bltOtherCertification.isComplete())
             {
               %>
                      <a target=_blank class=linkBase href = "tOtherCertification_ModifyDocument.jsp?EDITID=<%=working_bltOtherCertification.getOtherCertID()%>&EDIT=CREATE&dType=tOtherCertification">Create Document</a>
               <%
             }
             else
             {
               %>
                      <span class=tdBase>You must complete this item before you can attach a document</span>
               <%
             }
            }
            %>

        <%}%>
                  </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("CertificationType",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("CertificationType"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("CertificationType"))&&(!working_bltOtherCertification.isComplete("CertificationType")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Type:&nbsp;</b><%=working_bltOtherCertification.getCertificationType()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("CertificationNumber",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("CertificationNumber"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("CertificationNumber"))&&(!working_bltOtherCertification.isComplete("CertificationNumber")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Certificate/Document Number:&nbsp;</b><%=working_bltOtherCertification.getCertificationNumber()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("ExpirationDate",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("ExpirationDate"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("ExpirationDate"))&&(!working_bltOtherCertification.isComplete("ExpirationDate")) ){theClassF = "requiredFieldMain";}%>

                        <p class=<%=theClassF%> ><b>Expiration Date:&nbsp;</b><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltOtherCertification.getExpirationDate())%>" /></jsp:include></p>


<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("Name",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("Name"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("Name"))&&(!working_bltOtherCertification.isComplete("Name")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Issuing Institution:&nbsp;</b><%=working_bltOtherCertification.getName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("Address1",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("Address1"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("Address1"))&&(!working_bltOtherCertification.isComplete("Address1")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address:&nbsp;</b><%=working_bltOtherCertification.getAddress1()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("Address2",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("Address2"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("Address2"))&&(!working_bltOtherCertification.isComplete("Address2")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Address 2:&nbsp;</b><%=working_bltOtherCertification.getAddress2()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("City",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("City"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("City"))&&(!working_bltOtherCertification.isComplete("City")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Institution City:&nbsp;</b><%=working_bltOtherCertification.getCity()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("StateID",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("StateID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("StateID"))&&(!working_bltOtherCertification.isComplete("StateID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>State:&nbsp;</b><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltOtherCertification.getStateID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("Province",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("Province"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("Province"))&&(!working_bltOtherCertification.isComplete("Province")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Province, District, State:&nbsp;</b><%=working_bltOtherCertification.getProvince()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("ZIP",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("ZIP"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("ZIP"))&&(!working_bltOtherCertification.isComplete("ZIP")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>ZIP:&nbsp;</b><%=working_bltOtherCertification.getZIP()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("CountryID",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("CountryID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("CountryID"))&&(!working_bltOtherCertification.isComplete("CountryID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Country:&nbsp;</b><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltOtherCertification.getCountryID()%>" /></jsp:include></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("Phone",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("Phone"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("Phone"))&&(!working_bltOtherCertification.isComplete("Phone")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Phone (XXX-XXX-XXXX):&nbsp;</b><%=working_bltOtherCertification.getPhone()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("Fax",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("Fax"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("Fax"))&&(!working_bltOtherCertification.isComplete("Fax")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Fax (XXX-XXX-XXXX):&nbsp;</b><%=working_bltOtherCertification.getFax()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("ContactName",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("ContactName"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("ContactName"))&&(!working_bltOtherCertification.isComplete("ContactName")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact Name:&nbsp;</b><%=working_bltOtherCertification.getContactName()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("ContactEmail",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("ContactEmail"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("ContactEmail"))&&(!working_bltOtherCertification.isComplete("ContactEmail")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Contact E-mail:&nbsp;</b><%=working_bltOtherCertification.getContactEmail()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("DocuLinkID",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("DocuLinkID"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("DocuLinkID"))&&(!working_bltOtherCertification.isComplete("DocuLinkID")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>DocuLink:&nbsp;</b><%=working_bltOtherCertification.getDocuLinkID()%></p>

<%theClassF = "textBase";%>
<%if ((working_bltOtherCertification.isExpired("Comments",expiredDays))&&(working_bltOtherCertification.isExpiredCheck("Comments"))){theClassF = "expiredFieldMain";}%>
<%if ( (working_bltOtherCertification.isRequired("Comments"))&&(!working_bltOtherCertification.isComplete("Comments")) ){theClassF = "requiredFieldMain";}%>
            <p class=<%=theClassF%> ><b>Extra Comments:&nbsp;</b><%=working_bltOtherCertification.getComments()%></p>

        </td></tr></table></table><br>        <%
    }
    %>
    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
