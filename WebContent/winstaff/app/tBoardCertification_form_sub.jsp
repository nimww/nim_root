<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltBoardCertification,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tBoardCertification_form_sub.jsp
    Created on Sep/11/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iBoardCertificationID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection4", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iBoardCertificationID")) 
    {
        iBoardCertificationID        =    (Integer)pageControllerHash.get("iBoardCertificationID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltBoardCertification        BoardCertification        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        BoardCertification        =    new    bltBoardCertification(iBoardCertificationID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        BoardCertification        =    new    bltBoardCertification(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !BoardCertification.getUniqueCreateDate().equals(testObj)  )
    {
         BoardCertification.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !BoardCertification.getUniqueModifyDate().equals(testObj)  )
    {
         BoardCertification.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !BoardCertification.getUniqueModifyComments().equals(testObj)  )
    {
         BoardCertification.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID")) ;
    if ( !BoardCertification.getPhysicianID().equals(testObj)  )
    {
         BoardCertification.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification PhysicianID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Specialty")) ;
    if ( !BoardCertification.getSpecialty().equals(testObj)  )
    {
         BoardCertification.setSpecialty( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Specialty not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SubSpecialty")) ;
    if ( !BoardCertification.getSubSpecialty().equals(testObj)  )
    {
         BoardCertification.setSubSpecialty( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification SubSpecialty not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsListed")) ;
    if ( !BoardCertification.getIsListed().equals(testObj)  )
    {
         BoardCertification.setIsListed( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification IsListed not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsPrimary")) ;
    if ( !BoardCertification.getIsPrimary().equals(testObj)  )
    {
         BoardCertification.setIsPrimary( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification IsPrimary not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SpecialtyBoardCertified")) ;
    if ( !BoardCertification.getSpecialtyBoardCertified().equals(testObj)  )
    {
         BoardCertification.setSpecialtyBoardCertified( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification SpecialtyBoardCertified not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BoardName")) ;
    if ( !BoardCertification.getBoardName().equals(testObj)  )
    {
         BoardCertification.setBoardName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification BoardName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName")) ;
    if ( !BoardCertification.getContactName().equals(testObj)  )
    {
         BoardCertification.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !BoardCertification.getContactEmail().equals(testObj)  )
    {
         BoardCertification.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address1")) ;
    if ( !BoardCertification.getAddress1().equals(testObj)  )
    {
         BoardCertification.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2")) ;
    if ( !BoardCertification.getAddress2().equals(testObj)  )
    {
         BoardCertification.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City")) ;
    if ( !BoardCertification.getCity().equals(testObj)  )
    {
         BoardCertification.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("StateID")) ;
    if ( !BoardCertification.getStateID().equals(testObj)  )
    {
         BoardCertification.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification StateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province")) ;
    if ( !BoardCertification.getProvince().equals(testObj)  )
    {
         BoardCertification.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP")) ;
    if ( !BoardCertification.getZIP().equals(testObj)  )
    {
         BoardCertification.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID")) ;
    if ( !BoardCertification.getCountryID().equals(testObj)  )
    {
         BoardCertification.setCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone")) ;
    if ( !BoardCertification.getPhone().equals(testObj)  )
    {
         BoardCertification.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Fax")) ;
    if ( !BoardCertification.getFax().equals(testObj)  )
    {
         BoardCertification.setFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Fax not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("BoardDateInitialCertified"))) ;
    if ( !BoardCertification.getBoardDateInitialCertified().equals(testObj)  )
    {
         BoardCertification.setBoardDateInitialCertified( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification BoardDateInitialCertified not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("BoardDateRecertified"))) ;
    if ( !BoardCertification.getBoardDateRecertified().equals(testObj)  )
    {
         BoardCertification.setBoardDateRecertified( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification BoardDateRecertified not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ExpirationDate"))) ;
    if ( !BoardCertification.getExpirationDate().equals(testObj)  )
    {
         BoardCertification.setExpirationDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification ExpirationDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DocumentNumber")) ;
    if ( !BoardCertification.getDocumentNumber().equals(testObj)  )
    {
         BoardCertification.setDocumentNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification DocumentNumber not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("IfNotCert")) ;
    if ( !BoardCertification.getIfNotCert().equals(testObj)  )
    {
         BoardCertification.setIfNotCert( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification IfNotCert not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CertEligible")) ;
    if ( !BoardCertification.getCertEligible().equals(testObj)  )
    {
         BoardCertification.setCertEligible( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification CertEligible not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("EligibleStartDate"))) ;
    if ( !BoardCertification.getEligibleStartDate().equals(testObj)  )
    {
         BoardCertification.setEligibleStartDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification EligibleStartDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("EligibleExpirationDate"))) ;
    if ( !BoardCertification.getEligibleExpirationDate().equals(testObj)  )
    {
         BoardCertification.setEligibleExpirationDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification EligibleExpirationDate not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CertPlanningToTake")) ;
    if ( !BoardCertification.getCertPlanningToTake().equals(testObj)  )
    {
         BoardCertification.setCertPlanningToTake( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification CertPlanningToTake not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("CertPlanDate"))) ;
    if ( !BoardCertification.getCertPlanDate().equals(testObj)  )
    {
         BoardCertification.setCertPlanDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification CertPlanDate not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DocuLinkID")) ;
    if ( !BoardCertification.getDocuLinkID().equals(testObj)  )
    {
         BoardCertification.setDocuLinkID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification DocuLinkID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !BoardCertification.getComments().equals(testObj)  )
    {
         BoardCertification.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("BoardCertification Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tBoardCertification","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	BoardCertification.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    BoardCertification.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        BoardCertification.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }	//Doculink Addition
	//Doculink Does Exist, prompt to Modify
	Integer iPhysicianID = BoardCertification.getPhysicianID();
     if (!errorRouteMe&&BoardCertification.getDocuLinkID().intValue()>0)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement(BoardCertification.getDocuLinkID());
            if (myDoc.getDocumentFileName().equalsIgnoreCase("")) 
            {
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tBoardCertification";
                myDUtils.setDocumentType(myDType, BoardCertification.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
            }
            else
            {
                //archive then create new
                myDoc.setArchived(new Integer("1"));
                myDoc.commitData();
                myDoc = new bltDocumentManagement();
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tBoardCertification";
                myDUtils.setDocumentType(myDType, BoardCertification.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
                BoardCertification.setDocuLinkID(myDoc.getUniqueID());
                BoardCertification.commitData();
            }
     }
     else if (!errorRouteMe)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement();
            DocumentManagerUtils myDUtils = new DocumentManagerUtils();
            String myDType = "tBoardCertification";
            myDUtils.setDocumentType(myDType, BoardCertification.getUniqueID());
            myDoc.setDocumentName(myDUtils.getDocumentName());
            myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
            myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
            myDoc.setArchived(new Integer(2));
            myDoc.commitData();
            BoardCertification.setDocuLinkID(myDoc.getUniqueID());
            BoardCertification.commitData();
     }

    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
