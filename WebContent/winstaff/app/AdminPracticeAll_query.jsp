
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tAdminPracticeLU_main_LU_AdminID.jsp
    Created on Apr/23/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=100%>
    <tr><td width=10>&nbsp;</td>
    <td>
    <span class=title>Imaging Centers &amp; Practices</span>


    <%
//initial declaration of list class and parentID
    Integer        iGenAdminID        =    null;
	String		savedSQL = null;
	String		savedURL = null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iGenAdminID")) 
    {
        iGenAdminID        =    (Integer)pageControllerHash.get("iGenAdminID");
        accessValid = true;
    }
    if (pageControllerHash.containsKey("SQL_AdminPracticeAll_Query")) 
    {
        savedSQL        =    (String)pageControllerHash.get("SQL_AdminPracticeAll_Query");
//        accessValid = true;
    }
    if (pageControllerHash.containsKey("URL_AdminPracticeAll_Query")) 
    {
        savedURL        =    (String)pageControllerHash.get("URL_AdminPracticeAll_Query");
//        accessValid = true;
    }
  //page security
  if (accessValid)
  {



		java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
//      pageControllerHash.put("sParentReturnPage","AdminPracticeAll_query.jsp");

%>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td> 
<%
try
{


String myPracticeID = "";
String myModalityTypeID = "-1";
String mySpecialServicesID = "0";
String myActivityOnly = "-1";
String myDueBy= "";
String myContractingStatusID = "";
String myRelationshipTypeID = "";
String myPracticeName = "";
String myownername2 = "";
String myOfficeAddress1 = "";
String myOfficeCity = "";
String myOfficeStateID = "0";
String myOfficeZip = "";
String myZCCounty = "";
String orderBy = "firstname";
int startID = 0;
int maxResults = 200;

boolean firstDisplay = false;

try
{
	maxResults = Integer.parseInt(request.getParameter("maxResults"));
	startID = Integer.parseInt(request.getParameter("startID"));
	myPracticeID = request.getParameter("PracticeID");
	myModalityTypeID = request.getParameter("ModalityTypeID");
	mySpecialServicesID = request.getParameter("SpecialServicesID");
	myActivityOnly = request.getParameter("ActivityOnly");
	myDueBy = request.getParameter("dueBy");
	myContractingStatusID = request.getParameter("ContractingStatusID");
	myRelationshipTypeID = request.getParameter("RelationshipTypeID");
	myPracticeName = request.getParameter("PracticeName");
	myownername2 = request.getParameter("ownername2");
	myOfficeAddress1 = request.getParameter("OfficeAddress1");
	myOfficeCity = request.getParameter("OfficeCity");
	myOfficeStateID = request.getParameter("OfficeStateID");
	myOfficeZip = request.getParameter("OfficeZip");
	myZCCounty = request.getParameter("ZCCounty");
	orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
	//out.print("--------------"  + e);
	maxResults = 200;
	startID = 0;
	firstDisplay = true;
	myPracticeID = "";
	myModalityTypeID = "-1";
	mySpecialServicesID = "0";
	myActivityOnly ="0";
	myDueBy = "";
	myContractingStatusID = "";
	myRelationshipTypeID = "";
	myPracticeName = "";
	myownername2 = "";
	myOfficeAddress1 = "";
	myOfficeCity = "";
	myOfficeStateID = "0";
	myOfficeZip = "";
	myZCCounty = "";
	orderBy = "PracticeID";
}
if (orderBy == null)
{
	out.print("-------------- order by null" );
	maxResults = 200;
	startID = 0;
	firstDisplay = true;
	myPracticeID = "";
	myModalityTypeID = "-1";
	mySpecialServicesID = "0";
	myActivityOnly ="0";
	myDueBy = "";
	myContractingStatusID = "";
	myRelationshipTypeID = "";
	myownername2 = "";
	myPracticeName = "";
	myOfficeAddress1 = "";
	myOfficeCity = "";
	myOfficeStateID = "0";
	myOfficeZip = "";
	myZCCounty = "";
	orderBy = "PracticeID";
}


if (firstDisplay&&savedURL==null)
{
%>



<table width = 100% cellpadding=2 cellspacing=2>
<tr>

<td>
<form name="form1" method="get" action="AdminPracticeAll_query.jsp" id="apsearchform1">
  <table width="400" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#003333">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
        <tr>
          <td class=tdHeaderAlt>
            Practice ID
            </td>
          <td>
            <input type=text name="PracticeID">
            </td>
        </tr>
        <tr>
          <td class="tdHeaderAlt"> Account Name </td>
          <td><input name="ownername2" type="text" id="ownername2" /></td>
        </tr>
        <tr>
          <td class="tdHeaderAlt"> Relationship</td>
          <td><select name="RelationshipTypeID" id="RelationshipTypeID" ><option  value=0>--</option>
<option  value=1>Owner</option>
<option  value=2 selected="selected">Admin</option>
<option  value=3>Broker</option></select>
</td>
        </tr>
        <tr>
          <td class="tdHeaderAlt">Status</td>
          <td><select name="ContractingStatusID" id="ContractingStatusID" >
            <option <%if (myContractingStatusID.equalsIgnoreCase("-1") ) {out.println("selected");}%> value="-1">Show All</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("-20") ) {out.println("selected");}%> value="-20">OTA + Contracted</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("-30") ) {out.println("selected");}%> value="-40">All Contracted</option>            
            <option <%if (myContractingStatusID.equalsIgnoreCase("-1") ) {out.println("selected");}%> value="-1">----</option> 
            <option <%if (myContractingStatusID.equalsIgnoreCase("0") ) {out.println("selected");}%> value="0">None</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("3") ) {out.println("selected");}%> value="3">Target</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("5") ) {out.println("selected");}%> value="5">REM: Send Contract</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("1") ) {out.println("selected");}%> value="1">OTA</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("7") ) {out.println("selected");}%> value="7">OON</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("2") ) {out.println("selected");}%> value="2">Contracted</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("9") ) {out.println("selected");}%> value="9">Contracted [*]</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("8") ) {out.println("selected");}%> value="8">Contracted [Select]</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("4") ) {out.println("selected");}%> value="4">Not Interested</option>
          </select></td>
        </tr>
        <tr>
          <td class="tdHeaderAlt"> Practice Name </td>
          <td><input type="text" name="PracticeName" /></td>
          </tr>
        <tr>
          <td class="tdHeaderAlt"> Address1</td>
          <td><input name="OfficeAddress1" type="text" id="OfficeAddress1" /></td>
        </tr>
        <tr>
          <td class=tdHeaderAlt>City</td>
          <td>
            <input type=text name="OfficeCity">
            </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         State
         </td>
         <td>
              <select name="OfficeStateID">
                <jsp:include page="../generic/tStateLILong.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="0" />
                </jsp:include>
              </select>
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
ZIP
         </td>
         <td>
         <input type=text name="OfficeZip">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
        County
         </td>
         <td>
         <input type=text name="ZCCounty">
         </td>
        </tr>

            <tr>
            <td class=tdHeaderAlt><p>Modality:</p></td>
            <td> 
      <select name="ModalityTypeID" id="ModalityTypeID" >
<option <%if (myModalityTypeID.equalsIgnoreCase("-1") ) {out.println("selected");}%> selected="selected" value="-1">All</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("1") ) {out.println("selected");}%>  value=1>MRI</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("2") ) {out.println("selected");}%> value=2>CT</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("3") ) {out.println("selected");}%> value=3>BMD</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("4") ) {out.println("selected");}%> value=4>FL</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("5") ) {out.println("selected");}%> value=5>MG</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("6") ) {out.println("selected");}%> value=6>NM</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("7") ) {out.println("selected");}%> value=7>US</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("8") ) {out.println("selected");}%> value=8>XR</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("9") ) {out.println("selected");}%> value=9>EMG</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("10") ) {out.println("selected");}%> value=10>PET/CT</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("15") ) {out.println("selected");}%> value=15>Discogram</option>
</select>
            </td>
          </tr>
            <tr>
              <td class="tdHeaderAlt"><p>Special-Services:</p></td>
              <td> <select name="SpecialServicesID" id="SpecialServicesID" >
                <option <%if (mySpecialServicesID.equalsIgnoreCase("0") ) {out.println("selected");}%>  value="0">Show All</option>
                <option <%if (mySpecialServicesID.equalsIgnoreCase("1") ) {out.println("selected");}%>  value="1">Has Arthrogram</option>
                <option <%if (mySpecialServicesID.equalsIgnoreCase("2") ) {out.println("selected");}%> value="2">Has Myelogram</option>
                <option <%if (mySpecialServicesID.equalsIgnoreCase("3") ) {out.println("selected");}%>  value="3">Has Both</option>
              </select></td>
            </tr>
            <tr>
              <td class=tdHeaderAlt>Max Results:</td>
              <td><select class="tdBaseAltYellow"  name="maxResults" id="maxResults">
                  <option value="25" <%if (maxResults ==25){out.println(" selected");}%> >25</option>
                  <option value="50" <%if (maxResults ==50){out.println(" selected");}%> >50</option>
                  <option value="75" <%if (maxResults ==75){out.println(" selected");}%> >75</option>
                  <option value="100" <%if (maxResults ==100){out.println(" selected");}%> >100</option>
                  <option value="150" <%if (maxResults ==150){out.println(" selected");}%> >150</option>
                  <option value="200" <%if (maxResults ==200){out.println(" selected");}%> >200</option>
                  <option value="250" <%if (maxResults ==250){out.println(" selected");}%> >250</option>
                  <option value="300" <%if (maxResults ==300){out.println(" selected");}%> >300</option>
                  <option value="400" <%if (maxResults ==400){out.println(" selected");}%> >400</option>
                  <option value="500" <%if (maxResults ==500){out.println(" selected");}%> >500</option>
                  <option value="600" <%if (maxResults ==600){out.println(" selected");}%> >600</option>
                  <option value="700" <%if (maxResults ==700){out.println(" selected");}%> >700</option>
                  <option value="800" <%if (maxResults ==800){out.println(" selected");}%> >800</option>
                </select></td>
            </tr>
            <tr>
              <td class="tdHeaderAlt">View Only Records w/Activity:</td>
              <td><select class="tdBaseAltYellow"  name="ActivityOnly" id="ActivityOnly">
                <option value="0" <%if (maxResults ==0){out.println(" selected");}%> >No</option>
                <option value="1" <%if (maxResults ==1){out.println(" selected");}%> >Yes</option>
              </select> 
                <br />
                Due By:
<input name="dueBy" class="tdBaseAltYellow"  type="text" id="dueBy" size="10" maxlength="20" /></td>
            </tr>
            <tr >
              <td class=tdHeaderAlt>Sort:</td>
              <td><select name="orderBy" id="orderBy">
                <option value="PracticeID" <%if (orderBy.equalsIgnoreCase("PracticeID")){out.println(" selected");}%> >ID</option>
                <option value="PracticeName" <%if (orderBy.equalsIgnoreCase("PracticeName")){out.println(" selected");}%> >Practice Name</option>
                <option value="OfficeCity" <%if (orderBy.equalsIgnoreCase("OfficeCity")){out.println(" selected");}%> >City</option>
                <option value="OfficeStateID" <%if (orderBy.equalsIgnoreCase("OfficeStateID")){out.println(" selected");}%> >State</option>
                <option value="OfficeZip" <%if (orderBy.equalsIgnoreCase("OfficeZip")){out.println(" selected");}%> >Postal Code</option>
                <!--option value="ownername2" <%if (orderBy.equalsIgnoreCase("ownername2")){out.println(" selected");}%> >Owner Name</option-->
              </select></td>
            </tr>
            <tr class=tdHeaderAlt> 
            <td> 
              <input type=hidden name="startID" value=0></td>
            <td><input type="submit" name="Submit2" value="Submit" /></td>
          </tr>
        </table>
</td>
    </tr>
  </table>
  </form>
<p> 
  <%
}
else if (firstDisplay&&savedURL!=null)
{
pageControllerHash.remove("URL_AdminPracticeAll_MRI_query");
session.setAttribute("pageControllerHash",pageControllerHash);

%>
    <script language="javascript">
//    alert( '<%=savedURL%>');
    document.location = '<%=savedURL%>';

    </script>




    <%
}
else
{

%>
  <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</p>
<table width=100% border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
<tr>
<td>
<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
<tr>
<td>
<form name="selfForm" method="get" action="AdminPracticeAll_query.jsp" id="apsearchform1">
  <table border=0 cellspacing=2 width='100%' cellpadding=3>
    <tr > 
      <td colspan=5 class=tdHeader>
      <table border="1" cellspacing="0" cellpadding="2">
  <tr class=tdBaseAlt>
    <td rowspan="3" align="center" valign="middle" class="title">Map<br />
      Tools</td>
    <td>Accounts/Practices</td>
    <td>Modality</td>
  </tr>
  <tr class=tdBase>
    <td><a href="maps/map2js.jsp" target="mapResults" >Map Results</a>&nbsp;</td>
    <td><a href="maps/map2js.jsp?mty=mod1" target="mapResults">Map Modality</a></td>
  </tr>
  <tr class=tdBase>
    <td><a href="maps/map2js.jsp?mty=green" target="mapResults">Map in Green</a>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

      
   </td>
      <td colspan=2 class=tdBase align=right><p>Sort:
          <select name=orderBy>
            <option value=PracticeID <%if (orderBy.equalsIgnoreCase("PracticeID")){out.println(" selected");}%> >ID</option>
            <option value=PracticeName <%if (orderBy.equalsIgnoreCase("PracticeName")){out.println(" selected");}%> >Practice Name</option>
            <option value=OfficeCity <%if (orderBy.equalsIgnoreCase("OfficeCity")){out.println(" selected");}%> >City</option>
            <option value=OfficeStateID <%if (orderBy.equalsIgnoreCase("OfficeStateID")){out.println(" selected");}%> >State</option>
            <option value=OfficeZip <%if (orderBy.equalsIgnoreCase("OfficeZip")){out.println(" selected");}%> >Postal Code</option>
                <!--option value="ownername2" <%if (orderBy.equalsIgnoreCase("ownername2")){out.println(" selected");}%> >Owner Name</option-->
            </select>
       <br/>        Max Results: 
          <select  class="tdBaseAltYellow"  name="maxResults">
            <option value="25" <%if (maxResults ==25){out.println(" selected");}%> >25</option>
            <option value="50" <%if (maxResults ==50){out.println(" selected");}%> >50</option>
            <option value="75" <%if (maxResults ==75){out.println(" selected");}%> >75</option>
            <option value="100" <%if (maxResults ==100){out.println(" selected");}%> >100</option>
            <option value="150" <%if (maxResults ==150){out.println(" selected");}%> >150</option>
            <option value="200" <%if (maxResults ==200){out.println(" selected");}%> >200</option>
            <option value="250" <%if (maxResults ==250){out.println(" selected");}%> >250</option>
            <option value="300" <%if (maxResults ==300){out.println(" selected");}%> >300</option>
            <option value="400" <%if (maxResults ==400){out.println(" selected");}%> >400</option>
            <option value="500" <%if (maxResults ==500){out.println(" selected");}%> >500</option>
            <option value="600" <%if (maxResults ==600){out.println(" selected");}%> >600</option>
            <option value="700" <%if (maxResults ==700){out.println(" selected");}%> >700</option>
            <option value="800" <%if (maxResults ==800){out.println(" selected");}%> >800</option>
            <option value="2000" <%if (maxResults ==2000){out.println(" selected");}%> >2000</option>
            </select>
          <br /><input type="button" class="tdBaseAltRed"  onclick="document.getElementById('ownername2').value='';document.getElementById('PracticeID').value='';document.getElementById('OfficeCity').value='';document.getElementById('OfficeStateID').value='0';document.getElementById('OfficeZip').value='';document.getElementById('PracticeName').value='';document.getElementById('ZCCounty').value='';document.getElementById('ModalityTypeID').value='-1';document.getElementById('ZCCounty').value='';document.getElementById('OfficeAddress1').value='';document.getElementById('ContractingStatusID').value=-1;document.getElementById('SpecialServicesID').value=0;"  value="Clear Search" align="middle" />
      </p>       </td>    
      <td colspan="2" nowrap class=tdHeader align="center"> 
        <input type=hidden name=startID value=0 >
        
        Special: 
        <select name="SpecialServicesID" id="SpecialServicesID" >
                <option <%if (mySpecialServicesID.equalsIgnoreCase("0") ) {out.println("selected");}%>  value="0">Show All</option>
                <option <%if (mySpecialServicesID.equalsIgnoreCase("1") ) {out.println("selected");}%>  value="1">Has Arthrogram</option>
                <option <%if (mySpecialServicesID.equalsIgnoreCase("2") ) {out.println("selected");}%> value="2">Has Myelogram</option>
                <option <%if (mySpecialServicesID.equalsIgnoreCase("3") ) {out.println("selected");}%>  value="3">Has Both</option>
              </select>
        
        
        </td>

      <td colspan="3" align="center" class=tdHeader> 
        <p  align="center"> View Only Records w/Activity?
          <select class="tdBaseAltYellow"  name="ActivityOnly" id="ActivityOnly" onchange="javascript:if (this.value==0){document.getElementById('dueBy').className='tdBaseAltGrey';} else {document.getElementById('dueBy').className='tdBaseAltYellow';}">
                <option value="0" <%if (myActivityOnly.equalsIgnoreCase("0")){out.println(" selected");}%> >No</option>
                <option value="1" <%if (myActivityOnly.equalsIgnoreCase("1")){out.println(" selected");}%> >Yes</option>
              </select>
          <br />
        Due by: 
        <label>
          <input name="dueBy" <%if (myActivityOnly.equalsIgnoreCase("0")){out.println(" class=\"tdBaseAltGrey\" ");} else {out.println(" class=\"tdBaseAltYellow\" ");}%>  type="text" id="dueBy"  value="<%=myDueBy%>" size="10"  maxlength="20" />
        </label>
        </p>
      </td>
      <td align="center" class=tdHeader><input type="Submit" class="tdBaseAltGreen"  onclick="this.disabled=true;document.selfForm.submit();" name="Submit" value="Submit" align="middle" /><br />
</td>
    </tr>
    <tr class=tdHeader> 
      <td  colspan=1>&nbsp;      </td>
      <td  colspan=1>&nbsp;      </td>
      <td><select name="RelationshipTypeID" id="RelationshipTypeID" >
<option <%if (myRelationshipTypeID.equalsIgnoreCase("0") ) {out.println("selected");}%> value=0>--</option>
<option <%if (myRelationshipTypeID.equalsIgnoreCase("1") ) {out.println("selected");}%> value=1>Owner</option>
<option <%if (myRelationshipTypeID.equalsIgnoreCase("2") ) {out.println("selected");}%> value=2>Admin</option>
<option <%if (myRelationshipTypeID.equalsIgnoreCase("3") ) {out.println("selected");}%> value=3>Broker</option></select></td>
      <td colspan=1> 
        <input type="text" name="ownername2" id="ownername2" value="<%=myownername2%>" size="20">
      </td>
      <td colspan=1> 
        <input type="text" name="PracticeID" id="PracticeID"  value="<%=myPracticeID%>" size="10">
      </td>
      <td colspan=1> 
        <select name="ContractingStatusID" id="ContractingStatusID" >
            <option <%if (myContractingStatusID.equalsIgnoreCase("-1") ) {out.println("selected");}%> value="-1">Show All</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("-20") ) {out.println("selected");}%> value="-20">OTA + Contracted</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("-40") ) {out.println("selected");}%> value="-40">All Contracted</option>
            <option  value="-1">----</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("0") ) {out.println("selected");}%> value="0">None</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("3") ) {out.println("selected");}%> value="3">Target</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("5") ) {out.println("selected");}%> value="5">REM: Send Contract</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("1") ) {out.println("selected");}%> value="1">OTA</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("7") ) {out.println("selected");}%> value="7">OON</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("2") ) {out.println("selected");}%> value="2">Contracted</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("9") ) {out.println("selected");}%> value="9">Contracted [*]</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("8") ) {out.println("selected");}%> value="8">Contracted [Select]</option>
            <option <%if (myContractingStatusID.equalsIgnoreCase("4") ) {out.println("selected");}%> value="4">Not Interested</option>
        </select></td>
      <td><input type="text" name="PracticeName"  id="PracticeName" value="<%=myPracticeName%>" size="20" /></td>
      <td><input type="text" name="OfficeAddress1"  id="OfficeAddress1" value="<%=myOfficeAddress1%>" size="20" /></td>
      <td colspan=1> 
        <input type="text" name="OfficeCity" id="OfficeCity" value="<%=myOfficeCity%>" size="10">
      </td>
      <td colspan=1> 
              <select name="OfficeStateID"  id="OfficeStateID" >
                <jsp:include page="../generic/tStateLIShort.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myOfficeStateID%>" />
                </jsp:include>
              </select>
      </td>
      <td colspan=1> 
        <input name="OfficeZip" type="text" id="OfficeZip" value="<%=myOfficeZip%>" size="10" maxlength="5">
      </td>
      <td colspan=1> 
        <input type="text" name="ZCCounty" id="ZCCounty" value="<%=myZCCounty%>" size="15">
      </td>
      <td>
      <select name="ModalityTypeID" id="ModalityTypeID" >
<option <%if (myModalityTypeID.equalsIgnoreCase("-1") ) {out.println("selected");}%> value="-1">All</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("3") ) {out.println("selected");}%> value=3>BMD</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("2") ) {out.println("selected");}%> value=2>CT</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("9") ) {out.println("selected");}%> value=9>EMG</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("4") ) {out.println("selected");}%> value=4>FL</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("5") ) {out.println("selected");}%> value=5>MG</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("1") ) {out.println("selected");}%> value=1>MRI</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("6") ) {out.println("selected");}%> value=6>NM</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("10") ) {out.println("selected");}%> value=10>PET/CT</option> 
<option <%if (myModalityTypeID.equalsIgnoreCase("7") ) {out.println("selected");}%> value=7>US</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("8") ) {out.println("selected");}%> value=8>XR</option>
<option <%if (myModalityTypeID.equalsIgnoreCase("15") ) {out.println("selected");}%> value=15>Discogram</option>

</select>
</td>
    </tr>
    <tr class=tdHeader> 
    <td>#</td>
    <td>Action</td>
      <td>Rel.</td>
      <td colspan=1> 
        Account 
      </td>
      <td colspan=1> 
        PracticeID
      </td>
      <td colspan=1> 
        Status</td>
      <td>Name</td>
      <td>Address</td>
      <td colspan=1> 
        City
      </td>
      <td colspan=1> 
        State
      </td>
      <td colspan=1> 
ZIP
      </td>
      <td colspan=1> 
        County
      </td>
      <td>&nbsp;</td>
    </tr>
 

<%



		//String myWhere = "where ( (PracticeID IN (select PracticeID from tAdminPracticeLU where (adminID='"+iGenAdminID+"')))	 ";
	//String myWhere = "where ( adminID="+iGenAdminID;
	//String myWhere = "where  (adminID="+iGenAdminID+") and (practiceID in (select practiceID from tPracticeMaster where (practiceID>0";
	String myWhere = "where   (tAdminPracticeLU.PracticeID=tPracticeMaster.PracticeID) and (tPracticeMaster.PracticeID>0";
	
	boolean theFirst = false;

if (!firstDisplay)
{
	try
	{
	if (!myPracticeID.equalsIgnoreCase(""))
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "tPracticeMaster.PracticeID = '" + DataControlUtils.fixApostrophe(myPracticeID) +"'";
		theFirst = false;
	}
	if (myModalityTypeID.equalsIgnoreCase("-1"))
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "tNIM3_Modality.ModalityTypeID >0";
		theFirst = false;
	}
	else if (!myModalityTypeID.equalsIgnoreCase(""))
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "tNIM3_Modality.ModalityTypeID = '" + DataControlUtils.fixApostrophe(myModalityTypeID) +"'";
		theFirst = false;
	}
	if (!myRelationshipTypeID.equalsIgnoreCase(""))
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "tAdminPracticeLU.RelationshipTypeID = '" + DataControlUtils.fixApostrophe(myRelationshipTypeID) +"'";
		theFirst = false;
	}
	if (!myPracticeName.equalsIgnoreCase(""))
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "UPPER(tPracticeMaster.PracticeName) LIKE UPPER('%" + DataControlUtils.fixApostrophe(myPracticeName) +"%')";
		theFirst = false;
	}
	if (!mySpecialServicesID.equalsIgnoreCase("")&&!mySpecialServicesID.equalsIgnoreCase("0"))
	{
			if (mySpecialServicesID.equalsIgnoreCase("1"))
			{
				if (!theFirst) { myWhere+=" and ";}
				myWhere += "tPracticeMaster.DiagnosticMDArthrogram = 1";
				theFirst = false;
			}
			else if (mySpecialServicesID.equalsIgnoreCase("2"))
			{
				if (!theFirst) { myWhere+=" and ";}
				myWhere += "tPracticeMaster.DiagnosticMDMyelogram = 1";
				theFirst = false;
			}
			else if (mySpecialServicesID.equalsIgnoreCase("3"))
			{
				if (!theFirst) { myWhere+=" and ";}
				myWhere += "tPracticeMaster.DiagnosticMDArthrogram = 1 AND tPracticeMaster.DiagnosticMDMyelogram = 1";
				theFirst = false;
			}
	}

	if (!myContractingStatusID.equalsIgnoreCase("")&&!myContractingStatusID.equalsIgnoreCase("-1"))
	{
			if (myContractingStatusID.equalsIgnoreCase("-10"))
			{
				if (!theFirst) { myWhere+=" and ";}
				myWhere += "tPracticeMaster.ContractingStatusID in (1,6)";
				theFirst = false;
			}
			else if (myContractingStatusID.equalsIgnoreCase("-20"))
			{
				if (!theFirst) { myWhere+=" and ";}
				myWhere += "tPracticeMaster.ContractingStatusID in (1,6,2,8,9,10,11)";
				theFirst = false;
			}
			else if (myContractingStatusID.equalsIgnoreCase("-30"))
			{
				if (!theFirst) { myWhere+=" and ";}
				myWhere += "tPracticeMaster.ContractingStatusID in (1,2,8,9,10,11)";
				theFirst = false;
			}
			else if (myContractingStatusID.equalsIgnoreCase("-40"))
			{
				if (!theFirst) { myWhere+=" and ";}
				myWhere += "tPracticeMaster.ContractingStatusID in (8,2,9,10,11)";
				theFirst = false;
			}
			else 
			{
				if (!theFirst) { myWhere+=" and ";}
				myWhere += "tPracticeMaster.ContractingStatusID = '" + DataControlUtils.fixApostrophe(myContractingStatusID) +"'";
				theFirst = false;
			}
	}
	if (!myownername2.equalsIgnoreCase(""))
	{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "UPPER(tAdminMaster.Name) LIKE UPPER('%" + DataControlUtils.fixApostrophe(myownername2) +"%')";
	theFirst = false;
	}
	if (!myOfficeCity.equalsIgnoreCase(""))
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "UPPER(tPracticeMaster.OfficeCity) LIKE UPPER('%" + DataControlUtils.fixApostrophe(myOfficeCity) +"%')";
		theFirst = false;
	}
	if (!myOfficeAddress1.equalsIgnoreCase(""))
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "UPPER(tPracticeMaster.OfficeAddress1) LIKE UPPER('%" + DataControlUtils.fixApostrophe(myOfficeAddress1) +"%')";
		theFirst = false;
	}
	if (!myZCCounty.equalsIgnoreCase(""))
	{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "UPPER(zip_code.county) LIKE UPPER('%" + DataControlUtils.fixApostrophe(myZCCounty) +"%')";
	theFirst = false;
	}
	if (!myOfficeStateID.equalsIgnoreCase("0"))
	{
	if (!myOfficeStateID.equalsIgnoreCase(""))
	{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "tPracticeMaster.OfficeStateID = " + myOfficeStateID +"";
	theFirst = false;
	}
	}
	if (!myOfficeZip.equalsIgnoreCase(""))
	{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "tPracticeMaster.OfficeZip LIKE '%" + myOfficeZip +"%'";
	theFirst = false;
	}
	myWhere += ")";
	//myWhere += ") ))";
	
	//System.out.println(myWhere);
	if (theFirst||myWhere.equalsIgnoreCase(")"))
	{
	myWhere = "";
	}
	
	
	}
	catch(Exception e)
	{
	out.println("FFF:"+e);
	}

}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
String mySQL = "";
String mySQLMap = "";
if (true||!firstDisplay)
{
//	mySQL = ("select tAdminMaster.Name as ownername2,tAdminMaster.AdminID as AdminID, ZIP_CODE.county as ZCCounty, tPracticeMaster.PracticeID, tPracticeMaster.PracticeName,tPracticeMaster.OfficeCity,tPracticeMaster.OfficeStateID,tPracticeMaster.OfficeZIP,tAdminPracticeLU.PracticeID, tAdminPracticeLU.LookupID from tAdminMaster INNER JOIN tAdminPracticeLU on tAdminPracticeLU.adminid=tAdminMaster.adminID INNER JOIN tPracticeMaster on tPracticeMaster.practiceid = tAdminPracticeLU.practiceid INNER JOIN ZIP_CODE on ZIP_CODE.zip_code = substring(tPracticeMaster.OfficeZip,0,6) " + myWhere + "order by tPracticeMaster." + orderBy + " LIMIT " + (maxResults+1) + " OFFSET " + startID + "");	
	if (myActivityOnly.equalsIgnoreCase("1"))
	{
		try
		{
			java.util.Date myDueByDate = PLCUtils.getSubDate(myDueBy);
			myWhere+= " AND  (  (tPracticeActivity.isCompleted in (0,2) AND tPracticeActivity.RemindDate - interval '1 day' <='" +  dbdf.format(myDueByDate) + "' AND tPracticeActivity.RemindDate>'1900-01-01') OR (tPracticeActivity_Admin.isCompleted in (0,2) AND  tPracticeActivity_Admin.RemindDate  - interval '1 day' <='" +  dbdf.format(myDueByDate) + "' AND tPracticeActivity_Admin.RemindDate>'1900-01-01')  )";
		}
		catch(Exception eDate)
		{
//			out.print(eDate);
		}
	}
//	mySQL = ("select max(tPracticeActivity.reminddate) as reminddate, max(tPracticeActivity_Admin.reminddate) AS reminddate_admin ,max(tPracticeActivity.iscompleted) as iscompleted, max(tPracticeActivity_Admin.iscompleted) AS iscompleted_admin, tAdminPracticeLU.relationshiptypeid, tNIM3_Modality.modalitytypeid, tmri_modelli.isopen, tmri_modelli.teslastrength, tPracticeMaster.ContractingStatusID, tPracticeMaster.DiagnosticMDArthrogram, tPracticeMaster.DiagnosticMDMyelogram, tAdminMaster.Name as ownername2,tAdminMaster.AdminID as AdminID, ZIP_CODE.county as ZCCounty, tPracticeMaster.PracticeID, tPracticeMaster.PracticeName, tPracticeMaster.OfficeAddress1, tPracticeMaster.OfficeCity, tPracticeMaster.OfficeStateID,tPracticeMaster.OfficeZIP,tAdminPracticeLU.PracticeID, tAdminPracticeLU.LookupID from tAdminMaster INNER JOIN tAdminPracticeLU on tAdminPracticeLU.adminid=tAdminMaster.adminID INNER JOIN tPracticeMaster on tPracticeMaster.practiceid = tAdminPracticeLU.practiceid LEFT JOIN ZIP_CODE on ZIP_CODE.zip_code = substring(tPracticeMaster.OfficeZip,0,6) INNER JOIN tNIM3_Modality on tNIM3_Modality.practiceid=tPracticeMaster.practiceid LEFT JOIN tmri_modelli on tmri_modelli.mri_modelid=tNIM3_Modality.modalitymodelid  LEFT JOIN tPracticeActivity on tPracticeActivity.practiceid = tPracticeMaster.practiceid LEFT JOIN tPracticeActivity AS tPracticeActivity_Admin  on tPracticeActivity_Admin.adminid = tAdminMaster.adminid " + myWhere + " group by tmri_modelli.isopen, tmri_modelli.teslastrength,  tAdminPracticeLU.relationshiptypeid, tNIM3_Modality.modalitytypeid, tPracticeMaster.ContractingStatusID, tPracticeMaster.DiagnosticMDArthrogram, tPracticeMaster.DiagnosticMDMyelogram, tAdminMaster.Name, tAdminMaster.AdminID, ZIP_CODE.county, tPracticeMaster.PracticeID, tPracticeMaster.PracticeName, tPracticeMaster.OfficeAddress1, tPracticeMaster.OfficeCity, tPracticeMaster.OfficeStateID,tPracticeMaster.OfficeZIP,tAdminPracticeLU.PracticeID, tAdminPracticeLU.LookupID  order by tPracticeMaster." + orderBy + " LIMIT " + maxResults);	

	mySQL = ("select max(tPracticeActivity.reminddate) as reminddate, max(tPracticeActivity_Admin.reminddate) AS reminddate_admin ,max(tPracticeActivity.iscompleted) as iscompleted, max(tPracticeActivity_Admin.iscompleted) AS iscompleted_admin, tAdminPracticeLU.relationshiptypeid, tNIM3_Modality.modalitytypeid, tmri_modelli.isopen, tmri_modelli.teslastrength, tPracticeMaster.ContractingStatusID, tPracticeMaster.DiagnosticMDArthrogram, tPracticeMaster.DiagnosticMDMyelogram, tAdminMaster.Name as ownername2,tAdminMaster.AdminID as AdminID, ZIP_CODE.county as ZCCounty, tPracticeMaster.PracticeID, tPracticeMaster.PracticeName, tPracticeMaster.OfficeAddress1,coalesce(tPracticeMaster.OfficeAddress2,'') OfficeAddress2, tPracticeMaster.OfficeCity, tPracticeMaster.OfficeStateID,tPracticeMaster.OfficeZIP,tAdminPracticeLU.PracticeID, tAdminPracticeLU.LookupID from tAdminMaster INNER JOIN tAdminPracticeLU on tAdminPracticeLU.adminid=tAdminMaster.adminID INNER JOIN tPracticeMaster on tPracticeMaster.practiceid = tAdminPracticeLU.practiceid LEFT JOIN ZIP_CODE on ZIP_CODE.zip_code = substring(tPracticeMaster.OfficeZip,0,6) INNER JOIN tNIM3_Modality on tNIM3_Modality.practiceid=tPracticeMaster.practiceid LEFT JOIN tmri_modelli on tmri_modelli.mri_modelid=tNIM3_Modality.modalitymodelid  LEFT JOIN tPracticeActivity on tPracticeActivity.practiceid = tPracticeMaster.practiceid LEFT JOIN tPracticeActivity AS tPracticeActivity_Admin  on tPracticeActivity_Admin.adminid = tAdminMaster.adminid " + myWhere + " group by tmri_modelli.isopen, tmri_modelli.teslastrength,  tAdminPracticeLU.relationshiptypeid, tNIM3_Modality.modalitytypeid, tPracticeMaster.ContractingStatusID, tPracticeMaster.DiagnosticMDArthrogram, tPracticeMaster.DiagnosticMDMyelogram, tAdminMaster.Name, tAdminMaster.AdminID, ZIP_CODE.county, tPracticeMaster.PracticeID, tPracticeMaster.PracticeName, tPracticeMaster.OfficeAddress1, OfficeAddress2, tPracticeMaster.OfficeCity, tPracticeMaster.OfficeStateID,tPracticeMaster.OfficeZIP,tAdminPracticeLU.PracticeID, tAdminPracticeLU.LookupID  order by tPracticeMaster." + orderBy + " LIMIT " + maxResults);	
	mySQLMap = "select   max(tPracticeActivity.reminddate) as reminddate, max(tPracticeActivity_Admin.reminddate) AS reminddate_admin,tAdminPracticeLU.relationshiptypeid,  tNIM3_Modality.modalitytypeid, tmri_modelli.isopen, tmri_modelli.teslastrength, tPracticeMaster.ContractingStatusID,  tPracticeMaster.DiagnosticMDArthrogram, tPracticeMaster.DiagnosticMDMyelogram, \"public\".\"GEOCODES\".lat, \"public\".\"GEOCODES\".lon, tAdminMaster.Name as ownername2,tAdminMaster.AdminID as AdminID, ZIP_CODE.county as ZCCounty, tPracticeMaster.PracticeID, tPracticeMaster.PracticeName,tPracticeMaster.OfficeAddress1, tPracticeMaster.OfficeAddress1, tPracticeMaster.OfficeCity, tStateLI.ShortState as OfficeState,tPracticeMaster.OfficeZIP,tAdminPracticeLU.PracticeID, tAdminPracticeLU.LookupID,(tPracticeMaster.OfficeAddress1 || ' ' || tPracticeMaster.OfficeCity || ' ' ||  tStateLI.ShortState || ' ' || tPracticeMaster.OfficeZIP) AS GCAddress from tAdminMaster INNER JOIN tAdminPracticeLU on tAdminPracticeLU.adminid=tAdminMaster.adminID INNER JOIN tPracticeMaster on tPracticeMaster.practiceid = tAdminPracticeLU.practiceid LEFT JOIN ZIP_CODE on ZIP_CODE.zip_code = substring(tPracticeMaster.OfficeZip,0,6)   INNER JOIN tStateLI on tStateLI.stateID = tPracticeMaster.OfficeStateID  LEFT JOIN \"public\".\"GEOCODES\" on \"public\".\"GEOCODES\".address =(tPracticeMaster.OfficeAddress1 || ' ' || tPracticeMaster.OfficeCity || ' ' ||  tStateLI.ShortState || ' ' || tPracticeMaster.OfficeZIP) INNER JOIN tNIM3_Modality on tNIM3_Modality.practiceid=tPracticeMaster.practiceid  LEFT JOIN tmri_modelli on tmri_modelli.mri_modelid=tNIM3_Modality.modalitymodelid LEFT JOIN tPracticeActivity on tPracticeActivity.practiceid = tPracticeMaster.practiceid LEFT JOIN tPracticeActivity AS tPracticeActivity_Admin  on tPracticeActivity_Admin.adminid = tAdminMaster.adminid  " + myWhere + " group by  tmri_modelli.isopen, tmri_modelli.teslastrength, tAdminPracticeLU.relationshiptypeid,  tNIM3_Modality.modalitytypeid, tPracticeMaster.ContractingStatusID, tPracticeMaster.DiagnosticMDArthrogram, tPracticeMaster.DiagnosticMDMyelogram, \"public\".\"GEOCODES\".lat, \"public\".\"GEOCODES\".lon, tAdminMaster.Name,tAdminMaster.AdminID, ZIP_CODE.county, tPracticeMaster.PracticeID, tPracticeMaster.PracticeName,tPracticeMaster.OfficeAddress1, tPracticeMaster.OfficeAddress1, tPracticeMaster.OfficeCity,tStateLI.ShortState,tPracticeMaster.OfficeZIP,tAdminPracticeLU.PracticeID, tAdminPracticeLU.LookupID, GCAddress order by tPracticeMaster." + orderBy + " LIMIT "+ maxResults;		
}
else
{
	mySQL = savedSQL;	
}

//String mySQL = ("select tPracticeMaster.PracticeID,tPracticeMaster.PracticeName,tPracticeMaster.OfficeCity,tPracticeMaster.OfficeStateID,tPracticeMaster.OfficeZIP,tAdminPracticeLU.PracticeID, tAdminPracticeLU.LookupID from tAdminMaster INNER JOIN tAdminPracticeLU on tAdminPracticeLU.adminid=tAdminMaster.adminID INNER JOIN tPracticeMaster on tPracticeMaster.practiceid = tAdminPracticeLU.practiceid " + myWhere + "order by tPracticeMaster." + orderBy + " LIMIT " + (maxResults+1) + " OFFSET " + startID + "");
pageControllerHash.put("URL_AdminPracticeAll_Query",request.getRequestURL()+"?"+request.getQueryString());
pageControllerHash.put("SQL_AdminPracticeAll_Query",mySQLMap);
session.setAttribute("pageControllerHash",pageControllerHash);

myRS = mySS.executeStatement(mySQL);
//out.print(mySQL);
//myRS = mySS.executeStatement("select * from tAdminPracticeLU " + myWhere + " order by " + orderBy);

}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

//String myMainTable= " ";
try
{

	int endCount = 0;
	
	int cnt=0;
	int cnt2=0;
	while (myRS!=null&&myRS.next())
	{
		//bltPracticeMaster pm = new bltPracticeMaster(new Integer(myRS.getString("PracticeID")));
		
		String iOwnerID = myRS.getString("AdminID");
		
		String smyDueDate = "None";
		try
		{
			smyDueDate = PLCUtils.getDisplayDateWithTime(dbdft.parse(myRS.getString("RemindDate")));
			if (smyDueDate.equalsIgnoreCase(" ")||smyDueDate.equalsIgnoreCase(""))
			{
				smyDueDate = "None";
			}
		}
		catch(Exception edd)
		{
		}
		String smyDueDate_admin = "None";
		try
		{
			smyDueDate_admin = PLCUtils.getDisplayDateWithTime(dbdft.parse(myRS.getString("RemindDate_admin")));
			if (smyDueDate_admin.equalsIgnoreCase(" ")||smyDueDate_admin.equalsIgnoreCase(""))
			{
				smyDueDate_admin = "None";
			}
		}
		catch(Exception edd)
		{
		}
		String pmPracticeID = myRS.getString("PracticeID");
		String mod_tesla = myRS.getString("teslastrength");
		String mod_isopen = myRS.getString("isopen");
		String amownername2 = myRS.getString("ownername2");
		String pmPracticeName = myRS.getString("PracticeName");
		String pmOfficeCity = myRS.getString("OfficeCity");
		
		String pmOfficeAddress1 = myRS.getString("OfficeAddress1");
		String pmOfficeAddress2 = "";
		try{
		 pmOfficeAddress2 = myRS.getString("OfficeAddress2");
		} catch (Exception e){}
		String zcCounty = myRS.getString("ZCCounty");
		String pmContractingStatusID = myRS.getString("ContractingStatusID");
		String pmlRelationshipTypeID = myRS.getString("RelationshipTypeID");
		//Integer pmOfficeStateID  = new Integer( 0);
		String pmOfficeStateID  = myRS.getString("OfficeStateID");
		String pmOfficeZIP = myRS.getString("OfficeZIP");
		String amIscompleted = myRS.getString("iscompleted_admin");
		String pmIscompleted = myRS.getString("iscompleted");
		String pm_mod_modalitytypeid = myRS.getString("modalitytypeid");
		String offers_athrogram = myRS.getString("diagnosticmdarthrogram");
		String offers_myelogram = myRS.getString("diagnosticmdmyelogram");
		
		
		
		
		
		
		cnt++;
		cnt2++;
		
		String myClass = "tdBase";
		if (pmContractingStatusID.equalsIgnoreCase("0") )
		{
			myClass = "tdBaseAltGrey";
		//	out.print("A - NC";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("1") )
		{
			myClass = "tdBaseAltCyan";
		//	out.print("E - Negotiating";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("2") )
		{
			myClass = "tdBaseAltGreen";
		//	out.print("F - Contracted";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("3") )
		{
			myClass = "tdBaseAltYellow";
		//	out.print("B - Target";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("4") )
		{
			myClass = "tdBaseAltRed2";
		//	out.print("F - Not Interested";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("5") )
		{
			myClass = "tdBaseAltPurple";
		//	out.print("D - Send Contract";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("6") )
		{
			myClass = "tdBaseAltOrange";
		//	out.print("C - Talking";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("7") )
		{
			myClass = "tdBaseAltPink";
		//	out.print("TargetMerge";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("8") )
		{
			myClass = "tdBaseAltOliveMed";
		//	out.print("TargetMerge";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("9") )
		{
			myClass = "tdBaseAltLime";
		//	out.print("Contracted[*]";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("10") )
		{
			myClass = "tdBaseAltLime2";
		//	out.print("Contracted[V*]";
		}
		else if (pmContractingStatusID.equalsIgnoreCase("11") )
		{
			myClass = "tdBaseAltLime3";
		//	out.print("Contracted[V*]";
		}
		out.print("<tr class="+myClass+">");
if (false)
{
}
		out.print("<td>"+cnt+"</td>");
		//out.print("<td><a href = \"tAdminPracticeLU_main_PracticeMaster_AdminID_form_authorize.jsp?EDIT=edit&EDITID="+pmPracticeID+"&EDIT=edit\"><img border=0 src=\"ui_"+thePLCID+"/icons/edit_AdminID.gif\"></a>        <a class=linkBase  onClick=\"return confirmDelete()\"   href = \"tAdminPracticeLU_main_LU_AdminID_form_authorize.jsp?EDIT=del&EDITID="+myRS.getString("LookUpID")+"&KM=p\"><img border=0 src=\"ui_"+thePLCID+"/icons/remove_AdminID.gif\"></td>";
		out.print("<td>");
		out.print("<a target=editResults href=AdminPracticeAll_query_auth.jsp?EDIT=owner&EDITID=" + iOwnerID + ">Account</a><br>");
		out.print("<a target=editResults  href=AdminPracticeAll_query_auth.jsp?EDIT=practice&EDITID=" + pmPracticeID + "&EDIT2ID=" + iOwnerID + ">Practice</a>");
		out.print("</td>");
		out.print("<td><strong>");
		if (pmlRelationshipTypeID.equalsIgnoreCase("0") )
		{
			out.print("None");
		}
		else if (pmlRelationshipTypeID.equalsIgnoreCase("1") )
		{
			out.print("Owner");
		}
		else if (pmlRelationshipTypeID.equalsIgnoreCase("2") )
		{
			out.print("Admin");
		}
		else if (pmlRelationshipTypeID.equalsIgnoreCase("3") )
		{
			out.print("Broker");
		}

out.print("</strong></td>");
		out.print("<td>");
		out.print("<a href=# onClick=\"document.getElementById('ownername2').value='" + amownername2+ "';document.getElementById('apsearchform1').submit();\">" + amownername2+"</a>");
		if (amIscompleted!=null&&(amIscompleted.equalsIgnoreCase("0")||amIscompleted.equalsIgnoreCase("2")))
		{
			out.print("<br>Due: <strong>" + smyDueDate_admin+"</strong>");
		}
		out.print("</td>");
		out.print("<td>");
		out.print(pmPracticeID+"");
		out.print("</td>");
		out.print("<td>");
		if (pmContractingStatusID.equalsIgnoreCase("0") )
		{
			out.print("None");
		}
		else if (pmContractingStatusID.equalsIgnoreCase("1") )
		{
			out.print("OTA");
		}
		else if (pmContractingStatusID.equalsIgnoreCase("2") )
		{
			out.print("Contracted");
		}
		else if (pmContractingStatusID.equalsIgnoreCase("3") )
		{
			out.print("Target");
		}
		else if (pmContractingStatusID.equalsIgnoreCase("4") )
		{
			out.print("Not Interested");
		}
		else if (pmContractingStatusID.equalsIgnoreCase("5") )
		{
			out.print("REM: Send Contract");
		}
		else if (pmContractingStatusID.equalsIgnoreCase("6") )
		{
			out.print("Talking");
		}
		else if (pmContractingStatusID.equalsIgnoreCase("7") )
		{
			out.print("OON");
		}
		else if (pmContractingStatusID.equalsIgnoreCase("8") )
		{
			out.print("Contracted [Select]");
		}
		else if (pmContractingStatusID.equalsIgnoreCase("9") )
		{
			out.print("Contracted [*]");
		}
		else if (pmContractingStatusID.equalsIgnoreCase("10") )
		{
			out.print("Contracted [V*]");
		}
		else if (pmContractingStatusID.equalsIgnoreCase("11") )
		{
			out.print("Contracted [IPA]");
		}
		out.print("</td>");
		out.print("<td>");
		out.print(pmPracticeName+"");
		if (pmIscompleted!=null&&(pmIscompleted.equalsIgnoreCase("0")||pmIscompleted.equalsIgnoreCase("2")))
		{
			out.print("<br>Due: <strong>" + smyDueDate+"</strong>");
		}
		out.print("</td>");
		out.print("<td>");
		out.print("" + pmOfficeAddress1+"");
		if(!pmOfficeAddress2.isEmpty()){
			out.print("<br>" + pmOfficeAddress2+"");
		}
		out.print("</td>");
		out.print("<td>");
		out.print("<a href=# onClick=\"document.getElementById('OfficeCity').value='" + pmOfficeCity+ "';document.getElementById('apsearchform1').submit();\">" + pmOfficeCity+"</a>");
		out.print("</td>");
		out.print("<td>");
		if (pmOfficeStateID.equalsIgnoreCase("49") ) {out.print( "AK");}
		else if (pmOfficeStateID.equalsIgnoreCase("30") ) {out.print( "AL");}
		else if (pmOfficeStateID.equalsIgnoreCase("21") ) {out.print( "AR");}
		else if (pmOfficeStateID.equalsIgnoreCase("8") ) {out.print( "AZ");}
		else if (pmOfficeStateID.equalsIgnoreCase("1") ) {out.print( "CA");}
		else if (pmOfficeStateID.equalsIgnoreCase("10") ) {out.print( "CO");}
		else if (pmOfficeStateID.equalsIgnoreCase("45") ) {out.print( "CT");}
		else if (pmOfficeStateID.equalsIgnoreCase("51") ) {out.print( "DC");}
		else if (pmOfficeStateID.equalsIgnoreCase("47") ) {out.print( "DE");}
		else if (pmOfficeStateID.equalsIgnoreCase("33") ) {out.print( "FL");}
		else if (pmOfficeStateID.equalsIgnoreCase("32") ) {out.print( "GA");}
		else if (pmOfficeStateID.equalsIgnoreCase("50") ) {out.print( "HI");}
		else if (pmOfficeStateID.equalsIgnoreCase("19") ) {out.print( "IA");}
		else if (pmOfficeStateID.equalsIgnoreCase("5") ) {out.print( "ID");}
		else if (pmOfficeStateID.equalsIgnoreCase("24") ) {out.print( "IL");}
		else if (pmOfficeStateID.equalsIgnoreCase("27") ) {out.print( "IN");}
		else if (pmOfficeStateID.equalsIgnoreCase("15") ) {out.print( "KS");}
		else if (pmOfficeStateID.equalsIgnoreCase("28") ) {out.print( "KY");}
		else if (pmOfficeStateID.equalsIgnoreCase("22") ) {out.print( "LA");}
		else if (pmOfficeStateID.equalsIgnoreCase("43") ) {out.print( "MA");}
		else if (pmOfficeStateID.equalsIgnoreCase("48") ) {out.print( "MD");}
		else if (pmOfficeStateID.equalsIgnoreCase("34") ) {out.print( "ME");}
		else if (pmOfficeStateID.equalsIgnoreCase("26") ) {out.print( "MI");}
		else if (pmOfficeStateID.equalsIgnoreCase("18") ) {out.print( "MN");}
		else if (pmOfficeStateID.equalsIgnoreCase("20") ) {out.print( "MO");}
		else if (pmOfficeStateID.equalsIgnoreCase("25") ) {out.print( "MS");}
		else if (pmOfficeStateID.equalsIgnoreCase("4") ) {out.print( "MT");}
		else if (pmOfficeStateID.equalsIgnoreCase("52") ) {out.print( "NA");}
		else if (pmOfficeStateID.equalsIgnoreCase("41") ) {out.print( "NC");}
		else if (pmOfficeStateID.equalsIgnoreCase("12") ) {out.print( "ND");}
		else if (pmOfficeStateID.equalsIgnoreCase("14") ) {out.print( "NE");}
		else if (pmOfficeStateID.equalsIgnoreCase("35") ) {out.print( "NH");}
		else if (pmOfficeStateID.equalsIgnoreCase("46") ) {out.print( "NJ");}
		else if (pmOfficeStateID.equalsIgnoreCase("11") ) {out.print( "NM");}
		else if (pmOfficeStateID.equalsIgnoreCase("6") ) {out.print( "NV");}
		else if (pmOfficeStateID.equalsIgnoreCase("37") ) {out.print( "NY");}
		else if (pmOfficeStateID.equalsIgnoreCase("31") ) {out.print( "OH");}
		else if (pmOfficeStateID.equalsIgnoreCase("16") ) {out.print( "OK");}
		else if (pmOfficeStateID.equalsIgnoreCase("3") ) {out.print( "OR");}
		else if (pmOfficeStateID.equalsIgnoreCase("38") ) {out.print( "PA");}
		else if (pmOfficeStateID.equalsIgnoreCase("44") ) {out.print( "RI");}
		else if (pmOfficeStateID.equalsIgnoreCase("42") ) {out.print( "SC");}
		else if (pmOfficeStateID.equalsIgnoreCase("13") ) {out.print( "SD");}
		else if (pmOfficeStateID.equalsIgnoreCase("29") ) {out.print( "TN");}
		else if (pmOfficeStateID.equalsIgnoreCase("17") ) {out.print( "TX");}
		else if (pmOfficeStateID.equalsIgnoreCase("7") ) {out.print( "UT");}
		else if (pmOfficeStateID.equalsIgnoreCase("40") ) {out.print( "VA");}
		else if (pmOfficeStateID.equalsIgnoreCase("36") ) {out.print( "VT");}
		else if (pmOfficeStateID.equalsIgnoreCase("2") ) {out.print( "WA");}
		else if (pmOfficeStateID.equalsIgnoreCase("23") ) {out.print( "WI");}
		else if (pmOfficeStateID.equalsIgnoreCase("39") ) {out.print( "WV");}
		else if (pmOfficeStateID.equalsIgnoreCase("9") ) {out.print( "WY");}
out.print("</td>");
		out.print("<td>");
		try
		{
			out.print("<a href=# onClick=\"document.getElementById('OfficeZip').value='" + pmOfficeZIP.substring(0,5)+ "';document.getElementById('apsearchform1').submit();\">" + pmOfficeZIP+"</a>");
		}
		catch (Exception eZIP)
		{
			out.print("<a href=# onClick=\"document.getElementById('OfficeZip').value='" + pmOfficeZIP + "';document.getElementById('apsearchform1').submit();\">" + pmOfficeZIP+"</a>");
		}
		out.print("</td>");
		out.print("<td>");
		out.print("<a href=# onClick=\"document.getElementById('ZCCounty').value='" + zcCounty+ "';document.getElementById('apsearchform1').submit();\">" + zcCounty+"</a>");
		out.print("</td>");
		out.print("<td>");
if (pm_mod_modalitytypeid.equalsIgnoreCase("1") ) {out.println("MRI");}
if (pm_mod_modalitytypeid.equalsIgnoreCase("2") ) {out.println("CT");}
if (pm_mod_modalitytypeid.equalsIgnoreCase("3") ) {out.println("BMD");}
if (pm_mod_modalitytypeid.equalsIgnoreCase("4") ) {out.println("FL");}
if (pm_mod_modalitytypeid.equalsIgnoreCase("5") ) {out.println("MG");}
if (pm_mod_modalitytypeid.equalsIgnoreCase("6") ) {out.println("NM");}
if (pm_mod_modalitytypeid.equalsIgnoreCase("7") ) {out.println("US");}
if (pm_mod_modalitytypeid.equalsIgnoreCase("8") ) {out.println("XR");}
if (pm_mod_modalitytypeid.equalsIgnoreCase("9") ) {out.println("EMG");}
if (pm_mod_modalitytypeid.equalsIgnoreCase("10") ) {out.println("PET/CT");}
if (pm_mod_modalitytypeid.equalsIgnoreCase("15") ) {out.println("Discogram");}
		if (mod_isopen.equalsIgnoreCase("-1"))
		{
			out.print("<br />Empty");
		}
		else if (mod_isopen.equalsIgnoreCase("1"))
		{
			out.print("<br />O:<strong><u>" +mod_tesla + "</u></strong>");
		}
		else
		{
			out.print("<br />T:<strong><u>" +mod_tesla + "</u></strong>");
		}
		if (offers_athrogram.equalsIgnoreCase("1")){
			out.print("<br /><strong>Arthrogram</strong>");
		}
		if (offers_myelogram.equalsIgnoreCase("1")){
			out.print("<br /><strong>Myelogram</strong>");
		}
		
		 
		
		out.print("</td></tr>");

	}
mySS.closeAll();
endCount = cnt;


}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}



%>



</table> 


  </table>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>     </td>
  </tr>
</table>



<%
}

}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}


  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>

    </td></tr></table>

