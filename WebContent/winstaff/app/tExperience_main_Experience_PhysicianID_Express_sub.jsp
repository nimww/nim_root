<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltExperience,com.winstaff.bltExperience_List,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: tExperience_main_Experience_PhysicianID.jsp
    Created on Nov/12/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

    <table cellpadding=0 cellspacing=0 border=0 width=800 >
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection7", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }

  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tExperience_main_Experience_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iExperienceID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltExperience_List        bltExperience_List        =    new    bltExperience_List(iPhysicianID);

//declaration of Enumeration
    bltExperience        Experience;
    ListElement         leCurrentElement;
    Enumeration eList = bltExperience_List.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tExperience"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        Experience  = (bltExperience) leCurrentElement.getObject();
      }
      else
      {
        Experience  = new bltExperience();
        Experience.setPhysicianID(iPhysicianID);
        isNewRecord= true;
      }
        Experience.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !Experience.getUniqueCreateDate().equals(testObj)  )
    {
         Experience.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !Experience.getUniqueModifyDate().equals(testObj)  )
    {
         Experience.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !Experience.getUniqueModifyComments().equals(testObj)  )
    {
         Experience.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID_"+iExpress)) ;
    if ( !Experience.getPhysicianID().equals(testObj)  )
    {
         Experience.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience PhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TypeOfExperience_"+iExpress)) ;
    if ( !Experience.getTypeOfExperience().equals(testObj)  )
    {
         Experience.setTypeOfExperience( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience TypeOfExperience not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Other_"+iExpress)) ;
    if ( !Experience.getOther().equals(testObj)  )
    {
         Experience.setOther( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Other not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Specialty_"+iExpress)) ;
    if ( !Experience.getSpecialty().equals(testObj)  )
    {
         Experience.setSpecialty( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Specialty not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateFrom_"+iExpress))) ;
    if ( !Experience.getDateFrom().equals(testObj)  )
    {
         Experience.setDateFrom( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience DateFrom not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateTo_"+iExpress))) ;
    if ( !Experience.getDateTo().equals(testObj)  )
    {
         Experience.setDateTo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience DateTo not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NoComplete_"+iExpress)) ;
    if ( !Experience.getNoComplete().equals(testObj)  )
    {
         Experience.setNoComplete( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience NoComplete not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Name_"+iExpress)) ;
    if ( !Experience.getName().equals(testObj)  )
    {
         Experience.setName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Name not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address1_"+iExpress)) ;
    if ( !Experience.getAddress1().equals(testObj)  )
    {
         Experience.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2_"+iExpress)) ;
    if ( !Experience.getAddress2().equals(testObj)  )
    {
         Experience.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City_"+iExpress)) ;
    if ( !Experience.getCity().equals(testObj)  )
    {
         Experience.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("StateID_"+iExpress)) ;
    if ( !Experience.getStateID().equals(testObj)  )
    {
         Experience.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience StateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province_"+iExpress)) ;
    if ( !Experience.getProvince().equals(testObj)  )
    {
         Experience.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP_"+iExpress)) ;
    if ( !Experience.getZIP().equals(testObj)  )
    {
         Experience.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID_"+iExpress)) ;
    if ( !Experience.getCountryID().equals(testObj)  )
    {
         Experience.setCountryID( testObj,UserSecurityGroupID   );
         //testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone_"+iExpress)) ;
    if ( !Experience.getPhone().equals(testObj)  )
    {
         Experience.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Fax_"+iExpress)) ;
    if ( !Experience.getFax().equals(testObj)  )
    {
         Experience.setFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Fax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName_"+iExpress)) ;
    if ( !Experience.getContactName().equals(testObj)  )
    {
         Experience.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail_"+iExpress)) ;
    if ( !Experience.getContactEmail().equals(testObj)  )
    {
         Experience.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !Experience.getComments().equals(testObj)  )
    {
         Experience.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("Experience Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tExperience","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	Experience.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    Experience.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
	        Experience.commitData();
	        if (!Experience.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }
    }
}       }
    }//while

if (errorRouteMeTotal)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else 
{
String nextPage = request.getParameter("nextPage");
if (nextPage==null||nextPage.equalsIgnoreCase(""))
{
	nextPage = ConfigurationMessages.getExpressLinkRaw("tExperience","next",iExpressMode);
}
if (!isAllComplete)
{
	%>
	<script language=Javascript>
	      if ( confirm("<%=ConfigurationMessages.getMessage("ConfirmReqFieldsReturn")%>") )
	      {
	          document.location="<%=ConfigurationMessages.getDataCategoryLink("tExperience","Express")%>";
	      }
	      else 
	      {
	          document.location="<%=nextPage%>";
	      }
	</script>
  <%
}
else
{
	%>
	<script language=Javascript>
	      document.location="<%=nextPage%>";
	</script>
  <%
}
}
       
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
