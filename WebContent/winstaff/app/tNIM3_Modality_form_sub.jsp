<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tNIM3_Modality_form_sub.jsp
    Created on Oct/28/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iModalityID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iModalityID")) 
    {
        iModalityID        =    (Integer)pageControllerHash.get("iModalityID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
        if (iModalityID.intValue() != iEDITID.intValue())
        {
        	accessValid = false;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltNIM3_Modality        NIM3_Modality        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        NIM3_Modality        =    new    bltNIM3_Modality(iModalityID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_Modality        =    new    bltNIM3_Modality(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !NIM3_Modality.getUniqueCreateDate().equals(testObj)  )
    {
         NIM3_Modality.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !NIM3_Modality.getUniqueModifyDate().equals(testObj)  )
    {
         NIM3_Modality.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !NIM3_Modality.getUniqueModifyComments().equals(testObj)  )
    {
         NIM3_Modality.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PracticeID")) ;
    if ( !NIM3_Modality.getPracticeID().equals(testObj)  )
    {
         NIM3_Modality.setPracticeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality PracticeID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ModalityTypeID")) ;
    if ( !NIM3_Modality.getModalityTypeID().equals(testObj)  )
    {
         NIM3_Modality.setModalityTypeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality ModalityTypeID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ModalityModelID")) ;
    if ( !NIM3_Modality.getModalityModelID().equals(testObj)  )
    {
         NIM3_Modality.setModalityModelID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality ModalityModelID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SoftwareVersion")) ;
    if ( !NIM3_Modality.getSoftwareVersion().equals(testObj)  )
    {
         NIM3_Modality.setSoftwareVersion( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality SoftwareVersion not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("LastServiceDate"))) ;
    if ( !NIM3_Modality.getLastServiceDate().equals(testObj)  )
    {
         NIM3_Modality.setLastServiceDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality LastServiceDate not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CoilWrist")) ;
    if ( !NIM3_Modality.getCoilWrist().equals(testObj)  )
    {
         NIM3_Modality.setCoilWrist( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality CoilWrist not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CoilElbow")) ;
    if ( !NIM3_Modality.getCoilElbow().equals(testObj)  )
    {
         NIM3_Modality.setCoilElbow( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality CoilElbow not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CoilKnee")) ;
    if ( !NIM3_Modality.getCoilKnee().equals(testObj)  )
    {
         NIM3_Modality.setCoilKnee( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality CoilKnee not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CoilAnkle")) ;
    if ( !NIM3_Modality.getCoilAnkle().equals(testObj)  )
    {
         NIM3_Modality.setCoilAnkle( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality CoilAnkle not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CoilShoulder")) ;
    if ( !NIM3_Modality.getCoilShoulder().equals(testObj)  )
    {
         NIM3_Modality.setCoilShoulder( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality CoilShoulder not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CoilSpine")) ;
    if ( !NIM3_Modality.getCoilSpine().equals(testObj)  )
    {
         NIM3_Modality.setCoilSpine( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality CoilSpine not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsACR")) ;
    if ( !NIM3_Modality.getIsACR().equals(testObj)  )
    {
         NIM3_Modality.setIsACR( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality IsACR not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ACRExpiration"))) ;
    if ( !NIM3_Modality.getACRExpiration().equals(testObj)  )
    {
         NIM3_Modality.setACRExpiration( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality ACRExpiration not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsACRVerified")) ;
    if ( !NIM3_Modality.getIsACRVerified().equals(testObj)  )
    {
         NIM3_Modality.setIsACRVerified( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality IsACRVerified not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ACRVerifiedDate"))) ;
    if ( !NIM3_Modality.getACRVerifiedDate().equals(testObj)  )
    {
         NIM3_Modality.setACRVerifiedDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality ACRVerifiedDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !NIM3_Modality.getComments().equals(testObj)  )
    {
         NIM3_Modality.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tNIM3_Modality","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	NIM3_Modality.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    NIM3_Modality.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        NIM3_Modality.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
nextPage = "tNIM3_Modality_main_NIM3_Modality_PracticeID.jsp";
%><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
