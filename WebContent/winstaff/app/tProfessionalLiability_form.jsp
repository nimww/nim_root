<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.ConfigurationInformation,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltProfessionalLiability" %>
<%/*

    filename: out\jsp\tProfessionalLiability_form.jsp
    Created on Mar/21/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
  <tr>
    <td width=10>&nbsp;</td>
    <td> <%=ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tProfessionalLiability")%> 
      <%
//initial declaration of list class and parentID
    Integer        iProfessionalLiabilityID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection9", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iProfessionalLiabilityID")) 
    {
        iProfessionalLiabilityID        =    (Integer)pageControllerHash.get("iProfessionalLiabilityID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tProfessionalLiability_form.jsp");
//initial declaration of list class and parentID

    bltProfessionalLiability        ProfessionalLiability        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        ProfessionalLiability        =    new    bltProfessionalLiability(iProfessionalLiabilityID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        ProfessionalLiability        =    new    bltProfessionalLiability(UserSecurityGroupID, true);
    }

//fields
        %>
      <%@ include file="tProfessionalLiability_form_instructions.jsp" %>
      <form action="tProfessionalLiability_form_sub.jsp" name="tProfessionalLiability_form1" method="POST">
        <%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >
        <%
    }
%>
        <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
          <tr>
            <td class=tableColor> 
              <table cellpadding=0 cellspacing=0 width=100%>
                <%
            if ( (ProfessionalLiability.isRequired("CoverageType",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("CoverageType")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("CoverageType",expiredDays))&&(ProfessionalLiability.isExpiredCheck("CoverageType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("CoverageType",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Coverage Type&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="CoverageType" >
                        <jsp:include page="../generic/tCoverageTypeLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=ProfessionalLiability.getCoverageType()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoverageType&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("CoverageType")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("CoverageType",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Coverage Type&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tCoverageTypeLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=ProfessionalLiability.getCoverageType()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CoverageType&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("CoverageType")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("InsuranceCarrier",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("InsuranceCarrier")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("InsuranceCarrier",expiredDays))&&(ProfessionalLiability.isExpiredCheck("InsuranceCarrier",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("InsuranceCarrier",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Insurance Carrier&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="InsuranceCarrier" value="<%=ProfessionalLiability.getInsuranceCarrier()%>">
                      &nbsp;<a href="#" onClick="window.open('LI_tInsuranceLI_Search.jsp?OFieldName=InsuranceCarrier','BNPop','status=yes,scrollbars=yes,resizable=yes,width=400,height=300'); return false;"><img align=middle border=0 src=images/icon_lu.gif></a>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InsuranceCarrier&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("InsuranceCarrier")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("InsuranceCarrier",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Insurance Carrier&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getInsuranceCarrier()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InsuranceCarrier&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("InsuranceCarrier")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("PolicyHolder",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("PolicyHolder")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("PolicyHolder",expiredDays))&&(ProfessionalLiability.isExpiredCheck("PolicyHolder",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("PolicyHolder",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Policy Holder&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="PolicyHolder" value="<%=ProfessionalLiability.getPolicyHolder()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PolicyHolder&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("PolicyHolder")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("PolicyHolder",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Policy Holder&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getPolicyHolder()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PolicyHolder&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("PolicyHolder")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("AgentName",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("AgentName")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("AgentName",expiredDays))&&(ProfessionalLiability.isExpiredCheck("AgentName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("AgentName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Agent Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="AgentName" value="<%=ProfessionalLiability.getAgentName()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AgentName&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("AgentName")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("AgentName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Agent Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getAgentName()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AgentName&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("AgentName")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("Address1",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("Address1")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("Address1",expiredDays))&&(ProfessionalLiability.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("Address1",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Address&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="Address1" value="<%=ProfessionalLiability.getAddress1()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("Address1")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("Address1",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Address&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getAddress1()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("Address1")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("Address2",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("Address2")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("Address2",expiredDays))&&(ProfessionalLiability.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("Address2",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="20" type=text size="80" name="Address2" value="<%=ProfessionalLiability.getAddress2()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("Address2")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("Address2",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getAddress2()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("Address2")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("City",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("City")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("City",expiredDays))&&(ProfessionalLiability.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("City",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>City&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="30" type=text size="80" name="City" value="<%=ProfessionalLiability.getCity()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("City")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("City",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>City&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getCity()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("City")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("StateID",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("StateID")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("StateID",expiredDays))&&(ProfessionalLiability.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("StateID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="StateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=ProfessionalLiability.getStateID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("StateID")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("StateID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=ProfessionalLiability.getStateID()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("StateID")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("Province",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("Province")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("Province",expiredDays))&&(ProfessionalLiability.isExpiredCheck("Province",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("Province",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="Province" value="<%=ProfessionalLiability.getProvince()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("Province")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("Province",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getProvince()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("Province")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("ZIP",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("ZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("ZIP",expiredDays))&&(ProfessionalLiability.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("ZIP",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="ZIP" value="<%=ProfessionalLiability.getZIP()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("ZIP")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("ZIP",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getZIP()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("ZIP")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("CountryID",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("CountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("CountryID",expiredDays))&&(ProfessionalLiability.isExpiredCheck("CountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("CountryID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="CountryID" >
                        <jsp:include page="../generic/tCountryLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=ProfessionalLiability.getCountryID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("CountryID")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("CountryID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=ProfessionalLiability.getCountryID()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("CountryID")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("Phone",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("Phone")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("Phone",expiredDays))&&(ProfessionalLiability.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("Phone",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="Phone" value="<%=ProfessionalLiability.getPhone()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("Phone")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("Phone",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getPhone()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("Phone")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("Fax",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("Fax")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("Fax",expiredDays))&&(ProfessionalLiability.isExpiredCheck("Fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("Fax",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="Fax" value="<%=ProfessionalLiability.getFax()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("Fax")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("Fax",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getFax()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("Fax")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("ContactName",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("ContactName")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("ContactName",expiredDays))&&(ProfessionalLiability.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("ContactName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="ContactName" value="<%=ProfessionalLiability.getContactName()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("ContactName")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("ContactName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getContactName()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("ContactName")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("ContactEmail",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("ContactEmail",expiredDays))&&(ProfessionalLiability.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="75" type=text size="80" name="ContactEmail" value="<%=ProfessionalLiability.getContactEmail()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getContactEmail()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("PolicyNumber",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("PolicyNumber")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("PolicyNumber",expiredDays))&&(ProfessionalLiability.isExpiredCheck("PolicyNumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("PolicyNumber",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Policy Number&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="PolicyNumber" value="<%=ProfessionalLiability.getPolicyNumber()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PolicyNumber&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("PolicyNumber")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("PolicyNumber",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Policy Number&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getPolicyNumber()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PolicyNumber&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("PolicyNumber")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("OriginalEffectiveDate",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("OriginalEffectiveDate")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("OriginalEffectiveDate",expiredDays))&&(ProfessionalLiability.isExpiredCheck("OriginalEffectiveDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <tr class=instructions>
                  <td colspan=2>
                    <hr noshade>
                  </td>
                </tr>
                <%
            if ((ProfessionalLiability.isWrite("OriginalEffectiveDate",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Original Effective Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=20  type=text size="80" name="OriginalEffectiveDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalLiability.getOriginalEffectiveDate())%>" /></jsp:include>' >
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OriginalEffectiveDate&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("OriginalEffectiveDate")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("OriginalEffectiveDate",UserSecurityGroupID)))
            {
                        %>
                < 
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Original Effective Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalLiability.getOriginalEffectiveDate())%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OriginalEffectiveDate&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("OriginalEffectiveDate")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr class=instructions> 
                  <td colspan=2><b>If this policy is terminated, please 
                    enter the date it was terminated.  If this policy is still current, please leave this field blank:</b></td>
                </tr>
                <%
            if ( (ProfessionalLiability.isRequired("TerminationDate",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("TerminationDate")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("TerminationDate",expiredDays))&&(ProfessionalLiability.isExpiredCheck("TerminationDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("TerminationDate",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Policy Termination Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=20  type=text size="80" name="TerminationDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalLiability.getTerminationDate())%>" /></jsp:include>' >
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TerminationDate&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("TerminationDate")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("TerminationDate",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Policy Termination Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalLiability.getTerminationDate())%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TerminationDate&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("TerminationDate")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr class=instructions>
                  <td colspan=2>&nbsp; </td>
                </tr>
                <tr class=instructions> 
                  <td colspan=2><b>If this policy is still <u>current</u>, please 
                    enter the current &quot;policy term&quot; dates below:</b></td>
                </tr>
                <%
            if ( (ProfessionalLiability.isRequired("InsuredFromDate",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("InsuredFromDate")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("InsuredFromDate",expiredDays))&&(ProfessionalLiability.isExpiredCheck("InsuredFromDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("InsuredFromDate",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Current Insured From Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=20  type=text size="80" name="InsuredFromDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalLiability.getInsuredFromDate())%>" /></jsp:include>' >
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InsuredFromDate&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("InsuredFromDate")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("InsuredFromDate",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Current Insured From Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalLiability.getInsuredFromDate())%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InsuredFromDate&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("InsuredFromDate")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("InsuredToDate",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("InsuredToDate")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("InsuredToDate",expiredDays))&&(ProfessionalLiability.isExpiredCheck("InsuredToDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("InsuredToDate",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Current Insured To Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=20  type=text size="80" name="InsuredToDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalLiability.getInsuredToDate())%>" /></jsp:include>' >
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InsuredToDate&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("InsuredToDate")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("InsuredToDate",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Current Insured To Date&nbsp;(mm/DD/yyyy):&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=dbdf.format(ProfessionalLiability.getInsuredToDate())%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InsuredToDate&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("InsuredToDate")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr class=instructions>
                  <td colspan=2>
                    <hr noshade>
                  </td>
                </tr>
                <%
            if ( (ProfessionalLiability.isRequired("PerClaimAmount",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("PerClaimAmount")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("PerClaimAmount",expiredDays))&&(ProfessionalLiability.isExpiredCheck("PerClaimAmount",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("PerClaimAmount",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Per Claim Amount:&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=14  type=text size="80" name="PerClaimAmount" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(ProfessionalLiability.getPerClaimAmount())%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PerClaimAmount&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("PerClaimAmount")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("PerClaimAmount",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Per Claim Amount:</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PLCUtils.getDisplayDefaultDecimalFormat2(ProfessionalLiability.getPerClaimAmount())%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PerClaimAmount&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("PerClaimAmount")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("AggregateAmount",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("AggregateAmount")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("AggregateAmount",expiredDays))&&(ProfessionalLiability.isExpiredCheck("AggregateAmount",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("AggregateAmount",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Aggregate Amount:&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength=14  type=text size="80" name="AggregateAmount" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(ProfessionalLiability.getAggregateAmount())%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AggregateAmount&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("AggregateAmount")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("AggregateAmount",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Aggregate Amount:&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PLCUtils.getDisplayDefaultDecimalFormat2(ProfessionalLiability.getAggregateAmount())%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AggregateAmount&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("AggregateAmount")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("DescOfSurcharge",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("DescOfSurcharge")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("DescOfSurcharge",expiredDays))&&(ProfessionalLiability.isExpiredCheck("DescOfSurcharge",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("DescOfSurcharge",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=ProfessionalLiability.getEnglish("DescOfSurcharge")%>&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <textarea onKeyDown="textAreaStop(this,200)" rows="2" name="DescOfSurcharge" cols="40" maxlength=200><%=ProfessionalLiability.getDescOfSurcharge()%></textarea>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DescOfSurcharge&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("DescOfSurcharge")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("DescOfSurcharge",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=ProfessionalLiability.getEnglish("DescOfSurcharge")%>&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getDescOfSurcharge()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DescOfSurcharge&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("DescOfSurcharge")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("DocuLinkID",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("DocuLinkID")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("DocuLinkID",expiredDays))&&(ProfessionalLiability.isExpiredCheck("DocuLinkID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("DocuLinkID",UserSecurityGroupID)))
            {
                        if (ProfessionalLiability.getDocuLinkID().intValue()>0)
                        {
                            bltDocumentManagement myDoc = new bltDocumentManagement(ProfessionalLiability.getDocuLinkID());
                            if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                            {
				    pageControllerHash.put("sFileName",ConfigurationInformation.sLinkedPDFDirectory + "\\" + myDoc.getDocumentFileName());
				    pageControllerHash.put("sDownloadName",myDoc.getDocumentFileName());
				    pageControllerHash.put("bDownload",new Boolean(false));
				    session.setAttribute("pageControllerHash",pageControllerHash);
                            %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Attached Document:&nbsp;</p>
                  </td>
                  <td valign=top> 
                    <p><a target=_blank href="fileRetrieve.jsp">view</a></b></p>
                  </td>
                </tr>
                <%
                           }
                            else
                            {
                            %>
                <%
                            }
                        }
            }
            else if ((ProfessionalLiability.isRead("DocuLinkID",UserSecurityGroupID)))
            {
                        bltDocumentManagement myDoc = new bltDocumentManagement(ProfessionalLiability.getDocuLinkID());
                            if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                            {
				    pageControllerHash.put("sFileName",ConfigurationInformation.sLinkedPDFDirectory + "\\" + myDoc.getDocumentFileName());
				    pageControllerHash.put("sDownloadName",myDoc.getDocumentFileName());
				    pageControllerHash.put("bDownload",new Boolean(false));
				    session.setAttribute("pageControllerHash",pageControllerHash);
                            %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Attached Document:&nbsp;</p>
                  </td>
                  <td valign=top> 
                    <p><a target=_blank href="fileRetrieve.jsp">view</a></b></p>
                  </td>
                </tr>
                <%
                           }
                        else
                        {
                        %>
                <%
                        }
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (ProfessionalLiability.isRequired("Comments",UserSecurityGroupID))&&(!ProfessionalLiability.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((ProfessionalLiability.isExpired("Comments",expiredDays))&&(ProfessionalLiability.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((ProfessionalLiability.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=ProfessionalLiability.getEnglish("Comments")%>&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=ProfessionalLiability.getComments()%></textarea>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("Comments")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((ProfessionalLiability.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=ProfessionalLiability.getEnglish("Comments")%>&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=ProfessionalLiability.getComments()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tProfessionalLiability&amp;sRefID=<%=ProfessionalLiability.getProfessionalLiabilityID()%>&amp;sFieldNameDisp=<%=ProfessionalLiability.getEnglish("Comments")%>&amp;sTableNameDisp=tProfessionalLiability','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr> 
                  <td width=40%>&nbsp;</td>
                  <td width=60%>&nbsp;</td>
                </tr>
              </table>
              <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
              <input type=hidden name=routePageReference value="sParentReturnPage">
              <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
              <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                <tr class=requiredField>
                  <td> 
                    <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>
                    &nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tProfessionalLiability")%> 
                    <br>
                    <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">
                    &nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tProfessionalLiability")%> 
                  </td>
                </tr>
              </table>
              <br>
              <br>
              <br>
              <%
              }
              %>
              <p>
                <input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit>
              </p>
              <%}%>
            </td>
          </tr>
        </table>
      </form>
      <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
    </td>
  </tr>
</table>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" >
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
