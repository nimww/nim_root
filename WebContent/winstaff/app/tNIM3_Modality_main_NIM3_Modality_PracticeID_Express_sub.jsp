<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.*, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltNIM3_Modality, com.winstaff.bltNIM3_Modality_List, com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: tNIM3_Modality_main_NIM3_Modality_PracticeID.jsp
    Created on Oct/28/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

    <table cellpadding=0 cellspacing=0 border=0 width=800 >
<%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;
    }

  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_Modality_main_NIM3_Modality_PracticeID_Expand.jsp");
    pageControllerHash.remove("iModalityID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltNIM3_Modality_List        bltNIM3_Modality_List        =    new    bltNIM3_Modality_List(iPracticeID);

//declaration of Enumeration
    bltNIM3_Modality        NIM3_Modality;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Modality_List.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tNIM3_Modality"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        NIM3_Modality  = (bltNIM3_Modality) leCurrentElement.getObject();
      }
      else
      {
        NIM3_Modality  = new bltNIM3_Modality();
        NIM3_Modality.setPracticeID(iPracticeID);
        isNewRecord= true;
      }
        NIM3_Modality.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !NIM3_Modality.getUniqueCreateDate().equals(testObj)  )
    {
         NIM3_Modality.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !NIM3_Modality.getUniqueModifyDate().equals(testObj)  )
    {
         NIM3_Modality.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !NIM3_Modality.getUniqueModifyComments().equals(testObj)  )
    {
         NIM3_Modality.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PracticeID_"+iExpress)) ;
    if ( !NIM3_Modality.getPracticeID().equals(testObj)  )
    {
         NIM3_Modality.setPracticeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality PracticeID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ModalityTypeID_"+iExpress)) ;
    if ( !NIM3_Modality.getModalityTypeID().equals(testObj)  )
    {
         NIM3_Modality.setModalityTypeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality ModalityTypeID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ModalityModelID_"+iExpress)) ;
    if ( !NIM3_Modality.getModalityModelID().equals(testObj)  )
    {
         NIM3_Modality.setModalityModelID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality ModalityModelID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SoftwareVersion_"+iExpress)) ;
    if ( !NIM3_Modality.getSoftwareVersion().equals(testObj)  )
    {
         NIM3_Modality.setSoftwareVersion( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality SoftwareVersion not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("LastServiceDate_"+iExpress))) ;
    if ( !NIM3_Modality.getLastServiceDate().equals(testObj)  )
    {
         NIM3_Modality.setLastServiceDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality LastServiceDate not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SmallPartSpecialtyCoil_"+iExpress)) ;
    if ( !NIM3_Modality.getSmallPartSpecialtyCoil().equals(testObj)  )
    {
         NIM3_Modality.setSmallPartSpecialtyCoil( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality SmallPartSpecialtyCoil not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsACR_"+iExpress)) ;
    if ( !NIM3_Modality.getIsACR().equals(testObj)  )
    {
         NIM3_Modality.setIsACR( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality IsACR not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ACRExpiration_"+iExpress))) ;
    if ( !NIM3_Modality.getACRExpiration().equals(testObj)  )
    {
         NIM3_Modality.setACRExpiration( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality ACRExpiration not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsACRVerified_"+iExpress)) ;
    if ( !NIM3_Modality.getIsACRVerified().equals(testObj)  )
    {
         NIM3_Modality.setIsACRVerified( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality IsACRVerified not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ACRVerifiedDate_"+iExpress))) ;
    if ( !NIM3_Modality.getACRVerifiedDate().equals(testObj)  )
    {
         NIM3_Modality.setACRVerifiedDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality ACRVerifiedDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !NIM3_Modality.getComments().equals(testObj)  )
    {
         NIM3_Modality.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Modality Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tNIM3_Modality","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	NIM3_Modality.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    NIM3_Modality.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
	        NIM3_Modality.commitData();
	        if (!NIM3_Modality.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }
    }
}       }
    }//while

if (errorRouteMeTotal)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else 
{
String nextPage = request.getParameter("nextPage");
if (nextPage==null||nextPage.equalsIgnoreCase(""))
{
	nextPage = ConfigurationMessages.getExpressLinkRaw("tNIM3_Modality","next",iExpressMode);
}
if (!isAllComplete)
{
	%>
	<script language=Javascript>
	      if ( confirm("<%=ConfigurationMessages.getMessage("ConfirmReqFieldsReturn")%>") )
	      {
	          document.location="<%=ConfigurationMessages.getDataCategoryLink("tNIM3_Modality","Express")%>";
	      }
	      else 
	      {
	          document.location="<%=nextPage%>";
	      }
	</script>
  <%
}
else
{
	%>
	<script language=Javascript>
	      document.location="<%=nextPage%>";
	</script>
  <%
}
}
       
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PracticeID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>