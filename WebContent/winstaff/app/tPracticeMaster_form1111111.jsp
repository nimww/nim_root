<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPracticeMaster" %>
<%/*

    filename: out\jsp\tPracticeMaster_form.jsp
    Created on Feb/17/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
  <tr> 
    <td width=10>&nbsp;</td>
    <td> <%=ConfigurationMessages.getHTML("INTERVIEWTopControl","tPracticeMaster1")%> 
      <%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tPracticeMaster_form.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID

    bltPracticeMaster        PracticeMaster        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PracticeMaster        =    new    bltPracticeMaster(iPracticeID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PracticeMaster        =    new    bltPracticeMaster(UserSecurityGroupID, true);
    }

//fields
        %>
      <%@ include file="tPracticeMaster_form_instructions.jsp" %>
      <form action="tPracticeMaster_form_sub.jsp" name="tPracticeMaster_form1">
        <%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >
        <%
    }
%>
        <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
          <tr> 
            <td class=tableColor> 
              <table cellpadding=0 cellspacing=0 width=100%>
                <tr> 
                  <td valign=top colspan="2" class=title>General Office Information:</td>
                </tr>
                <tr> 
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <%
            if ( (PracticeMaster.isRequired("PracticeName",UserSecurityGroupID))&&(!PracticeMaster.isComplete("PracticeName")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("PracticeName",expiredDays))&&(PracticeMaster.isExpiredCheck("PracticeName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("PracticeName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Practice Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="PracticeName" value="<%=PracticeMaster.getPracticeName()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("PracticeName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Practice Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getPracticeName()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("DepartmentName",UserSecurityGroupID))&&(!PracticeMaster.isComplete("DepartmentName")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("DepartmentName",expiredDays))&&(PracticeMaster.isExpiredCheck("DepartmentName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("DepartmentName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Department Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="DepartmentName" value="<%=PracticeMaster.getDepartmentName()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DepartmentName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DepartmentName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("DepartmentName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Department Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getDepartmentName()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DepartmentName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DepartmentName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("TypeOfPractice",UserSecurityGroupID))&&(!PracticeMaster.isComplete("TypeOfPractice")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("TypeOfPractice",expiredDays))&&(PracticeMaster.isExpiredCheck("TypeOfPractice",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("TypeOfPractice",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Type of Practice&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="TypeOfPractice" >
                        <jsp:include page="../generic/tPracticeTypeLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getTypeOfPractice()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TypeOfPractice&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("TypeOfPractice")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("TypeOfPractice",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Type of Practice&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tPracticeTypeLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getTypeOfPractice()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TypeOfPractice&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("TypeOfPractice")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeFederalTaxID",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeFederalTaxID")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeFederalTaxID",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeFederalTaxID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeFederalTaxID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Federal Tax ID&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="OfficeFederalTaxID" value="<%=PracticeMaster.getOfficeFederalTaxID()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeFederalTaxID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeFederalTaxID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeFederalTaxID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Federal Tax ID&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficeFederalTaxID()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeFederalTaxID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeFederalTaxID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeTaxIDNameAffiliation",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeTaxIDNameAffiliation")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeTaxIDNameAffiliation",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeTaxIDNameAffiliation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeTaxIDNameAffiliation",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Name affiliated with tax ID&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="OfficeTaxIDNameAffiliation" value="<%=PracticeMaster.getOfficeTaxIDNameAffiliation()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeTaxIDNameAffiliation&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeTaxIDNameAffiliation")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeTaxIDNameAffiliation",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Name affiliated with tax ID&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficeTaxIDNameAffiliation()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeTaxIDNameAffiliation&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeTaxIDNameAffiliation")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeEmail",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeEmail",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeEmail",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Email&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="75" type=text size="80" name="OfficeEmail" value="<%=PracticeMaster.getOfficeEmail()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeEmail",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Email&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficeEmail()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeAddress1",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAddress1",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeAddress1",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Address&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="OfficeAddress1" value="<%=PracticeMaster.getOfficeAddress1()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeAddress1",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Address&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficeAddress1()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeAddress2",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAddress2",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeAddress2",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="20" type=text size="80" name="OfficeAddress2" value="<%=PracticeMaster.getOfficeAddress2()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeAddress2",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficeAddress2()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeCity",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeCity")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeCity",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeCity",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>City, Town, Province&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="30" type=text size="80" name="OfficeCity" value="<%=PracticeMaster.getOfficeCity()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeCity",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>City, Town, Province&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficeCity()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeStateID",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeStateID",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeStateID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="OfficeStateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOfficeStateID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeStateID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOfficeStateID()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeProvince",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeProvince")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeProvince",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeProvince",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeProvince",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="100" type=text size="80" name="OfficeProvince" value="<%=PracticeMaster.getOfficeProvince()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeProvince&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeProvince")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeProvince",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Province, District, State&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficeProvince()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeProvince&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeProvince")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeZIP",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeZIP",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeZIP",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="10" type=text size="80" name="OfficeZIP" value="<%=PracticeMaster.getOfficeZIP()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeZIP",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficeZIP()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeCountryID",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeCountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeCountryID",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeCountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeCountryID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="OfficeCountryID" >
                        <jsp:include page="../generic/tCountryLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOfficeCountryID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeCountryID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeCountryID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeCountryID",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Country&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOfficeCountryID()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeCountryID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeCountryID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficePhone",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficePhone")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficePhone",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficePhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficePhone",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Phone Number (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="20" type=text size="80" name="OfficePhone" value="<%=PracticeMaster.getOfficePhone()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficePhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficePhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficePhone",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Phone Number (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficePhone()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficePhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficePhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("BackOfficePhoneNo",UserSecurityGroupID))&&(!PracticeMaster.isComplete("BackOfficePhoneNo")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("BackOfficePhoneNo",expiredDays))&&(PracticeMaster.isExpiredCheck("BackOfficePhoneNo",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("BackOfficePhoneNo",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Back Office Phone (if applicable) 
                      (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="20" type=text size="80" name="BackOfficePhoneNo" value="<%=PracticeMaster.getBackOfficePhoneNo()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BackOfficePhoneNo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BackOfficePhoneNo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("BackOfficePhoneNo",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Back Office Phone (if applicable) 
                      (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getBackOfficePhoneNo()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BackOfficePhoneNo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BackOfficePhoneNo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeFaxNo",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeFaxNo")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeFaxNo",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeFaxNo",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeFaxNo",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Fax&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="20" type=text size="80" name="OfficeFaxNo" value="<%=PracticeMaster.getOfficeFaxNo()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeFaxNo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeFaxNo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeFaxNo",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Fax&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficeFaxNo()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeFaxNo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeFaxNo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeManagerFirstName",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeManagerFirstName")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeManagerFirstName",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeManagerFirstName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeManagerFirstName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Manager First Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="OfficeManagerFirstName" value="<%=PracticeMaster.getOfficeManagerFirstName()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeManagerFirstName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Manager First Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficeManagerFirstName()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeManagerLastName",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeManagerLastName")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeManagerLastName",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeManagerLastName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeManagerLastName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Manager Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="OfficeManagerLastName" value="<%=PracticeMaster.getOfficeManagerLastName()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeManagerLastName",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Manager Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficeManagerLastName()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeManagerEmail",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeManagerEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeManagerEmail",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeManagerEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeManagerEmail",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Manager Email&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="75" type=text size="80" name="OfficeManagerEmail" value="<%=PracticeMaster.getOfficeManagerEmail()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeManagerEmail",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Manager Email&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficeManagerEmail()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("OfficeManagerPhone",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeManagerPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeManagerPhone",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeManagerPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("OfficeManagerPhone",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Manager Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="20" type=text size="80" name="OfficeManagerPhone" value="<%=PracticeMaster.getOfficeManagerPhone()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("OfficeManagerPhone",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Office Manager Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getOfficeManagerPhone()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr> 
                  <td colspan=2> 
                    <hr noshade>
                  </td>
                </tr>
                <tr class=title> 
                  <td colspan=2>Office Hours:</td>
                </tr>
                <tr class=title> 
                  <td colspan=2>&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan=2> 
                    <table class=tdBase border=1 bordercolor="#333333" cellpadding="2" cellspacing="0" align="left">
                      <tr class=tdHeaderAlt> 
                        <td colspan=1> 
                          <div align="center">&nbsp;</div>
                        </td>
                        <td colspan=2> 
                          <div align="center">Normal Hours </div>
                        </td>
                        <td colspan=2> 
                          <div align="center">After Hours </div>
                        </td>
                      </tr >
                      <tr class=tdBaseAlt> 
                        <td>Day</td>
                        <td> Open </td>
                        <td> Close </td>
                        <td> Open </td>
                        <td> Close </td>
                      </tr >
                      <tr> 
                        <td class=tdBaseAlt> 
                          <table class=tdBaseAlt  width="100" border="0" cellspacing="0" cellpadding="0" height="100%">
                            <tr> 
                              <td height="21">Monday</td>
                            </tr>
                            <tr> 
                              <td height="21">Tuesday</td>
                            </tr>
                            <tr> 
                              <td height="21">Wednesday</td>
                            </tr>
                            <tr> 
                              <td height="21">Thursday</td>
                            </tr>
                            <tr> 
                              <td height="21">Friday</td>
                            </tr>
                            <tr> 
                              <td height="21">Saturday</td>
                            </tr>
                            <tr> 
                              <td height="21">Sunday</td>
                            </tr>
                          </table>
                        </td>
                        <td> 
                          <table class=tdBase  border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenMonday" value="<%=PracticeMaster.getOfficeWorkHoursOpenMonday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenMonday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenTuesday" value="<%=PracticeMaster.getOfficeWorkHoursOpenTuesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenTuesday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenWednesday" value="<%=PracticeMaster.getOfficeWorkHoursOpenWednesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenWednesday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenThursday" value="<%=PracticeMaster.getOfficeWorkHoursOpenThursday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenThursday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenFriday" value="<%=PracticeMaster.getOfficeWorkHoursOpenFriday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenFriday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenSaturday" value="<%=PracticeMaster.getOfficeWorkHoursOpenSaturday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenSaturday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursOpenSunday" value="<%=PracticeMaster.getOfficeWorkHoursOpenSunday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursOpenSunday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                        <td> 
                          <table  class=tdBase border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseMonday" value="<%=PracticeMaster.getOfficeWorkHoursCloseMonday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseMonday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseTuesday" value="<%=PracticeMaster.getOfficeWorkHoursCloseTuesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseTuesday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseWednesday" value="<%=PracticeMaster.getOfficeWorkHoursCloseWednesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseWednesday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseThursday" value="<%=PracticeMaster.getOfficeWorkHoursCloseThursday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseThursday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseFriday" value="<%=PracticeMaster.getOfficeWorkHoursCloseFriday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseFriday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseSaturday" value="<%=PracticeMaster.getOfficeWorkHoursCloseSaturday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseSaturday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeWorkHoursCloseSunday" value="<%=PracticeMaster.getOfficeWorkHoursCloseSunday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeWorkHoursCloseSunday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                        <td> 
                          <table class=tdBase  border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenMonday" value="<%=PracticeMaster.getOfficeAfterHoursOpenMonday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenMonday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenTuesday" value="<%=PracticeMaster.getOfficeAfterHoursOpenTuesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenTuesday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenWednesday" value="<%=PracticeMaster.getOfficeAfterHoursOpenWednesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenWednesday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenThursday" value="<%=PracticeMaster.getOfficeAfterHoursOpenThursday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenThursday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenFriday" value="<%=PracticeMaster.getOfficeAfterHoursOpenFriday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenFriday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenSaturday" value="<%=PracticeMaster.getOfficeAfterHoursOpenSaturday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenSaturday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursOpenSunday" value="<%=PracticeMaster.getOfficeAfterHoursOpenSunday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursOpenSunday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                        <td> 
                          <table  class=tdBase border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseMonday" value="<%=PracticeMaster.getOfficeAfterHoursCloseMonday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseMonday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseTuesday" value="<%=PracticeMaster.getOfficeAfterHoursCloseTuesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseTuesday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseWednesday" value="<%=PracticeMaster.getOfficeAfterHoursCloseWednesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseWednesday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseThursday" value="<%=PracticeMaster.getOfficeAfterHoursCloseThursday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseThursday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseFriday" value="<%=PracticeMaster.getOfficeAfterHoursCloseFriday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseFriday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseSaturday" value="<%=PracticeMaster.getOfficeAfterHoursCloseSaturday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseSaturday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input maxlength="10" type=text size="8" name="OfficeAfterHoursCloseSunday" value="<%=PracticeMaster.getOfficeAfterHoursCloseSunday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p><%=PracticeMaster.getOfficeAfterHoursCloseSunday()%>&nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan=2> 
                    <hr noshade>
                  </td>
                </tr>
                <tr> 
                  <td colspan=2 class=title>Additional Office Information:</td>
                </tr>
                <tr>
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <%
            if ( (PracticeMaster.isRequired("AnsweringService",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AnsweringService")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AnsweringService",expiredDays))&&(PracticeMaster.isExpiredCheck("AnsweringService",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("AnsweringService",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Do you have an answering service?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="AnsweringService" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAnsweringService()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnsweringService&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnsweringService")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("AnsweringService",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Do you have an answering service?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAnsweringService()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnsweringService&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnsweringService")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("AnsweringServicePhone",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AnsweringServicePhone")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AnsweringServicePhone",expiredDays))&&(PracticeMaster.isExpiredCheck("AnsweringServicePhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("AnsweringServicePhone",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Answering Service Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="20" type=text size="80" name="AnsweringServicePhone" value="<%=PracticeMaster.getAnsweringServicePhone()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnsweringServicePhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnsweringServicePhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("AnsweringServicePhone",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Answering Service Phone (XXX-XXX-XXXX)&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getAnsweringServicePhone()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AnsweringServicePhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AnsweringServicePhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("Coverage247",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Coverage247")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Coverage247",expiredDays))&&(PracticeMaster.isExpiredCheck("Coverage247",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("Coverage247",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Do you provide 24-7 coverage?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="Coverage247" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCoverage247()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Coverage247&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Coverage247")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("Coverage247",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Do you provide 24-7 coverage?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCoverage247()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Coverage247&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Coverage247")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("EDI",UserSecurityGroupID))&&(!PracticeMaster.isComplete("EDI")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("EDI",expiredDays))&&(PracticeMaster.isExpiredCheck("EDI",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("EDI",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Do you participate in EDI (electronic 
                      data interchange)?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="EDI" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getEDI()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EDI&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("EDI")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("EDI",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Do you participate in EDI (electronic 
                      data interchange)?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getEDI()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EDI&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("EDI")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("EDINetwork",UserSecurityGroupID))&&(!PracticeMaster.isComplete("EDINetwork")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("EDINetwork",expiredDays))&&(PracticeMaster.isExpiredCheck("EDINetwork",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("EDINetwork",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Which Network&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="EDINetwork" value="<%=PracticeMaster.getEDINetwork()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EDINetwork&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("EDINetwork")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("EDINetwork",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Which Network&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getEDINetwork()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EDINetwork&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("EDINetwork")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("PracticeManagementSystemYN",UserSecurityGroupID))&&(!PracticeMaster.isComplete("PracticeManagementSystemYN")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("PracticeManagementSystemYN",expiredDays))&&(PracticeMaster.isExpiredCheck("PracticeManagementSystemYN",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("PracticeManagementSystemYN",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Do you use a practice management 
                      system/software?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="PracticeManagementSystemYN" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getPracticeManagementSystemYN()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeManagementSystemYN&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeManagementSystemYN")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("PracticeManagementSystemYN",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Do you use a practice management 
                      system/software?&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getPracticeManagementSystemYN()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeManagementSystemYN&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeManagementSystemYN")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("PracticeManagementSystem",UserSecurityGroupID))&&(!PracticeMaster.isComplete("PracticeManagementSystem")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("PracticeManagementSystem",expiredDays))&&(PracticeMaster.isExpiredCheck("PracticeManagementSystem",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("PracticeManagementSystem",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Which System&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <input maxlength="50" type=text size="80" name="PracticeManagementSystem" value="<%=PracticeMaster.getPracticeManagementSystem()%>">
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeManagementSystem&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeManagementSystem")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("PracticeManagementSystem",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Which System&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getPracticeManagementSystem()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeManagementSystem&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeManagementSystem")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("MinorityEnterprise",UserSecurityGroupID))&&(!PracticeMaster.isComplete("MinorityEnterprise")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("MinorityEnterprise",expiredDays))&&(PracticeMaster.isExpiredCheck("MinorityEnterprise",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("MinorityEnterprise",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Does your office qualify as a minority 
                      business enterprise? &nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <select   name="MinorityEnterprise" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMinorityEnterprise()%>" />
                        </jsp:include>
                      </select>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MinorityEnterprise&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MinorityEnterprise")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("MinorityEnterprise",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Does your office qualify as a minority 
                      business enterprise? &nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getMinorityEnterprise()%>" />
                      </jsp:include>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MinorityEnterprise&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MinorityEnterprise")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (PracticeMaster.isRequired("Comments",UserSecurityGroupID))&&(!PracticeMaster.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("Comments",expiredDays))&&(PracticeMaster.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((PracticeMaster.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("Comments")%>&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p> 
                      <textarea onKeyDown="textAreaStop(this)" rows="2" name="Comments" cols="40" maxlength=200><%=PracticeMaster.getComments()%></textarea>
                      &nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Comments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((PracticeMaster.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b><%=PracticeMaster.getEnglish("Comments")%>&nbsp;</b></p>
                  </td>
                  <td valign=top> 
                    <p><%=PracticeMaster.getComments()%>&nbsp; 
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Comments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr> 
                  <td width=40%>&nbsp;</td>
                  <td width=60%>&nbsp;</td>
                </tr>
              </table>
              <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
              <input type=hidden name=routePageReference value="sParentReturnPage">
              <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
              <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                <tr class=requiredField> 
                  <td> 
                    <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>
                    &nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tPracticeMaster")%> 
                    <br>
                    <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">
                    &nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tPracticeMaster")%> 
                  </td>
                </tr>
              </table>
              <br>
              <br>
              <br>
              <%
              }
              %>
              <p> 
                <input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit>
              </p>
              <%}%>
            </td>
          </tr>
        </table>
      </form>
      <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
    </td>
  </tr>
</table>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PracticeID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
