 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.PLCUtils,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltHCOMaster" %>
<%/*
    filename: out\jsp\tHCOMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>
<%@ include file="../generic/CheckLogin.jsp" %>
<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_HCOID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iHCOID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iHCOID")) 
    {
        iHCOID        =    (Integer)pageControllerHash.get("iHCOID");
        accessValid = true;    
   }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","pro-file_Status.jsp");
    pageControllerHash.put("sParentReturnPage","pro-file_Status.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);
bltHCOMaster        HCOMaster        =   new  bltHCOMaster (iHCOID);

%>
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p class=title>PRO-FILE HCO Status</p>
      <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
        <tr> 
          <td> 
            <table width="100%" border="1" cellspacing="0" cellpadding="2" bordercolor="#CCCCCC">
              <tr> 
                <td class=tdHeader width="30%" valign="top"> 
                  <div align="right">HCO Information:&nbsp;</div>
                </td>
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"> Name:&nbsp;</td>
                      <td><%=HCOMaster.getName()%></td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top"> Address:&nbsp;</td>
                      <td><%=HCOMaster.getAddress1()%>&nbsp;<%=HCOMaster.getAddress2()%><br>
                        <%=HCOMaster.getCity()%>&nbsp; 
                        <jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=HCOMaster.getStateID()%>" />
                        </jsp:include>
                        &nbsp;<%=HCOMaster.getZIP()%></td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top">Email:&nbsp;</td>
                      <td><a href="mailto:<%=HCOMaster.getContactEmail()%>" ><%=HCOMaster.getContactEmail()%></a></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td class=tdHeader width="30%" valign="top"> 
                  <div align="right">User Information:&nbsp;</div>
                </td>
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top">User Logon Name:&nbsp;</td>
                      <td><%=CurrentUserAccount.getLogonUserName()%></td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top">User Access:&nbsp;</td>
                      <td> 
                        <jsp:include page="../generic/tUserAccessTypeLILong_translate.jsp" flush="true" > 
                        <jsp:param name="CurrentSelection" value="<%=CurrentUserAccount.getAccessType()%>" />
                        </jsp:include>
                      </td>
                    </tr>
                    <tr class=tdBase> 
                      <td width="30%" align="right" valign="top">Contact Email:&nbsp;</td>
                      <td><a href="mailto:<%=CurrentUserAccount.getContactEmail()%>" ><%=CurrentUserAccount.getContactEmail()%></a></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <br>
      <table width="100%" border="1" cellspacing="0" cellpadding="0" bordercolor="#333333">
        <tr> 
          <td> 
            <table width="100%" border="1" cellspacing="0" cellpadding="3" bordercolor="#999999">
              <tr class=tdHeaderAlt> 
                <td nowrap>Action</td>
                <td width="90%">Description</td>
              </tr>
              <tr class=tdBase> 
                <td nowrap><a href="HCO-query.jsp">Search/Query All Practitioners</a></td>
                <td width="90%">Search entire database of practitioners</td>
              </tr>
              <tr class=tdBaseAlt> 
                <td nowrap><a href="HCO-query_HCOID.jsp">Search/Query Authorized Practitioners 
                  Only</a></td>
                <td width="90%">Search only those practitioners who have authorized 
                  your Healthcare Organization.</td>
              </tr>
              <tr class=tdBase> 
                <td nowrap><a href="reports_HCOID.jsp">Run Reports</a> </td>
                <td width="90%">Allows reports to be generated</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
</table>
<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_HCOID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" > 
<jsp:param name="plcID" value="<%=thePLCID%>"/>
</jsp:include>
