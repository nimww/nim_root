<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltPracticeMaster,com.winstaff.bltAdminPracticeLU,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPhysicianPracticeLU,com.winstaff.bltPhysicianPracticeLU_List_LU_PhysicianID" %>
<%/*
    filename: out\jsp\tPhysicianPracticeLU_main_LU_PhysicianID_form_create.jsp
    Created on Feb/06/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    Integer        iAdminID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
        }

//create Practice
bltPracticeMaster pm = new bltPracticeMaster();
    pm.setUniqueCreateDate(new java.util.Date());
    pm.setUniqueModifyDate(new java.util.Date());
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        pm.setUniqueModifyComments(""+UserLogonDescription);
    }

pm.commitData();
Integer iPracticeID = pm.getUniqueID();


//creating LU file Automatically
    bltPhysicianPracticeLU        working_bltPhysicianPracticeLU = new bltPhysicianPracticeLU();
    working_bltPhysicianPracticeLU.setPhysicianID(iPhysicianID);
    working_bltPhysicianPracticeLU.setPracticeID(iPracticeID);
    working_bltPhysicianPracticeLU.setUniqueCreateDate(new java.util.Date());
    working_bltPhysicianPracticeLU.setUniqueModifyDate(new java.util.Date());
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltPhysicianPracticeLU.setUniqueModifyComments(""+UserLogonDescription);
    }

            working_bltPhysicianPracticeLU.commitData();


    bltAdminPracticeLU        working_bltAdminPracticeLU = new bltAdminPracticeLU();
    working_bltAdminPracticeLU.setAdminID(iAdminID);
    working_bltAdminPracticeLU.setPracticeID(iPracticeID);
    working_bltAdminPracticeLU.setUniqueCreateDate(new java.util.Date());
    working_bltAdminPracticeLU.setUniqueModifyDate(new java.util.Date());
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltAdminPracticeLU.setUniqueModifyComments(""+UserLogonDescription);
    }

            working_bltAdminPracticeLU.commitData();





        {
            {
                pageControllerHash.put("iPracticeID",iPracticeID);
                pageControllerHash.put("iLookupID",working_bltPhysicianPracticeLU.getUniqueID());
                session.setAttribute("pageControllerHash",pageControllerHash);
                //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
                String targetRedirect = "tPracticeMaster_form_Basic.jsp?EDITID="+iPracticeID+"&routePageReference=sParentReturnPage&EDIT=edit"+parameterPassString    ;
                {
                    targetRedirect = "tPracticeMaster_form_basic.jsp?EDITID="+iPracticeID+"&routePageReference=sParentReturnPage&EDIT=edit"+parameterPassString    ;
                }
                response.sendRedirect(targetRedirect);
            }
        }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>





