<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPracticeMaster,com.winstaff.bltPhysicianPracticeLU,com.winstaff.bltPhysicianPracticeLU_List_LU_PhysicianID" %>
<%/*
    filename: out\jsp\tPhysicianPracticeLU_main_PracticeMaster_PhysicianID_form_create.jsp
    Created on Mar/03/2003
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }

//declaration of Enumeration
    bltPracticeMaster        working_bltPracticeMaster = new bltPracticeMaster();
    working_bltPracticeMaster.setUniqueCreateDate(new java.util.Date());
    working_bltPracticeMaster.setUniqueModifyDate(new java.util.Date());
    working_bltPracticeMaster.commitDataForced();
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltPracticeMaster.setUniqueModifyComments(""+UserLogonDescription);
    }

    bltPhysicianPracticeLU        working_bltPhysicianPracticeLU = new bltPhysicianPracticeLU();
    working_bltPhysicianPracticeLU.setPhysicianID(iPhysicianID);
    working_bltPhysicianPracticeLU.setPracticeID(working_bltPracticeMaster.getUniqueID());
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltPhysicianPracticeLU.setUniqueModifyComments(""+UserLogonDescription);
    }

    working_bltPhysicianPracticeLU.commitData();
    {
        pageControllerHash.put("iPracticeID",working_bltPracticeMaster.getUniqueID());
        pageControllerHash.put("sKeyMasterReference","p");
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tPracticeMaster_form.jsp?nullParam=null"+parameterPassString    ;

        {
            targetRedirect = "tPracticeMaster_form.jsp?EDITID="+working_bltPhysicianPracticeLU.getUniqueID()+"&routePageReference=sParentReturnPage&EDIT=edit";
        }
        response.sendRedirect(targetRedirect);
    }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

