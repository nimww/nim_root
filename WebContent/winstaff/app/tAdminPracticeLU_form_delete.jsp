<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tAdminPracticeLU_form_delete.jsp
    Created on Feb/06/2003
    Type: _form Delete
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iLookupID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iLookupID")) 
    {
        iLookupID        =    (Integer)pageControllerHash.get("iLookupID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {

//initial declaration of list class and parentID

    bltAdminPracticeLU        AdminPracticeLU        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("del") )
    {
        AdminPracticeLU        =    new    bltAdminPracticeLU(iLookupID);
	Integer tempPractID = AdminPracticeLU.getPracticeID();
        AdminPracticeLU.setAdminID(new Integer("0"));
        AdminPracticeLU.setPracticeID(new Integer("0"));
        //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            AdminPracticeLU.commitData();
//just remove this practice
//accountMaintenance AM = new accountMaintenance();
//	    AM.removePractice(tempPractID);
            out.println("Successful delete");
        }
    }
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
}
if (nextPage!=null)
{
    response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>
Successful Delete

  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>


