<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPracticeMaster" %>
<%/*

    filename: out\jsp\tPracticeMaster_form.jsp
    Created on Mar/03/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<script language = javascript>
function BillingFill()
{
	document.forms[0].BillingAddress1.value = document.forms[0].OfficeAddress1.value;
	document.forms[0].BillingAddress2.value = document.forms[0].OfficeAddress2.value;
	document.forms[0].BillingCity.value = document.forms[0].OfficeCity.value;
	document.forms[0].BillingProvince.value = document.forms[0].OfficeProvince.value;
	document.forms[0].BillingStateID.value = document.forms[0].OfficeStateID.value;
	document.forms[0].BillingZIP.value = document.forms[0].OfficeZIP.value;
	document.forms[0].BillingCountryID.value = document.forms[0].OfficeCountryID.value;
	document.forms[0].BillingPhone.value = document.forms[0].OfficePhone.value;
	document.forms[0].BillingFax.value = document.forms[0].OfficeFaxNo.value;
	document.forms[0].BillingFirstName.value = document.forms[0].OfficeManagerFirstName.value;
	document.forms[0].BillingLastName.value = document.forms[0].OfficeManagerLastName.value;
	document.forms[0].BillingPayableTo.value = document.forms[0].OfficeTaxIDNameAffiliation.value;
	return false;
}

function CredFill()
{
	document.forms[0].CredentialingContactAddress1.value = document.forms[0].OfficeAddress1.value;
	document.forms[0].CredentialingContactAddress2.value = document.forms[0].OfficeAddress2.value;
	document.forms[0].CredentialingContactCity.value = document.forms[0].OfficeCity.value;
	document.forms[0].CredentialingContactProvince.value = document.forms[0].OfficeProvince.value;
	document.forms[0].CredentialingContactStateID.value = document.forms[0].OfficeStateID.value;
	document.forms[0].CredentialingContactZIP.value = document.forms[0].OfficeZIP.value;
	document.forms[0].CredentialingContactCountryID.value = document.forms[0].OfficeCountryID.value;
	document.forms[0].CredentialingContactPhone.value = document.forms[0].OfficePhone.value;
	document.forms[0].CredentialingContactFax.value = document.forms[0].OfficeFaxNo.value;
	document.forms[0].CredentialingContactFirstName.value = document.forms[0].OfficeManagerFirstName.value;
	document.forms[0].CredentialingContactLastName.value = document.forms[0].OfficeManagerLastName.value;	
	document.forms[0].CredentiallingContactEmail.value = document.forms[0].OfficeManagerEmail.value;	

	return false;
}
</script>



<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PracticeID_nim.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<!--body onUnload="if (document.getElementById('isSaveMe').value=='1'&&confirm('Want to save first?')){document.getElementById('tPracticeMaster_form1').submit();}">-->

<table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
  <tr>
    <td width=10>&nbsp;</td>
    <td> 
    <td> <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tPracticeMaster1")%> 
      <%
//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
    boolean isImagingCenter = true;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

//    pageControllerHash.put("sLocalChildReturnPage","tPracticeMaster_form2.jsp?EDIT=edit");
//    pageControllerHash.put("sParentReturnPage","tPracticeMaster_form2.jsp?EDIT=edit");
    pageControllerHash.put("sLocalChildReturnPage","practice_Status.jsp");
    pageControllerHash.put("sParentReturnPage","practice_Status.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID

    bltPracticeMaster        PracticeMaster        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PracticeMaster        =    new    bltPracticeMaster(iPracticeID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PracticeMaster        =    new    bltPracticeMaster(UserSecurityGroupID, true);
    }

//fields
        %>
      <%@ include file="tPracticeMaster_form_instructions.jsp" %>
      <form action="tPracticeMaster_form_sub.jsp" name="tPracticeMaster_form1"  id="tPracticeMaster_form1" method="POST">
      <input name="EDITID" value="<%=iPracticeID%>" type="hidden">
        <input onChange="document.getElementById('isSaveMe').value='1';" type="hidden" name="isSaveMe" id="isSaveMe" value="0" />
        <%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input onChange="document.getElementById('isSaveMe').value='1';" type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >
        <%
    }
%>
        <%  String theClass ="tdBase";%>


        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
          <tr>
            <td class=tableColor> 
              <table cellpadding=0 cellspacing=0 width=100%>
                <tr class=title>
                  <td colspan=3><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>General Practice Information:</td>
                      <td align="right"> <input onChange="document.getElementById('isSaveMe').value='1';"  class="tdBaseAltGreen" onClick="document.getElementById('isSaveMe').value='0';"  type=Submit value="Save" name=Submit></td>
                    </tr>
                  </table>
</td>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Practice Name&nbsp;</b></p>
                  </td>
                  <td width="90%" valign=top>
                    <p>
                      <input class="tdHeader" onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="40" name="PracticeName" value="<%=PracticeMaster.getPracticeName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PracticeName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("PracticeName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td width="90%" rowspan="14" valign=top><table border="1" cellpadding="2" cellspacing="0">
                    <tr class="tdHeaderAlt">
                      <td>Procedure:&nbsp;</td>
                      <td>Price:&nbsp;</td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>MRI
wo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="10" name="Price_Mod_MRI_WO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_MRI_WO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_MRI_WO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>MRI 
                        w</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="10" name="Price_Mod_MRI_W" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_MRI_W())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_W&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_MRI_W")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>MRI wwo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="10" name="Price_Mod_MRI_WWO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_MRI_WWO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_MRI_WWO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_MRI_WWO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>CT w</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="10" name="Price_Mod_CT_W" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_CT_W())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_W&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_CT_W")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>CT wo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="10" name="Price_Mod_CT_WO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_CT_WO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_CT_WO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                    <tr>
                      <td valign="top" nowrap><p class="<%=theClass%>" ><b>CT wwo</b></p></td>
                      <td valign="top"><p>
                        <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10"  type="text" size="10" name="Price_Mod_CT_WWO" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(PracticeMaster.getPrice_Mod_CT_WWO())%>" />
                        &nbsp;
                        <%if (isShowAudit){%>
                        <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Price_Mod_CT_WWO&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Price_Mod_CT_WWO")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align="middle" border="0" src="images/icon_audit.gif" /></a>
                        <%}%>
                      </p></td>
                    </tr>
                  </table></td>
                </tr>

                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Department Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="DepartmentName" value="<%=PracticeMaster.getDepartmentName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DepartmentName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DepartmentName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Contracting Status&nbsp;</b></p></td><td valign=top><p><select  onchange="document.getElementById('isSaveMe').value='1';"    name="ContractingStatusID" ><jsp:include page="../generic/tPracticeContractStatusLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=PracticeMaster.getContractingStatusID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContractingStatusID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("ContractingStatusID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                </tr>

                     <tr><td valign=top><p class=<%=theClass%> ><b>Owner Name&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="OwnerName" value="<%=PracticeMaster.getOwnerName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OwnerName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OwnerName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Notes&nbsp;</b></p>
                  <input name="none1" class="tdBaseAltYellow" type="button" id="none1" onClick="document.getElementById('isSaveMe').value='1'; document.getElementById('Comments').value += '\n[' + Date() + ' - <%=CurrentUserAccount.getLogonUserName()%>]\n';" value="Add Date/Time Stamp"></td>
                  <td valign=top>
                    <p>
                      <textarea  onchange="document.getElementById('isSaveMe').value='1';"   onKeyDown="textAreaStop(this,4500)" rows="6" name="Comments" id="Comments" cols="40"><%=PracticeMaster.getComments()%></textarea>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("Comments")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="OfficeAddress1" value="<%=PracticeMaster.getOfficeAddress1()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="OfficeAddress2" value="<%=PracticeMaster.getOfficeAddress2()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>City, Town, Province&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="30" type=text size="80" name="OfficeCity" value="<%=PracticeMaster.getOfficeCity()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select  onchange="document.getElementById('isSaveMe').value='1';"    name="OfficeStateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getOfficeStateID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="80" name="OfficeZIP" value="<%=PracticeMaster.getOfficeZIP()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Phone Number</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="OfficePhone" value="<%=PracticeMaster.getOfficePhone()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficePhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficePhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>


                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Fax&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="OfficeFaxNo" value="<%=PracticeMaster.getOfficeFaxNo()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeFaxNo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeFaxNo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Type of Practice&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select  onchange="document.getElementById('isSaveMe').value='1';"    name="TypeOfPractice" >
                        <jsp:include page="../generic/tPracticeTypeLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getTypeOfPractice()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TypeOfPractice&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("TypeOfPractice")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                   <tr><td valign=top><p class=<%=theClass%> ><b>Web URL&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="WebURL" value="<%=PracticeMaster.getWebURL()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=WebURL&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("WebURL")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Back Office Phone</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="BackOfficePhoneNo2" value="<%=PracticeMaster.getBackOfficePhoneNo()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BackOfficePhoneNo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BackOfficePhoneNo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Email&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="75" type=text size="80" name="OfficeEmail" value="<%=PracticeMaster.getOfficeEmail()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Languages Spoken&nbsp;</b></p></td>
                  <td valign=top><p>
                    <textarea  onchange="document.getElementById('isSaveMe').value='1';"   onKeyDown="textAreaStop(this,200)" rows="2" name="LanguagesSpokenInOffice" cols="40" maxlength=200><%=PracticeMaster.getLanguagesSpokenInOffice()%></textarea>
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LanguagesSpokenInOffice&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("LanguagesSpokenInOffice")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager First Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="OfficeManagerFirstName" value="<%=PracticeMaster.getOfficeManagerFirstName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="OfficeManagerLastName" value="<%=PracticeMaster.getOfficeManagerLastName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager Phone&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="OfficeManagerPhone" value="<%=PracticeMaster.getOfficeManagerPhone()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Office Manager Email&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="75" type=text size="80" name="OfficeManagerEmail" value="<%=PracticeMaster.getOfficeManagerEmail()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeManagerEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeManagerEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Office Federal Tax ID&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="OfficeFederalTaxID2" value="<%=PracticeMaster.getOfficeFederalTaxID()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeFederalTaxID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeFederalTaxID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Name affiliated with tax ID&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="900" type=text size="80" name="OfficeTaxIDNameAffiliation2" value="<%=PracticeMaster.getOfficeTaxIDNameAffiliation()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeTaxIDNameAffiliation&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeTaxIDNameAffiliation")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                </tr>

                <tr> 
                  <td colspan=3> 
                    <hr noshade>
                  </td>
                </tr>
                <tr class=title> 
                  <td colspan=3>Billing Information <span class=tdBase>(<a href="#" onClick="return BillingFill()">Click 
                    here</a> if this is the same as the Practice Office Information)</span></td>
                <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Make Checks Payable To&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="BillingPayableTo" value="<%=PracticeMaster.getBillingPayableTo()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingPayableTo&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingPayableTo")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr><td valign=top><p class=<%=theClass%> ><b>Billing Address Company Name&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="75" type=text size="80" name="BillingAddressName" value="<%=PracticeMaster.getBillingAddressName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingAddressName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingAddressName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Billing Contact First Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="BillingFirstName" value="<%=PracticeMaster.getBillingFirstName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Billing Contact Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="BillingLastName" value="<%=PracticeMaster.getBillingLastName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="BillingAddress1" value="<%=PracticeMaster.getBillingAddress1()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="BillingAddress2" value="<%=PracticeMaster.getBillingAddress2()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>City&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="30" type=text size="80" name="BillingCity" value="<%=PracticeMaster.getBillingCity()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select  onchange="document.getElementById('isSaveMe').value='1';"    name="BillingStateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getBillingStateID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="80" name="BillingZIP" value="<%=PracticeMaster.getBillingZIP()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Phone&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="BillingPhone" value="<%=PracticeMaster.getBillingPhone()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Fax&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="BillingFax2" value="<%=PracticeMaster.getBillingFax()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingFax&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingFax")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Email&nbsp;</b></p></td><td valign=top><p><input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="BillingEmail" value="<%=PracticeMaster.getBillingEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("BillingEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>                  <td valign=top>&nbsp;</td>
</tr>
                <tr> 
                  <td colspan=3> 
                    <hr noshade>
                  </td>
                </tr>
                <tr class=title> 
                  <td colspan=3>Credentialing Contact <span class=tdBase>(<a href="#" onClick="return CredFill()">Click 
                    here</a> if this is the same as the Practice Office Information)</span></td>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b> Company Name&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="75" type=text size="80" name="CredentialingContactAddressName" value="<%=PracticeMaster.getCredentialingContactAddressName()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactAddressName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactAddressName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr> 
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>First Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="CredentialingContactFirstName" value="<%=PracticeMaster.getCredentialingContactFirstName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactFirstName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactFirstName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Last Name&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="CredentialingContactLastName" value="<%=PracticeMaster.getCredentialingContactLastName()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactLastName&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactLastName")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="CredentialingContactAddress1" value="<%=PracticeMaster.getCredentialingContactAddress1()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactAddress1&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactAddress1")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Address 2&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="20" type=text size="80" name="CredentialingContactAddress2" value="<%=PracticeMaster.getCredentialingContactAddress2()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactAddress2&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactAddress2")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>City&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="30" type=text size="80" name="CredentialingContactCity" value="<%=PracticeMaster.getCredentialingContactCity()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactCity&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactCity")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>State&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select  onchange="document.getElementById('isSaveMe').value='1';"    name="CredentialingContactStateID" >
                        <jsp:include page="../generic/tStateLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getCredentialingContactStateID()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactStateID&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactStateID")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>ZIP&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="CredentialingContactZIP" value="<%=PracticeMaster.getCredentialingContactZIP()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactZIP&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactZIP")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Phone&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="CredentialingContactPhone" value="<%=PracticeMaster.getCredentialingContactPhone()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactPhone&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactPhone")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Fax&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="50" type=text size="80" name="CredentialingContactFax" value="<%=PracticeMaster.getCredentialingContactFax()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentialingContactFax&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentialingContactFax")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Email&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="CredentiallingContactEmail" value="<%=PracticeMaster.getCredentiallingContactEmail()%>">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CredentiallingContactEmail&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("CredentiallingContactEmail")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>State License Number&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="StateLicenseNumber2" value="<%=PracticeMaster.getStateLicenseNumber()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateLicenseNumber&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("StateLicenseNumber")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>NPI Number&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="NPINumber2" value="<%=PracticeMaster.getNPINumber()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NPINumber&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("NPINumber")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Medicare License Number&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="MedicareLicenseNumber2" value="<%=PracticeMaster.getMedicareLicenseNumber()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MedicareLicenseNumber&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MedicareLicenseNumber")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>MalPractice Policy Carrier&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="900" type=text size="80" name="MalPracticePolicyCarrier2" value="<%=PracticeMaster.getMalPracticePolicyCarrier()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MalPracticePolicyCarrier&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MalPracticePolicyCarrier")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Malpractice Policy Number&nbsp;</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="100" type=text size="80" name="MalPracticePolicyNumber2" value="<%=PracticeMaster.getMalPracticePolicyNumber()%>">
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MalPracticePolicyNumber&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MalPracticePolicyNumber")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Malpractice Policy Exp Date</b></p></td>
                  <td valign=top><p>
                    <input onChange="document.getElementById('isSaveMe').value='1';" maxlength=10  type=text size="80" name="MalPracticePolicyExpDate2" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(PracticeMaster.getMalPracticePolicyExpDate())%>" /></jsp:include>' >
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MalPracticePolicyExpDate&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("MalPracticePolicyExpDate")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Walk-ins?</b></p></td>
                  <td valign=top><p>
                    <select  onchange="document.getElementById('isSaveMe').value='1';"    name="WalkinMRICT2" >
                      <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getWalkinMRICT()%>" />                
                        </jsp:include>
                    </select>
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=WalkinMRICT&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("WalkinMRICT")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Diagnostic Tech-Only?</b></p></td>
                  <td valign=top><p>
                    <select  onchange="document.getElementById('isSaveMe').value='1';"    name="DiagnosticTechOnly2" >
                      <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDiagnosticTechOnly()%>" />                
                        </jsp:include>
                    </select>
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DiagnosticTechOnly&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DiagnosticTechOnly")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>DICOM Forward?</b></p></td>
                  <td valign=top><p>
                    <select  onchange="document.getElementById('isSaveMe').value='1';"    name="DICOMForward2" >
                      <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDICOMForward()%>" />                
                        </jsp:include>
                    </select>
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DICOMForward&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DICOMForward")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top><p class=<%=theClass%> ><b>Diagnostic After Hours&nbsp;</b></p></td>
                  <td valign=top><p>
                    <select  onchange="document.getElementById('isSaveMe').value='1';"    name="DiagnosticAfterHours2" >
                      <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getDiagnosticAfterHours()%>" />                
                        </jsp:include>
                    </select>
                    &nbsp;
                    <%if (isShowAudit){%>
                    <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DiagnosticAfterHours&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("DiagnosticAfterHours")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                    <%}%>
                  </p></td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr>
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan=3> 
                    <hr noshade>
                  </td>
                </tr>
                <tr class=title> 
                  <td colspan=3>Office Hours:</td>
                </tr>
                <tr class=title> 
                  <td colspan=3>&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan=3> 
                    <table class=tdBase border=1 bordercolor="#333333" cellpadding="2" cellspacing="0" align="left">
                      <tr class=tdHeaderAlt> 
                        <td colspan=1> 
                          <div align="center">&nbsp;</div>
                        </td>
                        <td colspan=2> 
                          <div align="center">Normal Hours </div>
                        </td>
                        <td colspan=2> 
                          <div align="center">After Hours </div>
                        </td>
                      </tr >
                      <tr class=tdBaseAlt> 
                        <td>Day</td>
                        <td> Open </td>
                        <td> Close </td>
                        <td> Open </td>
                        <td> Close </td>
                      </tr >
                      <tr> 
                        <td class=tdBaseAlt> 
                          <table class=tdBaseAlt  border="0" cellspacing="0" cellpadding="0" >
                            <tr> 
                              <td height="21">Monday</td>
                            </tr>
                            <tr> 
                              <td height="21">Tuesday</td>
                            </tr>
                            <tr> 
                              <td height="21">Wednesday</td>
                            </tr>
                            <tr> 
                              <td height="21">Thursday</td>
                            </tr>
                            <tr> 
                              <td height="21">Friday</td>
                            </tr>
                            <tr> 
                              <td height="21">Saturday</td>
                            </tr>
                            <tr> 
                              <td height="21">Sunday</td>
                            </tr>
                          </table>
                        </td>
                        <td> 
                          <table class=tdBase  border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenMonday" value="<%=PracticeMaster.getOfficeWorkHoursOpenMonday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenTuesday" value="<%=PracticeMaster.getOfficeWorkHoursOpenTuesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenWednesday" value="<%=PracticeMaster.getOfficeWorkHoursOpenWednesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenThursday" value="<%=PracticeMaster.getOfficeWorkHoursOpenThursday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenFriday" value="<%=PracticeMaster.getOfficeWorkHoursOpenFriday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenSaturday" value="<%=PracticeMaster.getOfficeWorkHoursOpenSaturday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursOpenSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursOpenSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursOpenSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursOpenSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursOpenSunday" value="<%=PracticeMaster.getOfficeWorkHoursOpenSunday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                        <td> 
                          <table  class=tdBase border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseMonday" value="<%=PracticeMaster.getOfficeWorkHoursCloseMonday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseTuesday" value="<%=PracticeMaster.getOfficeWorkHoursCloseTuesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseWednesday" value="<%=PracticeMaster.getOfficeWorkHoursCloseWednesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseThursday" value="<%=PracticeMaster.getOfficeWorkHoursCloseThursday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseFriday" value="<%=PracticeMaster.getOfficeWorkHoursCloseFriday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseSaturday" value="<%=PracticeMaster.getOfficeWorkHoursCloseSaturday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeWorkHoursCloseSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeWorkHoursCloseSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeWorkHoursCloseSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeWorkHoursCloseSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeWorkHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeWorkHoursCloseSunday" value="<%=PracticeMaster.getOfficeWorkHoursCloseSunday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeWorkHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeWorkHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeWorkHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                        <td> 
                          <table class=tdBase  border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenMonday" value="<%=PracticeMaster.getOfficeAfterHoursOpenMonday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenMonday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenTuesday" value="<%=PracticeMaster.getOfficeAfterHoursOpenTuesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenTuesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenWednesday" value="<%=PracticeMaster.getOfficeAfterHoursOpenWednesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenWednesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenThursday" value="<%=PracticeMaster.getOfficeAfterHoursOpenThursday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenThursday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenFriday" value="<%=PracticeMaster.getOfficeAfterHoursOpenFriday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenFriday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenSaturday" value="<%=PracticeMaster.getOfficeAfterHoursOpenSaturday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenSaturday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursOpenSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursOpenSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursOpenSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursOpenSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursOpenSunday" value="<%=PracticeMaster.getOfficeAfterHoursOpenSunday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursOpenSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursOpenSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursOpenSunday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                        <td> 
                          <table  class=tdBase border=0 cellpadding="0" cellspacing="0">
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseMonday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseMonday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseMonday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseMonday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseMonday" value="<%=PracticeMaster.getOfficeAfterHoursCloseMonday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseMonday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseMonday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseMonday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseTuesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseTuesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseTuesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseTuesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseTuesday" value="<%=PracticeMaster.getOfficeAfterHoursCloseTuesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseTuesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseTuesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseTuesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseWednesday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseWednesday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseWednesday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseWednesday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseWednesday" value="<%=PracticeMaster.getOfficeAfterHoursCloseWednesday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseWednesday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseWednesday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseWednesday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseThursday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseThursday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseThursday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseThursday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseThursday" value="<%=PracticeMaster.getOfficeAfterHoursCloseThursday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseThursday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseThursday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseThursday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseFriday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseFriday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseFriday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseFriday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseFriday" value="<%=PracticeMaster.getOfficeAfterHoursCloseFriday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseFriday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseFriday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseFriday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseSaturday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseSaturday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseSaturday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseSaturday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseSaturday" value="<%=PracticeMaster.getOfficeAfterHoursCloseSaturday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseSaturday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseSaturday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseSaturday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                            <%
            if ( (PracticeMaster.isRequired("OfficeAfterHoursCloseSunday",UserSecurityGroupID))&&(!PracticeMaster.isComplete("OfficeAfterHoursCloseSunday")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("OfficeAfterHoursCloseSunday",expiredDays))&&(PracticeMaster.isExpiredCheck("OfficeAfterHoursCloseSunday",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if ((PracticeMaster.isWrite("OfficeAfterHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <tr> 
                              <td valign=top> 
                                <p> 
                                  <input onChange="document.getElementById('isSaveMe').value='1';" maxlength="10" type=text size="8" name="OfficeAfterHoursCloseSunday" value="<%=PracticeMaster.getOfficeAfterHoursCloseSunday()%>">
                                  &nbsp; 
                                  <%if (isShowAudit){%>
                                  <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OfficeAfterHoursCloseSunday&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("OfficeAfterHoursCloseSunday")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a> 
                                  <%}%>
                                </p>
                              </td>
                            </tr>
                            <%
            }
            else if ((PracticeMaster.isRead("OfficeAfterHoursCloseSunday",UserSecurityGroupID)))
            {
                        %>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td colspan=3> 
                    <hr noshade>
                  </td>
                </tr>
                <%
            if ( (PracticeMaster.isRequired("AllowPhysEdit",UserSecurityGroupID))&&(!PracticeMaster.isComplete("AllowPhysEdit")) )
            {
                theClass = "requiredField";
            }
            else if ((PracticeMaster.isExpired("AllowPhysEdit",expiredDays))&&(PracticeMaster.isExpiredCheck("AllowPhysEdit",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (false&&(PracticeMaster.isWrite("AllowPhysEdit",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Allow Practitioner to edit this 
                      Practice&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <select  onchange="document.getElementById('isSaveMe').value='1';"    name="AllowPhysEdit" >
                        <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
                        <jsp:param name="CurrentSelection" value="<%=PracticeMaster.getAllowPhysEdit()%>" />
                        </jsp:include>
                      </select>
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AllowPhysEdit&amp;sTableName=tPracticeMaster&amp;sRefID=<%=PracticeMaster.getPracticeID()%>&amp;sFieldNameDisp=<%=PracticeMaster.getEnglish("AllowPhysEdit")%>&amp;sTableNameDisp=tPracticeMaster','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                  <td valign=top>&nbsp;</td>
                </tr>
                <%
            }
            else if (false&&(PracticeMaster.isRead("AllowPhysEdit",UserSecurityGroupID)))
            {
                        %>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <tr>
                  <td width=40%>&nbsp;</td>
                  <td width=90%>&nbsp;</td>
                  <td width=90%>&nbsp;</td>
                </tr>
              </table>
              <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
              <input onChange="document.getElementById('isSaveMe').value='1';" type=hidden name=routePageReference value="sParentReturnPage">
              <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
              <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                <tr class=requiredField>
                  <td> 
                    <input onChange="document.getElementById('isSaveMe').value='1';"  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>
                    &nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tPracticeMaster")%> 
                    <br>
                    <input onChange="document.getElementById('isSaveMe').value='1';"  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">
                    &nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tPracticeMaster")%> 
                  </td>
                </tr>
              </table>
              <br>
              <br>
              <br>
              <%
              }
              %>
              <p>
                <input onChange="document.getElementById('isSaveMe').value='1';"  class="tdBaseAltGreen" onClick="document.getElementById('isSaveMe').value='0';"  type=Submit value="Save" name=Submit>
              </p>
              <%}%>
            </td>
          </tr>
        </table>
      </form>
      <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
    </td>
  </tr>
</table>
