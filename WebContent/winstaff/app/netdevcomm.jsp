<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>NetDev CommTrack</title>
</head>

<body>
<%@ include file="../generic/CheckLogin.jsp" %>
<%
/*bltNIM3_NetDevCommTrack NIM3_NetDevCommTrack = null;
searchDB2 conn = new searchDB2();
String query = "select max(netdevcommtrackid)+1 netdevcommtrackid from tnim3_netdevcommtrack";
java.sql.ResultSet results = conn.executeStatement(query);
*/
bltUserAccount UA = new bltUserAccount(CurrentUserAccount.getUserID());
bltNIM3_NetDevCommTrack NIM3_NetDevCommTrack = null;
String query = "select max(netdevcommtrackid)+1 netdevcommtrackid from tnim3_netdevcommtrack";
searchDB2 conn = new searchDB2();
java.sql.ResultSet results = conn.executeStatement(query);

while (results!=null&&results.next())
{
	NIM3_NetDevCommTrack = new bltNIM3_NetDevCommTrack(results.getInt("netdevcommtrackid"),true);
}
conn.closeAll();

String testChangeID = "0";
try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getNowDate(false)) ;
    if ( !NIM3_NetDevCommTrack.getUniqueCreateDate().equals(testObj)  )
    {
         NIM3_NetDevCommTrack.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_NetDevCommTrack UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getNowDate(false)) ;
    if ( !NIM3_NetDevCommTrack.getUniqueModifyDate().equals(testObj)  )
    {
         NIM3_NetDevCommTrack.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_NetDevCommTrack UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(UA.getContactFirstName()+" "+UA.getContactLastName()) ;
    if ( !NIM3_NetDevCommTrack.getUniqueModifyComments().equals(testObj)  )
    {
         NIM3_NetDevCommTrack.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_NetDevCommTrack UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AccountID")) ;
    if ( !NIM3_NetDevCommTrack.getAccountID().equals(testObj)  )
    {
         NIM3_NetDevCommTrack.setAccountID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_NetDevCommTrack AccountID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CommTypeID")) ;
    if ( !NIM3_NetDevCommTrack.getCommTypeID().equals(testObj)  )
    {
         NIM3_NetDevCommTrack.setCommTypeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_NetDevCommTrack CommTypeID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getNowDate(false)) ;
    if ( !NIM3_NetDevCommTrack.getCommStart().equals(testObj)  )
    {
         NIM3_NetDevCommTrack.setCommStart( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_NetDevCommTrack CommStart not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getNowDate(false)) ;
    if ( !NIM3_NetDevCommTrack.getCommEnd().equals(testObj)  )
    {
         NIM3_NetDevCommTrack.setCommEnd( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_NetDevCommTrack CommEnd not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageText")) ;
    if ( !NIM3_NetDevCommTrack.getMessageText().equals(testObj)  )
    {
         NIM3_NetDevCommTrack.setMessageText( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_NetDevCommTrack MessageText not set. this is ok-not an error");
}

try
{
    String testObj = new String(UA.getContactFirstName()+" "+UA.getContactLastName()) ;
    if ( !NIM3_NetDevCommTrack.getMessageName().equals(testObj)  )
    {
         NIM3_NetDevCommTrack.setMessageName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }
}
catch(Exception e)
{
     //out.println("NIM3_NetDevCommTrack MessageName not set. this is ok-not an error");
}

if (testChangeID.equalsIgnoreCase("1"))
{
	NIM3_NetDevCommTrack.commitData();
}
%>


</body>
</html>