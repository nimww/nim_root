<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltPLCLI, com.winstaff.bltHCOPhysicianLU,com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltPhysicianMaster,com.winstaff.bltAdminPhysicianLU,com.winstaff.bltAdminPhysicianLU_List_LU_AdminID" %>
<%/*
    filename: out\jsp\tAdminPhysicianLU_main_PhysicianMaster_AdminID_form_create.jsp
    Created on Mar/21/2003
    Type: n-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iAdminID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    {
        iAdminID        =    (Integer)pageControllerHash.get("iAdminID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }

//declaration of Enumeration
    bltPhysicianMaster        working_bltPhysicianMaster = new bltPhysicianMaster();
    working_bltPhysicianMaster.setUniqueCreateDate(new java.util.Date());
    working_bltPhysicianMaster.setUniqueModifyDate(new java.util.Date());
    working_bltPhysicianMaster.commitDataForced();
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltPhysicianMaster.setUniqueModifyComments(""+UserLogonDescription);
    }


    bltAdminPhysicianLU        working_bltAdminPhysicianLU = new bltAdminPhysicianLU();
    working_bltAdminPhysicianLU.setAdminID(iAdminID);
    working_bltAdminPhysicianLU.setPhysicianID(working_bltPhysicianMaster.getUniqueID());
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltAdminPhysicianLU.setUniqueModifyComments(""+UserLogonDescription);
    }

    working_bltAdminPhysicianLU.commitData();
    
    //manaul add of HCO auth
    Integer iPLCID = new Integer(thePLCID);
    Integer iHCOID = (new bltPLCLI(iPLCID)).getHCOID();
    bltHCOPhysicianLU myHCOLU = new bltHCOPhysicianLU();
    myHCOLU.setPhysicianID(working_bltPhysicianMaster.getUniqueID());
    myHCOLU.setHCOID (iHCOID);
    myHCOLU.commitData();


    {
        pageControllerHash.put("iPhysicianID",working_bltPhysicianMaster.getUniqueID());
        pageControllerHash.put("sKeyMasterReference","p");
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tPhysicianMaster_form.jsp?nullParam=null"+parameterPassString    ;

        {
            targetRedirect = "tPhysicianMaster_form.jsp?EDITID="+working_bltAdminPhysicianLU.getUniqueID()+"&routePageReference=sParentReturnPage&EDIT=edit";
            targetRedirect = "pro-file_Status.jsp";

        }
        response.sendRedirect(targetRedirect);
    }
    

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

