<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltLicenseRegistration, com.winstaff.bltLicenseRegistration_List" %>
<%/*
    filename: tLicenseRegistration_main_LicenseRegistration_PhysicianID.jsp
    Created on May/26/2004
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    String MasterTableWidthVO = "100%";
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    if (pageControllerHash.containsKey("MasterTableWidthVO")) 
    {
        MasterTableWidthVO = (String)pageControllerHash.get("MasterTableWidthVO");
    }
    %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidthVO%> >
    <tr><td width=10>&nbsp;</td><td>




<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection3", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID") ) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tLicenseRegistration_main_LicenseRegistration_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iLicenseRegistrationID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltLicenseRegistration_List        bltLicenseRegistration_List        =    new    bltLicenseRegistration_List(iPhysicianID);

//declaration of Enumeration
    bltLicenseRegistration        LicenseRegistration;
    ListElement         leCurrentElement;
    Enumeration eList = bltLicenseRegistration_List.elements();
    %>
        <%@ include file="tLicenseRegistration_main_LicenseRegistration_PhysicianID_instructions.jsp" %>


    <%
    int iExpress=0;
    while (eList.hasMoreElements())
    {
       iExpress++;
         %>
         <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        LicenseRegistration  = (bltLicenseRegistration) leCurrentElement.getObject();
      }
      else
      {
        LicenseRegistration  = new bltLicenseRegistration();
        isNewRecord= true;
      }
        LicenseRegistration.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tLicenseRegistration")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                 </table>
                 <table width="100%" border="1" cellspacing="0" bordercolor="#333333">
                  <tr>
                   <td>
                     <table width="100%" border="0" cellspacing="0">
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

  {

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (LicenseRegistration.isRequired("IsCurrent",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("IsCurrent")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("IsCurrent",expiredDays))&&(LicenseRegistration.isExpiredCheck("IsCurrent",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("IsCurrent",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is this a current license?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getIsCurrent()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsCurrent&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("IsCurrent")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("IsCurrent",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is this a current license?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getIsCurrent()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsCurrent&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("IsCurrent")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (LicenseRegistration.isRequired("LicenseType",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("LicenseType")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("LicenseType",expiredDays))&&(LicenseRegistration.isExpiredCheck("LicenseType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("LicenseType",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Type&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tLicenseTypeLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getLicenseType()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LicenseType&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("LicenseType")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("LicenseType",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Type&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tLicenseTypeLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getLicenseType()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LicenseType&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("LicenseType")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (LicenseRegistration.isRequired("SubType",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("SubType")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("SubType",expiredDays))&&(LicenseRegistration.isExpiredCheck("SubType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("SubType",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Sub-Type (or description)&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getSubType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SubType&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("SubType")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("SubType",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Sub-Type (or description)&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getSubType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SubType&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("SubType")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("NPDBFOL",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("NPDBFOL")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("NPDBFOL",expiredDays))&&(LicenseRegistration.isExpiredCheck("NPDBFOL",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("NPDBFOL",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Field of Licensure&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tNPDBFOLTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getNPDBFOL()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NPDBFOL&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("NPDBFOL")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("NPDBFOL",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Field of Licensure&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tNPDBFOLTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getNPDBFOL()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NPDBFOL&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("NPDBFOL")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (LicenseRegistration.isRequired("StateID",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("StateID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("StateID",expiredDays))&&(LicenseRegistration.isExpiredCheck("StateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("StateID")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("StateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StateID&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("StateID")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (LicenseRegistration.isRequired("IsPractice",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("IsPractice")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("IsPractice",expiredDays))&&(LicenseRegistration.isExpiredCheck("IsPractice",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("IsPractice",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you currently practice in this state?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getIsPractice()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsPractice&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("IsPractice")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("IsPractice",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Do you currently practice in this state?&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getIsPractice()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsPractice&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("IsPractice")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (LicenseRegistration.isRequired("LicenseDocumentNumber",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("LicenseDocumentNumber")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("LicenseDocumentNumber",expiredDays))&&(LicenseRegistration.isExpiredCheck("LicenseDocumentNumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("LicenseDocumentNumber",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Document Number&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getLicenseDocumentNumber()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LicenseDocumentNumber&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("LicenseDocumentNumber")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("LicenseDocumentNumber",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Document Number&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getLicenseDocumentNumber()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LicenseDocumentNumber&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("LicenseDocumentNumber")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("IssueDate",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("IssueDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("IssueDate",expiredDays))&&(LicenseRegistration.isExpiredCheck("IssueDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((LicenseRegistration.isWrite("IssueDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Issue Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(LicenseRegistration.getIssueDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IssueDate&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("IssueDate")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("IssueDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Issue Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(LicenseRegistration.getIssueDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IssueDate&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("IssueDate")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("ExpirationDate",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("ExpirationDate")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("ExpirationDate",expiredDays))&&(LicenseRegistration.isExpiredCheck("ExpirationDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((LicenseRegistration.isWrite("ExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(LicenseRegistration.getExpirationDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpirationDate&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ExpirationDate")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("ExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Expiration Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(LicenseRegistration.getExpirationDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpirationDate&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ExpirationDate")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("Name",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Name")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("Name",expiredDays))&&(LicenseRegistration.isExpiredCheck("Name",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("Name",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Licensing Organization&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Name")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("Name",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Licensing Organization&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Name&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Name")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("Address1",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Address1")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("Address1",expiredDays))&&(LicenseRegistration.isExpiredCheck("Address1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("Address1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Address1")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("Address1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address1&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Address1")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("Address2",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Address2")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("Address2",expiredDays))&&(LicenseRegistration.isExpiredCheck("Address2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("Address2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Address2")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("Address2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Address2&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Address2")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("City",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("City")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("City",expiredDays))&&(LicenseRegistration.isExpiredCheck("City",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("City",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Institution City&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("City")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("City",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Institution City&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=City&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("City")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("OrgStateID",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("OrgStateID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("OrgStateID",expiredDays))&&(LicenseRegistration.isExpiredCheck("OrgStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("OrgStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getOrgStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OrgStateID&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("OrgStateID")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("OrgStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getOrgStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=OrgStateID&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("OrgStateID")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (LicenseRegistration.isRequired("Province",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Province")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("Province",expiredDays))&&(LicenseRegistration.isExpiredCheck("Province",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("Province",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Non-US Province, District, State&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Province")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("Province",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Non-US Province, District, State&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Province&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Province")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("ZIP",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("ZIP")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("ZIP",expiredDays))&&(LicenseRegistration.isExpiredCheck("ZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("ZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ZIP")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("ZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ZIP&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ZIP")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("CountryID",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("CountryID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("CountryID",expiredDays))&&(LicenseRegistration.isExpiredCheck("CountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("CountryID")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("CountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=LicenseRegistration.getCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CountryID&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("CountryID")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (LicenseRegistration.isRequired("Phone",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Phone")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("Phone",expiredDays))&&(LicenseRegistration.isExpiredCheck("Phone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("Phone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Phone")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("Phone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Phone&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Phone")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("Fax",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Fax")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("Fax",expiredDays))&&(LicenseRegistration.isExpiredCheck("Fax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("Fax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Fax")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("Fax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Fax&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Fax")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("ContactName",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("ContactName")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("ContactName",expiredDays))&&(LicenseRegistration.isExpiredCheck("ContactName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("ContactName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getContactName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ContactName")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("ContactName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Name&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getContactName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactName&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ContactName")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("ContactEmail",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("ContactEmail",expiredDays))&&(LicenseRegistration.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (LicenseRegistration.isRequired("DocuLinkID",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("DocuLinkID")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("DocuLinkID",expiredDays))&&(LicenseRegistration.isExpiredCheck("DocuLinkID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("DocuLinkID",UserSecurityGroupID)))
            {
                        if (LicenseRegistration.getDocuLinkID().intValue()>0)
                        {
                            bltDocumentManagement myDoc = new bltDocumentManagement(LicenseRegistration.getDocuLinkID());
                            if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                            {
                            %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Attached Document:&nbsp;</p></td><td valign=top><p><a href="pdf/<%=myDoc.getDocumentFileName()%>">view</a></b></p></td></tr>
                            <%
                           }
                            else
                            {
                            %>

                            <%
                            }
                        }
            }
            else if ((LicenseRegistration.isRead("DocuLinkID",UserSecurityGroupID)))
            {
                        bltDocumentManagement myDoc = new bltDocumentManagement(LicenseRegistration.getDocuLinkID());
                        if (!myDoc.getDocumentFileName().equalsIgnoreCase(""))
                        {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Attached Document:&nbsp;</p></td><td valign=top><p><a href="pdf/<%=myDoc.getDocumentFileName()%>">view</a></b></p></td></tr>
                        <%
                        }
                        else
                        {
                        %>

                        <%
                        }
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (LicenseRegistration.isRequired("Comments",UserSecurityGroupID))&&(!LicenseRegistration.isComplete("Comments")) )
            {
                theClass = "requiredField";
                if (!isNewRecord)
                {
                    theClass = "requiredFieldExpress";
                }
            }
            else if ((LicenseRegistration.isExpired("Comments",expiredDays))&&(LicenseRegistration.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((LicenseRegistration.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=LicenseRegistration.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Comments")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((LicenseRegistration.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=LicenseRegistration.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=LicenseRegistration.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tLicenseRegistration&amp;sRefID=<%=LicenseRegistration.getLicenseRegistrationID()%>&amp;sFieldNameDisp=<%=LicenseRegistration.getEnglish("Comments")%>&amp;sTableNameDisp=tLicenseRegistration','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>



        </td></tr></table>
        </td></tr></table>
        <%
  }%>

        </td></tr></table></table><br>        <%
    }
    %>




    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


