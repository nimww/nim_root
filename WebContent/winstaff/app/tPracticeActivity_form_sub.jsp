<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tPracticeActivity_form_sub.jsp
    Created on Oct/30/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iPracticeActivityID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iPracticeActivityID")) 
    {
        iPracticeActivityID        =    (Integer)pageControllerHash.get("iPracticeActivityID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
        if (iPracticeActivityID.intValue() != iEDITID.intValue())
        {
        	accessValid = false;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltPracticeActivity        PracticeActivity        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PracticeActivity        =    new    bltPracticeActivity(iPracticeActivityID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PracticeActivity        =    new    bltPracticeActivity(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !PracticeActivity.getUniqueCreateDate().equals(testObj)  )
    {
         PracticeActivity.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !PracticeActivity.getUniqueModifyDate().equals(testObj)  )
    {
         PracticeActivity.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !PracticeActivity.getUniqueModifyComments().equals(testObj)  )
    {
         PracticeActivity.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PracticeID")) ;
    if ( !PracticeActivity.getPracticeID().equals(testObj)  )
    {
         PracticeActivity.setPracticeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity PracticeID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Name")) ;
    if ( !PracticeActivity.getName().equals(testObj)  )
    {
         PracticeActivity.setName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity Name not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ActivityType")) ;
    if ( !PracticeActivity.getActivityType().equals(testObj)  )
    {
         PracticeActivity.setActivityType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity ActivityType not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("isCompleted")) ;
    if ( !PracticeActivity.getisCompleted().equals(testObj)  )
    {
         PracticeActivity.setisCompleted( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity isCompleted not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("StartDate"))) ;
    if ( !PracticeActivity.getStartDate().equals(testObj)  )
    {
         PracticeActivity.setStartDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity StartDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("EndDate"))) ;
    if ( !PracticeActivity.getEndDate().equals(testObj)  )
    {
         PracticeActivity.setEndDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity EndDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("RemindDate"))) ;
    if ( !PracticeActivity.getRemindDate().equals(testObj)  )
    {
         PracticeActivity.setRemindDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity RemindDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Description")) ;
    if ( !PracticeActivity.getDescription().equals(testObj)  )
    {
         PracticeActivity.setDescription( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity Description not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("TaskFileReference")) ;
    if ( !PracticeActivity.getTaskFileReference().equals(testObj)  )
    {
         PracticeActivity.setTaskFileReference( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity TaskFileReference not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("TaskLogicReference")) ;
    if ( !PracticeActivity.getTaskLogicReference().equals(testObj)  )
    {
         PracticeActivity.setTaskLogicReference( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity TaskLogicReference not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Summary")) ;
    if ( !PracticeActivity.getSummary().equals(testObj)  )
    {
         PracticeActivity.setSummary( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity Summary not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !PracticeActivity.getComments().equals(testObj)  )
    {
         PracticeActivity.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity Comments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReferenceID")) ;
    if ( !PracticeActivity.getReferenceID().equals(testObj)  )
    {
         PracticeActivity.setReferenceID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity ReferenceID not set. this is ok-not an error");
}


//upload doc
boolean isUpload = false;
try
{
    String testObj = new String(request.getParameter("AttachDoc")) ;
    if ( testObj.equalsIgnoreCase("1")  )
    {
		isUpload = true;
    }
}
catch(Exception e)
{
     //out.println("PracticeActivity EndDate not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("AssignedToID")) ;
    if ( !PracticeActivity.getAssignedToID().equals(testObj)  )
    {
         PracticeActivity.setAssignedToID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeActivity AssignedToID not set. this is ok-not an error");
}


boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
					if (pageControllerHash.containsKey("sParentReturnPage"))
					{
						sINTNext = (String)pageControllerHash.get("sParentReturnPage");
					}
//	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tPracticeActivity","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	PracticeActivity.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    PracticeActivity.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        PracticeActivity.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (isUpload)
{
	    %><script language=Javascript>
	      document.location="tPracticeActivity_upload.jsp";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
	
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
