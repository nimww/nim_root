<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.DataControlUtils,com.winstaff.bltPracticeMaster,com.winstaff.searchDB2, com.winstaff.bltStateLI,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils,com.winstaff.bltAdminPracticeLU,com.winstaff.bltAdminPracticeLU_List_LU_AdminID" %>
<%/*
    filename: tAdminPracticeLU_main_LU_AdminID.jsp
    Created on Apr/23/2002
    Type: 1-n main class file
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=100%>
    <tr><td width=10>&nbsp;</td>
    <td>
    <span class=title>Unlinked Practices</span><br />
Note: Only the first 200 are shown.


    <%
//initial declaration of list class and parentID
    Integer        iGenAdminID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iGenAdminID")) 
    {
        iGenAdminID        =    (Integer)pageControllerHash.get("iGenAdminID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","Practice_query.jsp?PracticeID=&PracticeName=&OfficeCity=&RelationshipTypeID=-1&OfficeStateID=0&OfficeZip=&orderBy=PracticeID&startID=0&maxResults=50&Submit2=Submit");

%>&nbsp;&nbsp;&nbsp;&nbsp;
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr> 
    <td> 
<%
try
{

String myPracticeID = "";
String myPracticeName = "";
String myOfficeCity = "";
String myOfficeStateID = "0";
String myOfficeZip = "";
String orderBy = "firstname";
int startID = 0;
int maxResults = 50;

boolean firstDisplay = false;

try
{
	maxResults = Integer.parseInt(request.getParameter("maxResults"));
	startID = Integer.parseInt(request.getParameter("startID"));
	myPracticeID = request.getParameter("PracticeID");
	myPracticeName = request.getParameter("PracticeName");
	myOfficeCity = request.getParameter("OfficeCity");
	myOfficeStateID = request.getParameter("OfficeStateID");
	myOfficeZip = request.getParameter("OfficeZip");
	orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
	maxResults = 50;
	startID = 0;
	firstDisplay = true;
	myPracticeID = "";
	myPracticeName = "";
	myOfficeCity = "";
	myOfficeStateID = "0";
	myOfficeZip = "";
	orderBy = "PracticeID";
}
if (orderBy == null)
{
	maxResults = 50;
	startID = 0;
	firstDisplay = true;
	myPracticeID = "";
	myPracticeName = "";
	myOfficeCity = "";
	myOfficeStateID = "0";
	myOfficeZip = "";
	orderBy = "PracticeID";
}


firstDisplay = false;
if (firstDisplay)
{
}
else
{



%>

  <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</p>
<table width=100% border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
<tr>
<td>
<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
<tr>
<td>

<form name="selfForm" method="get" action="Practice_query.jsp">
  <table border=0 cellspacing=1 width='100%' cellpadding=2>
    <tr > 
      <td colspan=3 class=tdHeader><p>Enter Search Criteria:</p></td>
      <td colspan=2 class=tdBase align=right>
       
    <input type=hidden name=maxResults value=<%=maxResults%> >
        <input type=hidden name=startID value=0 ><p>Sort:
          <select name=orderBy>
            <option value=PracticeID <%if (orderBy.equalsIgnoreCase("PracticeID")){out.println(" selected");}%> >ID</option>
            <option value=PracticeName <%if (orderBy.equalsIgnoreCase("PracticeName")){out.println(" selected");}%> >Practice Name</option>
            <option value=OfficeCity <%if (orderBy.equalsIgnoreCase("OfficeCity")){out.println(" selected");}%> >City</option>
            <option value=OfficeStateID <%if (orderBy.equalsIgnoreCase("OfficeStateID")){out.println(" selected");}%> >State</option>
            <option value=OfficeZip <%if (orderBy.equalsIgnoreCase("OfficeZip")){out.println(" selected");}%> >Postal Code</option>
          </select></p>
      </td>
      <td class=tdHeader colspan=2> 
        <p  align="center"> 
          <input type="Submit"  class="tdBaseAltGreen" name="Submit" value="Submit" align="middle">
          </p>
      </td>
    </tr>
    <tr class=tdHeader> 
      <td  colspan=1>&nbsp;      </td>
      <td  colspan=1>&nbsp;      </td>
      <td colspan=1> 
        <input type="text" name="PracticeID" value="<%=myPracticeID%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="PracticeName" value="<%=myPracticeName%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="OfficeCity" value="<%=myOfficeCity%>" size="10">
      </td>
      <td colspan=1> 
              <select name="OfficeStateID">
                <jsp:include page="../generic/tStateLIShort.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myOfficeStateID%>" />
                </jsp:include>
              </select>
      </td>
      <td colspan=1> 
        <input type="text" name="OfficeZip" value="<%=myOfficeZip%>" size="10">
      </td>
    </tr>
    <tr class=tdHeader> 
    <td>#</td>
    <td>Action</td>
      <td colspan=1> 
        PracticeID
      </td>
      <td colspan=1> 
        Practice Name
      </td>
      <td colspan=1> 
        City
      </td>
      <td colspan=1> 
        State
      </td>
      <td colspan=1> 
        Postal Code
      </td>
    </tr>



<%



//String myWhere = "where ( (PracticeID IN (select PracticeID from tAdminPracticeLU where (adminID='"+iAdminID+"')))	 ";
//String myWhere = "where ( adminID="+iAdminID;
//String myWhere = "where  (adminID="+iAdminID+") and (practiceID in (select practiceID from tPracticeMaster where (practiceID>0";
String myWhere = "where  (tAdminPracticeLU.lookupid is null OR tNIM3_Modality.modalityid is null) and (tPracticeMaster.PracticeID>0";

boolean theFirst = false;

try
{
if (!myPracticeID.equalsIgnoreCase(""))
{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "tPracticeMaster.PracticeID = '" + myPracticeID +"'";
	theFirst = false;
}
if (!myPracticeName.equalsIgnoreCase(""))
{
	if (!theFirst) { myWhere+=" and ";}
	myWhere += "UPPER(tPracticeMaster.PracticeName) LIKE UPPER('%" + DataControlUtils.fixApostrophe(myPracticeName) +"%')";
	theFirst = false;
}
if (!myOfficeCity.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "UPPER(tPracticeMaster.OfficeCity) LIKE UPPER('%" + DataControlUtils.fixApostrophe(myOfficeCity) +"%')";
theFirst = false;
}
if (!myOfficeStateID.equalsIgnoreCase("0"))
{
if (!myOfficeStateID.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "tPracticeMaster.OfficeStateID = " + myOfficeStateID +"";
theFirst = false;
}
}
if (!myOfficeZip.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "UPPER(tPracticeMaster.OfficeZip) LIKE UPPER('%" + myOfficeZip +"%')";
theFirst = false;
}
myWhere += ")";
//myWhere += ") ))";

//System.out.println(myWhere);
if (theFirst||myWhere.equalsIgnoreCase(")"))
{
myWhere = "";
}


}
catch(Exception e)
{
out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
String mySQL = ("select tPracticeMaster.PracticeID,tPracticeMaster.PracticeName,tPracticeMaster.OfficeCity,tPracticeMaster.OfficeStateID,tPracticeMaster.OfficeZIP from tPracticeMaster LEFT JOIN tAdminPracticeLU on tAdminPracticeLU.practiceid=tPracticeMaster.practiceID LEFT JOIN tNIM3_Modality on tNIM3_Modality.practiceid = tPracticeMaster.practiceid " + myWhere + "order by tPracticeMaster." + orderBy + " ");
myRS = mySS.executeStatement(mySQL);
//out.print(mySQL);
//myRS = mySS.executeStatement("select * from tAdminPracticeLU " + myWhere + " order by " + orderBy);

}
catch(Exception e)
{
	out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
//bltPracticeMaster pm = new bltPracticeMaster(new Integer(myRS.getString("PracticeID")));

String pmPracticeID = myRS.getString("PracticeID");
String pmPracticeName = myRS.getString("PracticeName");
String pmOfficeCity = myRS.getString("OfficeCity");
String pmOfficeStateID  = ( myRS.getString("OfficeStateID"));
String pmOfficeZIP = myRS.getString("OfficeZIP");







cnt++;
//if (cnt>=startID&&cnt<=startID+maxResults)
if (true||cnt<=maxResults)
{
cnt2++;

String myClass = "tdBase";
if (cnt2%2!=0)
{
myClass = "tdBaseAlt";
}
out.print("<tr class="+myClass+">");
out.print("<td>"+cnt+"</td>");
out.print("<td>NONE</td>");
out.print("<td>");
out.print("<h3>" + pmPracticeID+"</h3>");
out.print("</td>");
out.print("<td>");
out.print(pmPracticeName+"");
out.print("</td>");
out.print("<td>");
out.print(pmOfficeCity+"");
out.print("</td>");
out.print("<td>");

		if (pmOfficeStateID.equalsIgnoreCase("49") ) {out.print( "AK");}
		else if (pmOfficeStateID.equalsIgnoreCase("30") ) {out.print( "AL");}
		else if (pmOfficeStateID.equalsIgnoreCase("21") ) {out.print( "AR");}
		else if (pmOfficeStateID.equalsIgnoreCase("8") ) {out.print( "AZ");}
		else if (pmOfficeStateID.equalsIgnoreCase("1") ) {out.print( "CA");}
		else if (pmOfficeStateID.equalsIgnoreCase("10") ) {out.print( "CO");}
		else if (pmOfficeStateID.equalsIgnoreCase("45") ) {out.print( "CT");}
		else if (pmOfficeStateID.equalsIgnoreCase("51") ) {out.print( "DC");}
		else if (pmOfficeStateID.equalsIgnoreCase("47") ) {out.print( "DE");}
		else if (pmOfficeStateID.equalsIgnoreCase("33") ) {out.print( "FL");}
		else if (pmOfficeStateID.equalsIgnoreCase("32") ) {out.print( "GA");}
		else if (pmOfficeStateID.equalsIgnoreCase("50") ) {out.print( "HI");}
		else if (pmOfficeStateID.equalsIgnoreCase("19") ) {out.print( "IA");}
		else if (pmOfficeStateID.equalsIgnoreCase("5") ) {out.print( "ID");}
		else if (pmOfficeStateID.equalsIgnoreCase("24") ) {out.print( "IL");}
		else if (pmOfficeStateID.equalsIgnoreCase("27") ) {out.print( "IN");}
		else if (pmOfficeStateID.equalsIgnoreCase("15") ) {out.print( "KS");}
		else if (pmOfficeStateID.equalsIgnoreCase("28") ) {out.print( "KY");}
		else if (pmOfficeStateID.equalsIgnoreCase("22") ) {out.print( "LA");}
		else if (pmOfficeStateID.equalsIgnoreCase("43") ) {out.print( "MA");}
		else if (pmOfficeStateID.equalsIgnoreCase("48") ) {out.print( "MD");}
		else if (pmOfficeStateID.equalsIgnoreCase("34") ) {out.print( "ME");}
		else if (pmOfficeStateID.equalsIgnoreCase("26") ) {out.print( "MI");}
		else if (pmOfficeStateID.equalsIgnoreCase("18") ) {out.print( "MN");}
		else if (pmOfficeStateID.equalsIgnoreCase("20") ) {out.print( "MO");}
		else if (pmOfficeStateID.equalsIgnoreCase("25") ) {out.print( "MS");}
		else if (pmOfficeStateID.equalsIgnoreCase("4") ) {out.print( "MT");}
		else if (pmOfficeStateID.equalsIgnoreCase("52") ) {out.print( "NA");}
		else if (pmOfficeStateID.equalsIgnoreCase("41") ) {out.print( "NC");}
		else if (pmOfficeStateID.equalsIgnoreCase("12") ) {out.print( "ND");}
		else if (pmOfficeStateID.equalsIgnoreCase("14") ) {out.print( "NE");}
		else if (pmOfficeStateID.equalsIgnoreCase("35") ) {out.print( "NH");}
		else if (pmOfficeStateID.equalsIgnoreCase("46") ) {out.print( "NJ");}
		else if (pmOfficeStateID.equalsIgnoreCase("11") ) {out.print( "NM");}
		else if (pmOfficeStateID.equalsIgnoreCase("6") ) {out.print( "NV");}
		else if (pmOfficeStateID.equalsIgnoreCase("37") ) {out.print( "NY");}
		else if (pmOfficeStateID.equalsIgnoreCase("31") ) {out.print( "OH");}
		else if (pmOfficeStateID.equalsIgnoreCase("16") ) {out.print( "OK");}
		else if (pmOfficeStateID.equalsIgnoreCase("3") ) {out.print( "OR");}
		else if (pmOfficeStateID.equalsIgnoreCase("38") ) {out.print( "PA");}
		else if (pmOfficeStateID.equalsIgnoreCase("44") ) {out.print( "RI");}
		else if (pmOfficeStateID.equalsIgnoreCase("42") ) {out.print( "SC");}
		else if (pmOfficeStateID.equalsIgnoreCase("13") ) {out.print( "SD");}
		else if (pmOfficeStateID.equalsIgnoreCase("29") ) {out.print( "TN");}
		else if (pmOfficeStateID.equalsIgnoreCase("17") ) {out.print( "TX");}
		else if (pmOfficeStateID.equalsIgnoreCase("7") ) {out.print( "UT");}
		else if (pmOfficeStateID.equalsIgnoreCase("40") ) {out.print( "VA");}
		else if (pmOfficeStateID.equalsIgnoreCase("36") ) {out.print( "VT");}
		else if (pmOfficeStateID.equalsIgnoreCase("2") ) {out.print( "WA");}
		else if (pmOfficeStateID.equalsIgnoreCase("23") ) {out.print( "WI");}
		else if (pmOfficeStateID.equalsIgnoreCase("39") ) {out.print( "WV");}
		else if (pmOfficeStateID.equalsIgnoreCase("9") ) {out.print( "WY");}



out.print("</td>");
out.print("<td>");
out.print(pmOfficeZIP+"");
out.print("</td>");
out.print("</tr>");
}
   }
mySS.closeAll();
endCount = cnt;







}
catch(Exception e)
{
out.println("PrevNext:"+e);
}




try{

%>


</table> 


<%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>



  </table>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>     </td>
  </tr>
</table>



<%

  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }
%>

    </td></tr></table>

