<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*,java.util.List, java.util.Arrays" %>
<%/*
    filename: out\jsp\tPracticeMaster_form_sub.jsp
    Created on Mar/04/2003
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iPracticeID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("Practice1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
		
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
	String EDITID = "0";
    if ( request.getParameter( "EDITID" )!=null)
    {
        EDITID        =    request.getParameter( "EDITID" );
    }
	if (new Integer(EDITID).intValue()!=iPracticeID.intValue()  )
	{
		accessValid=false;
		%>
        <script language="javascript">
		alert ("Invalid Save\n\nDid you Open Another Record Prior to Saving (possibly in another window or tab)?");
		</script>
        <%
	}
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltPracticeMaster        PracticeMaster        =    null;
    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        PracticeMaster        =    new    bltPracticeMaster(iPracticeID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        PracticeMaster        =    new    bltPracticeMaster(UserSecurityGroupID,true);
    }

String testChangeID = "0";
String netDevCommTrack = "";



try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !PracticeMaster.getUniqueCreateDate().equals(testObj)  )
    {
         PracticeMaster.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !PracticeMaster.getUniqueModifyDate().equals(testObj)  )
    {
         PracticeMaster.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !PracticeMaster.getUniqueModifyComments().equals(testObj)  )
    {
         PracticeMaster.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster UniqueModifyComments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PracticeName")) ;
    if ( !PracticeMaster.getPracticeName().equals(testObj)  )
    {
         netDevCommTrack += "Updated PracticeName from: "+PracticeMaster.getPracticeName()+" to "+testObj+"<br>";
		 PracticeMaster.setPracticeName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PracticeName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DepartmentName")) ;
    if ( !PracticeMaster.getDepartmentName().equals(testObj)  )
    {
         netDevCommTrack += "Updated DepartmentName from: "+PracticeMaster.getDepartmentName()+" to "+testObj+"<br>";
		 PracticeMaster.setDepartmentName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DepartmentName not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TypeOfPractice")) ;
    if ( !PracticeMaster.getTypeOfPractice().equals(testObj)  )
    {
         String incoming ="";
		 switch(testObj){
			case 7: incoming = "Clinic"; break;
			case 2: incoming = "Group Practice"; break;
			case 4: incoming = "Multi Specialty"; break;
			case 0: incoming = "Not Applicable"; break;
			case 6: incoming = "Other"; break;
			case 5: incoming = "Partnership"; break;
			case 3: incoming = "Single Specialty"; break;
			case 1: incoming = "Solo Practice"; break; 
		 }
		 String old ="";
		 switch(PracticeMaster.getTypeOfPractice()){
			case 7: old = "Clinic"; break;
			case 2: old = "Group Practice"; break;
			case 4: old = "Multi Specialty"; break;
			case 0: old = "Not Applicable"; break;
			case 6: old = "Other"; break;
			case 5: old = "Partnership"; break;
			case 3: old = "Single Specialty"; break;
			case 1: old = "Solo Practice"; break; 
		 }
		 
		 netDevCommTrack += "Updated TypeOfPractice from: "+old+" to "+incoming+"<br>";
		 PracticeMaster.setTypeOfPractice( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster TypeOfPractice not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeFederalTaxID")) ;
    if ( !PracticeMaster.getOfficeFederalTaxID().equals(testObj)  )
    {
         netDevCommTrack += "Updated OfficeFederalTaxID from: "+PracticeMaster.getOfficeFederalTaxID()+" to "+testObj+"<br>";
		 PracticeMaster.setOfficeFederalTaxID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeFederalTaxID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeTaxIDNameAffiliation")) ;
    if ( !PracticeMaster.getOfficeTaxIDNameAffiliation().equals(testObj)  )
    {
         netDevCommTrack += "Updated OfficeTaxIDNameAffiliation from: "+PracticeMaster.getOfficeTaxIDNameAffiliation()+" to "+testObj+"<br>";
		 PracticeMaster.setOfficeTaxIDNameAffiliation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeTaxIDNameAffiliation not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ContractingStatusID")) ;
    if ( !PracticeMaster.getContractingStatusID().equals(testObj)  )
    {
         String incoming = "";
		 switch (new Integer(request.getParameter("ContractingStatusID"))) {
		  case 0:
				incoming = "None"; break;
		  case 3:
				incoming = "Target"; break;
		  case 5:
				incoming = "Sent Contract"; break;
		  case 1:
				incoming = "OTA"; break;
		  case 2:
				incoming = "Contracted"; break;
		  case 11:
				incoming = "Contracted [IPA]"; break;
		  case 9:
				incoming = "Contracted [*]"; break;
		  case 8:
				incoming = "Contracted [Select]"; break;
		  case 7:
				incoming = "OON"; break;
		  case 4:
				incoming = "Not Interested"; break;	  
		  }
		  String old = "";
		  switch (PracticeMaster.getContractingStatusID()) {
			  case 0:
					old = "None"; break;
			  case 3:
					old = "Target"; break;
			  case 5:
					old = "Sent Contract"; break;
			  case 1:
					old = "OTA"; break;
			  case 2:
					old = "Contracted"; break;
			  case 11:
					old = "Contracted [IPA]"; break;
			  case 9:
					old = "Contracted [*]"; break;
			  case 8:
					old = "Contracted [Select]"; break;
			  case 7:
					old = "OON"; break;
			  case 4:
					old = "Not Interested"; break;	  
		  }

		 netDevCommTrack += "Updated ContractingStatusID from: "+old+" to "+incoming+"<br>";
		 PracticeMaster.setContractingStatusID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster ContractingStatusID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OwnerName")) ;
    if ( !PracticeMaster.getOwnerName().equals(testObj)  )
    {
         netDevCommTrack += "Updated OwnerName from: "+PracticeMaster.getOwnerName()+" to "+testObj+"<br>";
		 PracticeMaster.setOwnerName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OwnerName not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_MRI_W"))) ;
    if ( !PracticeMaster.getPrice_Mod_MRI_W().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_MRI_W from: "+PracticeMaster.getPrice_Mod_MRI_W().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_MRI_W( testObj,UserSecurityGroupID   );
         testChangeID = "1";
		 
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_W not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_MRI_WO"))) ;
    if ( !PracticeMaster.getPrice_Mod_MRI_WO().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_MRI_WO from: "+PracticeMaster.getPrice_Mod_MRI_WO().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_MRI_WO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_WO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_MRI_WWO"))) ;
    if ( !PracticeMaster.getPrice_Mod_MRI_WWO().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_MRI_WWO from: "+PracticeMaster.getPrice_Mod_MRI_WWO().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_MRI_WWO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_WWO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_CT_W"))) ;
    if ( !PracticeMaster.getPrice_Mod_CT_W().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_CT_W from: "+PracticeMaster.getPrice_Mod_CT_W().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_CT_W( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_W not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_CT_WO"))) ;
    if ( !PracticeMaster.getPrice_Mod_CT_WO().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_CT_WO from: "+PracticeMaster.getPrice_Mod_CT_WO().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_CT_WO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_WO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_CT_WWO"))) ;
    if ( !PracticeMaster.getPrice_Mod_CT_WWO().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_CT_WWO from: "+PracticeMaster.getPrice_Mod_CT_WWO().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_CT_WWO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_WWO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_MRI_W"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_MRI_W().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_MRI_W from: "+PracticeMaster.getGH_Price_Mod_MRI_W().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_MRI_W( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_W not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_MRI_WO"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_MRI_WO().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_MRI_WO from: "+PracticeMaster.getGH_Price_Mod_MRI_WO().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_MRI_WO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_WO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_MRI_WWO"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_MRI_WWO().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_MRI_WWO from: "+PracticeMaster.getGH_Price_Mod_MRI_WWO().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_MRI_WWO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_WWO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_CT_W"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_CT_W().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_CT_W from: "+PracticeMaster.getGH_Price_Mod_CT_W().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_CT_W( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_W not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_CT_WO"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_CT_WO().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_CT_WO from: "+PracticeMaster.getGH_Price_Mod_CT_WO().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_CT_WO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_WO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_CT_WWO"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_CT_WWO().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_CT_WWO from: "+PracticeMaster.getGH_Price_Mod_CT_WWO().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_CT_WWO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_WWO not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAddress1")) ;
    if ( !PracticeMaster.getOfficeAddress1().equals(testObj)  )
    {
         netDevCommTrack += "Updated OfficeAddress1 from: "+PracticeMaster.getOfficeAddress1()+" to "+testObj+"<br>";
		 PracticeMaster.setOfficeAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAddress2")) ;
    if ( !PracticeMaster.getOfficeAddress2().equals(testObj)  )
    {
         netDevCommTrack += "Updated OfficeAddress2 from: "+PracticeMaster.getOfficeAddress2()+" to "+testObj+"<br>";
		 PracticeMaster.setOfficeAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeCity")) ;
    if ( !PracticeMaster.getOfficeCity().equals(testObj)  )
    {
         netDevCommTrack += "Updated OfficeCity from: "+PracticeMaster.getOfficeCity()+" to "+testObj+"<br>";
		 PracticeMaster.setOfficeCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("OfficeStateID")) ;
    if ( !PracticeMaster.getOfficeStateID().equals(testObj)  )
    {
         bltStateLI incoming = new bltStateLI(testObj);
		 bltStateLI old = new bltStateLI(PracticeMaster.getOfficeStateID());
		 
		 netDevCommTrack += "Updated OfficeState from: "+old.getShortState()+" to "+incoming.getShortState()+"<br>";
		 PracticeMaster.setOfficeStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeProvince")) ;
    if ( !PracticeMaster.getOfficeProvince().equals(testObj)  )
    {
         netDevCommTrack += "Updated OfficeProvince from: "+PracticeMaster.getOfficeProvince()+" to "+testObj+"<br>";
		 PracticeMaster.setOfficeProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeProvince not set. this is ok-not an error");
}

List<String> zipl = new java.util.ArrayList<String>();
String zipquery = "SELECT\n" +
		"	zi.zip_code\n" +
		"FROM\n" +
		"	zip_code zi";
searchDB2 ss = new searchDB2();
java.sql.ResultSet rs = ss.executeStatement(zipquery);
while(rs.next()){
	zipl.add(rs.getString(1));
}
ss.closeAll();
try
{
    String testObj = new String(request.getParameter("OfficeZIP")) ;
    if ( !PracticeMaster.getOfficeZIP().equals(testObj) && zipl.contains(testObj.toString()))
    {
         netDevCommTrack += "Updated OfficeZIP from: "+PracticeMaster.getOfficeZIP()+" to "+testObj+"<br>";
		 PracticeMaster.setOfficeZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("OfficeCountryID")) ;
    if ( !PracticeMaster.getOfficeCountryID().equals(testObj)  )
    {
         PracticeMaster.setOfficeCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeCountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficePhone")) ;
    if ( !PracticeMaster.getOfficePhone().equals(testObj)  )
    {
         netDevCommTrack += "Updated OfficePhone from: "+PracticeMaster.getOfficePhone()+" to "+testObj+"<br>";
		 PracticeMaster.setOfficePhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficePhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BackOfficePhoneNo")) ;
    if ( !PracticeMaster.getBackOfficePhoneNo().equals(testObj)  )
    {
         PracticeMaster.setBackOfficePhoneNo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BackOfficePhoneNo not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeFaxNo")) ;
    if ( !PracticeMaster.getOfficeFaxNo().equals(testObj)  )
    {
         netDevCommTrack += "Updated OfficeFaxNo from: "+PracticeMaster.getOfficeFaxNo()+" to "+testObj+"<br>";
		 PracticeMaster.setOfficeFaxNo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeFaxNo not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FaxReportReminderOverride")) ;
    if ( !PracticeMaster.getFaxReportReminderOverride().equals(testObj)  )
    {
         PracticeMaster.setFaxReportReminderOverride( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeFaxNo not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("OfficeEmail")) ;
    if ( !PracticeMaster.getOfficeEmail().equals(testObj)  )
    {
         PracticeMaster.setOfficeEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeManagerFirstName")) ;
    if ( !PracticeMaster.getOfficeManagerFirstName().equals(testObj)  )
    {
         PracticeMaster.setOfficeManagerFirstName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeManagerFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeManagerLastName")) ;
    if ( !PracticeMaster.getOfficeManagerLastName().equals(testObj)  )
    {
         PracticeMaster.setOfficeManagerLastName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeManagerLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeManagerPhone")) ;
    if ( !PracticeMaster.getOfficeManagerPhone().equals(testObj)  )
    {
         PracticeMaster.setOfficeManagerPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeManagerPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeManagerEmail")) ;
    if ( !PracticeMaster.getOfficeManagerEmail().equals(testObj)  )
    {
         PracticeMaster.setOfficeManagerEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeManagerEmail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AnsweringService")) ;
    if ( !PracticeMaster.getAnsweringService().equals(testObj)  )
    {
         PracticeMaster.setAnsweringService( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AnsweringService not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AnsweringServicePhone")) ;
    if ( !PracticeMaster.getAnsweringServicePhone().equals(testObj)  )
    {
         PracticeMaster.setAnsweringServicePhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AnsweringServicePhone not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Coverage247")) ;
    if ( !PracticeMaster.getCoverage247().equals(testObj)  )
    {
         PracticeMaster.setCoverage247( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Coverage247 not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MinorityEnterprise")) ;
    if ( !PracticeMaster.getMinorityEnterprise().equals(testObj)  )
    {
         PracticeMaster.setMinorityEnterprise( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MinorityEnterprise not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LanguagesSpokenInOffice")) ;
    if ( !PracticeMaster.getLanguagesSpokenInOffice().equals(testObj)  )
    {
         PracticeMaster.setLanguagesSpokenInOffice( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster LanguagesSpokenInOffice not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AcceptAllNewPatients")) ;
    if ( !PracticeMaster.getAcceptAllNewPatients().equals(testObj)  )
    {
         PracticeMaster.setAcceptAllNewPatients( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AcceptAllNewPatients not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AcceptExistingPayorChange")) ;
    if ( !PracticeMaster.getAcceptExistingPayorChange().equals(testObj)  )
    {
         PracticeMaster.setAcceptExistingPayorChange( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AcceptExistingPayorChange not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AcceptNewFromReferralOnly")) ;
    if ( !PracticeMaster.getAcceptNewFromReferralOnly().equals(testObj)  )
    {
         PracticeMaster.setAcceptNewFromReferralOnly( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AcceptNewFromReferralOnly not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AcceptNewMedicare")) ;
    if ( !PracticeMaster.getAcceptNewMedicare().equals(testObj)  )
    {
         PracticeMaster.setAcceptNewMedicare( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AcceptNewMedicare not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AcceptNewMedicaid")) ;
    if ( !PracticeMaster.getAcceptNewMedicaid().equals(testObj)  )
    {
         PracticeMaster.setAcceptNewMedicaid( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AcceptNewMedicaid not set. this is ok-not an error");
}


if (testChangeID.equalsIgnoreCase("1"))
{

	PracticeMaster.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    PracticeMaster.setUniqueModifyComments(UserLogonDescription);
	    PracticeMaster.commitData();
    }
}


try
{
    String testObj = new String(request.getParameter("PracticeLimitationAge")) ;
    if ( !PracticeMaster.getPracticeLimitationAge().equals(testObj)  )
    {
         PracticeMaster.setPracticeLimitationAge( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PracticeLimitationAge not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PracticeLimitationSex")) ;
    if ( !PracticeMaster.getPracticeLimitationSex().equals(testObj)  )
    {
         PracticeMaster.setPracticeLimitationSex( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PracticeLimitationSex not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PracticeLimitationOther")) ;
    if ( !PracticeMaster.getPracticeLimitationOther().equals(testObj)  )
    {
         PracticeMaster.setPracticeLimitationOther( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PracticeLimitationOther not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingPayableTo")) ;
    if ( !PracticeMaster.getBillingPayableTo().equals(testObj)  )
    {
         PracticeMaster.setBillingPayableTo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingPayableTo not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingFirstName")) ;
    if ( !PracticeMaster.getBillingFirstName().equals(testObj)  )
    {
         PracticeMaster.setBillingFirstName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingLastName")) ;
    if ( !PracticeMaster.getBillingLastName().equals(testObj)  )
    {
         PracticeMaster.setBillingLastName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingAddress1")) ;
    if ( !PracticeMaster.getBillingAddress1().equals(testObj)  )
    {
         PracticeMaster.setBillingAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingAddress2")) ;
    if ( !PracticeMaster.getBillingAddress2().equals(testObj)  )
    {
         PracticeMaster.setBillingAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingCity")) ;
    if ( !PracticeMaster.getBillingCity().equals(testObj)  )
    {
         PracticeMaster.setBillingCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("BillingStateID")) ;
    if ( !PracticeMaster.getBillingStateID().equals(testObj)  )
    {
         PracticeMaster.setBillingStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingProvince")) ;
    if ( !PracticeMaster.getBillingProvince().equals(testObj)  )
    {
         PracticeMaster.setBillingProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingProvince not set. this is ok-not an error");
}
List<String> zipl2 = new java.util.ArrayList<String>();
String zipquery2 = "SELECT\n" +
		"	zi.zip_code\n" +
		"FROM\n" +
		"	zip_code zi";
searchDB2 ss2 = new searchDB2();
java.sql.ResultSet rs2 = ss2.executeStatement(zipquery2);
while(rs2.next()){
	zipl2.add(rs2.getString(1));
}
ss2.closeAll();
try
{
    String testObj = new String(request.getParameter("BillingZIP"));
    if ( !PracticeMaster.getBillingZIP().equals(testObj) && zipl2.contains(testObj.toString()))
    {
         PracticeMaster.setBillingZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("BillingCountryID")) ;
    if ( !PracticeMaster.getBillingCountryID().equals(testObj)  )
    {
         PracticeMaster.setBillingCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingCountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingPhone")) ;
    if ( !PracticeMaster.getBillingPhone().equals(testObj)  )
    {
         PracticeMaster.setBillingPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingFax")) ;
    if ( !PracticeMaster.getBillingFax().equals(testObj)  )
    {
         PracticeMaster.setBillingFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactFirstName")) ;
    if ( !PracticeMaster.getCredentialingContactFirstName().equals(testObj)  )
    {
         netDevCommTrack += "Updated CredentialingContactFirstName from: "+PracticeMaster.getCredentialingContactFirstName()+" to "+testObj+"<br>";
		 PracticeMaster.setCredentialingContactFirstName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactLastName")) ;
    if ( !PracticeMaster.getCredentialingContactLastName().equals(testObj)  )
    {
         netDevCommTrack += "Updated CredentialingContactLastName from: "+PracticeMaster.getCredentialingContactLastName()+" to "+testObj+"<br>";
		 PracticeMaster.setCredentialingContactLastName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactAddress1")) ;
    if ( !PracticeMaster.getCredentialingContactAddress1().equals(testObj)  )
    {
         netDevCommTrack += "Updated CredentialingContactAddress1 from: "+PracticeMaster.getCredentialingContactAddress1()+" to "+testObj+"<br>";
		 PracticeMaster.setCredentialingContactAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactAddress2")) ;
    if ( !PracticeMaster.getCredentialingContactAddress2().equals(testObj)  )
    {
         netDevCommTrack += "Updated CredentialingContactAddress2 from: "+PracticeMaster.getCredentialingContactAddress2()+" to "+testObj+"<br>";
		 PracticeMaster.setCredentialingContactAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactCity")) ;
    if ( !PracticeMaster.getCredentialingContactCity().equals(testObj)  )
    {
         netDevCommTrack += "Updated CredentialingContactCity from: "+PracticeMaster.getCredentialingContactCity()+" to "+testObj+"<br>";
		 PracticeMaster.setCredentialingContactCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CredentialingContactStateID")) ;
    if ( !PracticeMaster.getCredentialingContactStateID().equals(testObj)  )
    {
         bltStateLI incoming = new bltStateLI(testObj);
		 bltStateLI old = new bltStateLI(PracticeMaster.getOfficeStateID());
		 
		 netDevCommTrack += "Updated CredentialingContactStateID from: "+old.getShortState()+" to "+incoming.getShortState()+"<br>";
		 PracticeMaster.setCredentialingContactStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactProvince")) ;
    if ( !PracticeMaster.getCredentialingContactProvince().equals(testObj)  )
    {
         netDevCommTrack += "Updated CredentialingContactLastName from: "+PracticeMaster.getCredentialingContactLastName()+" to "+testObj+"<br>";
		 PracticeMaster.setCredentialingContactProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactProvince not set. this is ok-not an error");
}
List<String> zipl3 = new java.util.ArrayList<String>();
String zipquery3 = "SELECT\n" +
		"	zi.zip_code\n" +
		"FROM\n" +
		"	zip_code zi";
searchDB2 ss3 = new searchDB2();
java.sql.ResultSet rs3 = ss3.executeStatement(zipquery3);
while(rs3.next()){
	zipl3.add(rs3.getString(1));
}
ss3.closeAll();
try
{
    String testObj = new String(request.getParameter("CredentialingContactZIP")) ;
    if ( !PracticeMaster.getCredentialingContactZIP().equals(testObj)  && zipl3.contains(testObj.toString()))
    {
         netDevCommTrack += "Updated CredentialingContactZIP from: "+PracticeMaster.getCredentialingContactZIP()+" to "+testObj+"<br>";
		 PracticeMaster.setCredentialingContactZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CredentialingContactCountryID")) ;
    if ( !PracticeMaster.getCredentialingContactCountryID().equals(testObj)  )
    {
         PracticeMaster.setCredentialingContactCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactCountryID not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("CredentialingContactPhone")) ;
    if ( !PracticeMaster.getCredentialingContactPhone().equals(testObj)  )
    {
         netDevCommTrack += "Updated CredentialingContactPhone from: "+PracticeMaster.getCredentialingContactPhone()+" to "+testObj+"<br>";
		 PracticeMaster.setCredentialingContactPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactFax")) ;
    if ( !PracticeMaster.getCredentialingContactFax().equals(testObj)  )
    {
         netDevCommTrack += "Updated CredentialingContactFax from: "+PracticeMaster.getCredentialingContactFax()+" to "+testObj+"<br>";
		 PracticeMaster.setCredentialingContactFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentiallingContactEmail")) ;
    if ( !PracticeMaster.getCredentiallingContactEmail().equals(testObj)  )
    {
         netDevCommTrack += "Updated CredentiallingContactEmail from: "+PracticeMaster.getCredentialingContactLastName()+" to "+testObj+"<br>";
		 PracticeMaster.setCredentiallingContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentiallingContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenMonday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenMonday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenMonday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenMonday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenTuesday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenTuesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenTuesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenTuesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenWednesday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenWednesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenWednesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenWednesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenThursday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenThursday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenThursday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenThursday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenFriday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenFriday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenFriday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenFriday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenSaturday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenSaturday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenSaturday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenSaturday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursOpenSunday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursOpenSunday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursOpenSunday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursOpenSunday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseMonday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseMonday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseMonday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseMonday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseTuesday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseTuesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseTuesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseTuesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseWednesday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseWednesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseWednesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseWednesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseThursday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseThursday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseThursday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseThursday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseFriday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseFriday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseFriday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseFriday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseSaturday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseSaturday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseSaturday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseSaturday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeWorkHoursCloseSunday")) ;
    if ( !PracticeMaster.getOfficeWorkHoursCloseSunday().equals(testObj)  )
    {
         PracticeMaster.setOfficeWorkHoursCloseSunday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeWorkHoursCloseSunday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenMonday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenMonday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenMonday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenMonday not set. this is ok-not an error");
}

if (testChangeID.equalsIgnoreCase("1"))
{

	PracticeMaster.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    PracticeMaster.setUniqueModifyComments(UserLogonDescription);
	    PracticeMaster.commitData();
    }
}


try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenTuesday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenTuesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenTuesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenTuesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenWednesday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenWednesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenWednesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenWednesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenThursday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenThursday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenThursday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenThursday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenFriday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenFriday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenFriday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenFriday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenSaturday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenSaturday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenSaturday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenSaturday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursOpenSunday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursOpenSunday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursOpenSunday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursOpenSunday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseMonday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseMonday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseMonday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseMonday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseTuesday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseTuesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseTuesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseTuesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseWednesday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseWednesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseWednesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseWednesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseThursday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseThursday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseThursday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseThursday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseFriday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseFriday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseFriday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseFriday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseSaturday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseSaturday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseSaturday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseSaturday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAfterHoursCloseSunday")) ;
    if ( !PracticeMaster.getOfficeAfterHoursCloseSunday().equals(testObj)  )
    {
         PracticeMaster.setOfficeAfterHoursCloseSunday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeAfterHoursCloseSunday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !PracticeMaster.getComments().equals(testObj)  )
    {
         PracticeMaster.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Comments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AnesthesiaLocal")) ;
    if ( !PracticeMaster.getAnesthesiaLocal().equals(testObj)  )
    {
         PracticeMaster.setAnesthesiaLocal( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AnesthesiaLocal not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AnesthesiaRegional")) ;
    if ( !PracticeMaster.getAnesthesiaRegional().equals(testObj)  )
    {
         PracticeMaster.setAnesthesiaRegional( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AnesthesiaRegional not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AnesthesiaConscious")) ;
    if ( !PracticeMaster.getAnesthesiaConscious().equals(testObj)  )
    {
         PracticeMaster.setAnesthesiaConscious( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AnesthesiaConscious not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AnesthesiaGeneral")) ;
    if ( !PracticeMaster.getAnesthesiaGeneral().equals(testObj)  )
    {
         PracticeMaster.setAnesthesiaGeneral( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AnesthesiaGeneral not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MinorSurgery")) ;
    if ( !PracticeMaster.getMinorSurgery().equals(testObj)  )
    {
         PracticeMaster.setMinorSurgery( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MinorSurgery not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Gynecology")) ;
    if ( !PracticeMaster.getGynecology().equals(testObj)  )
    {
         PracticeMaster.setGynecology( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Gynecology not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("XRayProcedures")) ;
    if ( !PracticeMaster.getXRayProcedures().equals(testObj)  )
    {
         PracticeMaster.setXRayProcedures( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster XRayProcedures not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DrawBlood")) ;
    if ( !PracticeMaster.getDrawBlood().equals(testObj)  )
    {
         PracticeMaster.setDrawBlood( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DrawBlood not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("BasicLab")) ;
    if ( !PracticeMaster.getBasicLab().equals(testObj)  )
    {
         PracticeMaster.setBasicLab( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BasicLab not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("EKG")) ;
    if ( !PracticeMaster.getEKG().equals(testObj)  )
    {
         PracticeMaster.setEKG( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster EKG not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MinorLacerations")) ;
    if ( !PracticeMaster.getMinorLacerations().equals(testObj)  )
    {
         PracticeMaster.setMinorLacerations( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MinorLacerations not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Pulmonary")) ;
    if ( !PracticeMaster.getPulmonary().equals(testObj)  )
    {
         PracticeMaster.setPulmonary( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Pulmonary not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Allergy")) ;
    if ( !PracticeMaster.getAllergy().equals(testObj)  )
    {
         PracticeMaster.setAllergy( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Allergy not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("VisualScreen")) ;
    if ( !PracticeMaster.getVisualScreen().equals(testObj)  )
    {
         PracticeMaster.setVisualScreen( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster VisualScreen not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Audiometry")) ;
    if ( !PracticeMaster.getAudiometry().equals(testObj)  )
    {
         PracticeMaster.setAudiometry( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Audiometry not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Sigmoidoscopy")) ;
    if ( !PracticeMaster.getSigmoidoscopy().equals(testObj)  )
    {
         PracticeMaster.setSigmoidoscopy( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Sigmoidoscopy not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Immunizations")) ;
    if ( !PracticeMaster.getImmunizations().equals(testObj)  )
    {
         PracticeMaster.setImmunizations( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Immunizations not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Asthma")) ;
    if ( !PracticeMaster.getAsthma().equals(testObj)  )
    {
         PracticeMaster.setAsthma( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Asthma not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IVTreatment")) ;
    if ( !PracticeMaster.getIVTreatment().equals(testObj)  )
    {
         PracticeMaster.setIVTreatment( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster IVTreatment not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Osteopathic")) ;
    if ( !PracticeMaster.getOsteopathic().equals(testObj)  )
    {
         PracticeMaster.setOsteopathic( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Osteopathic not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Hydration")) ;
    if ( !PracticeMaster.getHydration().equals(testObj)  )
    {
         PracticeMaster.setHydration( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Hydration not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CardiacStress")) ;
    if ( !PracticeMaster.getCardiacStress().equals(testObj)  )
    {
         PracticeMaster.setCardiacStress( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CardiacStress not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicalTherapy")) ;
    if ( !PracticeMaster.getPhysicalTherapy().equals(testObj)  )
    {
         PracticeMaster.setPhysicalTherapy( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PhysicalTherapy not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MaternalHealth")) ;
    if ( !PracticeMaster.getMaternalHealth().equals(testObj)  )
    {
         PracticeMaster.setMaternalHealth( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MaternalHealth not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CHDP")) ;
    if ( !PracticeMaster.getCHDP().equals(testObj)  )
    {
         PracticeMaster.setCHDP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CHDP not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeServicesComments")) ;
    if ( !PracticeMaster.getOfficeServicesComments().equals(testObj)  )
    {
         PracticeMaster.setOfficeServicesComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeServicesComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("LicenseDisplayed")) ;
    if ( !PracticeMaster.getLicenseDisplayed().equals(testObj)  )
    {
         PracticeMaster.setLicenseDisplayed( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster LicenseDisplayed not set. this is ok-not an error");
}

if (testChangeID.equalsIgnoreCase("1"))
{

	PracticeMaster.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    PracticeMaster.setUniqueModifyComments(UserLogonDescription);
	    PracticeMaster.commitData();
    }
}


try
{
    Integer testObj = new Integer(request.getParameter("CPRPresent")) ;
    if ( !PracticeMaster.getCPRPresent().equals(testObj)  )
    {
         PracticeMaster.setCPRPresent( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CPRPresent not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AmbuBagAvailable")) ;
    if ( !PracticeMaster.getAmbuBagAvailable().equals(testObj)  )
    {
         PracticeMaster.setAmbuBagAvailable( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AmbuBagAvailable not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("OxygenAvailable")) ;
    if ( !PracticeMaster.getOxygenAvailable().equals(testObj)  )
    {
         PracticeMaster.setOxygenAvailable( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OxygenAvailable not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SurgicalSuite")) ;
    if ( !PracticeMaster.getSurgicalSuite().equals(testObj)  )
    {
         PracticeMaster.setSurgicalSuite( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster SurgicalSuite not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CertType")) ;
    if ( !PracticeMaster.getCertType().equals(testObj)  )
    {
         PracticeMaster.setCertType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CertType not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("LabOnSite")) ;
    if ( !PracticeMaster.getLabOnSite().equals(testObj)  )
    {
         PracticeMaster.setLabOnSite( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster LabOnSite not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CLIANumber")) ;
    if ( !PracticeMaster.getCLIANumber().equals(testObj)  )
    {
         PracticeMaster.setCLIANumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CLIANumber not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CLIAWaiver")) ;
    if ( !PracticeMaster.getCLIAWaiver().equals(testObj)  )
    {
         PracticeMaster.setCLIAWaiver( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CLIAWaiver not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ADAAccessibility")) ;
    if ( !PracticeMaster.getADAAccessibility().equals(testObj)  )
    {
         PracticeMaster.setADAAccessibility( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster ADAAccessibility not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HandicapAccessBuilding")) ;
    if ( !PracticeMaster.getHandicapAccessBuilding().equals(testObj)  )
    {
         PracticeMaster.setHandicapAccessBuilding( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster HandicapAccessBuilding not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HandicapAccessRampRail")) ;
    if ( !PracticeMaster.getHandicapAccessRampRail().equals(testObj)  )
    {
         PracticeMaster.setHandicapAccessRampRail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster HandicapAccessRampRail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HandicapAccessParking")) ;
    if ( !PracticeMaster.getHandicapAccessParking().equals(testObj)  )
    {
         PracticeMaster.setHandicapAccessParking( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster HandicapAccessParking not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HandicapAccessWheel")) ;
    if ( !PracticeMaster.getHandicapAccessWheel().equals(testObj)  )
    {
         PracticeMaster.setHandicapAccessWheel( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster HandicapAccessWheel not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HandicapAccessRail")) ;
    if ( !PracticeMaster.getHandicapAccessRail().equals(testObj)  )
    {
         PracticeMaster.setHandicapAccessRail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster HandicapAccessRail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HandicapAccessElevator")) ;
    if ( !PracticeMaster.getHandicapAccessElevator().equals(testObj)  )
    {
         PracticeMaster.setHandicapAccessElevator( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster HandicapAccessElevator not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HandicapAccessBraille")) ;
    if ( !PracticeMaster.getHandicapAccessBraille().equals(testObj)  )
    {
         PracticeMaster.setHandicapAccessBraille( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster HandicapAccessBraille not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HandicapFountainPhone")) ;
    if ( !PracticeMaster.getHandicapFountainPhone().equals(testObj)  )
    {
         PracticeMaster.setHandicapFountainPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster HandicapFountainPhone not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("NearPublicTransportation")) ;
    if ( !PracticeMaster.getNearPublicTransportation().equals(testObj)  )
    {
         PracticeMaster.setNearPublicTransportation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster NearPublicTransportation not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DisabledTTY")) ;
    if ( !PracticeMaster.getDisabledTTY().equals(testObj)  )
    {
         PracticeMaster.setDisabledTTY( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DisabledTTY not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DisabledASL")) ;
    if ( !PracticeMaster.getDisabledASL().equals(testObj)  )
    {
         PracticeMaster.setDisabledASL( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DisabledASL not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ChildcareServices")) ;
    if ( !PracticeMaster.getChildcareServices().equals(testObj)  )
    {
         PracticeMaster.setChildcareServices( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster ChildcareServices not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SafetyFireExtinguisher")) ;
    if ( !PracticeMaster.getSafetyFireExtinguisher().equals(testObj)  )
    {
         PracticeMaster.setSafetyFireExtinguisher( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster SafetyFireExtinguisher not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SafetyExtinguisherInspected")) ;
    if ( !PracticeMaster.getSafetyExtinguisherInspected().equals(testObj)  )
    {
         PracticeMaster.setSafetyExtinguisherInspected( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster SafetyExtinguisherInspected not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SafetySmokeDetectors")) ;
    if ( !PracticeMaster.getSafetySmokeDetectors().equals(testObj)  )
    {
         PracticeMaster.setSafetySmokeDetectors( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster SafetySmokeDetectors not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SafetyCorridorsClear")) ;
    if ( !PracticeMaster.getSafetyCorridorsClear().equals(testObj)  )
    {
         PracticeMaster.setSafetyCorridorsClear( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster SafetyCorridorsClear not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("InfectionControlHandwashing")) ;
    if ( !PracticeMaster.getInfectionControlHandwashing().equals(testObj)  )
    {
         PracticeMaster.setInfectionControlHandwashing( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster InfectionControlHandwashing not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("InfectionControlGloves")) ;
    if ( !PracticeMaster.getInfectionControlGloves().equals(testObj)  )
    {
         PracticeMaster.setInfectionControlGloves( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster InfectionControlGloves not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("InfectionControlBloodCleaned")) ;
    if ( !PracticeMaster.getInfectionControlBloodCleaned().equals(testObj)  )
    {
         PracticeMaster.setInfectionControlBloodCleaned( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster InfectionControlBloodCleaned not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("FacilityComments")) ;
    if ( !PracticeMaster.getFacilityComments().equals(testObj)  )
    {
         PracticeMaster.setFacilityComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster FacilityComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MedicalRecordsIndividual")) ;
    if ( !PracticeMaster.getMedicalRecordsIndividual().equals(testObj)  )
    {
         PracticeMaster.setMedicalRecordsIndividual( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MedicalRecordsIndividual not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MedicalRecordsInaccessible")) ;
    if ( !PracticeMaster.getMedicalRecordsInaccessible().equals(testObj)  )
    {
         PracticeMaster.setMedicalRecordsInaccessible( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MedicalRecordsInaccessible not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MedicalRecordsUniform")) ;
    if ( !PracticeMaster.getMedicalRecordsUniform().equals(testObj)  )
    {
         PracticeMaster.setMedicalRecordsUniform( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MedicalRecordsUniform not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MedicalRecordsPatientsNamePerPage")) ;
    if ( !PracticeMaster.getMedicalRecordsPatientsNamePerPage().equals(testObj)  )
    {
         PracticeMaster.setMedicalRecordsPatientsNamePerPage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MedicalRecordsPatientsNamePerPage not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MedicalRecordsSignatureAuthor")) ;
    if ( !PracticeMaster.getMedicalRecordsSignatureAuthor().equals(testObj)  )
    {
         PracticeMaster.setMedicalRecordsSignatureAuthor( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MedicalRecordsSignatureAuthor not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MedicalRecordsEntryDate")) ;
    if ( !PracticeMaster.getMedicalRecordsEntryDate().equals(testObj)  )
    {
         PracticeMaster.setMedicalRecordsEntryDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MedicalRecordsEntryDate not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MedicalRecordsHistory")) ;
    if ( !PracticeMaster.getMedicalRecordsHistory().equals(testObj)  )
    {
         PracticeMaster.setMedicalRecordsHistory( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MedicalRecordsHistory not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("MedicalRecordsAdverse")) ;
    if ( !PracticeMaster.getMedicalRecordsAdverse().equals(testObj)  )
    {
         PracticeMaster.setMedicalRecordsAdverse( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MedicalRecordsAdverse not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MedicalRecordsComments")) ;
    if ( !PracticeMaster.getMedicalRecordsComments().equals(testObj)  )
    {
         PracticeMaster.setMedicalRecordsComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MedicalRecordsComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AllowPhysEdit")) ;
    if ( !PracticeMaster.getAllowPhysEdit().equals(testObj)  )
    {
         PracticeMaster.setAllowPhysEdit( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AllowPhysEdit not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("DiagnosticUltraSound")) ;
    if ( !PracticeMaster.getDiagnosticUltraSound().equals(testObj)  )
    {
         PracticeMaster.setDiagnosticUltraSound( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DiagnosticUltraSound not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Endoscopy")) ;
    if ( !PracticeMaster.getEndoscopy().equals(testObj)  )
    {
         PracticeMaster.setEndoscopy( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Endoscopy not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AcceptReferralsComments")) ;
    if ( !PracticeMaster.getAcceptReferralsComments().equals(testObj)  )
    {
         PracticeMaster.setAcceptReferralsComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AcceptReferralsComments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientBringComments")) ;
    if ( !PracticeMaster.getPatientBringComments().equals(testObj)  )
    {
         PracticeMaster.setPatientBringComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PatientBringComments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientReferralQuestions")) ;
    if ( !PracticeMaster.getPatientReferralQuestions().equals(testObj)  )
    {
         PracticeMaster.setPatientReferralQuestions( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PatientReferralQuestions not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("CLIAExpirationDate"))) ;
    if ( !PracticeMaster.getCLIAExpirationDate().equals(testObj)  )
    {
         PracticeMaster.setCLIAExpirationDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CLIAExpirationDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AuditStatus")) ;
    if ( !PracticeMaster.getAuditStatus().equals(testObj)  )
    {
         PracticeMaster.setAuditStatus( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AuditStatus not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("AuditDate"))) ;
    if ( !PracticeMaster.getAuditDate().equals(testObj)  )
    {
         PracticeMaster.setAuditDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AuditDate not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AuditScore")) ;
    if ( !PracticeMaster.getAuditScore().equals(testObj)  )
    {
         PracticeMaster.setAuditScore( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster AuditScore not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("WebURL")) ;
    if ( !PracticeMaster.getWebURL().equals(testObj)  )
    {
         PracticeMaster.setWebURL( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster WebURL not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NPINumber")) ;
    if ( !PracticeMaster.getNPINumber().equals(testObj)  )
    {
         PracticeMaster.setNPINumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster NPINumber not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("StateLicenseNumber")) ;
    if ( !PracticeMaster.getStateLicenseNumber().equals(testObj)  )
    {
         netDevCommTrack += "Updated StateLicenseNumber from: "+PracticeMaster.getStateLicenseNumber()+" to "+testObj+"<br>";
		 PracticeMaster.setStateLicenseNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster StateLicenseNumber not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MedicareLicenseNumber")) ;
    if ( !PracticeMaster.getMedicareLicenseNumber().equals(testObj)  )
    {
         netDevCommTrack += "Updated MedicareLicenseNumber from: "+PracticeMaster.getMedicareLicenseNumber()+" to "+testObj+"<br>";
		 PracticeMaster.setMedicareLicenseNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MedicareLicenseNumber not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("WalkinMRICT")) ;
    if ( !PracticeMaster.getWalkinMRICT().equals(testObj)  )
    {
         PracticeMaster.setWalkinMRICT( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster WalkinMRICT not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DiagnosticTechOnly")) ;
    if ( !PracticeMaster.getDiagnosticTechOnly().equals(testObj)  )
    {
         PracticeMaster.setDiagnosticTechOnly( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DiagnosticTechOnly not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DICOMForward")) ;
    if ( !PracticeMaster.getDICOMForward().equals(testObj)  )
    {
         PracticeMaster.setDICOMForward( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DICOMForward not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DiagnosticAfterHours")) ;
    if ( !PracticeMaster.getDiagnosticAfterHours().equals(testObj)  )
    {
         PracticeMaster.setDiagnosticAfterHours( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DiagnosticAfterHours not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("MalPracticePolicyNumber")) ;
    if ( !PracticeMaster.getMalPracticePolicyNumber().equals(testObj)  )
    {
         netDevCommTrack += "Updated General Liability Policy# from: "+PracticeMaster.getMalPracticePolicyNumber()+" to "+testObj+"<br>";
		 PracticeMaster.setMalPracticePolicyNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MalPracticePolicyNumber not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("MalPracticePolicyExpDate"))) ;
    if ( !PracticeMaster.getMalPracticePolicyExpDate().equals(testObj)  )
    {
         netDevCommTrack += "Updated General Liability Expiration Date from: "+displayDateSDF1.format(PracticeMaster.getMalPracticePolicyExpDate()).toString()+" to "+displayDateSDF1.format(testObj).toString()+"<br>";
		 PracticeMaster.setMalPracticePolicyExpDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MalPracticePolicyExpDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingAddressName")) ;
    if ( !PracticeMaster.getBillingAddressName().equals(testObj)  )
    {
         PracticeMaster.setBillingAddressName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingAddressName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CredentialingContactAddressName")) ;
    if ( !PracticeMaster.getCredentialingContactAddressName().equals(testObj)  )
    {
         PracticeMaster.setCredentialingContactAddressName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster CredentialingContactAddressName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MalPracticePolicyCarrier")) ;
    if ( !PracticeMaster.getMalPracticePolicyCarrier().equals(testObj)  )
    {
         netDevCommTrack += "Updated General Liability Carrier from: "+PracticeMaster.getMalPracticePolicyCarrier()+" to "+testObj+"<br>";
		 PracticeMaster.setMalPracticePolicyCarrier( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster MalPracticePolicyCarrier not set. this is ok-not an error");
}
try
{
    String testObj = new String(request.getParameter("BillingEmail")) ;
    if ( !PracticeMaster.getBillingEmail().equals(testObj)  )
    {
         PracticeMaster.setBillingEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster BillingEmail not set. this is ok-not an error");
}



try
{
    Integer testObj = new Integer(request.getParameter("FeeScheduleRefID")) ;
    if ( !PracticeMaster.getFeeScheduleRefID().equals(testObj)  )
    {
         netDevCommTrack += "Updated FeeScheduleRefID from: "+PracticeMaster.getFeeScheduleRefID().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setFeeScheduleRefID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster FeeScheduleRefID not set. this is ok-not an error");
}
try
{
    Integer testObj = new Integer(request.getParameter("GH_FeeScheduleRefID")) ;
    if ( !PracticeMaster.getGH_FeeScheduleRefID().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_FeeScheduleRefID from: "+PracticeMaster.getGH_FeeScheduleRefID().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_FeeScheduleRefID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster FeeScheduleRefID not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("FeePercentage"))) ;
    if ( !PracticeMaster.getFeePercentage().equals(testObj)  )
    {
         netDevCommTrack += "Updated FeePercentage from: "+PracticeMaster.getFeePercentage().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setFeePercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster FeePercentage not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_FeePercentage"))) ;
    if ( !PracticeMaster.getGH_FeePercentage().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_FeePercentage from: "+PracticeMaster.getGH_FeePercentage().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_FeePercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster FeePercentage not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("Rate_CurbAppeal")) ;
    if ( !PracticeMaster.getRate_CurbAppeal().equals(testObj)  )
    {
         PracticeMaster.setRate_CurbAppeal( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Rate_CurbAppeal not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Rate_AccessSafety")) ;
    if ( !PracticeMaster.getRate_AccessSafety().equals(testObj)  )
    {
         PracticeMaster.setRate_AccessSafety( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Rate_AccessSafety not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Rate_Cleanliness")) ;
    if ( !PracticeMaster.getRate_Cleanliness().equals(testObj)  )
    {
         PracticeMaster.setRate_Cleanliness( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Rate_Cleanliness not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Rate_Appropriateness")) ;
    if ( !PracticeMaster.getRate_Appropriateness().equals(testObj)  )
    {
         PracticeMaster.setRate_Appropriateness( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Rate_Appropriateness not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Rate_StaffFriendly")) ;
    if ( !PracticeMaster.getRate_StaffFriendly().equals(testObj)  )
    {
         PracticeMaster.setRate_StaffFriendly( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Rate_StaffFriendly not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Rate_AttentiveTimely")) ;
    if ( !PracticeMaster.getRate_AttentiveTimely().equals(testObj)  )
    {
         PracticeMaster.setRate_AttentiveTimely( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Rate_AttentiveTimely not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("InitialContractDate"))) ;
    if ( !PracticeMaster.getInitialContractDate().equals(testObj)  )
    {
         PracticeMaster.setInitialContractDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster InitialContractDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ContractDate"))) ;
    if ( !PracticeMaster.getContractDate().equals(testObj)  )
    {
         PracticeMaster.setContractDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster ContractDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TerminatedDate"))) ;
    if ( !PracticeMaster.getTerminatedDate().equals(testObj)  )
    {
         PracticeMaster.setTerminatedDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster ContractDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PACSSystemVendor")) ;
    if ( !PracticeMaster.getPACSSystemVendor().equals(testObj)  )
    {
         PracticeMaster.setPACSSystemVendor( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PACSSystemVendor not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("RISSystemVendor")) ;
    if ( !PracticeMaster.getRISSystemVendor().equals(testObj)  )
    {
         PracticeMaster.setRISSystemVendor( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster RISSystemVendor not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsBlueStar")) ;
    if ( !PracticeMaster.getIsBlueStar().equals(testObj)  )
    {
         PracticeMaster.setIsBlueStar( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster IsBlueStar not set. this is ok-not an error");
}
try
{
    Integer testObj = new Integer(request.getParameter("Terminated")) ;
    if ( !PracticeMaster.getTerminated().equals(testObj)  )
    {
         PracticeMaster.setTerminated( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster IsBlueStar not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("InitialQuota")) ;
    if ( !PracticeMaster.getInitialQuota().equals(testObj)  )
    {
         PracticeMaster.setInitialQuota( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster InitialQuota not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PaymentTermsNet")) ;
    if ( !PracticeMaster.getPaymentTermsNet().equals(testObj)  )
    {
         PracticeMaster.setPaymentTermsNet( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PaymentTermsNet not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PaymentTermsMethod")) ;
    if ( !PracticeMaster.getPaymentTermsMethod().equals(testObj)  )
    {
         PracticeMaster.setPaymentTermsMethod( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PaymentTermsMethod not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DoesAging")) ;
    if ( !PracticeMaster.getDoesAging().equals(testObj)  )
    {
         PracticeMaster.setDoesAging( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DoesAging not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("DiagnosticMDArthrogram")) ;
    if ( !PracticeMaster.getDiagnosticMDArthrogram().equals(testObj)  )
    {
         PracticeMaster.setDiagnosticMDArthrogram( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DiagnosticMDArthrogram not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("GH_InitialQuota")) ;
    if ( !PracticeMaster.getGH_InitialQuota().equals(testObj)  )
    {
         PracticeMaster.setGH_InitialQuota( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster InitialQuota not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("GH_PaymentTermsNet")) ;
    if ( !PracticeMaster.getGH_PaymentTermsNet().equals(testObj)  )
    {
         PracticeMaster.setGH_PaymentTermsNet( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PaymentTermsNet not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("GH_PaymentTermsMethod")) ;
    if ( !PracticeMaster.getGH_PaymentTermsMethod().equals(testObj)  )
    {
         PracticeMaster.setGH_PaymentTermsMethod( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PaymentTermsMethod not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("clientTypeID")) ;
    if ( !PracticeMaster.getClientTypeID().equals(testObj)  )
    {
         PracticeMaster.setClientTypeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DoesAging not set. this is ok-not an error");
}
try
{
    Integer testObj = new Integer(request.getParameter("GH_DoesAging")) ;
    if ( !PracticeMaster.getGH_DoesAging().equals(testObj)  )
    {
         PracticeMaster.setGH_DoesAging( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DoesAging not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("GH_DiagnosticMDArthrogram")) ;
    if ( !PracticeMaster.getGH_DiagnosticMDArthrogram().equals(testObj)  )
    {
         PracticeMaster.setGH_DiagnosticMDArthrogram( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DiagnosticMDArthrogram not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursOpenMonday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursOpenMonday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursOpenMonday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursOpenMonday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursOpenTuesday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursOpenTuesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursOpenTuesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursOpenTuesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursOpenWednesday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursOpenWednesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursOpenWednesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursOpenWednesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursOpenThursday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursOpenThursday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursOpenThursday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursOpenThursday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursOpenFriday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursOpenFriday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursOpenFriday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursOpenFriday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursOpenSaturday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursOpenSaturday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursOpenSaturday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursOpenSaturday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursOpenSunday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursOpenSunday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursOpenSunday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursOpenSunday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursCloseMonday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursCloseMonday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursCloseMonday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursCloseMonday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursCloseTuesday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursCloseTuesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursCloseTuesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursCloseTuesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursCloseWednesday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursCloseWednesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursCloseWednesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursCloseWednesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursCloseThursday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursCloseThursday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursCloseThursday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursCloseThursday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursCloseFriday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursCloseFriday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursCloseFriday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursCloseFriday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursCloseSaturday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursCloseSaturday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursCloseSaturday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursCloseSaturday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDArthrogramHoursCloseSunday")) ;
    if ( !PracticeMaster.getOfficeMDArthrogramHoursCloseSunday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDArthrogramHoursCloseSunday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDArthrogramHoursCloseSunday not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DiagnosticMDMyelogram")) ;
    if ( !PracticeMaster.getDiagnosticMDMyelogram().equals(testObj)  )
    {
         PracticeMaster.setDiagnosticMDMyelogram( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DiagnosticMDMyelogram not set. this is ok-not an error");
}
try
{
    Integer testObj = new Integer(request.getParameter("GH_DiagnosticMDMyelogram")) ;
    if ( !PracticeMaster.getGH_DiagnosticMDMyelogram().equals(testObj)  )
    {
         PracticeMaster.setGH_DiagnosticMDMyelogram( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster DiagnosticMDMyelogram not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursOpenMonday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursOpenMonday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursOpenMonday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursOpenMonday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursOpenTuesday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursOpenTuesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursOpenTuesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursOpenTuesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursOpenWednesday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursOpenWednesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursOpenWednesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursOpenWednesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursOpenThursday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursOpenThursday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursOpenThursday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursOpenThursday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursOpenFriday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursOpenFriday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursOpenFriday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursOpenFriday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursOpenSaturday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursOpenSaturday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursOpenSaturday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursOpenSaturday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursOpenSunday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursOpenSunday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursOpenSunday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursOpenSunday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursCloseMonday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursCloseMonday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursCloseMonday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursCloseMonday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursCloseTuesday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursCloseTuesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursCloseTuesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursCloseTuesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursCloseWednesday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursCloseWednesday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursCloseWednesday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursCloseWednesday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursCloseThursday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursCloseThursday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursCloseThursday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursCloseThursday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursCloseFriday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursCloseFriday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursCloseFriday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursCloseFriday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursCloseSaturday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursCloseSaturday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursCloseSaturday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursCloseSaturday not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeMDMyelogramHoursCloseSunday")) ;
    if ( !PracticeMaster.getOfficeMDMyelogramHoursCloseSunday().equals(testObj)  )
    {
         PracticeMaster.setOfficeMDMyelogramHoursCloseSunday( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster OfficeMDMyelogramHoursCloseSunday not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("PetLinQID")) ;
    if ( !PracticeMaster.getPetLinQID().equals(testObj)  )
    {
         PracticeMaster.setPetLinQID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster PetLinQID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Scheduling_Notes")) ;
    if ( !PracticeMaster.getScheduling_Notes().equals(testObj)  )
    {
         PracticeMaster.setScheduling_Notes( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Scheduling_Notes not set. this is ok-not an error");
}




try
{
    Integer testObj = new Integer(request.getParameter("ContractGlobal")) ;
    if ( !PracticeMaster.getContractGlobal().equals(testObj)  )
    {
         String incoming = "";
		 switch (new Integer(request.getParameter("ContractGlobal"))) {
		  case 2:
				incoming = "None"; break;
		  case 3:
				incoming = "Target"; break;
		  case 1:
				incoming = "Sent Contract"; break;
		  }
		  String old = "";
		 switch (testObj) {
		  case 2:
				old = "None"; break;
		  case 3:
				old = "Target"; break;
		  case 1:
				old = "Sent Contract"; break;
		  }
		 netDevCommTrack += "Updated ContractGlobal from: "+old+" to "+incoming+"<br>";
		 
		 PracticeMaster.setContractGlobal( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster ContractGlobal not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ContractTechOnly")) ;
    if ( !PracticeMaster.getContractTechOnly().equals(testObj)  )
    {
         String incoming = "";
		 switch (new Integer(request.getParameter("ContractTechOnly"))) {
		  case 2:
				incoming = "None"; break;
		  case 3:
				incoming = "Target"; break;
		  case 1:
				incoming = "Sent Contract"; break;
		  }
		  String old = "";
		 switch (testObj) {
		  case 2:
				old = "None"; break;
		  case 3:
				old = "Target"; break;
		  case 1:
				old = "Sent Contract"; break;
		  }
		 netDevCommTrack += "Updated ContractTechOnly from: "+old+" to "+incoming+"<br>";
		 
		 PracticeMaster.setContractTechOnly( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster ContractTechOnly not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ContractProfessionalOnly")) ;
    if ( !PracticeMaster.getContractProfessionalOnly().equals(testObj)  )
    {
         String incoming = "";
		 switch (new Integer(request.getParameter("ContractProfessionalOnly"))) {
		  case 2:
				incoming = "None"; break;
		  case 3:
				incoming = "Target"; break;
		  case 1:
				incoming = "Sent Contract"; break;
		  }
		  String old = "";
		 switch (testObj) {
		  case 2:
				old = "None"; break;
		  case 3:
				old = "Target"; break;
		  case 1:
				old = "Sent Contract"; break;
		  }
		 netDevCommTrack += "Updated ContractProfessionalOnly from: "+old+" to "+incoming+"<br>";
		 
		 PracticeMaster.setContractProfessionalOnly( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster ContractProfessionalOnly not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("FeeScheduleOverRefID")) ;
    if ( !PracticeMaster.getFeeScheduleOverRefID().equals(testObj)  )
    {
         netDevCommTrack += "Updated FeeScheduleOverRefID from: "+PracticeMaster.getFeeScheduleOverRefID().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setFeeScheduleOverRefID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster FeeScheduleOverRefID not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("FeeOverPercentage"))) ;
    if ( !PracticeMaster.getFeeOverPercentage().equals(testObj)  )
    {
         netDevCommTrack += "Updated FeeOverPercentage from: "+PracticeMaster.getFeeOverPercentage().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setFeeOverPercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster FeeOverPercentage not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("GH_FeeScheduleOverRefID")) ;
    if ( !PracticeMaster.getGH_FeeScheduleOverRefID().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_FeeScheduleOverRefID from: "+PracticeMaster.getGH_FeeScheduleOverRefID().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_FeeScheduleOverRefID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster FeeScheduleOverRefID not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_FeeOverPercentage"))) ;
    if ( !PracticeMaster.getGH_FeeOverPercentage().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_FeeOverPercentage from: "+PracticeMaster.getGH_FeeOverPercentage().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_FeeOverPercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster FeeOverPercentage not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("GH_FeeScheduleOverRefID")) ;
    if ( !PracticeMaster.getGH_FeeScheduleOverRefID().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_FeeScheduleOverRefID from: "+PracticeMaster.getGH_FeeScheduleOverRefID().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_FeeScheduleOverRefID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster FeeScheduleOverRefID not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_FeeOverPercentage"))) ;
    if ( !PracticeMaster.getGH_FeeOverPercentage().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_FeeOverPercentage from: "+PracticeMaster.getGH_FeeOverPercentage().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_FeeOverPercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster FeeOverPercentage not set. this is ok-not an error");
}


try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_MRI_W_26"))) ;
    if ( !PracticeMaster.getPrice_Mod_MRI_W_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_MRI_W_26 from: "+PracticeMaster.getPrice_Mod_MRI_W_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_MRI_W_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_W_26 not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_MRI_WO_26"))) ;
    if ( !PracticeMaster.getPrice_Mod_MRI_WO_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_MRI_WO_26 from: "+PracticeMaster.getPrice_Mod_MRI_WO_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_MRI_WO_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_WO_26 not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_MRI_WWO_26"))) ;
    if ( !PracticeMaster.getPrice_Mod_MRI_WWO_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_MRI_WWO_26 from: "+PracticeMaster.getPrice_Mod_MRI_WWO_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_MRI_WWO_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_WWO_26 not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_CT_W_26"))) ;
    if ( !PracticeMaster.getPrice_Mod_CT_W_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_CT_W_26 from: "+PracticeMaster.getPrice_Mod_CT_W_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_CT_W_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_W_26 not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_CT_WO_26"))) ;
    if ( !PracticeMaster.getPrice_Mod_CT_WO_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_CT_WO_26 from: "+PracticeMaster.getPrice_Mod_CT_WO_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_CT_WO_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_WO_26 not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_CT_WWO_26"))) ;
    if ( !PracticeMaster.getPrice_Mod_CT_WWO_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_CT_WWO_26 from: "+PracticeMaster.getPrice_Mod_CT_WWO_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_CT_WWO_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_WWO_26 not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_MRI_W_TC"))) ;
    if ( !PracticeMaster.getPrice_Mod_MRI_W_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_MRI_W_TC from: "+PracticeMaster.getPrice_Mod_MRI_W_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_MRI_W_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_W_TC not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_MRI_WO_TC"))) ;
    if ( !PracticeMaster.getPrice_Mod_MRI_WO_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_MRI_WO_TC from: "+PracticeMaster.getPrice_Mod_MRI_WO_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_MRI_WO_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_WO_TC not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_MRI_WWO_TC"))) ;
    if ( !PracticeMaster.getPrice_Mod_MRI_WWO_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_MRI_WWO_TC from: "+PracticeMaster.getPrice_Mod_MRI_WWO_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_MRI_WWO_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_WWO_TC not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_CT_W_TC"))) ;
    if ( !PracticeMaster.getPrice_Mod_CT_W_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_CT_W_TC from: "+PracticeMaster.getPrice_Mod_CT_W_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_CT_W_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_W_TC not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_CT_WO_TC"))) ;
    if ( !PracticeMaster.getPrice_Mod_CT_WO_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_CT_WO_TC from: "+PracticeMaster.getPrice_Mod_CT_WO_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_CT_WO_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_WO_TC not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_CT_WWO_TC"))) ;
    if ( !PracticeMaster.getPrice_Mod_CT_WWO_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_CT_WWO_TC from: "+PracticeMaster.getPrice_Mod_CT_WWO_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_CT_WWO_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_WWO_TC not set. this is ok-not an error");
}


//Start PET CT Saves

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_PETCT_WWO"))) ;
    if ( !PracticeMaster.getPrice_Mod_PETCT_WWO().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_PETCT_WWO from: "+PracticeMaster.getPrice_Mod_PETCT_WWO().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_PETCT_WWO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_PETCT_WWO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_PETCT_WWO_TC"))) ;
    if ( !PracticeMaster.getPrice_Mod_PETCT_WWO_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_PETCT_WWO_TC from: "+PracticeMaster.getPrice_Mod_PETCT_WWO_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_PETCT_WWO_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_PETCT_WWO_TC not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_PETCT_WWO_26"))) ;
    if ( !PracticeMaster.getPrice_Mod_PETCT_WWO_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated Price_Mod_PETCT_WWO_26 from: "+PracticeMaster.getPrice_Mod_PETCT_WWO_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setPrice_Mod_PETCT_WWO_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_PETCT_WWO_26 not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_PETCT_WWO"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_PETCT_WWO().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_PETCT_WWO from: "+PracticeMaster.getGH_Price_Mod_PETCT_WWO().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_PETCT_WWO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster GH_GH_GH_Price_Mod_PETCT_WWO not set. this is ok-not an error");
}




try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_PETCT_WWO_TC"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_PETCT_WWO_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_PETCT_WWO_TC from: "+PracticeMaster.getGH_Price_Mod_PETCT_WWO_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_PETCT_WWO_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster GH_Price_Mod_PETCT_WWO_TC not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_PETCT_WWO_26"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_PETCT_WWO_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_PETCT_WWO_26 from: "+PracticeMaster.getGH_Price_Mod_PETCT_WWO_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_PETCT_WWO_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster GH_Price_Mod_PETCT_WWO_26 not set. this is ok-not an error");
}



//End PET CT Saves






try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_MRI_W_26"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_MRI_W_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_MRI_W_26 from: "+PracticeMaster.getGH_Price_Mod_MRI_W_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_MRI_W_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_W_26 not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_MRI_WO_26"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_MRI_WO_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_MRI_WO_26 from: "+PracticeMaster.getGH_Price_Mod_MRI_WO_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_MRI_WO_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_WO_26 not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_MRI_WWO_26"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_MRI_WWO_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_MRI_WWO_26 from: "+PracticeMaster.getGH_Price_Mod_MRI_WWO_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_MRI_WWO_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_WWO_26 not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_CT_W_26"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_CT_W_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_CT_W_26 from: "+PracticeMaster.getGH_Price_Mod_CT_W_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_CT_W_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_W_26 not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_CT_WO_26"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_CT_WO_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_CT_WO_26 from: "+PracticeMaster.getGH_Price_Mod_CT_WO_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_CT_WO_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_WO_26 not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_CT_WWO_26"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_CT_WWO_26().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_CT_WWO_26 from: "+PracticeMaster.getGH_Price_Mod_CT_WWO_26().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_CT_WWO_26( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_WWO_26 not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_MRI_W_TC"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_MRI_W_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_MRI_W_TC from: "+PracticeMaster.getGH_Price_Mod_MRI_W_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_MRI_W_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_W_TC not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_MRI_WO_TC"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_MRI_WO_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_MRI_WO_TC from: "+PracticeMaster.getGH_Price_Mod_MRI_WO_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_MRI_WO_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_WO_TC not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_MRI_WWO_TC"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_MRI_WWO_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_MRI_WWO_TC from: "+PracticeMaster.getGH_Price_Mod_MRI_WWO_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_MRI_WWO_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_MRI_WWO_TC not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_CT_W_TC"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_CT_W_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_CT_W_TC from: "+PracticeMaster.getGH_Price_Mod_CT_W_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_CT_W_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_W_TC not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_CT_WO_TC"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_CT_WO_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_CT_WO_TC from: "+PracticeMaster.getGH_Price_Mod_CT_WO_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_CT_WO_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_WO_TC not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("GH_Price_Mod_CT_WWO_TC"))) ;
    if ( !PracticeMaster.getGH_Price_Mod_CT_WWO_TC().equals(testObj)  )
    {
         netDevCommTrack += "Updated GH_Price_Mod_CT_WWO_TC from: "+PracticeMaster.getGH_Price_Mod_CT_WWO_TC().toString()+" to "+testObj.toString()+"<br>";
		 PracticeMaster.setGH_Price_Mod_CT_WWO_TC( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("PracticeMaster Price_Mod_CT_WWO_TC not set. this is ok-not an error");
}






boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";

// If an edit, update information; if new, append information
boolean errorRouteMe = false;

if (testChangeID.equalsIgnoreCase("1"))
{
	bltUserAccount UA = new bltUserAccount(CurrentUserAccount.getUserID());
	bltNIM3_NetDevCommTrack NIM3_NetDevCommTrack = null;
	String query = "select max(netdevcommtrackid)+1 netdevcommtrackid from tnim3_netdevcommtrack";
	searchDB2 conn = new searchDB2();
	java.sql.ResultSet results = conn.executeStatement(query);
	
	while (results!=null&&results.next())
	{
		NIM3_NetDevCommTrack = new bltNIM3_NetDevCommTrack(results.getInt("netdevcommtrackid"),true);
	}
	conn.closeAll();
	
	NIM3_NetDevCommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false),UserSecurityGroupID);
	NIM3_NetDevCommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false),UserSecurityGroupID);
	NIM3_NetDevCommTrack.setUniqueModifyComments(UA.getContactFirstName()+" "+UA.getContactLastName(),UserSecurityGroupID);
	NIM3_NetDevCommTrack.setAccountID(iPracticeID,UserSecurityGroupID);
	NIM3_NetDevCommTrack.setCommTypeID(0,UserSecurityGroupID);
	NIM3_NetDevCommTrack.setCommStart(PLCUtils.getNowDate(false),UserSecurityGroupID);
	NIM3_NetDevCommTrack.setCommEnd(PLCUtils.getNowDate(false),UserSecurityGroupID);
	NIM3_NetDevCommTrack.setMessageText(netDevCommTrack,UserSecurityGroupID);
	NIM3_NetDevCommTrack.setMessageName(UA.getContactFirstName()+" "+UA.getContactLastName(),UserSecurityGroupID);
	
	NIM3_NetDevCommTrack.commitData();
	
	PracticeMaster.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    PracticeMaster.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
		    PracticeMaster.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}

if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	nextPage = "practice_Status.jsp";
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>

  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
