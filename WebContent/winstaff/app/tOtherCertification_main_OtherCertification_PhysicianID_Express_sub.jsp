<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages,com.winstaff.bltOtherCertification,com.winstaff.bltOtherCertification_List,com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: tOtherCertification_main_OtherCertification_PhysicianID.jsp
    Created on Nov/12/2003
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
    <%
    Integer iExpressMode = new Integer(1);
    if (pageControllerHash.containsKey("iExpressMode")) 
    {
        iExpressMode =    (Integer)pageControllerHash.get("iExpressMode");
    }
    %>

    <table cellpadding=0 cellspacing=0 border=0 width=800 >
<%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("PractitionerSection5", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }

  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tOtherCertification_main_OtherCertification_PhysicianID_Expand.jsp");
    pageControllerHash.remove("iOtherCertID");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltOtherCertification_List        bltOtherCertification_List        =    new    bltOtherCertification_List(iPhysicianID);

//declaration of Enumeration
    bltOtherCertification        OtherCertification;
    ListElement         leCurrentElement;
    Enumeration eList = bltOtherCertification_List.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tOtherCertification"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        OtherCertification  = (bltOtherCertification) leCurrentElement.getObject();
      }
      else
      {
        OtherCertification  = new bltOtherCertification();
        OtherCertification.setPhysicianID(iPhysicianID);
        isNewRecord= true;
      }
        OtherCertification.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !OtherCertification.getUniqueCreateDate().equals(testObj)  )
    {
         OtherCertification.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !OtherCertification.getUniqueModifyDate().equals(testObj)  )
    {
         OtherCertification.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !OtherCertification.getUniqueModifyComments().equals(testObj)  )
    {
         OtherCertification.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PhysicianID_"+iExpress)) ;
    if ( !OtherCertification.getPhysicianID().equals(testObj)  )
    {
         OtherCertification.setPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification PhysicianID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CertificationType_"+iExpress)) ;
    if ( !OtherCertification.getCertificationType().equals(testObj)  )
    {
         OtherCertification.setCertificationType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification CertificationType not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CertificationNumber_"+iExpress)) ;
    if ( !OtherCertification.getCertificationNumber().equals(testObj)  )
    {
         OtherCertification.setCertificationNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification CertificationNumber not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ExpirationDate_"+iExpress))) ;
    if ( !OtherCertification.getExpirationDate().equals(testObj)  )
    {
         OtherCertification.setExpirationDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification ExpirationDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Name_"+iExpress)) ;
    if ( !OtherCertification.getName().equals(testObj)  )
    {
         OtherCertification.setName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Name not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address1_"+iExpress)) ;
    if ( !OtherCertification.getAddress1().equals(testObj)  )
    {
         OtherCertification.setAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Address1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Address2_"+iExpress)) ;
    if ( !OtherCertification.getAddress2().equals(testObj)  )
    {
         OtherCertification.setAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Address2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("City_"+iExpress)) ;
    if ( !OtherCertification.getCity().equals(testObj)  )
    {
         OtherCertification.setCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification City not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("StateID_"+iExpress)) ;
    if ( !OtherCertification.getStateID().equals(testObj)  )
    {
         OtherCertification.setStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification StateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Province_"+iExpress)) ;
    if ( !OtherCertification.getProvince().equals(testObj)  )
    {
         OtherCertification.setProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Province not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ZIP_"+iExpress)) ;
    if ( !OtherCertification.getZIP().equals(testObj)  )
    {
         OtherCertification.setZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification ZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CountryID_"+iExpress)) ;
    if ( !OtherCertification.getCountryID().equals(testObj)  )
    {
         OtherCertification.setCountryID( testObj,UserSecurityGroupID   );
         //testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification CountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Phone_"+iExpress)) ;
    if ( !OtherCertification.getPhone().equals(testObj)  )
    {
         OtherCertification.setPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Phone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Fax_"+iExpress)) ;
    if ( !OtherCertification.getFax().equals(testObj)  )
    {
         OtherCertification.setFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Fax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName_"+iExpress)) ;
    if ( !OtherCertification.getContactName().equals(testObj)  )
    {
         OtherCertification.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail_"+iExpress)) ;
    if ( !OtherCertification.getContactEmail().equals(testObj)  )
    {
         OtherCertification.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification ContactEmail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DocuLinkID_"+iExpress)) ;
    if ( !OtherCertification.getDocuLinkID().equals(testObj)  )
    {
         OtherCertification.setDocuLinkID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification DocuLinkID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !OtherCertification.getComments().equals(testObj)  )
    {
         OtherCertification.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("OtherCertification Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tOtherCertification","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	OtherCertification.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    OtherCertification.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
	        OtherCertification.commitData();
	        if (!OtherCertification.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }	//Doculink Addition
	//Doculink Does Exist, prompt to Modify

     if (!errorRouteMe&&OtherCertification.getDocuLinkID().intValue()>0)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement(OtherCertification.getDocuLinkID());
            if (myDoc.getDocumentFileName().equalsIgnoreCase("")) 
            {
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tOtherCertification";
                myDUtils.setDocumentType(myDType, OtherCertification.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
            }
            else
            {
                //archive then create new
                myDoc.setArchived(new Integer("1"));
                myDoc.commitData();
                myDoc = new bltDocumentManagement();
                DocumentManagerUtils myDUtils = new DocumentManagerUtils();
                String myDType = "tOtherCertification";
                myDUtils.setDocumentType(myDType, OtherCertification.getUniqueID());
                myDoc.setDocumentName(myDUtils.getDocumentName());
                myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
                myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
                myDoc.setArchived(new Integer(2));
                myDoc.commitData();
                OtherCertification.setDocuLinkID(myDoc.getUniqueID());
                OtherCertification.commitData();
            }
     }
     else if (!errorRouteMe)
     {
            bltDocumentManagement myDoc = new bltDocumentManagement();
            DocumentManagerUtils myDUtils = new DocumentManagerUtils();
            String myDType = "tOtherCertification";
            myDUtils.setDocumentType(myDType, OtherCertification.getUniqueID());
            myDoc.setDocumentName(myDUtils.getDocumentName());
            myDoc.setDateOfExpiration(myDUtils.getDateOfExpiration());
            myDoc.setDocumentTypeID(myDUtils.getDocumentTypeID());
                myDoc.setPhysicianID(iPhysicianID);
            myDoc.setArchived(new Integer(2));
            myDoc.commitData();
            OtherCertification.setDocuLinkID(myDoc.getUniqueID());
            OtherCertification.commitData();
     }

    }
}       }
    }//while

if (errorRouteMeTotal)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else 
{
String nextPage = request.getParameter("nextPage");
if (nextPage==null||nextPage.equalsIgnoreCase(""))
{
	nextPage = ConfigurationMessages.getExpressLinkRaw("tOtherCertification","next",iExpressMode);
}
if (!isAllComplete)
{
	%>
	<script language=Javascript>
	      if ( confirm("<%=ConfigurationMessages.getMessage("ConfirmReqFieldsReturn")%>") )
	      {
	          document.location="<%=ConfigurationMessages.getDataCategoryLink("tOtherCertification","Express")%>";
	      }
	      else 
	      {
	          document.location="<%=nextPage%>";
	      }
	</script>
  <%
}
else
{
	%>
	<script language=Javascript>
	      document.location="<%=nextPage%>";
	</script>
  <%
}
}
       
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_PhysicianID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
