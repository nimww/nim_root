<%
String pageTitle = "FAQs";
%>
<%@ include file="/header.jsp" %>

<div class="container">
  <div class="row">
    <div  class="span8"  style="margin-bottom:6%">
    
    <img src="../img/MedicalCorner.jpg">
      <h1 id="top">FAQs</h1>
	
	 
						<div class="accordion" id="q1">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q1" href="#collapsecomtrack1">
								How do I utilize your services?
							  </a>
							</div>
							<div id="collapsecomtrack1" class="accordion-body collapse out">
							  <div class="accordion-inner" id="divNote">
									To schedule an appointment please reserve your procedure and pay online using our easy search tool. A representative from the imaging center you selected will call you within 2 business days to set up your appointment. You need a prescription from your referring physician to schedule so please send your RX to the imaging center prior to your appointment. You will obtain the full facility details upon completion of the reservation and payment process. The final report will be sent to your referring physician within 48 hours of your test. If you would like to speak with one of our representatives for more information, please call 888-693-8867.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q2">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q2" href="#collapsecomtrack2">
								What information do you need from me to schedule an appointment?
							  </a>
							</div>
							<div id="collapsecomtrack2" class="accordion-body collapse out">
							  <div class="accordion-inner" id="divNote">
									We will need demographic information like name, address, and phone numbers, as well as your Doctor's information. You must also have your prescription from your doctor detailing the requested test.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q3">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q3" href="#collapsecomtrack3">
								How do I pay for your services?
							  </a>
							</div>
							<div id="collapsecomtrack3" class="accordion-body collapse out">
							  <div class="accordion-inner" id="divNote">
									We accept all major credit and debit cards online or over the phone. Approval occurs immediately and you are then ready to be scheduled for your appointment.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q4">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q4" href="#collapsecomtrack4">
								What is contrast?
							  </a>
							</div>
							<div id="collapsecomtrack4" class="accordion-body collapse out">
							  <div class="accordion-inner" id="divNote">
									In some cases, a dye called contrast material may be used. It will either be put in a vein through an IV or you will be asked to drink the dye. The contrast agent shows up white on the images. This makes your organs, blood vessels, and tissues more visible which help the radiologist interpret these imaging studies. please consult your doctor on wether or not contrast is required for your test.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q5">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q5" href="#collapsecomtrack5">
								How do I get a copy of the report to my doctor?
							  </a>
							</div>
							<div id="collapsecomtrack5" class="accordion-body collapse out">
							  <div class="accordion-inner" id="divNote">
									Within 48 hours of the test, the final report will be sent to your doctor. If you need a copy, just let us know and we will mail or fax you a copy also.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q6">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q6" href="#collapsecomtrack6">
								What if my doctor wants to see the medical images of the test?
							  </a>
							</div>
							<div id="collapsecomtrack6" class="accordion-body collapse out">
							  <div class="accordion-inner" id="divNote">
									When you have your test done, just remember to ask the facility for a copy of the test on a CD. We can also assist you with this request and provide your doctor access to the image online.
							  </div>
							</div>
						  </div>					  
						</div>						
						<div class="accordion" id="q7">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q7" href="#collapsecomtrack7">
								Where can I find your price list?
							  </a>
							</div>
							<div id="collapsecomtrack7" class="accordion-body collapse out">
							  <div class="accordion-inner" id="divNote">
									For prices and locations near you, simply enter your zip code and procedure details into our Facility Search box. We offer up to 75% savings on MRIs, CTs and Ultrasounds.<br /><br />
									<i class="icon-info-sign"></i>As an example, in California, a patient without insurance can expect to pay $1,200 to $3,000 for an MRI. Through our company, the same service is offered at up to a 75% savings! Our services are available most anywhere in the country.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q8">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q8" href="#collapsecomtrack8">
								What makes NextImage Direct the best option for these diagnostic tests?
							  </a>
							</div>
							<div id="collapsecomtrack8" class="accordion-body collapse out">
							  <div class="accordion-inner" id="divNote">
									<ul>
										<li>High Quality services with up to 75% savings</li>
										<li>Access to the premier, fully accredited imaging centers nationwide</li>
										<li>Quality diagnosis by qualified and credentialed physicians</li>
										<li>Same day or next day scheduling</li>        
									</ul>
							  </div>
							</div>
						  </div>					  
						</div>      
	  <p><a href="#top" class="pull-right">TOPs</a></p>
	   
    </div>
    
	   	  
	   	   
	
    	
    <jsp:include page="../sidebar.jsp">
    	<jsp:param value="zipSearch" name="zip" />
    	<jsp:param value="../img/MRI.png" name="picture" />
    	<jsp:param value="testimonials" name="head" />     	
    	<jsp:param value="t1" name="t"/>
	</jsp:include>
   
  </div>
  
</div>
<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>

<jsp:include page="../footer.jsp"></jsp:include>
