<%@page language="java" import="com.winstaff.*,com.winstaff.login.*" %>
<% 
String page_title ="Partner Portal";
 %>
<%@include file="auth.jsp" %>
<%@include file="header.jsp" %>
<%

String thisYear = request.getParameter("year");

try{
	if(new Integer(thisYear)>2000 && new Integer(thisYear)<2040){
		
	} else if(thisYear==null){
		thisYear = new java.text.SimpleDateFormat("yyyy").format(new java.util.Date());
	} else {
		thisYear = new java.text.SimpleDateFormat("yyyy").format(new java.util.Date());
	}
} catch (Exception e){
	thisYear = new java.text.SimpleDateFormat("yyyy").format(new java.util.Date());
}

java.util.ArrayList<String> months = new java.util.ArrayList<String>();
months.add("Jan");
months.add("Feb");
months.add("Mar");
months.add("Apr");
months.add("May");
months.add("Jun");
months.add("Jul");
months.add("Aug");
months.add("Sep");
months.add("Oct");
months.add("Nov");
months.add("Dec");
java.util.ArrayList<Integer> web_links = new java.util.ArrayList<Integer>();
java.util.ArrayList<Integer> completes = new java.util.ArrayList<Integer>();
//initialize
for (int i = 0; i<12; i++){
    web_links.add(0);
    completes.add(0);
}

boolean limitMe = false;
String limitID = "";
if (request.getParameter("limit")!=null && isNIDAdmin){
	limitMe = true;
	limitID = request.getParameter("limit");
}

Boolean show_all = false;
if (request.getParameter("show_all")!=null){
	show_all = true;
}


if (request.getParameter("out")!=null){
	session.removeValue("nid_partner");
}

String selectedAccountDisplay = "";
if ( lo_partner!=null){
	selectedAccountDisplay = lo_partner.getPayerMaster().getPayerName();
}

if (limitMe)
{
	selectedAccountDisplay = new bltNIM3_PayerMaster(new Integer(limitID)).getPayerName();
}

if (show_all)
{
	selectedAccountDisplay = "All Partners**";
}


String username = request.getParameter("username");
String password = request.getParameter("password");
Integer UID = null;
if (username !=null && !username.equalsIgnoreCase("") && password!=null && !password.equalsIgnoreCase("") ){
	UID = com.winstaff.login.LoginUtils.checkLogin(username, password, "partnerid");
	if (UID!=null){
		session.putValue("nid_partner",UID);
		response.sendRedirect("index.jsp");

	%>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="alert alert-success">
						  <button type="button" class="close" data-dismiss="alert">&times;</button>
						  <strong class="lead">Success!</strong> Thank you for signing in!
						</div>	
					</div>	
				</div>	
			</div>	
	<%
	} else {
	%>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="alert alert-error">
						  <button type="button" class="close" data-dismiss="alert">&times;</button>
						  <strong class="lead">Warning!</strong> Invalid username and/or password.  Please try again!
						</div>	
					</div>	
				</div>	
			</div>	
	<%
	}
	
}
%>


<div class="container-fluid">
				<% if (!isAuthenticated) {
				%>
				<div class="row-fluid">
					<div class="span12">
						<div class="well">
							<h2>Secure Login Required</h2>
						</div>
						<form class="form-inline" method="post">
						  <input type="text" class="input-medium" name="username" placeholder="Username">
						  <input type="password" class="input-medium" name="password" placeholder="Password">
						  <button type="submit" class="btn">Sign in</button>
						</form>
					</div>
				</div>
				<%
				} else {
				%>
				<div class="row-fluid">
					<div class="span12">
						<div class="well">
							<% 
							if (isNIDAdmin&&show_all){
							%>
							<a href="index.jsp" class="btn btn-inverse pull-right"><i class="icon-star icon-white"></i>&nbsp;Showing All</a>
							<%
							} else if (limitMe) {
							%>
							<a href="index.jsp?show_all=true" class="btn btn-inverse pull-right"><i class="icon-star-empty icon-white"></i>&nbsp;Showing <strong><%=selectedAccountDisplay%></strong> [<%=limitID%>] Only</a>
							<%
							} else if (isNIDAdmin) {
							%>
							<a href="index.jsp?show_all=true" class="btn btn-inverse pull-right"><i class="icon-star-empty icon-white"></i>&nbsp;Showing NID-123 Only</a>
							<%
							}
							%>
							<h2>Stats for Account: <%=selectedAccountDisplay%></h2>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<div class="well well-small">
							<h4>30-Day Total Web <em>Click</em> Activity</h4>
							 <div id="chart_div_daily" style="height: 300px;"></div>
						</div>
					</div>
					<div class="span6">
						<div class="well well-small">
							<h4>YTD <em>Clicks</em> vs. <em>Completes</em></h4>
							 <div id="yearly_total" style="height: 300px;"></div>
						</div>
					</div>
				</div>
				<hr>

				<div class="row-fluid">
					<div class="span12">
						<div class="well well-small">
							<h4>Web Link <em>Clicks</em> by Month <%=thisYear%></h4>
						</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Code</th>
										<th>Jan</th>
										<th>Feb</th>
										<th>Mar</th>
										<th>Apr</th>
										<th>May</th>
										<th>Jun</th>
										<th>Jul</th>
										<th>Aug</th>
										<th>Sep</th>
										<th>Oct</th>
										<th>Nov</th>
										<th>Dec</th>
									</tr>
								</thead>
								<tbody>
								<%
									{
										searchDB2 mySS = new searchDB2();
										try{
											db_PreparedStatementObject myDPSO = new db_PreparedStatementObject("SELECT	weblink,date_part('month', transactiondate) AS imonth,	COUNT(*)AS iCount FROM 	tnim3_weblinktracker WHERE date_part('year', transactiondate) = '"+thisYear+"' AND payerid =? GROUP BY imonth, weblink  order by weblink, imonth ");
											myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,lo_partner.getPayerMaster().getPayerID().toString()));
											if (isNIDAdmin&&show_all){
												myDPSO = new db_PreparedStatementObject("SELECT	weblink,date_part('month', transactiondate) AS imonth,	COUNT(*)AS iCount FROM 	tnim3_weblinktracker WHERE date_part('year', transactiondate) = '"+thisYear+"' AND payerid >0  GROUP BY imonth, weblink  order by weblink, imonth ");
											}

											if (isNIDAdmin&&limitMe){
												myDPSO = new db_PreparedStatementObject("SELECT	weblink,date_part('month', transactiondate) AS imonth,	COUNT(*)AS iCount FROM 	tnim3_weblinktracker WHERE date_part('year', transactiondate) = '"+thisYear+"' AND payerid =? GROUP BY imonth, weblink  order by weblink, imonth ");
												myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,limitID));
											}

											java.sql.ResultSet myRS = mySS.executePreparedStatement(myDPSO);
											String current_code = "";
											int current_month=0;
											int cnt=0;
											while (myRS!=null&&myRS.next())
											{
												current_month++;
												String temp_code = myRS.getString("weblink");
												int temp_month= myRS.getInt("iMonth");
												int  temp_count= myRS.getInt("icount");
												cnt++;
												if (cnt==1){
													current_code = temp_code;
													%>
													<tr>
														<td><%=temp_code%></td>
													<%
												}
												if (!current_code.equalsIgnoreCase(temp_code)){
													//close out the last code
													while (current_month<=12){
														current_month++;
														%>
														<td>0</td>
														<%
													}
													%>
													</tr>
													<tr>
														<td><%=temp_code%></td>
													<%
													current_code = temp_code;
													current_month= 1;
												}
												while (current_month<temp_month){
													current_month++;
													%>
													<td>0</td>
													<%
												}
												web_links.set(current_month-1, web_links.get(current_month-1)+temp_count);
												%>
												<td><%=temp_count%></td>
												<%
											}
											while (current_month<12){
												current_month++;
												%>
												<td>0</td>
												<%
											}
											%>
											</tr>
											<%
											mySS.closeAll();
										}
										catch(Exception ee)   {
												DebugLogger.e("LoginUtils:checkLogin","Error in login check ["+ee+"]");
										} finally {
											mySS.closeAll();
										}		  

									}
									
								%>
									<tr>
									<th>Totals:</th>
									<%
										for (int i=0; i<12; i++){
											%>
											<th><%=web_links.get(i)%></th>
											<%
										}
									%>
									</tr>
									
								
								</tbody>
							</table>
							
					</div>
				</div>		
				<hr>

				<div class="row-fluid">
					<div class="span12">
						<div class="well well-small">
							<h4>Web Link <em>Completes</em> by Month <%=thisYear%></h4>
						</div>
							<table class="table table-striped table-bordered table-hover ">
								<thead>
									<tr>
										<th>Code</th>
										<th>Jan</th>
										<th>Feb</th>
										<th>Mar</th>
										<th>Apr</th>
										<th>May</th>
										<th>Jun</th>
										<th>Jul</th>
										<th>Aug</th>
										<th>Sep</th>
										<th>Oct</th>
										<th>Nov</th>
										<th>Dec</th>
									</tr>
								</thead>
								<tbody>
								<%
									{
										searchDB2 mySS = new searchDB2();
										try{
											db_PreparedStatementObject myDPSO = new db_PreparedStatementObject("SELECT	tnim3_referral.referralweblink,date_part('month', tnim3_encounter.rec_bill_pro) AS imonth,	COUNT(*) AS iCount FROM 	tnim3_referral INNER JOIN tnim3_encounter on tnim3_encounter.referralid = tnim3_referral.referralid INNER JOIN tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid WHERE tnim3_caseaccount.payerid=? and date_part('year', tnim3_encounter.rec_bill_pro) = '"+thisYear+"' GROUP BY imonth, tnim3_referral.referralweblink  order by tnim3_referral.referralweblink, imonth ");
											myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,lo_partner.getPayerMaster().getPayerID().toString()));
											if (isNIDAdmin&&show_all){
												myDPSO = new db_PreparedStatementObject("SELECT	tnim3_referral.referralweblink,date_part('month', tnim3_encounter.rec_bill_pro) AS imonth,	COUNT(*) AS iCount FROM 	tnim3_referral INNER JOIN tnim3_encounter on tnim3_encounter.referralid = tnim3_referral.referralid INNER JOIN tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid INNER JOIN tnim3_payermaster on tnim3_payermaster.payerid =  tnim3_caseaccount.payerid WHERE tnim3_caseaccount.payerid>0 and tnim3_payermaster.isnid=1 and date_part('year', tnim3_encounter.rec_bill_pro) = '"+thisYear+"' GROUP BY imonth, tnim3_referral.referralweblink  order by tnim3_referral.referralweblink, imonth ");
											}
											
											if (isNIDAdmin&&limitMe){
												 myDPSO = new db_PreparedStatementObject("SELECT	tnim3_referral.referralweblink,date_part('month', tnim3_encounter.rec_bill_pro) AS imonth,	COUNT(*) AS iCount FROM 	tnim3_referral INNER JOIN tnim3_encounter on tnim3_encounter.referralid = tnim3_referral.referralid INNER JOIN tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid WHERE tnim3_caseaccount.payerid=? and date_part('year', tnim3_encounter.rec_bill_pro) = '"+thisYear+"' GROUP BY imonth, tnim3_referral.referralweblink  order by tnim3_referral.referralweblink, imonth ");
												myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,limitID));
											}
											

											
											java.sql.ResultSet myRS = mySS.executePreparedStatement(myDPSO);
											String current_code = "";
											int current_month=0;
											int cnt=0;
											while (myRS!=null&&myRS.next())
											{
												current_month++;
												String temp_code = myRS.getString("referralweblink");
												int temp_month= myRS.getInt("iMonth");
												int  temp_count= myRS.getInt("icount");
												cnt++;
												if (cnt==1){
													current_code = temp_code;
													%>
													<tr>
														<td><%=temp_code%></td>
													<%
												}
												if (!current_code.equalsIgnoreCase(temp_code)){
													//close out the last code
													while (current_month<=12){
														current_month++;
														%>
														<td>0</td>
														<%
													}
													%>
													</tr>
													<tr>
														<td><%=temp_code%></td>
													<%
													current_code = temp_code;
													current_month= 1;
												}
												while (current_month<temp_month){
													current_month++;
													%>
													<td>0</td>
													<%
												}
												completes.set(current_month-1, completes.get(current_month-1)+temp_count);
												%>
												<td><%=temp_count%></td>
												<%
											}
											while (current_month<12){
												current_month++;
												%>
												<td>0</td>
												<%
											}
											%>
											</tr>
											<%
											mySS.closeAll();
										}
										catch(Exception ee)   {
												DebugLogger.e("LoginUtils:checkLogin","Error in login check ["+ee+"]");
										} finally {
											mySS.closeAll();
										}		  

									}
								%>
									<tr>
									<th>Totals:</th>
									<%
										for (int i=0; i<12; i++){
											%>
											<th><%=completes.get(i)%></th>
											<%
										}
									%>
									</tr>
								
								</tbody>
							</table>
					</div>
				</div>		
				<hr>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="well well-small">
							<h4>Referral Code <em>Completes</em> by Month <%=thisYear%></h4>
						</div>
							<table class="table table-striped table-bordered table-hover ">
								<thead>
									<tr>
										<th>Code</th>
										<th>Jan</th>
										<th>Feb</th>
										<th>Mar</th>
										<th>Apr</th>
										<th>May</th>
										<th>Jun</th>
										<th>Jul</th>
										<th>Aug</th>
										<th>Sep</th>
										<th>Oct</th>
										<th>Nov</th>
										<th>Dec</th>
									</tr>
								</thead>
								<tbody>
								<%
									{
										searchDB2 mySS = new searchDB2();
										try{
											db_PreparedStatementObject myDPSO = new db_PreparedStatementObject("SELECT	tnim3_encounter.nidreferralcode,date_part('month', tnim3_encounter.rec_bill_pro) AS imonth,	COUNT(*) AS iCount FROM 	tnim3_referral INNER JOIN tnim3_encounter on tnim3_encounter.referralid = tnim3_referral.referralid INNER JOIN tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid WHERE tnim3_caseaccount.payerid=? and date_part('year', tnim3_encounter.rec_bill_pro) = '"+thisYear+"' GROUP BY imonth, tnim3_encounter.nidreferralcode  order by tnim3_encounter.nidreferralcode, imonth ");
											myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,lo_partner.getPayerMaster().getPayerID().toString()));
											if (isNIDAdmin&&show_all){
												myDPSO = new db_PreparedStatementObject("SELECT	tnim3_encounter.nidreferralcode,date_part('month', tnim3_encounter.rec_bill_pro) AS imonth,	COUNT(*) AS iCount FROM 	tnim3_referral INNER JOIN tnim3_encounter on tnim3_encounter.referralid = tnim3_referral.referralid INNER JOIN tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid INNER JOIN tnim3_payermaster on tnim3_payermaster.payerid = tnim3_caseaccount.payerid WHERE tnim3_caseaccount.payerid>0 and tnim3_payermaster.isnid=1 and date_part('year', tnim3_encounter.rec_bill_pro) = '"+thisYear+"' GROUP BY imonth, tnim3_encounter.nidreferralcode  order by tnim3_encounter.nidreferralcode, imonth ");
											}

											
											if (isNIDAdmin&&limitMe){
												myDPSO = new db_PreparedStatementObject("SELECT	tnim3_encounter.nidreferralcode,date_part('month', tnim3_encounter.rec_bill_pro) AS imonth,	COUNT(*) AS iCount FROM 	tnim3_referral INNER JOIN tnim3_encounter on tnim3_encounter.referralid = tnim3_referral.referralid INNER JOIN tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid WHERE tnim3_caseaccount.payerid=? and date_part('year', tnim3_encounter.rec_bill_pro) = '"+thisYear+"' GROUP BY imonth, tnim3_encounter.nidreferralcode  order by tnim3_encounter.nidreferralcode, imonth ");
												myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,limitID));
											}
											
											
											
											java.sql.ResultSet myRS = mySS.executePreparedStatement(myDPSO);
											String current_code = "";
											int current_month=0;
											int cnt=0;
											while (myRS!=null&&myRS.next())
											{
												current_month++;
												String temp_code = myRS.getString("nidreferralcode");
												int temp_month= myRS.getInt("iMonth");
												int  temp_count= myRS.getInt("icount");
												cnt++;
												if (cnt==1){
													current_code = temp_code;
													%>
													<tr>
														<td><%=temp_code%></td>
													<%
												}
												if (!current_code.equalsIgnoreCase(temp_code)){
													//close out the last code
													while (current_month<=12){
														current_month++;
														%>
														<td>0</td>
														<%
													}
													%>
													</tr>
													<tr>
														<td><%=temp_code%></td>
													<%
													current_code = temp_code;
													current_month= 1;
												}
												while (current_month<temp_month){
													current_month++;
													%>
													<td>0</td>
													<%
												}
												%>
												<td><%=temp_count%></td>
												<%
											}
											while (current_month<12){
												current_month++;
												%>
												<td>0</td>
												<%
											}
											%>
											</tr>
											<%
											mySS.closeAll();
										}
										catch(Exception ee)   {
												DebugLogger.e("LoginUtils:checkLogin","Error in login check ["+ee+"]");
										} finally {
											mySS.closeAll();
										}		  

									}
								%>
									<tr>
									<th>Totals:</th>
									<%
										for (int i=0; i<12; i++){
											%>
											<th><%=completes.get(i)%></th>
											<%
										}
									%>
									</tr>
								
								</tbody>
							</table>
					</div>
				</div>		
				<%
				}
				%>
</div>
	
	
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Day', 'Views'],
		  <%
		{
			searchDB2 mySS = new searchDB2();
			try{
				db_PreparedStatementObject myDPSO = new db_PreparedStatementObject("SELECT lpad(date_part('month', transactiondate)::text,2, '0') || '-' || lpad(date_part('day', transactiondate)::text,2, '0') as iday, count(*) as iCount FROM tnim3_weblinktracker where transactiondate + INTERVAL '30 days' > now() and payerid=? group by iday order by iday");
				myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,lo_partner.getPayerMaster().getPayerID().toString()));

				if (isNIDAdmin&&show_all){
					myDPSO = new db_PreparedStatementObject("SELECT lpad(date_part('month', transactiondate)::text,2, '0') || '-' || lpad(date_part('day', transactiondate)::text,2, '0') as iday, count(*) as iCount FROM tnim3_weblinktracker where transactiondate + INTERVAL '30 days' > now() group by iday order by iday");
				}

				
				if (isNIDAdmin&&limitMe){
					myDPSO = new db_PreparedStatementObject("SELECT lpad(date_part('month', transactiondate)::text,2, '0') || '-' || lpad(date_part('day', transactiondate)::text,2, '0') as iday, count(*) as iCount FROM tnim3_weblinktracker where transactiondate + INTERVAL '30 days' > now() and payerid=? group by iday order by iday");
					myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,limitID));
				}



				java.sql.ResultSet myRS = mySS.executePreparedStatement(myDPSO);
				while (myRS!=null&&myRS.next())
				{
					%>
					['<%=myRS.getString("iday")%>', <%=myRS.getInt("icount")%>],
					<%
				}
				mySS.closeAll();
			}
			catch(Exception ee)   {
					DebugLogger.e("LoginUtils:checkLogin","Error in login check ["+ee+"]");
			} finally {
				mySS.closeAll();
			}		  

		}
		  %>
        ]);

        var options = {
          hAxis: {title: 'Day', titleTextStyle: {color: '#8A8'}},
		  colors:['#0099CC'],
		  legend: {position: 'none', textStyle: {color: 'blue', fontSize: 16}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_daily'));
        chart.draw(data, options);
      }
</script>	
	
	<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Month', 'Clicks', 'Completes'],
		  <%
		{
			for (int i=0; i<12; i++){
				%>
				['<%=months.get(i)%>', <%=web_links.get(i)%>, <%=completes.get(i)%>],
				<%
			}
		}
		  %>
        ]);

        var options = {
          hAxis: {title: 'Day', titleTextStyle: {color: '#8A8'}},
		  isStacked:true,
		  colors:['#0099CC','#00CC88'],
		  legend: {position: 'below', textStyle: {color: 'blue', fontSize: 12}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('yearly_total'));
        chart.draw(data, options);
      }
</script>	


	
<jsp:include page="../footer.jsp"></jsp:include>




