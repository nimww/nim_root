<%@page language="java" import="com.winstaff.*,com.winstaff.login.*" %>
<% 
String page_title ="Partner Portal";
 %>
 <%@include file="auth.jsp" %>

<%@include file="header.jsp"%>
<%

java.util.ArrayList<String> months = new java.util.ArrayList<String>();
months.add("Jan");
months.add("Feb");
months.add("Mar");
months.add("Apr");
months.add("May");
months.add("Jun");
months.add("Jul");
months.add("Aug");
months.add("Sep");
months.add("Oct");
months.add("Nov");
months.add("Dec");
java.util.ArrayList<Integer> web_links = new java.util.ArrayList<Integer>();
java.util.ArrayList<Integer> completes = new java.util.ArrayList<Integer>();
//initialize
for (int i = 0; i<12; i++){
    web_links.add(0);
    completes.add(0);
}


if (request.getParameter("out")!=null){
	session.removeValue("nid_partner");
}



%>
<%
Boolean show_all = false;
if (request.getParameter("show_all")!=null){
	show_all = true;
}
%>


<div class="container-fluid">
				<% if (!isAuthenticated) {
				response.sendRedirect("index.jsp");
				} else {
				%>
				<div class="row-fluid">
					<div class="span12">
						<div class="well well-small">
							<h2>NID Admin Tools</h2>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<div class="well well-small">
							<h4>Login Assist</h4>
						</div>
						<table class="table table-hover table-striped table-condensed">
						<thead>
							<tr>
								<th>Partner Name</th>
								<th>Weblink Code</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td ><strong>Clear Code</strong></td>
								<td>&nbsp;</td>
								<td>
									<a href="clear.jsp" target="_blank" class="btn btn btn-danger"><i class="icon-refresh icon-white"></i> Clear & Launch</a>												
								</td>
							</tr>
						
						<%
								{
									searchDB2 mySS = new searchDB2();
									try{
										db_PreparedStatementObject myDPSO = new db_PreparedStatementObject("SELECT * from tNIM3_PayerMaster where isNID=1 Order By Case When payerid = 1568 Then 1 end, payername");
//											myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,lo_partner.getPayerMaster().getPayerID().toString()));

										java.sql.ResultSet myRS = mySS.executePreparedStatement(myDPSO);
										while (myRS!=null&&myRS.next())
										{
										
											%>
											<tr>
												<td><%=myRS.getString("payername")%></td>
												<td><%=myRS.getString("nid_webcode")%></td>
												<td>
												<% 
													if (myRS.getString("nid_webcode").equalsIgnoreCase("")){
													%>
														<a href="clear.jsp" target="_blank" class="btn btn-small"><i class="icon-refresh icon-white"></i> Launch</a>													
												   <%
													} else {
													%>
														<a href="/partners/<%=myRS.getString("nid_webcode")%>" target="_blank" class="btn btn-primary btn-small"><i class="icon-share-alt icon-white"></i> Launch</a>
													<%
													}
													%>
														&nbsp;&nbsp;<a href="index.jsp?limit=<%=myRS.getString("payerid")%>" class="btn btn-info btn-small"><i class="icon-th icon-white"></i> Stats</a>
												</td>
											</tr>
											<%
										}
									} catch (Exception e) {
									}
								}
						%>
						</tbody>
						</table>
					</div>
					<div class="span6">
						<div class="well well-small">
							<h4>&nbsp;</h4>
						</div>
					</div>
				</div>
				<%
				}
				%>
</div>


	
<jsp:include page="../footer.jsp"></jsp:include>




