<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<META NAME="title" content="NextImage Direct, Cost Saving MRI, Cost Saving CT, Cost Saving Ultrasound, Underinsured, High deductible, Self pay ,MRI, Cheap Magnetic Resonance Imaging, Cheap, Low Cost MRIs, Affordable MRIs,Discount MRIs, Magnetic Resonance Imaging prices, MRI Prices,MRI Costs, Affordable MRI">
<META NAME="description" content="NextImage Direct, Cost Saving MRI, CT, Ultrasound for Underinsured, High deductible, or Self pay">
<META NAME="keywords" content="MRI, MRIs, low cost MRIs,discount MRI,MRI prices, magnetic resonance imaging,discount magnetic resonance imaging, affordable MRI, MRI, CT, Cat Scan, US, Ultrasound, Underinsured, High deductible, Self pay">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title><%= page_title %> | Cost Saving MRI, CT, Ultrasound for Underinsured, High deductible, or Self pay</title>

<link href="//netdna.bootstrapcdn.com/bootswatch/2.3.1/flatly/bootstrap.min.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-responsive.min.css" rel="stylesheet">
<%
//    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
%>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<link href="/css/style.css" rel="stylesheet" media="screen">
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37595845-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<img src="/img/logo.png"><br><br>
		</div>
	</div>
  <div class="navbar  ">
    <div class="navbar-inner">
	  <a class="brand" href="/partner/">
     		Partner Portal (<em>beta</em>)
	  </a>
		<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
      <div class="container">
        <div class="nav-collapse collapse" id="main-menu">
          <ul class="nav" id="main-menu-left">
            <li><a href="index.jsp"><i class="icon-th icon-white"></i>&nbsp;Stats</a></li>
			<% if (isNIDAdmin) {
			%>
            <li><a href="admin.jsp"><i class="icon-play icon-white"></i>&nbsp;Admin</a></li>
			<%
			}
			%>
          </ul>
          <ul class="nav pull-right" id="main-menu-left">
            <li class="pull-right"><a href="index.jsp?out=true"><i class="icon-off icon-white"></i>&nbsp;Sign Out</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
