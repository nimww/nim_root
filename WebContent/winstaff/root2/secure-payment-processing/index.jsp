<%/**
* Secure payment processing
*/
String pageTitle = "Secure Payment Processing";
%>
<%@ include file="/header.jsp" %>
<%
	String custom = "";
	if (rCodeO!=null && (custom==null ||  custom.equalsIgnoreCase("")) ){
		custom = rCodeO.toString();
	}
	
%>
<%
		searchDB2 conn = null;
		String query = null;
		java.sql.ResultSet rs = null;
		PracticeMaster_SearchResultsObject pm_sro = null;
		String myCPTwizard = null;
		String myCPT = null;
		String bp = null;

		int myICCode = 0;
		
		String myCounty = null;
		CPTGroupObject2 myCPTO2_full = null;
		Double dPrice = null;

		if (request.getParameter("ic_code") != null
				&& request.getParameter("myCPTwizard") != null) {

			myCPTwizard = request.getParameter("myCPTwizard");
			
			conn = new searchDB2();
			query = "select cpt1, bp1 from tcptwizard where cptwizardid = "
					+ myCPTwizard;
			rs = conn.executeStatement(query);
			//out.print(query);
			while (rs.next()) {
				myCPT = rs.getString("cpt1");
				bp = rs.getString("bp1");
			}
			conn.closeAll();
			
			myICCode = new Integer(request.getParameter("ic_code"));
			myCounty = new bltPracticeMaster(myICCode).getOfficeCity();
			
			pm_sro = new PracticeMaster_SearchResultsObject(new Integer(myICCode));
			
			Double theCost = NIMUtils.getCPTGroupPayPriceforPracticeMaster(pm_sro.practiceMaster,NIMUtils.CPTWizardToCPTGroupObject(new bltCPTWizard(new Integer(myCPTwizard))), PLCUtils.getToday(), NIMUtils.PAYMENT_TYPE_GROUPHEALTH);

			
			NIM3_ReferralSourceObject myRSO = NID_SubmitCaseObject.translateWebCodesToReferralSource(request.getParameter("referralSource"),request.getParameter("referralCode"), custom);
			pm_sro.setPrice(theCost);       
			
			payerMaster = new bltNIM3_PayerMaster(myRSO.getPayerID());
            bltNIM3_NIDPromo nidPromo = null;
            if (myRSO.getNidPromo()!=null){
                nidPromo = new bltNIM3_NIDPromo(myRSO.getNidPromo());
            }
			pm_sro.setCustomerPrice(NIDUtils.getNIDFinalPrice(theCost, pm_sro.getPracticeMaster(), payerMaster, nidPromo ));
            //out.print("pm_sro: "+pm_sro.getCustomerPrice());
			
			myCPTO2_full = new CPTGroupObject2(myCPT, "");
			dPrice = NIMUtils.getCPTGroupPayPriceforPracticeMaster(
					new bltPracticeMaster(myICCode), myCPTO2_full,
					PLCUtils.getToday());
}					
%>





<%
	Boolean isGoodSubmit = false;
	NID_SubmitCaseObject NID_sco = new NID_SubmitCaseObject();

	String customWebLink = "";
	if (rCodeO!=null){
		customWebLink = rCodeO.toString();
	}
	
	
	try{


		NID_sco.setRemoteAddr(request.getRemoteAddr());
		NID_sco.setWebLinkCode(customWebLink);
		NID_sco.setRemoteHost(request.getRemoteHost());
		//Need to work on this for referral codes once we get implementation forms
		NID_sco.setPayerID(123);
		NID_sco.setPatientFirstName(request.getParameter("iwFirstName"));
		NID_sco.setPatientLastName(request.getParameter("iwLastName"));
		NID_sco.setPatientAddress1(request.getParameter("iwAddress"));
		NID_sco.setPatientCity(request.getParameter("iwCity"));
		NID_sco.setPatientZIP(request.getParameter("iwZip"));
		try{
			NID_sco.setPatientStateID(new Integer(request.getParameter("iwState")));
		}
		catch(Exception e){}
		NID_sco.setPatientHomePhone(request.getParameter("iwPhone1")+request.getParameter("iwPhone2")+request.getParameter("iwPhone3"));
		NID_sco.setPatientEmail(request.getParameter("iwEmail"));
		NID_sco.setPatientHeight(request.getParameter("iwHeight"));
		NID_sco.setPatientGender(request.getParameter("iwGender"));
		try{
			NID_sco.setPatientDOB(new SimpleDateFormat("MM/dd/yyyy").parse(request.getParameter("iwDOB")));
		}
		catch(Exception e){
		}
		NID_sco.setSelectedPracticeID(new Integer(request.getParameter("pid")));
		NID_sco.setWizardID(new Integer(request.getParameter("wizardid")));
		try{
			NID_sco.setPatientWeight(new Integer(request.getParameter("iwWeight")));
		}
		catch(Exception e){
		}
		try{
			NID_sco.setClaustrophobic(new Integer(request.getParameter("isClaustrophobic")));
		}
		catch(Exception e){
					NID_sco.setClaustrophobic(0);
		}

		try{
			NID_sco.setPregnant(new Integer(request.getParameter("isPregnant")));
		}
		catch(Exception e){
			NID_sco.setPregnant(0);
		}

		try{
			NID_sco.setMetal(new Integer(request.getParameter("isMetal")));
		}
		catch(Exception e){}

		try{
			NID_sco.setHasConditions(new Integer(request.getParameter("isHasConditions")));
		}
		catch(Exception e){}
		
		try{
			NID_sco.setPatientHasMetalInBodyDesc(request.getParameter("metalDesc"));
		}
		catch(Exception e){}
		
		//NID_sco.setPatientHasKidneyLiverHypertensionDiabeticConditionsDesc(request.getParameter("conditionsDesc"));

		//NID_sco.setDiagnosis(request.getParameter("diagnosis"));
		NID_sco.setBodyPartDisplay(request.getParameter("bodyPart"));
		NID_sco.setReferringDoctorFirstName(request.getParameter("mdFirstName"));
		NID_sco.setReferringDoctorLastName(request.getParameter("mdLastName"));
		NID_sco.setReferringDoctorPhone(request.getParameter("mdPhone1")+request.getParameter("mdPhone2")+request.getParameter("mdPhone3"));
		NID_sco.setReferringDoctorFax(request.getParameter("mdFax1")+request.getParameter("mdFax2")+request.getParameter("mdFax3"));
//		NID_sco.setPatientAvailability1(request.getParameter("time1"));
//		NID_sco.setPatientAvailability2(request.getParameter("time2"));
//			NID_sco.setPatientAvailability3(request.getParameter("time3"));
		
		try{
			NID_sco.setReferralSource(request.getParameter("referralSource"));
		}
		catch(Exception e){NID_sco.setReferralSource("");}
		
		
		try{
			NID_sco.setReferralCode(request.getParameter("referralCode"));
		}
		catch(Exception e){NID_sco.setReferralCode("");}
		
		
		
	isGoodSubmit = NID_sco.submitCase();
	//isGoodSubmit = true;
	} catch (Exception e){

	}
	if (!isGoodSubmit){
	%>
	<div class="container">
		<div class="alert alert-danger"><h1>You have encountered and error in reserving your center.</h1> <h2>  Please click back and ensure that all information is correctly filled out or call us at 888-693-8867 so that we can assist you in your order. </h2></div>
	</div>
	<%
	}
	else {

%>
<script>

	function submitCard(){
		$("#card-form").hide();
		$("#cc_loading").show();
		var c_name = $("#c_name").val();
		var c_address = $("#c_address").val();
		var c_city = $("#c_city").val();
		var c_state = $("#c_state").val();
		var c_zip = $("#c_zip").val();
		var c_cardnumber = $("#c_cardnumber").val();
		var c_exp = $("#c_exp_month").val()+$("#c_exp_year").val();
		var c_email = "Accounting@nextimagemedical.com";
		var ic_code = "<%=request.getParameter("pid")%>";
		var wiz_id = "<%=request.getParameter("wizardid")%>";
		var cID = "<%=NID_sco.getSubmittedCaseID()%>";
		var rID = "<%=NID_sco.getSubmittedReferralID()%>";
		var eID = "<%=NID_sco.getSubmittedEncounterID()%>"; 
		var dataString = "c_name="+c_name+"&c_address="+c_address+"&c_city="+c_city+"&c_state="+c_state+"&c_zip="+c_zip+"&c_cardnumber="+c_cardnumber+"&c_exp="+c_exp+"&c_email="+c_email+"&wiz_id="+wiz_id+"&cID="+cID+"&eID="+eID+"&rID="+rID+"&ic_code="+ic_code;
		$.ajax({
			type: "GET",
			url: "/sage_process.jsp",
			data: dataString,
			success: function() {
				$("#cc_loading").hide();	
				$("#cc_response").show();	
			}
		}).done(function(html){
			$("#cc_response").html(html);
		});	
	}

	$(document).ready(function(){
	
		$('#form-submit').click(function(){
			
						if ($('#tos').is(':checked')) {
							submitCard();
						} else {
							alert("You must agree to the Terms of Service before submitting payment.");
						}
				
			
		});
		
	});
	
	function test(){
		alert($('#c_name').val());
	}
	function clearForm(){
		$('#c_name').val('');
		$('#c_address').val('');
		$('#c_city').val('');
		$('#c_state').val('');
		$('#c_zip').val('');
		$('#c_cardnumber').val('');
	}
	
	function startOver(){
		clearForm();
		$("#card-form").show();
		$("#cc_loading").hide();
		$("#cc_response").hide();
	}
</script>
<div class="container">
 <div  id="card-form">
 
	<div class="row">
		
		<h3 class="span12" >Appointment Details</h3>
	</div>
	<div class="row" >
		<div class="span4">			
			<div class="backgroundSecure">
					<h5>Requested Facility: </h5>
					<h4><%=myCounty%>- <%=myICCode%></h4>
					</div>
			</div>
		<div class="span4" >			
			<div class="reserveFacilities" style="min-height:125px" >
					<h4><%=payerMaster.getCustomerDisplayName()%><br>discount price: </h4>
					<h3>$<%=Math.round(pm_sro.getCustomerPrice())%></h3>
			</div>
		</div>
		<div class="span4" >			
			<div class="backgroundSecure"">
					<h5>Procedure </h5>
					 <h4><%=bp%></h4>
					 <h5>CPT Code: <%=myCPT%></h5>
			</div>
		</div>
	</div>
 	
 	
 	
 	
 	
 	
 	
 	
 	
 	 <div class="row-fluid" style="margin-top:5px;">
		<div class="div">
			<div class="" style="background-color:#333333;color:white;">
				<h4 class="">Credit Card Payment</h4>															
			</div>						
		</div>
		<div class="row-fluid img-polaroid" style="width:1160px;margin-top:-10px" >
			<div class="" >
				<div class=""><label>Name on Card</label><input type="text" name="c_name" id="c_name" placeholder="Card holder name" value="<%=request.getParameter("iwFirstName")%> <%=request.getParameter("iwLastName")%>"></div>
			</div>
	<div class="" >
		<div class="span6"><label>Card Number</label><input type="text" name="c_cardnumber" value="" id="c_cardnumber" class="ptForm" placeholder="No dashes please"></div>
		<div class="span6">
		<label>Expiration</label>
		<select name="c_exp_month" id="c_exp_month" class="select-style" style="width:145px">
			<option value = "01">January</option>
			<option value = "02">February</option>
			<option value = "03">March</option>
			<option value = "04">April</option>
			<option value = "05">May</option>
			<option value = "06">June</option>
			<option value = "07">July</option>
			<option value = "08">August</option>
			<option value = "09">September</option>
			<option value = "10">October</option>
			<option value = "11">November</option>
			<option value = "12">December</option>        
		</select>
		<select name="c_exp_year" id="c_exp_year" class="select-style" style="width:70px">
			<%Calendar year = Calendar.getInstance();
			for (int c = 0; c<20; c++ ){%>
				<option value="<%=year.get(Calendar.YEAR)-2000%>"><%=year.get(Calendar.YEAR)%></option>
				<%year.add(Calendar.YEAR,1);
			}%>      
		</select>
		</div>
	</div>
	<div class="" >
		<div class="">
			<label>Billing Address</label><input type="text" name="c_address" id="c_address" tabindex="8" class="input-block-level" placeholder="Address" value="<%=request.getParameter("iwAddress")%>">
		</div>
		<div class="">
			<label>City State & Zip</label> <input type="text" name="c_city" id="c_city" class="ptForm" tabindex="9" placeholder="City" value="<%=request.getParameter("iwCity")%>"> 
			<select	name="c_state" id="c_state" class="select-style" style="width: 90px; margin-bottom: 10px" tabindex="10">
				<option value="">State</option>
				<% conn = new searchDB2();
				query = ("select * from tstateli where stateid not in (0,52) order by shortstate");
				rs = conn.executeStatement(query);
				while (rs != null && rs.next()) {%>
				<option <%=new Integer(request.getParameter("iwState"))==rs.getInt("stateid") ? "selected":""%> value="<%=rs.getString("shortstate")%>"><%=rs.getString("shortstate")%> </option>
				<%} conn.closeAll(); %>
			</select> 
			<input type="text" name="c_zip" id="c_zip" style="width: 119px; margin-bottom: 10px" tabindex="11" max=5 placeholder="ZIP" value="<%=request.getParameter("iwZip")%>">
		</div>
	</div>
		</div>
	</div>	
		
		
		
 
 
	<%-- <div class="row" >
		<div class="span12">
			<h3>Credit Card Payment</h3>
		</div>
	</div>
	<div class="row" >
		<div class="span6"><label>Name on Card</label><input type="text" name="c_name" id="c_name" placeholder="Card holder name" value="<%=request.getParameter("iwFirstName")%> <%=request.getParameter("iwLastName")%>"></div>
	</div>
	<div class="row" >
		<div class="span3"><label>Card Number</label><input type="text" name="c_cardnumber" value="" id="c_cardnumber" class="ptForm" placeholder="No dashes please"></div>
		<div class="span3">
		<label>Expiration</label>
		<select name="c_exp_month" id="c_exp_month" class="select-style" style="width:145px">
			<option value = "01">January</option>
			<option value = "02">February</option>
			<option value = "03">March</option>
			<option value = "04">April</option>
			<option value = "05">May</option>
			<option value = "06">June</option>
			<option value = "07">July</option>
			<option value = "08">August</option>
			<option value = "09">September</option>
			<option value = "10">October</option>
			<option value = "11">November</option>
			<option value = "12">December</option>        
		</select>
		<select name="c_exp_year" id="c_exp_year" class="select-style" style="width:70px">
			<%Calendar year = Calendar.getInstance();
			for (int c = 0; c<20; c++ ){%>
				<option value="<%=year.get(Calendar.YEAR)-2000%>"><%=year.get(Calendar.YEAR)%></option>
				<%year.add(Calendar.YEAR,1);
			}%>      
		</select>
		</div>
	</div>
	<div class="row" >
		<div class="span6">
			<label>Billing Address</label><input type="text" name="c_address" id="c_address" tabindex="8" class="input-block-level" placeholder="Address" value="<%=request.getParameter("iwAddress")%>">
		</div>
		<div class="span6">
			<label>City State & Zip</label> <input type="text" name="c_city" id="c_city" class="ptForm" tabindex="9" placeholder="City" value="<%=request.getParameter("iwCity")%>"> 
			<select	name="c_state" id="c_state" class="select-style" style="width: 90px; margin-bottom: 10px" tabindex="10">
				<option value="">State</option>
				<% conn = new searchDB2();
				query = ("select * from tstateli where stateid not in (0,52) order by shortstate");
				rs = conn.executeStatement(query);
				while (rs != null && rs.next()) {%>
				<option <%=new Integer(request.getParameter("iwState"))==rs.getInt("stateid") ? "selected":""%> value="<%=rs.getString("shortstate")%>"><%=rs.getString("shortstate")%> </option>
				<%} conn.closeAll(); %>
			</select> 
			<input type="text" name="c_zip" id="c_zip" style="width: 119px; margin-bottom: 10px" tabindex="11" max=5 placeholder="ZIP" value="<%=request.getParameter("iwZip")%>">
		</div>
	</div> --%>
	<div class="row" >
		<div class="span6">
        	<input type="checkbox" id="tos"> <span id="tosLabel">I agree to the <a href="/payment-terms-of-service/" target="_blank">payment terms of service.</a></span><br><br>
			<button class="btn btn-primary" id="form-submit">Submit</button>
			<button class="btn btn-primary" onclick="clearForm();">Clear Form</button>
		</div>
	</div>
 </div>
	
    <div class="row" id="cc_response" style="display:none;">
    	
    </div>
	<div class="row">
		<div class="loadResults" id="cc_loading" style="min-height:400px; display:none;">
		</div>
	</div>
 </div>

<%
}
%>

<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>

<jsp:include page="../footer.jsp"></jsp:include>
