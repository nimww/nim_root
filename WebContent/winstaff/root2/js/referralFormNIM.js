/*Created by Giovanni Hernandez 06-27-13*/

			function test(){				
				if($('#fileRx').val() !='' && $("#CaseClaimNumber1").val().length > 0 && $("#refFirstName").val().length > 0 && $("#refLastName").val().length > 0 && $("#AdjusterBranch").val().length > 0 && $("#PatientFirstName").val().length > 0 && $("#PatientLastName").val().length > 0){
					alert("Thank you for submitting referral!");
					document.getElementById('referCase').submit();
					}					
					else{
					
						var isValid = true;
							if($('#fileRx').val()==''){
								$('input[type="text"].required').each(function(){
									if($.trim($(this).val()) == ''){
										isValid = false;
										$(this).css({
											"border":"1px solid red",							
										});
									}
									else{
										$(this).css({
											"border":"",							
										});
									}
									
								});
								alert('Choose Document to upload!');
							}
							else{								
									if($("#CaseClaimNumber1").val().length <= 0){
											isValid = false;
											$("#CaseClaimNumber1").css({
												"border":"1px solid red",
											});						
										
									}
									if($("#refFirstName").val().length <= 0){
											isValid = false;
											$("#refFirstName").css({
												"border":"1px solid red",
											});						
										
									}
									if($("#refLastName").val().length <= 0){
											isValid = false;
											$("#refLastName").css({
												"border":"1px solid red",
											});						
										
									}
									if($("#AdjusterBranch").val().length <= 0){
											isValid = false;
											$("#AdjusterBranch").css({
												"border":"1px solid red",
											});						
											
									}
									if($("#PatientFirstName").val().length <= 0){
											isValid = false;
											$("#PatientFirstName").css({
												"border":"1px solid red",
											});						
										
									}
									if($("#PatientLastName").val().length <= 0){
											isValid = false;
											$("#PatientLastName").css({
												"border":"1px solid red",
											});						
										
									}
								}							
							if(isValid == false){
								return false;
								}
							/*else{
								alert('Thank you for submiting referral!');
								document.getElementById('referCase').submit();
								}*/
							
					}
				}					

				function validateEmail(ReferringPhysicianEmail){
				   var a = document.getElementById(ReferringPhysicianEmail).value;
				   var filter = /^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*?)\s*;?\s*)+/;
					if(filter.test(a)){
						return true;
					}
					else{
						return false;
					}
				}
				
				function validateEmail(AdjusterEmail){
				   var a = document.getElementById(AdjusterEmail).value;
				   var filter = /^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*?)\s*;?\s*)+/;
					if(filter.test(a)){
						return true;
					}
					else{
						return false;
					}
				}
				
				function valZip(){
					if($('#PatientZip').val().length != 5){
						alert("Enter valid zip code");
						}		
					}	
						
				function tab(field,nextFieldID){
				if(field.value.length >= field.maxLength){
					document.getElementsByClassName(nextFieldID)[0].focus();
					}
				}
				
			//modified	 

$(document).ready(function(){
			var page = 0;
			 function loadPage(x){
				 $("tr[name*='page']").each(function(i, row){
						$(row).attr("style","display:none;"); 
					 });
				 $("tr[name='page"+x+"']").each(function(i, row){
					$(row).attr("style",""); 
				 });
			 }
			
			var rows = $("tbody.tablepg").find("tr");
			 
			 $(rows).each(function (i, row){
				 if(i % 15 == 0){
					 page++;
				 }
				 $(row).attr("name", "page" + page);	
				 $(row).attr("style","display:none;");
			 });
			 loadPage(1);
			 for(var i = 0; i < page ; i++){
				 var obj = $(document.createElement("li"));
				 var a = $(document.createElement("a")).html((i+1)).click(function(){
					 var pageNumber = $(this).html();
					 loadPage(pageNumber);
				 });
				 $(obj).append(a);
				 $("#pageList").append(obj);
			 }
		
			$('#PatientDOI').datepicker({changeYear: true, yearRange : '1950:2010'});
			$('#FollowUpDate').datepicker({changeYear: true, yearRange : '2013:2014'});
			$('#PatientDOB').datepicker({changeYear: true, yearRange : '1920:2000'});
			$('#DOM').datepicker({changeYear: true, yearRange : '1950:2010'});
			
			$(function() {
				$( "#PatientDOI" ).datepicker({
				changeMonth: true,
				changeYear: true				
			});
			});
			
			$(function() {
				$( "#FollowUpDate" ).datepicker({
				changeMonth: true,
				changeYear: true
			});
			});
			
			$(function() {
				$( "#PatientDOB" ).datepicker({
				changeMonth: true,
				changeYear: true
			});
			});
			
			$(function() {
				$( "#DOM" ).datepicker({
				changeMonth: true,
				changeYear: true
			});
			});			
						
			$(".lbs").keydown(function(event) {
			// Allow: backspace, delete, tab, escape, and enter
				if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
				 // Allow: Ctrl+A
					(event.keyCode == 65 && event.ctrlKey === true) || 
				 // Allow: home, end, left, right
					(event.keyCode >= 35 && event.keyCode <= 39)) {
					 // let it happen, don't do anything
						return;
				}
					else {
						// Ensure that it is a number and stop the keypress allowing shift for capitals
						if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) 
						{
							event.preventDefault(); 
						}   
					}
				});	
				
				
				
				$(".noNumb").keydown(function(event) {
			
						// Ensure that it is a number and stop the keypress
						if ( (event.keyCode > 47 && event.keyCode < 58)) {
							
							
							event.preventDefault(); 
						}   
				
				});	

				$('#ReferringPhysicianEmail').blur(function() {
				  if(!validateEmail('ReferringPhysicianEmail'))
				  {
					  alert('Although email is not required if selected must be valid!');
				  }
				  
				});
			   
				$('#AdjusterEmail').blur(function() {
				  if(!validateEmail('AdjusterEmail'))
				  {
					  alert('Although email is not required if selected must be valid!');
				  }
				  
				});
			   
				/*$('#AdjusterPhone1, #AdjusterPhone2, #AdjusterPhone3').bind('keypress blur', function(){
						$('#AdjusterPhone').val($('#AdjusterPhone1').val() + $('#AdjusterPhone2').val() + $('#AdjusterPhone3').val());
					});
					
				$('#AdjusterFax1, #AdjusterFax2, #AdjusterFax3').bind('keypress blur', function(){
						$('#AdjusterFax').val($('#AdjusterFax1').val() + $('#AdjusterFax2').val() + $('#AdjusterFax3').val());
				});
				
				$('#PatientPhone1, #PatientPhone2, #PatientPhone3').bind('keypress blur', function(){
						$('#PatientHomePhone').val($('#PatientPhone1').val() + $('#PatientPhone2').val() + $('#PatientPhone3').val());
				});
				
				$('#PatientMobilePhone1, #PatientMobilePhone2, #PatientMobilePhone3').bind('keypress blur', function(){
						$('#PatientMobilePhone').val($('#PatientMobilePhone1').val() + $('#PatientMobilePhone2').val() + $('#PatientMobilePhone3').val());
				});
				
				$('#EmployerPhone1, #EmployerPhone2, #EmployerPhone3').bind('keypress blur', function(){
						$('#EmployerPhone').val($('#EmployerPhone1').val() + $('#EmployerPhone2').val() + $('#EmployerPhone3').val());
				});
				
				$('#MDPhone1, #MDPhone2, #MDPhone3').bind('keypress blur', function(){
						$('#MDPhone').val($('#MDPhone1').val() + $('#MDPhone2').val() + $('#MDPhone3').val());
				});
				
				$('#MDFax1, #MDFax2, #MDFax3').bind('keypress blur', function(){
						$('#MDFax').val($('#MDFax1').val() + $('#MDFax2').val() + $('#MDFax3').val());
				});*/
				
				
				$('#style').click(function(){                   
                        $('.display').show();
                        $('.read').removeAttr("readonly");                      
                });
                
                $('#locked').click(function(){
                        $('.read').attr('readonly', true);
                        $('.display').hide();
                });	
				
				$("input[type='file']").change(
					function(){
					if($(this).val() != ''){
						$('.hideMe').hide();
					}
				});	
							
		});