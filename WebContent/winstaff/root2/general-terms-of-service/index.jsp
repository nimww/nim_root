<%
String pageTitle = "General Terms of Service";
%>
<%@ include file="/header.jsp" %>

<div class="container">
	<div class="row">
		<div class="span7">
			<h1>General Terms of Service</h1>
			<br>
			<p>By using this website for submitting a request for service, or purchase of a service from this website, I acknowledge that I have read, understand, and agree to the following terms of service.</p>
			
			<ol>
				<li>Medical Advice Disclaimer: NextImage Medical, Inc. dba NextImage Direct (NextImage) negotiates discounts with medical providers on behalf of purchasers of such medical services. A purchaser of medical services includes, but is not limited to, individual patients, insurance companies, and third party administrators. NextImage is neither a health insurance company nor a health plan. NextImage does not imply nor do we provide any professional medical services. The content of the Web Site, including without limitation, text, copy, audio, video, photographs, illustrations, graphics and other visuals, is for informational purposes only and does not constitute professional medical advice, diagnosis, treatment or recommendations of any kind. You should always seek the advice of your qualified heath care professionals with any questions or concerns you may have regarding your individual needs and any medical conditions.
                </li>
				<li>Privacy and Confidentiality.  NextImage follows all current HIPAA regulations to assure that your private medical information remains confidential. I agree that I have been informed of my rights to privacy regarding my protected health information under the Health Insurance Portability & Accountability Act of 1996 (HIPAA).  I understand that this information may be used to 1) Coordinate my treatment among a number of health care providers who may be involved in that treatment directly and indirectly. 2) Obtain payment from third-party payers for my healthcare services, if applicable. 3) Conduct normal business operations such as quality assessment and improvement activities.
                </li>
				<li>Indemnification. You agree to defend, indemnify and hold NextImage and its affiliates directors, officers, employees and agents harmless from any and all claims, liabilities, costs and expenses, including reasonable attorneys' fees, arising in any way from your use of the Web Site or purchased services, your placement or transmission of any message, content, information, software or other materials through the Web Site, or your breach or violation of the law or of these Terms and Conditions.  NextImage reserves the right, at its own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, and in such case, you agree to cooperate with NextImage defense of such claim.
                </li>
			</ol>
        
		</div>
				<jsp:include page="../sidebar.jsp">	
				<jsp:param value="zipSearch" name="zip" />	
				<jsp:param value="../img/MRI.png" name="picture" />
				<jsp:param value="general-terms-of-service" name="head" />
				<jsp:param value="t1" name="t"/>
				</jsp:include>
	</div>
</div>
<jsp:include page="../footer.jsp"></jsp:include>
