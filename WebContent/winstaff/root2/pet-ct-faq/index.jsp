<%
String pageTitle = "PET-CT FAQs";
%>
<%@ include file="/header.jsp" %>

<div class="container">
  <div class="row">
    <div class="span8">
      <h1 id="top">PET-CT FAQs</h1>
	  
						<div class="accordion" id="q1">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q1" href="#collapsecomtrack1">
								What is a PET-CT scan?
							  </a>
							</div>
							<div id="collapsecomtrack1" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									<p>PET-CT is a type of nuclear medicine imaging that is highly advanced. PET/CT combines two different types of imaging into one procedure. PET and CT together produce a more accurate picture of what is happening in the body than either PET or CT alone, providing more accurate diagnoses. PET-CT is a very good tool for detecting the severity of many types of cancers as well as detecting many other abnormalities within the body. </p>
									
									PET and CT together produce a more accurate picture of what is happening in the body than either PET or CT alone, providing more accurate diagnoses. PET-CT is a very good tool for detecting the severity of many types of cancers as well as detecting many other abnormalities within the body.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q2">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q2" href="#collapsecomtrack2">
								What is PET?
							  </a>
							</div>
							<div id="collapsecomtrack2" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									 
									<p>A PET scan measures important body functions, such as blood flow, oxygen use, and sugar (glucose) metabolism, to help doctors evaluate how well organs and tissues are functioning. 
										These imaging scans use radioactive materials called radiopharmaceuticals or radiotracers. Depending on the type of exam, the radiotracer is either injected into the body, swallowed or inhaled as a gas.
									</p>
							  </div>
							</div>
						  </div>
						</div>
						<div class="accordion" id="q3">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q3" href="#collapsecomtrack3">
								What is a CT scan?
							  </a>
							</div>
							<div id="collapsecomtrack3" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									 CT stands for Computed Tomography. It is a computerized X-ray machine that examines your body�s organs, bones and tissues in greater detail than regular x-rays do. For your CT scan, you may require contrast material which helps produce an even clearer image. The CT scanner is comprised of a table and a bracket. The bracket is the donut shape part that houses the X-ray source. The X-ray source rotates inside the bracket as the patient moves through the large hole in the machine. Data is obtained and processed by a computer to produce a two dimensional image.
							  </div>
							</div>
						  </div>					  
						</div>						
						<div class="accordion" id="q4">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q4" href="#collapsecomtrack4">
								How does a PET-CT work?
							  </a>
							</div>
							<div id="collapsecomtrack4" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									 <p>A PET image is color coded � different colors show various levels of cell activity. A CT scan shows the exact locations of the body�s organs and also can show abnormal growths. When a CT scan is laid over a PET scan, doctors can pinpoint the exact location of abnormal cell activity. They can also see the level and extent of that activity. Even when an abnormal growth is not yet visible on a CT scan, the PET scan can show the abnormal cell activity. </p>
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q5">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q5" href="#collapsecomtrack5">
								What are some common uses of the procedure?
							  </a>
							</div>
							<div id="collapsecomtrack5" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									  <ul>
										<li>Detect cancer.</li>
										<li>Determine whether a cancer has spread in the body.</li>
										<li>Determine if a cancer has returned after treatment.</li>
										<li>Determine blood flow to the heart muscle.</li>
										<li>Determine the effects of a heart attack on areas of the heart.</li>
										<li>Evaluate brain abnormalities, such as tumors, memory disorders, seizures and other central nervous system disorders.</li>
										<li>Map normal human brain and heart function.</li>
									  </ul>
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q6">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q6" href="#collapsecomtrack6">
								How do I prepare for my PET-CT exam?
							  </a>
							</div>
							<div id="collapsecomtrack6" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									  <p> This section gives you general guidelines to prepare for your exam. Your doctor, nurse, or testing center will give you more detailed instructions. You will receive specific instructions based on the type of PET scan you are undergoing.</p>
									  <ul>        
										<li>Wear loose-fitting, comfortable clothes to the test. </li>
										<li>You should inform your physician and the technologist performing your exam of any medications you are taking, including vitamins and herbal supplements. You should also inform them if you have any allergies, asthma and about recent illnesses or other medical conditions.</li>
										<li>Women should inform their doctors if they are breastfeeding or there is any chance they may be pregnant as a PET-CT scan could put the baby at risk. </li>
										<li>If you have a fear of enclosed spaces, tell your doctor before the day of your PET-CT exam as your doctor can prescribe medication to help you relax during the exam.</li>
										<li>Do not schedule any other test that uses radioactive material for the same day as your PET-CT exam. </li>
										<li>Plan to be at the testing center for 2 to 2 1/2 hours on the day of your PET-CT exam. It is very important to arrive on time at the testing center. </li>
										<li>If you cannot come to the testing center at your scheduled time, please call 48 hours in advance to reschedule your test.</li>  
									  </ul>
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q7">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q7" href="#collapsecomtrack7">
								What happens during the exam?
							  </a>
							</div>
							<div id="collapsecomtrack7" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									<p>When you arrive at the imaging facility you may be asked to change into a hospital gown or remove any clothing or jewelry that may interfere with the scan. A nurse or radiation technologist will provide the radioactive substance needed for the scan through an intravenous (IV) injection. The radioactive substance will not cause any sensation in your body when injected. The radioactive substance will take approximately 30-90 minutes to travel through your body and to be absorbed by the organ or tissue being studied. You will be asked to rest quietly, avoiding movement and talking as too much movement can interfere with the normal distribution of the radioactive material.</p>
		
									<p>Depending on which part of your body is being scanned, you may also be given a contrast agent for the CT scan. The contrast can be given orally (swallowed), through an IV, or through an injection (shot). The contrast agent helps create a clearer picture of specific parts of your body. You may get a warm, flushed feeling; a taste of salt or metal in your mouth; or nausea for a few minutes, which are all normal reactions that last just a few minutes. If you experience a more serious reaction, such as trouble breathing, tell the radiation technologist immediately.</p>
		
									<p>When it comes time for the scan, the technologist will help you onto the table. The table may be padded and have straps or a special cradle to keep your head in place. You will most likely be asked to lie on your back, although you may be asked to lie on your side, depending on which body part is being studied. During the exam, the technologist will monitor the test through a window in a control room and you will be able to communicate through an intercom system.</p>
		
									<p>The PET-CT scanner resembles a large donut. The exam table moves back and forth through the machine as the scanner rotates around you.  The scans are done sequentially � first the CT scans, then the PET scans. The scan is not painful; however, you will need to lie still for the entire scan. You can also expect to hear clicking sounds from the machine.</p>
		
									<p>The appointment typically lasts up to an hour; although it can vary depending on the body part you are getting scanned. The technologist should be able to give you a time estimate before the scan begin</p>
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q8">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q8" href="#collapsecomtrack8">
								What happens after the scan?
							  </a>
							</div>
							<div id="collapsecomtrack8" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									 <p>You can expect to return to your normal activities following the PET-CT scan, including driving. Drinking water will help to flush out the contrast material and radioactive substance from your body. </p>
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q9">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q9" href="#collapsecomtrack9">
								How do I get my results?
							  </a>
							</div>
							<div id="collapsecomtrack9" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									<p>Your final report will be sent to your referring physician within 48 hours of the test. Your referring physician will discuss the results with you during your follow-up appointment.</p>
							  </div>
							</div>
						  </div>					  
						</div>
						<strong><a class="pull-right" href="#top" >TOP</a></strong>
      
      <!--<h4><strong>What happens before the exam?</strong></h4>
      	<p>You will need to remove all metal objects that could interfere with the scan. If necessary, an IV will be inserted in your arm. You may receive the radiotracer, a drug that emits radioactivity, via an IV, by swallowing or inhaling as a gas. Typically, it will take approximately 60 minutes for the radiotracer to travel through your body and to be absorbed by the organ or tissue being studied. You will be asked to rest quietly, avoiding movement and talking. You may be asked to drink some contrast material that will localize in the intestines and help the radiologist interpreting the study.</p>-->
    
  </div>
    <jsp:include page="../sidebar.jsp">
    	<jsp:param value="zipSearch" name="zip" />
    	<jsp:param value="../img/MRI.png" name="picture" />
    	<jsp:param value="testimonials" name="head" />
    	<jsp:param value="t1" name="t"/>
	</jsp:include>
  </div>
</div>
<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
<jsp:include page="../footer.jsp"></jsp:include>
