<%
String pageTitle = "Payment Terms of Service";
%>
<%@ include file="/header.jsp" %>

<div class="container">
	<div class="row">
		<div class="span7">
			<h1 style="font-size: 44px;">Payment Terms of Service</h1>
			<br>
			<p>By using this website for purchase of a service I acknowledge that I have read, understand, and agree to the following terms of service.</p>
			
			<ol>
				<li><strong>Payment Terms and Conditions. </strong>All services are prepaid prior to assignment of provider.</li>
                
                <ol style="list-style-type: lower-alpha;">
                    <li><strong>Cancellation Refund Policy:</strong> : If you cancel your purchase at any time, a refund will be applied less a $50 processing fee.</li>
                    <li><strong>No Shows:</strong> : If you do not show up for your appointment and you did not contact NextImage prior to your appointment time, you may not receive any refund. </li>
                    <li><strong>Transportation and Translation Services:</strong>  We do not provide transportation or translation services.</li> 
                </ol>
			</ol>
        
		</div>
		<jsp:include page="../sidebar.jsp">	
				<jsp:param value="zipSearch" name="zip" />	
				<jsp:param value="../img/MRI.png" name="picture" />
				<jsp:param value="securepaymentprocessing" name="head" />
				<jsp:param value="t1" name="t"/>
				</jsp:include>
	</div>
</div>
<jsp:include page="../footer.jsp"></jsp:include>





By using this website for purchase of a service I acknowledge that I have read, understand, and agree to the following terms of service.

1.	

a.	
b.	
c.	Transportation and Translation Services:  We do not provide transportation or translation services.
