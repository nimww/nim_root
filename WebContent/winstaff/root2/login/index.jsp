<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="java.text.DecimalFormat, com.winstaff.*,java.text.SimpleDateFormat,java.util.Calendar" %>
<%
String pageTitle = "NextImage Direct";
%>
<%@ include file="/header.jsp" %>
<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" rel="stylesheet"> -->
	<style>/* Gio Styles */
	.gfont{font-family: Proxima Nova Regular,Skolar Bold,Segoe UI Bold,Roboto Slab,Droid Serif,AvenirNext-Bold,Avenir Bold,Georgia,Times New Roman,Times,Diplomata,serif;font-weight: 700;font-style: normal;color:#4c4f53!important;}
	.dgfont{font-family: Proxima Nova Regular,Skolar Bold,Segoe UI Bold,Roboto Slab,Droid Serif,AvenirNext-Bold,Avenir Bold,Georgia,Times New Roman,Times,Diplomata,serif;font-weight: 700;font-style: normal;color:white!important;}
	.moGfont{font-family: Proxima Nova Regular,Segoe UI,Roboto,Droid Sans,Helvetica Neue,Arial,sans-serif,Diplomata,serif;font-style: normal;font-weight: 400;}
	.gfontsy{font-family: Proxima Nova Regular,Skolar,Roboto Slab,Droid Serif,Cambria,Georgia,Times New Roman;font-weight: 400;font-style: normal;}
	.moGfontItal{font-family: Proxima Nova Regular,Segoe UI,Roboto,Droid Sans,Helvetica Neue,Arial,sans-serif,Diplomata,serif;font-style: normal;font-weight: 400;}
	.parahov{color: white; background-color: #999;}
	.dnsc{display:none;}
	.gfontTWO{font-family: Proxima Nova Regular,Skolar Bold,Segoe UI Bold,Roboto Slab,Droid Serif,AvenirNext-Bold,Avenir Bold,Georgia,Times New Roman,Times,Diplomata,serif;font-weight: 700;font-size:15px;font-style: normal;color:#4c4f53!important;}
	.gfontTWOWh{font-family: Proxima Nova Regular,Skolar Bold,Segoe UI Bold,Roboto Slab,Droid Serif,AvenirNext-Bold,Avenir Bold,Georgia,Times New Roman,Times,Diplomata,serif;font-weight: 700;font-size:15px;font-style: normal;color:#white!important;}
	.onhv:hover, .onhv:hover{color: #131313;font-size:19px;font-weight: 600;font-style:italic;}
	.ml{margin-left:10px}
	.gfontthree{font-family: Proxima Nova Regular,Skolar Bold,Segoe UI Bold,Roboto Slab,Droid Serif,AvenirNext-Bold,Avenir Bold,Georgia,Times New Roman,Times,Diplomata,serif;font-weight: 700;font-size:18px;font-style: normal;color:#4c4f53!important;}
	.gfont40{font-family: Proxima Nova Regular,Skolar Bold,Segoe UI Bold,Roboto Slab,Droid Serif,AvenirNext-Bold,Avenir Bold,Georgia,Times New Roman,Times,Diplomata,serif;font-weight: 700;font-size:24px;font-style: normal;color:#4c4f53!important;margin-top:3%;}
	.bgirl{color:#120A8F!important;}
	.bpad{padding-left:35px!important;}
	.lpad{margin-bottom:8px!important;}
	.mreq{color: #ff0000!important;}
	.bcoo{color:#0000FF!important;}
	 hr.style-five {border: 0;height: 0; /* Firefox... */ box-shadow: 0 0 10px 1px black; margin-top: 15px; margin-bottom: 15px;		}
	 hr.style-five:after {  /* Not really supposed to work, but does */content: "\00a0";  /* Prevent margin collapse */}
	 .nanit{font-family:Skolar Italic,Roboto Slab,Droid Serif,Cambria,Georgia,Times New Roman,Times,serif;font-weight: 400;font-style: italic;font-size:16px;padding:1%;}
	 .mybtsp{font-family:Skolar Italic,Roboto Slab,Droid Serif,Cambria,Georgia,Times New Roman,Times,serif;font-weight: 400;font-style: italic;font-size:13px;padding:1%;}
	 .mtop{margin-top:1%;}
	.mtop2{margin-top:2%;}
	.ui-autocomplete {position: relative;}
    .custospmods{margin-top:-4%;padding:5%;}
    .tml{margin-left:3%;}
	.mbottom2{margin-bottom:2%;}
	.mpad3{padding:4%;}
	.mpad1{padding:1%;}
	.mpad10{padding:7%;}
	.mpad-top5{padding-top:5%;}
	.mpad-bot5{padding-bottom:5%;}
	.bornone{border:none;}
	.dnsm{display:none;}
	</style>
<body>
	
	<div class="container">
    	<div class="row-fluid" style="background-color:#444;color:#fff" >
			<div class="span12" style="min-height:500px;">
					<form  action="/find-facilities-near-you/" method="post" id="findCenterForm" onSubmit="return findCenter();" >
						<div class="row-fluid span7 offset4">
							<h3>Login to our secure report center.</h3>
						</div>
						<article class="span7 offset4" style="padding:2%">
				 		 	<div class="row-fluid">
				 		 		<input name="username" type="text" class=""  placeholder="username" />
				 		 	</div>
				 		 	<div class="row-fluid">
				 		 		<input name="password" type="text" class=""  placeholder="password" />
				 		 	</div>
						</article>
			  		</form>
			</div>
		</div>
	</div>		
</body>
