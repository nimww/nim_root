<!--Created By Giovanni Hernandez 06/27/2013-->
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/header.jsp" %>
<form:form method="POST" commandname="userSettings" action="userSettings">                                    
                <div class="container">                    
                    <div class="row-fluid" style="background-image:linear-gradient(to bottom, #ffffff, #f2f2f2); border:1px solid#d4d4d4; -webkit-border-radius: 4px; border-radius: 4px; -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.065); box-shadow: 0 1px 4px rgba(0, 0, 0, 0.065);"><!-- Case Account rgb(102,204,255)-->
                        <h4 class="text-center" style="color:#777777">User Settings</h4>
                    </div>                    
                                
                    <div class="row-fluid" style="margin-top:1%">                    
                        <div class="span3" id="longForm">
                                <strong style="margin-left:3%">First Name: </strong>                                        
                        </div>                        
                        <div class="span3" id="longForm">                                
                                <form:input path="groupBranch" cssClass="required noNumb " placeholder="Santee" readonly="true"/>                         
                        </div>                        
                        <div class="span3" id="longForm" >
                                <strong style="margin-left:3%">Last Name: </strong>                                
                        </div>                        
                        <div class="span3" id="longForm">                            
                                <form:input path="groupName" cssClass="required noNumb tabbedFrom1" placeholder="USHW" readonly="true"/>                                 
                        </div>                        
                    </div>                    
                    
                    <div class="row-fluid">                                                    
                        <div class="span3" id="longForm">
                            <strong style="margin-top:17%; margin-left:3%">Email: </strong><br /><br />
                            <strong  style="margin-top:11%; margin-left:3%">Fax: </strong>                                
                        </div>                        
                        <div class="span3" id="longForm">                                
                            <form:input path="groupEmail" cssStyle="margin-top:2%" onchange="emailVal();" placeholder="EMAIL"/>                         
                            
                            <form:input path="groupFax1" value="" placeholder="XXX" id="PatientMobile" cssClass="lbs area1 read" cssStyle="width:21%;" maxlength="3" onkeyup="tab(this,'pre1')"/><span>-</span>                             
                                
                            <form:input path="groupFax2" value="" placeholder="XXX" id="PatientMobile" cssClass="lbs pre1 read" cssStyle="width:24%;" maxlength="3" onkeyup="tab(this,'exch1')"/><span>-</span>                         
                            
                            <form:input path="groupFax3" value="" placeholder="XXXX" id="PatientMobile" cssClass="lbs exch1 read" cssStyle="width:27%;" maxlength="4" onkeyup="tab(this,'tabbedFrom1')" />                                 
                        </div>                        
                        <div class="span3" id="longForm">
                            <p style="margin-top:4%; margin-left:3%"><strong>Direct Phone: </strong></p>
                            <p style="margin-top:7%; margin-left:3%"><strong>Mobile: </strong></p>                                
                        </div>                        
                        <div class="span3" id="longForm">                                
                            
                            <form:input path="directPhone1" value="" placeholder="XXX" id="PatientPhone" cssClass="lbs area2 read" cssStyle="width:21%; margin-top:1%" maxlength="3" onkeyup="tab(this,'pre2')" /><span>-</span>                            
                            
                            <form:input path="directPhone2" value="" placeholder="XXX" id="PatientPhone" cssClass="lbs pre2 read" cssStyle="width:24%;margin-top:1%" maxlength="3" onkeyup="tab(this,'exch2')"/><span>-</span>                            
                            
                            <form:input path="directPhone3"  value="" placeholder="XXXX" id="PatientPhone" cssClass="lbs exch2 read" cssStyle="width:27%;margin-top:1%" maxlength="4" onkeyup="tab(this,'area3')"/>                            
                            
                            <form:input path="directMobile1" value="" placeholder="XXX" id="PatientMobile" cssClass="lbs area3 read" cssStyle="width:21%;margin-top:1%" maxlength="3" onkeyup="tab(this,'pre3')"/><span>-</span>                            
                                
                            <form:input path="directMobile2" value="" placeholder="XXX" id="PatientMobile" cssClass="lbs pre3 read" cssStyle="width:24%;margin-top:1%" maxlength="3" onkeyup="tab(this,'exch3')"/><span>-</span>                        
                                
                            <form:input path="directMobile3" value="" placeholder="XXXX" id="PatientMobile" cssClass="lbs exch3 read" cssStyle="width:27%;margin-top:1%" maxlength="4" onkeyup="tab(this,'sched')"/>                            
                        </div>                    
                    </div>    
                    
                    <div class="row-fluid" style="background-image:linear-gradient(to bottom, #ffffff, #f2f2f2); border:1px solid#d4d4d4; -webkit-border-radius: 4px; border-radius: 4px; -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.065); box-shadow: 0 1px 4px rgba(0, 0, 0, 0.065); margin-top:2%"><!-- Case Account rgb(102,204,255)-->
                        <h4 class="text-center" style="color:#777777">Preferred Notifications for Group </h4>
                    </div>    
                    
                    <div class="row-fluid" style="margin-top:2%;">
                        <div class="span2">
                            <strong><p id="sched" style="margin-left:3%">Scheduled</p></strong>
                        </div>
                        <div class="span2">
                            <form:select path="scheduled" cssStyle="width:90%" items="${alertNotify}" />                            
                        </div>        
                        <div class="span2">
                            <strong><p style="margin-left:3%">Reports</p></strong>
                        </div>
                        <div class="span2">                            
                            <form:select path="reports" items="${alertNotify}" cssStyle="width:90%" />
                        </div>    
                        <div class="span2">
                            <strong><p style="margin-left:3%">Other</p></strong>
                        </div>
                        <div class="span2">
                            <form:select path="other" items="${alertNotify}" cssStyle="width:90%"/>
                            
                        </div>                            
                    </div>
                    
                    <div class="row-fluid" style="background-image:linear-gradient(to bottom, #ffffff, #f2f2f2); border:1px solid#d4d4d4; -webkit-border-radius: 4px; border-radius: 4px; -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.065); box-shadow: 0 1px 4px rgba(0, 0, 0, 0.065); margin-top:2%; margin-bottom:2%">
                        <h4 class="text-center" style="color:#777777">Password Reset </h4>
                    </div>
                    <span style="margin-left:150px; margin-top:10px;"><strong>Note: Your new password
							must be a length of 8 or
							greater. Passwords can be alphanumeric</strong></span><br />
                    <div class="span6 offset3 row-fluid" style="margin-top:2%">
                    <div>
                    
                    </div>
                        <div class="span3">
                        
                            <p style="margin-top:4%"><strong >Confirm Password:</strong></p>
                            <p style="margin-top:7%"><strong >New Password:</strong></p>
                            <p style="margin-top:9%"><strong >Verify New Password:</strong></p>
                        </div>
                        <div class="span3">
                            
                            <form:input path="confirmPassword" cssClass="read" value="" placeholder="Confirm Password"/>
                            
                            <form:input path="newPassword" cssClass="read" value="" placeholder="New Password"/>
                            
                            <form:input path="verifyNewPassword" cssClass="read" value="" placeholder="Verify New Password"/>
                        </div>
                        
                    </div>
                    
                        <div class="row-fluid">                            
                            <button class="pull-right ButtonsReadyForView" role="button" value="click" type="submit"  >Save Data</button>
                        </div>                    
                </div>                
    </form:form>
<%@ include file="footer.jsp" %>
