<%@page contentType="text/html" language="java" import="com.winstaff.*,org.apache.http.*,java.io.*" %>
<%searchDB2 conn = new searchDB2();
String query = "";
java.sql.ResultSet rs = null;
boolean first = true;
String[] modality = {"MR", "CT", "US"}; 
String[] special = {"Normal","Arthrogram"};
String[] contrast = {"Without","With","WithAndWithout"};
String[] orientation = {"","Left","Right"};%>
<script>
<%for (String mod : modality){for(String spec : special){for(String cont : contrast){for(String ori : orientation){
query = "select cptwizardid, bodypart,bp1 from tcptwizard where modality='"+mod+"' and special='"+spec+"' and  contrast='"+cont+"' and orientation='"+ori+"' order by bp1";
rs = conn.executeStatement(query);first = true;%>
	var <%=mod%><%=spec%><%=cont%><%=ori%> = {<%while (rs.next()){if(first){%>
		"<%=rs.getString("cptwizardid")%>":"<%=rs.getString("bodypart")%>"<% first=false;} else {%>
		,"<%=rs.getString("cptwizardid")%>":"<%=rs.getString("bodypart")%>"<%}}%>
	}

	function get<%=mod%><%=spec%><%=cont%><%=ori%>(){
		$('#bp').find('option').remove();
		$('#bp').append("<option value=\"-1\">Please Select Body Part</option>")
		$.each(<%=mod%><%=spec%><%=cont%><%=ori%>, function(key, value) {   
			$('#bp')
				.append($("<option></option>")
				.attr("value",key)
				.text(value)); 
			});
	};
<%}}}}%>
</script>