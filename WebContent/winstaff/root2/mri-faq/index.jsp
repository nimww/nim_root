<%
String pageTitle = "MRI FAQs";
%>
<%@ include file="/header.jsp" %>

<div class="container">
  <div class="row">
    <div class="span8">
      <h1 id="top">MRI Scans FAQs</h1>
	  	  
						<div class="accordion" id="q1">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q1" href="#collapsecomtrack1">
								What Is Magnetic Resonance Imaging (MRI)?
							  </a>
							</div>
							<div id="collapsecomtrack1" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									MRI is an imaging technique that provides valuable information about joints, soft tissue and bones to your physician. MRI is different than an x-ray. It uses your body�s natural magnetic field to produce images. During an MRI exam, magnetic fields and radio waves safely scan the injured part of your body. Special MRI equipment, including a computer, a large, powerful magnet and radio waves, piece together information about your body�s tissues and structures based on the natural magnetic fields emitted by your body. A computer instantly converts this information into images that can show your doctor the details of your physical ailment with amazing clarity.
									MRI exams provide a painless, easy and accurate way for the doctors to look inside various parts of your body without the need for radiation.
									There are no known side effects of MRI exams.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q2">
						  <div class="accordion-group">
							<div class="accordion-heading">
								  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q2" href="#collapsecomtrack2">
									What is contrast?
								  </a>
							</div>
							<div id="collapsecomtrack2" class="accordion-body collapse out">
							  <div class="accordion-inner">
									In some cases, a dye called contrast material may be used. It will either be put in a vein through an IV or you will be asked to drink the dye. The contrast agent shows up white on the images. This makes your organs, blood vessels, and tissues more visible which help the radiologist interpret these imaging studies. please consult your doctor on wether or not contrast is required for your test.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q3">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q3" href="#collapsecomtrack3">
								Before Your MRI Exam
							  </a>
							</div>
							<div id="collapsecomtrack3" class="accordion-body collapse out">
							  <div class="accordion-inner">
									We recommend that you wear loose, comfortable clothing without any metal snaps, zippers or under wires. Avoid wearing jewelry and remove medication/magnetic patches.<br><br>
									Follow all exam preparation instructions given to you by the imaging center staff. You will be asked to remove all loose metal objects from your body.<br><br>
									Because of the way MRI works, certain metallic objects cannot be present in the MRI unit. It is critical that you inform the imaging center staff of any metal objects inside your body prior to scheduling your MRI.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q4">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q4" href="#collapsecomtrack4">
								Examples of Metallic Objects Include:
							  </a>
							</div>
							<div id="collapsecomtrack4" class="accordion-body collapse out">
							  <div class="accordion-inner">
									<ul>
										<li>Pacemaker</li>
										<li>Joint or bone pins</li>
										<li>Metal plates</li>
										<li>Unremoved bullets, shrapnel or BB shot</li>
										<li>Inner ear implants</li>
										<li>Aneurysm clips, surgical clips or stents</li>
										<li>Metal fragments from welding </li>
									</ul>
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q5">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q5" href="#collapsecomtrack5">
								During Your MRI Exam
							  </a>
							</div>
							<div id="collapsecomtrack5" class="accordion-body collapse out">
								<div class="accordion-inner">
									An MRI is painless. The individual who will perform the MRI study is known as a Radiologic Technologist, a highly skilled and educated person that works under close supervision of the physician.<br><br>
									You will be placed on an examination table. The table will then move slowly into the MRI unit,
									which is an air-conditioned tunnel-like space that is open on both ends. The technologist will have you in full view and will be in contact with you via a two way microphone system.<br><br>
									Throughout the exam, you will hear some loud noises, ranging from buzzing to knocking. These are normal sounds of the equipment.<br><br>
									It is important that you do not move during the exam as this will produce blurred images.
								</div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q6">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q6" href="#collapsecomtrack6">
								What is a Closed MRI?
							  </a>
							</div>
							<div id="collapsecomtrack6" class="accordion-body collapse out">
								<div class="accordion-inner">
									A closed MRI refers to the geometry of the magnets. Closed MRIs have a tube-like configuration; the strength of the magnetic fields is variable, but high field superconducting MRI scanners are typically 1.5 to 3 Tesla closed, or traditional, MRIs. The closed MRI is faster, and the high field provides better resolution and higher magnet strength for smaller parts of the anatomy.
								</div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q7">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q7" href="#collapsecomtrack7">
								What is an Open MRI?
							  </a>
							</div>
							<div id="collapsecomtrack7" class="accordion-body collapse out">
								<div class="accordion-inner">
									In contrast to a traditional closed MRI, Open MRI technology uses a convenient �open air� construction. Because of this spacious design, claustrophobic patients will experience less anxiety during the scanning process. Since Open MRI acquires detailed images without the use of radiation, you can be confident that you are receiving the latest in safety, comfort and diagnostic excellence. However, these machines typically have a lower strength magnet so it is recommended that you go to a closed machine which yields a better quality image whenever possible.
								</div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q8">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q8" href="#collapsecomtrack8">
									Will I Require an Injection?
							  </a>
							</div>
							<div id="collapsecomtrack8" class="accordion-body collapse out">
								<div class="accordion-inner">
									Most MRI tests will not require a contrast injection; however in certain circumstances depending on the diagnosis, it may improve the accuracy of the scan. If contrast is required, either an IV will be started to administer the contrast or you will be asked to swallow it. Please consult your physician on wether or not your exam requires contrast material. Please consult your physician on whether or not your exam requires contrast material.
								</div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q9">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q9" href="#collapsecomtrack9">
								What If I Require Sedation?
							  </a>
							</div>
							<div id="collapsecomtrack9" class="accordion-body collapse out">
								<div class="accordion-inner">
									If you feel that you may be claustrophobic or have anxiety, please contact your referring physician or primary care physician to prescribe medication for you to take prior to the test.
								</div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q10">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q10" href="#collapsecomtrack10">
								What If I Am Pregnant?
							  </a>
							</div>
							<div id="collapsecomtrack10" class="accordion-body collapse out">
								<div class="accordion-inner">
									If you are pregnant or could be pregnant at the time of your appointment please inform your referring physician prior to getting the scan performed.<br><br>
									MRI is usually avoided in the first trimester of pregnancy unless the diagnosis cannot wait and your doctor considers MRI to be the best investigation.
								</div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q11">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q11" href="#collapsecomtrack11">
									Following the Exam
							  </a>
							</div>
							<div id="collapsecomtrack11" class="accordion-body collapse out">
								<div class="accordion-inner">
									No recovery period is necessary following an MRI exam. You may resume your usual activities and normal diet immediately after the exam.
								</div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q12">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q12" href="#collapsecomtrack12">
									Wondering About Exam Results?
							  </a>
							</div>
							<div id="collapsecomtrack12" class="accordion-body collapse out">
								<div class="accordion-inner">
									A radiologist, a physician who specializes in the interpretation of MRI imaging tests, will review the images and send the final report to your referring physician within 48 hours of the test. Contact your referring physician to make a follow-up appointment. 
								</div>
							</div>
						  </div>					  
						</div>     
    <strong><a class="pull-right" href="#top" >TOP</a></strong>
      
        
  </div>
    
	<jsp:include page="../sidebar.jsp">
				<jsp:param value="zipSearch" name="zip" />
		    	<jsp:param value="../img/MRI.png" name="picture" />
		    	<jsp:param value="testimonials" name="head" />
		    	<jsp:param value="t1" name="t"/>
	</jsp:include>
  </div>
</div>
<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
<jsp:include page="../footer.jsp"></jsp:include>
