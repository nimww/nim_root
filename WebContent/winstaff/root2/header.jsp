<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="java.text.DecimalFormat, com.winstaff.*,java.text.SimpleDateFormat,java.util.Calendar" %>

<!doctype html>
<html>
<head>
<%if(pageTitle.equalsIgnoreCase("Find Facilities Near You") || pageTitle.equalsIgnoreCase("Reserve Image Center")){%>	
<link rel="stylesheet" href="/css/custom-theme/jquery-ui-1.10.2.custom.min.css" />
<%} %>

<%

bltNIM3_PayerMaster payerMaster = null;

Object rCodeO = session.getAttribute("nid");
String rCode = "--";
if (rCodeO!=null){
	rCode = rCodeO.toString();
	NIM3_ReferralSourceObject myRSO = NID_SubmitCaseObject.translateWebCodesToReferralSource ("","",rCode);
	payerMaster = new bltNIM3_PayerMaster(myRSO.getPayerID());

} else {
	//default to NID 123
	payerMaster = new bltNIM3_PayerMaster(123);
}
out.println("<!-- "+session.getAttribute("nid")+" -->");

%>









<meta name="viewport" content="width=device-width, initial-scale=1.0">
<META NAME="title" content="NextImage Direct, Cost Saving MRI, Cost Saving CT, Cost Saving Ultrasound, Underinsured, High deductible, Self pay ,MRI, Cheap Magnetic Resonance Imaging, Cheap, Low Cost MRIs, Affordable MRIs,Discount MRIs, Magnetic Resonance Imaging prices, MRI Prices,MRI Costs, Affordable MRI">
<META NAME="description" content="NextImage Direct, Cost Saving MRI, CT, Ultrasound for Underinsured, High deductible, or Self pay">
<META NAME="keywords" content="MRI, MRIs, low cost MRIs,discount MRI,MRI prices, magnetic resonance imaging,discount magnetic resonance imaging, affordable MRI, MRI, CT, Cat Scan, US, Ultrasound, Underinsured, High deductible, Self pay">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title><%= pageTitle %> | Cost Saving MRI, CT, Ultrasound for Underinsured, High deductible, or Self pay</title>

<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
<script type="text/javascript" charset="utf8" src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script src="/js/NID.js"></script>
<link href="/css/style.css" rel="stylesheet" media="screen">
<link href="/css/newCssChanges.css" rel="stylesheet" media="screen">

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37595845-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div class="container">
  <div class="row" style="margin:8px 0"> 
  
  
  <a href="/"><img src="/img/logo.png" style="margin-right:20px"> </a>
  <%
  if (payerMaster.getNID_ShowLogo()==1){
  %>
		 <img src="/img/partner/<%=payerMaster.getPayerID()%>.png">  
  <%}
  %>
<%=rCode.equals("care1") ? "<img src=\"/img/partner/1691.png\">":""%>

  <%=rCode.equals("ndcsavingsclub") ? "<img src=\"/img/partner/ndc.png\">":""%>
<%=rCode.equals("mot") ? "<img src=\"/img/partner/BenefitHubWelcome.jpg\">":""%>
    
  <%  
  	if(rCode.equals("ndcsavingsclub")){  
  %>
  		<script>
  		$(document).ready(function(){
  			$('#hidebuttonforndcsavingsclub').hide();
  			$('#clickforrxhideforndc').hide();
  			$('#prescreenhideneedrxndc').hide();
  			$('#moveleftwhenhidingrxndc').css({"margin-left":"-630px",});
  		});
  		
  		</script>
  <%} %>
  
  <h3 class="pull-right" ><a href="/find-facilities-near-you/" class="ButtonsHeaderNID ">Reserve a center now!</a><br>
		<span style="font-size: 24px;">
		
		
		<%  
  			if(rCode.equals("nb")){  
  		%>
			855-312-3450
		<%}else if(rCode.equals("dl")){ %>
			888-608-6099
		<%}else if(rCode.equals("mot")){%>
			855-461-8014
			<small>Use code "Benefit-hub" for an additional discount!</small>
		<%}else if(rCode.equals("dl")){ %>
			888-608-6099
		<%}else if(rCode.equals("benprovi")){%>
			855-306-6167			
		<%}else if(rCode.equals("care1")){%>
			858-764-8012 
		<%}else{%>
			888-693-8867
		<%}%>
		</span>
  </h3><br>

</div>
  <div class="navbar  navbar-inverse">
    <div class="navbar-inner">
		<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
      <div class="container">
        <div class="nav-collapse collapse" id="main-menu">
          <ul class="nav" id="main-menu-left">
            <li><a href="/">Home</a></li>
            <li><a href="/why-nextimage-direct/">Why NextImage Direct?</a></li>
            <li><a href="/how-it-works/">How it Works</a></li>
            <li><a href="/healthcare-professionals/">Healthcare Professionals</a></li>
            <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#">FAQs <b class="caret"></b></a>
              <ul class="dropdown-menu" id="swatch-menu">
                <li><a href="/faqs/">General FAQ</a></li>
                <li><a href="/mri-faq/">MRI FAQ</a></li>
                <li><a href="/ct-faq/">CT FAQ</a></li>
                <li><a href="/us-faq/">Ultrasound FAQ</a></li>
                <li><a href="/pet-ct-faq/">PET-CT FAQ</a></li>
              </ul>
              <li><a href="/directLabs/">Lab Work</a></li>
              <!--  <li><a href="/RxExamples/">Help Reading RX</a></li>-->
            </li>
            <li><a href="/contact/">Contact Us</a></li>
            
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
