<%
String pageTitle = "Ultrasound FAQs";
%>
<%@ include file="/header.jsp" %>

<div class="container">
  <div class="row">
    <div class="span8">
      <h1 id="top">Ultrasound FAQs</h1>
						<div class="accordion" id="q1">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q1" href="#collapsecomtrack1">
								What is an Ultrasound?
							  </a>
							</div>
							<div id="collapsecomtrack1" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									Ultrasound imaging, also referred to as a sonogram, is a noninvasive medical test that helps physicians diagnose and treat medical conditions. An ultrasound is a device that uses high frequency sound waves to create an image of some part of the inside of the body, such as the stomach, liver, heart, kidney, tendons, muscles, joints and blood vessels. <br><br>
									Obstetric ultrasound exams are frequently used to check the baby in the womb.<br><br>
									Ultrasound is widely available, low cost and easy to use, especially when compared with other modalities, such as MRI and CT. 
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q2">
						  <div class="accordion-group">
							<div class="accordion-heading">
								  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q2" href="#collapsecomtrack2">
									Are Ultrasounds safe? 
								  </a>
							</div>
							<div id="collapsecomtrack2" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									Experts say that since sound waves, rather than radiation, are used ultrasound exams are safe.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q3">
						  <div class="accordion-group">
							<div class="accordion-heading">
								  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q2" href="#collapsecomtrack3">
									How do you prepare for an Ultrasound? 
								  </a>
							</div>
							<div id="collapsecomtrack3" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									Wear loose comfortable clothing to your ultrasound exam. You may need to remove all clothing and jewelry in the area to be examined. Other preparation procedures depend on the type of exam you will have. For some scans your doctor may instruct you not to eat or drink for as many as 12 hours before your exam. For others you may be asked to drink up to six glasses of water two hours prior to your exam and avoid urinating so that your bladder is full when the exam begins.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q4">
						  <div class="accordion-group">
							<div class="accordion-heading">
								  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q4" href="#collapsecomtrack4">
									How does an Ultrasound work?
								  </a>
							</div>
							<div id="collapsecomtrack4" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									During the ultrasound exam, a wand called a transducer is used to view the organ and produce pictures for the study. The transducer emits sound and detects the returning echoes when it is placed on or over the body part being studied. The transducer transmits this information to a computer that displays the images on a monitor.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q5">
						  <div class="accordion-group">
							<div class="accordion-heading">
								  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q5" href="#collapsecomtrack5">
									What will I experience during and after the exam?
								  </a>
							</div>
							<div id="collapsecomtrack5" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									You will lie down on a table for the exam. Clothing over the area to be studied is removed and a special gel is applied to the skin to achieve good contact as an instrument, called a transducer, is passed back and forth. The transducer transmits the information to a computer that displays the information on a monitor. A specially trained technologist will perform the exam. <br><br>
									After the ultrasound exam, you should be able to resume your normal activities immediately.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q6">
						  <div class="accordion-group">
							<div class="accordion-heading">
								  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q6" href="#collapsecomtrack6">
									How long does it take to get an Ultrasound?
								  </a>
							</div>
							<div id="collapsecomtrack6" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									The exam time may vary depending on the area being studied, but most exams take about 15 minutes.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q7">
						  <div class="accordion-group">
							<div class="accordion-heading">
								  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q7" href="#collapsecomtrack7">
									Will I need someone to drive for me after the Ultrasound?
								  </a>
							</div>
							<div id="collapsecomtrack7" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									No, ultrasound is a safe test that will not affect your ability to drive. There are no harmful side effects and there is virtually no discomfort during the test.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q8">
						  <div class="accordion-group">
							<div class="accordion-heading">
								  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q8" href="#collapsecomtrack8">
									What are the benefits versus the risks?
								  </a>
							</div>
							<div id="collapsecomtrack8" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									<p>Benefits: </p>
									  <ul>
										<li>Ultrasounds are noninvasive</li>
										<li>Ultrasound is widely available, easy-to-use and less expensive than other imaging methods.</li>
										<li>Ultrasounds are safe and do not use ionizing radiation</li>
										<li>Ultrasounds give a clear picture of soft tissues that do not show up well on x-ray images.</li>
									  </ul>
									  <p>Risks:</p>
									  <ul><li>There are no known risks for standard diagnostic ultrasounds</li></ul>
							  </div>
							</div>
						  </div>					  
						</div>  		
						<p>For more info and FAQS go to<a href="http://www.ehealthmd.com"> www.ehealthmd.com</a> or <a href="http://ehealthmd.com/content/ultrasound-frequently-asked-questions#axzz2M9Anqfnu">http://ehealthmd.com/content/ultrasound-frequently-asked-questions#axzz2M9Anqfnu </a></p> 
						
						<strong><a class="pull-right" href="#top" >TOP</a></strong>
  </div>
    <jsp:include page="../sidebar.jsp">
    	<jsp:param value="zipSearch" name="zip" />
    	<jsp:param value="../img/MRI.png" name="picture" />
    	<jsp:param value="testimonials" name="head" />
    	<jsp:param value="t1" name="t"/>    	
	</jsp:include>
  </div>
</div>
<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
<jsp:include page="../footer.jsp"></jsp:include>
