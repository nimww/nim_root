    <%@ page isErrorPage="true" %>
    <%
	if (exception!=null){
		com.winstaff.DebugLogger.printLine("WEB.XML: ERROR [" + exception.toString() + "]");
	}
	%>
<script language="JavaScript">
<!--
function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}
//-->
</script>
<link rel="stylesheet" href="/winstaff/nim3/ui_200/style_sched.css" type="text/css">

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
  <tr>
    <td width="100%" height="100%"> 
      <table width="50%" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#333333">
        <tr> 
          <td>
            <table border="0" align="center" cellpadding="10" cellspacing="0">
              <tr> 
                <td align="center" class="big3">Error - Time Out<b>*</b></td>
              </tr>
              <tr> 
                <td align="center" class="borderHighlight1"><br />
                  Your account has been timed-out<br />
                  for security 
                  reasons to protect your data.<br />
                  <br /></td>
              </tr>
              <tr align="center"> 
                <td class="tdBase"> 
                    <p><a href="http://www.nextimagemedical.com/login.php" target="_top" class="big2">To Login<br />
                    please click here:</a></p>
                </td>
              </tr>
            </table>
          </td>
          <td width="225"><img src="/images/error1.jpg" width="225" height="300" /></td>
        </tr>
        <tr>
          <td colspan="2" align="center" bgcolor="#000000" class="tdHeaderBright">NextImage Medical - Copyright 2007-2011</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<!--
//[<% 
	if (exception!=null){
	   out.print(exception.toString());
	}
	   
	   
	   %>]
-->