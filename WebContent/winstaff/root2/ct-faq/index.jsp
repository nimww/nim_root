<%
String pageTitle = "CT FAQs";
%>
<%@ include file="/header.jsp" %>

<div class="container">
  <div class="row">
    <div class="span8">
      <h1 id="top">CT scan FAQs</h1>
	 
						<div class="accordion" id="q1">
						  <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q1" href="#collapsecomtrack1">
								What is a CT scan?
							  </a>
							</div>
							<div id="collapsecomtrack1" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									CT stands for Computed Tomography. It is a computerized X-ray machine that examines the body. The scanner is comprised of a table and a bracket. The bracket is the donut shape part that houses the X-ray source. The X-ray source rotates inside the bracket as the patient moves through. Data is obtained and processed by a computer to produce a two dimensional image.
							  </div>
							</div>
						  </div>					  
						</div>
     
						<div class="accordion" id="q2">
							 <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q2" href="#collapsecomtrack2">
									What body parts can the CT scanner evaluate?
							  </a>
							</div>
							<div id="collapsecomtrack2" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									What body parts can the CT scanner evaluate?</strong></h4>
									A CT scan can be used to study all parts of your body, such as the chest, belly, pelvis, or an arm or leg. It can take pictures of body organs, such as the liver, pancreas, intestines, kidneys, bladder, adrenal glands, lungs, and heart. It also can study blood vessels, bones, and the spinal cord.
							  </div>
							</div>
						  </div>					  
						</div>
						
						<div class="accordion" id="q3">
							 <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q3" href="#collapsecomtrack3">
									What is contrast?
							  </a>
							</div>
							<div id="collapsecomtrack3" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									In some cases, a dye called contrast material may be used. It may be put in a vein (IV) in your arm, or it may be placed into other parts of your body to see those areas better. For some types of CT scans you drink the dye. The contrast agent shows up white on CT images. This makes your organs, blood vessels, and tissues more visible, which helps the radiologist interpret these imaging studies. Please consult your Doctor on whether or not contrast is required for your test.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q4">
							 <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q4" href="#collapsecomtrack4">
									How do I prepare for a CT scan?
							  </a>
							</div>
							<div id="collapsecomtrack4" class="accordion-body collapse out">
							  <div class="accordion-inner">
									a.	The imaging center staff will provide detailed information on preparing for your particular scan. You may be instructed to not eat or drink for several hours before the test, especially if contrast material, or �dye,� is to be used. Some people are allergic to certain contrast material. Patients with allergies, particularly to iodine, should inform their doctor and the imaging center staff ahead of time. Women who are pregnant or may become pregnant should always inform their doctor and the CT technician to determine whether it is safe to have a CT scan<br />      
								  <ul id="q5">
									<li>Wear loose, comfortable clothing</li>
									<li>Leave jewelry, including your watch, at home</li>       
									<li>Jewelry hairpins, eyeglasses and dentures must be removed when having a head scan</li>
								  </ul>
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q5">
							 <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q5" href="#collapsecomtrack5">
									How long will it take to do a CT scan?
							  </a>
							</div>
							<div id="collapsecomtrack5" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									Expect the exam to be pain free, fast and easy. Most scans take just a few minutes to complete.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q6">
							 <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q6" href="#collapsecomtrack6">
									What will I experience during and after the scan? 
							  </a>
							</div>
							<div id="collapsecomtrack6" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									The actual scan will take only a few minutes; however, plan on being at the scanning facility for a longer period of time due to the preparations before and observation time after the procedure.<br><br>
									During the procedure, you are placed on a table that slides into the center of the large, doughnut-shaped X-ray machine. Once the procedure begins, you must remain very still while the images are taken. The technician will tell you when to hold your breath and when to exhale during the scan. This helps ensure the clearest images for your physician.<br><br>
									After a CT exam, you can return to your normal diet and activities. If you received contrast material, you may be given special instructions. For example, you may be told to drink plenty of water in order to flush your kidneys.  After the images are reviewed by the radiologist, the final report is sent to your physician.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q7">
							 <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q7" href="#collapsecomtrack7">
									What happens after the scan?
							  </a>
							</div>
							<div id="collapsecomtrack7" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									Typically after a CT scan, you can resume your normal diet and activities. If you received contrast material, you may be given special instructions. For example, you may be told to drink plenty of water in order to flush your kidneys.  After the images are reviewed by the radiologist, the final report is sent to your physician.
							  </div>
							</div>
						  </div>					  
						</div>
						<div class="accordion" id="q8">
							 <div class="accordion-group">
							<div class="accordion-heading">
							  <a class="accordion-toggle" data-toggle="collapse" data-parent="#q8" href="#collapsecomtrack8">
									Are there risks in obtaining a CT scan?
							  </a>
							</div>
							<div id="collapsecomtrack8" class="accordion-body collapse out">
							  <div class="accordion-inner" >
									A CT scan is a very low-risk procedure.  Some people experience a feeling of warmth throughout their body or the urge to urinate after receiving intravenous (IV) contrast material. These are normal and temporary reactions that go away once the scan is complete and the contrast material has passed through your system. <br><br>
									The amount of radiation a person receives during a CT scan is minimal and has not been shown to produce any adverse effects. However, if a woman is pregnant, the risk to the fetus is unknown. It is critical that female patients inform the imaging center staff of pregnancy and discuss alternative methods of imaging. All are encouraged to discuss the risks versus the benefits of your CT scan with your doctor.
							  </div>
							</div>
						  </div>					  
						</div>    
       <strong><a href="#top" class="pull-right">TOP</a></strong> 
  </div>
    <jsp:include page="../sidebar.jsp">
    	<jsp:param value="zipSearch" name="zip" />
    	<jsp:param value="../img/MRI.png" name="picture" />
    	<jsp:param value="testimonials" name="head" />
    	<jsp:param value="t1" name="t"/>
	</jsp:include>
  </div>
</div>
<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
<jsp:include page="../footer.jsp"></jsp:include>
