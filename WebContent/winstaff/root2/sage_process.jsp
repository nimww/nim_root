<%@page contentType="text/html" language="java" import="java.text.DecimalFormat ,com.winstaff.*, com.winstaff.password.*, org.apache.http.*" %>

<%
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      
	  java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("M/d/yyyy HH:mm:ss");
      java.text.SimpleDateFormat displayDateTimePL = new java.text.SimpleDateFormat("M/d/yyyy");


		org.apache.http.impl.client.DefaultHttpClient httpclient = new org.apache.http.impl.client.DefaultHttpClient();
		org.apache.http.client.methods.HttpPost httpost = new org.apache.http.client.methods.HttpPost("https://gateway.sagepayments.net/cgi-bin/eftBankcard.dll?transaction");        
		java.util.List <org.apache.http.NameValuePair> nvps = new java.util.ArrayList <org.apache.http.NameValuePair>();
		
		PracticeMaster_SearchResultsObject pm_sro = new PracticeMaster_SearchResultsObject(new Integer(request.getParameter("ic_code")));
		
		String wiz = request.getParameter("wiz_id");
		
		Double theCost = NIMUtils.getCPTGroupPayPriceforPracticeMaster(pm_sro.practiceMaster,NIMUtils.CPTWizardToCPTGroupObject(new bltCPTWizard(new Integer(wiz))), PLCUtils.getToday(), NIMUtils.PAYMENT_TYPE_GROUPHEALTH);
		

		Object rCodeO = session.getAttribute("nid");
		String rCode = "--";
		if (rCodeO!=null){
			rCode = rCodeO.toString();
		}
		pm_sro.setPrice(theCost);
		NIM3_ReferralSourceObject myRSO = NID_SubmitCaseObject.translateWebCodesToReferralSource("","", rCode);

		bltNIM3_PayerMaster payerMaster = new bltNIM3_PayerMaster(myRSO.getPayerID());
		bltNIM3_NIDPromo nidPromo = null;
		if (myRSO.getNidPromo()!=null){
			nidPromo = new bltNIM3_NIDPromo(myRSO.getNidPromo());
		}
		pm_sro.setCustomerPrice(NIDUtils.getNIDFinalPrice(theCost, pm_sro.getPracticeMaster(), payerMaster, nidPromo ));
		
		
		//out.print(Math.round(pm_sro.getCustomerPrice())+"<br>");
		//out.print(wiz);
		
		nvps.add(new org.apache.http.message.BasicNameValuePair("M_id", "839864946224"));
		nvps.add(new org.apache.http.message.BasicNameValuePair("M_key", "W7F3O2R9M9S5"));
		nvps.add(new org.apache.http.message.BasicNameValuePair("T_amt", String.valueOf(Math.round(pm_sro.getCustomerPrice())) ));
		//nvps.add(new org.apache.http.message.BasicNameValuePair("T_amt", "0.02"));
		nvps.add(new org.apache.http.message.BasicNameValuePair("T_code", "01"));
		nvps.add(new org.apache.http.message.BasicNameValuePair("C_name", request.getParameter("c_name")));
		nvps.add(new org.apache.http.message.BasicNameValuePair("C_address", request.getParameter("c_address")));
		nvps.add(new org.apache.http.message.BasicNameValuePair("C_city", request.getParameter("c_city")));
		nvps.add(new org.apache.http.message.BasicNameValuePair("C_state", request.getParameter("c_state")));
		nvps.add(new org.apache.http.message.BasicNameValuePair("C_zip", request.getParameter("c_zip")));
		nvps.add(new org.apache.http.message.BasicNameValuePair("C_cardnumber", request.getParameter("c_cardnumber")));
		nvps.add(new org.apache.http.message.BasicNameValuePair("C_exp", request.getParameter("c_exp")));//(MMYY)
		nvps.add(new org.apache.http.message.BasicNameValuePair("C_email", request.getParameter("c_email")));
		
		httpost.setEntity(new org.apache.http.client.entity.UrlEncodedFormEntity(nvps, org.apache.http.protocol.HTTP.UTF_8));
		org.apache.http.HttpResponse response2 = httpclient.execute(httpost);
	    org.apache.http.HttpEntity entity = response2.getEntity();
		
		String res = org.apache.http.util.EntityUtils.toString(entity);
		//String res = "FFF";
		
		
		
		//String res = "A133936APPROVED 133936                 18PA00CCICFXA9n0IDCICFXA9o0";//approved msg
		//String res = "X911911SECURITY VIOLATION              00P 0000000000000"//deny msg
		
		/*out.println("<textarea>"+res+"</textarea><br>");
		out.println("<br>Approval indicator: "+res.substring(1,2)+"<br>");	
		out.println("<br>code: "+res.substring(2,8)+"<br>");												
		out.println("<br>message: "+res.substring(8,40)+"<br>");
		//out.println("<br>internal field seperator: "+res.substring(40,42)+"<br>");
		out.println("<br>cvv indicator: "+res.substring(42,43)+"<br>");
		out.println("<br>avs indicator: "+res.substring(43,44)+"<br>");
		out.println("<br>risk indicator "+res.substring(44,46)+"<br>");
		out.println("<br>reference: "+res.substring(46,56)+"<br>");
		//out.println("<br>field separator: "+res.substring(56,57)+"<br>");		
		out.println("<br>order number: "+res.substring(57,res.length())+"<br>");*/
		
		bltNIM3_CommTrack NIM3_CommTrack = new bltNIM3_CommTrack();
		
		if (res.substring(1,2).equals("A")){
			out.print("<div class=\"span9\">");
			out.println("<h3>Thank you! Your card was approved.</h3> <p>Order Number: "+res.substring(57,res.length()-2)+"</p><p>We are in the process of notifying the imaging center of your reservation. You should receive a call from them within 2 business days to setup your specific appointment.</p><p>Thank you for choosing NextImage for your radiology needs!</p>");	
			
			//out.println("<h3>Thank you! Your card was approved.</h3> <p>Order Number: test</p><p>We will call within 1 business day to verify the procedure and book a specific appointment time.</p><p>To expedite your order, please have your physician's office fax your prescription to us at (888) 371-3302</p>");	
			out.print("</div>");
			
			NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(new Integer(request.getParameter("eID")),"load");
			
			bltCCardTransaction ccTrans = new bltCCardTransaction();
			ccTrans.setUniqueCreateDate(PLCUtils.getNowDate(false));
			ccTrans.setUniqueModifyDate(PLCUtils.getNowDate(false));
			ccTrans.setUniqueModifyComments("Sage Payment via NID");
			ccTrans.setTransactionDate(PLCUtils.getNowDate(false));
			ccTrans.setActionID(1);
			ccTrans.setCName(request.getParameter("c_name"));
			ccTrans.setCAddress(request.getParameter("c_address"));
			ccTrans.setCCity(request.getParameter("c_city"));
			ccTrans.setCState(request.getParameter("c_state"));
			ccTrans.setCZip(request.getParameter("c_zip"));
			ccTrans.setCCNumber(new Integer(request.getParameter("c_cardnumber").substring(request.getParameter("c_cardnumber").length()-4,request.getParameter("c_cardnumber").length())));
			ccTrans.setCEXP(request.getParameter("c_exp"));
			ccTrans.setOrderNumber(res.substring(57,res.length()-2));
			ccTrans.setAuthNumber(res.substring(8,40));
			ccTrans.setAmountCharged(  String.valueOf(new Double(Math.round(pm_sro.getCustomerPrice()))) );
			ccTrans.setEncounterID(new Integer(request.getParameter("eID")));
			ccTrans.commitData();
			myEO2.setReportSkip_RefDr(true);
			myEO2.getNIM3_Encounter().setTimeTrack_ReqRpReview(PLCUtils.getSubDate("na"));
			myEO2.getNIM3_Encounter().setTimeTrack_ReqRpReview_UserID(NID_SubmitCaseObject.NID_ORDERS_USER);
			myEO2.getNIM3_Encounter().setReportFileID(NIMUtils.EncounterBypassRXID);
			
			NID_SubmitCaseObject.updateCaseWithPayment(myEO2, "", res, pm_sro,ccTrans );
			
		}
		else{
			out.print("<div class=\"span11\">");
			out.println("<h3>We're sorry there appears to be a problem processing your card.</h3><p>If you would like to try again, please press the Start Over button</p>");
			
			NIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
			NIM3_CommTrack.setMessageName("Sage Payment via NID");
			NIM3_CommTrack.setMessageCompany ("Sage Payment via NID");
			NIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
			NIM3_CommTrack.setUniqueModifyComments("Created by Sage Payment via NID");
			NIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
			NIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
			
			NIM3_CommTrack.setCaseID(new Integer(request.getParameter("cID")));
			NIM3_CommTrack.setEncounterID(new Integer(request.getParameter("eID")));
			NIM3_CommTrack.setReferralID(new Integer(request.getParameter("rID")));
			
			NIM3_CommTrack.setExtUserID(new Integer(0));
			NIM3_CommTrack.setCommTypeID(new Integer(2));//1=message 2=sched     0 = bogus
	
			String commtrackMessageText = "CC: "+res.substring(8,40)+"<br>";
			commtrackMessageText += "Charge Attempt: $"+Math.round(pm_sro.getCustomerPrice());
			commtrackMessageText += "<br>Payment NOT processed.";
			NIM3_CommTrack.setMessageText(commtrackMessageText);
			NIM3_CommTrack.commitData();
			
			//test fake success
			if (request.getParameter("c_name").equalsIgnoreCase("0VfYDS_FY0GWkY0l9KOx31")){
				NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(new Integer(request.getParameter("eID")),"load");
				NID_SubmitCaseObject.fakeCaseUpdate(myEO2);
			}
			out.print("<button class=\"btn btn-primary\" onclick=\"startOver();\">Start Over</button>");
			out.print("</div>");
		}


/*		org.apache.http.client.ResponseHandler<String> responseHandler = new org.apache.http.impl.client.BasicResponseHandler();
        String responseBody = httpclient.execute(httpost, responseHandler);
        out.println(responseBody);		
*/		
 
%>