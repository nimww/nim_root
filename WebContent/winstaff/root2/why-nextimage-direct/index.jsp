<%
String pageTitle = "Why NextImage Direct";
%>
<%@ include file="/header.jsp" %>

<div class="container">
	<div class="row">
		<div class="span8">
			<h1>Why NextImage Direct?</h1>
			<br>
			<p>A better Image leads to a Better Diagnosis, Better Treatment,
				and a Better Recovery. We are not just about providing a discount
				MRI or discount CT. At NextImage, we provide you with a quality,
				credentialed nationwide network to give you quality care at
				affordable a price.</p>
			<p>
				NextImage Direct is perfect for those who are <strong>UNDERINSURED</strong>,
				have <strong>HIGH DEDUCTIBLE</strong> plans, or wish to <strong>SELF
					PAY</strong>.
			</p>
			<h4>
				<strong>We are not just about providing a discount MRI, CT or Ultrasound</strong>
			</h4>
          <p><i class="icon-play"></i> Cost savings of up to 75%
		  <br><i class="icon-play"></i> Access to over 3,000 credentialed imaging providers nationwide
		  <br><i class="icon-play"></i> 24-48 Hour Scheduling in your area
		  <br><i class="icon-play"></i> Quality diagnosis by certified radiologists
		</div>
		<jsp:include page="../sidebar.jsp">	
		<jsp:param value="zipSearch" name="zip" />	
		<jsp:param value="../img/MRI.png" name="picture" />
		<jsp:param value="whyNextImageDirect" name="head" />
		<jsp:param value="t1" name="t"/>
		</jsp:include>
	</div>
</div>
<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
<jsp:include page="../footer.jsp"></jsp:include>
