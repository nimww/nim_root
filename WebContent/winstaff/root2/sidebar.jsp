<div class="span4 ">
<div >
  <%if(request.getParameter("zip").equals("zipSearch")) {%>
	<div >
			<div class="backgroundHome">
				<form  action="/find-facilities-near-you/" method="post" id="findCenterForm" onSubmit="return findCenter();" >
			  <h3>Find Facilities Near You!</h3>
			  <p>
				<label for="zip">Your ZIP Code:</label>
				<input name="zipT" id="zipT" maxlength="5" onChange="valZip()" placeholder="ZIP" >
				<input type="hidden" name="zip" placeholder="ZIP">
			  </p>
			  <p>
				<label >Your Procedure:</label>
				<input type="radio" name="modality" checked value="MR">
				MRI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="modality" value="CT">
				CT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="modality" value="US">
				Ultrasound&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
			  <button type="submit" class="ButtonsNID" id="findCenterBtn" ><i class="icon-zoom-in icon-white"></i> Find a Center</button> 
			  </form>
		</div>
      </div>	 
  	 	
	<%} %>
	
	<img src="<%=request.getParameter("picture") %>" >
	
	<h3 class="sidebarH3">
		<%if (request.getParameter("head").equals("mri-header")) {%>
	  	  Testimonials!  	
	  	 <%} else if (request.getParameter("head").equals("ct-header")) { %>  
		  Testimonials!
	  	<%} else if (request.getParameter("head").equals("pet-ct-header")) { %>  
		  Testimonials
		  <%} else if (request.getParameter("head").equals("us-header")) { %>  
		  Testimonials!
		  <%} else if (request.getParameter("head").equals("contact")) { %>  
		  Testimonials! 		  
		   <%} else if (request.getParameter("head").equals("faq")) { %>  
		  Testimonials!
		  <%} else if (request.getParameter("head").equals("howItWorks")) { %>  
		  Testimonials!
		   <%} else if (request.getParameter("head").equals("whyNextImageDirect")) { %>  
		  Testimonials!
		  <%} else if (request.getParameter("head").equals("healthCarePros")) { %>  
		  Testimonials!
		  <%} else if (request.getParameter("head").equals("testimonials")) { %>  
		  Testimonials!
		  
		  <%} else if (request.getParameter("head").equals("general-terms-of-service")) { %>  
		  Testimonials!
		  <%} else if (request.getParameter("head").equals("securepaymentprocessing")) { %>  
		  Testimonials!
  		<%} %>
  </h3>	    
  
   	  
	<%if(request.getParameter("t").equals("t1")){ %> 
	  	<div class="zipBackground" >
		<p id="test"></p>
		</div>
	<%} %>
	
</div> 
</div> 
<script>
$(document).ready(function(){
	var test = new Array();

	test[0] = "Your company is providing the best service, bar none.  No one even comes close to the level of satisfaction I have had since beginning business with NextImage Medical.";
	test[1] = "I wanted to share with you my experience with NextImage Medical. I have all great things to say about the customer service I received from scheduling my appointment to having the MRI procedure done; all the staff and physicians were very professional and kind to me. I would recommend NextImage to all who are in need of having a diagnostic procedure performed.";
	test[2] = "I really trust and depend on your company.  Very happy with the service!";
	test[3] = "I am very pleased with you service and always recommend your company!";
	test[4] = "NextImage is very prompt, efficient and excels in customer service. I am very pleased with the service provided!";
	test[5] = "You do a fantastic job and your website is very easy to use!";
	test[6] = "Very friendly, accurate and pleasant personalities.";
	test[7] = "Your company is providing me the best service bar none. No one even comes close to the level of satisfaction I have had since beginning business with NextImage Medical.";

	function startSeq(i,iSpeed, iPause) {

	    $('#test').html(test[i]);
	    $('#test').stop().fadeIn(iSpeed,function() {
	        setTimeout(function() {
	            $('#test').stop().fadeOut(iSpeed,function() {
	                setTimeout(function() {
	                     if (i++ == test.length) i =0;
	                    startSeq(i,iSpeed,iPause); 
	                },0);
	            });
	        },iPause);
	    });
	}

	function startRandom(iSpeed, iPause) {
	    var i = Math.floor(Math.random()*test.length);
	    $('#test').html(test[i]);
	    $('#test').stop().fadeIn(iSpeed,function() {
	        setTimeout(function() {
	            $('#test').stop().fadeOut(iSpeed,function() {
	                setTimeout(function() {
	                     if (i++ == test.length) i =0;
	                    startRandom(iSpeed,iPause); 
	                },0);
	            });
	        },iPause);
	    });
	}

	//startSeq(0,1000,1000);
	startRandom(4000,12000);
});
</script>