<%@ include file="header.jsp"%>




<!--Created by Giovanni Hernandez 06-27-13-->
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ include file="header.jsp"%>
<form:form method="POST" commandname="ReferCase" id="referCase" action="referCase" enctype="multipart/form-data" >
<div class="container">

<div class="div" style="margin-bottom:2%">
	<div class="row-fluid referCaseDivBar">					
		<div class="uploadDiv">
			<h4 class="uploadH4" href="#"  data-toggle="tooltip-top" title="Documents include RX, Authorization, Patient Demographics">Upload Documents for Quick Referral!</h4>														
		</div>						
	</div>
</div>
<strong>Quick Form Docs:</strong><br />
<ul class="dl-horizontal" style="margin-left:3%">

<strong>
<li> 
	Please upload a copy of RX, patient demographics, and an authorization if available.
</li>
<li>
	Symbol denotes required fields or documents for quick referral.
</li>

<li>
<i class="divTPA2">NOTE: </i> If no file is attached form will not be submitted. Upon successful submission, a "Thank you for submitting referral!" pop up will appear.
</li>
</strong>
</ul>

<div class="row-fluid">	
	<div>
	<span class="redText">&nbsp;Upload Rx</span>
	<form:input path="fileRx" type="file"
	accept="application/pdf,image/tiff" />
	<span class="redText">&nbsp;Upload Other Doc's</span>
	<form:input path="fileOther" type="file"
	accept="application/pdf,image/tiff" />
	</div>
</div>
 
<div class="div referCaseDivBar" >
	<div class="row-fluid uploadDiv">					
		<div>
			<h4 href="#"  data-toggle="tooltip-top" title="Documents include RX, Authorization, Patient Demographics" class="margin-left">Insurance Company/TPA</h4>															
		</div>						
	</div>
</div>
 
<div class="row-fluid">

<div Class="row-fluid">
	<div class="span3 margin-left" id="longForm">
		<strong><p class="divTPA1" style="margin-left:3%;"> Claim Number: </p></strong>
		<strong><p class="divTPA1" style="margin-left:4%;"> Ins/TPA-Carrier Branch:</p></strong>
		<strong><p class="divTPA2" style="margin-left:5%;"> Link Me As: </p></strong>
		<strong><p class="divTPA3" style="margin-left:5%;"> Adjuster/NCM First Name: </p></strong>
		<strong><p class="divTPA4" style="margin-left:5%;"> Adjuster/NCM Last Name: </p></strong>	
		
		
		
	</div>
	
	<div class="span3 margin-left" id="longForm" >
		<form:input  path="CaseClaimNumber1"  type="text"  cssClass="required margTop4" placeholder="Claim Number" />
		<form:input cssClass="noNumb required" path="AdjusterBranch" type="text" placeholder="Adjuster Branch" />	
		<form:select  path="LinkType" >
			<form:options items="${LinkType}" />
		</form:select>
		<form:input  cssClass="refName noNumb required block" path="refFirstName"  type="text" placeholder="Adjuster/NCM First Name" />
			<span id="error" name="error"></span>
		<form:input  cssClass="refName noNumb required" path="refLastName" type="text"  placeholder="Adjuster/NCM Last Name" />	
		
		
		
	</div>
	
	<div class="span3 margin-left2" id="longForm" >
		<strong><p style="margin-top:7%; margin-left:2%;color:red"> Patient First Name: </p></strong>		
		<strong><p style="margin-top:8%; color:red; margin-left:2%"> Patient Last Name: </p></strong>				
		<strong><p  style="margin-top:10%;margin-left:2%;"> Adjuster/NCM Phone:</p></strong>
		<strong><p  style="margin-top:8%;margin-left:2%;"> Adjuster/NCM Fax:</p></strong>
		<strong><p name="refEmail" style="margin-top:10%; margin-left:2%"> Adjuster Email:</p></strong>
		
	</div>
	
	<div class="span3" id="longForm">
		
		<form:input cssClass="required noNumb required " path="PatientFirstName" type="text" cssStyle="margin-top:5%;" placeholder="Patient First Name" />
		<form:input cssClass="required noNumb required" path="PatientLastName" type="text" placeholder="Patient Last Name" />					
		<form:input type="text"  path="AdjusterPhone1" value="" placeholder="XXX" cssClass="lbs area0 " cssStyle="width:21%;" maxlength="3" onkeyup="tab(this,'pre0')" /><span>-</span>						
							
		<form:input type="text"  path="AdjusterPhone2" value="" placeholder="XXX" cssClass="lbs pre0 " cssStyle="width:24%;" maxlength="3" onkeyup="tab(this,'exch0')"/><span>-</span>
						
		<form:input type="text"  path="AdjusterPhone3" value="" placeholder="XXXX" cssClass="lbs exch0 " cssStyle="width:27%;" maxlength="4" onkeyup="tab(this,'area')" />
						
								
		<form:input type="text"  path="AdjusterFax1" value="" placeholder="XXX" cssClass="lbs area " cssStyle="width:21%;" maxlength="3" onkeyup="tab(this,'pre')" /><span>-</span>
						
		<form:input type="text"  path="AdjusterFax2" value="" placeholder="XXX" cssClass="lbs pre " cssStyle="width:24%;" maxlength="3" onkeyup="tab(this,'exch')" /><span>-</span>
						
		<form:input type="text"  path="AdjusterFax3" value="" placeholder="XXXX" cssClass="lbs exch " cssStyle="width:27%;" maxlength="4" onkeyup="tab(this,'tabbedTo')" />
						
		
		<form:input cssClass="tabbedTo" path="AdjusterEmail" type="text"  onchange="emailVal();" placeholder="Adjuster email" />	
						
	</div>
	
</div>

 <div class="div hideMe" style="margin-bottom:3%; margin-top:3%; ">
	<div class="row-fluid" style="background-image:linear-gradient(to bottom, #ffffff, #f2f2f2);  border:1px solid#d4d4d4; -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px; -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.065); box-shadow: 0 1px 4px rgba(0, 0, 0, 0.065); " >
		<div class="span5" style="margin-left:1%;">						
			<h4 style="color:#777777">Patient/Claimant Information</h4>					
		</div>
		<div class="span5" style="margin-left:9%" >						
			<h4 style="color:#777777">Optional Info to Expedite Scheduling</h4>					
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span3" id="longForm" style="margin-left:1%" >
		<strong><p class="hideMe" style="margin-top:2%">Is Stat: </p></strong>
		<strong><p style="margin-top:6%;" >Patient First Name: </p></strong>
		<strong><p style="margin-top:9%;">Patient Last Name: </p></strong>							
		<strong><p class="hideMe" style="margin-top:7%;">DOB: </p></strong>
		<strong><p class="hideMe" style="margin-top:8%"></i>SSN: <i style="color:#2f96b4">(Numbers Only)</i></p></strong>
		<strong><p class="hideMe" style="margin-top:7%">Date of Injury: </p></strong>
		<strong><p class="hideMe" style="margin-top:9%">Address: </p></strong>
		<strong><p class="hideMe" style="margin-top:10%">City: </p></strong>
		<strong><p class="hideMe" style="margin-top:10%">State: </p></strong>
		<strong><p class="hideMe" style="margin-top:8%">Zip Code: </p></strong>
		<strong><p class="hideMe" style="margin-top:10%">Home Phone: </p></strong>
		<strong><p class="hideMe" style="margin-top:8%">Mobile Phone: </p></strong>
	</div>
	
	<div class="span3" id="longForm" style="margin-left:0%" >
	
	<style>
		span.yesNo label{
			display:inline;
			margin: 0 5px 0;
		}
	</style>
	<span class="yesNo hideMe"></span>
		<div class="span9 hideMe"><form:radiobuttons path="stat" items="${yesNo}" type="radio" element="span class='yesNo'" /><br /></div>		
		
		
		<form:input cssClass="required noNumb" path="PatientFirstName" type="text"  placeholder="Patient First Name" />
		<form:input cssClass="required noNumb" path="PatientLastName" type="text" placeholder="Patient Last Name" />
		<form:input cssClass="required hideMe" path="PatientDOB" type="text" readonly="true" placeholder="MM/DD/YYYY" cssStyle="cursor:pointer; background-color:white" />
		<form:input cssClass="lbs hideMe" path="PatientSSN" type="text" maxlength="9" placeholder="ie. 123456789" />
		<form:input cssClass="required hideMe" path="PatientDOI" type="text" readonly="true" placeholder="MM/DD/YYYY" cssStyle="cursor:pointer; background-color:white"/>
		<form:input cssClass="required hideMe" path="PatientAddress1" id="pAdrs" type="text" placeholder="Address" />
		<form:input cssClass="required noNumb hideMe" path="PatientCity" type="text" placeholder="City" />							
		<form:select cssClass="span5 hideMe" path="PatientStateID">
			<form:options items="${stateList}"></form:options>										
		</form:select>	
		
								
			<form:input cssClass="required lbs hideMe" path="PatientZip" type="text" maxlength="5" onChange="valZip()" placeholder="Zip code" />
												
			<form:input type="text" path="PatientHomePhone1" value="" placeholder="XXX" cssClass="lbs area1 required hideMe" cssStyle="width:21%;" maxlength="3" onkeyup="tab(this,'pre1')" /><span class="hideMe">-</span>
								
			<form:input type="text" path="PatientHomePhone2" value="" placeholder="XXX" cssClass="lbs pre1 required hideMe" cssStyle="width:24%" maxlength="3" onkeyup="tab(this,'exch1')" /><span class="hideMe">-</span>
								
			<form:input type="text" path="PatientHomePhone3" value="" placeholder="XXXX" cssClass="lbs exch1 required hideMe"  cssStyle="width:27%;" maxlength="4" onkeyup="tab(this,'area2')" />						
											
			<form:input type="text" path="PatientMobilePhone1" value="" placeholder="XXX" cssClass="lbs area2 hideMe" cssStyle="width:21%;" maxlength="3" onkeyup="tab(this,'pre2')" /><span class="hideMe">-</span>
								
			<form:input type="text" path="PatientMobilePhone2" value="" placeholder="XXX" cssClass="lbs pre2 hideMe" cssStyle="width:24%;" maxlength="3" onkeyup="tab(this,'exch2')"/><span class="hideMe">-</span>
								
			<form:input type="text" path="PatientMobilePhone3" value="" placeholder="XXXX" cssClass="lbs exch2 hideMe" cssStyle="width:27%;" maxlength="4" onkeyup="tab(this,'tabbedFrom2')" />			
	</div>	
	
	<div class="span3 hideMe" id="longForm">	
		<strong><p class="hideMe" style="margin-top:2%">Employer: </p></strong>
		<strong><p class="hideMe" style="margin-top:10%">Employer Phone: </p></strong>							
		<strong><p class="hideMe" style="margin-top:6%">Is Claustrophobic: </p></strong>
		<strong><p class="hideMe" style="margin-top:7%">Patient Height: </p></strong>							
		<strong><p class="hideMe" style="margin-top:8%">Patient Weight: </p></strong>
		<strong><p class="hideMe" style="margin-top:10%">Is Pregnant: </p></strong>
		<strong><p class="hideMe" style="margin-top:8%">Has Metal in Body: </p></strong>
		<strong><p class="hideMe">Date of Metal in Body: </p></strong>
		<strong><p class="hideMe" style="margin-top:10%">Has Kidney, Liver, Hypertension, Diabetic Conditions any or All </p></strong><br />
	</div>

	<div class="span3 hideMe" id="longForm">
			<form:input cssClass="noNumb tabbedFrom2 hideMe" path="EmployerName" type="text" placeholder="Employer"/>
							
			<form:input type="text" path="EmployerPhone1" value="" placeholder="XXX" cssClass="lbs area3 hideMe" cssStyle="width:21%; " maxlength="3" onkeyup="tab(this,'pre3')" /><span>-</span>
							
			<form:input type="text" path="EmployerPhone2" value="" placeholder="XXX" cssClass="lbs pre3 hideMe" cssStyle="width:24%;" maxlength="3" onkeyup="tab(this,'exch3')" /><span>-</span>
							
			<form:input type="text" path="EmployerPhone3" value="" placeholder="XXXX" cssClass="lbs exch3 hideMe" cssStyle="width:27%;" maxlength="4" onkeyup="tab(this,'tabbedFrom3')" />
						
			<span class="yesNo hideMe"></span>
			<div class="span9 hideMe"><form:radiobuttons class="tabbedFrom3" path="claust" items="${yesNo}" type="radio" element="span class='yesNo'" /><br /></div>
		
			
			<form:select cssClass="span5 hideMe" path="height">
                 <form:options items="${heightList}" />
			</form:select><br />							
			<form:input path="weight" cssClass="input-mini lbs hideMe" type="text"  maxlength="3" placeholder="lbs" /><br />			
			
						
			<span class="yesNo hideMe"></span>
			<div class="span9 hideMe"><form:radiobuttons path="prego" items="${yesNo}" type="radio" element="span class='yesNo'" /><br /></div>
			
						
			<span class="yesNo hideMe"></span>
			<div class="span9 hideMe"><form:radiobuttons path="metal" items="${yesNo}" type="radio" element="span class='yesNo'" /><br /></div>		
			
			<form:input path="DOM" cssClass="noNumb hideMe" type="text" cssStyle="margin-top:5%;margin-bottom:5%; cursor:pointer; background-color:white" readonly="true" placeholder="MM/DD/YYYY" /><br />			
						
			<span class="yesNo hideMe"></span>
			<div class="span9 hideMe"><form:radiobuttons path="diseases" items="${yesNo}" type="radio" element="span class='yesNo'" /></div>
	</div>	
	
</div>

	<div class="div hideMe" style="margin-bottom:3%; margin-top:3%; background-image:linear-gradient(to bottom, #ffffff, #f2f2f2);  border:1px solid#d4d4d4; -webkit-border-radius: 4px; border-radius: 4px; -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.065); box-shadow: 0 1px 4px rgba(0, 0, 0, 0.065);"">
		<div class="row-fluid" style="color:white">					
			<div style="margin-top:1%" style="color:white">
				<h4 href="#"  data-toggle="tooltip-top" title="Documents include RX, Authorization, Patient Demographics" style="margin-left:1%;color:#777777">Referring/Treating Physician and Test Information</h4>			
			</div>						
		</div>
	</div>

<div class="row-fluid hideMe">
	<div class="span3" style="margin-left:1%" >
		<strong><p style="margin-top:2%">MD Provider: </p></strong>
		<strong><p style="margin-top:7%">MD Phone: </p></strong>
		<strong><p style="margin-top:7%">MD Fax: </p></strong>						
	</div>
	
	<div class="span3" style="margin-left:0%">								
						
		<form:input path="MDName" cssClass="required noNumb"  id="MDname" placeholder="Doogie Howser, MD" />						
							
		<form:input type="text" path="MDPhone1" value="" placeholder="XXX" id="MDPhone1" cssClass="lbs area4 required" cssStyle="width:21%;" maxlength="3" onkeyup="tab(this,'pre4')" /><span>-</span>
							
		<form:input type="text" path="MDPhone2" value="" placeholder="XXX" id="MDPhone2" cssClass="lbs pre4 required" cssStyle="width:24%" maxlength="3" onkeyup="tab(this,'exch4')" /><span>-</span>
							
		<form:input type="text" path="MDPhone3" value="" placeholder="XXXX" id="MDPhone3" cssClass="lbs exch4 required" tabindex="9" cssStyle="width:27%" maxlength="4" onkeyup="tab(this,'area5')" />											
		
		<form:input type="text" path="MDFax1" value="" placeholder="XXX" id="MDFax1" cssClass="lbs area5 required" cssStyle="width:21%" maxlength="3" onkeyup="tab(this,'pre5')" /><span>-</span>
							
		<form:input type="text" path="MDFax2" value="" placeholder="XXX" id="MDFax2" cssClass="lbs pre5 required" cssStyle="width:24%" maxlength="3" onkeyup="tab(this,'exch5')" /><span>-</span>
							
		<form:input type="text" path="MDFax3" value="" placeholder="XXXX" id="MDFax3" cssClass="lbs exch5 required" cssStyle="width:27%" maxlength="4" onkeyup="tab(this,'tabbedFrom5')" />
										
	</div>
	
	<div class="span3">
		<strong><p style="margin-top:2%">Follow-up Appt: </p></strong>
		<strong><p style="margin-top:9%">MD email: </p></strong>
	</div>
	
	<div class="span3">						
		<form:input path="FollowUpDate" type="text" cssClass="tabbedFrom5" readonly="true" placeholder="mm/dd/yyyy" cssStyle="cursor:pointer; background-color:white" />
		<form:input path="MDEmail" id="ReferringPhysicianEmail" type="text"  placeholder="EMAIL" />
	</div>
	
</div>
<hr />
<div class="row-fluid hideMe" style="margin-left:1%; margin-top:2%">
	<div class="span2">						
	   <strong><p>Procedure:</strong>					
	</div>		
				
	<div class="span2">				
		<form:select path="proc" cssClass="span11">
            <form:options items="${procList}" />
		</form:select><br />
	</div>
				
	<div class="span2">					
	   <strong><p>Body Part:</p></strong>						
	</div>
	<div class="span2">						
	   <form:select path="bp" cssClass="span11">
			<form:options items="${bpList}" />							
		</form:select><br />						
	</div>		
				
	<div class="span2">						
	   <strong><p>Rule Out:</p></strong>						
	</div>
				
	<div class="span2">					
	   <form:select path="RuleOut" cssClass="span11">
			<form:options items="${ruleList}" />								
		</form:select><br />							
	</div>
</div>

<div class="row-fluid hideMe" style="margin-top:3%; margin-left:1%">
	<div class="span2" style="margin-top:1%">
		<strong><p style="margin-top:-6%;">Hand Carry CD</p></strong>
	</div>
	<div class="span2">
				
			<span class="yesNo"></span>
			<div class="span10"><form:radiobuttons path="cd" items="${yesNo}" type="radio" element="span class='yesNo'" /><br /></div>		
	</div>
	<div class="span2" style="margin-top:1%">
		<strong><p style="margin-top:-6%;">Hand Carry Films</p></strong>
	</div>
	<div class="span2">				
			<span class="yesNo"></span>
			<div class="span10"><form:radiobuttons path="films" items="${yesNo}" type="radio" element="span class='yesNo'" /><br /></div>		
	</div>					
	<div class="span2" style="margin-top:1%">
		<strong><p style="margin-top:-6%;">Age Report</p></strong>
	</div>
	<div class="span2">					
			<span class="yesNo"></span>
			<div class="span10"><form:radiobuttons path="report" items="${yesNo}" type="radio" element="span class='yesNo'" /><br /></div>		
	</div>									
</div>

<div class="row-fluid" style="margin-left:1%;margin-top:4%">
	<div class="span3">
		<strong><p>Notes or special Requests:</p></strong>
	</div>
	<div>
		<form:textarea path="MessageText" cssStyle="width:98%; height:65px; background-color:#F9F6F4; resize:none"></form:textarea>
	</div>					
											
</div>


<div>
	<a class="btn" role="button" type="button" id="submitReferral" value="submit" onclick="test()" style="margin-left:1%; color:#336699">Send Referral!</a>
</div>

</div>
</div>

</form:form>
<%@ include file="footer.jsp"%>










<!-- <style>
	.commentBackground{/* IE10 Consumer Preview */ 
background-image: -ms-linear-gradient(top, #f2f2f2 0%, #f2f2f2 100%);

/* Mozilla Firefox */ 
background-image: -moz-linear-gradient(top, #f2f2f2 0%, #ffffff 100%);

/* Opera */ 
background-image: -o-linear-gradient(top, #f2f2f2 0%, #ffffff 100%);

/* Webkit (Safari/Chrome 10) */ 
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #f2f2f2), color-stop(1, #ffffff));

/* Webkit (Chrome 11+) */ 
background-image: -webkit-linear-gradient(top, #f2f2f2 0%, #ffffff 100%);

/* W3C Markup, IE10 Release Preview */ 
background-image: linear-gradient(to bottom, #f2f2f2 0%, #ffffff 100%);

border-radius: 4px 4px 4px 4px;

filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='#ffffff', endColorstr='#f2f2f2');/*For IE7-8-9*/ 
height: 1%;/*For IE7*/
}

.commentMainDiv{
	margin-top:20px;
	padding-top:20px;
}

.h4{
	margin-left:2%;
}
.fnDiv2{
	width:37%; margin-top:1%; margin-left:3%
}

.commPhone{
	width:9%; 
	margin-left:10px;
}

.commPhone{
	width:9%; 
	margin-left:10px;
}

.ta{
	margin-left:6%; 
	width:90%;
}
.sub{
	  margin-left:48px; 
	  margin-top:2%;
}
.commBoxDiv{
	margin-top:3%;
}
.str{
	margin-left:7%;
	}
.str2{
	margin-left:15%;
	}
.div2{
	margin-left:3%;
}
.margL{
	margin-left: 2%;
}
.lnDiv2{
	width:35%; margin-top:1%; margin-left:2%
}
</style>

<script>
	function val(){
		if($('.validate') == '' ){
			
		}
	}
</script>

<div class="container" >
	
	  <div class="row">
	  <form class="form-search" onsubmit="test(); return false; " name="form" action="" method="post">
		<div>
		<div class="row-fluid" style="background-image:linear-gradient(to bottom, #ffffff, #f2f2f2); border:1px solid#d4d4d4; -webkit-border-radius: 4px; border-radius: 4px; -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.065); box-shadow: 0 1px 4px rgba(0, 0, 0, 0.065);"><!-- Case Account rgb(102,204,255)-->
                        <!-- <h4 class="text-center" style="color:#777777">Comments or Questions? We would love to hear from you!</h4>
                    </div> 
			<div class="span10 offset1" >
			 					
					<div class="commentBackground commentMainDiv">
					<!-- <h4 class="h4">Comments or Questions</h4>-->
					
					<!-- <div class="row row-fluid div2">
					First: <input class="lbs fnDiv2 validate required noNumb" name="fName" type="text"  placeholder="John" />
					Last: &nbsp;&nbsp;<input class="lbs lnDiv2 contactForm required noNumb" name="lName" type="text" placeholder="Doe" />		
					</div>
					<div class="row row-fluid" style="margin-top:2%; margin-left:3%">
						email: <input class="validate required" type="text" id="AdjusterEmail" name="contactEmail" style="margin-left:2%; width:37%" placeholder="email" />		
						Phone: &nbsp;<input class="noNumb contactForm required commPhone lbs required" onchange="emailVal();" type="text" style="margin-left:1%; width:9%" name="area" maxlength="3" onkeyup="tab(this,'pre')" />-<input class="noNumb pre required lbs" name="pre" type="text" style="width:9%" maxlength="3" onkeyup="tab(this,'exch')" />-<input class="noNumb exch required lbs" name="exch" type="text" style="width:12%" maxlength="4" onkeyup="tab(this,'ta')" />
					</div><br />
					
					
					<div class="row-fluid">
					
					<div class="span4 " >
						<strong class="str2">Preferred method of contact:</strong>
					</div>	
					<div class="span4 ">
						email: &nbsp;<input type="checkbox" value="true" name="email" checked />&nbsp; &nbsp;phone: &nbsp;<input name="phone" value="true" type="checkbox" />
					</div>
									
					</div>
					
					
					
					
					
					
					
						
					<div class="row commBox">
						<strong class="str">Leave comments or questions below</strong>
						<textarea class="ta" name="ta" rows="5" ></textarea>	
						<button type="button" class="ButtonsNID sub" >Submit</button>
						<div class="commBoxDiv">
						
							<div class="text-center">
								<small><strong >NextImage Medical</strong></small><br />							
								<small><strong >3390 Carmel Mountain Rd #150 San Diego, CA 92121</strong></small><br />							
								<small><strong >Ph: 888-693-8867 Fax: 888-371-3302</strong></small>
							</div>
						
						</div>
						
						
					</div>
					
					
					</div>
					 
			</div>
			
		</div>
		
		 </form> 
			
		
		
	  </div>
	
</div>-->