 * CREATE TABLE Files_also (
    id       SERIAL      PRIMARY KEY,
    filename VARCHAR(64) NOT NULL,
    filesize INT         NOT NULL,
    data     BYTEA       NOT NULL,
    created  TIMESTAMP   NOT NULL );
 * 
 *
 * CREATE TABLE Files (
    id       SERIAL      PRIMARY KEY,
    filename VARCHAR(64) NULL,
    data     BYTEA       NULL);
 * 