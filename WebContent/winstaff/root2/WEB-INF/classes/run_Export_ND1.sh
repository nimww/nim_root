#!/bin/bash  
   
#
# This script fires the fax followup procedure.
#

cd /var/lib/tomcat5/webapps/ROOT/WEB-INF/classes/


JAVA_HOME=/usr
JARPATH=/var/lib/tomcat5/webapps/ROOT/WEB-INF/lib/
CP_PATH=/var/lib/tomcat5/webapps/ROOT/WEB-INF/classes/

CLASSPATH=$CP_PATH/:.:$JARPATH/apache-mime4j-0.6.jar\
:$JARPATH/catalina-root.jar\
:$JARPATH/commons-codec-1.3.jar\
:$JARPATH/commons-fileupload-1.2.1.jar\
:$JARPATH/commons-io-1.4.jar\
:$JARPATH/commons-lang3-3.1.jar\
:$JARPATH/commons-logging-1.1.1.jar\
:$JARPATH/dsn.jar\
:$JARPATH/edi-editor-1.6.5-SNAPSHOT.jar\
:$JARPATH/edi-editor-2.0-SNAPSHOT.jar\
:$JARPATH/edireader-4.7.6-SNAPSHOT.jar\
:$JARPATH/ediwriter-4.7.6-SNAPSHOT.jar\
:$JARPATH/gson-1.7.1.jar\
:$JARPATH/httpclient-4.0.1.jar\
:$JARPATH/httpcore-4.0.1.jar\
:$JARPATH/httpmime-4.0.1.jar\
:$JARPATH/imap.jar\
:$JARPATH/itext-1.4.7.jar\
:$JARPATH/jFdfTk.jar\
:$JARPATH/joda-time-1.6.2.jar\
:$JARPATH/mail.jar\
:$JARPATH/mailapi.jar\
:$JARPATH/mysql-connector-java-5.1.12-bin.jar\
:$JARPATH/poi-3.6-20091214.jar\
:$JARPATH/pop3.jar\
:$JARPATH/postgresql-9.1-902.jdbc4.jar\
:$JARPATH/smtp.jar\
:$JARPATH/deploy.jar

$JAVA_HOME/bin/java -cp $CLASSPATH com.winstaff.NIM_Export_ND1 -cp
   
exit 0 
