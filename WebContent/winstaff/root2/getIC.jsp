<%@page contentType="text/html" language="java" import="java.text.DecimalFormat ,com.winstaff.*" %>
<%
int iRange = Integer.parseInt(request.getParameter( "iRange" ));
String myCPT = "72148";

String myZIP = null;
java.util.Vector<PracticeMaster_SearchResultsObject> vZIP = null;
java.util.Iterator itZIP = null;
String searchNotes = "";
String isClaustrophobic = request.getParameter("isClaustrophobic");
String myCPTwizard = null;
String bp = null;
if ( request.getParameter( "ssZIP" )!=null) {
	myZIP = request.getParameter( "ssZIP" );
	myCPTwizard = request.getParameter( "cptWizardID" );
	
	searchDB2 conn = new searchDB2();
	String query = "select cpt1, bp1 from tcptwizard where cptwizardid = "+myCPTwizard;
	java.sql.ResultSet rs = conn.executeStatement(query);
	
	while (rs.next()){
		myCPT = rs.getString("cpt1");
		bp = rs.getString("bp1");
	}
	out.print("<span class='btn btn-primary'><i class='icon-list-alt icon-white'></i> Listing centers for: "+bp+"</span><br><br>");
	conn.closeAll();
} else {
	myZIP = "92037";
}
		
String myCounty = NIMUtils.getCounty(myZIP);
	
		
CPTGroupObject2 myCPTO2_full =  new CPTGroupObject2 (myCPT,"");
		
try{
	ImagingCenterListObject myImagingCenterListObject = NIMUtils.getListOfCenters(myZIP, iRange,myCPTO2_full, new com.winstaff.ContactAddressObject("","", "", myZIP), null, 9999.00, false, 800.00, false);
	searchNotes = myImagingCenterListObject.getSearchNotes_Text("<hr>");
	vZIP = myImagingCenterListObject.getListOfCenters();
}
catch (Exception eeee){
%>


<h2>No Matching Centers</h2>
<%}
	
int i2cnt=0;
if (vZIP==null){%>
<h2>Please Try Your Search Again.</h2>
<%}
else{%>
<table width="100%">
  <tr>
    <th align="left"> Center Location </th>
    <th> Standard Price </th>
    <th> Your Price </th>
    <th> Your Savings </th>
    <th>&nbsp; </th>
  </tr>
  <%
        for (int iVect=0; iVect<vZIP.size(); iVect++) 
        {
            i2cnt++;
            PracticeMaster_SearchResultsObject myPracM_SRO = (PracticeMaster_SearchResultsObject) vZIP.get(iVect);
            String sNewDistance = "Greater than 90 mi";
            int maxClass = 1;
            if (myPracM_SRO.getTradMR_Class()>maxClass)
            {
                maxClass = myPracM_SRO.getTradMR_Class();
            }
            else if (myPracM_SRO.getOpenMR_Class()>maxClass)
            {
                maxClass = myPracM_SRO.getOpenMR_Class();
            }
            try
            {
                Double myDistanceD = myPracM_SRO.getDistance();
                
                if (myDistanceD<=10) {
                    sNewDistance = "Less than 10 mi";
                } 
                else if (myDistanceD<=15) {
                    sNewDistance = "Less than 15 mi";
                } 
                else if (myDistanceD<=20) {
                    sNewDistance = "Less than 20 mi";
                } 
                else if (myDistanceD<=30) {
                    sNewDistance = "Less than 30 mi";
                }
                else if (myDistanceD<=60) {
                    sNewDistance = "Less than 60 mi";
                }
                else if (myDistanceD<=90) {
                    sNewDistance = "Less than 90 mi";
                }
        
            }
            catch (Exception eeeeee)
            {
                sNewDistance = "Less than 20 miles";
            }
            
            Double dPrice = NIMUtils.getCPTGroupPayPriceforPracticeMaster(myPracM_SRO.practiceMaster, myCPTO2_full, PLCUtils.getToday());
            String sPrice = "Please Call for Special Pricing";
            if (dPrice!=null)
            {
                dPrice = dPrice.doubleValue() * NIMUtils.NID_MARK_UP;  
                if (dPrice>200.00){
                    sPrice = Double.toString(dPrice);
                }
            }
            %>
  <%if (vZIP.size()!=0){ %>
    <tr <%if(myPracM_SRO.getOpenMR()){%>class="openMRI"<%}else {%>class="closedMRI"<%}%> 
				<%if (isClaustrophobic.equals("1")&&myPracM_SRO.getOpenMR()){%> 
				
				<%} else if (isClaustrophobic.equals("1")&&!myPracM_SRO.getOpenMR()){%>	
					style="display:none;"
				<%} else if(myPracM_SRO.getOpenMR()){%>
					style="display:none;"
				<%}%>
			>
  
  
    <td style="width:384px;"><p style="padding: 0;margin: 0;font-weight: bold;font-size: 16px;width:364px;"> <%=PLCUtils.toProperCase(myPracM_SRO.practiceMaster.getOfficeCity(),true)%> </p>
      <%=sNewDistance%> from <%=myZIP%><br /></td>
    <td style="text-align:center;font-size:18px;color: #727272;"><%
                java.util.Random r = new java.util.Random();
                double randomValue = 1553 + (1712 - 1553) * r.nextDouble();
                out.print("$"+Math.round(randomValue));
                %></td>
    <td style="text-align:center;font-size:19px;font-weight:500;"><%="$"+Math.round(Double.parseDouble(sPrice))%></td>
    <td style="text-align:center;font-size:19px;font-weight:500;color: #2DB402;"><%=Math.round(100-((Double.parseDouble(sPrice)/randomValue)*100))%>% </td>
    <td><form class="reserve" action="/reserve-facilities/" method="post">
        <input type="hidden" name="myCPTwizard" value="<%=myCPTwizard%>">
        <input type="hidden" name="ic_code" value="<%=myPracM_SRO.practiceMaster.getPracticeID()%>">
        <input type="hidden" name="myZIP" value="<%=myZIP%>">
        <input type="hidden" name="custom" value="n">
        <input type="submit" class="btn" value="Reserve">
      </form></td>
  </tr>
  <%}
            else{%>
  <tr>
    <td colspan="6" align="center"><h3>No results</h3></td>
  </tr>
  <%}%>
  <%}%>
</table>
<br>
<p>*Price not guaranteed until procedure is verified and payment is received.  Reserve now!</p>
<%}%>
