<%
String pageTitle = "How it Works";
%>
<%@ include file="/header.jsp" %>

<script>
function test(){				
	if($('.contactForm').val() !='' ){		
		document.getElementById('index').submit();
		}					
		else{
		
			var isValid = true;
				if($('.contactForm').val()==''){
					$('input[type="text"].required').each(function(){
						if($.trim($(this).val()) == ''){
							isValid = false;
							$(this).css({
								"border":"1px solid red",							
							});
						}
						else{
							$(this).css({
								"border":"",							
							});
						}
					});
				}
				else{								
						if($(".required").val().length <= 0){
								isValid = false;
								$(".required").css({
									"border":"1px solid red",
								});						
							
						}
										
							
						}
					}							
				if(isValid == false){
					return false;
					}
					else{												
						document.getElementById('index').submit();
						return true;
						}
				
		}
		
	
</script>

<div class="container" >
	
	  <div class="row">
	  <form class="form-search" onsubmit="test(); return false; " name="form" action="/contactConfirm.jsp" method="post">
		<div>
			<div class="span8" >
			 					
					<div class="commentBackground commentMainDiv">
					<h4 class="h4">Comments or Questions</h4>
					
					<div class="row row-fluid div2">
					First: <input class="lbs fnDiv2 contactForm required" name="fName" type="text"  placeholder="John" />
					Last: &nbsp;&nbsp;<input class="lbs lnDiv2 contactForm required"  name="lName" type="text" placeholder="Doe" />		
					</div>
					<div class="row row-fluid" style="margin-top:2%; margin-left:3%">
						email: <input class="contactForm required" type="text" id="contactEmail" name="contactEmail" style="margin-left:2%; width:33%" placeholder="email" />		
						Phone: <input class="noNumb contactForm required" type="text" name="area"style="width:9%; margin-left:1%" maxlength="3" onkeyup="tab(this,'pre')" />-<input class="noNumb pre required" name="pre" type="text" style="width:9%" maxlength="3" onkeyup="tab(this,'exch')" />-<input class="noNumb exch required" name="exch" type="text" style="width:9%" maxlength="4" onkeyup="tab(this,'ta')" />
					</div><br />
					
					
					<div class="row-fluid">
					
					<div class="span4 " >
						<strong class="str">prefered method of contact:</strong>
					</div>	
					<div class="span4 ">
						email: &nbsp;<input type="checkbox" value="true" name="email" checked />&nbsp; &nbsp;phone: &nbsp;<input class="commPhone" name="phone" value="true" type="checkbox" />
					</div>
									
					</div>
					
					
					
					
					
					
					
						
					<div class="row commBox">
						<strong class="str">Leave comments or questions below</strong>
						<textarea class="ta" name="ta" rows="5" ></textarea>	
						<button type="submit" class="ButtonsNID sub" >Submit</button>
						<div class="commBoxDiv">
						
							<div class="text-center">
								<small><strong >NextImage Direct</strong></small><br />							
								<small><strong >3390 Carmel Mountain Rd #150 San Diego, CA 92121</strong></small><br />							
								<small><strong >Ph: 888-693-8867 Fax: 888-371-3302</strong></small>
							</div>
						
						</div>
						
						
					</div>
					
					
					</div>
					 
			</div>
			
		</div>
		
		 </form> 
			
		
		<div class="pull-right">
								
			<jsp:include page="../sidebar.jsp">
		    	<jsp:param value="zipSearch" name="zip" />
		    	<jsp:param value="../img/MRI.png" name="picture" />
		    	<jsp:param value="contact" name="head" />
		    	<jsp:param value="t1" name="t"/>
			</jsp:include>
			
		</div>
	  </div>
	
</div>
<jsp:include page="../footer.jsp"></jsp:include>
<script>

	$('#contactEmail').blur(function() {
				  if(!validateEmail('contactEmail'))
				  {
					  alert('Although email is not required if selected must be valid!');
				  }
				  
				});
				
	$(".noNumb").keydown(function(event) {
			// Allow: backspace, delete, tab, escape, and enter
				if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
				 // Allow: Ctrl+A
					(event.keyCode == 65 && event.ctrlKey === true) || 
				 // Allow: home, end, left, right
					(event.keyCode >= 35 && event.keyCode <= 39)) {
					 // let it happen, don't do anything
						return;
				}
					else {
						// Ensure that it is a number and stop the keypress
						if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
							event.preventDefault(); 
					}   
				}
				});	
				
	$(".lbs").keydown(function(event) {
			
						// Ensure that it is a number and stop the keypress
						if (event.shiftKey || (event.keyCode > 47 && event.keyCode < 58)) {
							event.preventDefault(); 
						}   
				
				});	


function validateEmail(contactEmail){
				   var a = document.getElementById(contactEmail).value;
				   var filter = /^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*?)\s*;?\s*)+/;
					if(filter.test(a)){
						return true;
					}
					else{
						return false;
					}
				}
				
function tab(field,nextFieldID){
				if(field.value.length >= field.maxLength){
					document.getElementsByClassName(nextFieldID)[0].focus();
					}
				}
</script>