<%
String pageTitle = "Healthcare Professionals";
%>
<%@ include file="/header.jsp" %>

<div class="container">
  <div class="row">
    <div class="span8">
      <h1>Healthcare Professionals</h1><br>
      <h4><strong>Do you have patients that are <u>uninsured</u> or have <u>high-deductible</u> plans? We are here to help providing high quality imaging diagnostics catered for these patients.</strong></h4>
      <p><em>We offer the following to help you and your patient:</em></p>
      <ul>
        <li>Fully credentialed centers and American College of Radiology (ACR) certified equipment</li>
        <li>High strength magnets that yield higher quality images</li>
        <li>Better Images lead to better diagnosis</li>
        <li>In many cases your patient will be able to get their diagnostic test on the same day - we measure performance in hours not days. So, no waiting for days or even weeks</li>
      </ul>
      <h4><strong>I need to send in a Prescription/Rx for one of my patients - how do I do that?</strong></h4>
      <p>You should have received a notice from us with a direct fax number to the center that your patient has selected.  If you did not receive that, please let us know.  You can fax the Rx to us as well - just make sure that the patient's name and phone number are included. Fax your prescription to: 888-371-3302</p>
      <h4><strong>How do I get a copy of the report for one of my patients?</strong></h4>
      <p>As soon as the report is complete and reviewed, you will automatically receive a copy. We coordinate with our contracted centers to ensure that the report is delivered as quickly as possible. It is our goal to provide expedient service so that you and your patient can move forward with needed treatment. If you would like to inquire about the status of a report, please contact us at 888-693-8867.</p>
      <h4><strong>Who reads my patient's diagnostic image?</strong></h4>
      <p>We believe that quality images and quality diagnosis leads to the best treatment. We do not work with every center; we only work with the best. Your patient's diagnostic test will be read by one of our quality, credentialed radiologists.
</p>

	<h4><strong>What equipment do you use?</strong></h4>
	<p>When it comes to MRIs, we prefer higher field magnets (1.5-3.0 Tesla strength).  Unless there are no other's available or your patient has specific needs that require a lower-field Open MRI, we encourage all scans to be done in higher-field MRIs.</p>
	      
    </div>
    		<jsp:include page="../sidebar.jsp">
	    		<jsp:param value="zipSearch" name="zip" />
	    		<jsp:param value="../img/MRI.png" name="picture" />    			
				<jsp:param value="healthCarePros" name="head" />
				<jsp:param value="t1" name="t"/>
			</jsp:include>
  </div>
</div>
<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
<jsp:include page="../footer.jsp"></jsp:include>
