<%
String pageTitle = "Healthcare Professionals";
%>
<%@ include file="/header.jsp" %>
<script>
		$(document).ready(function(){
			if($('#1').click(function(){
				$('#without').show();
				$('#headerWithout').show();
				$('#headerWith').hide();
				$('#headerWithAndWithout').hide();
				$('#with').hide();
				$('#withAndWithout').hide();
			}));
			if($('#2').click(function(){
				$('#with').show();
				$('#headerWith').show();
				$('#headerWithout').hide();
				$('#headerWithAndWithout').hide();
				$('#without').hide();
				$('#withAndWithout').hide();
			}));
			
			if($('#3').click(function(){
				$('#withAndWithout').show();
				$('#headerWithAndWithout').show();
				$('#headerWithout').hide();
				$('#headerWith').hide();
				$('#without').hide();
				$('#with').hide();
			}));
			
			
			/*$('.enlarge').hover(function() {
			    $(this).css("cursor", "pointer");
			    $(this).animate({
			        width: "40%",
			        height: "40%"
			    }, 'slow');

			}, function() {
			    $(this).animate({
			        width: "32%"
			    }, 'slow');

			});*/
		});

</script>
 <div class="container">
 <h1>Sample Prescriptions</h1>
 <h3>See below for prescription samples to help you identify what your doctor is ordering.</h3>
  	<div class="row-fluid">  
		  	<div class="span3 " id="links" style="min-height:350px">
		  		<!--  <img src="../img/us_Machine.jpg">-->
		  		<h5 style="margin-left:20px">Prescription type list</h5>
		  		<a href="#" id="1" style="margin-left:7%; ">Without Contrast</a><br />
		  		<a href="#" id="2" style="margin-left:7%">With Contrast</a><br />
		  		<a href="#" id="3" style="margin-left:7%">With and Without Contrast</a><br />
		  	</div> 
	    
			<div class="span9 row-fluid">
			
				<div id="without" >
				<h5 id="headerWithout" >Rx Examples without Contrast</h5>
				<div class="span4" style="margin-left:-1%;">
			    	<div class="well well-small ">
						<img href="#myModal" src="../img/rx/mri_wo.jpg" data-toggle="modal" style="width:100%;">
					</div>
					<a href="#myModal" data-toggle="modal" id="headerWithout">MRI w/o contrast</a>
				</div>							
					<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
						<h3 id="myModalLabel">MRI w/o contrast</h3>
						</div>
						<div class="modal-body">
							<img src="../img/rx/mri_wo.jpg">
						</div>
						<div class="modal-footer">
							<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
						</div>
					</div>
					<div class="span4">
						<div class="well well-small " style="margin-left:1%">
							<img href="#myModal2" src="../img/rx/ct_wo.jpg" data-toggle="modal" style="width:100%;">
						</div>
						<a href="#myModal2" data-toggle="modal" id="headerWithout" style="margin-left:3%;">CT chest w/o contrast</a>
					</div>						
					<div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
						<h3 id="myModalLabel">CT chest w/o contrast</h3>;
						</div>
						<div class="modal-body">
							<img src="../img/rx/ct_wo.jpg">
						</div>
						<div class="modal-footer">style="margin-left:-1%;"
							<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
						</div>
					</div>
				</div>
				<div id="with" style="display:none;">
				<h5 id="headerWith" >Rx Examples with Contrast</h5>
				<div class="span4" style="margin-left:-1%;">
			    	<div class="well well-small" >
						<img href="#myModal3" src="../img/rx/ct_w.jpg" data-toggle="modal" style="width:100%;">
					</div>
					<a href="#myModal3" data-toggle="modal" id="myModalLabel">CT abdomen + contrast</a>
				</div>							
					<div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
						<h3 id="myModalLabel">CT abdomen + contrast</h3>
						</div>
						<div class="modal-body">
							<img src="../img/rx/ct_w.jpg">
						</div>
						<div class="modal-footer">
							<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
						</div>
					</div>
					<div class="span4">
					<div class="well well-small" >
						<img href="#myModal4" src="../img/rx/mri_wc2.jpg" data-toggle="modal" style="width:100%;">
					</div>
					<a href="#myModal4" data-toggle="modal" id="myModalLabel">MRI pelvis with w/c</a>
					</div>						
					<div id="myModal4" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
						<h3 id="myModalLabel">MRI pelvis with w/c</h3>
						</div>
						<div class="modal-body">
							<img src="../img/rx/mri_wc2.jpg">
						</div>
						<div class="modal-footer">
							<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
						</div>
					</div>
					<div class="span4">
					<div class="well well-small" >
						<img href="#myModal5" src="../img/rx/mri_w.jpg" data-toggle="modal" style="width:100%;">
					</div>
					<a href="#myModal5" data-toggle="modal" id="myModalLabel">MRI C-spine w/contrast</a>
					</div>						
					<div id="myModal5" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
						<h3 id="myModalLabel">MRI C-spine w/contrast</h3>
						</div>
						<div class="modal-body">
							<img src="../img/rx/mri_w.jpg">
						</div>
						<div class="modal-footer">
							<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
						</div>
					</div>
					
				</div>
				
					
				<div id="withAndWithout" style="display:none;" >
				<h5 id="headerWithAndWithout" >Rx Examples with and without Contrast</h5>
				<div class="span4" style="margin-left:-1%;">	
					<div class=" well well-small" >
						<img href="#myModal6" src="../img/rx/mri_wwo.jpg" data-toggle="modal" style="width:100%;">
					</div>
					<a href="#myModal6" data-toggle="modal" id="myModalLabel">MRI hip wwo contrast</a>
				</div>							
					<div id="myModal6" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
						<h3 id="myModalLabel">MRI hip wwo contrast</h3>
						</div>
						<div class="modal-body">
							<img src="../img/rx/mri_wwo.jpg">
						</div>
						<div class="modal-footer">
							<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
						</div>
					</div>
				</div>
				
				
	  		</div>
	</div>	
</div>
<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
<jsp:include page="../footer.jsp"></jsp:include>


							
		
						
										
								
						