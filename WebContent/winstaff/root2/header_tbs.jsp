<!doctype html>
<html>
<head>
<%if(request.getParameter("title").equals("Find Facilities Near You")||request.getParameter("title").equals("Reserve Image Center")){%>
	<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="com.winstaff.*" %>
	<link rel="stylesheet" href="/css/custom-theme/jquery-ui-1.10.2.custom.min.css" />
<%} %>
<%
Object rCodeO = session.getAttribute("nid");
String rCode = "--";
if (rCodeO!=null){
	rCode = rCodeO.toString();
}
%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<META NAME="title" content="NextImage Direct, Cost Saving MRI, Cost Saving CT, Cost Saving Ultrasound, Underinsured, High deductible, Self pay ,MRI, Cheap Magnetic Resonance Imaging, Cheap, Low Cost MRIs, Affordable MRIs,Discount MRIs, Magnetic Resonance Imaging prices, MRI Prices,MRI Costs, Affordable MRI">
<META NAME="description" content="NextImage Direct, Cost Saving MRI, CT, Ultrasound for Underinsured, High deductible, or Self pay">
<META NAME="keywords" content="MRI, MRIs, low cost MRIs,discount MRI,MRI prices, magnetic resonance imaging,discount magnetic resonance imaging, affordable MRI, MRI, CT, Cat Scan, US, Ultrasound, Underinsured, High deductible, Self pay">
<title><%= request.getParameter("title") %> | Cost Saving MRI, CT, Ultrasound for Underinsured, High deductible, or Self pay</title>
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<link href="/css/style.css" rel="stylesheet" media="screen">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37595845-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div class="container">
  <div class="row" style="margin:8px 0"> 
  <%
  if (rCode.equals("nhla")){%>
		 <img src="/img/nhla.jpg">  
  <%}
  %>
  <img src="/img/logo.png" style="margin-right:20px"> 
  <%
  if (rCode.equals("amvantage")){%>
		 <img src="/img/AmVantage.png" style="position: relative;top: 7px;left: 25px;">  
  <%}
  %>
  
  <%
  if (rCode.equals("adcperks")){%>
		 <img src="/img/adcperks.png" >  
  <%}
  %>
  
  <%
  if (rCode.equals("motivano")){%>
		 <img src="/img/motivano.jpeg">  
  <%}
  %>
  

  
  <h3 class="pull-right" style="display:inline;">Reserve a center now!</h3></div>
  <div class="navbar navbar-inverse">
    <div class="navbar-inner">
		<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		  <span class="icon-bar"></span>
		</button>
      <div class="container">
        <div class="nav-collapse collapse" id="main-menu">
          <ul class="nav" id="main-menu-left">
            <li><a href="/">Home</a></li>
            <li><a href="/why-nextimage-direct/">Why NextImage Direct?</a></li>
            <li><a href="/how-it-works/">How it Works</a></li>
            <li><a href="/healthcare-professionals/">Healthcare Professionals</a></li>
            <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#">FAQs <b class="caret"></b></a>
              <ul class="dropdown-menu" id="swatch-menu">
                <li><a href="/faqs/">General FAQ</a></li>
                <li><a href="/mri-faq/">MRI FAQ</a></li>
                <li><a href="/ct-faq/">CT FAQ</a></li>
                <li><a href="/us-faq/">Ultrasound FAQ</a></li>
                <li><a href="/pet-ct-faq/">PET-CT FAQ</a></li>
              </ul>
            </li>
            <li><a href="/contact/">Contact Us</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
