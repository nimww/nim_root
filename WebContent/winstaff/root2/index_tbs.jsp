<jsp:include page="header_tbs.jsp"><jsp:param value="NextImage Direct" name="title"/></jsp:include>

<script>
function findCenter(){
	var zip = $('input[name=zipT]').val();
	if(zip == null || zip == ''){
		$('label[for=zip]').css({'color':'red'});
		alert('Please Enter your 5 digit Zip');
		return false;
	} else if (!/^\d{5}$/.test(zip)) {
		$('label[for=zip]').css({'color':'red'});
		alert('Please Enter your 5 digit Zip. Only numbers are acceptible [0-9]');
		return false;
	} else if (/^\d{5}$/.test(zip)) {
		$('input[name=zip]').val($('input[name=zipT]').val());
		$('#findCenterForm').submit();
		//window.location.href = "/find-facilities-near-you/?zip="+$('input[name=zip]').val()+"&modality="+$('input[name=modality]').val()+"&dist="+$('input[name=dist]').val();
	}
	
}
</script>
<div class="container">
    <div class="row" >
		<div class="span4" >
			<div class="well">
				<form action="/find-facilities-near-you/" method="post" id="findCenterForm" onSubmit="findCenter();" >
			  <h3>Find Facilities Near You!</h3>
			  <p>
				<label for="zip">Your ZIP Code:</label>
				<input name="zipT" placeholder="ZIP" >
				<input type="hidden" name="zip" placeholder="ZIP">
			  </p>
			  <p>
				<label >Your Procedure:</label>
				<input type="radio" name="modality" checked value="MR">
				MRI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="modality" value="CT">
				CT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="modality" value="US">
				Ultrasound&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
			  <button type="submit" class="btn btn-success" id="findCenterBtn" ><i class="icon-zoom-in icon-white"></i> Find a Center</button> 
			  </form>
		</div>
      </div>
      <div class="span8"> 
		  <div id="myCarousel" class="carousel slide">
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
			</ol>
			<!-- Carousel items -->
			<div class="carousel-inner">
			<div class="active item"><img class="featured" src="img/slides/slide2.jpg"> </div>
			<div class="item"><img class="featured" src="img/slides/slide1.jpg"> </div>
			<div class="item"><img class="featured" src="img/slides/slide3.jpg"> </div>
			</div>
			<!-- Carousel nav -->
			<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
			<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
		</div>
    </div>
  </div>
  
  

      <div class="featurette">
        <img class="featurette-image pull-right" src="assets/slide/dialog-information.png" width="20%">
        <h2 class="featurette-heading">Quality. Affordable.<span class="muted"> Imaging.</span></h2>
        <p class="lead">NextImage Direct is the premier radiology management services company that offers you high-quality, low-cost imaging solutions. We offer savings of up to 75% for those who are uninsured, those who carry high deductible health plans and those who wish to self-pay.</p>
  <p>Individuals and families now have an affordable option to costly imaging services. NextImage Direct provides the same quality assurances of rigorous credentialing and certification as health insurance companies ensuring the best possible diagnosis, resulting in the best treatment options. A better image leads to a better diagnosis, better treatment, and a better recovery. We are not just about providing a discount MRI or a discount CT. At NextImage, we provide you with a quality, credentialed nationwide network to give you quality care at an affordable price.		
        </p>		
      </div>
  

      <ul class="thumbnails">
        <li class="span3">
			<div class="thumbnail well">
				<img src="img/whynid.jpg">
				<h4 class="lead"><strong>Why NextImage?</strong></h4>
				<p><i class="icon-play"></i> Cost savings of up to 50%-75% on MRI, CT, and Ultrasound imaging services
				<br><i class="icon-play"></i> Access to over 3,000 credentialed imaging providers nationwide
				<br><i class="icon-play"></i> Same or next day scheduling in your area
				<br><i class="icon-play"></i> Quality diagnosis by certified physicians
				</p>
			</div>
        </li>
        <li class="span3">
			<div class="thumbnail well">
				<img src="img/howitworks.jpg">
				  <h4 class="lead"><strong>How it Works</strong></h4>
				  <p>To Schedule an MRI or CT, NextImage Direct will need a prescription from your referring physician.
				  <br><br><a href="/how-it-works/" class="btn btn-primary btn-small">More Here <i class="icon-play icon-white"></i></a></p>
			</div>
        </li>
        <li class="span3">
			<div class="thumbnail well">
				<img src="img/healthcarepros.jpg">
				  <h4 class="lead"><strong>Quality Healthcare</strong></h4>
				  <p>A proper diagnosis is critical to your healthcare.  NextImage brings over two decades of experience in diagnostic imaging to ensure that you are working with the top in quality.  Through our years of building relationships, we have secured reduced rates with the top providers in your area. </p>
				</div>
        </li>
        <li class="span3">
			<div class="thumbnail well">
				<img  src="img/contactus.jpg">
				  <h4 class="lead"><strong>Contact Us</strong></h4>
				  <p><strong>3390 Carmel Mountain Rd #150<br>
					San Diego, CA 92121</strong><br>
					<strong>Ph:</strong> 888-693-8867<br>
					<strong>Fax:</strong> 888-371-3302<br>
					<a href="mailto:contact@nextimagedirect.com">contact@nextimagedirect.com</a></p>
			</div>
        </li>
      </ul>
  </div>
<jsp:include page="footer.jsp"></jsp:include>