<%
String pageTitle = "How it Works";
%>
<%@ include file="/header.jsp" %>

<div class="container">
  <div class="row">
    <div class="span8">
      <h1>How it Works</h1><br>
      <p class="lead">Working with NextImage is easy. </p>
      <p class="alert alert-info">To schedule your MRI, CT or Ultrasound, enter your zip code in our search box to find a facility near you. Reserve and pay for your facility online. The imaging center will contact you within two business days to schedule.</p>
	<h3><strong>Step 1</strong></h3>
      <p>From our <a href="/index.jsp" class="btn btn-mini">Home</a> screen, find a facility near you by entering your zip code and test needed (MR, CT, US, etc.).</p>
      <h3><strong>Step 2</strong></h3>
      <p>Select additional criteria for your procedure such as <em>contrast</em>, <em>orientation</em>, and <em>body part</em>	  </p>

	  <p><img class="img-polaroid" src="/img/how-it-works-1.png"></p>
      <h3><strong>Step 3</strong></h3>
      <p>Once you have selected your procedure, click on Find Center.</p>

	  <p><img class="img-polaroid" src="/img/how-it-works-2.png"></p>
      <h3><strong>Step 4</strong></h3>
      <p>Review the available options.  You will see several choices that vary in distance, price, and available diagnostic equipment.  Note: we take all of these factors into consideration when selecting the "best value" center.  You can select any of the centers shown. </p>

	  <p><img class="img-polaroid" src="/img/how-it-works-3.png"></p>
      <p class="alert alert-info">Because we have negotiated such deep discounts, we are not able to show you the name of the center until you purchase.  </p>

      <h3><strong>Step 5</strong></h3>
      <p>Select your center and completely fill out the online form.  After filling out your information, you can then pay using any major credit card.  Upon successful payment, your price will be reserved.  We will process your information and the center will call you within 1-2 business days to schedule your appointment. </p>
	  <hr>
      <p class="lead">Thank you for using NextImage Direct!</p>
      <p>If you have any questions or problems locating a center, please call us at: <strong>888-693-8867</strong></p>
    </div>
    <jsp:include page="../sidebar.jsp">
    		<jsp:param value="zipSearch" name="zip" />
    		<jsp:param value="../img/MRI.png" name="picture" />
			<jsp:param value="howItWorks" name="head" />
    		<jsp:param value="t1" name="t"/>
    </jsp:include>
  </div>
</div>
<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
<jsp:include page="../footer.jsp"></jsp:include>
