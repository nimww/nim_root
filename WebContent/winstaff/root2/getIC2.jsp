 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    Created on 3/15/2013
    Created by: Scott Ellis
*/%>
 

<div class="container">


<%
Object rCodeO = session.getAttribute("nid");
String rCode = "--";
bltNIM3_PayerMaster payerMaster = null;;
if (rCodeO!=null){
	rCode = rCodeO.toString();
	NIM3_ReferralSourceObject myRSO = NID_SubmitCaseObject.translateWebCodesToReferralSource ("","",rCode);
	payerMaster = new bltNIM3_PayerMaster(myRSO.getPayerID());
}
String pricePrefix = "Your";
if (payerMaster!=null && !payerMaster.getCustomerDisplayName().equalsIgnoreCase("")){
	pricePrefix = payerMaster.getCustomerDisplayName();
}

String zip=request.getParameter("zip");
int wid = new Integer(request.getParameter("wizard"));
boolean ro = new Boolean(request.getParameter("ro"));

if (zip==null || zip.equalsIgnoreCase("")) {

zip = "";
%>
<h2>No Zip Code inputed.</h2>
<%
} else {
	//Load the CPT Wizard object that the user selected
	bltCPTWizard myWizard = new bltCPTWizard(wid);

	//Translate the Wizard ID into a CPTGroupObject2 (which contains a group of CPT codes, etc.)
	CPTGroupObject2 cgo2 = NIMUtils.CPTWizardToCPTGroupObject(myWizard);

	//Get a listing of centers that are 30 miles from the zip and can service the CPT Group Object 2.  
	//Note the final parameter is a boolean that tells whether the results should be limited only to centers containing all the CPT Group objects.  Default = True
	//  This means that if the user needs an US only centers that have US will show up.  
	ImagingCenterListObject ICLO = NIDUtils.getListOfCenters(zip, 50, cgo2, true, ro,100, rCode);

	if (ro&&cgo2.isHasMRI()){
	%>
		<div class="alert alert-info badge-inverse"><h4>Note, we are only showing centers that have <em>Open MRI</em> machines because you selected the <em>Claustrophobia</em> option above. </h4></div>
	<%
	}

	if (ICLO==null){
	%>
		<div class="alert alert-warning"><h4>Sorry, we could not find any centers within 200 miles.  Try a nearby zip code to see if that works or give us a call for more options.</h4> </div>
	<%
	} else if (ICLO.getListOfCenters()==null){
	%>
		<div class="alert alert-warning">Bad ICLO List (Debug: Should not happen)</div>
	<%
	} else {
		%>
		<table class="table table-striped table-bordered table-hover" id="example" style="margin-bottom:0">
		<thead>
		<tr>
			<th>Distance</th>
			<th>Features</th>
			<th>Price</th> 
		</tr> 
		</thead>
		<tbody>
		<%
			int cnt=0;
			for (PracticeMaster_SearchResultsObject pm_sro: ICLO.getListOfCenters()){
			cnt++;
			%>
			<tr onClick="document.getElementById('reserve_form_<%=cnt%>').submit();" >
				<td data-title="distance" title="Estimated distance">
					<%=String.format("%02d", Math.round(pm_sro.getDistance()))%> miles away
					<br>
					<%=pm_sro.getPracticeMaster().getOfficeCity().toUpperCase()%>
					<br>
					<small style="color: lightgrey;"><%=pm_sro.getPracticeMaster().getPracticeID()%></small>
					<br>
					<form class="reserve" action="/reserve-facilities/" method="get" id = "reserve_form_<%=cnt%>">
				        <input type="hidden" name="myCPTwizard" value="<%=wid%>">
				        <input type="hidden" name="ic_code" value="<%=pm_sro.getPracticeMaster().getPracticeID()%>">
				        <input type="hidden" name="myZIP" value="<%=zip%>">
				        <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Select Center</button>
				     </form>
					
				</td>				
				<td data-title="features" >
					<div style="display:none"><%=pm_sro.getMACScore()%> </div>				
				<%
				if (pm_sro.getBadge_SuperHighField()) {
				%>
					<div class="badge badge-success activate-tooltip" data-title="Has a 'Super' high field (3.0 Tesla) MRI available.  3T MRI's are capable of producing some of the highest quality images.">3T</div>
				<%
				} else if (pm_sro.getBadge_HighField()) {
				%>
					<div class="badge badge-info activate-tooltip" data-title=" This imaging center has a MRI machine with a 1.5 tesla magnet strength or higher. These closed MRIs are faster and provide higher resolution images than the low field or Open MRI machines. Refer to the FAQs for more information."><i class="icon-star icon-white"></i>1.5T</div>
				<%
				} else if (false){  //do not show this badge right now
				%>
					<div class="badge badge-warning activate-tooltip" data-title="Has a low-field MRI (Less than 1.0 Tesla).  Low field MRIs produce lower resolution images.  Open-MRIs frequently use low-field magnets. In some cases these are required, but a high-field is preferred when possible. "><i class="icon-warning-sign icon-white"></i>&lt;1.0T</div>
				<%
				}

				if (pm_sro.getBadge_Open()) {
				%>
					<div class="badge badge-warning activate-tooltip" data-title="Has an OpenMRI style device.  Note these devices normally produce lower resolution images compared to a traditional high-field.  These are needed for cases of severe claustrophobia or certain body-types." >Open MRI</div>
				<%
				}
				if (pm_sro.getBadge_ACR()) {
				%>			
					<div class="badge badge-info activate-tooltip" data-title="One or more machines has been Accredited by the American College of Radiology">ACR Accredited</div><br>
				<%
				}
				if (pm_sro.getBadge_BestValue()) {
				%>
					<div class="badge badge-success activate-tooltip" data-title=" Based on your specific procedure, cost and distance, this center is the best match for your needs."><i class="icon-star icon-white "></i>Best Match!</div>
				<%
				}
				%>
			</td>
			<td  data-title="costsavings" >
			<%
			if (pm_sro.getSavings()>0.0) {
			%>
				<table class="table-condensed">
					<tr>
						<td>Usual & Customary </td><td>$<%=Math.round(pm_sro.getPriceUC())%></td>
					</tr>
					<tr span class="alert-info">
						<td >Discount </td><td>$<%=Math.round(pm_sro.getSavings())%> &nbsp;&nbsp;&nbsp;(<%=Math.round(pm_sro.getSavingsPercentage())%>%)</td>
					</tr>
					<tr class="alert-success">
						<td ><h4><%=pricePrefix%> Price:</h4></td><td><h4>$<%=Math.round(pm_sro.getCustomerPrice())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button onClick="document.getElementById('reserve_form_<%=cnt%>').submit();" class="btn btn-mini btn-success"><i class="icon-plus icon-white"></i></button></h4> </td>
					</tr>
				</table>
			<%
			} else {
			%>
				<table class="table-condensed">
					<tr class="alert-success">
						<td ><h4><%=pricePrefix%> Price:</h4></td><td><h4>$<%=Math.round(pm_sro.getCustomerPrice())%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button onClick="document.getElementById('reserve_form_<%=cnt%>').submit();" class="btn btn-mini btn-success"><i class="icon-plus icon-white"></i></button></h4> </td>
					</tr>
				</table>
			
			<%
			}
			%>
			</td>
			</tr>
			<%
			}
		%>
		
		</tbody>
		</table>
		<%
		
	}


}

%>


</div>

<!-- Script calls are placed here since getIC2.jsp is called via AJAX and may not have access to prior called scrips -->

<script src="/js/bootstrap_tooltip.js"></script>
<!-- DataTables CSS -->
<link rel="stylesheet" type="text/css" href="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
 <!-- jQuery -->
<script type="text/javascript" charset="utf8" src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
<!-- DataTables -->
<script type="text/javascript" charset="utf8" src="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>

<script>
$(document).ready(function() {
	$('.activate-tooltip').tooltip();

    var oTable = $('#example').dataTable( {
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": false
    } );
	// Sort immediately with columns 0 and 1
	oTable.fnSort( [ [1,'desc'] ] );	
	
		
} );


</script>



