<%
String pageTitle = "NextImage Direct";
%>
<%@ include file="/header.jsp" %>

<div class="container">
    <div class="row" >
		<div class="span4" >
			<div class="backgroundHome">
				<form  action="/find-facilities-near-you/" method="post" id="findCenterForm" onSubmit="return findCenter();" >
			  <h3>Find Facilities Near You!</h3>
			  <p>
				<label for="zip">Your ZIP Code:</label>
				<input name="zipT" id="zipT" maxlength="5" onChange="valZip()" placeholder="ZIP" >
				<input type="hidden" name="zip" placeholder="ZIP">
			  </p>
			  <p>
				<label >Your Procedure:</label>
				<input type="radio" name="modality" checked value="MR">
				MRI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="modality" value="CT">
				CT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="modality" value="US">
				Ultrasound&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
			  <button type="submit" class="ButtonsNID" id="findCenterBtn" ><i class="icon-zoom-in icon-white"></i> Find a Center</button> <br>
              <p style="font-size: 12px;margin-top:15px">
              		* Please call
              			<%  
				  			if(rCode.equals("nb")){  
				  		%>
							855-312-3450
						<%}else if(rCode.equals("dl")){ %>
							888-608-6099
						<%}else if(rCode.equals("benprovi")){%>
							855-306-6167
						<%}else if(rCode.equals("care1")){%>
							858-764-8012
						<%}else{%>
							888-693-8867
						<%} %>
              		for pricing & locations for Pet-CT, Myelogram, Arthrogram and EMG services
              </p>
			  </form>
		</div>
      </div>
      <div class="span8"> 
		  <div id="myCarousel" class="carousel slide">
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
			</ol>
			<!-- Carousel items -->
			<div class="carousel-inner">
			<% 
				if (payerMaster.getNID_ShowBanner()==1){
			%>
				<div class="active item"><img class="featured" src="img/partner/<%=payerMaster.getPayerID()%>_banner.png"> </div>
				<div class="item"><img class="featured" src="img/slides/NID_Banner.jpg"> </div>
				<div class="item"><img class="featured" src="img/slides/banner1.jpg"> </div>
			<% 
				} else {
			%>
			<div class="active item"><img class="featured" src="img/slides/NID_Banner.jpg"> </div>
			<div class="item"><img class="featured" src="img/slides/banner1.jpg"> </div>
			<% 
				}
			%>
			</div>
			<!-- Carousel nav -->
			<a class="carousel-control left" href="#myCarousel" data-slide="prev" style="background-color:#CCCCCC">&lsaquo;</a>
			<a class="carousel-control right" href="#myCarousel" data-slide="next" style="background-color:#CCCCCC">&rsaquo;</a>
		</div>
    </div>
    <div class="span12" >
    	<a style="margin-bottom:7px;width:320px;" href="http://www.mdaligne.com/MDAligne/Contact-a-Doctor" target="_blank" class="btn btn-warning btn-large">Need a doctors prescription</a>
    	<a style="margin-bottom:7px;width:320px;" href="/find-facilities-near-you/" class="btn btn-success btn-large">Order Here!</a>
    	<a style="margin-bottom:7px;width:320px;" onclick="LC_API.open_chat_window();" class="btn btn-primary btn-large">Questions? Live chat now!</a>
    </div><br>
  </div>  

      <!-- <div class="featurette">
		<p class="lead" id="hidebuttonforndcsavingsclub" >Don't have an Rx? Click here for one.<br><br><a href="http://www.mdaligne.com/MDAligne/Contact-a-Doctor" target="_blank" class="btn btn-warning btn-large">Contact a Doctor </a></p>
      </div>  
 -->
      <ul class="thumbnails">
        <li class="span3">
			<div class="thumbnail backgroundThumbNails" style="min-height:495px;" >
				<a href="why-nextimage-direct" ><img src="img/whynid.jpg"></a>
				<h4 class="lead"><strong>Why NextImage?</strong></h4>
				<p><i class="icon-play"></i> Cost savings of up to 75% on MRI, CT, and Ultrasound imaging services
				<br><i class="icon-play"></i> Access to over 3,000 credentialed imaging providers nationwide
				<br><i class="icon-play"></i> 24-48 Hour Scheduling in your area
				<br><i class="icon-play"></i> Quality diagnosis by certified radiologists
				</p>
			</div>
        </li>
        <li class="span3">
			<div class="thumbnail backgroundThumbNails" style="min-height:495px;" >
				<a href="/how-it-works/" ><img src="img/howitworks.jpg"></a>
				  <h4 class="lead"><strong>How it Works</strong></h4>
				  <p>To Schedule an MRI, CT, or Ultrasound, enter your zip code in our search box to find a facility near you. Reserve and pay for your facility online. The imaging center will contact you within two business days to schedule.
				  <br><br><a href="/how-it-works/" class="btn btn-primary btn-small">More Here <i class="icon-play icon-white"></i></a></p>
			</div>
        </li>
        <li class="span3">
			<div class="thumbnail backgroundThumbNails" style="height:495px;" >
				<a href="faqs" ><img src="img/healthcarepros.jpg"></a>
				  <h4 class="lead"><strong>Services</strong></h4>
						<p>At NextImage we offer discount imaging including:</p>
				  <p> 
						<i class="icon-play"></i> <a href="mri-faq">MRI</a><br />
						<i class="icon-play"></i> <a href="ct-faq">CT</a><br />
						<i class="icon-play"></i> <a href="us-faq">Ultrasound</a>
				  </p>
                  <p>Call
                  	<strong> 
                  		<%  
				  			if(rCode.equals("nb")){  
				  		%>
							855-312-3450
						<%}else if(rCode.equals("dl")){ %>
							888-608-6099
						<%}else if(rCode.equals("benprovi")){%>
							855-306-6167
						<%}else if(rCode.equals("care1")){%>
							858-764-8012 
						<%}else{%>
							888-693-8867
						<%} %> 
                  	</strong> 
                  	 to speak to one of our care coordinators for pricing and locations on the following:</p>
				  <p> 
						<i class="icon-play"></i> PetCT<br />
						<i class="icon-play"></i> EMG<br />
						<i class="icon-play"></i> Myelogram<br>
                        <i class="icon-play"></i> Arthrogram<br>
                       	<!--  <i class="icon-play"></i> Mammogram-->
				  </p>
				</div>
        </li>
        <li class="span3">
			<div class="thumbnail backgroundThumbNails" style="min-height:495px;" >
				<a href="contact" ><img  src="img/LadyWithHeadset.jpg"></a>
				  <h4 class="lead"><strong>Contact Us</strong></h4>
				  <p><strong>3390 Carmel Mountain Rd #150<br>
					San Diego, CA 92121</strong><br>
					<strong>Ph:</strong> 
						<%  
				  			if(rCode.equals("nb")){  
				  		%>
							855-312-3450
						<%}else if(rCode.equals("dl")){ %>
							888-608-6099
						<%}else if(rCode.equals("benprovi")){%>
							855-306-6167
						<%}else if(rCode.equals("care1")){%>
							858-764-8012
						<%}else{%>
							888-693-8867
						<%} %> 
					<strong>Fax:</strong> 888-371-3302<br>
					<a href="mailto:contact@nextimagedirect.com">contact@nextimagedirect.com</a></p>
			</div>
        </li>
      </ul>
      <h2 class="featurette-heading">Quality. Affordable. Imaging.</h2>
      <p class="lead">NextImage Direct is the premier radiology management services company that offers you high-quality, low-cost imaging solutions. We offer savings of up to 75% for those who are uninsured, those who carry high deductible health plans and those who wish to self-pay.</p>
  </div>
	<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
<jsp:include page="footer.jsp"></jsp:include>
