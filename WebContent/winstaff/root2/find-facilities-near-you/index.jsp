<!doctype html> 	
<%
/**
* Find Facilities Near You
*/
String zip = request.getParameter("zip");
if (zip==null){
	zip="";
}
String modality = request.getParameter("modality");
if (modality==null){
	modality="MR";
}
%>
<%
String pageTitle = "Find Facilities Near You";
%>
<%@ include file="/header.jsp" %>
<jsp:include page="../getCPTWizard.jsp"></jsp:include>

<!-- DataTables CSS -->
<link rel="stylesheet" type="text/css" href="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
 <!-- jQuery -->
<!-- <script type="text/javascript" charset="utf8" src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>-->
<!-- DataTables -->
<script type="text/javascript" charset="utf8" src="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>	
<script src="../js/search-facilities.js"></script>
<script>
/*var $dd = $('#bp');
if($dd.length > 0){
	var sv = $dd.text();
	var $options = $('option', $dd);
	var arrVals = [];
	$options.each(function(){
		arrVals.push({
			val: $(this).val(),
			text: $(this).text();
		});
	});
	arrVals.sort(function(a,b){
		if(a.val>b.val){
			return 1;
		}
		else if(a.val==b.val){
			return 0;
			}
		else{
			return -1;
		}
	});
	for(var i = 0, l = arrVals.length; i < l; i++){
		$($options[i]).val(arrVals[i].val).text(arrVals[i].text)
	}
	$dd.val(selectedVal);
}	*/
</script>
<div class="container">
  <div class="row">
      <div class="span4" >
		<div class="findFacilitiesNearYou">
          <h3>Find Facilities Near You!</h3>
          <p>
            <label>Your ZIP Code:</label>
            <input name="zip" placeholder="ZIP" id="zip" onChange="valZip()" value="<%=zip%>" maxlength="5">
			<span class="error"></span>
          </p>
          <p>
            <label style="margin-bottom:0">Your Procedure:</label>
            <input type="radio" name="modality" value="MR" <%=modality.equals("MR") ? "checked":"" %> onClick="getBP();">
            MRI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="modality" value="CT" <%=modality.equals("CT") ? "checked":"" %> onClick="getBP();">
            CT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="modality" value="US" <%=modality.equals("US") ? "checked":"" %> onClick="getBP();">
            Ultrasound&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
          <br>
          <p style="font-size: 12px;">
          
          * Please call 
          					<%if(rCode.equals("nb")){  
							  		%>
										855-312-3450
									<%}else if(rCode.equals("dl")){ %>
										888-608-6099
									<%}else if(rCode.equals("benprovi")){%>
										855-306-6167
									<%}else{%>
										888-693-8867 
									<%} %>
          	for pricing & locations for Pet-CT, Myelogram, Arthrogram, and EMG services</p>
          </div>
      </div>
      <div class="span8">
		<div class="findFacilitiesNearYou">
		 <div class="row " >          
			<div class="span4">
            <h3>Select Procedure Options:</h3>
            <label >Special:</label>
            <select name="special" id="special" onChange="getBP();">
              <option value="Normal">Normal</option>
              <option value="Arthrogram">Arthrogram(Coming Soon!)</option>
            </select>
            <br>
            <label class="activate-tooltip" style="display:inline"  data-title="In some cases, a dye called contrast material may be used. The contrast makes your organs more visible which helps the radiologist interpret these imaging studies. Refer to the FAQs for more information.">Contrast: <i class="icon-question-sign"></i></label>
            <br>
			<select name="contrast" id="contrast" onChange="getBP();">
              <option value="Without">Without</option>
              <option value="With">With</option>
              <option value="WithAndWithout">With+Without</option>
            </select>
            <label >Orientation:</label>
            <input type="radio" name="orientation" id="orinone" value="" checked onClick="getBP();">
            None&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="orientation" id="orileft" value="Left" onClick="getBP();">
            Left&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="orientation" value="Right" onClick="getBP();">
            Right&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>          
           
            <br>

          </div>
          <div class="span3">
            <h3>Check all that apply:</h3>
            <input type="checkbox" name="clastrophobic" id="clastrophobic">
            &nbsp;
			
            <label  style="display:inline" class="activate-tooltip" data-title="<h6>Claustrophobic would indicate a severe reaction to a confined space. Closed MRI units yield the best picture quality leading to better diagnosis.</h6>">Claustrophobic: <i class="icon-question-sign"></i></label>
            <br>
            <br>
            <label style="display:inline" class="activate-tooltip" data-title="Please specify body region to be scanned." >Body Part: <i class="icon-question-sign"></i></label>
			<br>
            <select class="bp" name="bp" id="bp" style="width:100%;" >			
            </select>
            <a href="#" class="btn btn-success pull-right required" id="find-center" onclick="return findCenters();"><i class="icon-zoom-in icon-white"></i> Find a Center</a>
            <span id="test"></span>
            <br /><br /><br />
            <a href="http://www.mdaligne.com/MDAligne/Contact-a-Doctor" target="_blank" id="clickforrxhideforndc" >Click here to obtain an Rx</a>
         </div>
      </div>
    </div>
  </div> 
  </div>
  <br>
  <div class="row">
    <div id="loadResults" class="span12">
    <h2>Please Select Procedure to Find a center</h2>
    </div>
  </div>  
</div>
	<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
<jsp:include page="../footer.jsp"></jsp:include>
