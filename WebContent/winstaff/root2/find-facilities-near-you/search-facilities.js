/*
 * Search Facilities
 *
 * @author : Po Le po.le@nextimagemedical.com
 *
 */

var selectedModalityList= [];
/* Multi procedure disabled
function addModality(){
	var bp = $('#bp option:selected').text();
	
	if (selectedModalityList.length<3){
		selectedModalityList.splice(selectedModalityList.length,1,bp);
	
		$('#added-modality-container').text('');
		selectedModalityList.forEach(function(mod){
			$('#added-modality-container').append('<span class="btn btn-small btn-info  added-modality">'+mod+'</span><br>');
		});
		$('#add-modality-btn, #find-center').addClass('btn-success');
	}
	if (selectedModalityList.length==3){
		$('#add-modality-btn').removeClass('btn-success');
	}	
}

$(document).on('click', '.added-modality', function() {
	selectedModalityList.splice($.inArray($(this).text(),selectedModalityList),1);
	$('#added-modality-container').text('');
	selectedModalityList.forEach(function(mod){
		$('#added-modality-container').append('<span class="btn btn-small btn-info added-modality">'+mod+'</span><br>');
	});
	$('#add-modality-btn').addClass('btn-success');
	if(selectedModalityList.length==0){
		$('#find-center').addClass('btn-warning').removeClass('btn-success');
	}
});
*/
function getIC(){
	var rs = $('#loadResults');	
	
	rs.text('').addClass("loadResults");
}

function getBP(){
	var modality = $('input[name=modality]:checked').val();
	var special = $('select#special').val();
	var contrast = $('select#contrast').val();
	var orientation = $('input[name=orientation]:checked').val();
	
	$('select#contrast, input[name=orientation], select#special, input[name=clastrophobic], option[value=Without]').removeAttr("disabled");
	
	if(modality=="MR" && special=="Arthrogram"){	
		if($('select#contrast').val()=="Without"){
			$('select#contrast').val("With")
		}
		$('select#contrast option[value=Without]').attr("disabled","disabled");
		$('#orinone').attr("disabled","disabled");
		if(orientation==""){
			$('#orileft').prop('checked', true);
		}
		
	}
	
	if(modality=="CT" && special=="Arthrogram"){
		$('select#contrast').val("With").attr("disabled","disabled");
	}
	
	if(modality=="US"){
		$('select#contrast').val("Without").attr("disabled","disabled");
		$('select#special').val("Normal").attr("disabled","disabled");
	}
	
	if(modality=="US" || modality=="CT"){
		$('input[name=clastrophobic]').prop('checked', false).attr("disabled","disabled");;
	}
	
	modality = $('input[name=modality]:checked').val();
	special = $('select#special').val();
	contrast = $('select#contrast').val();
	orientation = $('input[name=orientation]:checked').val();
	var fn = window["get"+modality+special+contrast+orientation];
	fn();	
}

$(document).ready(function(){
	getBP();
	//getIC();
});