<%
String pageTitle = "How it Works";
%>
<%@ include file="/header.jsp" %>
	<div class="container">
		
		<div class="">
			<img src="/img/NID_DL.png" style="height:35%; width:35%; margin-left:30%"></img><br /><br />
				<div class="span8 offset1">
					<p>NextImage Direct has partnered with Direct Labs to provide direct access laboratory testing for those who want to take charge 
							of their own health and personally monitor their own wellness. 
							Direct Labs assists in the prevention or early detection of disease by providing discounted, high quality 
							online blood and laboratory testing services directly and confidentially to consumers.
					</p>
				</div>	
				<div class="span8 offset1">
					<h4 style="color: rgb(76, 105, 155)">
						Who Needs DirectLabs
					</h4>
					<img alt="heart " src="/img/smallHeart.png" >  Individual Consumers <br />
					<img alt="heart " src="/img/smallHeart.png" >  Healthcare Providers <br />
					<img alt="heart " src="/img/smallHeart.png" >  Employers <br />
					<img alt="heart " src="/img/smallHeart.png" >  Associations <br />
					<img alt="heart " src="/img/smallHeart.png" >  Wellness Providers <br /><br />
					
					<h4 style="color: rgb(76, 105, 155)">Getting started with your discount online blood test is as easy as 1-2-3</h4>
					
					<img alt="heart " src="/img/mediumHeart.png" >  <strong style="color: rgb(76, 105, 155)">Order your test</strong> <br />
					<p class="span8" style="margin-left:6%">
						The process is simple<br />
						Order online OR <br />
						call (800) 908-0000
					</p><br />
					
					
					<img alt="heart " src="/img/mediumHeart.png" >  <strong style="color: rgb(76, 105, 155)">Go to Lab Patient Service Center</strong> <br />
					<p class="span8" style="margin-left:6%">
						Over 3000 locations<br />
						one convenient to your home or work <br />
						
					</p><br />
					
					
					<img alt="heart " src="/img/mediumHeart.png" >  <strong style="color: rgb(76, 105, 155)">Results available online</strong> <br />
					<p class="span8" style="margin-left:6%; margin-bottom:4%">
						Most results are received in 24-48<br />
						hours, securely & confidentially <br />
						
					</p>
					
					<h4 style="color: rgb(76, 105, 155)"><a href="http://www.directlabs.com/nextimagedirect" target="_blank" style="color: rgb(63, 140, 144)" >Click here</a> to learn more and schedule your blood test.</h4><br />
					
					<p>*Direct Labs services not available in MA, MD, ND, and SD</p><br />
					<p>To place an order in NJ, NY, or RI, visit <a href="https://www.directlabs.com/access/Home/tabid/10599/language/en-US/Default.aspx" target="_blank">DirectLabs.com/ACCESS</a></p>
				</div>
		</div>
		<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
	</div>
<jsp:include page="/footer.jsp"></jsp:include>