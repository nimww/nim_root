<%@page contentType="text/html" language="java" import="com.winstaff.*,java.util.*,com.google.gson.Gson" %>
<%
response.setContentType("application/javascript");

String callbackName = request.getParameter("cb"); 
String zip = request.getParameter("zip"); 
String key = request.getParameter("k"); 
String requestingUrl = request.getParameter("r"); 
String rCode = "--";
int wizardID = new Integer(request.getParameter("wid"));
Date date = new Date();
bltNID_WidgetTrack wt = new bltNID_WidgetTrack();

wt.setUniqueModifyDate(date);
wt.setUniqueCreateDate(date);
wt.setUniqueModifyComments(requestingUrl);
wt.setKeyID(key);
wt.setZipCode(zip);
wt.setRequestingURL(requestingUrl);
wt.setCPTWizardID(wizardID);

wt.commitData();

boolean ro = false;

ArrayList<PracticeMasterJson> list = new ArrayList<PracticeMasterJson>();

ImagingCenterListObject ICLO = NIDUtils.getListOfCenters(zip, 50, NIMUtils.CPTWizardToCPTGroupObject(new bltCPTWizard(wizardID)), true, ro,100, rCode);

Gson gson = new Gson();


for (PracticeMaster_SearchResultsObject pm_sro: ICLO.getListOfCenters()){

String practiceDistance = String.format("%02d", Math.round(pm_sro.getDistance())) + " miles away";
String practiceCity = pm_sro.getPracticeMaster().getOfficeCity().toUpperCase();
int practiceId = pm_sro.getPracticeMaster().getPracticeID();
double macScore = pm_sro.getMACScore();
boolean badgeSuperHighField = pm_sro.getBadge_SuperHighField();
boolean badgeHighField = pm_sro.getBadge_HighField();
boolean badgeOpenMr = pm_sro.getBadge_Open();
boolean badgeACR = pm_sro.getBadge_ACR();
boolean badgeBestValue = pm_sro.getBadge_BestValue();
double priceUC = pm_sro.getPriceUC();
double priceSaving = Math.round(pm_sro.getSavings());
int priceSavingPercent = Math.round(pm_sro.getSavingsPercentage());
double priceCustomer = Math.round(pm_sro.getCustomerPrice());



list.add(new PracticeMasterJson( practiceDistance, practiceCity, practiceId, macScore, badgeSuperHighField, badgeHighField, badgeOpenMr, badgeACR, badgeBestValue, priceUC, priceSaving, priceSavingPercent, priceCustomer, wizardID, zip));

}

String jsonp = callbackName + "(" + gson.toJson(list) + ")";

out.print(jsonp);

%>