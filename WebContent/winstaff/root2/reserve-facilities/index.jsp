<%
	/**
	 * Reserve facilities
	 */
String pageTitle = "Reserve Image Center";
%>
<%@ include file="/header.jsp" %>
<%
	String custom = "";
	if (rCodeO!=null && (custom==null ||  custom.equalsIgnoreCase("")) ){
		custom = rCodeO.toString();
	}
	
%>

<style>
	.display{
		display:none;
	}
</style>

<script src="/js/jquery-ui-timepicker-addon.js"></script>

<script type="text/javascript">
	function tab(field,nextFieldID){
				if(field.value.length >= field.maxLength){
					document.getElementsByClassName(nextFieldID)[0].focus();
				}
			}
</script>

<script>
	$(document).ready(function(e) {

		$('.pickdate').click(function() {
			var dateTypeVar = $('#datepicker').datetimepicker({
				dateFormat : 'mm-dd-yyyy hh:mm TT'
			}).val();
			var id = 'input[name=' + $(this).attr('id') + ']';
			$(id).val(dateTypeVar);
		});
		
			
			
			$('#iwEmail').change(function() {
				  if(!validateEmail('iwEmail'))
				  {
					  alert('Invalid Email Address');
				  }
				  
			   });	

			$(".noNumb").keydown(function(event) {
				// Allow: backspace, delete, tab, escape, and enter
					if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
					 // Allow: Ctrl+A
						(event.keyCode == 65 && event.ctrlKey === true) || 
					 // Allow: home, end, left, right
						(event.keyCode >= 35 && event.keyCode <= 39)) {
						 // let it happen, don't do anything
							return;
					}
						else {
							// Ensure that it is a number and stop the keypress allowing shift for capitals
							if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) 
							{
								event.preventDefault(); 
							}   
						}
					});	
					
					
					
					$(".noLetters").keydown(function(event) {
				
							// Ensure that it is a number and stop the keypress
							if ( (event.keyCode > 47 && event.keyCode < 58)) {
								
								
								event.preventDefault(); 
							}   
					
					});		
		$('.check').click(function(){
			this.checked?$('.display').show(1000):$('.display').hide(1000);
		});
		
		$('.check2').click(function(){
			this.checked?$('.display').hide(1000):$('.display').show(1000);
		});
	});
	
	function validateEmail(iwEmail){
		   var a = document.getElementById(iwEmail).value;
		   var filter = /^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*?)\s*;?\s*)+/;
			if(filter.test(a)){
				return true;
			}
			else{
				return false;
			}
		}
			
	$(function() {
		$("#datepicker").datetimepicker({
			showButtonPanel : false,
			minDate:0
		});
	});

	$(function() {
		$("#iwDOB").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "1900:2010"
		});
	});
	
	function valZip(){
	if($('#iwZip').val().length != 5){
		alert("Enter valid zip code");
		}		
	}
	
	
	
	function validateForm(){
		var preferredDate1=$("#time1").val();
		var preferredDate2=$("#time2").val();
		var preferredDate3=$("#time3").val();
		
		var iwFirstName=$("#iwFirstName").val();
		var iwLastName=$("#iwLastName").val();
		var iwPhone1=$("#iwPhone1").val();
		var iwPhone2=$("#iwPhone2").val();
		var iwPhone3=$("#iwPhone3").val();
		var iwEmail=$("#iwEmail").val();
		var iwDOB=$("#iwDOB").val();
		var iwGender=$("#iwGender").val();
	
		var iwAddress=$("#iwAddress").val();
		var iwCity=$("#iwCity").val();
		var iwState=$("#iwState").val();
		var iwZip=$("#iwZip").val();
		
		//var rx=$("input:radio[name='rx']:checked").val();
		//var isMetal=$("input:radio[name='isMetal']:checked").val();
		//var metalDesc=$("#metalDesc").val();
		//var isAllergies=$("#input:radio[name='isHasConditions']:checked").val();
		//var allergiesDesc=$("#allergiesDesc").val();
		
		var iwHeight=$("#iwHeight").val();
		var iwWeight=$("#iwWeight").val();
		
		var mdFirstName=$("#mdFirstName").val();
		var mdLastName=$("#mdLastName").val();
		var mdPhone1=$("#mdPhone1").val();
		var mdPhone2=$("#mdPhone2").val();
		var mdPhone3=$("#mdPhone3").val();
		var mdFax1=$("#mdFax1").val();
		var mdFax2=$("#mdFax2").val();
		var mdFax3=$("#mdFax3").val();

		//var referralSource=$("#referralSource").val();
		//var referralCode=$("#referralCode").val();
		var requiredFields = "Please complete the following fields:\n";
		var requiredComplete = true;
		
		if (false&&(preferredDate1==null || preferredDate1=="") ) {
			requiredFields += "- Preferred Date 1\n";
			$('label[for=time1]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=time1]').css({"color":"black"});}
		
		if (false&&(preferredDate2==null || preferredDate2=="") ) {
			requiredFields += "- Preferred Date 2\n";
			$('label[for=time2]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=time2]').css({"color":"black"});}
		
		if (false&&(preferredDate3==null || preferredDate3=="") ) {
			requiredFields += "- Preferred Date 3\n";
			$('label[for=time3]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=time3]').css({"color":"black"});}

		if ((iwFirstName==null || iwFirstName=="") ) {
			requiredFields += "- First Name\n";
			$('label[for=iwFirstName]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=iwFirstName]').css({"color":"black"});}
		 
		if ((iwLastName==null || iwLastName=="") ) {
			requiredFields += "- Last Name\n";
			$('label[for=iwLastName]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=iwLastName]').css({"color":"black"});}
		
		if ( (iwPhone1==null || iwPhone1=="") || (iwPhone2==null || iwPhone2=="") || (iwPhone3==null || iwPhone3=="") ) {
			requiredFields += "- Phone\n";
			$('label[for=iwPhone]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=iwPhone]').css({"color":"black"});}
		
		if ((iwEmail==null || iwEmail=="") ) {
			requiredFields += "- Email\n";
			$('label[for=iwEmail]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=iwEmail]').css({"color":"black"});}
		
		if ((iwDOB==null || iwDOB=="") ) {
			requiredFields += "- DOB\n";
			$('label[for=iwDOB]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=iwDOB]').css({"color":"black"});}
		
		if ((iwGender==null || iwGender=="") ) {
			requiredFields += "- Gender\n";
			$('label[for=iwGender]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=iwGender]').css({"color":"black"});}
		if ((iwAddress==null || iwAddress=="") ) {
			requiredFields += "- Address\n";
			$('label[for=iwAddress]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=iwAddress]').css({"color":"black"});}
		
		if ((iwCity==null || iwCity=="") ) {
			requiredFields += "- City\n";
			$('#labelCity').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('#labelCity').css({"color":"black"});}
		
		if ((iwState==null || iwState=="") ) {
			requiredFields += "- State\n";
			$('#labelState').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('#labelState').css({"color":"black"});}
		
		if ((iwZip==null || iwZip=="" || !iwZip.match(/\b\d{5}\b/g)) ) {
			requiredFields += "- Zip\n";
			$('#labelZip').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('#labelZip').css({"color":"black"});}
		
		/*if ((rx==null || rx=="") ) {
			requiredFields += "- rx\n";
			$('label[for=rx]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=rx]').css({"color":"black"});}
		
		if ((isMetal==null || isMetal=="") ) {
			requiredFields += "- Metal?\n";
			$('label[for=isMetal]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=isMetal]').css({"color":"black"});}
		
		if ((isAllergies==null || isAllergies=="") ) {
			requiredFields += "- Have Allergies\n";
			$('label[for=isAllergies]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=isAllergies]').css({"color":"black"});}*/
		
		if ((iwHeight==null || iwHeight=="") ) {
			requiredFields += "- Height\n";
			$('label[for=iwHeight]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=iwHeight]').css({"color":"black"});}
		
		if ((iwWeight==null || iwWeight=="" || !iwWeight.match(/^\d{1,4}$/)) ) {
			requiredFields += "- Weight\n";
			$('label[for=iwWeight]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=iwWeight]').css({"color":"black"});}
		if ((mdFirstName==null || mdFirstName=="") ) {
			requiredFields += "- First Name\n";
			$('label[for=mdFirstName]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=mdFirstName]').css({"color":"black"});}
		
		if ((mdLastName==null || mdLastName=="") ) {
			requiredFields += "- Last Name\n";
			$('label[for=mdLastName]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=mdLastName]').css({"color":"black"});}
		
		if ( (mdPhone1==null || mdPhone1=="") || (mdPhone2==null || mdPhone2=="") || (mdPhone3==null || mdPhone3=="") ) {
			requiredFields += "- Doctor Phone\n";
			$('label[for=mdPhone]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=mdPhone]').css({"color":"black"});}
		
		if ( (mdFax1==null || mdFax1=="") || (mdFax2==null || mdFax2=="") || (mdFax3==null || mdFax3=="") ) {
			requiredFields += "- Doctor Fax\n";
			$('label[for=mdFax]').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('label[for=mdFax]').css({"color":"black"});}
		
		if ((!$("#tos").is(":checked")) ) {
			requiredFields += "- Must Agree to Terms of Service\n";
			$('#tosLabel').css({"color":"red"});
			requiredComplete = false;
		}
		else {$('#tosLabel').css({"color":"black"});}
		
		if (!requiredComplete){
			alert(requiredFields);
			return false;
		}
		
	}
</script>
<div class="container">

	<%
		searchDB2 conn = null;
		String query = null;
		java.sql.ResultSet rs = null;

		String myCPTwizard = null;
		String myCPT = null;
		String bp = null;

		int myICCode = 0;
		String isClaustrophobic = null;
		String myCounty = null;
		CPTGroupObject2 myCPTO2_full = null;
		Double dPrice = null;

		if (request.getParameter("ic_code") != null
				&& request.getParameter("myCPTwizard") != null) {

			myCPTwizard = request.getParameter("myCPTwizard");
			
			
			conn = new searchDB2();
			query = "select cpt1, bp1 from tcptwizard where cptwizardid = "
					+ myCPTwizard;
			rs = conn.executeStatement(query);
			//out.print(query);
			while (rs.next()) {
				myCPT = rs.getString("cpt1");
				bp = rs.getString("bp1");
			}
			conn.closeAll();

			myICCode = new Integer(request.getParameter("ic_code"));
			myCounty = new bltPracticeMaster(myICCode).getOfficeCity();
			
			PracticeMaster_SearchResultsObject pm_sro = new PracticeMaster_SearchResultsObject(new Integer(myICCode));
			
			Double theCost = NIMUtils.getCPTGroupPayPriceforPracticeMaster(pm_sro.practiceMaster,NIMUtils.CPTWizardToCPTGroupObject(new bltCPTWizard(new Integer(myCPTwizard))), PLCUtils.getToday(), NIMUtils.PAYMENT_TYPE_GROUPHEALTH);
			pm_sro.setPrice(theCost);

            NIM3_ReferralSourceObject myRSO = NID_SubmitCaseObject.translateWebCodesToReferralSource("","", custom);
            bltNIM3_NIDPromo nidPromo = null;
            if (myRSO.getNidPromo()!=null){
                nidPromo = new bltNIM3_NIDPromo(myRSO.getNidPromo());
            }
            pm_sro.setCustomerPrice(NIDUtils.getNIDFinalPrice(theCost, pm_sro.getPracticeMaster(), payerMaster, nidPromo ));
			
			myCPTO2_full = new CPTGroupObject2(myCPT, "");
			dPrice = NIMUtils.getCPTGroupPayPriceforPracticeMaster(
					new bltPracticeMaster(myICCode), myCPTO2_full,
					PLCUtils.getToday());
	%>
	<%!public String customLabel(String company, int iSelect) {
		//iSelect 1 = referral source. 2 = referral code

		String sLabel = "";
		String sAppend = "";

		//Set Label
		if (iSelect == 0) {
			if (company.equals("nb") || company.equals("coverdell")
					|| company.equals("pps") ) {
				sLabel = "";
			} else {
				sLabel = "How did you hear about us?";
			}
		}

		if (iSelect == 1) {
			if (company.equals("nop")) {
				sLabel = "Rx Group Number*";
			} else if (company.equals("nb")) {
				sLabel = "Group ID Number*";
			} else if (company.equals("nhla")) {
				sLabel = "Employer*";
			} else if (company.equals("coverdell") || company.equals("pps") || company.equals("dl")) {
				sLabel = "";
			} else {
				sLabel = "Referral Code or ID number";
			}
		}

		//Set Note
		if (company.equals("nop")) {
			sAppend = "<br>If you do not have your Rx Group Number, please call<br>888-600-5769 for assistance.";
		}

		String[] sSelect = new String[2];

		sSelect[0] = sLabel;
		sSelect[1] = sLabel + sAppend;

		return sSelect[iSelect];
	}

	public String customInput(String company, int iSelect) {
		//iSelect 1 = referral source. 2 = referral code

		String sType = "";
		String sValue = "";
		String sReadOnly = "";

		//Set Input type
		if (iSelect == 0) {
			if (company.equals("nb") || company.equals("coverdell")
					|| company.equals("pps")) {
				sType = "hidden";
			} else {
				sType = "text";
			}
		} else if (iSelect == 1) {
			if (company.equals("coverdell") || company.equals("pps")|| company.equals("dl")) {
				sType = "hidden";
			} else {
				sType = "text";
			}
		}

		//Set Value 
		if (iSelect == 0) {
			if (company.equals("nb")) {
				sValue = "NB";
			} else if (company.equals("nop")) {
				sValue = "nop";
			} else if (company.equals("nhla")) {
				sValue = "NHLA";
			} else if (company.equals("coverdell")) {
				sValue = "Coverdell";
			} else if (company.equals("pps")) {
				sValue = "Premier Payor Solutions";
			}
		} else if (iSelect == 1) {
			if (company.equals("dl")) {
				sValue = "DL";
			} else {
				sValue = "";
			}
		}

		//Set Read Only
		if (iSelect == 0) {
			if (company.equals("nb") || company.equals("nop")
					|| company.equals("nhla") || company.equals("coverdell")
					|| company.equals("pps")) {
				sReadOnly = "readonly";
			}
		} else if (iSelect == 1) {
			sReadOnly = "";
		}

		String[] sSelect = new String[2];

		sSelect[0] = "<input tabindex=\"29\" type=\""
				+ sType
				+ "\" value=\""
				+ sValue
				+ "\" "
				+ sReadOnly
				+ " id=\"referralSource\" class=\"ptForm\" name=\"referralSource\">";
		sSelect[1] = "<input tabindex=\"30\" type=\""
				+ sType
				+ "\" value=\""
				+ sValue
				+ "\" "
				+ sReadOnly
				+ " id=\"referralCode\" class=\"ptForm\" name=\"referralCode\"><br>";

		return sSelect[iSelect];
	}%>
	
	
	
    <form action="/secure-payment-processing/" method="post" onSubmit="return validateForm();">
    
    
    
    
    
	<div class="row">
		
		<h3 class="span12" >Appointment Details</h3>
	</div>
	<div class="row">
		<div class="span4" >			
			<div class="reserveFacilities" style="min-height:125px;" >
					<h5>Requested Facility: </h5>
					<h4><%=myCounty%>- <%=myICCode%></h4>
					</div>
					<input type="hidden" name="ic_code" value="<%=myICCode%>" />
					<input type="hidden" name="myCPTwizard" value="<%=myCPTwizard%>">
			</div>
		<div class="span4" >			
			<div class="reserveFacilities" style="min-height:125px" >
					<h4><%=payerMaster.getCustomerDisplayName()%><br>discount price: </h4>
					<h3>$<%=Math.round(pm_sro.getCustomerPrice())%></h3>
			</div>
		</div>
		<div class="span4" >			
			<div class="reserveFacilities" style="min-height:125px" >
					<h5>Procedure </h5>
					 <h4><%=bp%></h4>
					 <h5>CPT Code: <%=myCPT%></h5>
			</div>
		</div>
	</div>
	
	
	
	
	
	
	
	<div class="row-fluid">
	<div class="div">
			<div class="" style="background-color:#fdf0a2;;color:rgb(66, 66, 66);padding:1px 5px;">
				<h5 class="">Image Center address and phone number will be emailed to you after you secure your appointment and issue credit card payment. If you do not have an email or have questions, please call us.</h5>															
			</div>						
		</div>
		<div class="div">
			<div class="" style="background-color:#333333;color:white;">
				<h4 class="">Patient Information</h4>															
			</div>						
		</div>
		<div class="row-fluid img-polaroid" style="width:1160px;margin-top:-10px" >
			<div class="row-fluid">
				<div class="span3">
					<label for="iwFirstName">First Name</label> <input type="text" name="iwFirstName"
						value="" id="iwFirstName" class="ptForm noLetters" tabindex="1" placeholder="First name">
				</div>
				<div class="span3">
					<label for="iwLastName">Last Name</label> <input type="text" name="iwLastName"
						value="" id="iwLastName" class="ptForm noLetters" tabindex="2" placeholder="Last name">
				</div>
				<div class="span3">
					<label for="iwDOB">DOB</label> <input name="iwDOB" readonly id="iwDOB" tabindex="3">
				</div>
				<div class="span3">
					<label for="iwGender">Gender</label> <select name="iwGender" id="iwGender" tabindex="4">
						<option value="">Select Gender</option>
						<option value="M">Male</option>
						<option value="F">Female</option>
					</select>
				</div>
			</div>
			<div class="row-fluid">
			<div class="row-fluid span6">
				<div>
					<label for="iwAddress">Address</label> <input type="text" name="iwAddress" value="" tabindex="5"
							id="iwAddress" class="ptForm add" style="width:89%" placeholder="Address">
				</div>
				
				
			</div>
			<div class="row-fluid span6">
			<div>
					
			<label for="iwCityStateZip"><span id="labelCity">City</span><span id="labelState" style="margin-left:280px" >State</span><span id="labelZip" style="margin-left:11%">Zip</span></label> <input type="text" name="iwCity"
					value="" id="iwCity" class="ptForm noLetters" tabindex="6"  placeholder="City" /> 
					<select tabindex="7" name="iwState" id="iwState" class="select-style" style="width: 90px; margin-bottom: 10px; margin-left:73px">
						<option value="">State</option>
						<%
						conn = new searchDB2();
							query = ("select * from tstateli where stateid not in (0,52) order by shortstate");
							rs = conn.executeStatement(query);
							while (rs != null && rs.next()) {
						%>
						<option value="<%=rs.getString("stateid")%>"><%=rs.getString("shortstate")%></option>
						<%
							}
								conn.closeAll();
						%>
						</select>
							
						<input type="text" name="iwZip" value="" id="iwZip" class="ptForm noNumb" tabindex="8" style="width: 105px; margin-bottom: 10px; margin-left:1%" onChange="valZip()" maxlength=5 placeholder="Zipcode">
			</div>
			</div>
			</div>
			<div class="row-fluid">
				<div class="span3">
						<!--<label for="iwAddress">Address</label> <input type="text" name="iwAddress" value="" tabindex="3"
							id="iwAddress" class="ptForm input-block-level" placeholder="Address">-->
						<label for="iwEmail">Email</label> 
						<input type="text" name="iwEmail" value="" tabindex="9"	id="iwEmail" class="ptForm email" placeholder="Email">
						
						<span class="error"></span>	
				</div>
					
				<div class="span3"  style="margin-left:">
						<label for="iwPhone">Phone</label><input type="text" name="iwPhone1" value="" placeholder="XXX"
						id="iwPhone1" class="ptForm noNumb area" tabindex="10" style="width:45px" onkeyup="tab(this,'pre')" maxlength="3" /><span>-</span>
						
						<input type="text" name="iwPhone2" value="" placeholder="XXX"
						id="iwPhone2" class="ptForm noNumb pre" tabindex="11" style="width:52px" maxlength="3" onkeyup="tab(this,'exch')" /><span>-</span>
						
						<input type="text" name="iwPhone3" value="" placeholder="XXXX"
						id="iwPhone3" class="ptForm noNumb exch" tabindex="12" style="width:63px" maxlength="4" onkeyup="tab(this,'myweight')" />
				</div>
				<div class="span3 prescreen">
					<label for="iwHeight">Height</label> <select class="myweight" name="iwHeight" id="iwHeight"
						tabindex="13">
						<option value="">N/A</option>
						<option value="4 ft 0 in">4 ft 0 in</option>
						<option value="4 ft 1 in">4 ft 1 in</option>
						<option value="4 ft 2 in">4 ft 2 in</option>
						<option value="4 ft 3 in">4 ft 3 in</option>
						<option value="4 ft 4 in">4 ft 4 in</option>
						<option value="4 ft 5 in">4 ft 5 in</option>
						<option value="4 ft 6 in">4 ft 6 in</option>
						<option value="4 ft 7 in">4 ft 7 in</option>
						<option value="4 ft 8 in">4 ft 8 in</option>
						<option value="4 ft 9 in">4 ft 9 in</option>
						<option value="4 ft 10 in">4 ft 10 in</option>
						<option value="4 ft 11 in">4 ft 11 in</option>
						<option value="5 ft 0 in">5 ft 0 in</option>
						<option value="5 ft 1 in">5 ft 1 in</option>
						<option value="5 ft 2 in">5 ft 2 in</option>
						<option value="5 ft 3 in">5 ft 3 in</option>
						<option value="5 ft 4 in">5 ft 4 in</option>
						<option value="5 ft 5 in">5 ft 5 in</option>
						<option value="5 ft 6 in">5 ft 6 in</option>
						<option value="5 ft 7 in">5 ft 7 in</option>
						<option value="5 ft 8 in">5 ft 8 in</option>
						<option value="5 ft 9 in">5 ft 9 in</option>
						<option value="5 ft 10 in">5 ft 10 in</option>
						<option value="5 ft 11 in">5 ft 11 in</option>
						<option value="6 ft 0 in">6 ft 0 in</option>
						<option value="6 ft 1 in">6 ft 1 in</option>
						<option value="6 ft 2 in">6 ft 2 in</option>
						<option value="6 ft 3 in">6 ft 3 in</option>
						<option value="6 ft 4 in">6 ft 4 in</option>
						<option value="6 ft 5 in">6 ft 5 in</option>
						<option value="6 ft 6 in">6 ft 6 in</option>
						<option value="6 ft 7 in">6 ft 7 in</option>
						<option value="6 ft 8 in">6 ft 8 in</option>
						<option value="6 ft 9 in">6 ft 9 in</option>
						<option value="6 ft 10 in">6 ft 10 in</option>
						<option value="6 ft 11 in">6 ft 11 in</option>
						<option value="7 ft 0 in">7 ft 0 in</option>
						<option value="7 ft 1 in">7 ft 1 in</option>
						<option value="7 ft 2 in">7 ft 2 in</option>
						<option value="7 ft 3 in">7 ft 3 in</option>
						<option value="7 ft 4 in">7 ft 4 in</option>
						<option value="7 ft 5 in">7 ft 5 in</option>
						<option value="7 ft 6 in">7 ft 6 in</option>
						<option value="7 ft 7 in">7 ft 7 in</option>
						<option value="7 ft 8 in">7 ft 8 in</option>
						<option value="7 ft 9 in">7 ft 9 in</option>
						<option value="7 ft 10 in">7 ft 10 in</option>
						<option value="7 ft 11 in">7 ft 11 in</option>
					</select>
				</div>
				<div class="span3 prescreen">
					<label for="iwWeight">Weight</label> <input class="noNumb" type="text" name="iwWeight" value=""
						id="iwWeight" tabindex="14" maxlength="3" placeholder="Weight">
				</div>
			</div>
		</div>
		
			
			
				

					
		
        </div>
        <script type="text/javascript">
        	$(document).ready(function(){
        		$('.check').click(function(){
        			$('#showmethebutton').show();
        		});
        		$('.check2').click(function(){
        			$('#showmethebutton').hide();
        		});
        	});
        </script>
        
        <div class="" style="margin-top:10px;">
        	<div class="alert alert-info ">
        		<div class="row" id="prescreenhideneedrxndc" >
					<div class="span4">
						<label for="rx">Do you have your imaging prescription?</label>
					</div>
					<div class="span2">
						Yes: <input type="radio" name="rx" value="1" id="rx" class="check2"
							checked tabindex="15">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No: <input
							type="radio" name="rx" value="2" id="rx" class="check" tabindex="14" ><br />
							
							<!-- <a class="display" href="http://www.mdaligne.com/MDAligne/Contact-a-Doctor">Click here to obtain an Rx</a>-->
					</div>
					<div class="span5" id="showmethebutton" style="display:none;" >
						<a id="hidebuttonforndcsavingsclub" href="http://www.mdaligne.com/MDAligne/Contact-a-Doctor" target="_blank" class="btn btn-warning">Contact a Doctor </a>
					</div>
				</div>
        	</div>
        </div>
		
		
     
     
     <div class="row-fluid" style="margin-top:-20px;">
		<div class="div">
			<div class="" style="background-color:#333333;color:white;">
				<h4 class="">Referring Physician's Information</h4>															
			</div>						
		</div>
		<div class="row-fluid img-polaroid" style="width:1160px;margin-top:-10px" >
     		<div class="">
				<div class="span3">
					<label for="mdFirstName">Physician's First Name</label> <input type="text"
						name="mdFirstName" value="" id="mdFirstName" class="ptForm noLetters"
						tabindex="16" placeholder="Physician's First Name">
				</div>
				<div class="span3">
					<label for="mdLastName">Physician's Last Name</label> <input type="text"
						name="mdLastName" value="" id="mdLastName" class="ptForm noLetters"
						tabindex="17" placeholder="Physician's Last Name">
				</div>
			</div>
			<div class="">
				<div class="span3">
					<label for="mdPhone">Physician's Phone</label> 
					
					<input type="text" name="mdPhone1" value=""  placeholder="XXX"
						id="mdPhone1" class="ptForm noNumb area1" tabindex="18" style="width:45px" maxlength="3" onkeyup="tab(this,'pre1')" /><span>-</span>
						
						<input type="text" name="mdPhone2" value="" placeholder="XXX"
						id="mdPhone2" class="ptForm noNumb pre1" tabindex="19" style="width:52px" maxlength="3" onkeyup="tab(this,'exch1')" /><span>-</span>
						
						<input type="text" name="mdPhone3" value="" placeholder="XXXX"
						id="mdPhone3" class="ptForm noNumb exch1" tabindex="20" style="width:52px" maxlength="4" onkeyup="tab(this,'area2')" />
					
				</div>
				<div class="span3">
					<label for="mdFax">Physician's Fax</label> 
					
					<input type="text" name="mdFax1" value=""  placeholder="XXX"
						id="mdFax1" class="ptForm noNumb area2" tabindex="21" style="width:45px" maxlength="3" onkeyup="tab(this,'pre2')" /><span>-</span>
						
						<input type="text" name="mdFax2" value="" placeholder="XXX"
						id="mdFax2" class="ptForm noNumb pre2" tabindex="22" style="width:52px" maxlength="3" onkeyup="tab(this,'exch2')" /><span>-</span>
						
						<input type="text" name="mdFax3" value="" placeholder="XXXX"
						id="mdFax3" class="ptForm exch2 noNumb" tabindex="23" style="width:52px" maxlength="4" onkeyup="tab(this,'')" />
					

				</div>
				<!--<div class="span6" >
			<h3></h3>
			<div class="row">
				<div class="span3">
					<label for=""><%=customLabel(custom, 0)%></label>
					<%=customInput(custom, 0)%>
				</div>
				<div class="span3">
					<label for=""><%=customLabel(custom, 1)%></label>
					<%=customInput(custom, 1)%>
				</div>
			</div>

		</div>-->
			</div>
     	</div>
     </div>
     
     
     
     
	
	
	<% if (payerMaster.getNID_ShowCode()==1 ||  payerMaster.getNID_ShowSource()==1 ){
	%>
	
	
		<div class="" style="margin-top:10px;">
        	<div class="alert alert-info ">
        		<div class="" id="prescreenhideneedrxndc" >
					<div class="row">
						<% if (payerMaster.getNID_ShowSource()==1 ){
						%>
							<div class="span5">
								<label for="" style="display:inline"><%=payerMaster.getNID_SourceDisplayName()%></label>
								<input type=text  tabindex="29" name="referralSource"  id="referralSource" class="ptForm">
							</div>
						<%
						}
						%>
						<% if (payerMaster.getNID_ShowCode()==1 ){
						%>
						
							<div class="span5">
								<label for="" style="display:inline"><%=payerMaster.getNID_CodeDisplayName()%></label>
								
								<input type=text  tabindex="30" name="referralCode"  id="referralCode" class="ptForm" <%if(rCode.equals("mot")){ %>readonly<%} %> value="<%if(rCode.equals("mot")){ %>Benefit-hub<%} %>">
							</div>
						
						<%
						}
						%>
					</div>
				</div>
        	</div>
        </div>
	
	
	
	
	<%-- <div class="row">
		<h3 style="margin-left:2%">&nbsp;How did you hear about us?</h3>
		<div class="span6" >
			
			
			<div class="row">
			
			<% if (payerMaster.getNID_ShowSource()==1 ){
			%>
				<div class="span3">
					<label for=""><%=payerMaster.getNID_SourceDisplayName()%></label>
					<input type=text  tabindex="29" name="referralSource"  id="referralSource" class="ptForm">
				</div>
			<%
			}
			%>
			<% if (payerMaster.getNID_ShowCode()==1 ){
			%>
				<div class="span3">
					<label for=""><%=payerMaster.getNID_CodeDisplayName()%></label>
					<input type=text  tabindex="30" name="referralCode"  id="referralCode" class="ptForm">
				</div>
			<%
			}
			%>
			</div>

		</div>
	
		
	</div> --%>
		
<%
}
%>
		<div class="span7">
				<input type="checkbox" id="tos" tabindex="30" > <span id="tosLabel">I agree to the <a href="/general-terms-of-service/" target="_blank">terms of service.</a></span><br><br>
				<button type="submit" id="email" class="btn btn-large btn-success" tabindex="31" >Reserve and Pay</button>

		</div>
	</div>
	<input type="hidden" name="wizardid" value="<%=myCPTwizard%>"> 
	<input type="hidden" name="pid" value="<%=myICCode%>"> 
	<input type="hidden" name="bodyPart" value="<%=bp%>"> 
	
	</form>
	<%
		} else {
	%>
	<h1>Error - Parameters not supplied</h1>
	<%
		}
	%>

</div>
<script type="text/javascript">
		var __lc = {};
		__lc.license = 4689701;
		
		(function() {
			var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
			lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
<jsp:include page="../footer.jsp"></jsp:include>
