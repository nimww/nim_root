<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltUpdateItem" %>
<%/*

    filename: out\jsp\tUpdateItem_form.jsp
    Created on Nov/10/2004
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%@ include file="../generic/generalDisplay.jsp" %>
<link rel="stylesheet" href="style.css" type="text/css">

<script language="JavaScript">
<!--
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
      <table width="100%" border="0" cellspacing="0" height="50">
        <tr>
          <td background="title1.jpg">&nbsp;</td>
        </tr>
      </table>
<br>
<table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td>
    <td> 
      <table width="700" border="1" cellspacing="0" cellpadding="3" bordercolor="#666666">
        <tr> 
          <td class="tdHeaderAlt" nowrap colspan="4"> 
            <input  style="font-family: Arial; font-size: 10px; background-color: #C0C0C0"   type="button" name="Submit3" value="Search..." onClick="MM_goToURL('this','tUpdateItem_query.jsp');return document.MM_returnValue">
          </td>
        </tr>
        <tr> 
          <td class="tdHeaderAlt" nowrap >Items in Planning</td>
          <td align="center" bgcolor="#FFFF00"><a href="tUpdateItem_form.jsp?EDIT=new">Add 
            Item</a></td>
          <td align="center" bgcolor="#CCCCCC">&nbsp;<a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=&StatusID=1&ItemTitle=&ReleaseVersion=TBD&ItemDescription=">List 
            &quot;Submitted&quot;<br>
            not yet in a Release</a></td>
          <td align="center" bgcolor="#CCCCCC"><a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=&StatusID=2&ItemTitle=&ReleaseVersion=TBD&ItemDescription=">List 
            &quot;Needs More Info&quot;<br>
            not yet in a Release</a></td>
        </tr>
        <tr> 
          <td class="tdHeaderAlt" nowrap >Items in Development</td>
          <td align="center" bgcolor="#66FFFF" colspan="3">&nbsp;<a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=&StatusID=1&ItemTitle=&ReleaseVersion=5.4&ItemDescription=">List 
            &quot;Submitted&quot; for Next Release</a></td>
        </tr>
        <tr> 
          <td class="tdHeaderAlt" nowrap>Items in Testing</td>
          <td align="center" bgcolor="#FFCC99"><a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=&StatusID=3&ItemTitle=&ReleaseVersion=&ItemDescription=">Items 
            in QA Testing</a></td>
          <td align="center" bgcolor="#FFCC99"><a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=&StatusID=4&ItemTitle=&ReleaseVersion=&ItemDescription=">Items 
            in Production Testing</a></td>
          <td align="center" bgcolor="#FFCCCC"><a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=&StatusID=6&ItemTitle=&ReleaseVersion=&ItemDescription=">Items 
            that Failed Testing</a></td>
        </tr>
        <tr> 
          <td class="tdHeaderAlt" nowrap>Other Searches</td>
          <td align="center" bgcolor="#CCFFCC"><a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=&StatusID=7&ItemTitle=&ReleaseVersion=&ItemDescription=">All 
            Fixed Items</a></td>
          <td align="center"><a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=1&StatusID=1&ItemTitle=&ReleaseVersion=&ItemDescription=">List 
            &quot;Submitted&quot; Priority 1</a></td>
          <td align="center"><a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=1&StatusID=2&ItemTitle=&ReleaseVersion=&ItemDescription=">List 
            &quot;Needs More Info&quot; Priority 1</a></td>
        </tr>
      </table>
      <br>
      <table width="700" border="1" cellspacing="0" cellpadding="3" bordercolor="#666666">
        <tr> 
          <td class="tdHeaderAlt">Release Version</td>
          <td class="tdHeaderAlt">Start Date</td>
          <td class="tdHeaderAlt">Target Dev. Date</td>
          <td class="tdHeaderAlt">Target QA Date</td>
          <td class="tdHeaderAlt">Target Production Date</td>
        </tr>
        <tr bgcolor="#FFD5BF"> 
          <td><a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=&StatusID=0&ItemTitle=&ReleaseVersion=5.1&ItemDescription=">5.1(in 
            production testing)</a></td>
          <td>NA</td>
          <td>NA</td>
          <td>NA</td>
          <td>1/10/2005 (in testing)</td>
        </tr>
        <tr bgcolor="#FFD5BF"> 
          <td><a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=&StatusID=0&ItemTitle=&ReleaseVersion=5.2&ItemDescription=">5.2(in 
            production testing)</a></td>
          <td>1/13/2005</td>
          <td>1/19/2005</td>
          <td>Target:1/20/2005<br>
            Actual: 1/27/2005</td>
          <td>Target: 1/20/2005 5PM<br>
            Actual: 1/27/2005 (in testing)</td>
        </tr>
        <tr bgcolor="#999999"> 
          <td><a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=&StatusID=0&ItemTitle=&ReleaseVersion=5.25&ItemDescription=">5.25 
            (fixed)</a></td>
          <td>1/27/2005</td>
          <td>1/27/2005<br>
            Actual: 1/27/2005</td>
          <td>1/27/2005<br>
            Actual: 2/3/2005</td>
          <td>1/27/2005 5PM<br>
            Actual: 2/3/2005</td>
        </tr>
        <tr bgcolor="#FFD5BF"> 
          <td><a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=&StatusID=0&ItemTitle=&ReleaseVersion=5.26&ItemDescription=">5.26 
            (in producting testing)</a></td>
          <td>1/30/2005</td>
          <td>2/2/2005<br>
            Actual: 1/31/2005<br>
          </td>
          <td>2/3/2005<br>
            Actual: 2/7/2005</td>
          <td>2/3/2005 5PM<br>
            Actual: 2/7/2005 (in testing)</td>
        </tr>
        <tr bgcolor="#FFD5BF"> 
          <td><a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=&StatusID=0&ItemTitle=&ReleaseVersion=5.3&ItemDescription=">5.3 
            (in production testing)</a></td>
          <td>2/7/2005</td>
          <td>2/17/2005<br>
            Actual: 2/17/2005</td>
          <td>2/17/2005<br>
            Actual: 2/17/2005 </td>
          <td>2/17/2005<br>
            Actual: 2/17/2005 </td>
        </tr>
        <tr bgcolor="#CCFFCC"> 
          <td><a href="tUpdateItem_query.jsp?maxResults=50&startID=0&orderBy=UpdateItemID&Submit=Submit&UpdateItemID=&SubmissionDate=&CategoryID=0&PriorityID=&StatusID=0&ItemTitle=&ReleaseVersion=5.4&ItemDescription=">5.4 
            (in development)</a></td>
          <td>2/24/2005</td>
          <td>2/28/2005<br>
          </td>
          <td>3/1/2005<br>
          </td>
          <td>3/2/2005</td>
        </tr>
      </table>
      <hr noshade>
      <b><span class="instructions">Make sure to click 'update' to save your changes 
      before leaving this page!</span></b> 
      <%
//initial declaration of list class and parentID
    Integer        iUpdateItemID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = new Integer(2);
   if (iSecurityCheck.intValue()!=0)
   {
    bltUpdateItem        UpdateItem        =    null;
	boolean isEdit=false;
    if (request.getParameter("EDITID")!=null) 
    {
        iUpdateItemID        =    new Integer( request.getParameter("EDITID"));
		isEdit=true;
	        accessValid = true;    
	    pageControllerHash.put("iUpdateItemID",iUpdateItemID);
	    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
	    {
		
	        UpdateItem        =    new    bltUpdateItem(iUpdateItemID,UserSecurityGroupID);
	    }
	}
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
    pageControllerHash.remove("iUpdateItemID");
        accessValid = true;    
        UpdateItem        =    new    bltUpdateItem(UserSecurityGroupID, true);
%>
      <hr noshade>
      <b><span class="instructions">Please make sure to search for your 'item' 
      before adding it to ensure that it is not already in the Database. To do 
      so, <a href="tUpdateItem_query.jsp">click here</a> and search under <u>description</u> 
      or <u>title</u>!</span></b> 
      <%
    }
		if (UpdateItem.getReleaseVersion().equalsIgnoreCase(""))
		{
			UpdateItem.setReleaseVersion("TBD");
		}
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tUpdateItem_form.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID



//fields
        %>
      <form action="tUpdateItem_form_sub.jsp" name="tUpdateItem_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
<input <%=HTMLFormStyleButton%> type=Submit value="Update" name=Submit>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>
            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if (isEdit)
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Item ID:</b></p>
                  </td><td valign=top><p><%=UpdateItem.getUpdateItemID()%></p></td></tr>
                        <%
	    }
            if ( (UpdateItem.isRequired("SubmissionDate",UserSecurityGroupID))&&(!UpdateItem.isComplete("SubmissionDate")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("SubmissionDate",expiredDays))&&(UpdateItem.isExpiredCheck("SubmissionDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (!isEdit&&(UpdateItem.isWrite("SubmissionDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Submission Date</b></p>
                  </td><td valign=top><p><input maxlength=10  type=text size="80" name="SubmissionDate" value='today' disabled onFocus=blur() >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=SubmissionDate&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("SubmissionDate")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("SubmissionDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Submission Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UpdateItem.getSubmissionDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=SubmissionDate&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("SubmissionDate")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UpdateItem.isRequired("CategoryID",UserSecurityGroupID))&&(!UpdateItem.isComplete("CategoryID")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("CategoryID",expiredDays))&&(UpdateItem.isExpiredCheck("CategoryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UpdateItem.isWrite("CategoryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Category&nbsp;</b></p>
                  </td><td valign=top><p><select   name="CategoryID" ><jsp:include page="../generic/tUpdateItemCategory.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UpdateItem.getCategoryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=CategoryID&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("CategoryID")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("CategoryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Category</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/tUpdateItemCategory_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UpdateItem.getCategoryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=CategoryID&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("CategoryID")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (UpdateItem.isRequired("ReleaseVersion",UserSecurityGroupID))&&(!UpdateItem.isComplete("ReleaseVersion")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("ReleaseVersion",expiredDays))&&(UpdateItem.isExpiredCheck("ReleaseVersion",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if (CurrentUserAccount.getLogonUserName().equalsIgnoreCase("v3update")&&(UpdateItem.isWrite("ReleaseVersion",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top>
                    <p class=<%=theClass%> ><b>Release Version&nbsp;</b></p>
                  </td>
                  <td valign=top>
                    <p>
                      <input type=text name="ReleaseVersion" maxlength=100 value = "<%=UpdateItem.getReleaseVersion()%>" size="80">
                      &nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=ReleaseVersion&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("ReleaseVersion")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else if ((UpdateItem.isRead("ReleaseVersion",UserSecurityGroupID)))
            {
                        %>
                <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Release Version</b></p>
                  </td>
                  <td valign=top>
                    <p><b><%=UpdateItem.getReleaseVersion()%></b>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=ReleaseVersion&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("ReleaseVersion")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                <%
            }
            else
            {
                        %>
                <%
            }
            %>
                <%
            if ( (UpdateItem.isRequired("PriorityID",UserSecurityGroupID))&&(!UpdateItem.isComplete("PriorityID")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("PriorityID",expiredDays))&&(UpdateItem.isExpiredCheck("PriorityID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <%
            if ((UpdateItem.isWrite("PriorityID",UserSecurityGroupID)))
            {
                        %>
                <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Priority</b></p>
                  </td><td valign=top><p><input maxlength="20" type=text size="80" name="PriorityID" value="<%=UpdateItem.getPriorityID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=PriorityID&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("PriorityID")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("PriorityID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Priority</b></p>
                  </td><td valign=top><p><%=UpdateItem.getPriorityID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=PriorityID&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("PriorityID")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UpdateItem.isRequired("StatusID",UserSecurityGroupID))&&(!UpdateItem.isComplete("StatusID")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("StatusID",expiredDays))&&(UpdateItem.isExpiredCheck("StatusID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UpdateItem.isWrite("StatusID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Status</b></p>
                  </td><td valign=top><p>
<%
if (isEdit)
{
%>
				  <select   name="StatusID" ><jsp:include page="../generic/tUpdateItemStatus.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UpdateItem.getStatusID()%>" /></jsp:include></select>
<%
}
else
{
%>
				  <select   name="StatusID" ><jsp:include page="../generic/tUpdateItemStatus.jsp" flush="true" ><jsp:param name="CurrentSelection" value="1" /></jsp:include></select>
<%
}
%>
				  &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=StatusID&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("StatusID")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("StatusID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Status</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/tUpdateItemStatus_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UpdateItem.getStatusID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=StatusID&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("StatusID")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UpdateItem.isRequired("ItemTitle",UserSecurityGroupID))&&(!UpdateItem.isComplete("ItemTitle")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("ItemTitle",expiredDays))&&(UpdateItem.isExpiredCheck("ItemTitle",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UpdateItem.isWrite("ItemTitle",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Item Title</b></p>
                  </td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="ItemTitle" cols="40" maxlength=200><%=UpdateItem.getItemTitle()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=ItemTitle&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("ItemTitle")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("ItemTitle",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Item Title</b></p>
                  </td><td valign=top><p><%=UpdateItem.getItemTitle()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=ItemTitle&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("ItemTitle")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UpdateItem.isRequired("ItemDescription",UserSecurityGroupID))&&(!UpdateItem.isComplete("ItemDescription")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("ItemDescription",expiredDays))&&(UpdateItem.isExpiredCheck("ItemDescription",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UpdateItem.isWrite("ItemDescription",UserSecurityGroupID)))
            {

int myRows = 1+(UpdateItem.getItemDescription().length() / 38);
                        %>
                       <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Description</b></p>
                  </td><td valign=top><p><textarea onKeyDown="textAreaStop(this,4000)" rows="<%=myRows%>" name="ItemDescription" cols="40" maxlength=4000><%=UpdateItem.getItemDescription()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=ItemDescription&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("ItemDescription")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("ItemDescription",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Description</b></p>
                  </td><td valign=top><p><%=UpdateItem.getItemDescription()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=ItemDescription&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("ItemDescription")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


                       <tr><td colspan=2><hr></td></tr>



                       <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Special Notes on how to test:<br> *if needed</b></p>
               </td>
                  <td valign=top> 
				  <%= UpdateItem.getHowToTestComments()  			  %>
			   </td></tr>

                       <tr><td colspan=2><hr></td></tr>


            <%
            if ( (UpdateItem.isRequired("DeveloperComments",UserSecurityGroupID))&&(!UpdateItem.isComplete("DeveloperComments")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("DeveloperComments",expiredDays))&&(UpdateItem.isExpiredCheck("DeveloperComments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UpdateItem.isWrite("DeveloperComments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Development Comments</b></p>
               </td>
                  <td valign=top> 
                    <span class=tdBaseAlt><p  	><%=UpdateItem.getDeveloperComments()%>&nbsp;</p></span>
			   <p><b>Add your Comments here:</b><br><textarea onKeyDown="textAreaStop(this,4000)" rows="2" name="DeveloperComments" cols="40" maxlength=4000></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=DeveloperComments&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("DeveloperComments")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
			   </td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("DeveloperComments",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Development Comments</b></p>
                  </td><td valign=top><p><%=UpdateItem.getDeveloperComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=DeveloperComments&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("DeveloperComments")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

                       <tr><td colspan=2><hr></td></tr>


            <%
            if ( (UpdateItem.isRequired("TesterComments",UserSecurityGroupID))&&(!UpdateItem.isComplete("TesterComments")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("TesterComments",expiredDays))&&(UpdateItem.isExpiredCheck("TesterComments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UpdateItem.isWrite("TesterComments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Tester Comments</b></p>
                  </td><td valign=top>
				  <span class=tdBaseAlt><p class=tdBaseAlt><%=UpdateItem.getTesterComments()%></p></span>
				  <p><b>Add your Comments here:</b><br><textarea onKeyDown="textAreaStop(this,4000)" rows="2" name="TesterComments" cols="40" maxlength=4000></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=TesterComments&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("TesterComments")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
				  </td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("TesterComments",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Tester Comments&nbsp;</b></p>
                  </td><td valign=top><p><%=UpdateItem.getTesterComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=TesterComments&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("TesterComments")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


                       <tr><td colspan=2><hr></td></tr>

            <%
            if ( (UpdateItem.isRequired("DateTestedOnDevelopment",UserSecurityGroupID))&&(!UpdateItem.isComplete("DateTestedOnDevelopment")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("DateTestedOnDevelopment",expiredDays))&&(UpdateItem.isExpiredCheck("DateTestedOnDevelopment",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((UpdateItem.isWrite("DateTestedOnDevelopment",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>DateTestedOnDevelopment&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="DateTestedOnDevelopment" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UpdateItem.getDateTestedOnDevelopment())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=DateTestedOnDevelopment&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("DateTestedOnDevelopment")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("DateTestedOnDevelopment",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>DateTestedOnDevelopment&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UpdateItem.getDateTestedOnDevelopment())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=DateTestedOnDevelopment&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("DateTestedOnDevelopment")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UpdateItem.isRequired("DateTestedOnQA",UserSecurityGroupID))&&(!UpdateItem.isComplete("DateTestedOnQA")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("DateTestedOnQA",expiredDays))&&(UpdateItem.isExpiredCheck("DateTestedOnQA",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((UpdateItem.isWrite("DateTestedOnQA",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>DateTestedOnQA&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="DateTestedOnQA" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UpdateItem.getDateTestedOnQA())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=DateTestedOnQA&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("DateTestedOnQA")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("DateTestedOnQA",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>DateTestedOnQA&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UpdateItem.getDateTestedOnQA())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=DateTestedOnQA&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("DateTestedOnQA")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UpdateItem.isRequired("DateTestedOnProduction",UserSecurityGroupID))&&(!UpdateItem.isComplete("DateTestedOnProduction")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("DateTestedOnProduction",expiredDays))&&(UpdateItem.isExpiredCheck("DateTestedOnProduction",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((UpdateItem.isWrite("DateTestedOnProduction",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>DateTestedOnProduction&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="DateTestedOnProduction" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UpdateItem.getDateTestedOnProduction())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=DateTestedOnProduction&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("DateTestedOnProduction")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("DateTestedOnProduction",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>DateTestedOnProduction&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UpdateItem.getDateTestedOnProduction())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=DateTestedOnProduction&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("DateTestedOnProduction")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UpdateItem.isRequired("RequesterInitials",UserSecurityGroupID))&&(!UpdateItem.isComplete("RequesterInitials")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("RequesterInitials",expiredDays))&&(UpdateItem.isExpiredCheck("RequesterInitials",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UpdateItem.isWrite("RequesterInitials",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=UpdateItem.getEnglish("RequesterInitials")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,4000)" rows="2" name="RequesterInitials" cols="40" maxlength=4000><%=UpdateItem.getRequesterInitials()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=RequesterInitials&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("RequesterInitials")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("RequesterInitials",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=UpdateItem.getEnglish("RequesterInitials")%>&nbsp;</b></p></td><td valign=top><p><%=UpdateItem.getRequesterInitials()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=RequesterInitials&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("RequesterInitials")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UpdateItem.isRequired("RemoteIP",UserSecurityGroupID))&&(!UpdateItem.isComplete("RemoteIP")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("RemoteIP",expiredDays))&&(UpdateItem.isExpiredCheck("RemoteIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((false&&UpdateItem.isWrite("RemoteIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>RemoteIP&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="RemoteIP" value="<%=UpdateItem.getRemoteIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=RemoteIP&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("RemoteIP")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("RemoteIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>RemoteIP&nbsp;</b></p></td><td valign=top><p><%=UpdateItem.getRemoteIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=RemoteIP&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("RemoteIP")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UpdateItem.isRequired("Comments",UserSecurityGroupID))&&(!UpdateItem.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((UpdateItem.isExpired("Comments",expiredDays))&&(UpdateItem.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UpdateItem.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=UpdateItem.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,4000)" rows="2" name="Comments" cols="40" maxlength=4000><%=UpdateItem.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("Comments")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UpdateItem.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=UpdateItem.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=UpdateItem.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../nim3/auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("Comments")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tUpdateItem")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tUpdateItem")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p>
                <input <%=HTMLFormStyleButton%> type=Submit value="Update" name=Submit>
              </p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>

