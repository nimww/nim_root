<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltUpdateItem, com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tUpdateItem_form_sub.jsp
    Created on Nov/10/2004
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
String theRIP = request.getRemoteAddr();
String theDate = PLCUtils.getDisplayDateWithTime(new java.util.Date());

//initial declaration of list class and parentID
    Integer        iUpdateItemID        =    null;
    boolean accessValid = false;
boolean isNew = false;
boolean isSendEmail = false;
   Integer iSecurityCheck = new Integer(2);
   if (iSecurityCheck.intValue()!=0)
   {


    bltUpdateItem        UpdateItem        =    null;
    if (pageControllerHash.containsKey("iUpdateItemID")) 
    {
        iUpdateItemID        =    (Integer)pageControllerHash.get("iUpdateItemID");
        accessValid = true;    
	    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
	    {
	        UpdateItem        =    new    bltUpdateItem(iUpdateItemID,UserSecurityGroupID);
	    }
	}
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
	isNew = true;
        accessValid = true;    
        UpdateItem        =    new    bltUpdateItem();
    }


  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID


String testChangeID = "0";
String theUser = CurrentUserAccount.getLogonUserName();
try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !UpdateItem.getUniqueCreateDate().equals(testObj)  )
    {
         UpdateItem.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !UpdateItem.getUniqueModifyDate().equals(testObj)  )
    {
         UpdateItem.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !UpdateItem.getUniqueModifyComments().equals(testObj)  )
    {
         UpdateItem.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem UniqueModifyComments not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
//    testObj = (PLCUtils.getSubDate(request.getParameter("SubmissionDate"))) ;
    if ( isNew )
    {
         UpdateItem.setSubmissionDate( new java.util.Date() );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem SubmissionDate not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CategoryID")) ;
    if ( !UpdateItem.getCategoryID().equals(testObj)  )
    {
         UpdateItem.setCategoryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem CategoryID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PriorityID")) ;
    if ( !UpdateItem.getPriorityID().equals(testObj)  )
    {
         UpdateItem.setPriorityID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem PriorityID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReleaseVersion")) ;
    if ( !UpdateItem.getReleaseVersion().equals(testObj)  )
    {
         UpdateItem.setReleaseVersion( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem ReleaseVersion not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("StatusID")) ;
    if ( !UpdateItem.getStatusID().equals(testObj)  )
    {
         UpdateItem.setStatusID( testObj,UserSecurityGroupID   );
	 if (UpdateItem.getStatusID().intValue()==3||UpdateItem.getStatusID().intValue()==4)
	 {
		isSendEmail = true;
	 }
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem StatusID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ItemTitle")) ;
    if ( !UpdateItem.getItemTitle().equals(testObj)  )
    {
         UpdateItem.setItemTitle( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem ItemTitle not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ItemDescription")) ;
    if ( !UpdateItem.getItemDescription().equals(testObj)  )
    {
         UpdateItem.setItemDescription( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem ItemDescription not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DeveloperComments")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
	String addOnMe = ""; 
	if (!UpdateItem.getDeveloperComments().equalsIgnoreCase(""))
	 {
		addOnMe = "<hr>";
         }
         UpdateItem.setDeveloperComments( UpdateItem.getDeveloperComments() +  addOnMe + "[" + theDate + "] [" + theRIP + "] <i>" + theUser + "</i>:<br> " + testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem DeveloperComments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("TesterComments")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
	String addOnMe = ""; 
	if (!UpdateItem.getTesterComments().equalsIgnoreCase(""))
	 {
		addOnMe = "<hr>";
         }
         UpdateItem.setTesterComments( UpdateItem.getTesterComments() +  addOnMe + "[" + theDate + "] [" + theRIP + "] <i>" + theUser + "</i>:<br> " + testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem TesterComments not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateTestedOnDevelopment"))) ;
    if ( !UpdateItem.getDateTestedOnDevelopment().equals(testObj)  )
    {
         UpdateItem.setDateTestedOnDevelopment( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem DateTestedOnDevelopment not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateTestedOnQA"))) ;
    if ( !UpdateItem.getDateTestedOnQA().equals(testObj)  )
    {
         UpdateItem.setDateTestedOnQA( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem DateTestedOnQA not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateTestedOnProduction"))) ;
    if ( !UpdateItem.getDateTestedOnProduction().equals(testObj)  )
    {
         UpdateItem.setDateTestedOnProduction( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem DateTestedOnProduction not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("RequesterInitials")) ;
    if ( !UpdateItem.getRequesterInitials().equals(testObj)  )
    {
         UpdateItem.setRequesterInitials( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem RequesterInitials not set. this is ok-not an error");
}

try
{
    if ( isNew )
    {
         UpdateItem.setRemoteIP( theRIP );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem RemoteIP not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !UpdateItem.getComments().equals(testObj)  )
    {
         UpdateItem.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UpdateItem Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	UpdateItem.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    {
	    UpdateItem.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
		if (UpdateItem.getReleaseVersion().equalsIgnoreCase(""))
		{
			UpdateItem.setReleaseVersion("TBD");
		}
	        UpdateItem.commitData();
	        if (isSendEmail)
		{
	                emailType myETSend = new emailType();
	                myETSend.setTo("alock@winstaff.com");
	                myETSend.setFrom("sellis@winstaff.com");
			String myStatus = (new bltUpdateItemStatusLI(UpdateItem.getStatusID())).getUpdateItemStatusText();
	                myETSend.setSubject("Notice: Winstaff Support Item " + UpdateItem.getUniqueID() + " is " + myStatus);
			String theBody = "The following item has been updated to (" + myStatus + ")\n";
			theBody += "\nItem: " + UpdateItem.getUniqueID();
			theBody += "\nTitle: " + UpdateItem.getItemTitle();
			theBody += "\nDate: " + displayDateSDF1.format(new java.util.Date());
	                myETSend.setBody(theBody);
			
	                if (myETSend.isSendMail())
	                {
		                myETSend.setTo("sellis@winstaff.com");
		                myETSend.isSendMail();
		                myETSend.setTo("blackwell.l@ghc.org");
		                myETSend.isSendMail();
				%>
				<script language=javascript>
				alert ("Email has been Sent");
				</script>
				<%
	                }
		}
		iUpdateItemID=UpdateItem.getUniqueID();

	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }

}
String nextPage="tUpdateItem_query.jsp";
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
nextPage = "tUpdateItem_form.jsp?EDIT=edit&EDITID="+iUpdateItemID;
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit?EDITID=UpdateItem.getUniqueID()");
}
        %>


  <%
  }
  else
  {
//   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
//out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
