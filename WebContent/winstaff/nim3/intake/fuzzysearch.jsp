<%@ page contentType="text/html; charset=utf-8" language="java" import="com.winstaff.*" errorPage="" %>
<%@ include file="../../generic/CheckLogin.jsp" %>
<%
boolean isDebug = false;
if (isScheduler)
{
	String sType = request.getParameter("sType");
	if (sType.equalsIgnoreCase("case"))
	{
								   
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		String mySQL = "SELECT \"vMQ4_Encounter_NoVoid_BP\".\"CaseClaimNumber\", \"vMQ4_Encounter_NoVoid_BP\".\"Parent_PayerName\", \"vMQ4_Encounter_NoVoid_BP\".\"PayerName\", \"vMQ4_Encounter_NoVoid_BP\".\"PatientFirstName\", \"vMQ4_Encounter_NoVoid_BP\".\"PatientLastName\", \"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_FirstName\", \"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_LastName\", \"vMQ4_Encounter_NoVoid_BP\".\"UA_NCM_FirstName\", \"vMQ4_Encounter_NoVoid_BP\".\"UA_NCM_LastName\", \"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\", \"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Status\", \"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Type\", \"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_display\", " + DBHelper.getLevenshtein_Stripped("\"vMQ4_Encounter_NoVoid_BP\".\"CaseClaimNumber\"", "?") + " as ls_case, " + DBHelper.getTrigramSimilarity_Stripped("\"vMQ4_Encounter_NoVoid_BP\".\"CaseClaimNumber\"", "?") + " as sim_case, " + DBHelper.getTrigramSimilarity_Stripped("\"vMQ4_Encounter_NoVoid_BP\".\"PatientLastName\"", "?") + " as sim_ln FROM \"vMQ4_Encounter_NoVoid_BP\" where " + DBHelper.getLevenshtein_Stripped("\"vMQ4_Encounter_NoVoid_BP\".\"CaseClaimNumber\"", "?") + "<3 AND (" + DBHelper.getTrigramSimilarity_Stripped("\"vMQ4_Encounter_NoVoid_BP\".\"PatientLastName\"", "?") + ">0.15 OR ?='') limit 10";
		//out.print(mySQL);
		%>
				<table width="100%" border="1" cellpadding="10" cellspacing="0">
		  <tr class=tdHeaderAlt>
			<td nowrap="nowrap" class=tdHeaderAlt>Action</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Claim</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Rec</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Payer</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Patient</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Adj</td>
			<td nowrap="nowrap" class=tdHeaderAlt>NCM</td>
			<td nowrap="nowrap" class=tdHeaderAlt>ScanPass</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Type</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Status</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Match</td>
		  </tr>
		
		<%
		try
		{
			db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,request.getParameter("fz_claimnumber")));
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,request.getParameter("fz_claimnumber")));
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,request.getParameter("fz_claimnumber")));
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,request.getParameter("fz_patientln")));
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,request.getParameter("fz_patientln")));
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,request.getParameter("fz_claimnumber")));
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,request.getParameter("fz_patientln")));
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,request.getParameter("fz_patientln")));
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,request.getParameter("fz_patientln")));
			myRS = mySS.executePreparedStatement(myDPSO);
			while (myRS!=null&&myRS.next())
			{
				String temp_ls_case = myRS.getString("ls_case");
				String temp_sim_case = myRS.getString("sim_case");
				String temp_sim_ln = myRS.getString("sim_ln");
				%>
		  <tr>
			<td nowrap="nowrap"><input type="button" value="Select" onClick="if (confirm('Are you sure this is the same case that is being referred?')){}"  ></td>
			<td nowrap="nowrap" <%if (temp_ls_case.equalsIgnoreCase("0")){%> class="tdBaseAltGreen" <%}%> ><%=myRS.getString("CaseClaimNumber")%>&nbsp;</td>
			<td nowrap="nowrap"><%=myRS.getString("Referral_ReceiveDate_display")%>&nbsp;</td>
			<td nowrap="nowrap"><%=myRS.getString("Parent_PayerName")%> - <%=myRS.getString("PayerName")%>&nbsp;</td>
			<td nowrap="nowrap" <%if (temp_sim_ln.equalsIgnoreCase("1")){%> class="tdBaseAltGreen" <%}%> ><%=myRS.getString("PatientFirstName")%> <%=myRS.getString("PatientLastName")%>&nbsp;</td>
			<td nowrap="nowrap"><%=myRS.getString("UA_Adjuster_FirstName")%> <%=myRS.getString("UA_Adjuster_LastName")%>&nbsp;</td>
			<td nowrap="nowrap"><%=myRS.getString("UA_NCM_FirstName")%> <%=myRS.getString("UA_NCM_LastName")%>&nbsp;</td>
			<td nowrap="nowrap"><%=myRS.getString("Encounter_ScanPass")%>&nbsp;</td>
			<td nowrap="nowrap"><%=myRS.getString("Encounter_Type")%>&nbsp;</td>
			<td nowrap="nowrap"><%=myRS.getString("Encounter_Status")%>&nbsp;</td>
			<td nowrap="nowrap">CN-Lev: <%=temp_ls_case%><br />CN-Sim: <%=temp_sim_case%><br />PN-Sim: <%=temp_sim_ln%></td>
		  </tr>
		
				<%
			}
		%>
		</table>
		
		<%	
		}
		catch(Exception e)
		{
			DebugLogger.println("NIMUtils:bltNIM3_Document:[MYSQL="+mySQL+"]:"+e);
		}
	}
	else if (sType.equalsIgnoreCase("ua_adj")||sType.equalsIgnoreCase("ua_rb"))
	{
								   
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
//		String mySQL = "select userid, contactfirstname, contactlastname, contactphone, contactemail, nim_usertype, payername, " + DBHelper.getTrigramSimilarity_Stripped("contactlastname", "?") + " as sim_ln, " + DBHelper.getTrigramSimilarity_Stripped("contactphone", "?") + " as sim_pn," + DBHelper.getTrigramSimilarity_Stripped("contactemail", "?") + " as sim_em  from tuseraccount INNER JOIN tnim3_payermaster on tnim3_payermaster.payerid = tuseraccount.payerid  where accounttype='AdjusterID' AND contactlastname>'' AND (" + DBHelper.getTrigramSimilarity_Stripped("contactlastname", "?") + ">0.2 OR (" + DBHelper.getTrigramSimilarity_Stripped("contactlastname", "?") + ">0.05 AND " + DBHelper.getTrigramSimilarity_Stripped("contactphone", "?") + ">0.5 AND contactphone>'') OR (" + DBHelper.getTrigramSimilarity_Stripped("contactemail", "?") + ">.5 AND contactemail <> 'Declined' AND contactemail > '')) order by " + DBHelper.getTrigramSimilarity_Stripped("contactlastname", "?") + " + " + DBHelper.getTrigramSimilarity_Stripped("contactemail", "?") + " + " + DBHelper.getTrigramSimilarity_Stripped("contactphone", "?") + " desc      limit 10";
		String mySQL = "select userid, contactfirstname, contactlastname, contactphone, contactemail, nim_usertype, payername, " + DBHelper.getTrigramSimilarity_Stripped("contactlastname", "?") + " as sim_ln, " + DBHelper.getTrigramSimilarity_Stripped("contactfirstname", "?") + " as sim_fn, " + DBHelper.getTrigramSimilarity_Stripped("contactphone", "?") + " as sim_pn," + DBHelper.getTrigramSimilarity_Stripped("contactemail", "?") + " as sim_em  from tuseraccount INNER JOIN tnim3_payermaster on tnim3_payermaster.payerid = tuseraccount.payerid  where accounttype='AdjusterID' AND contactlastname>'' AND (" + DBHelper.getTrigramSimilarity_Stripped("contactlastname", "?") + " + " + DBHelper.getTrigramSimilarity_Stripped("contactfirstname", "?") + "*0.75 + " + DBHelper.getTrigramSimilarity_Stripped("contactemail", "?") + " + " + DBHelper.getTrigramSimilarity_Stripped("contactphone", "?") + ")>0.4 order by (" + DBHelper.getTrigramSimilarity_Stripped("contactlastname", "?") + " + " + DBHelper.getTrigramSimilarity_Stripped("contactfirstname", "?") + "*0.75 + " + DBHelper.getTrigramSimilarity_Stripped("contactemail", "?") + " + " + DBHelper.getTrigramSimilarity_Stripped("contactphone", "?") + ") desc      limit 5";
//		out.print(mySQL);
		%>
				<table width="100%" border="1" cellpadding="3" cellspacing="0">
		  <tr class=tdHeaderAlt>
			<td nowrap="nowrap" class=tdHeaderAlt>Action</td>
			<td nowrap="nowrap" class=tdHeaderAlt>UserID</td>
			<td nowrap="nowrap" class=tdHeaderAlt>First</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Last</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Phone</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Email</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Type</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Payer</td>
			<td nowrap="nowrap" class=tdHeaderAlt>Match</td>
		  </tr>
		
		
		  <tr>
			<td nowrap="nowrap">y
			  <input type="button" value="Select" onClick="if (confirm('Are you sure this is the same user?')){document.getElementById('AdjusterID').value='<%=myRS.getString("userid")%>';disableInput_UA('Adjuster');alert('Now Load Zoom Display and enable Edit');}"  ></td>
			<td nowrap="nowrap"><%=myRS.getString("userid")%>&nbsp;</td>
			<td nowrap="nowrap" <%if (temp_sim_fn>=0.75){%> class="tdBaseAltGreen" <%}%> ><%=myRS.getString("contactfirstname")%>&nbsp;</td>
			<td nowrap="nowrap" <%if (temp_sim_ln>=1){%> class="tdBaseAltGreen" <%}%> ><%=myRS.getString("contactlastname")%>&nbsp;</td>
			<td nowrap="nowrap" <%if (temp_sim_pn>=1){%> class="tdBaseAltGreen" <%}%> ><%=myRS.getString("contactphone")%>&nbsp;</td>
			<td nowrap="nowrap" <%if (temp_sim_em>=1){%> class="tdBaseAltGreen" <%}%> ><%=myRS.getString("contactemail")%>&nbsp;</td>
			<td nowrap="nowrap"><%=myRS.getString("nim_usertype")%>&nbsp;</td>
			<td nowrap="nowrap"><%=myRS.getString("payername")%> - <%=myRS.getString("PayerName")%>&nbsp;</td>
			<td nowrap="nowrap"><span class="tdHeader">Score: <%=temp_sim_ln+temp_sim_fn+temp_sim_pn+temp_sim_em%></span><% if (isDebug){%><br />sim_ln: <%=temp_sim_ln%><br />sim_fn: <%=temp_sim_fn%><br />sim_pn: <%=temp_sim_pn%><br />sim_em: <%=temp_sim_em%><%}%></td>
		  </tr>
		
				<%
			}
		%>
		</table>
		
		<%	
		}
		catch(Exception e)
		{
			out.print(e);
			DebugLogger.println("NIMUtils:bltNIM3_Document:[MYSQL="+mySQL+"]:"+e);
		}
	}
}
else
{
	%>
    Not Authenticated
<%
}
		
%>