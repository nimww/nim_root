<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<%@ include file="../generic/CheckLogin.jsp" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>NIMPo - Awesome</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>

<style>
body{
	padding:0;
	margin:0;
	font-family:Verdana, Geneva, sans-serif;
}
#banner{
	background-color: #000;
	/*background-image: -webkit-linear-gradient(0deg, rgba(255,255,255,.07) 50%, transparent 50%), -webkit-linear-gradient(0deg, rgba(255,255,255,.13) 50%, transparent 50%), -webkit-linear-gradient(0deg, transparent 50%, rgba(255,255,255,.17) 50%), -webkit-linear-gradient(0deg, transparent 50%, rgba(255,255,255,.19) 50%);
	background-size: 13px, 29px, 37px, 53px;*/
	border-bottom: 2px solid black;	
}
.statsBox{
	border: 1px solid black;
	overflow: hidden;
	padding: 5px;	
	margin: 0;
	background: #3ab1e2; /* Old browsers */
	background: -moz-linear-gradient(top, #3ab1e2 0%, #a0ddf3 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#3ab1e2), color-stop(100%,#a0ddf3)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #3ab1e2 0%,#a0ddf3 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #3ab1e2 0%,#a0ddf3 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #3ab1e2 0%,#a0ddf3 100%); /* IE10+ */
	background: linear-gradient(to bottom, #3ab1e2 0%,#a0ddf3 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3ab1e2', endColorstr='#a0ddf3',GradientType=0 ); /* IE6-9 */
	min-width:1300px;
}
.statsBox2{
	border: 1px solid black;
	overflow: hidden;
	padding: 5px;	
	margin: 0;
	background: #d3f7fe; /* Old browsers */
	background: -moz-linear-gradient(top, #d3f7fe 0%, #eafbff 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d3f7fe), color-stop(100%,#eafbff)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #d3f7fe 0%,#eafbff 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #d3f7fe 0%,#eafbff 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #d3f7fe 0%,#eafbff 100%); /* IE10+ */
	background: linear-gradient(to bottom, #d3f7fe 0%,#eafbff 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d3f7fe', endColorstr='#eafbff',GradientType=0 ); /* IE6-9 */
	min-width:1300px;
}
.statsBox3{
	overflow:hidden;
	min-width:1300px;	
}
.stats-widget{
	padding: 10px;
	margin: 10px 5px;
	border: 1px solid black;
	background: #F1F0FF;
	float: left;	
	position: relative;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
	border-radius:4px;
}
.wl-widget{
	padding: 10px;
	margin: 5px;
	border: 1px solid black;
	background: #c3bffa;
	position: relative;
}
.stats-widget-title, .stats-widget-title-options{
	position: absolute;
	top: -10px;
	background: #50ddfc;
	padding: 4px;
	border: 1px solid black;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
	border-radius:4px;	
}
.stats-widget-title-options{
	float: right;
	top: -15px;
	right: 10px;
	background: transparent;
	border:0;
}
.columns{
	columns:30px 2;
	-webkit-columns:30px 2;
	-moz-columns:30px 2;
	margin:1px;
}
#stats1-body{
	padding: 13px;
}
#stats2-body, #stats3-body{
	width: 347px;
	padding: 13px;	
}
.select-style{
	border: 1px solid black;
	-webkit-appearance: none;
   -moz-appearance:    none;
   appearance:         none;
	padding: 5px;	
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
}
.select-style:hover{
	background:#9fe4f3;
}
.right{
	float:right;
}
.left{
	float:left;
}
.icon:hover{
	cursor:pointer;	
}
.icon:active{
	position:relative;
	top:2px;
	cursor:pointer;	
}
#banner li {
	display: inline;
	list-style: none;
	padding: 10px;
	border-radius: 4px;
	cursor:pointer;
}
#banner ul {
	display: inline;
}
.custom-wl-submit {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #91ffbd), color-stop(1, #74d6a2) );
	background:-moz-linear-gradient( center top, #91ffbd 5%, #74d6a2 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#91ffbd', endColorstr='#74d6a2');
	background-color:#91ffbd;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #cfcfcf;
	display:inline-block;
	color:#ffffff;
	font-family:Verdana;
	font-size:13px;
	font-weight:bold;
	padding:6px 10px;
	text-decoration:none;
	text-shadow:1px 1px 3px #000000;
}.custom-wl-submit:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #74d6a2), color-stop(1, #91ffbd) );
	background:-moz-linear-gradient( center top, #74d6a2 5%, #91ffbd 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#74d6a2', endColorstr='#91ffbd');
	background-color:#74d6a2;
}.custom-wl-submit:active {
	position:relative;
	top:1px;
}
#default-homepage table{
	font-size: 12px;
	border: 1px solid;
	border-radius: 5px;
	border-spacing: 0;	
	margin-top: 20px;
}
#default-homepage td{
	border-top: 0;
	border-bottom: 1px solid;
	border-left: 0px solid;
	border-right: 1px solid;
	padding: 2px;
}
</style>
<script>
$(document).ready(function(){
	$('#custom-hide').click(function(){
		$('#custom-worklist').slideUp();
	});
	$('#custom-show').click(function(){
		$('#custom-worklist').slideDown();
	});
	//$('#custom-worklist').load('nimpo-wl.jsp?wl=2');
	$('#unassigned-cases').load('nimpo/nimpo-unassigned-cases.jsp');
	$('#opencomm').load('nimpo/nimpo-opencomm.jsp');
	$('#openref').load('nimpo/nimpo-limbo.jsp');
	$('#needsched').load('nimpo/nimpo-needsched.jsp');
	$('#postappt').load('nimpo/nimpo-postappt.jsp');
	$('#rxrw').load('nimpo/nimpo-rxrw.jsp');
	$('#nextactions').load('nimpo/nimpo-nextactions.jsp');
	
	function loadWorkList(){
		$("#custom-worklist").html("<table style=\"background:#fff url('images/load_snail.gif') center no-repeat;height:600px;width:96%;margin:0 auto;\"></table>");
		var wlType = $('select#wl-type option:selected').val();
		var wlScheduler = $('select#wl-scheduler option:selected').val();
		var dataString = "wl="+wlType+"&scheduler="+wlScheduler;
		
		$.ajax({
			type: "POST",
			url: "nimpo-wl.jsp",
			data: dataString,
			success: function() {
				
			}
		}).done(function(html){
			$("#custom-worklist").html(html);
		});	
	}
});
</script>
</head>

<body>
<%if (true){%>
<div style="margin:0;padding:10px;color:white;" id="banner">NextImage Medical 
<ul>
	<li style="background: #333;">Dashboard</li>
    <li>Maintenance</li>
    <li>Settings</li>
</ul>

</div>

<div class="statsBox">
	<div class="stats-widget" id="stats1">
        <div id="stats1-title" class="stats-widget-title">
        	Scheduling Stats
        </div>
        <div id="stats1-options" class="stats-widget-title-options">
        	<img src="images/refresh.png" class="icon">
        </div>
       
        <div id="stats1-body">
			<%try{
                searchDB2 db = new searchDB2();
                java.sql.ResultSet result = null;
                String query ="select  (select count(distinct referral.referralid) from tnim3_encounter as \"encounter\" inner join tnim3_referral as \"referral\" on referral.referralid = encounter.referralid inner join tnim3_caseaccount as \"caseaccount\" on caseaccount.caseid = referral.caseid inner join tuseraccount as \"assignedto\" on assignedto.userid = caseaccount.assignedtoid where (encounter.encounterstatusid ='1' or encounter.encounterstatusid = '12') and encounter.uniquecreatedate > (now() - '3 month'::interval) and appointmentid = 0) as \"Active\",  (select count(DISTINCT tnim3_encounter.encounterid) from tnim3_encounter inner join tnim3_referral on tnim3_referral.referralid = tnim3_encounter.referralid  where tNIM3_Encounter.encounterstatusid in ('1','12') and tnim3_encounter.uniquecreatedate > (now() - '3 month'::interval)  and rxfileid=0) as \"NeedRx\", (select count(DISTINCT tnim3_caseaccount.caseid)  from tnim3_caseaccount inner join tnim3_referral on tnim3_referral.caseid = tnim3_caseaccount.caseid  RIGHT JOIN tnim3_encounter on tnim3_encounter.referralid = tnim3_referral.referralid left join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid where cast(patientdob as VARCHAR) not like '1800%' and cast(dateofinjury as VARCHAR) not like '1800%'  and adjusterid != 0  and  patientfirstname !='' and patientzip != '' and patientstateid !=0 and (patienthomephone !='' or patientcellphone !='') and caseclaimnumber !='' and  cast(appointmenttime as VARCHAR)  is null and encounterstatusid = '1' and tnim3_encounter.uniquecreatedate > (now() - '3 month'::interval) )as \"ReadyToSched\", (select count(DISTINCT encounterid) from tnim3_encounter inner join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid where encounterstatusid ='1' and tnim3_encounter.uniquecreatedate > (now() - '3 month'::interval) and appointmenttime < now() -'1 day':: INTERVAL and cast(appointmenttime as VARCHAR) not like '1800%' and istatus != 4 and reportfileid=0 and tnim3_encounter.appointmentid >0) as \"NeedRp\", (select count(DISTINCT encounterid) from tnim3_encounter inner join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid where encounterstatusid ='1' and tnim3_encounter.uniquecreatedate > (now() - '3 month'::interval) and appointmenttime > now() and cast(appointmenttime as VARCHAR) not like '1800%' and istatus != 4 and reportfileid=0 and tnim3_encounter.appointmentid >0) as \"AppointmentPend\", (select  count(DISTINCT encounterid)  from tnim3_encounter inner join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid where encounterstatusid in ('1', '12') and tnim3_encounter.uniquecreatedate > (now() - '3 month'::interval) and appointmenttime < now() and reportfileid!=0 and timetrack_reqrpreview_userid=0)  as \"RpNeedsRw\", (select count(DISTINCT tnim3_commtrack.encounterid)  from tnim3_commtrack INNER JOIN tnim3_encounter on tnim3_encounter.encounterid = tnim3_commtrack.encounterid where encounterstatusid ='9' and (messagetext LIKE '%Billing: In Progress%' or messagetext LIKE '%Moved to Billing%') and tnim3_commtrack.uniquecreatedate > (now() - '1 day'::interval)) as \"BillingInLastDay\" , (select  count(DISTINCT tnim3_commtrack.encounterid)  from tnim3_commtrack INNER JOIN tnim3_encounter on tnim3_encounter.encounterid = tnim3_commtrack.encounterid where encounterstatusid ='9' and (messagetext LIKE '%Billing: In Progress%' or messagetext LIKE '%Moved to Billing%') and tnim3_commtrack.uniquecreatedate > (now() - '7 day'::interval)) as \"BillingInLastWeek\", (select  count(DISTINCT tnim3_commtrack.encounterid)  from tnim3_commtrack INNER JOIN tnim3_encounter on tnim3_encounter.encounterid = tnim3_commtrack.encounterid where encounterstatusid ='9' and (messagetext LIKE '%Billing: In Progress%' or messagetext LIKE '%Moved to Billing%') and tnim3_commtrack.uniquecreatedate > (now() - '1 month'::interval)) as \"BillingInLastMonth\", (select count(encounterid) from tnim3_encounter where encounterstatusid='12') AS \"Escalated\", (select count(encounterid) from tnim3_encounter where seenetdev_waiting ='1') AS \"In NetDev\", ( select count(tNIM3_Encounter.encounterid) from tNIM3_Encounter  INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN (select distinct tnim3_service.encounterid from tnim3_service)as service on service.encounterid = tnim3_encounter.encounterid  INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid  where tNIM3_Encounter.AppointmentID=0 AND tnim3_referral.rxfileid>0 AND tnim3_referral.orderfileid>0 AND tnim3_referral.referringphysicianid>0 AND tnim3_caseaccount.adjusterid>0 AND tnim3_caseaccount.patientstateid>0  AND tnim3_caseaccount.patientaddress1 != '' AND tNIM3_encounter.timetrack_reqrxreview_userid=0 AND tnim3_referral.referralstatusid = '1' AND tNIM3_Encounter.encounterstatusid not in (0,2,3,5,6,9,10))   as \"Rx Review\"  ";
                result = db.executeStatement(query);
  
                while (result!=null&&result.next())
                {%>
                    <table>
                        <tr>
                            <td>Active</td><td><%=result.getString("Active")%></td>
                            <td style="padding-left:15px">Billing In Last Day</td><td><%=result.getString("BillingInLastDay")%></td>
                        </tr>
                        <tr>
                            <td>Need Rx</td><td><%=result.getString("NeedRx")%></td>
                            <td style="padding-left:15px">Billing In Last Week</td><td><%=result.getString("BillingInLastWeek")%></td>
                        </tr>
                        <tr>
                            <td>Need Rx Rw</td><td><%=result.getString("Rx Review")%></td>
                            <td style="padding-left:15px">Billing In Last Month</td><td><%=result.getString("BillingInLastMonth")%></td>
                        </tr>
                        <tr>
                            <td>Ready To Sched</td><td><%=result.getString("ReadyToSched")%></td>
                            <td style="padding-left:15px">Escalated Cases</td><td><%=result.getString("Escalated")%></td>
                        </tr>
                        <tr>
                            <td>Need Rp</td><td><%=result.getString("NeedRp")%></td>
                            <td style="padding-left:15px">In NetDev</td><td><%=result.getString("In NetDev")%></td>
                        </tr>
                        <tr>
                            <td>Rp Needs Rw</td><td><%=result.getString("RpNeedsRw")%></td>
                            <td style="padding-left:15px">Appointment Pend.</td><td><%=result.getString("AppointmentPend")%></td>
                        </tr>
                    </table>
                <%}
                db.closeAll();
            }
            catch (Exception e){}%>
        </div>
	</div>
    
	<div class="stats-widget" id="stats2">
    	<div id="stats2-title" class="stats-widget-title">
        	Number cases assigned 
        </div>
        <div id="stats2-options" class="stats-widget-title-options">
        	<img src="images/refresh.png" class="icon">
        </div>
        <div id="stats2-body">
			<%try{
                searchDB2 db = new searchDB2();
                java.sql.ResultSet result = null;;
                String query ="select assignedto.contactfirstname as \"scheduler\", count(assignedtoid) as \"assignedcases\" from tnim3_encounter as encounter inner join tnim3_referral as referral on referral.referralid = encounter.referralid inner join tnim3_caseaccount as caseaccount on caseaccount.caseid = referral.caseid inner join tuseraccount as assignedto on caseaccount.assignedtoid = assignedto.userid where lower(assignedto.logonusername) like '%nim3a%' and encounter.encounterstatusid in (1,12,11,8) and encounter.appointmentid = 0 group by assignedtoid, assignedto.contactfirstname,assignedto.logonusername order by count(assignedtoid) desc";
                result = db.executeStatement(query);
				boolean column = true;%>

				<table style="width:100%">
                <%while (result!=null&&result.next())
                {	
					if(column){%>
                    <tr><td><%=result.getString("scheduler")%></td><td><%=result.getString("assignedcases")%></td>
                    <%column=false;
					}else{%>
                    <td><%=result.getString("scheduler")%></td><td><%=result.getString("assignedcases")%></td></tr>
                    <%column=true;
					}%>
                <%}
                db.closeAll();
            }
            catch (Exception e){}%>
            	</table>
        </div>
	</div>
    
    <div class="stats-widget" id="stats3">
    	<div id="stats3-title" class="stats-widget-title">
       		Cases scheduled today 
        </div>
        <div id="stats3-options" class="stats-widget-title-options">
        	<img src="images/refresh.png" class="icon">
        </div>
        <div id="stats3-body">
			<%try{
                searchDB2 db = new searchDB2();
                java.sql.ResultSet result = null;;
                String query ="select assignedto.contactfirstname as \"scheduler\", count(assignedto.contactfirstname) as \"scheduled\" from tnim3_appointment as appointment inner join tnim3_encounter as encounter on encounter.encounterid = appointment.initialencounterid inner join tuseraccount as assignedto on assignedto.userid = appointment.scheduler_userid where lower(assignedto.logonusername) like '%nim3a%' and encounter.encounterstatusid in (1,12,11,8) and appointment.uniquecreatedate > CURRENT_DATE group by assignedto.contactfirstname,assignedto.logonusername order by count(assignedto.contactfirstname) desc";
                result = db.executeStatement(query);
                boolean column = true;%>

				<table style="width:100%">
                <%while (result!=null&&result.next())
                {	
					if(column){%>
                    <tr><td><%=result.getString("scheduler")%></td><td><%=result.getString("scheduled")%></td>
                    <%column=false;
					}else{%>
                    <td><%=result.getString("scheduler")%></td><td><%=result.getString("scheduled")%></td></tr>
                    <%column=true;
					}%>
                <%}
                db.closeAll();
            }
            catch (Exception e){}%>
            	</table>
        </div>
	</div>
</div>
<div id="custom-worklist-select" class="statsBox2">
    <div id="custom-worklist-select" class="">
        <span style="font-size: 13px;">View</span>
        <select class="select-style" name="wl-type" id="wl-type">
            <option value="1">Receipt Confirmations</option>
            <option value="2">Needs PreScreen</option>
            <option value="3">Missing Rx</option>
            <option value="4">Needs Rx Review</option>
            <option value="5">Ready To Schedule</option>
            <option value="6">Needs Report Review</option>
            <option value="7">Still Active: Why?</option>
            <option value="8">Hold Bill</option>
            <option value="9">Next Action Date Due</option>
            <option value="10">Unassigned Cases</option>
            <option value="11">Stat Cases</option>
            <option value="12">Open CPT 76140</option>
            <option value="13">Referral Status Pending or N/A</option>
            <option value="15">Missing reports</option>
            <option value="16">Escalated Cases</option>
            <option value="17">See Netdev waiting</option>
        </select>
        <span style="font-size: 13px;">for</span>
        <select class="select-style" name="wl-scheduler" id="wl-scheduler">
            <option value="0">Everybody</option>
            <%try{
                searchDB2 db = new searchDB2();
                java.sql.ResultSet result = null;;
                String query ="select contactfirstname||' '||contactlastname as \"scheduler\", userid from tuseraccount where lower(logonusername) ~ 'nim3a' and lower(logonusername) !~ 'rem' and status = 2";
                result = db.executeStatement(query);
                
                while (result!=null&&result.next())
                {%>
                    <option value="<%=result.getString("userid")%>"><%=result.getString("scheduler")%></option>
                <%}
                db.closeAll();
            }
            catch (Exception e){}%>
        </select>
        <input type="button" class="custom-wl-submit" value="go" onClick="loadWorkList();">
        <span class="right"><img src="images/refresh.png" class="icon" id="custom-refresh"><img src="images/hide.png" class="icon" id="custom-hide"><img src="images/show.png" class="icon" id="custom-show"></span>
    </div>
    <div id="custom-worklist" class="" style="font-size:11px"></div>
</div>

<div id="default-homepage" class="statsBox3">
    <div style="width:48%;padding: 10px;" class="left">
    	<div class="stats-widget" id="unassigned-cases" style="width:100%"></div>
        <div class="stats-widget" id="needsched" style="width:100%"></div>
        <div class="stats-widget" id="rxrw" style="width:100%"></div>
        
    </div>
    <div style="width:48%;padding: 10px;" class="right">
    	
        <div class="stats-widget" id="opencomm" style="width:96%"></div>
        <div class="stats-widget" id="postappt" style="width:96%"></div>
        <div class="stats-widget" id="nextactions" style="width:96%"></div>
        <%if (CurrentUserAccount.getUserID()==23610){%>
        <div class="stats-widget" id="openref" style="width:96%"></div>
        <%}%>
    </div>
</div>

<%}%>


 

</body>
</html>