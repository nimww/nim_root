<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*    " %>
<%/*
    filename: out\jsp\tNIM3_CaseAccount_form_sub.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
        if (iEncounterID.intValue() != iEDITID.intValue())
        {
        	accessValid = false;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);
		

//    bltNIM3_CaseAccount        myEO.NIM3_CaseAccount        =    new bltNIM3_CaseAccount(iCaseID);
//    bltNIM3_Encounter        myEO.NIM3_Encounter        =    new bltNIM3_Encounter(iEncounterID);
//    bltNIM3_Referral        myEO.NIM3_Referral        =    new bltNIM3_Referral(iReferralID);
	boolean isReferralGo = false;


      String tCaseAccount_tempAN =	myEO.NIM3_CaseAccount.getAuditNotes();
	  String tCaseAccount_tempAN_Status = "";

String tCaseAccount_testChangeID = "0";

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !myEO.NIM3_CaseAccount.getComments().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setComments( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount Comments not set. this is ok-not an error");
}

boolean errorRouteMe = false;
if (tCaseAccount_testChangeID.equalsIgnoreCase("1"))
{

	myEO.NIM3_CaseAccount.setUniqueModifyDate(PLCUtils.getNowDate(false));
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
	myEO.NIM3_CaseAccount.setUniqueModifyComments(UserLogonDescription);
	try
	{
		myEO.NIM3_CaseAccount.setAuditNotes(tCaseAccount_tempAN);
		myEO.NIM3_CaseAccount.commitData();
	}
	catch (Exception e55)
	{
		errorRouteMe = true;
	}



}

if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else 
{
	    %><script language=Javascript>
  		   alert("Case Notes Updated" );
	      document.location="ui_200/top-nav_sched_cond.jsp";
	      </script><%
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
