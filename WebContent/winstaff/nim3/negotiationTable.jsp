<%@page contentType="text/html" language="java" import="com.winstaff.*,org.apache.http.*,java.io.*,java.util.List,com.winstaff.NegotiationTable.*, java.util.LinkedHashMap"%>
<!-- @author Hoppho Le -->
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
<title>Negotiation Table</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script	src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="js/jquery.tablesorter.min.js"></script>
<style>
#ic_header {
	top: 0px;
	display: none;
}
.fixed {
	padding: 8px;
	line-height: 20px;
	text-align: left;
	vertical-align: top;
	border: 1px solid #dddddd;
	width: 408px;
	display: inline-block;
	margin-right: -5px;
	font-weight: bold;
	background: #fff;
	cursor: pointer;
}
tr#ic_tr th, tr#payer_tr th{
	cursor:pointer;
}
</style>
</head>

<body>
<%
		if (request.getParameter("zip") != null && request.getParameter("dist") != null) {

		NegotiationTable nt = new NegotiationTable(request.getParameter("zip"), new Integer(request.getParameter("dist")));
		LinkedHashMap<String, String> fsid1 = nt.getFSID(887);
		LinkedHashMap<String, String> fsid7 = nt.getFSID(888);
		List<IClist> iclist = nt.getIClist();
		List<Payerlist> payerlist = nt.getPayerist();
	%>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span1">
      <form>
        <label for="zip">
        <h4>Zip</h4>
        </label>
        <input type="number" name="zip" value="<%=request.getParameter("zip")%>" class="span12">
        <br>
        <label for="dist">
        <h4>Distance</h4>
        </label>
        <input type="number" name="dist" value="<%=request.getParameter("dist")%>" class="span12">
        <br>
        <input type="submit" class="btn">
      </form>
    </div>
    <div class="span5">
      <h3>Work Comp FSID 1</h3>
      <table class="table table-striped table-bordered">
        <tr>
          <th>&nbsp;</th>
          <th>Without</th>
          <th>With</th>
          <th>With/Without</th>
        </tr>
        <tr>
          <td><strong>MR</strong></td>
          <td><%=fsid1.get("MRwo")%></td>
          <td><%=fsid1.get("MRw")%></td>
          <td><%=fsid1.get("MRwwo")%></td>
        </tr>
        <tr>
          <td><strong>CT</strong></td>
          <td><%=fsid1.get("CTwo")%></td>
          <td><%=fsid1.get("CTw")%></td>
          <td><%=fsid1.get("CTwwo")%></td>
        </tr>
      </table>
    </div>
    <div class="span5">
      <h3>National Pricing FSID 7</h3>
      <table class="table table-striped table-bordered">
        <tr>
          <th>&nbsp;</th>
          <th>Without</th>
          <th>With</th>
          <th>With/Without</th>
        </tr>
        <tr>
          <td><strong>MR</strong></td>
          <td><%=fsid7.get("MRwo")%></td>
          <td><%=fsid7.get("MRw")%></td>
          <td><%=fsid7.get("MRwwo")%></td>
        </tr>
        <tr>
          <td><strong>CT</strong></td>
          <td><%=fsid7.get("CTwo")%></td>
          <td><%=fsid7.get("CTw")%></td>
          <td><%=fsid7.get("CTwwo")%></td>
        </tr>
      </table>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span6">
      <h3>IC Rates</h3>
      <div style="position: absolute;" id="ic_header"> 
      <span id="ic_header_name" th="0" table="table-1" class="fixed">Practice Name</span> 
      <span id="ic_header_mrwo" th="1" table="table-1" class="fixed">MR_WO</span> 
      <span id="ic_header_mrw" th="2" table="table-1" class="fixed">MR_W</span> 
      <span id="ic_header_mrwwo" th="3" table="table-1" class="fixed">MR_WWO</span> 
      <span id="ic_header_ctwo" th="4" table="table-1" class="fixed">CT_WO</span> 
      <span id="ic_header_ctw" th="5" table="table-1" class="fixed">CT_W</span> 
      <span id="ic_header_ctwwo" th="6" table="table-1" class="fixed">CT_WWO</span>
      
       </div>
      <table class="table table-striped table-bordered tablesorter" id="table-1">
        <thead>
          <tr class="" id="ic_tr">
            <th id="ic_name">Practice Name</th>
            <th id="ic_mrwo">MR_WO</th>
            <th id="ic_mrw">MR_W</th>
            <th id="ic_mrwwo">MR_WWO</th>
            <th id="ic_ctwo">CT_WO</th>
            <th id="ic_ctw">CT_W</th>
            <th id="ic_ctwwo">CT_WWO</th>
          </tr>
        </thead>
        <tbody>
          <%
							for (IClist l : iclist) {
						%>
          <tr>
            <td><%=l.getPracticeName()%></td>
            <td><%=l.getPriceMRwo()%></td>
            <td><%=l.getPriceMRw()%></td>
            <td><%=l.getPriceMRwwo()%></td>
            <td><%=l.getPriceCTwo()%></td>
            <td><%=l.getPriceCTw()%></td>
            <td><%=l.getPriceCTwwo()%></td>
          </tr>
          <%
						}
					%>
					
					
					<%
					 IClist list = null; 
						
					%>	
						<% 
						
						%>
						<tr>
          					<td><%  %></td>
          				</tr>
					<% 
					
					%>	
							<tr>
								<td>Average</td>
							</tr>
					
        </tbody>
        
      </table>
    </div>
    <div class="span6">
      <h3>Payer Allows</h3>
      <div style="position: absolute;" id="payer_header"> 
      <span id="payer_header_name" th="0" table="table-2" class="fixed">Payer Name</span> 
      <span id="payer_header_mrwo" th="1" table="table-2" class="fixed">MR_WO</span> 
      <span id="payer_header_mrw" th="2" table="table-2" class="fixed">MR_W</span> 
      <span id="payer_header_mrwwo" th="3" table="table-2" class="fixed">MR_WWO</span> 
    <span id="payer_header_ctwo" th="4" table="table-2" class="fixed">CT_WO</span> 
    <span id="payer_header_ctw" th="5" table="table-2" class="fixed">CT_W</span> 
    <span id="payer_header_ctwwo" th="6" table="table-2" class="fixed">CT_WWO</span> 
    <span id="payer_header_v11" th="7" table="table-2" class="fixed">Vol_2011</span> 
    <span id="payer_header_v12" th="8" table="table-2" class="fixed">Vol_2012</span> 
    <span id="payer_header_v13" th="9" table="table-2" class="fixed">Vol_2013</span> </div>
      <table class="table table-striped table-bordered" id="table-2">
        <thead>
          <tr id="payer_tr">
            <th id="payer_name">Payer Name</th>
            <th id="payer_mrwo">MR_WO</th>
            <th id="payer_mrw">MR_W</th>
            <th id="payer_mrwwo">MR_WWO</th>
            <th id="payer_ctwo">CT_WO</th>
            <th id="payer_ctw">CT_W</th>
            <th id="payer_ctwwo">CT_WWO</th>
            <th id="payer_v11">Vol_2011</th>
            <th id="payer_v12">Vol_2012</th>
            <th id="payer_v13">Vol_2013</th>
          </tr>
        </thead>
        <tbody>
          <%
						for (Payerlist p : payerlist) {
					%>
          <tr>
            <td class="payer_name"><%=p.getPayerName()%></td>
            <td class="payer_mrwo"><%=p.getPriceMRwo()%></td>
            <td class="payer_mrw"><%=p.getPriceMRw()%></td>
            <td class="payer_mrwwo"><%=p.getPriceMRwwo()%></td>
            <td class="payer_ctwo"><%=p.getPriceCTwo()%></td>
            <td class="payer_ctw"><%=p.getPriceCTw()%></td>
            <td class="payer_ctwwo"><%=p.getPriceCTwwo()%></td>
            <td class="payer_v11"><%=p.getVolume2011()%></td>
            <td class="payer_v12"><%=p.getVolume2012()%></td>
            <td class="payer_v13"><%=p.getVolume2013()%></td>
          </tr>
          <%
						}
					%>
        </tbody>
      </table>
    </div>
  </div>
</div>
<%
		} else {
	%>
<div class="container">
  <div class="row-fluid">
    <div class="span12">
      <form>
        <label for="zip">
        <h4>Zip</h4>
        </label>
        <input type="number" name="zip">
        <label for="dist">
        <h4>Distance</h4>
        </label>
        <input type="number" name="dist" value="30">
        <br>
        <input type="submit" class="btn">
      </form>
    </div>
  </div>
</div>
<%
		}
	%>
<script>
		var icTableOffset = $("#table-1").offset().top;
		var $ic_header = $("#ic_header");
		var $payer_header = $("#payer_header");
		var payer_width = $('#table-2').width();

		$('#ic_header_name').width($('#ic_name').width());
		$('#ic_header_mrwo').width($('#ic_mrwo').width());
		$('#ic_header_mrw').width($('#ic_mrw').width());
		$('#ic_header_mrwwo').width($('#ic_mrwwo').width());
		$('#ic_header_ctwo').width($('#ic_ctwo').width());
		$('#ic_header_ctw').width($('#ic_ctw').width());
		$('#ic_header_ctwwo').width($('#ic_ctwwo').width());

		$('#payer_header_name').width($('#payer_name').width());
		$('#payer_header_mrwo').width($('#payer_mrwo').width());
		$('#payer_header_mrw').width($('#payer_mrw').width());
		$('#payer_header_mrwwo').width($('#payer_mrwwo').width());
		$('#payer_header_ctwo').width($('#payer_ctwo').width());
		$('#payer_header_ctw').width($('#payer_ctw').width());
		$('#payer_header_ctwwo').width($('#payer_ctwwo').width());
		$('#payer_header_v11').width($('#payer_v11').width());
		$('#payer_header_v12').width($('#payer_v12').width());
		$('#payer_header_v13').width($('#payer_v13').width());
		$payer_header.css('width', payer_width);

		$('#table-1').tablesorter();
		$('#table-2').tablesorter();

		$(window).bind("scroll", function() {
			var windowOffset = $(this).scrollTop();
			var icBottom = icTableOffset + $("#table-1").height();
			var payerBottom = icTableOffset + $("#table-2").height();

			if (windowOffset >= icTableOffset && windowOffset <= icBottom) {
				$ic_header.show();
				$ic_header.css("top", windowOffset);

			} else {
				$ic_header.hide();
			}

			if (windowOffset >= icTableOffset && windowOffset <= payerBottom) {
				$payer_header.show();
				$payer_header.css("top", windowOffset);//.css('width',payer_width);
			} else {
				$payer_header.hide();
			}

			//alert(offset+" | "+tableOffset+" | "+tableBottom);
		});

		$(window).bind("resize", function() {
			$('#ic_header_name').width($('#ic_name').width());
			$('#ic_header_mrwo').width($('#ic_mrwo').width());
			$('#ic_header_mrw').width($('#ic_mrw').width());
			$('#ic_header_mrwwo').width($('#ic_mrwwo').width());
			$('#ic_header_ctwo').width($('#ic_ctwo').width());
			$('#ic_header_ctw').width($('#ic_ctw').width());
			$('#ic_header_ctwwo').width($('#ic_ctwwo').width());
			$('#payer_header_name').width($('#payer_name').width());
			$('#payer_header_mrwo').width($('#payer_mrwo').width());
			$('#payer_header_mrw').width($('#payer_mrw').width());
			$('#payer_header_mrwwo').width($('#payer_mrwwo').width());
			$('#payer_header_ctwo').width($('#payer_ctwo').width());
			$('#payer_header_ctw').width($('#payer_ctw').width());
			$('#payer_header_ctwwo').width($('#payer_ctwwo').width());
			$('#payer_header_v11').width($('#payer_v11').width());
			$('#payer_header_v12').width($('#payer_v12').width());
			$('#payer_header_v13').width($('#payer_v13').width());
			$payer_header.css('width', $('#table-2').width());
		});
		
		$(".fixed").click(function() {
			$("table#"+$(this).attr("table")+" thead").find("th:eq("+$(this).attr("th")+")").trigger("sort");
			return false;
		  });
		
		
		
	</script>
</body>
</html>
