<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.* " %>
<%/*
    filename: out\jsp\tUserAccount_form_sub.jsp
    Created on Jun/26/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

  //page security
  if (true)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltUserAccount        UserAccount        =    CurrentUserAccount;

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !UserAccount.getUniqueCreateDate().equals(testObj)  )
    {
         UserAccount.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !UserAccount.getUniqueModifyDate().equals(testObj)  )
    {
         UserAccount.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !UserAccount.getUniqueModifyComments().equals(testObj)  )
    {
         UserAccount.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UniqueModifyComments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactFirstName")) ;
    if ( !UserAccount.getContactFirstName().equals(testObj)  )
    {
         UserAccount.setContactFirstName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactLastName")) ;
    if ( !UserAccount.getContactLastName().equals(testObj)  )
    {
         UserAccount.setContactLastName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !UserAccount.getContactEmail().equals(testObj)  )
    {
         UserAccount.setContactEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactAddress1")) ;
    if ( !UserAccount.getContactAddress1().equals(testObj)  )
    {
         UserAccount.setContactAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactAddress2")) ;
    if ( !UserAccount.getContactAddress2().equals(testObj)  )
    {
         UserAccount.setContactAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactCity")) ;
    if ( !UserAccount.getContactCity().equals(testObj)  )
    {
         UserAccount.setContactCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ContactStateID")) ;
    if ( !UserAccount.getContactStateID().equals(testObj)  )
    {
         UserAccount.setContactStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactProvince")) ;
    if ( !UserAccount.getContactProvince().equals(testObj)  )
    {
         UserAccount.setContactProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactProvince not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactZIP")) ;
    if ( !UserAccount.getContactZIP().equals(testObj)  )
    {
         UserAccount.setContactZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ContactCountryID")) ;
    if ( !UserAccount.getContactCountryID().equals(testObj)  )
    {
         UserAccount.setContactCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactCountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactPhone")) ;
    if ( !UserAccount.getContactPhone().equals(testObj)  )
    {
         UserAccount.setContactPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactFax")) ;
    if ( !UserAccount.getContactFax().equals(testObj)  )
    {
         UserAccount.setContactFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactMobile")) ;
    if ( !UserAccount.getContactMobile().equals(testObj)  )
    {
         UserAccount.setContactMobile( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactMobile not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SecretQuestion")) ;
    if ( !UserAccount.getSecretQuestion().equals(testObj)  )
    {
         UserAccount.setSecretQuestion( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount SecretQuestion not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SecretAnswer")) ;
    if ( !UserAccount.getSecretAnswer().equals(testObj)  )
    {
         UserAccount.setSecretAnswer( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount SecretAnswer not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !UserAccount.getComments().equals(testObj)  )
    {
         UserAccount.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("BillingID")) ;
    if ( !UserAccount.getBillingID().equals(testObj)  )
    {
         UserAccount.setBillingID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount BillingID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PHDBAcknowledgementStatus")) ;
    if ( !UserAccount.getPHDBAcknowledgementStatus().equals(testObj)  )
    {
         UserAccount.setPHDBAcknowledgementStatus( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount PHDBAcknowledgementStatus not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PHDBLetterHeadStatus")) ;
    if ( !UserAccount.getPHDBLetterHeadStatus().equals(testObj)  )
    {
         UserAccount.setPHDBLetterHeadStatus( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount PHDBLetterHeadStatus not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("TaxID")) ;
    if ( !UserAccount.getTaxID().equals(testObj)  )
    {
         UserAccount.setTaxID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount TaxID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("NotUsingPHDBID")) ;
    if ( !UserAccount.getNotUsingPHDBID().equals(testObj)  )
    {
         UserAccount.setNotUsingPHDBID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount NotUsingPHDBID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NotUsingPHDBComments")) ;
    if ( !UserAccount.getNotUsingPHDBComments().equals(testObj)  )
    {
         UserAccount.setNotUsingPHDBComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount NotUsingPHDBComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PayerID")) ;
    if ( !UserAccount.getPayerID().equals(testObj)  )
    {
         UserAccount.setPayerID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount PayerID not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("AlertDays")) ;
    if ( !UserAccount.getAlertDays().equals(testObj)  )
    {
         UserAccount.setAlertDays( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount PayerID not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("Comm_Email_RequiresAttachement")) ;
    if ( !UserAccount.getComm_Email_RequiresAttachement().equals(testObj)  )
    {
         UserAccount.setComm_Email_RequiresAttachement( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Email_RequiresAttachement not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts_LevelUser")) ;
    if ( !UserAccount.getComm_Alerts_LevelUser().equals(testObj)  )
    {
         UserAccount.setComm_Alerts_LevelUser( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelUser not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts_LevelManager")) ;
    if ( !UserAccount.getComm_Alerts_LevelManager().equals(testObj)  )
    {
         UserAccount.setComm_Alerts_LevelManager( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelManager not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts_LevelBranch")) ;
    if ( !UserAccount.getComm_Alerts_LevelBranch().equals(testObj)  )
    {
         UserAccount.setComm_Alerts_LevelBranch( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelBranch not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts2_LevelUser")) ;
    if ( !UserAccount.getComm_Alerts2_LevelUser().equals(testObj)  )
    {
         UserAccount.setComm_Alerts2_LevelUser( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelUser not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts2_LevelManager")) ;
    if ( !UserAccount.getComm_Alerts2_LevelManager().equals(testObj)  )
    {
         UserAccount.setComm_Alerts2_LevelManager( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelManager not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts2_LevelBranch")) ;
    if ( !UserAccount.getComm_Alerts2_LevelBranch().equals(testObj)  )
    {
         UserAccount.setComm_Alerts2_LevelBranch( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelBranch not set. this is ok-not an error");
}





try
{
    Integer testObj = new Integer(request.getParameter("Comm_Report_LevelUser")) ;
    if ( !UserAccount.getComm_Report_LevelUser().equals(testObj)  )
    {
         UserAccount.setComm_Report_LevelUser( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Report_LevelUser not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Report_LevelManager")) ;
    if ( !UserAccount.getComm_Report_LevelManager().equals(testObj)  )
    {
         UserAccount.setComm_Report_LevelManager( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Report_LevelManager not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Report_LevelBranch")) ;
    if ( !UserAccount.getComm_Report_LevelBranch().equals(testObj)  )
    {
         UserAccount.setComm_Report_LevelBranch( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Report_LevelBranch not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UserNPI")) ;
    if ( !UserAccount.getUserNPI().equals(testObj)  )
    {
         UserAccount.setUserNPI( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UserNPI not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UserStateLicense")) ;
    if ( !UserAccount.getUserStateLicense().equals(testObj)  )
    {
         UserAccount.setUserStateLicense( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UserStateLicense not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UserStateLicenseDesc")) ;
    if ( !UserAccount.getUserStateLicenseDesc().equals(testObj)  )
    {
         UserAccount.setUserStateLicenseDesc( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UserStateLicenseDesc not set. this is ok-not an error");
}


try
{
	com.winstaff.password.Encrypt myEnc = new com.winstaff.password.Encrypt();
    String testObjCurrent = myEnc.getMD5Base64(   new String(request.getParameter("CurrentPassword"))   ) ;
    String testObjNew1 = new String(request.getParameter("NewPassword1")) ;
    String testObjNew2 = new String(request.getParameter("NewPassword2")) ;
	
	//test current
    if ( UserAccount.getLogonUserPassword().equals(testObjCurrent) && myEnc.checkPasswords(testObjNew1,testObjNew2)  )
    {
         UserAccount.setLogonUserPassword( myEnc.getMD5Base64(testObjNew1),UserSecurityGroupID   );
         testChangeID = "1";
		 %>
         <script language="javascript">
		 alert("Password Change Successful!");
		 </script>
		 <%
		 
    }
	else 
	{
		 %>
         <script language="javascript">
		 alert("Your password was NOT changed\n\nPlease make sure that your passwords match.\n\nPlease make sure that your new password is at least a length of 8 or more.");
		 </script>
		 <%
	}

}
catch(Exception e)
{
     //out.println("UserAccount ContactAddress1 not set. this is ok-not an error");
}


boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	UserAccount.setUniqueModifyDate(PLCUtils.getNowDate(false));
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    UserAccount.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        UserAccount.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
	      alert("User Settings have been updated.");
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
