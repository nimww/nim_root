<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.* " %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<script type="text/javascript" src="../js/jQuery_1.6.4.js"></script>          
 <script type="text/javascript">                                         
$(document).ready(function(){

   //Adjust height of overlay to fill screen when page loads
   $("#fuzz").css("height", $(document).height());

   //When the link that triggers the message is clicked fade in overlay/msgbox
   $(".alert").click(function(){
      $("#fuzz").fadeIn();
      return false;
   });

   //When the message box is closed, fade out
   $(".close").click(function(){
      $("#fuzz").fadeOut();
      return false;
   });

});

//Adjust height of overlay to fill screen when browser gets resized
$(window).bind("resize", function(){
   $("#fuzz").css("height", $(window).height());
});
</script> 

<style>
/*Styles for fuzz overlay & message box*/
 #fuzz{ position:absolute; top:0; left:0; width:100%; z-index:100; background: url('../js/images/dim.png'); display:none; text-align:left; }

   /*Message box, positioned in dead center of browser*/
   .msgbox{ position:absolute; width:300px; height:200px; z-index:200; border:5px solid #222; background: #FFF; top: 50%; left: 50%; margin-top: -100px; margin-left: -150px; }
      .msgbox img {border:none; margin:5px;}

      /*The "X" in the upper right corner of msgbox*/
      .close{ top:0px; float:right; }
</style>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_icid.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
    <table cellpadding=0 cellspacing=0 border=0  >
    <tr><td width=10>&nbsp;</td><td>
  <%
	//initial declaration of list class and parentID
    boolean accessValid = false;
    Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
    String ENCID = null;
	NIM3_EncounterObject myEO = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isIC&&request.getParameter("EDITID")!=null) 
    {
		ENCID = request.getParameter("EDITID");
		if (NIMUtils.isICIDAuthorizedToUploadReport(CurrentUserAccount.getReferenceID(),new Integer(ENCID),"D"))
		{
			myEO = new NIM3_EncounterObject(new Integer(ENCID));
	        accessValid = true;
		}
    }
  //page security
  if (accessValid)
  {
%>
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td class="tdHeader">DICOM - Upload  for Patient: <%=myEO.NIM3_CaseAccount.getPatientLastName()%>, <%=myEO.NIM3_CaseAccount.getPatientFirstName()%><br />
<br />
* Please make sure to ZIP all .DCM files into a single .ZIP file first. </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><form action="IC_UploadDICOM_sub.jsp" method="post" enctype="multipart/form-data">
        <p>
          <input type="file" name="fileData"/>
          <input type="hidden" name="EDIT" value="<%=ENCID%>"/>
        </p>
        <p>
<input type="button" class="alert"  onClick="this.disabled=true;submit();document.getElementById('progress').parentNode.style.display = 'block';" value="Upload" />          
</p>
      </form>      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>All information is secured through SSL Encryption.</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    </table>
    
<!--The overlay and message box-->
 <div id="fuzz">
    <div class="msgbox">
       <table cellpadding="10" width=100% cellspacing="0" border="0"><tr><td>    <span class="title">Uploading...</span><br />Please wait.  This may take several minutes.<br /><br />For example: A 50MB file upload can take 5-10 minutes depending on your line upload speed.</td></tr><tr><td align="center"	><div style="display:none"><img id="progress" src="../js/load5.gif" alt=""/>
</div></td></tr></table>
    </div>
 </div>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>