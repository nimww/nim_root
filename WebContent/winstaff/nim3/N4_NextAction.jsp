<%@page import="java.io.*,com.winstaff.*, java.util.*, java.text.SimpleDateFormat,com.google.gson.Gson;"%>
<%
	class incomingNa{
		String nextactionnotes = "";
		String nextactiondate = "";
		String sessionuserid;
		String en_id = "";
		String ca_id = "";
		String rf_id = "";
		public incomingNa(String nextactionnotes, String nextactiondate, String sessionuserid, String en_id, String ca_id, String rf_id) {
			this.nextactionnotes = nextactionnotes;
			this.nextactiondate = nextactiondate;
			this.sessionuserid = sessionuserid;
			this.ca_id = ca_id;
			this.rf_id = rf_id;
			this.en_id = en_id;
		}
		
		public String getNextactionnotes() {
			return nextactionnotes;
		}
		public void setNextactionnotes(String nextactionnotes) {
			this.nextactionnotes = nextactionnotes;
		}
		
		public String getNextactiondate() {
			return nextactiondate;
		}
		public void setNextactiondate(String nextactiondate) {
			this.nextactiondate = nextactiondate;
		}
		
		public String getCa_id() {
			return ca_id;
		}
		public void setCa_id(String ca_id) {
			this.ca_id = ca_id;
		}
		
		public String getRf_id() {
			return rf_id;
		}
		public void setRf_id(String rf_id) {
			this.rf_id = rf_id;
		}
		
		public String getEn_id() {
			return en_id;
		}
		public void setEn_id(String en_id) {
			this.en_id = en_id;
		}
}
%>
<%
	try{
		String nextactionnotes = "";
		String nextactiondate = "";
		String sessionuserid = "";
		String en_id = "";
		String ca_id = "";
		String rf_id = "";
	
		out.println(en_id + " " + sessionuserid + " " + nextactiondate);
		nextactionnotes = request.getParameter("nextactionnotes");
		nextactiondate = request.getParameter("nextactiondate");
		sessionuserid = request.getParameter("sessionuserid");
		en_id = request.getParameter("en_id");
		ca_id = request.getParameter("ca_id");
		rf_id = request.getParameter("rf_id");
		bltNIM3_Encounter en = new bltNIM3_Encounter(new Integer(en_id));
		bltNIM3_CommTrack ct = new bltNIM3_CommTrack();
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		//2015-03-02 10:00:47
		en.setNextActionNotes(nextactionnotes);
		en.setNextActionDate(date.parse(nextactiondate));
		en.setUniqueModifyComments("Next action notes modified by " + sessionuserid + " and set the following notes: " + nextactionnotes + " on" + nextactiondate);
		en.setUniqueModifyDate(new Date());
		en.commitData();
		out.println(en_id + " " + sessionuserid + " " + nextactiondate);
			ct.setComments("Next Action Modified by " + sessionuserid + " on " + (new Date()));
			ct.setIntUserID(new Integer(sessionuserid));
			ct.setCaseID(new Integer(ca_id));
			ct.setReferralID(new Integer(rf_id));
			ct.setMessageName(sessionuserid.toString());
			ct.setEncounterID(new Integer(en_id));
			ct.setUniqueCreateDate(new Date());
			ct.setMessageSubject("[ N4] ");
			ct.setMessageText(" N4 " + new Date());
			ct.setMessageCompany("N4 Generated");
			ct.setMessageSubject(nextactionnotes);
			ct.setCommStart(new Date());
			ct.setCommEnd(new Date());
			ct.commitData();
			
			out.println("Please Wait redirecting you now...");
			%>
			<script>
			 	document.location.href = "http://localhost:8080/n4/ca/<%=ca_id%>/<%=en_id%>";
			</script>	 			
			<% 
	}catch(Exception e){
		e.printStackTrace();
	}
%>