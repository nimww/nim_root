<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.*, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltNIM3_ServiceBillingTransaction, com.winstaff.DocumentManagerUtils" %>
<%/*
    filename: out\jsp\tNIM3_ServiceBillingTransaction_form_sub.jsp
    Created on Mar/09/2011
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {


    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
//        accessValid = true;
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
			if (NIMUtils.isAuthorizedCreateServiceBillingTransaction(iEncounterID,iEDITID))
			{
				accessValid = true;
			}
        }
    }

  //page security
  if (accessValid&&isScheduler2)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltNIM3_ServiceBillingTransaction        NIM3_ServiceBillingTransaction        =    null;

String testChangeID = "0";
	if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_ServiceBillingTransaction        =    new    bltNIM3_ServiceBillingTransaction(UserSecurityGroupID,true);
    }
	NIM3_ServiceBillingTransaction.setServiceID( iEDITID,UserSecurityGroupID   );
	testChangeID = "1";



try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !NIM3_ServiceBillingTransaction.getUniqueCreateDate().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !NIM3_ServiceBillingTransaction.getUniqueModifyDate().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !NIM3_ServiceBillingTransaction.getUniqueModifyComments().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ServiceBillingTransactionTypeID")) ;
    if ( !NIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setServiceBillingTransactionTypeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction ServiceBillingTransactionTypeID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ExportedDate"))) ;
    if ( !NIM3_ServiceBillingTransaction.getExportedDate().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setExportedDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction ExportedDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("TransactionTitle")) ;
    if ( !NIM3_ServiceBillingTransaction.getTransactionTitle().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setTransactionTitle( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction TransactionTitle not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("TransactionNote")) ;
    if ( !NIM3_ServiceBillingTransaction.getTransactionNote().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setTransactionNote( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction TransactionNote not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("TransactionAmount"))) ;
    if ( !NIM3_ServiceBillingTransaction.getTransactionAmount().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setTransactionAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction TransactionAmount not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReferenceNumber")) ;
    if ( !NIM3_ServiceBillingTransaction.getReferenceNumber().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setReferenceNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction ReferenceNumber not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ReferenceDate"))) ;
    if ( !NIM3_ServiceBillingTransaction.getReferenceDate().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setReferenceDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction ReferenceDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReferenceNotes")) ;
    if ( !NIM3_ServiceBillingTransaction.getReferenceNotes().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setReferenceNotes( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction ReferenceNotes not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Detail_Item_Qty")) ;
    if ( !NIM3_ServiceBillingTransaction.getDetail_Item_Qty().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setDetail_Item_Qty( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction Detail_Item_Qty not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Generated_UserID")) ;
    if ( !NIM3_ServiceBillingTransaction.getGenerated_UserID().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setGenerated_UserID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction Generated_UserID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !NIM3_ServiceBillingTransaction.getComments().equals(testObj)  )
    {
         NIM3_ServiceBillingTransaction.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_ServiceBillingTransaction Comments not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tNIM3_ServiceBillingTransaction","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	NIM3_ServiceBillingTransaction.setUniqueModifyDate(new java.util.Date());
	NIM3_ServiceBillingTransaction.setUniqueCreateDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    NIM3_ServiceBillingTransaction.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        NIM3_ServiceBillingTransaction.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
//	      document.location="<%=nextPage%>";
window.close();
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
