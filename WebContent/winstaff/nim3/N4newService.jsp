<%@page import="java.io.*,com.winstaff.*, java.util.*, java.text.SimpleDateFormat,com.google.gson.Gson;"%>
<%!class incomingService {
	public String cpt = "";
	public String bp = "";
	public String qty = "";
	public String cptModifier = "";
	public String ro = "";
	public String diag1 = "";
	public String diag2 = "";
	public String diag3 = "";
	public String diag4 = "";
	public incomingService(String cpt, String bp, String qty,String cptModifier,String ro , String diag1, String diag2, String diag3, String diag4) {
		this.cpt = cpt;
		this.bp = bp;
		this.qty = qty;
		this.cptModifier = cptModifier;
		this.ro = ro;
		this.diag1 = diag1;
		this.diag2 = diag2;
		this.diag3 = diag3;
		this.diag4 = diag4;
	}
	public String getCpt() {
		return cpt;
	}
	public void setCpt(String cpt) {
		this.cpt = cpt;
	}
	public String getBp() {
		return bp;
	}
	public void setBp(String bp) {
		this.bp = bp;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getCptModifier() {
		return cptModifier;
	}
	public void setCptModifiert(String cptModifier) {
		this.cptModifier = cptModifier;
	}
	public String getRo() {
		return ro;
	}
	public void setRo(String ro) {
		this.ro = ro;
	}
	public void setDiag1(String diag1) {
		this.diag1 = diag1;
	}
	public String getDiag1() {
		return diag1;
	}
	public String getDiag2() {
		return diag2;
	}
	public void setDiag2(String diag2) {
		this.diag2 = diag2;
	}
	public String getDiag3() {
		return diag3;
	}
	public void setDiag3(String diag3) {
		this.diag3 = diag3;
	}
	public String getDiag4() {
		return diag4;
	}
	public void setDiag4(String diag4) {
		this.diag4 = diag4;
	}
}
	%>
	<%
	String encounterid = "";
	String ca_id = "";
	String rf_id = "";
	String usessionid = "";
	List<incomingService> incomingServiceList = new ArrayList<incomingService>();
		 
		try{
			 encounterid = request.getParameter("encounterid");
			 ca_id = request.getParameter("ca_id");
			 rf_id = request.getParameter("rf_id");
			 usessionid = request.getParameter("usessionid");
			 for(int x = 1; x < 6; x++){
				 	String cpt = request.getParameter("services[" + x + "].cpt");
					String bp = request.getParameter("services[" + x + "].bp");
					String qty = request.getParameter("services[" + x + "].qty");
					String cptModifier = request.getParameter("services[" + x + "].cptModifier");
					String ro = request.getParameter("services[" + x + "].ro");
							String diag1 = request.getParameter("services[" + x + "].diag1");
							String diag2 = request.getParameter("services[" + x + "].diag2");
							String diag3 = request.getParameter("services[" + x + "].diag3");
							String diag4 = request.getParameter("services[" + x + "].diag4");
					if(cpt != null && !cpt.trim().isEmpty() ){
						 incomingServiceList.add(new incomingService(cpt, bp, qty, cptModifier, ro, diag1, diag2, diag3, diag4));
					 }
			 }
		}catch(Exception e){
			e.printStackTrace();
		}
		 
		bltNIM3_CommTrack ct = new bltNIM3_CommTrack();
	    java.util.Date now = new java.util.Date();
		
		SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy");	
		out.println("Number of services " + incomingServiceList.size() + " ");		
		 for(incomingService incoming : incomingServiceList){
	 			bltNIM3_Service sr = new bltNIM3_Service();
	 			if(!incoming.getCpt().trim().isEmpty() && incoming.getCpt().trim() != null){
	 				sr.setCPT(incoming.getCpt());
	 			}
					
				if(!incoming.getQty().trim().isEmpty() && incoming.getQty().trim() != null){
					sr.setCPTQty(new Integer(incoming.getQty()));
	 			}
				
				if(!incoming.getBp().trim().isEmpty() && incoming.getBp().trim() != null){
					sr.setCPTBodyPart(incoming.getBp());
	 			}
				
				if(!incoming.getCptModifier().trim().isEmpty() && incoming.getCptModifier().trim() != null){
					sr.setCPTModifier(incoming.getCptModifier());
				}
				
				if(!incoming.getRo().trim().isEmpty() && incoming.getRo().trim() != null){
					sr.setCPTText(incoming.getRo());
				}
				
				if(!incoming.getDiag1().trim().isEmpty() && incoming.getDiag1() != null){
					sr.setdCPT1(incoming.getDiag1());
				}
				if(!incoming.getDiag2().trim().isEmpty() && incoming.getDiag2() != null){
					sr.setdCPT2(incoming.getDiag2());						
				}
				if(!incoming.getDiag3().trim().isEmpty() && incoming.getDiag3() != null){
					sr.setdCPT3(incoming.getDiag3());
				}
				if(!incoming.getDiag4().trim().isEmpty() && incoming.getDiag4() != null){
					sr.setdCPT4(incoming.getDiag4());
				}
				
				sr.setServiceTypeID(3);
				sr.setServiceStatusID(1);
				sr.setEncounterID(new Integer(encounterid));
				sr.setUniqueModifyComments("Created by N4");
				sr.setUniqueCreateDate(PLCUtils.getNowDate(false));
				sr.setUniqueModifyDate(PLCUtils.getNowDate(false));
				
				sr.commitData();
	 		} 
				
		 	ct.setUniqueCreateDate(now);
			ct.setReferralID(new Integer(rf_id));
			ct.setEncounterID(new Integer(encounterid));
			ct.setIntUserID(new Integer(usessionid));
			ct.setMessageName("N4 Generated");
			ct.setMessageSubject("[Generated via N4]");
			ct.setMessageText("Generated via N4" + now);
			ct.setMessageCompany("N4 ..");
			ct.setMessageSubject(" SM..");
			ct.setCommStart(now);
			ct.setCommEnd(now);
			ct.commitData();
			out.println("Please Wait redirecting you now...");
			%>
		 	<script>
				document.location.href = "http://localhost:8080/n4/ca/<%=ca_id%>/<%=encounterid%>";
			</script>
		 	<%
			
	 		%>
