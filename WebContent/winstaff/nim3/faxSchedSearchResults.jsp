<%@page language="java" contentType="application/json; charset=UTF-8" import="java.io.*,com.winstaff.*, java.util.*, java.sql.*,com.google.gson.Gson;"%>
<%!class uaResult {
		String value;
		int id;
		String desc;
		
		public uaResult(String value, int id, String desc) {
			this.value = value;
			this.id = id;
			this.desc = desc;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
		
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
	}%>
<%
	List<uaResult> uaResultList = new ArrayList<uaResult>();

	if (request.getParameter("term") != null && !request.getParameter("term").isEmpty()) {
		String search = request.getParameter("term");

		String query = "SELECT\n" +
				"	userid,\n" +
				"	ua.contactfirstname || ' ' || ua.contactlastname,  ua.accounttype \n" +
				"from tuseraccount ua\n" +
				"join tnim3_payermaster pm on pm.payerid = ua.payerid\n" +
				"where LOWER (\n" +
				"		ua.contactfirstname || ' ' || ua.contactlastname\n" +
				"	) ~ lower('" + search + "')\n" +
				" and (lower(ua.accounttype) ~ 'scheduler' or ua.accounttype = 'GenAdminID')" +
				"ORDER BY\n" +
				"	userid desc\n" +
				"limit 15";
		searchDB2 db = new searchDB2();

		ResultSet rs = db.executeStatement(query);

		try {
			while (rs.next()) {
				uaResultList.add(new uaResult(rs.getString(2), rs.getInt(1),rs.getString(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closeAll();
		}
	}
	response.setHeader("Access-Control-Allow-Origin", "*");
	out.println(new Gson().toJson(uaResultList));
%>
