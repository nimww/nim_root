<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="java.text.DecimalFormat ,com.winstaff.*" %>
<%/*

    filename: out\jsp\NIM3_Encounter_form.jsp
    Created on May/28/2008
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>

<%@ include file="../generic/generalDisplay.jsp" %>





<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","NIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp");
    pageControllerHash.put("sParentReturnPage","NIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp");

//initial declaration of list class and parentID

    bltNIM3_Encounter NIM3_Encounter        =    null;

    if ( request.getParameter( "EDIT" )==null||request.getParameter( "EDIT" ).equalsIgnoreCase("edit")||request.getParameter( "EDIT" ).equalsIgnoreCase("sched") )
    {
        NIM3_Encounter        =    new    bltNIM3_Encounter(iEncounterID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_Encounter        =    new    bltNIM3_Encounter(UserSecurityGroupID, true);
    }

bltNIM3_Referral NIMR = new bltNIM3_Referral(NIM3_Encounter.getReferralID());
bltNIM3_CaseAccount NCA = new bltNIM3_CaseAccount(NIMR.getCaseID());

String phoneTemp = NCA.getPatientCellPhone().replaceAll("\\p{Punct}|\\s","");
if (phoneTemp.equalsIgnoreCase(""))
{
	phoneTemp = NCA.getPatientHomePhone().replaceAll("\\p{Punct}|\\s","");
}
if (!phoneTemp.equalsIgnoreCase("") && phoneTemp.length()>6)
{
	try
	{
	//	phoneTemp = phoneTemp.substring(0,3) + "-" + phoneTemp.substring(3,3) + "-" + phoneTemp.substring(5,4);
		phoneTemp = phoneTemp.substring(0,3) + "-" + phoneTemp.substring(3,6) + "-" + phoneTemp.substring(6,10);
	}
	catch (Exception eeeeee) 
	{
		out.println("phone: [" + eeeeee + "]");
	}
}

String ssZIP = NCA.getPatientZIP();
if (request.getParameter("ssZIP")!=null)
{
	ssZIP = request.getParameter("ssZIP");
}


//fields
        %>
          <%  String theClass ="tdBase";%>
    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr class=title><td width=10>&nbsp;</td><td class="title">Schedule Procedure</td></tr>
    <tr><td width=10>&nbsp;</td><td>
        <table width=100% border=0 bordercolor=#333333 cellpadding=5 cellspacing=0 >
         <tr>
           <td class="title" ><input type="button" onClick="this.disabled=true;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp';" value="Cancel Schedule" />
</td>
         </tr>
         <tr>
           <td class=title >Available appointments near Case Address:<%=NCA.getPatientAddress1()%><br />
<%=NCA.getPatientCity()%>, <%=new bltStateLI(NCA.getPatientStateID()).getShortState()%> <%=NCA.getPatientZIP()%></td>
         </tr>
         <tr>
           <td ><form id="form1" name="form1" method="post" action="tImaging_schedule.jsp">
             To search a different ZIP code, please enter it below and click search:<br />
<br />
             <input name="ssZIP" type="text"  class="inputHi" id="ssZIP" value="<%=ssZIP%>" size="10" maxlength="5"/>
             <input type="submit" name="Search" id="Search" value="Search" />
             <input type="hidden" name="EDIT" value="sched" />
           </form>
           </td>
         </tr>
         <tr>
           <td ><table width="100%" border="0" cellspacing="0" cellpadding="10">
<%
{
	ZIP_CODE_object myZCo = NIMUtils.get_zips_in_range(ssZIP,10);
	java.util.Vector myV = myZCo.getZipsInRange(10);
    int i2cnt=0;
	for (int i10=0;i10<myV.size();i10++)
	{
		String myZip = (String) myV.elementAt(i10);
//start big loop
	searchDB2_Remote mySS = new searchDB2_Remote();
	java.sql.ResultSet myRS = null;
	try
	{
		

		String mySQL = ("SELECT icid from `tICMaster` where centerzip like '" + myZip + "%' and istatus in (1,2,5)");
//		out.println(mySQL);
		myRS = mySS.executeStatement(mySQL);
		while(myRS!=null&&myRS.next())
		{
			i2cnt++;
			bltPracticeMaster myPracM = new bltPracticeMaster(myNALU.getProviderID());

//			bltICMaster myICM = new bltICMaster (new  Integer(myRS.getString("icid")));
			String theClass_ic = "tdBaseAlt2";
			if (i2cnt%2==0)
			{
				theClass_ic = "tdBaseAlt";
			}
	 	    java.text.DecimalFormat myDF = new java.text.DecimalFormat("0.0");
			String mySearchZip = myICM.getcenterzip();
			if (mySearchZip.length()>5)
			{
				mySearchZip = mySearchZip.substring(0,5);
			}
			String myDistance = "" + myDF.format(myZCo.tmZip.get( mySearchZip )) + " miles";
			if (myDistance.equalsIgnoreCase("0.0 miles"))
			{
				myDistance = "Same ZIP";
			}

String phoneTemp2 = myPracM.getcenterphone().replaceAll("\\p{Punct}|\\s","");
try
{
	phoneTemp2 = phoneTemp2.substring(0,3) + "-" + phoneTemp2.substring(3,6) + "-" + phoneTemp2.substring(6,10);
}
catch (Exception eeeeee) 
{
	out.println("Phone: " + eeeeee);
}


%>




			  <tr class=<%=theClass_ic%>>
				<td><strong><span class=expiredFieldMain>
				  <%if (myPracM.getContractingStatusID()>=1){%>Contracted Center<%}%></span></strong><br /><strong><%=myICM.getPracticeName()%></strong><br />
               Phone:  <strong><%=phoneTemp2%></strong><br />
				<strong><%=myPracM.getPracticeName()%></strong><br />
				<%=myPracM.getOfficeAddress1()%><br />
			<%=myPracM.getOfficeCity()%>, <%=new bltStateLI(myPracM.getOfficeStateID()).getShortState()%> <%=myPracM.getOfficeZIP()%><br />
<a href="http://maps.google.com/maps?q=<%=myPracM.getOfficeAddress1()%>,<%=myPracM.getOfficeZIP()%>" target="_blank">View Map</a><br />
Phone: <%=myPracM.getOfficePhone()%><br />

<%=myPracM.getPracticeID()%></td>
				<td ><h2><%=myDistance%></h2></td>
				<td><h2><a href="http://maps.google.com/maps?q=<%=myICM.getcenteraddress1()%>,<%=myICM.getcenterzip()%>" target="_blank"><img src="images/icon_viewmap.gif" width="111" height="20" border="0" /></a></h2></td>
				<td><span class="tdHeader">Available Times:</span>
				  <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#000033">
                  <tr class=tdHeaderAlt>
                    <td>Day</td>
                    <td>Time</td>
                    <td>Action</td>
                  </tr>

                <%
				if (true)
				{
					searchDB2 mySS_time = new searchDB2();
					java.sql.ResultSet myRS_time = null;
					try
					{
						String mySQL_time = "SELECT * from tnim3_appointment where providerid = " + myICM.getICID() + " AND appointmenttime > current_timestamp and appointmenttime <current_timestamp + INTERVAL '10 days' and iStatus=0 order by appointmenttime";
						//mySQL_time = "SELECT * from tnim_appointmentlu where  appointmenttime > current_timestamp and appointmenttime <current_timestamp + INTERVAL '7 days' and authorizationid=0";
						//out.println(mySQL_time);
						myRS_time = mySS_time.executeStatement(mySQL_time);
						int icnt=0;
						while(myRS_time!=null&&myRS_time.next())
						{
							icnt++;
							//bltNIM2_AppointmentLU myNALU = new bltNIM2_AppointmentLU(new Integer(myRS_time.getString("lookupid")));
							java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
							java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
							java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
							java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
							java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);
							java.util.Date dbAppointment = dbdft.parse(myRS_time.getString("appointmenttime"));
							String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
							String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
							String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
							String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
							String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
							String sAppointmentHour = displayDateHour.format((dbAppointment));
							String theClass_time = "tdBase";
							if (icnt%2==0)
							{
								theClass_time = "tdBaseAlt";
							}
						%>
                          <tr class=<%=theClass_time%>>
                            <td><span class=tdHeader><%=sAppointmentTimeDayWeek%>&nbsp;<br />
                            <%=sAppointmentTimeMonth%>&nbsp;<%=sAppointmentTimeDay%>&nbsp;<%=sAppointmentTimeYear%></span></td>
                            <td nowrap="nowrap"><span class=tdHeader><%=sAppointmentHour%></span></td>
                            <td><a href="tImaging_schedule_sub.jsp?EDIT=sched&EDITID=<%=myRS_time.getString("appointmentid")%> "><img src="images/icon_schedule-me.gif" width="142" height="16" border="0" /></a></td>
                          </tr>
                <%
						}
						if (icnt==0)
						{
						%>
                        <tr><td colspan="3">No Appointments Available</td></tr>
                        <%
						}
						%>
                        <tr><td colspan="3"><a class=linkBase href = "tNIM3_Appointment_main_NIM3_Appointment_ProviderID_form_create_PayerID.jsp?EDITID=<%=myICM.getICID()%>&EDIT=new&KM=p&INTNext=yes">Add Appointment</a>						</td></tr>
                        <%


					}
					catch(Exception e)
					{
							out.println("ResultsSet:"+e);
					}
						
				}
				%>
                </table>                </td>
			  </tr>
         <%     
//			out.println("<hr>");
		}
	}
	catch(Exception e)
	{
			out.println("ResultsSet:"+e);
	}
  }

//	out.println(myWhere);
}
%>           
           
</table>
           
           
           
           
           
           
           
           </td>
         </tr>
        </table>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>