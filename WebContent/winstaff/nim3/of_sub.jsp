
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import=" com.winstaff.*"%>
<%@ page  import="java.util.*;" %> 
<%
	// filename:<* out\jsp\tNIM3_CaseAccount_form_sub.jsp*>
	// Created on 01/26/2014
	// Created by: Giovanni Hernandez
%>
<%
	//initial declaration of list class and parentID
	java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(
			PLCUtils.String_dbdf);
	java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(
			PLCUtils.String_displayDateSDF1);
	//initial declaration of list class and parentID
	bltNIM3_CaseAccount NIM3_CaseAccount = new bltNIM3_CaseAccount();
	bltNIM3_Referral NIM3_Referral = new bltNIM3_Referral();
	bltNIM3_Encounter NIM3_Encounter = new bltNIM3_Encounter();
	bltNIM3_CommTrack NIM3_CommTrack = new bltNIM3_CommTrack();

	Integer UserSecurityGroupID = new Integer(2);
	String testChangeID = "0";
	String testChangeIDMSG = "0";
	String sComments = "";
	String rComments = "";
	String cComments = "";
	//for CaseAccount

	NIM3_CaseAccount.setCaseClaimNumber(request.getParameter("CaseClaimNumber1"),UserSecurityGroupID);
	
	sComments += "Special notes!: " + request.getParameter("MessageText")+ "\n";
	sComments += "Referral Source: " + request.getParameter("referralSource")+ "\n";
	sComments += "Type of Referral: " + request.getParameter("typeofclaim")+ "\n";
	sComments += "MD Follow-Up Appointment "	+ request.getParameter("followup");
	
	rComments += "Ref. Dr Name: " + request.getParameter("ReferringPhysicianName")+ "\n";
	rComments += "Ref. Dr Phone: " + request.getParameter("ReferringPhysicianPhone")+ "\n";
	rComments += "MD Fax " + request.getParameter("ReferringPhysicianFax");
	
	String[] procedureList = request.getParameterValues("procedure");
	String[] bpList = request.getParameterValues("bp");
	
	if(procedureList != null){
		for(int i = 0; i < procedureList.length; i++){
			rComments += "\n Procedure" +(i+1)+":"+ procedureList[i].toString();
			}
		}
		if(bpList != null){
			for(int i = 0; i < bpList.length; i++){
				rComments += "\n Body Part"+(i+1)+":"+ bpList[i].toString();
				}
			}
	NIM3_CaseAccount.setPayerID(23,UserSecurityGroupID);
	NIM3_CaseAccount.setPatientFirstName(request.getParameter("PatientFirstName"),	UserSecurityGroupID);
	NIM3_CaseAccount.setPatientLastName(request.getParameter("PatientLastName"),UserSecurityGroupID);
	NIM3_CaseAccount.setDateOfInjury((PLCUtils.getSubDate(request.getParameter("DateOfInjury"))),UserSecurityGroupID);
	NIM3_CaseAccount.setEmployerPhone(request.getParameter("EmployerPhone"), UserSecurityGroupID);
	NIM3_CaseAccount.setEmployerName(request.getParameter("EmployerName"), UserSecurityGroupID);
	NIM3_CaseAccount.setPatientDOB((PLCUtils.getSubDate(request.getParameter("PatientDOB"))),UserSecurityGroupID);
	NIM3_CaseAccount.setPatientHomePhone(request.getParameter("PatientHomePhone"),UserSecurityGroupID);
	NIM3_CaseAccount.setPatientCellPhone(request.getParameter("PatientCellPhone"),UserSecurityGroupID);
	NIM3_CaseAccount.setPatientAddress1(request.getParameter("PatientAddress1"),UserSecurityGroupID);
	NIM3_CaseAccount.setPatientAddress2(request.getParameter("PatientAddress2"),UserSecurityGroupID);
	NIM3_CaseAccount.setPatientCity(request.getParameter("PatientCity"), UserSecurityGroupID);
	NIM3_CaseAccount.setPatientStateID(	new Integer(request.getParameter("PatientStateID")),UserSecurityGroupID);
	NIM3_CaseAccount.setPatientZIP(request.getParameter("PatientZip"),UserSecurityGroupID);
	
	NIM3_CaseAccount.setCaseCode("CP" +  NIM3_CaseAccount.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
	NIM3_CaseAccount.setComments(sComments);
	NIM3_CaseAccount.commitData();
	
	NIM3_Referral.setCaseID(NIM3_CaseAccount.getUniqueID(),UserSecurityGroupID);
	NIM3_Referral.setReferralMethod("Online (Pub)");
	NIM3_Referral.setReferralTypeID(new Integer(3));
	NIM3_Referral.setReferralDate(PLCUtils.getNowDate(true));
	NIM3_Referral.setReceiveDate(PLCUtils.getNowDate(true));
		
	NIM3_Referral.setReferralStatusID(new Integer(1));
	NIM3_Referral.setComments(rComments);
	NIM3_Referral.setAttendingPhysicianID(52);
	NIM3_Referral.setReferringPhysicianID(NIM3_Encounter.getAttendingPhysicianID());
	NIM3_Referral.commitData();
	
	
	NIM3_Encounter.setPatientAvailability("followup");
	NIM3_Encounter.setReferralID(NIM3_Referral.getUniqueID());
	//NIM3_Encounter.setRequiresHandCarryCD(new Integer(request.getParameter("requireshandcarrycd")),UserSecurityGroupID);
	//NIM3_Encounter.setRequiresFilms(new Integer(request.getParameter("requiresfilms")),UserSecurityGroupID);
	//NIM3_Encounter.setRequiresAgeInjury(new Integer(request.getParameter("requiresageinjury")),UserSecurityGroupID);
	//NIM3_Encounter.setisSTAT(new Integer(request.getParameter("isSTAT")),UserSecurityGroupID);
	NIM3_Encounter.commitData();
	NIM3_Encounter.setScanPass("SP" +  NIM3_Encounter.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
	NIM3_Encounter.setEncounterStatusID(new Integer(1));
	NIM3_Encounter.commitData();
	

	//for CommTrack
	
	cComments += 
		"APPROVAL STATUS: " + request.getParameter("isapproved") +"\n"+
		"CONTACT NAME: " + request.getParameter("sourcename") +"\n"+
		"CONTACT PHONE PROVIDED: " + request.getParameter("contactphone") +"\n"+
		"This Referral was submitted thru: Online (Pub)" + "\n" +
		"Special notes or Request: " + request.getParameter("MessageText") + "\n" +
		"Claim Number: "+ request.getParameter("caseClaimNumber1") + "\n" +
		"How did you hear about us: " + request.getParameter("howdidyouhearaboutus") + "\n" +
		"Referral Source " + request.getParameter("referralSource") + "\n" +
		"Type of Referral: " + request.getParameter("typeofclaim") + "\n" +
		"Authorization Number: " + request.getParameter("authNumber") + "\n" +
		"Group Number: " + request.getParameter("groupNumber") + "\n\n" +
		"Payer: " + request.getParameter("PayerName") + "\n" +
		"Payer Phone: " + request.getParameter("insurancecompanyphone") + "\n" +
		"Payer Address: " + request.getParameter("insuranceaddress") + " " + request.getParameter("insurancecity") + " " +  request.getParameter("insuranceStateID") + " " + request.getParameter("insurancecoZip") + "\n\n" +
		"Adjuster Name: " + request.getParameter("AdjusterName") + "\n" +
		"Adjuster Phone: " + request.getParameter("PayerPhone") + "\n" +
		"Adjuster Fax: " + request.getParameter("PayerFax") + "\n" +
		"Adjuster Email " + request.getParameter("PayerEmail") + "\n\n" +
		"Nurse/Admin Name: " + request.getParameter("MessageName") + "\n" +
		"Phone Number: " + request.getParameter("MessagePhone") + "\n" +
		"Fax Number: " + request.getParameter("MessageFax") + "\n" +
		"Email: " + request.getParameter("MessageEmail") + "\n" +
		"Link As " + request.getParameter("LinkType") + "\n\n" +
		"Patient name: " + request.getParameter("PatientFirstName") + "" + request.getParameter("PatientLastName") + "\n" +
		"Patient Home Phone " + request.getParameter("PatientHomePhone") + "\n" +
		"Patient Cell Phone: " + request.getParameter("PatientCellPhone")+ "\n" +
		"Patient Address: " + request.getParameter("PatientAddress1") + "" + request.getParameter("PatientAddress2") + "" + request.getParameter("PatientCity") + ""+ request.getParameter("PatientStateID") + "" + request.getParameter("PatientZip") + "\n" +
		"Date of Injury: " +request.getParameter("DateOfInjury") + "\n\n" +
		"MD Follow-Up Appointment: "	+ request.getParameter("followup");
	
	NIM3_CommTrack.setMessageText(cComments,UserSecurityGroupID);
	NIM3_CommTrack.setMessageName(request.getParameter("MessageName"),UserSecurityGroupID);
	NIM3_CommTrack.setMessageEmail(request.getParameter("MessageEmail"), UserSecurityGroupID);
	NIM3_CommTrack.setMessagePhone(request.getParameter("MessagePhone"), UserSecurityGroupID);
	NIM3_CommTrack.setMessageFax(request.getParameter("MessageFax"),UserSecurityGroupID);
	NIM3_CommTrack.setAlertStatusCode(1,UserSecurityGroupID);
	NIM3_CommTrack.setCommStart(PLCUtils.getNowDate(true));
	NIM3_CommTrack.setReferralID(NIM3_Referral.getUniqueID());
	NIM3_CommTrack.setEncounterID(NIM3_Encounter.getUniqueID());
	NIM3_CommTrack.setCommTypeID(new Integer(1));//1=message 2=sched     0 = bogus
	
	
	NIM3_CommTrack.commitData();
	
	
	
%>
<link href="ui_200/style_PayerID.css" rel="stylesheet" type="text/css">

<link href="ui_200/style_PayerID.css" rel="stylesheet" type="text/css">
<script language="javascript">
//alert ("Thank you for your Request.\n\nClick OK to return to the Home Page");
//parent.window.location='https://staging.nextimagedirect.com/winstaff/nim3/of2Redirect.jsp';
parent.window.location='http://www.nextimagemedical.com';
</script>
