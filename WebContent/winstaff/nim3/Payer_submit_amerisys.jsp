<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<!DOCTYPE html>
<%@ include file="../generic/CheckLogin.jsp" %>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head>
<title>NextImage Medical | Amerisys Online Imaging Referral</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script src="js/amerisys.js" type="text/javascript"></script>
<style>
td{
	vertical-align: top;
}
#pleasewaitScreen{
	display:none;
}
</style>
<script>
function manualForm(){
	document.location.href = "Payer_submit1.jsp";
}

</script>
</head>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler) 
    {
        accessValid = true;
    }
    else if (isAdjuster) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
	}
    else if (isProvider) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
	}
	
	
  //page security
  bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
  if (accessValid && (CurrentUserAccount.getPayerID()==307||myPM.getParentPayerID()==307))
  {
	  
	  %>

	  <% 
	  
      //java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      //java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      //pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	
	if (isAdjuster||isAdjuster2||isAdjuster3||isProvider)
	  {
		  %>
<table width="<%=MasterTableWidth%>" border="0" cellspacing="0" cellpadding="10">
    <tr>
        <td>
            <table border="0"  cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2" align="left" bordercolor="#002F63">           
                    <%
                    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
                    String ip = request.getRemoteAddr();
                    String host = request.getRemoteHost();
                    
                    
                    //fields
                    %>
                    <p class=big2>Amerisys Online Imaging Referral</p>
                    <p><strong>(Integrated with Amerisys Claimant information)</strong></p>
                    <p><strong>If claimant can not be found, <a href="Payer_submit1.jsp">click here to add</a></strong></p>
                    <p>You are signed in, we have your information as follows:</p>
                    <p class=tdBase>Name: <strong><%=CurrentUserAccount.getContactFirstName()%>&nbsp;<%=CurrentUserAccount.getContactLastName()%> </strong> @  <strong><%=new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID()).getPayerName()%></strong><br>
                    Phone: <strong><%=CurrentUserAccount.getContactPhone()%></strong><br>Fax: <strong><%=CurrentUserAccount.getContactFax()%></strong><br>Email: <strong><%=CurrentUserAccount.getContactEmail()%></strong></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<form name="at_intake" action="Payer_submit_amerisys_sub.jsp" onsubmit="return validateForm()">
<div id="amer-form-container">

	<div id="amer-adj-link">
    	Please link me as the <label><select name="LinkType" id="LinkType">
              <%if (isAdjuster){%><option value="Adjuster" selected>Adjuster</option><%}%>
              <%if (isAdjuster){%> <option value="NCM">Nurse Case Manager</option>><%}%>
              <%if (isProvider){%> <option value="Dr">Referring Physician</option>><%}%>
              <option value="Admin">Case/Referral Admin</option>
		</select></label>
    </div>
    
	<%if (isAdjuster3||isProvider3)
{String myPayerUsersSelect = NIMUtils.getListOfValidUserByPayer_HTMLSelect(CurrentUserAccount.getPayerID(),0);%>
    <div style="display:none;">
    <p>As a Branch/Office Admin, you can link other users at your location to this case. <br><span class="instructionsBig1">*if users are not listed here, you may enter them at the end of the form.</span></p>
    <p>Please link <select name="Link2">      <option value="0" selected>No Additional User Links</option>
    <%=myPayerUsersSelect%></select> as the
    <label>
    <select name="LinkType2" id="LinkType2">
    <%if (isAdjuster){%><option value="Adjuster" selected>Adjuster</option><%}%>
    <%if (isAdjuster){%> <option value="NCM">Nurse Case Manager</option>><%}%>
    <%if (isProvider){%> <option value="Dr">Referring Physician</option>><%}%>
    <option value="Admin">Case/Referral Admin</option>
    </select>
    </label><br>
    Please link <select name="Link3">      <option value="0" selected>No Additional User Links</option>
    <%=myPayerUsersSelect%></select> as the
    <label>
    <select name="LinkType3" id="LinkType3">
    <%if (isAdjuster){%><option value="Adjuster" selected>Adjuster</option><%}%>
    <%if (isAdjuster){%> <option value="NCM">Nurse Case Manager</option>><%}%>
    <%if (isProvider){%> <option value="Dr">Referring Physician</option>><%}%>
    <option value="Admin">Case/Referral Admin</option>
    </select>
    </label>
    </p>
    </div>
<%}%>
      
    <div id="amer-claim-search">
    	<p><strong>Search by Claim Number or Last, First:</strong></p><input type="text" class="md-info-input-text" id="claimNumberSearch" onclick="document.at_intake.submit.disabled = true;" onBlur="document.at_intake.submit.disabled = false;"><input type="button" id="claimant-search" class="amer-input-button" value="Search" onClick="search_main()">
    </div>
    <div id="iw-info" class="amer-two-col">
    	<div class="left">
        	<p><strong>Client Claim#:</strong> <span id="clientClaimNum"></span></p>
            <span id="patientLastName" class="capitalize"></span>&nbsp;<span id="patientFirstName" class="capitalize"></span><br>
            <span id="patientAddress1" class="capitalize"></span>&nbsp;<span id="patientAddress2" class="capitalize"></span><br>
            <span id="patientCity" class="capitalize"></span>&nbsp;<span id="patientState" class="capitalize"></span>&nbsp;<span id="patientZip" class="capitalize"></span><br>
            <p>Phone: <span id="patientPhone"></span></p>
        </div>
        <div class="right">
        	<p>DOB: <span id="patientDOB"></span></p>
            <p>ADJ: <span id="adjusterFirstName" class="capitalize"></span> <span id="adjusterLastName" class="capitalize"></span></p>
            <p>Case Manager: <span id="casemanagerFirstName" class="capitalize"></span>&nbsp;<span id="casemanagerLastName" class="capitalize"></span></p>
            <p>Employer: <span id="employer" class="capitalize"></span></p>
            <p>DOI: <span id="patientDOI"></span></p>
        </div>
		<div style="display:none;">
        	<input type="text" id="clientClaimNum" name="clientClaimNum" readonly class="claimantInfo">
            <input type="text" id="patientFirstName" name="patientFirstName" readonly class="claimantInfo">
            <input type="text" id="patientLastName" name="patientLastName" readonly class="claimantInfo">
            <input type="text" id="patientAddress1" name="patientAddress1" readonly class="claimantInfo">
            <input type="text" id="patientAddress2" name="patientAddress2" readonly class="claimantInfo">
            <input type="text" id="patientCity" name="patientCity" readonly class="claimantInfo">
            <input type="text" id="patientState" name="patientState" readonly class="claimantInfo"> 
            <input type="text" id="patientZip" name="patientZip" readonly class="claimantInfo">
            <input type="text" id="patientPhone" name="patientPhone" readonly class="claimantInfo">
            <input type="text" id="patientDOB" name="patientDOB" readonly class="claimantInfo">
            <input type="text" id="adjusterFirstName" name="adjusterFirstName" readonly class="claimantInfo"> 
            <input type="text" id="adjusterLastName" name="adjusterLastName" readonly class="claimantInfo">
            <input type="text" id="casemanagerFirstName" name="casemanagerFirstName" readonly class="claimantInfo"> 
            <input type="text" id="casemanagerLastName" name="casemanagerLastName" readonly class="claimantInfo">
            <input type="text" id="employer" name="employer" readonly class="claimantInfo">
            <input type="text" id="patientDOI" name="patientDOI" readonly class="claimantInfo">
            <input name="patientGender" id="patientGender">
            
            <input name="refName" type=text size="20">
         	<input name="refEmail" type=text size="20">
            <input name="PayerPreAuthorizationConfirmation" type=text>
            <input name="PatientStateID" id="PatientStateID">
            <input name="PatientSSN" id="PatientSSN">

            <input name="PatientCellPhone" type="text" >
            <input name="EmployerPhone" type="text">
            <input name="employerAddress" type="text">
            <input name="employerCity"  type="text">
            <input name="employerStateID">
            <input name="employerZIP" type="text">
			<input name="AdjusterCompany" type="text">
			<input name="AdjusterCity"  type="text">
			<input name="AdjusterPhone" type="text">
			<input name="AdjusterFax" type="text">
			<input name="AdjusterEmail" type="text">            
        </div>
    </div>
    <div id="md-info" class="amer-two-col">
    	<div class="left">
        	<p><strong>Ordering/Treating Physician</Strong></p>
        	<input type="text" name="ReferringPhysicianName" class="md-info-input-text" id="mdName">
        </div>
        <div class="right">
        	<p>Phone: </p> <input type="text" name="ReferringPhysicianPhone" class="md-info-input-text"><br>
            <p>Fax: </p> <input type="text" name="ReferringPhysicianFax" class="md-info-input-text"><br>
            <p>Email: </p> <input type="text" name="ReferringPhysicianEmail" class="md-info-input-text">
        </div>
    </div>
    <div id="procedure-info" class="amer-two-col">
    	<div class="left">
        	<p><strong>Procedure Information & Body Part:</strong><br>
			(note if contrast is needed)</p>
        	
        </div>
        <div class="right">
        	<textarea name="CPTText" cols="60" rows="4" class="titleSub2" onKeyDown="textAreaStop(this,500)"></textarea>
        </div>
    </div>
    <div id="special-notes" class="amer-two-col">
    	<div class="left">
        	<p><strong>Special Notes/Instructions:</strong><br>
			(eg. Stat, Aged Report needed, Open MRI required, claimant hand carry CD/film, etc.)</p>
        	
        </div>
        <div class="right">
        	<textarea name="MessageText" cols="70" rows="10" class="titleSub2" onKeyDown="textAreaStop(this,1900)"></textarea>
        </div>
    </div>
    <div id="upload-doc" class="amer-two-col">
    	<div class="left">
        	<p><strong>Upload Documents:</strong><br>
			</p>
        </div>
        <div class="right">
        	Optionally, you can upload any relevant documents such as authorizations, scripts, orders, etc. after you click on the Submit button.
        </div>
    </div>
    <input type="submit" class="amer-input-button" value="Submit" name="submit" onClick="this.disable=true;"><input type="button" class="amer-input-button" value="Cancel" onclick="document.location='Payer_Home.jsp';" >
    
</div>
</form> 
<div id="ameri-query">
</div>
<div id="faded-bg"></div>
   
	
      

          
          <%
	  }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>