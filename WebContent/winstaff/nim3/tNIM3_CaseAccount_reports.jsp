<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   com.winstaff.bltNIM3_CaseAccount" %>
<%/*

    filename: out\jsp\tNIM3_CaseAccount_form.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_1/style.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tNIM3_CaseAccount")%>



<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

//    pageControllerHash.put("sLocalChildReturnPage","tNIM3_CaseAccount_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID

    bltNIM3_CaseAccount        NIM3_CaseAccount        =    null;
  
//fields
        %>

          <%  String theClass ="tdBase";%>
 Quick-links: <input class="inputButton_md_Default"  type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID.jsp';" value="View Referrals for this Case">&nbsp;&nbsp;&nbsp;&nbsp;<input class="inputButton_md_Default"  type=button onClick = "this.disabled=true;document.location ='tNIM3_CaseAccount_PayerID_query.jsp';" value="Search All Cases"><br />
<br />
          
          
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 >
         <tr>
           <td >
           <table border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td colspan="2" class="title">Reports</td>
    </tr>
  <tr>
    <td class="tdHeaderAlt">Document Reports</td>
    <td><a href="tNIM3_CaseAccount_reports_dox_capatability.jsp">Capability Reports</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
           </table>

	   
		   
        </td></tr></table>

        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>