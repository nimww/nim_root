<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   com.winstaff.bltNIM3_CaseAccount" %>
<%/*

    filename: out\jsp\tNIM3_CaseAccount_form.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_1/style.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<script type="text/javascript" src="reports/mochikit/MochiKit.js"></script>
<script type="text/javascript" src="reports/plotkit/Base.js"></script>
<script type="text/javascript" src="reports/plotkit/Layout.js"></script>
<script type="text/javascript" src="reports/plotkit/Canvas.js"></script>
<script type="text/javascript" src="reports/plotkit/excanvas.js"></script>
<script type="text/javascript" src="reports/plotkit/SweetCanvas.js"></script>



    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tNIM3_CaseAccount")%>



<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

//    pageControllerHash.put("sLocalChildReturnPage","tNIM3_CaseAccount_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID

    bltNIM3_CaseAccount        NIM3_CaseAccount        =    null;
  
//fields
        %>

          <%  String theClass ="tdBase";%>
          
          Quick-links: <input class="inputButton_md_Default"  type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID.jsp';" value="View Referrals for this Case">&nbsp;&nbsp;&nbsp;&nbsp;<input class="inputButton_md_Default"  type=button onClick = "this.disabled=true;document.location ='tNIM3_CaseAccount_PayerID_query.jsp';" value="Search All Cases"><br />
<br />

<table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 >
       <tr>
           <td >
           <table border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td colspan="2" class="title">Reports</td>
    </tr>
  <tr>
    <td class="tdHeaderAlt">Document Reports</td>
    <td>Capability Reports</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;
    
        <table width="100%" border="1" cellpadding="3" cellspacing="0">
        <tr class="tdHeaderAlt">
        <td>DOS</td>
        <td>Report</td>
        <td>Type</td>
        </tr>
        <%

			searchDB2 mySS = new searchDB2();
			
			java.sql.ResultSet myRS = null;;
			
			try
			{
				String mySQL = ("select EncounterID from tNIM3_Encounter INNER JOIN tNIM3_Referral on  tNIM3_Referral.referralid = tNIM3_Encounter.referralid where tNIM3_Referral.caseid = " + iCaseID + " order by dateofservice");
				//out.println(mySQL);
				myRS = mySS.executeStatement(mySQL);
			}
			catch(Exception e)
			{
				out.println("ResultsSet:"+e);
			}
			
			String myMainTable= " ";
			int endCount = 0;
			int cnt=0;
			int cnt2=0;
			while (myRS!=null&&myRS.next())
			{
				bltNIM3_Encounter working_bltNIM3_Encounter  =  new bltNIM3_Encounter(new Integer(myRS.getString("EncounterID")));
				bltNIM3_Document working_bltNIM3_Document  =  new bltNIM3_Document(working_bltNIM3_Encounter.getReportFileID());
				%>
                <tr>
                	<td><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Encounter.getDateOfService())%>" /></jsp:include></td>
                	<td><%if (working_bltNIM3_Encounter.getReportFileID()>0){%><input  class="inputButton_md_Create"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=working_bltNIM3_Encounter.getReportFileID()%>&KM=p';" value="View Report" /><%}else{%>Not Available<%}%></td>
                	<td><%=working_bltNIM3_Document.getDocName()%>&nbsp;</td>
                 </tr>
                <%
			}
		%>
        </table>
    
    
    
    
    </td>
    </tr>
  <tr>
    <td colspan="2">&nbsp;
    
    </td>
  </tr>
           </table>

	   
		   
        </td></tr></table>

        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>