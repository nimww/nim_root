<%@page  contentType = "application/txt"  language="java" import="com.winstaff.*" %><%@ include file="../generic/CheckLogin.jsp" %><%


//initial declaration of list class and parentID
String sDownloadName = "";
boolean accessValid = false;
sDownloadName        =   "BillingDownload.txt";
accessValid = true;    
//page security
if (accessValid&&isScheduler3)
{
//out.println(sFileName);
//out.println(sDownloadName);
//out.println(bDownload);

	try
	{
		response.setHeader("Content-Disposition", "attachment; filename="+sDownloadName  ); 
//start
		java.text.SimpleDateFormat QBDate = new java.text.SimpleDateFormat("MM/dd/yyyy");
        String myVal ="";
        //by default all payments that have been "Billed" (have a bill sent date) will be exported as long as the QB Export date is blank.
        //If the date is not blank, then the bill has already been exported.  If includeALL is set to TRUE.. then all cases will come out
        // If the From/Dates are not null, then it will filter the above results to only those billed between FromDate-ToDate
		String BillType = request.getParameter("BillType");
		String startDate = PLCUtils.getDBDFT_AsString(PLCUtils.getSubDate(request.getParameter("StartDate")));
		String endDate = PLCUtils.getDBDFT_AsString(PLCUtils.getSubDate(request.getParameter("EndDate")));
		
		if (BillType.equalsIgnoreCase("AR"))
		{
	        //myVal += "!TRNS\tTRNSID\tTRNSTYPE\tDATE\tACCNT\tNAME\tCLASS\tAMOUNT\tDOCNUM\tMEMO\tCLEAR\tTOPRINT\tADDR1\tADDR2\tADDR3\tTERMS\tPAID\n" +                "!SPL\tSPLID\tTRNSTYPE\tDATE\tACCNT\tNAME\tCLASS\tAMOUNT\tDOCNUM\tMEMO\tCLEAR\tQNTY\tPRICE\tINVITEM\tTAXABLE\t\t\n" +                "!ENDTRNS\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n";
	        out.print("!TRNS\tTRNSID\tTRNSTYPE\tDATE\tACCNT\tNAME\tCLASS\tAMOUNT\tDOCNUM\tMEMO\tCLEAR\tTOPRINT\tADDR1\tADDR2\tADDR3\tADDR4\tTERMS\tPAID\n" +                "!SPL\tSPLID\tTRNSTYPE\tDATE\tACCNT\tNAME\tCLASS\tAMOUNT\tDOCNUM\tMEMO\tCLEAR\tQNTY\tPRICE\tINVITEM\tTAXABLE\t\t\t\n" +                "!ENDTRNS\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n");
			searchDB2 mySS = new searchDB2();
			java.sql.ResultSet myRS = null;
			try
			{
				String mySQL = ("select EncounterID from tNIM3_Encounter where tNIM3_Encounter.EncounterStatusID in ("+NIMUtils.getSQL_Encounters_Active()+") and tNIM3_Encounter.sentto_bill_pay >'1800-01-04' and (tNIM3_Encounter.sentto_bill_pay>'"+startDate+"' AND tNIM3_Encounter.sentto_bill_pay<'"+endDate+"') and tNIM3_Encounter.exportpaymenttoqb<'1800-01-02'");
				myRS = mySS.executeStatement(mySQL);
	//            myVal += mySQL;
				int cnt = 0;
				while(myRS!=null&&myRS.next())
				{
					Integer iEncounterID = new Integer(myRS.getString("EncounterID"));
					try
					{
						cnt++;
						NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"Billing Load");
						if (myEO2.getNIM3_Encounter().getAppointmentID()>0)
						{
							NIM3_AddressObject myPract_AO = myEO2.getAppointmentPracticeAddressObject();
							String sPracticeState = myPract_AO.getState();
							String sEncMemo = myEO2.getNIM3_CaseAccount().getPatientFirstName() + " " + myEO2.getNIM3_CaseAccount().getPatientLastName() + " - " + myEO2.getNIM3_CaseAccount().getCaseClaimNumber();
							NIM3_AddressObject AOBillTo = NIMUtils.getWhoShouldIBillTo(myEO2.getNIM3_PayerMaster().getPayerID(), myEO2.getNIM3_CaseAccount().getBillToContactID());
							String sBill1 = AOBillTo.getAddressName();
							sBill1 = myEO2.getNIM3_PayerMaster().getPayerName();
							String sBill2 = AOBillTo.getAddress1();
							String sBill3 = AOBillTo.getAddress2();
							String sBill4 = AOBillTo.getCity() + ", " + AOBillTo.getState() + " " + AOBillTo.getZIP();
							String sClass = myEO2.getNIM3_PayerMaster().getAcquisitionDivision() + "-" + sPracticeState;

							bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID);
							//declaration of Enumeration
							bltNIM3_Service        working_bltNIM3_Service;
							com.winstaff.ListElement         leCurrentElement;
							java.util.Enumeration eList = bltNIM3_Service_List.elements();
							String tempServices = "";
							double EncTotal = 0.0;
							if (eList.hasMoreElements())
							{
								 while (eList.hasMoreElements())
								 {
									leCurrentElement    = (ListElement) eList.nextElement();
									working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
									String myGroup =  NIMUtils.getRadiologyGroup(working_bltNIM3_Service.getCPT());
									if (myGroup.equalsIgnoreCase(""))
									{
										myGroup = "other";
									}
//									String sItemDisplay = myGroup + " - " + working_bltNIM3_Service.getCPT() + " - " + NIMUtils.getCPTText(working_bltNIM3_Service.getCPT());
									String sItemDisplay = myGroup + " - " + working_bltNIM3_Service.getCPT();
									String ARCode = "42300";
									if (myGroup.indexOf("mr_")==0)
									{
										ARCode = "42000";
									}
									else if (myGroup.indexOf("emg")==0)
									{
										ARCode = "42100";
									}
									else if (myGroup.indexOf("ct_")==0)
									{
										ARCode = "42050";
									}
									else 
									{
										ARCode = "42300";
									}
									if (working_bltNIM3_Service.getServiceStatusID()==1)
									 {
										 tempServices+="SPL\t\tINVOICE\t"+QBDate.format(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay())+"\t"+ARCode+"\t\t"+sClass+"\t-"+working_bltNIM3_Service.getAllowAmount()+"\t"+myEO2.getNIM3_Encounter().getScanPass() + "\t"+sEncMemo+"\tN\t-1\t"+working_bltNIM3_Service.getAllowAmount()+"\t"+ sItemDisplay + "\tN\t\tNet 30\tN\n";
										 EncTotal+=  working_bltNIM3_Service.getAllowAmount();
									 }
								 }
							}
	//						myVal += "TRNS\t\tINVOICE\t"+QBDate.format(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay())+"\t11000\t"+myEO2.getNIM3_PayerMaster().getPayerID()+"\t"+sClass+"\t"+EncTotal+"\t"+myEO2.getNIM3_Encounter().getScanPass()+"\t"+sEncMemo+"\tN\tN\t"+sBill1+"\t"+sBill2+"\t"+sBill2+"\tNet 30\tN\n";
	//						myVal += tempServices;
	//						myVal +="ENDTRNS\n";
							
							out.print("TRNS\t\tINVOICE\t"+QBDate.format(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay())+"\t11000\t"+myEO2.getNIM3_PayerMaster().getPayerName()+"\t"+sClass+"\t"+EncTotal+"\t"+myEO2.getNIM3_Encounter().getScanPass()+"\t"+sEncMemo+"\tN\tN\t"+sBill1+"\t"+sBill2+"\t"+sBill3+"\t"+sBill4+"\tNet 30\tN\n");
							out.print(tempServices);
							out.print("ENDTRNS\n");
						}
						else
						{
							DebugLogger.printLine("NIMUtils:getBillingExport_PaymentsDue:Bad Encounter (Caught):[iEncounterID = ]"+iEncounterID)	;
						}
					}
					catch (Exception eeee2)
					{
						DebugLogger.printLine("NIMUtils:getBillingExport_PaymentsDue:Bad Encounter:[iEncounterID = ]"+iEncounterID+"["+eeee2+"]");
					}
				}
			}
			catch (Exception eeee)
			{
				DebugLogger.printLine("NIMUtils:getBillingExport_PaymentsDue:["+eeee+"]");
			}
			mySS.closeAll();
//out.print(myVal);
//end
		}
		else if (BillType.equalsIgnoreCase("AP"))
		{
	        //myVal += "!TRNS\tTRNSID\tTRNSTYPE\tDATE\tACCNT\tNAME\tCLASS\tAMOUNT\tDOCNUM\tMEMO\tCLEAR\tTOPRINT\tADDR1\tADDR2\tADDR3\tTERMS\tPAID\n" +                "!SPL\tSPLID\tTRNSTYPE\tDATE\tACCNT\tNAME\tCLASS\tAMOUNT\tDOCNUM\tMEMO\tCLEAR\tQNTY\tPRICE\tINVITEM\tTAXABLE\t\t\n" +                "!ENDTRNS\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n";
	        out.print("!TRNS\tTRNSID\tTRNSTYPE\tDATE\tACCNT\tCLASS\tNAME\tAMOUNT\tDOCNUM\tMEMO\tCLEAR\tTOPRINT\tADDR1\tADDR2\tADDR3\tADDR4\tTERMS\n!SPL\tSPLID\tTRNSTYPE\tDATE\tACCNT\tCLASS\tNAME\tAMOUNT\tDOCNUM\tMEMO\tCLEAR\t\t\t\t\t\t\n!ENDTRNS\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n");
			searchDB2 mySS = new searchDB2();
			java.sql.ResultSet myRS = null;
			try
			{
				String mySQL = ("select EncounterID from tNIM3_Encounter where tNIM3_Encounter.EncounterStatusID in ("+NIMUtils.getSQL_Encounters_Active()+") and tNIM3_Encounter.sentto_bill_pay >'1800-01-04' and (tNIM3_Encounter.sentto_bill_pay>'"+startDate+"' AND tNIM3_Encounter.sentto_bill_pay<'"+endDate+"') and tNIM3_Encounter.exportpaymenttoqb<'1800-01-02'");
				myRS = mySS.executeStatement(mySQL);
	//            myVal += mySQL;
				int cnt = 0;
				while(myRS!=null&&myRS.next())
				{
					Integer iEncounterID = new Integer(myRS.getString("EncounterID"));
					try
					{
						cnt++;
						NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"Billing Load");
						if (myEO2.getNIM3_Encounter().getAppointmentID()>0)
						{
							NIM3_AddressObject myPract_AO = myEO2.getAppointmentPracticeAddressObject();
							String sPracticeState = myPract_AO.getState();
							String sEncMemo = myEO2.getNIM3_CaseAccount().getPatientFirstName() + " " + myEO2.getNIM3_CaseAccount().getPatientLastName() + " - " + myEO2.getNIM3_CaseAccount().getCaseClaimNumber();
							String sBill1 = myEO2.getAppointment_PracticeMaster().getPracticeName();
							String sBill2 = myEO2.getAppointment_PracticeMaster().getOfficeAddress1();
							String sBill3 = myEO2.getAppointment_PracticeMaster().getOfficeAddress2();
							String sBill4 = myEO2.getAppointment_PracticeMaster().getOfficeCity() + ", " + new bltStateLI(myEO2.getAppointment_PracticeMaster().getOfficeStateID()).getShortState() + " " + myEO2.getAppointment_PracticeMaster().getOfficeZIP();
							String sClass = sPracticeState;
							String NetTerms = "60";
							bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID);
							//declaration of Enumeration
							bltNIM3_Service        working_bltNIM3_Service;
							com.winstaff.ListElement         leCurrentElement;
							java.util.Enumeration eList = bltNIM3_Service_List.elements();
							String tempServices = "";
							double EncTotal = 0.0;
							if (eList.hasMoreElements())
							{
								 while (eList.hasMoreElements())
								 {
									leCurrentElement    = (ListElement) eList.nextElement();
									working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
									String myGroup =  NIMUtils.getRadiologyGroup(working_bltNIM3_Service.getCPT());
									if (myGroup.equalsIgnoreCase(""))
									{
										myGroup = "other";
									}
//									String sItemDisplay = myGroup + " - " + working_bltNIM3_Service.getCPT() + " - " + NIMUtils.getCPTText(working_bltNIM3_Service.getCPT());
									String sItemDisplay = myGroup + " - " + working_bltNIM3_Service.getCPT();

									String APCode = "54000";
									if (myGroup.indexOf("mr_")==0)
									{
										APCode = "51000";
									}
									else if (myGroup.indexOf("emg")==0)
									{
										APCode = "52000";
									}
									else if (myGroup.indexOf("ct_")==0)
									{
										APCode = "51500";
									}
									else 
									{
										APCode = "54000";
									}
									
									if (working_bltNIM3_Service.getServiceStatusID()==1)
									 {
										 tempServices+="SPL\t\tBILL\t"+QBDate.format(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay())+"\t"+APCode+"\t"+sClass+"\t"+myEO2.getAppointment_PracticeMaster().getPracticeName()+"\t"+ working_bltNIM3_Service.getPaidOutAmount()+"\t"+myEO2.getNIM3_Encounter().getScanPass() + "\t"+sEncMemo+"\tN\t\t\t\t\t\t\n";
										 EncTotal+=  working_bltNIM3_Service.getPaidOutAmount();
									 }
								 }
							}
	//						myVal += "TRNS\t\tINVOICE\t"+QBDate.format(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay())+"\t11000\t"+myEO2.getNIM3_PayerMaster().getPayerID()+"\t"+sClass+"\t"+EncTotal+"\t"+myEO2.getNIM3_Encounter().getScanPass()+"\t"+sEncMemo+"\tN\tN\t"+sBill1+"\t"+sBill2+"\t"+sBill2+"\tNet 30\tN\n";
	//						myVal += tempServices;
	//						myVal +="ENDTRNS\n";
							
							out.print("TRNS\t\tBILL\t"+QBDate.format(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay())+"\t21000\t"+sClass+"\t"+myEO2.getAppointment_PracticeMaster().getPracticeName()+"\t-"+EncTotal+"\t"+myEO2.getNIM3_Encounter().getScanPass()+"\t"+sEncMemo+"\tN\tN\t"+sBill1+"\t"+sBill2+"\t"+sBill3+"\t"+sBill4+"\tNet "+NetTerms+"\n");
							out.print(tempServices);
							out.print("ENDTRNS\n");
						}
						else
						{
							DebugLogger.printLine("NIMUtils:getBillingExport_PaymentsDue:Bad Encounter (Caught):[iEncounterID = ]"+iEncounterID)	;
						}
					}
					catch (Exception eeee2)
					{
						DebugLogger.printLine("NIMUtils:getBillingExport_PaymentsDue:Bad Encounter:[iEncounterID = ]"+iEncounterID+"["+eeee2+"]");
					}
				}
			}
			catch (Exception eeee)
			{
				DebugLogger.printLine("NIMUtils:getBillingExport_PaymentsDue:["+eeee+"]");
			}
			mySS.closeAll();
//out.print(myVal);
//end
		}


	}
	catch(Exception e)
	{
	}
	
}
else
{
	out.println("illegal");
}
%>