<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*, java.lang.*" errorPage="" %>
<!DOCTYPE html>
<%@ include file="../generic/CheckLogin.jsp" %>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head>
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">	
$(document).keyup(function(e) {
	if (e.keyCode == 27) { parent.esc(); }
});
</script>

</head>

<body>
      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler) 
    {
        accessValid = true;
    }
    else if (isAdjuster) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
	}
    else if (isProvider) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
	}
	
	
  //page security
  if (accessValid)
  {%>
	  
	  <%!public String capitalize(String param){
			  String first;
			  String rest;
			  if (param.length() > 1){
			  first = param.substring(0,1).toUpperCase();
			  rest =  param.substring(1);
			  return first+rest;
			  }
			  return param;
			  
		  }
	  %>
	  <% java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MM/dd/yyyy");
//      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	boolean submit = Boolean.parseBoolean(request.getParameter("submit"));
	if (isAdjuster||isAdjuster2||isAdjuster3||isProvider||isScheduler)
	  {
		  String query = request.getParameter("query");
		  //String query = "2"; //test query
		  String query2 = request.getParameter("query2");
		  String mywhere ="";
		  String mywhere2 ="";
		  
		  searchDB2 search = new searchDB2();
		  ResultSet result = null;;
		  
		  if (query2.equalsIgnoreCase("undefined")){
			  mywhere = "lower(clientclaimnum) like lower('%"+query+"%') or lower(patientlastname) like lower('%"+query+"%')";
		  }
		  else{
			  mywhere = "lower(patientlastname) like lower('%"+query+"%')";
			  mywhere2 = " AND lower(patientfirstname) like lower('%"+query2+"%')";  
		  }
		  
		  
		  String SQL = "select employername, dateofinjury, patientgender, patientssn, patientfirstname, patientlastname, patientaddress1, patientaddress2, patientcity, patientstate, stateid, patientzip, patientcellphone, patientdob, clientclaimnum, casemanagerfirstname, casemanagerlastname, adjusterfirstname, adjusterlastname, uniquecreatedate from tnim3_eligibilityreference inner join tstateli on tstateli.shortstate = tnim3_eligibilityreference.patientstate where "+mywhere+mywhere2+" AND payerid = '307' order by uniquecreatedate desc";
		  //out.println(SQL);
		  result = search.executeStatement(SQL);
		  
		  %>
  
        <div id="amer-form-container">
			
			<% boolean isEmpty = true;
			while (result!=null&&result.next()){
				isEmpty = false;
				%>
            	<div class="amer-two-col ameri-results">
                    <div class="left">
                        <p><strong>Client Claim#: </strong><%=result.getString("clientclaimnum")%></p>
                        <p><%=result.getString("patientlastname")%>,&nbsp;<%=result.getString("patientfirstname")%></p>
                        <p><%=result.getString("patientaddress1")%></p>
                        <p><%=result.getString("patientaddress2")%></p>
                        <p><%=result.getString("patientcity")%>,&nbsp;<%=result.getString("patientstate")%>&nbsp;<%=result.getString("patientzip")%></p>
                        <p>Phone: <%=result.getString("patientcellphone")%></p>
                    </div>
                    <div class="right">
                        <p>DOB: <%=df.format(result.getDate("patientdob"))%></p>
                        <p>ADJ: <span><%=result.getString("adjusterfirstname")%>&nbsp;<%=result.getString("adjusterlastname")%></span></p>
                        <p>Case Manager: <span><%=result.getString("casemanagerfirstname")%> <%=result.getString("casemanagerlastname")%></span></p>
                        <p>Employer: <span><%=result.getString("employername")%></span></p>
                        <p>DOI: <%=df.format(result.getDate("dateofinjury"))%></p>
                        <p>SSN: <%=result.getString("patientssn")%></p>
	                    <%if (submit){ %>
	                        <input type="button" value="Select" class="amer-input-button" onClick="parent.selectClaimant(
	                        '<%=result.getString("clientclaimnum")%>', 
							'<%=result.getString("patientlastname")%>', 
							'<%=result.getString("patientfirstname")%>', 
							'<%=result.getString("patientaddress1")%>', 
							'<%=result.getString("patientaddress2")%>', 
							'<%=result.getString("patientcity")%>', 
							'<%=result.getString("patientstate").toUpperCase()%>', 
							'<%=result.getString("patientzip")%>', 
							'<%=result.getString("patientcellphone")%>', 
							'<%=df.format(result.getDate("patientdob"))%>', 
							'<%=result.getString("adjusterfirstname")%>', 
							'<%=result.getString("adjusterlastname")%>', 
							'<%=result.getString("casemanagerfirstname")%>', 
							'<%=result.getString("casemanagerlastname")%>', 
							'<%=result.getString("employername")%>', 
							'<%=df.format(result.getDate("dateofinjury"))%>',
	                        '<%=result.getString("patientgender")%>',
	                        '<%=result.getString("patientssn")%>',
	                        '<%=result.getInt("stateid")%>');">
	                    <%} %>
                    </div>
                </div>	
	        <%}
			if (isEmpty){out.print("<h1>No results</h1>");}
			
            search.closeAll();%>
        </div>
      	<%if (submit){ %>
      		<p><strong>If claimant can not be found, <a href="#" onclick="parent.manualForm();">click here to add</a></strong></p>
      	<%} %>

          
          <%
	  }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>