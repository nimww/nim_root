<%@page import="java.io.*,com.winstaff.*, java.util.*, java.sql.*, java.text.SimpleDateFormat,com.google.gson.Gson;"%>
<%!class incomingService {
		public String cpt = "";
		public String bp = "";
		public String mod = "";
		public String qty = "";
		public String diag1 = "";
		public String diag2 = "";
		public String diag3 = "";
		public String diag4 = "";
		public incomingService(String cpt, String bp, String mod, String qty, String diag1, String diag2, String diag3, String diag4) {
			this.cpt = cpt;
			this.bp = bp;
			this.mod = mod;
			this.qty = qty;
			this.diag1 = diag1;
			this.diag2 = diag2;
			this.diag3 = diag3;
			this.diag4 = diag4;
		}
		public String getCpt() {
			return cpt;
		}
		public void setCpt(String cpt) {
			this.cpt = cpt;
		}
		public String getBp() {
			return bp;
		}
		public void setBp(String bp) {
			this.bp = bp;
		}
		public String getMod() {
			return mod;
		}
		public void setMod(String mod) {
			this.mod = mod;
		}
		public String getQty() {
			return qty;
		}
		public void setQty(String qty) {
			this.qty = qty;
		}
		public String getDiag1() {
			return diag1;
		}
		public void setDiag1(String diag1) {
			this.diag1 = diag1;
		}
		public String getDiag2() {
			return diag2;
		}
		public void setDiag2(String diag2) {
			this.diag2 = diag2;
		}
		public String getDiag3() {
			return diag3;
		}
		public void setDiag3(String diag3) {
			this.diag3 = diag3;
		}
		public String getDiag4() {
			return diag4;
		}
		public void setDiag4(String diag4) {
			this.diag4 = diag4;
		}
		
	}%>
<%
	String firstName = "";
	String lastName = "";
	String address = "";
	String city = "";
	String state = "";
	String zip = "";
	String ssn = "";
	String phone = "";
	String dob = "";
	String doi = "";
	String faxid = "";
	String claimNumber = "";
	String assignedTo = "";
	String adjuster = "";
	String refMd = "";
	String refSource = "";
	
	List<incomingService> incomingServiceList = new ArrayList<incomingService>();

	boolean isGood = true;
	try {
		firstName = request.getParameter("firstName");
		lastName = request.getParameter("lastName");
		address = request.getParameter("address");
		city = request.getParameter("city");
		state = request.getParameter("state");
		zip = request.getParameter("zip");
		ssn = request.getParameter("ssn");
		phone = request.getParameter("phone");
		dob = request.getParameter("dob");
		doi = request.getParameter("doi");
		faxid = request.getParameter("faxid");
		claimNumber = request.getParameter("claimNumber");
		assignedTo = request.getParameter("assignedTo");
		adjuster = request.getParameter("adjuster");
		refMd = request.getParameter("refMd");
		refSource = request.getParameter("refSource");
	} catch (Exception e) {
		isGood = false;
	}
	for (int x = 0; x < 5; x++) {
			String cpt = request.getParameter("service[" + x + "].cpt");
			String bp = request.getParameter("service[" + x + "].bp");
			String mod = request.getParameter("service[" + x + "].mod");
			String qty = request.getParameter("service[" + x + "].qty");
			String diag1 = request.getParameter("service[" + x + "].diag1");
			String diag2 = request.getParameter("service[" + x + "].diag2");
			String diag3 = request.getParameter("service[" + x + "].diag3");
			String diag4 = request.getParameter("service[" + x + "].diag4");
			
			if(!cpt.isEmpty()){
				incomingServiceList.add(new incomingService(cpt, bp, mod, qty, diag1, diag2, diag3, diag4));
			} 
	}

	SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy");
	SimpleDateFormat timestamp = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	java.util.Date now = new java.util.Date();

	if (isGood) {

		bltNIM3_CaseAccount ca = new bltNIM3_CaseAccount();
		bltNIM3_Referral rf = new bltNIM3_Referral();
		bltNIM3_Encounter en = new bltNIM3_Encounter();
		try{
			ca.setPayerID(new bltUserAccount(new Integer(adjuster)).getPayerID());
		} catch(Exception e){
			ca.setPayerID(23);
		}
		
		ca.setCaseClaimNumber(claimNumber);
		ca.setCaseStatusID(1);
		ca.setPatientFirstName(firstName);
		ca.setPatientLastName(lastName);
		ca.setPatientAddress1(address);
		ca.setPatientCity(city);
		ca.setPatientHomePhone(phone);
		ca.setPatientStateID(new Integer(state));
		ca.setPatientZIP(zip);
		
		try{
			ca.setAdjusterID(new Integer(adjuster));
		} catch(Exception e){
		}
		try{
			ca.setAssignedToID(new Integer(assignedTo));
		} catch(Exception e){
		}
		
		try {
			ca.setDateOfInjury(date.parse(doi));
		} catch (Exception e) {
		}
		try {
			ca.setPatientDOB(date.parse(dob));
		} catch (Exception e) {
		}
		ca.setPatientSSN((ssn.isEmpty() ? "Not Given":ssn));
		ca.setUniqueCreateDate(now);
		ca.setUniqueModifyDate(now);
		ca.setUniqueModifyComments("Create via N4 FAX API");
		ca.commitData();
		rf.setCaseID(ca.getUniqueID());
		try{
			rf.setReferringPhysicianID(new Integer(refMd));	
		} catch(Exception e){
			e.printStackTrace();
		}
		try{
			rf.setReferredByContactID(new Integer(refSource));	
		} catch(Exception e){
			e.printStackTrace();
		}
		
		rf.setReferralStatusID(1);
		rf.setUniqueModifyComments("Create via N4 FAX API");
		rf.setReferralTypeID(3);
		rf.setAttendingPhysicianID(52);
		rf.setReferralStatusID(1);
		
		rf.commitData();

		en.setReferralID(rf.getUniqueID());
		en.setUniqueModifyComments("Create via N4 FAX API");
		en.setEncounterStatusID(1);
		en.setEncounterTypeID(9);
		en.commitData();
		en.setScanPass("SP" + en.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random() * 100) + com.winstaff.password.RandomString.generateString(2).toUpperCase());
		en.commitData();

		for (incomingService is : incomingServiceList) {
			bltNIM3_Service sr = new bltNIM3_Service();
			sr.setCPT(is.getCpt());
			sr.setUniqueModifyComments("Create via N4 FAX API");
			sr.setCPTQty(new Integer(is.getQty()));
			sr.setCPTModifier(is.getMod());
			sr.setServiceStatusID(1);
			sr.setCPTBodyPart(is.getBp());
			sr.setServiceTypeID(3);
			sr.setEncounterID(en.getUniqueID());
			sr.setdCPT1(is.getDiag1());
			sr.setdCPT2(is.getDiag2());
			sr.setdCPT3(is.getDiag3());
			sr.setdCPT4(is.getDiag4());
			sr.commitData();
		} %>

		<script>
		document.location.href = "http://beta.nextimagemedical.com/fax/worklist/<%=faxid%>/<%=en.getScanPass()%>";
		</script>
	<%} else {
		out.println("0");
	}
%>
