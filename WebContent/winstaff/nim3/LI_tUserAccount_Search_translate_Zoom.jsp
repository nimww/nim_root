
<%@ include file="../generic/CheckLogin.jsp" %>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;" >
<%
Integer CurrentSelection = new Integer(0);
try
{
	CurrentSelection = new Integer(request.getParameter ("CurrentSelection"));
	if (CurrentSelection!=null&&CurrentSelection.intValue()>0)
	{
		com.winstaff.bltUserAccount myUA = new com.winstaff.bltUserAccount (new Integer(CurrentSelection));
		com.winstaff.bltNIM3_PayerMaster myPA = new com.winstaff.bltNIM3_PayerMaster (myUA.getPayerID());
		if (myUA.getAccountType().equalsIgnoreCase("PhysicianID")||myUA.getAccountType().equalsIgnoreCase("PhysicianID2")||myUA.getAccountType().equalsIgnoreCase("PhysicianID3")||myUA.getAccountType().equalsIgnoreCase("SchedulerID")||myUA.getAccountType().equalsIgnoreCase("SchedulerID2")||myUA.getAccountType().equalsIgnoreCase("SchedulerID3")||myUA.getAccountType().equalsIgnoreCase("AdjusterID")||myUA.getAccountType().equalsIgnoreCase("AdjusterID2")||myUA.getAccountType().equalsIgnoreCase("AdjusterID3")||myUA.getAccountType().equalsIgnoreCase("PayerAdminID"))
		{
				%><table border="1" cellspacing="0" cellpadding="3">
                  <tr style="font-family: Arial, Helvetica, sans-serif;font-size:24px;color:#000">
                    <td colspan="2">Contact Details for UserID: <%=myUA.getUniqueID()%> </td>
                  </tr>
                  <tr>
                    <td>Name</td>
                    <td><strong><%=myUA.getContactFirstName()%>&nbsp;<%=myUA.getContactLastName()%></strong>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>Payer/Group</td>
                    <td><strong><%=myPA.getPayerName()%></strong>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>Phone</td>
                    <td><strong><%=myUA.getContactPhone()%></strong>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>Fax</td>
                    <td><strong><%=myUA.getContactFax()%></strong>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td><strong><%if (!myUA.getContactEmail().equalsIgnoreCase("Declined")){%><a href="mailto:<%=myUA.getContactEmail()%>"><%=myUA.getContactEmail()%></a><%}else{%><%=myUA.getContactEmail()%><%}%></strong>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>Alt Email</td>
                    <td><strong><%if (!myUA.getContactEmail2().equalsIgnoreCase("Declined")){%><a href="mailto:<%=myUA.getContactEmail2()%>"><%=myUA.getContactEmail2()%></a><%}else{%><%=myUA.getContactEmail2()%><%}%></strong>&nbsp;</td>
                  </tr>
<%if (isScheduler)
{%>
                  <tr>
                    <td>User Notes</td>
                    <td><%=myUA.getImportantNotes()%><br /><%=myUA.getImportantNotes_Alert()%><br /><%=myUA.getEmailAlertNotes_Alert()%></td>
                  </tr>
                  <tr>
                    <td>UserName</td>
                    <td><strong><%=myUA.getLogonUserName()%></strong>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>ScanPass/Appt Conf</td>
                    <td>[<strong><jsp:include page="../generic/tUAAlertsLILong_translate.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myUA.getComm_Alerts_LevelUser()%>" />
                </jsp:include></strong>]</td>
                  </tr>
                  <tr>
                    <td>Other Alerts</td>
                    <td>[<strong><jsp:include page="../generic/tUAAlertsLILong_translate.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myUA.getComm_Alerts2_LevelUser()%>" />
                </jsp:include></strong>]</td>
                  </tr>
                  <tr>
                    <td>Reports</td>
                    <td>[<strong><jsp:include page="../generic/tUAAlertsLILong_translate.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myUA.getComm_Report_LevelUser()%>" />
                </jsp:include></strong>]</td>
                  </tr>
                  <tr>
                    <td colspan="2"><hr /></td>
                  </tr>
                  <tr>
                    <td>NPI</td>
                    <td><strong><%=myUA.getUserNPI()%></strong></td>
                  </tr>
                  <tr>
                    <td>State License</td>
                    <td><strong><%=myUA.getUserStateLicense()%></strong> [<strong><%=myUA.getUserStateLicenseDesc()%></strong>]             </td>
                  </tr>
                  <tr>
                    <td colspan="2"><hr />Address (if on file)</td>
                  </tr>
                  <tr>
                    <td colspan="2"><strong><%=myUA.getCompanyName()%></strong><br />
<%=myUA.getContactAddress1()%>&nbsp;<%=myUA.getContactAddress2()%><br />
<%=myUA.getContactCity()%>, <%=(new com.winstaff.bltStateLI(myUA.getContactStateID())).getShortState()%> <%=myUA.getContactZIP()%></td>
                  </tr>
<%
}%>
                </table>
		<%
		}
	}
}
catch(Exception e)
{
}%>