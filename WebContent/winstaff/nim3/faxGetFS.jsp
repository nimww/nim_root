<%@page language="java" contentType="application/json; charset=UTF-8" import="com.winstaff.*, java.lang.*,java.util.*, com.google.gson.Gson;"%>
<%!class searchResult {
		String value;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}%>
<%
	
// Double getFeeSchedulePrice(Integer FSID, Double FSID_Percent, String sZIP, String sCPT, String sMod, java.util.Date EffDate)
	Integer payerId = new Integer(request.getParameter("payerId"));
	String zip = request.getParameter("zip");
	String cpt = request.getParameter("cpt");
	String mod = request.getParameter("mod");
	java.util.Date date = new java.util.Date();
	
	bltNIM3_PayerMaster pm = new bltNIM3_PayerMaster(payerId);
	
	Double fee = 0d;
	
	fee = NIMUtils.getFeeSchedulePrice(pm.getContract1_FeeScheduleID(), pm.getContract1_FeePercentage(), zip, cpt, mod, date);
	
	if(fee==0d){
		fee = NIMUtils.getFeeSchedulePrice(pm.getContract2_FeeScheduleID(), pm.getContract2_FeePercentage(), zip, cpt, mod, date);
	} 
	if(fee==0d){
		fee = NIMUtils.getFeeSchedulePrice(pm.getContract3_FeeScheduleID(), pm.getContract3_FeePercentage(), zip, cpt, mod, date);
	} 
	response.setHeader("Access-Control-Allow-Origin", "*");
	searchResult sr = new searchResult();
	/* String value = ; */
	sr.setValue(String.format("%.2f", fee));
	
	out.println(new Gson().toJson(sr));
		
%>