<%@page contentType="text/html" language="java" import="java.util.ArrayList,java.sql.*,com.winstaff.*,org.apache.http.*,java.io.*,java.util.List,com.winstaff.EDI_MCMC_Mod.*"%>
<%@ include file="../generic/CheckLogin.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<meta charset="utf-8">

<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">


<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link href="https://rawgithub.com/jharding/typeahead.js-bootstrap.css/master/typeahead.js-bootstrap.css" rel="stylesheet" media="screen">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>MCMC</title>
<script>
		$(document).ready(function(){
			$('.showMyTbl').click(function(){
				$('#myTable').toggle('slow');
			});
			$('.myedit').click(function(){
				$('#myeditshow').show();
			});
			$('.mycancel').click(function(){
				$('#myeditshow').hide();
				$('.clear').val("");
			});
			$('#commit').click(function(e) {
		        var isValid = true;
		        $('input[type="text"].required').each(function() {
		            if ($.trim($(this).val()) == '') {
		                isValid = false;
		                $(this).css({
		                    "border": "1px solid red",
		                    "background": "#FFCECE"
		                });
		            }
		            else {
		                $(this).css({
		                    "border": "",
		                    "background": ""
		                });
		            }
		        });
		        if (isValid == false) 
		            e.preventDefault();
		        else 
		            alert('Thank you for submitting');
		    });
			$('.decimal').keyup(function(){
			    var val = $(this).val();
			    if(isNaN(val)){
			         val = val.replace(/[^0-9\.]/g,'');
			         if(val.split('.').length>2) 
			             val =val.replace(/\.+$/,"");
			    }
			    $(this).val(val); 
			});
		});
		function sticky_relocate() {
		    var window_top = $(window).scrollTop();
		    var div_top = $('#sticky-anchor').offset().top;
		    if (window_top > div_top) {
		        $('#sticky').addClass('stick');
		    } else {
		        $('#sticky').removeClass('stick');
		    }
		}

		$(function () {
		    $(window).scroll(sticky_relocate);
		    sticky_relocate();
		});
</script>
</head>
		<%
		if(request.getParameter("lastname") != null){
			EDI_MCMC_Mod mcmod = new EDI_MCMC_Mod(request.getParameter("lastname"));
			List<mcmcModel> mylist = mcmod.getMCMCList();
			List<lnModel> mln = mcmod.getLNModel();
		%>
	<body style="padding:1%;">
	
	
	
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

 
	
	
	
	
	
	
	
		<div class="container-fluid">
			
			<div class="" >
				<div class="row alert alert-warning" role="alert" style="">
					<div class="">
						<h3>Select Action</h3>
						<form method="post" class="form-search">
							<div class="row">
								<div class="col-md-4" style="">
									<label style="margin-top:5px">Search by last name</label>
						  			<input type="text" name="lastname" class="input-medium search-query" id="uqinput"  >
						  			<button type="submit" class="btn btn-info">Search</button>
						  		</div>
						  		<div class="col-md-2">
						  			<a class="btn btn-info showMyTbl">Toggle Full Table</a>
						  		</div>
						  		<div class="col-md-2">
						  			<a class="btn btn-info myedit">Edit data</a>
						  		</div>
						  	</div>
						</form>
					</div>
					<div style="display:none;" id="myeditshow">
						<h3 style="color:red;">Warning you are now editing data</h3>
						<div class="">
							<form method="post" >
								<div class="col-md-2">
								<label>BillID</label>
									<input name="billid" type="text" class="input-medium clear required decimal" >
								</div>
								<div class="col-md-2">
								<label>Status</label>
									<input name="casestatus" type="text" class="input-medium clear required decimal"  >
								</div>
								<div class="col-md-2" >
								<label>Network Allow</label>
									<input name="netallow" type="text" class="input-medium clear required decimal" >
								</div>
								<div class="col-md-2" >
								<label>Network Savings</label>
									<input name="netsavings" type="text" class="input-medium clear required decimal" >
								</div>
								<div class="col-md-1"  style="margin-top:12px">
									<button type="submit" class="btn btn-info" id="commit" >Submit</button>
								</div>
								<div class="col-md-1"  style="margin-top:12px">
									<button type="button" id="" class="btn btn-info mycancel" >Cancel</button>
								</div>
							</form>
						</div>
				</div>
				</div>
				<div class="row">
					<h3>Filtered Results</h3>
					<table class="table table-condensed table-hover" style="display:" >
							 	<thead>
							 		<tr>
							 			<th class="col-md-1">MCMC BillId</th>
							 			<th class="col-md-1">Status</th>
							 			<th class="col-md-1">Claim Number</th>
							 			<th class="col-md-1">Receive Date</th>
							 			<th class="col-md-1">Patient Name</th>
							 			<th class="col-md-1">FS</th>
							 			<th class="col-md-1">Provider Charge</th>
							 			<th class="col-md-1">State Allow</th>
							 			<th class="col-md-1">MCMC Allow</th>
							 			<th class="col-md-1">Network Allow</th>
							 			<th class="col-md-1">Network Savings</th>
							 			<th class="col-md-1">CPT Code</th>
							 			<th class="col-md-1">Units</th>
							 		</tr>
							 	</thead>
								<%
									for(int x = 0; x < mln.size(); x++){
								%> 
								<tbody>
								 	<tr>
								 		<td><%=mln.get(x).getBillid() %></td>
								 		<td><%=mln.get(x).getStatus() %></td>
								 		<td><%=mln.get(x).getClaimnumber() %></td>
								 		<td><%=mln.get(x).getReceivedate() %></td>
								 		<td><%=mln.get(x).getPtfirst() + " " + mln.get(x).getPtlast() %></td>
								 		<td><%=mln.get(x).getFs() %></td>
								 		<td><%=mln.get(x).getProviderchrg() %></td>
								 		<td><%=mln.get(x).getStateallow() %></td>
								 		<td><%=mln.get(x).getMcmcallow() %></td>
								 		<td><%=mln.get(x).getNetworkallow() %></td>
								 		<td><%=mln.get(x).getNetworksavings() %></td>
								 		<td><%=mln.get(x).getCpt() %></td>
								 		<td><%=mln.get(x).getUnits() %></td>
								 	</tr>
								 </tbody>
								<%
									}
								%> 
								
					</table>
				</div>
				<div style="display:none;" id="myeditshow">
				<h3 style="color:red;">Warning you are now editing data</h3>
				<div class="row alert alert-warning" role="alert">
					<form method="post" >
						<div class="col-md-2">
						<label>BillID</label>
							<input name="billid" type="text" class="input-medium clear" >
						</div>
						<div class="col-md-2">
						<label>Status</label>
							<input name="casestatus" type="text" class="input-medium clear" >
						</div>
						<div class="col-md-2" >
						<label>Network Allow</label>
							<input name="netallow" type="text" class="input-medium clear" >
						</div>
						<div class="col-md-2" >
						<label>Network Savings</label>
							<input name="netsavings" type="text" class="input-medium clear" >
						</div>
						<div class="col-md-1"  style="margin-top:12px">
							<button type="submit" class="btn btn-info" >Submit</button>
						</div>
						<div class="col-md-1"  style="margin-top:12px">
							<button type="button" id="" class="btn btn-info mycancel" >Cancel</button>
						</div>
					</form>
				</div>
			</div>
			</div>
			
			<div class="" style="">		
				<div class="row" id="myTable" style="display:none;margin-top:10px;" >
				<h3>Full patient table</h3>
					<table class="table table-condensed table-hover" >
							 	<thead>
							 		<tr>
							 			<th class="col-md-1">MCMC BillId</th>
							 			<th class="col-md-1">Status</th>
							 			<th class="col-md-1">Claim Number</th>
							 			<th class="col-md-1">Receive Date</th>
							 			<th class="col-md-1">Patient Name</th>
							 			<th class="col-md-1">FS</th>
							 			<th class="col-md-1">Provider Charge</th>
							 			<th class="col-md-1">State Allow</th>
							 			<th class="col-md-1">MCMC Allow</th>
							 			<th class="col-md-1">Network Allow</th>
							 			<th class="col-md-1">Network Savings</th>
							 			<th class="col-md-1">CPT Code</th>
							 			<th class="col-md-1">Units</th>
							 		</tr>
							 	</thead>
								<%
									for(int x = 0; x < mylist.size(); x++){
								%> 
								<tbody>
								 	<tr>
								 		<td><%=mylist.get(x).getMcmcid() %></td>
								 		<td><%=mylist.get(x).getStatusid() %></td>
								 		<td><%=mylist.get(x).getClaimNumber() %></td>
								 		<td><%=mylist.get(x).getTimeIn() %></td>
								 		<td><%=mylist.get(x).getPatientFirstName()  + " " +  mylist.get(x).getPatientLastName() %></td>
								 		<td><%=mylist.get(x).getFsType() %></td>
								 		<td><%=mylist.get(x).getProviderCharge() %></td>
								 		<td><%=mylist.get(x).getStateAllow() %></td>
								 		<td><%=mylist.get(x).getMcmcAllow() %></td>
								 		<td><%=mylist.get(x).getNetworkAllow() %></td>
								 		<td><%=mylist.get(x).getNetworkSavings() %></td>
								 		<td><%=mylist.get(x).getCptCode() %></td>
								 		<td><%=mylist.get(x).getUnits() %></td>
								 	</tr>
								 </tbody>
								<%
									}
								%> 
								
					</table>
				</div>
			</div>
			
			<% }else if(request.getParameter("billid") != null) {
				EDI_MCMC_Mod mcmcinjection = new EDI_MCMC_Mod( new Integer(request.getParameter("billid")) ,  new Integer(request.getParameter("casestatus")),  new Double(request.getParameter("netallow")),  new Double(request.getParameter("netsavings")));
				mcmcinjection.updateMCMCRow();
			%>
			
			<div class="row" style="padding:15px">
					<div class="alert alert-success" role="alert">
						<h2 style="color:green">Save Successful. </h2>
						<h3>Select Action</h3>
						<form method="post" class="form-search">
							<label>Search by last name</label>
						  	<input type="text" name="lastname" class="input-medium search-query" id="uqinput" style="margin-top:5px" value="">
						  	<button type="submit" class="btn" style="margin-top:5px">Search</button>
						</form>
					</div>
			</div>
			
			<% } else{ %>
				<div class="col-md-2" style="">
					<div class="">
						<h3>Select Action</h3>
						<form method="post" class="form-search">
							<label>Search by last name</label>
						  	<input type="text" name="lastname" class="input-medium search-query" id="uqinput" style="margin-top:5px" value="">
						  	<button type="submit" class="btn" style="margin-top:5px">Search</button>
						</form>
					</div>
				</div>
			<%}%>
		</div>
	</body>
	
</html>