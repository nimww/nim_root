


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tNIM3_Encounter_form.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear_fade.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<script language="javascript">
function removeCommas(sVal)
{
	return sVal.replace(',','');
//	return sVal;
}


function RecAmountRecalc()
{
	myTotal = 0;
	{
		if (document.getElementById('ServiceStatusID_1').value==1)
		{
			myTotal += +removeCommas(document.getElementById("ReceivedCheck1Amount_1").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck2Amount_1").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck3Amount_1").value);
			RecAmount1 = 0;
			RecAmount1 += +removeCommas(document.getElementById("ReceivedCheck1Amount_1").value);
			RecAmount1 += +removeCommas(document.getElementById("ReceivedCheck2Amount_1").value);
			RecAmount1 += +removeCommas(document.getElementById("ReceivedCheck3Amount_1").value);
			document.getElementById("ReceivedAmount_1").value = RecAmount1;
		}

		if (document.getElementById('ServiceStatusID_2').value==1)
		{
			myTotal += +removeCommas(document.getElementById("ReceivedCheck1Amount_2").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck2Amount_2").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck3Amount_2").value);
			RecAmount2 = 0;
			RecAmount2 += +removeCommas(document.getElementById("ReceivedCheck1Amount_2").value);
			RecAmount2 += +removeCommas(document.getElementById("ReceivedCheck2Amount_2").value);
			RecAmount2 += +removeCommas(document.getElementById("ReceivedCheck3Amount_2").value);
			document.getElementById("ReceivedAmount_2").value = RecAmount2;
		}

		if (document.getElementById('ServiceStatusID_3').value==1)
		{
			myTotal += +removeCommas(document.getElementById("ReceivedCheck1Amount_3").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck2Amount_3").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck3Amount_3").value);
			RecAmount3 = 0;
			RecAmount3 += +removeCommas(document.getElementById("ReceivedCheck1Amount_3").value);
			RecAmount3 += +removeCommas(document.getElementById("ReceivedCheck2Amount_3").value);
			RecAmount3 += +removeCommas(document.getElementById("ReceivedCheck3Amount_3").value);
			document.getElementById("ReceivedAmount_3").value = RecAmount3;
		}


/*
		if (document.getElementById('ServiceStatusID_3').vAlign==1)
		{
			myTotal += +removeCommas(document.getElementById("ReceivedCheck1Amount_3").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck2Amount_3").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck3Amount_3").value);
			RecAmount3 = 0;
			RecAmount3 += +removeCommas(document.getElementById("ReceivedCheck1Amount_3").value);
			RecAmount3 += +removeCommas(document.getElementById("ReceivedCheck2Amount_3").value);
			RecAmount3 += +removeCommas(document.getElementById("ReceivedCheck3Amount_3").value);
			document.getElementById("ReceivedAmount_3").value = RecAmount3;
		}
*/
		if (document.getElementById('ServiceStatusID_4').value==1)
		{
			myTotal += +removeCommas(document.getElementById("ReceivedCheck1Amount_4").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck2Amount_4").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck3Amount_4").value);
			RecAmount4 = 0;
			RecAmount4 += +removeCommas(document.getElementById("ReceivedCheck1Amount_4").value);
			RecAmount4 += +removeCommas(document.getElementById("ReceivedCheck2Amount_4").value);
			RecAmount4 += +removeCommas(document.getElementById("ReceivedCheck3Amount_4").value);
			document.getElementById("ReceivedAmount_4").value = RecAmount4;
		}

		if (document.getElementById('ServiceStatusID_5').value==1)
		{
			myTotal += +removeCommas(document.getElementById("ReceivedCheck1Amount_5").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck2Amount_5").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck3Amount_5").value);
			RecAmount5 = 0;
			RecAmount5 += +removeCommas(document.getElementById("ReceivedCheck1Amount_5").value);
			RecAmount5 += +removeCommas(document.getElementById("ReceivedCheck2Amount_5").value);
			RecAmount5 += +removeCommas(document.getElementById("ReceivedCheck3Amount_5").value);
			document.getElementById("ReceivedAmount_5").value = RecAmount5;
		}

		if (document.getElementById('ServiceStatusID_6').value==1)
		{
			myTotal += +removeCommas(document.getElementById("ReceivedCheck1Amount_6").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck2Amount_6").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck3Amount_6").value);
			RecAmount6 = 0;
			RecAmount6 += +removeCommas(document.getElementById("ReceivedCheck1Amount_6").value);
			RecAmount6 += +removeCommas(document.getElementById("ReceivedCheck2Amount_6").value);
			RecAmount6 += +removeCommas(document.getElementById("ReceivedCheck3Amount_6").value);
			document.getElementById("ReceivedAmount_6").value = RecAmount6;
		}

		if (document.getElementById('ServiceStatusID_7').value==1)
		{
			myTotal += +removeCommas(document.getElementById("ReceivedCheck1Amount_7").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck2Amount_7").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck3Amount_7").value);
			RecAmount7 = 0;
			RecAmount7 += +removeCommas(document.getElementById("ReceivedCheck1Amount_7").value);
			RecAmount7 += +removeCommas(document.getElementById("ReceivedCheck2Amount_7").value);
			RecAmount7 += +removeCommas(document.getElementById("ReceivedCheck3Amount_7").value);
			document.getElementById("ReceivedAmount_7").value = RecAmount7;
		}

		if (document.getElementById('ServiceStatusID_8').value==1)
		{
			myTotal += +removeCommas(document.getElementById("ReceivedCheck1Amount_8").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck2Amount_8").value);
			myTotal += +removeCommas(document.getElementById("ReceivedCheck3Amount_8").value);
			RecAmount8 = 0;
			RecAmount8 += +removeCommas(document.getElementById("ReceivedCheck1Amount_8").value);
			RecAmount8 += +removeCommas(document.getElementById("ReceivedCheck2Amount_8").value);
			RecAmount8 += +removeCommas(document.getElementById("ReceivedCheck3Amount_8").value);
			document.getElementById("ReceivedAmount_8").value = RecAmount8;
		}
	}
	document.getElementById("dTotalRec").value = myTotal;
	
	myTotalExpected = 0;
	myTotalExpectedOut = 0;

	if (document.getElementById('ServiceStatusID_1').value==1)
	{
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmount_1").value)); 
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmountAdjustment_1").value)); 
		myTotalExpectedOut += +(removeCommas(document.getElementById("PaidOutAmount_1").value) );
	}
	if (document.getElementById('ServiceStatusID_2').value==1)
	{
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmount_2").value)); 
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmountAdjustment_2").value)); 
		myTotalExpectedOut += +(removeCommas(document.getElementById("PaidOutAmount_2").value) );
	}
	if (document.getElementById('ServiceStatusID_3').value==1)
	{
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmount_3").value)); 
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmountAdjustment_3").value)); 
		myTotalExpectedOut += +(removeCommas(document.getElementById("PaidOutAmount_3").value) );
	}
	if (document.getElementById('ServiceStatusID_4').value==1)
	{
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmount_4").value)); 
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmountAdjustment_4").value)); 
		myTotalExpectedOut += +(removeCommas(document.getElementById("PaidOutAmount_4").value) );
	}
	if (document.getElementById('ServiceStatusID_5').value==1)
	{
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmount_5").value)); 
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmountAdjustment_5").value)); 
		myTotalExpectedOut += +(removeCommas(document.getElementById("PaidOutAmount_5").value) );
	}
	if (document.getElementById('ServiceStatusID_6').value==1)
	{
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmount_6").value)); 
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmountAdjustment_6").value)); 
		myTotalExpectedOut += +(removeCommas(document.getElementById("PaidOutAmount_6").value) );
	}
	if (document.getElementById('ServiceStatusID_7').value==1)
	{
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmount_7").value)); 
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmountAdjustment_7").value)); 
		myTotalExpectedOut += +(removeCommas(document.getElementById("PaidOutAmount_7").value) );
	}
	if (document.getElementById('ServiceStatusID_8').value==1)
	{
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmount_8").value)); 
		myTotalExpected += +(removeCommas(document.getElementById("AllowAmountAdjustment_8").value)); 
		myTotalExpectedOut += +(removeCommas(document.getElementById("PaidOutAmount_8").value) );
	}
	document.getElementById("dTotalExp").value = myTotalExpected;

	document.getElementById("dTotalExpOut").value = myTotalExpectedOut;

	
}

</script>
<script language="javascript">
show_pws_Loading();
</script>

    <table id="bigfade" cellpadding=0 cellspacing=0 border=0 width=100%>
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tNIM3_Encounter")%>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
//        accessValid = true;
        if (iEncounterID.intValue() == iEDITID.intValue())
        {
        	accessValid = true;
        }
    }
  //page security
  if (accessValid&&isScheduler)
  {
	java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
	java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);	java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_Encounter_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID

    bltNIM3_Encounter        NIM3_Encounter        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("nim3expbill") )
    {
        NIM3_Encounter        =    new    bltNIM3_Encounter(iEncounterID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_Encounter        =    new    bltNIM3_Encounter(UserSecurityGroupID, true);
    }

//fields
        %>
        <form action="tNIM3_EncounterService_form_sub.jsp" name="tNIM3_Encounter_form1" method="POST" >
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
    %>
        <input type="hidden" name="EDITID" value = "<%=iEDITID%>" >  

          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr>
           <td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=50%>
                        <tr>
                          <td colspan="3" valign=middle><img src="images/marker_encounter.gif" width="114" height="31" align="middle" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="inputButton_md_Create" onClick = "this.disabled=false;$('table').fadeOut('slow');show_pws_Calc();submit();" type=Submit value="Save" name=Submit></td>
                        </tr>



<tr>
  <td colspan="3">

<table border="1" cellspacing="0" cellpadding="2" class="tableCanvasLightGrey">
  <tr class="tdHeaderBright">
    <td colspan="2">TimeTrack Dates (workflow times)</td>
    <td>User</td>
    <td>RCode</td>
    <td>to Ref Dr</td>
    <td>to Case Admin</td>
    <td>to  Case Admin 2</td>
    <td>to  Case Admin 3</td>
    <td>to  Case Admin 4</td>
    <td>to Adjuster</td>
    <td>to NCM</td>
    <td>to Provider</td>
    </tr>



            <%
            if ( (NIM3_Encounter.isRequired("TimeTrack_ReqRec",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("TimeTrack_ReqRec")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("TimeTrack_ReqRec",expiredDays))&&(NIM3_Encounter.isExpiredCheck("TimeTrack_ReqRec",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_Encounter.isWrite("TimeTrack_ReqRec",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Received</b></p></td><td valign=top><p><input name="TimeTrack_ReqRec"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getTimeTrack_ReqRec())%>" /></jsp:include>' size="17" maxlength=20 <% if (!isScheduler3){%>readonly="readonly"<%}%> >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRec&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqRec")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqRec_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRec_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqRec_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
            <%
            if ( (NIM3_Encounter.isRequired("SentTo_ReqRec_RefDr",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_ReqRec_RefDr")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_ReqRec_RefDr",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_RefDr",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

<td valign=top><p><select   name="TimeTrack_ReqRec_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Encounter.getTimeTrack_ReqRec_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRec_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqRec_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>


                        <td valign=top>
                                  <%
            if ((NIM3_Encounter.isWrite("SentTo_ReqRec_RefDr",UserSecurityGroupID)))
            {
                        %>
<p><input maxlength=20  type=text size="17" name="SentTo_ReqRec_RefDr" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_RefDr())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_RefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_RefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if ((NIM3_Encounter.isRead("SentTo_ReqRec_RefDr",UserSecurityGroupID)))
            {
                        %>
                       <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_RefDr())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_RefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_RefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>
</td>
                        <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_ReqRec_Adm",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_ReqRec_Adm")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_ReqRec_Adm",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_Adm",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((NIM3_Encounter.isWrite("SentTo_ReqRec_Adm",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <input maxlength=20  type=text size="17" name="SentTo_ReqRec_Adm" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_Adm())%>" /></jsp:include>' >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else if ((NIM3_Encounter.isRead("SentTo_ReqRec_Adm",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                              <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_Adm())%>" />                          
                            </jsp:include>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %></td>
                        <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_ReqRec_Adm2",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_ReqRec_Adm2")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_ReqRec_Adm2",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_Adm2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((NIM3_Encounter.isWrite("SentTo_ReqRec_Adm2",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <input maxlength=20  type=text size="17" name="SentTo_ReqRec_Adm2" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_Adm2())%>" /></jsp:include>' >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm2&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm2")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else if ((NIM3_Encounter.isRead("SentTo_ReqRec_Adm2",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                              <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_Adm2())%>" />                          
                            </jsp:include>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm2&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm2")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %></td>
                        <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_ReqRec_Adm3",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_ReqRec_Adm3")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_ReqRec_Adm3",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_Adm3",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((NIM3_Encounter.isWrite("SentTo_ReqRec_Adm3",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <input maxlength=20  type=text size="17" name="SentTo_ReqRec_Adm3" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_Adm3())%>" /></jsp:include>' >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm3&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm3")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else if ((NIM3_Encounter.isRead("SentTo_ReqRec_Adm3",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                              <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_Adm3())%>" />                          
                            </jsp:include>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm3&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm3")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %></td>
                        <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_ReqRec_Adm4",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_ReqRec_Adm4")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_ReqRec_Adm4",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_Adm4",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((NIM3_Encounter.isWrite("SentTo_ReqRec_Adm4",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <input maxlength=20  type=text size="17" name="SentTo_ReqRec_Adm4" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_Adm4())%>" /></jsp:include>' >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm4&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm4")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else if ((NIM3_Encounter.isRead("SentTo_ReqRec_Adm4",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                              <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_Adm4())%>" />                          
                            </jsp:include>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm4&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm4")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %></td>
                        <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_ReqRec_Adj",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_ReqRec_Adj")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_ReqRec_Adj",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_Adj",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((NIM3_Encounter.isWrite("SentTo_ReqRec_Adj",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <input maxlength=20  type=text size="17" name="SentTo_ReqRec_Adj" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_Adj())%>" /></jsp:include>' >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_Adj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else if ((NIM3_Encounter.isRead("SentTo_ReqRec_Adj",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                              <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_Adj())%>" />                          
                            </jsp:include>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_Adj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %></td>


                          <td valign=top>
            <%
            if ( (NIM3_Encounter.isRequired("SentTo_ReqRec_NCM",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_ReqRec_NCM")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_ReqRec_NCM",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_NCM",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_Encounter.isWrite("SentTo_ReqRec_NCM",UserSecurityGroupID)))
            {
                        %>
<p><input maxlength=20  type=text size="17" name="SentTo_ReqRec_NCM" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_NCM())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_NCM&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_NCM")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if ((NIM3_Encounter.isRead("SentTo_ReqRec_NCM",UserSecurityGroupID)))
            {
                        %>
                        <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_ReqRec_NCM())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_NCM&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_NCM")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

</td>
                          <td valign=top>&nbsp;</td>
                        </tr>
                        <%
            }
            else if ((NIM3_Encounter.isRead("TimeTrack_ReqRec",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (NIM3_Encounter.isRequired("TimeTrack_ReqCreated",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("TimeTrack_ReqCreated")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("TimeTrack_ReqCreated",expiredDays))&&(NIM3_Encounter.isExpiredCheck("TimeTrack_ReqCreated",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_Encounter.isWrite("TimeTrack_ReqCreated",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Created</b></p></td><td valign=top><p><input name="TimeTrack_ReqCreated"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getTimeTrack_ReqCreated())%>" /></jsp:include>' size="17" maxlength=20 <% if (!isScheduler3){%>readonly="readonly"<%}%> >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqCreated&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqCreated")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqCreated_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqCreated_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqCreated_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                        <%
            }
            else if ((NIM3_Encounter.isRead("TimeTrack_ReqCreated",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



<td valign=top><p><select   name="TimeTrack_ReqCreated_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Encounter.getTimeTrack_ReqCreated_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqCreated_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqCreated_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>                          <td valign=top>&nbsp;</td>
<td valign=top>&nbsp;</td>
<td valign=top>&nbsp;</td>
<td valign=top>&nbsp;</td>
<td valign=top>&nbsp;</td>
<td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>

                        </tr>


            <%
            if ( (NIM3_Encounter.isRequired("TimeTrack_ReqProc",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("TimeTrack_ReqProc")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("TimeTrack_ReqProc",expiredDays))&&(NIM3_Encounter.isExpiredCheck("TimeTrack_ReqProc",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_Encounter.isWrite("TimeTrack_ReqProc",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Processed</b></p></td><td valign=top><p><input name="TimeTrack_ReqProc"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getTimeTrack_ReqProc())%>" /></jsp:include>' size="17" maxlength=20 <% if (!isScheduler3){%>readonly="readonly"<%}%> >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqProc&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqProc")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqProc_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqProc_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqProc_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
            <%
            if ( (NIM3_Encounter.isRequired("SentTo_DataProc_RefDr",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_DataProc_RefDr")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_DataProc_RefDr",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_DataProc_RefDr",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

<td valign=top><p><select   name="TimeTrack_ReqProc_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Encounter.getTimeTrack_ReqProc_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqProc_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqProc_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>

                          <td valign=top>

            <%
            if (false&&(NIM3_Encounter.isWrite("SentTo_DataProc_RefDr",UserSecurityGroupID)))
            {
                        %>
                        <p><input maxlength=20  type=text size="17" name="SentTo_DataProc_RefDr" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_DataProc_RefDr())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_DataProc_RefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_DataProc_RefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if (false&&(NIM3_Encounter.isRead("SentTo_DataProc_RefDr",UserSecurityGroupID)))
            {
                        %>
                       <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_DataProc_RefDr())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_DataProc_RefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_DataProc_RefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_DataProc_Adj",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_DataProc_Adj")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_DataProc_Adj",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_DataProc_Adj",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (false&&(NIM3_Encounter.isWrite("SentTo_DataProc_Adj",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_DataProc_Adj" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_DataProc_Adj())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_DataProc_Adj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_DataProc_Adj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (false&&(NIM3_Encounter.isRead("SentTo_DataProc_Adj",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_DataProc_Adj())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_DataProc_Adj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_DataProc_Adj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
  &nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><strong>RX Review</strong></td>
                          <td valign=top><p>
                            <input name="TimeTrack_ReqRxReview"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getTimeTrack_ReqRxReview())%>" /></jsp:include>' size="17" maxlength=20 <% if (!isScheduler3){%>readonly="readonly"<%}%> >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRxReview&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqRxReview")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p></td>
                          <td valign=top><%=new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqRxReview_UserID()).getLogonUserName()%>&nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRxReview_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqRxReview_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%></td>
                          <td valign=top><p>
                            <select   name="TimeTrack_ReqRxReview_RCodeID" >
                              <jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=NIM3_Encounter.getTimeTrack_ReqRxReview_RCodeID()%>" />                        
                              </jsp:include>
                            </select>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRxReview_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqRxReview_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p></td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                        <%
            }
            else if ((NIM3_Encounter.isRead("TimeTrack_ReqProc",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



<tr>
<td><strong>Approved</strong></td>


<td valign=top><p><input name="TimeTrack_ReqApproved"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getTimeTrack_ReqApproved())%>" /></jsp:include>' size="17" maxlength=20 <% if (!isScheduler3){%>readonly="readonly"<%}%> >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqApproved&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqApproved")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>





<td valign=top><%=new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqApproved_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqApproved_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqApproved_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>


<td valign=top><p><select   name="TimeTrack_ReqApproved_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Encounter.getTimeTrack_ReqApproved_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqApproved_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqApproved_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>

<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>

            <%
            if ( (NIM3_Encounter.isRequired("TimeTrack_ReqSched",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("TimeTrack_ReqSched")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("TimeTrack_ReqSched",expiredDays))&&(NIM3_Encounter.isExpiredCheck("TimeTrack_ReqSched",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_Encounter.isWrite("TimeTrack_ReqSched",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Scheduled</b></p></td><td valign=top><p><input name="TimeTrack_ReqSched"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getTimeTrack_ReqSched())%>" /></jsp:include>' size="17" maxlength=20 <% if (!isScheduler3){%>readonly="readonly"<%}%> >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqSched&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqSched")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqSched_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqSched_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqSched_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>

            <%
            if ( (NIM3_Encounter.isRequired("SentTo_SP_RefDr",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_SP_RefDr")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_SP_RefDr",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_SP_RefDr",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>


<td valign=top><p><select   name="TimeTrack_ReqSched_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Encounter.getTimeTrack_ReqSched_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqSched_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqSched_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>

                          <td valign=top>

            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_SP_RefDr",UserSecurityGroupID)))
            {
                        %>
               <input maxlength=20  type=text size="17" name="SentTo_SP_RefDr" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_RefDr())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_RefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_RefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                        <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentTo_SP_RefDr",UserSecurityGroupID)))
            {
                        %>
                        <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_RefDr())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_RefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_RefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

</td>
                          <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_SP_Adm",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_SP_Adm")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_SP_Adm",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_SP_Adm",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_SP_Adm",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_SP_Adm" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_Adm())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_Adm")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentTo_SP_Adm",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_Adm())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_Adm")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_SP_Adm2",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_SP_Adm2")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_SP_Adm2",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_SP_Adm2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_SP_Adm2",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_SP_Adm2" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_Adm2())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm2&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_Adm2")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentTo_SP_Adm2",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_Adm2())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm2&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_Adm2")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_SP_Adm3",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_SP_Adm3")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_SP_Adm3",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_SP_Adm3",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_SP_Adm3",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_SP_Adm3" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_Adm3())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm3&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_Adm3")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentTo_SP_Adm3",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_Adm3())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm3&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_Adm3")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_SP_Adm4",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_SP_Adm4")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_SP_Adm4",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_SP_Adm4",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_SP_Adm4",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_SP_Adm4" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_Adm4())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm4&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_Adm4")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentTo_SP_Adm4",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_Adm4())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm4&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_Adm4")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_SP_Adj",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_SP_Adj")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_SP_Adj",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_SP_Adj",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_SP_Adj",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_SP_Adj" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_Adj())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_Adj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentTo_SP_Adj",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_Adj())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_Adj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top>
            <%
            if ( (NIM3_Encounter.isRequired("SentTo_SP_NCM",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_SP_NCM")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_SP_NCM",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_SP_NCM",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_SP_NCM",UserSecurityGroupID)))
            {
                        %>
                    <p><input maxlength=20  type=text size="17" name="SentTo_SP_NCM" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_NCM())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_NCM&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_NCM")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentTo_SP_NCM",UserSecurityGroupID)))
            {
                        %>
                       <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_NCM())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_NCM&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_NCM")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

</td>
                          <td valign=top>
            <%
            if ( (NIM3_Encounter.isRequired("SentTo_SP_IC",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_SP_IC")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_SP_IC",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_SP_IC",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_SP_IC",UserSecurityGroupID)))
            {
                        %>
                       <p><input maxlength=20  type=text size="17" name="SentTo_SP_IC" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_IC())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_IC&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_IC")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentTo_SP_IC",UserSecurityGroupID)))
            {
                        %>
                        <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_SP_IC())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_IC&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_SP_IC")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

</td>
                        </tr>

                        <tr>
                          <td valign=top ><p class=<%=theClass%> ><strong>Initial Appointment</strong></p></td>
                          <td valign=top><p>
                            <input name="TimeTrack_ReqInitialAppointment"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getTimeTrack_ReqInitialAppointment())%>" /></jsp:include>' size="17" maxlength=20            <% if (!isScheduler3){%>readonly="readonly"<%}%> >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqInitialAppointment&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqInitialAppointment")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p></td>
                          <td valign=top><%=new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqInitialAppointment_UserID()).getLogonUserName()%>&nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqInitialAppointment_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqInitialAppointment_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%></td>
                          <td valign=top><p>
                            <select   name="TimeTrack_ReqInitialAppointment_RCodeID" >                              <jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=NIM3_Encounter.getTimeTrack_ReqInitialAppointment_RCodeID()%>" />                          
                              </jsp:include>
                            </select>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqInitialAppointment_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqInitialAppointment_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p></td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                        </tr>
                        <%
            }
            else if ((NIM3_Encounter.isRead("TimeTrack_ReqInitialAppointment",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>







            <%
            if ( (NIM3_Encounter.isRequired("TimeTrack_ReqDelivered",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("TimeTrack_ReqDelivered")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("TimeTrack_ReqDelivered",expiredDays))&&(NIM3_Encounter.isExpiredCheck("TimeTrack_ReqDelivered",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_Encounter.isWrite("TimeTrack_ReqDelivered",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Report Delivered</b></p></td><td valign=top><p><input name="TimeTrack_ReqDelivered"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getTimeTrack_ReqDelivered())%>" /></jsp:include>' size="17" maxlength=20 <% if (!isScheduler3){%>readonly="readonly"<%}%> >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqDelivered&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqDelivered")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqDelivered_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqDelivered_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqDelivered_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>

            <%
            if ( (NIM3_Encounter.isRequired("SentToRefDr",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentToRefDr")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentToRefDr",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentToRefDr",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>


<td valign=top><p><select   name="TimeTrack_ReqDelivered_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Encounter.getTimeTrack_ReqDelivered_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqDelivered_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqDelivered_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                          <td valign=top>

            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentToRefDr",UserSecurityGroupID)))
            {
                        %><p><input maxlength=20  type=text size="17" name="SentToRefDr" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentToRefDr())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToRefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentToRefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentToRefDr",UserSecurityGroupID)))
            {
                        %>
                        <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentToRefDr())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToRefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentToRefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

</td>
                          <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentToAdm",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentToAdm")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentToAdm",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentToAdm",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentToAdm",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentToAdm" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentToAdm())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToAdm&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentToAdm")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentToAdm",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentToAdm())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToAdm&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentToAdm")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_RP_Adm2",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_RP_Adm2")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_RP_Adm2",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_RP_Adm2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_RP_Adm2",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_RP_Adm2" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_RP_Adm2())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_Adm2&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_RP_Adm2")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentTo_RP_Adm2",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_RP_Adm2())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_Adm2&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_RP_Adm2")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_RP_Adm3",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_RP_Adm3")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_RP_Adm3",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_RP_Adm3",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_RP_Adm3",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_RP_Adm3" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_RP_Adm3())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_Adm3&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_RP_Adm3")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentTo_RP_Adm3",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_RP_Adm3())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_Adm3&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_RP_Adm3")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentTo_RP_Adm4",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_RP_Adm4")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_RP_Adm4",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_RP_Adm4",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_RP_Adm4",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_RP_Adm4" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_RP_Adm4())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_Adm4&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_RP_Adm4")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentTo_RP_Adm4",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_RP_Adm4())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_Adm4&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_RP_Adm4")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (NIM3_Encounter.isRequired("SentToAdj",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentToAdj")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentToAdj",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentToAdj",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentToAdj",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentToAdj" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentToAdj())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToAdj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentToAdj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentToAdj",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentToAdj())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToAdj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentToAdj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top>

            <%
            if ( (NIM3_Encounter.isRequired("SentTo_RP_NCM",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_RP_NCM")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_RP_NCM",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_RP_NCM",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_RP_NCM",UserSecurityGroupID)))
            {
                        %>
                        <p><input maxlength=20  type=text size="17" name="SentTo_RP_NCM" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_RP_NCM())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_NCM&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_RP_NCM")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentTo_RP_NCM",UserSecurityGroupID)))
            {
                        %>
                        <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_RP_NCM())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_NCM&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_RP_NCM")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

</td>
                          <td valign=top>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><strong>Report Review</strong></td>
                          <td valign=top><p>
                            <input name="TimeTrack_ReqRpReview"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getTimeTrack_ReqRpReview())%>" /></jsp:include>' size="17" maxlength=20 <% if (!isScheduler3){%>readonly="readonly"<%}%> >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRpReview&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqRpReview")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p></td>
                          <td valign=top><%=new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqRpReview_UserID()).getLogonUserName()%>&nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRpReview_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqRpReview_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%></td>
                          <td valign=top><p>
                            <select   name="TimeTrack_ReqRpReview_RCodeID" >
                              <jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=NIM3_Encounter.getTimeTrack_ReqRpReview_RCodeID()%>" />
                              </jsp:include>
                            </select>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRpReview_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqRpReview_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p></td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                        <%
            }
            else if ((NIM3_Encounter.isRead("TimeTrack_ReqDelivered",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (NIM3_Encounter.isRequired("TimeTrack_ReqPaidIn",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("TimeTrack_ReqPaidIn")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("TimeTrack_ReqPaidIn",expiredDays))&&(NIM3_Encounter.isExpiredCheck("TimeTrack_ReqPaidIn",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_Encounter.isWrite("TimeTrack_ReqPaidIn",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Payment - In</b></p></td><td valign=top><p><input name="TimeTrack_ReqPaidIn"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getTimeTrack_ReqPaidIn())%>" /></jsp:include>' size="17" maxlength=20 <% if (!isScheduler3){%>readonly="readonly"<%}%> >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqPaidIn&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqPaidIn")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqPaidIn_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqPaidIn_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqPaidIn_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                        <%
            }
            else if ((NIM3_Encounter.isRead("TimeTrack_ReqPaidIn",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

<td valign=top><p><select   name="TimeTrack_ReqPaidIn_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Encounter.getTimeTrack_ReqPaidIn_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqPaidIn_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqPaidIn_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>



                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                        </tr>




            <%
            if ( (NIM3_Encounter.isRequired("TimeTrack_ReqPaidOut",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("TimeTrack_ReqPaidOut")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("TimeTrack_ReqPaidOut",expiredDays))&&(NIM3_Encounter.isExpiredCheck("TimeTrack_ReqPaidOut",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_Encounter.isWrite("TimeTrack_ReqPaidOut",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Payment - Out</b></p></td><td valign=top><p><input name="TimeTrack_ReqPaidOut"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getTimeTrack_ReqPaidOut())%>" /></jsp:include>' size="17" maxlength=20 <% if (!isScheduler3){%>readonly="readonly"<%}%> >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqPaidOut&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqPaidOut")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(NIM3_Encounter.getTimeTrack_ReqPaidOut_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqPaidOut_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqPaidOut_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                        <%
            }
            else if ((NIM3_Encounter.isRead("TimeTrack_ReqPaidOut",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



<td valign=top><p><select   name="TimeTrack_ReqPaidOut_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Encounter.getTimeTrack_ReqPaidOut_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqPaidOut_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("TimeTrack_ReqPaidOut_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                        </tr>



</table>


</td></tr>










            <%
            if ( (NIM3_Encounter.isRequired("SentTo_ReqRec_RefDr_CRID",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_ReqRec_RefDr_CRID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_ReqRec_RefDr_CRID",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_RefDr_CRID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_Encounter.isWrite("SentTo_ReqRec_RefDr_CRID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>SentTo_ReqRec_RefDr_CRID&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="SentTo_ReqRec_RefDr_CRID" value="<%=NIM3_Encounter.getSentTo_ReqRec_RefDr_CRID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_RefDr_CRID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_RefDr_CRID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_Encounter.isRead("SentTo_ReqRec_RefDr_CRID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>SentTo_ReqRec_RefDr_CRID&nbsp;</b></p></td><td valign=top><p><%=NIM3_Encounter.getSentTo_ReqRec_RefDr_CRID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_RefDr_CRID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_RefDr_CRID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (NIM3_Encounter.isRequired("SentTo_ReqRec_Adj_CRID",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_ReqRec_Adj_CRID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_ReqRec_Adj_CRID",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_Adj_CRID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_Encounter.isWrite("SentTo_ReqRec_Adj_CRID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>SentTo_ReqRec_Adj_CRID&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="SentTo_ReqRec_Adj_CRID" value="<%=NIM3_Encounter.getSentTo_ReqRec_Adj_CRID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adj_CRID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_Adj_CRID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_Encounter.isRead("SentTo_ReqRec_Adj_CRID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>SentTo_ReqRec_Adj_CRID&nbsp;</b></p></td><td valign=top><p><%=NIM3_Encounter.getSentTo_ReqRec_Adj_CRID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adj_CRID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_ReqRec_Adj_CRID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>






            <%
            if ( (NIM3_Encounter.isRequired("SentTo_DataProc_RefDr_CRID",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_DataProc_RefDr_CRID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_DataProc_RefDr_CRID",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_DataProc_RefDr_CRID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_Encounter.isWrite("SentTo_DataProc_RefDr_CRID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>SentTo_DataProc_RefDr_CRID&nbsp;</b></p></td><td colspan="2" valign=top><p><input maxlength="20" type=text size="80" name="SentTo_DataProc_RefDr_CRID" value="<%=NIM3_Encounter.getSentTo_DataProc_RefDr_CRID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_DataProc_RefDr_CRID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_DataProc_RefDr_CRID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_Encounter.isRead("SentTo_DataProc_RefDr_CRID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>SentTo_DataProc_RefDr_CRID&nbsp;</b></p></td><td colspan="2" valign=top><p><%=NIM3_Encounter.getSentTo_DataProc_RefDr_CRID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_DataProc_RefDr_CRID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_DataProc_RefDr_CRID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>





            <%
            if ( (NIM3_Encounter.isRequired("SentTo_DataProc_Adj_CRID",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_DataProc_Adj_CRID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_DataProc_Adj_CRID",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_DataProc_Adj_CRID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_Encounter.isWrite("SentTo_DataProc_Adj_CRID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>SentTo_DataProc_Adj_CRID&nbsp;</b></p></td><td colspan="2" valign=top><p><input maxlength="20" type=text size="80" name="SentTo_DataProc_Adj_CRID" value="<%=NIM3_Encounter.getSentTo_DataProc_Adj_CRID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_DataProc_Adj_CRID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_DataProc_Adj_CRID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_Encounter.isRead("SentTo_DataProc_Adj_CRID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>SentTo_DataProc_Adj_CRID&nbsp;</b></p></td><td colspan="2" valign=top><p><%=NIM3_Encounter.getSentTo_DataProc_Adj_CRID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_DataProc_Adj_CRID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_DataProc_Adj_CRID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>






            <%
            if ( (NIM3_Encounter.isRequired("SentToIC",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentToIC")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentToIC",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentToIC",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (false&&isScheduler&&(NIM3_Encounter.isWrite("SentToIC",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                          <td valign=top nowrap="nowrap"><p class=<%=theClass%> ><b>Date Report Sent To Provider<br /></b><b>(mm/dd/yyyy hh:mm am/pm):</b></p></td><td colspan="2" valign=top><p><input name="SentToIC"  type=text disabled value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentToIC())%>" /></jsp:include>' size="30" maxlength=20 >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToIC&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentToIC")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&isScheduler&&(NIM3_Encounter.isRead("SentToIC",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top nowrap="nowrap"><p class=<%=theClass%> ><b>Sent To Provider&nbsp;<br />
                        (mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentToIC())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToIC&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentToIC")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



<tr class="tdHeaderAlt">
  <td colspan="3">Billing Dates</td></tr>



            <%
            if ( (NIM3_Encounter.isRequired("SentTo_Bill_Pay",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_Bill_Pay")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_Bill_Pay",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_Bill_Pay",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_Bill_Pay",UserSecurityGroupID)))
            {
                        %><tr><td colspan="3"><hr></td></tr>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Bill Sent To Payer&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><input maxlength=20  type=text size="30" name="SentTo_Bill_Pay" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_Bill_Pay())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_Bill_Pay&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_Bill_Pay")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("SentTo_Bill_Pay",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Bill Sent To Payer&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_Bill_Pay())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_Bill_Pay&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_Bill_Pay")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_Encounter.isRequired("Rec_Bill_Pay",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("Rec_Bill_Pay")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("Rec_Bill_Pay",expiredDays))&&(NIM3_Encounter.isExpiredCheck("Rec_Bill_Pay",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (false&&isScheduler&&(NIM3_Encounter.isWrite("Rec_Bill_Pay",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Payment Rec From Payer&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><input maxlength=20  type=text size="30" name="Rec_Bill_Pay" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getRec_Bill_Pay())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pay&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("Rec_Bill_Pay")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&isScheduler&&(NIM3_Encounter.isRead("Rec_Bill_Pay",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Payment Rec By Payer&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getRec_Bill_Pay())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pay&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("Rec_Bill_Pay")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (NIM3_Encounter.isRequired("Rec_Bill_Pro",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("Rec_Bill_Pro")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("Rec_Bill_Pro",expiredDays))&&(NIM3_Encounter.isExpiredCheck("Rec_Bill_Pro",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("Rec_Bill_Pro",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Bill Rec From Provider&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><input maxlength=20  type=text size="30" name="Rec_Bill_Pro" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getRec_Bill_Pro())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pro&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("Rec_Bill_Pro")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (isScheduler&&(NIM3_Encounter.isRead("Rec_Bill_Pro",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Bill Rec By Provider&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getRec_Bill_Pro())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pro&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("Rec_Bill_Pro")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Encounter.isRequired("AmountBilledByProvider",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("AmountBilledByProvider")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("AmountBilledByProvider",expiredDays))&&(NIM3_Encounter.isExpiredCheck("AmountBilledByProvider",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Encounter.isWrite("AmountBilledByProvider",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Amount Billed By Provider:&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="20" name="AmountBilledByProvider" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Encounter.getAmountBilledByProvider())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AmountBilledByProvider&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("AmountBilledByProvider")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Encounter.isRead("AmountBilledByProvider",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>AmountBilledByProvider&nbsp;</b></p></td><td valign=top><p><%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Encounter.getAmountBilledByProvider())%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AmountBilledByProvider&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("AmountBilledByProvider")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>





            <%
            if ( (NIM3_Encounter.isRequired("SentTo_Bill_Pro",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("SentTo_Bill_Pro")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("SentTo_Bill_Pro",expiredDays))&&(NIM3_Encounter.isExpiredCheck("SentTo_Bill_Pro",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(NIM3_Encounter.isWrite("SentTo_Bill_Pro",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top bgcolor="#EDF7E1"><p class=<%=theClass%> ><b>Payment Sent To Provider&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top bgcolor="#EDF7E1"> </td>
                          <td valign=top>&nbsp;</td>
                </tr>
                        <tr>
                          <td valign=top bgcolor="#EDF7E1">&nbsp;</td>
                          <td valign=top bgcolor="#EDF7E1"><table width="25%" border="1" cellspacing="0" cellpadding="2">
  <tr class=tdHeaderAlt>
    <td colspan="3" align="center">Payment Sent <u>To</u> Provider</td>
    </tr>
  <tr class=tdHeaderAlt>
    <td>Check #&nbsp;</td>
    <td>Amount</td>
    <td>Date</td>
  </tr>
  <tr>
    <td><input maxlength="20" type=text size="15" name="PaidToProviderCheck1Number" id="PaidToProviderCheck1Number" value="<%=NIM3_Encounter.getPaidToProviderCheck1Number()%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="10" name="PaidToProviderCheck1Amount" id="PaidToProviderCheck1Amount" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Encounter.getPaidToProviderCheck1Amount())%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="15" name="PaidToProviderCheck1Date" id="PaidToProviderCheck1Date" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getPaidToProviderCheck1Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
  <tr>
    <td><input maxlength="20" type=text size="15" name="PaidToProviderCheck2Number" id="PaidToProviderCheck2Number" value="<%=NIM3_Encounter.getPaidToProviderCheck2Number()%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="10" name="PaidToProviderCheck2Amount" id="PaidToProviderCheck2Amount" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Encounter.getPaidToProviderCheck2Amount())%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="15" name="PaidToProviderCheck2Date" id="PaidToProviderCheck2Date" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getPaidToProviderCheck2Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
  <tr>
    <td><input maxlength="20" type=text size="15" name="PaidToProviderCheck3Number" id="PaidToProviderCheck3Number" value="<%=NIM3_Encounter.getPaidToProviderCheck3Number()%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="10" name="PaidToProviderCheck3Amount" id="PaidToProviderCheck3Amount" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Encounter.getPaidToProviderCheck3Amount())%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="15" name="PaidToProviderCheck3Date" id="PaidToProviderCheck3Date" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getPaidToProviderCheck3Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
</table>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                </tr>
                        <%
            }
            else if (false&&isScheduler&&(NIM3_Encounter.isRead("SentTo_Bill_Pro",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Payment Sent To Provider&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getSentTo_Bill_Pro())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_Bill_Pro&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("SentTo_Bill_Pro")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>








            <%
            if ( (NIM3_Encounter.isRequired("DateOfService",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("DateOfService")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("DateOfService",expiredDays))&&(NIM3_Encounter.isExpiredCheck("DateOfService",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_Encounter.isWrite("DateOfService",UserSecurityGroupID)))
            {
                        %><tr><td colspan="3"><hr></td></tr>
                        <tr><td valign=middle nowrap="nowrap"><p class=<%=theClass%> ><b>Date Of Service<br />
        (mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=middle><p><input maxlength=20  type=text size="30" name="DateOfService" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getDateOfService())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfService&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("DateOfService")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Encounter.isRead("DateOfService",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=middle><p class=<%=theClass%> ><b>Date Of Service&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=middle><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Encounter.getDateOfService())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfService&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("DateOfService")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Encounter.isRequired("LODID",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("LODID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("LODID",expiredDays))&&(NIM3_Encounter.isExpiredCheck("LODID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_Encounter.isWrite("LODID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Level of Disability&nbsp;</b></p></td><td colspan="2" valign=top><p><select   name="LODID" ><jsp:include page="../generic/tLODLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Encounter.getLODID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LODID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("LODID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_Encounter.isRead("LODID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Level of Disability&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/tLODLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Encounter.getLODID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LODID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("LODID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

       <%
            if ( (NIM3_Encounter.isRequired("Comments",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("Comments",expiredDays))&&(NIM3_Encounter.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_Encounter.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=middle><p class=<%=theClass%> ><b><%=NIM3_Encounter.getEnglish("Comments")%>&nbsp;</b></p></td><td colspan="2" valign=middle><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=NIM3_Encounter.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_Encounter.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=middle> <p class=<%=theClass%> ><b><%=NIM3_Encounter.getEnglish("Comments")%>&nbsp;</b></p></td><td colspan="2" valign=middle><p><%=NIM3_Encounter.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <tr><td>&nbsp;</td><td colspan="2">&nbsp;</td></tr>
            </table>
        </td></tr></table>


<table cellpadding="5">
  <tr class="borderHighlightGreen">
    <td class="borderHighlightGreen">Total Received Amount from Payer for Encounter:&nbsp;</td>
    <td class="borderHighlightGreen">$<input name="dTotalRec" id="dTotalRec" type="text" class="title" width="8">&nbsp;</td>
  </tr>
  <tr class="borderHighlightGreen">
    <td class="borderHighlightGreen">Total Expected Amount from Payer for Encounter:&nbsp;</td>
    <td class="borderHighlightGreen">$<input name="dTotalExp" id="dTotalExp" type="text" class="title" width="8">&nbsp;</td>
  </tr>
  <tr class="borderHighlightGreen">
    <td class="borderHighlightGreen">Total Expected to be Paid Out to Provider:&nbsp;</td>
    <td class="borderHighlightGreen">$<input name="dTotalExpOut" id="dTotalExpOut" type="text" class="title" width="8">&nbsp;</td>
  </tr>
</table>
<!-- Start -->
<%
    bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID,"","ServiceID");

//declaration of Enumeration
    bltNIM3_Service        NIM3_Service;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Service_List.elements();
    %>
 <table border="0" bordercolor="333333" cellpadding="0"  cellspacing="0" width="100%">        <tr><td>
<br />

        </td></tr>
    <%
    int iExpress=0;
    int iMaxExpress=8;
//    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tNIM3_Service"))
    while (eList.hasMoreElements()||iExpress<iMaxExpress)
    {
       iExpress++;
         %>
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        NIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
      }
      else
      {
        NIM3_Service  = new bltNIM3_Service();
        isNewRecord= true;
      }
        NIM3_Service.GroupSecurityInit(UserSecurityGroupID);
//        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tNIM3_Service")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableColor><tr>
          <td>
        <table cellpadding=0 border=0 cellspacing=0 width=100%>




                     <tr>
                       <td colspan=2 valign=top> <b>Status&nbsp;</b><select onChange="RecAmountRecalc();"    name="ServiceStatusID_<%=iExpress%>"  id="ServiceStatusID_<%=iExpress%>" ><jsp:include page="../generic/tServiceStatusLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Service.getServiceStatusID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ServiceStatusID&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("ServiceStatusID")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                     </tr>
                     <tr><td colspan=2 valign=top>CPT&nbsp;: <strong><%=NIM3_Service.getCPT()%></strong>&nbsp;&nbsp;CPT Modifier&nbsp; <strong><%=NIM3_Service.getCPTModifier()%></strong>&nbsp;&nbsp;QTY: <%=NIM3_Service.getCPTQty()%> &nbsp;&nbsp;CPT BodyPart&nbsp;<strong><%=NIM3_Service.getCPTBodyPart()%></strong></td></tr>


                     <tr><td colspan="2" valign=top>
<p class=<%=theClass%> ><b>Diagnosis/Rule-out&nbsp;</b><input name="CPTText_<%=iExpress%>" type=text value="<%=NIM3_Service.getCPTText()%>" size="80" maxlength="100">
                     &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CPTText&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("CPTText")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>                     
and/or

                     
                     <p class=<%=theClass%> >
                     
                     
                     
                     <b>Diag/ICD 1&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT1_<%=iExpress%>" value="<%=NIM3_Service.getdCPT1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT1&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT1")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>

                     <b>2&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT2_<%=iExpress%>" value="<%=NIM3_Service.getdCPT2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT2&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT2")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                     
                     <b>3&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT3_<%=iExpress%>" value="<%=NIM3_Service.getdCPT3()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT3&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT3")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                     
                     <b>4&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT4_<%=iExpress%>" value="<%=NIM3_Service.getdCPT4()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT4&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT4")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                     
                     
                     
                     
                     
                     </p></td></tr>
  <tr><td valign=top><p class=<%=theClass%> ><b>Bill Amount&nbsp;</b>&nbsp;$<input maxlength="10" type=text size="20" name="BillAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getBillAmount())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("BillAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
  &nbsp;&nbsp;<b>Contract/Allow Amount&nbsp;</b>&nbsp;$
  <input maxlength="10" type=text size="20" name="AllowAmount_<%=iExpress%>" id="AllowAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getAllowAmount())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AllowAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("AllowAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;&nbsp;Adjustment <input maxlength=10  type=text size="8" name="AllowAmountAdjustment_<%=iExpress%>" id="AllowAmountAdjustment_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getAllowAmountAdjustment())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AllowAmountAdjustment&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("AllowAmountAdjustment")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;&nbsp;<br><b>Received Amount&nbsp;</b>&nbsp;$<input name="ReceivedAmount_<%=iExpress%>" id="ReceivedAmount_<%=iExpress%>" type=text value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReceivedAmount())%>" size="20" maxlength="10" <% if (!isScheduler3){%>readonly="readonly"<%}%> >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReceivedAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("ReceivedAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
  &nbsp;&nbsp;<b>Pay-out Amount&nbsp;</b>&nbsp;$
  <input maxlength="10" type=text size="20" name="PaidOutAmount_<%=iExpress%>" id="PaidOutAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getPaidOutAmount())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PaidOutAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("PaidOutAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>

<table width="25%" border="1" cellspacing="0" cellpadding="2">
  <tr class=tdHeaderAlt>
    <td colspan="3" align="center">Payment Received <u>From</u> Payer</td>
    </tr>
  <tr class=tdHeaderAlt>
    <td>Received Check #&nbsp;</td>
    <td>Amount</td>
    <td>Date</td>
  </tr>
  <tr>
    <td><input maxlength="20" type=text size="20" name="ReceivedCheck1Number_<%=iExpress%>" id="ReceivedCheck1Number_<%=iExpress%>" value="<%=NIM3_Service.getReceivedCheck1Number()%>">&nbsp;</td>
    <td><input maxlength=10 onChange="RecAmountRecalc();"  type=text size="10" name="ReceivedCheck1Amount_<%=iExpress%>" id="ReceivedCheck1Amount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReceivedCheck1Amount())%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="20" name="ReceivedCheck1Date_<%=iExpress%>" id="ReceivedCheck1Date_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Service.getReceivedCheck1Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
  <tr>
    <td><input maxlength="20" type=text size="20" name="ReceivedCheck2Number_<%=iExpress%>" id="ReceivedCheck2Number_<%=iExpress%>" value="<%=NIM3_Service.getReceivedCheck2Number()%>">&nbsp;</td>
    <td><input maxlength=10 onChange="RecAmountRecalc();"  type=text size="10" name="ReceivedCheck2Amount_<%=iExpress%>" id="ReceivedCheck2Amount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReceivedCheck2Amount())%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="20" name="ReceivedCheck2Date_<%=iExpress%>" id="ReceivedCheck2Date_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Service.getReceivedCheck2Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
  <tr>
    <td><input maxlength="20" type=text size="20" name="ReceivedCheck3Number_<%=iExpress%>" id="ReceivedCheck3Number_<%=iExpress%>" value="<%=NIM3_Service.getReceivedCheck3Number()%>">&nbsp;</td>
    <td><input maxlength=10 onChange="RecAmountRecalc();"  type=text size="10" name="ReceivedCheck3Amount_<%=iExpress%>" id="ReceivedCheck3Amount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReceivedCheck3Amount())%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="20" name="ReceivedCheck3Date_<%=iExpress%>" id="ReceivedCheck3Date_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Service.getReceivedCheck3Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
</table>
</p>

  </td></tr>                     <tr>
                       <td valign=top>&nbsp;</td>
                       <td>&nbsp;</td>
                     </tr>


        </table>
        </td></tr></table>
        </td></tr>
        <tr><td>&nbsp;</td></tr>

        <%
    }
    %>
        <tr><td>
        <input type=hidden name=nextPage value="">
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
            <p><input class="inputButton_md_Create" onClick = "this.disabled=true;document.body.scrollTop=1;$('table').fadeOut('slow');show_pws_Calc();submit();" type=Submit value="Save" name=Submit></p>
        <%}%>
        
        <hr>
        Audit Notes:<br>
        <textarea name="" cols="90" rows="5" readonly="readonly"><%=NIM3_Encounter.getAuditNotes()%></textarea>
        
        </td></tr> </table>

    </table>


        </form>



<!-- End -->            
            
            
            
            
            
            
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
    </td></tr></table>
<script language="javascript">
RecAmountRecalc();
hide_pws_Loading();
</script>

