<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.* " %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
    <table cellpadding=0 cellspacing=0 border=0  >
    <tr><td width=10>&nbsp;</td><td>
      <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM_Encounter")%>
      
      
      
  <%
//initial declaration of list class and parentID
    Integer        iReferralID        =    null;
    Integer        iEncounterID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);

   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
	    iEncounterID = (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
    else if (pageControllerHash.containsKey("iReferralID")) 
    {
	    iReferralID = (Integer)pageControllerHash.get("iReferralID");
        accessValid = true;
    }
    
    NIM3_EncounterObject2        myEO2        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("uphcfapro") )
    {
//        NIM3_Encounter        =    new    bltNIM3_Encounter(iEncounterID,UserSecurityGroupID);
        myEO2        =    new    NIM3_EncounterObject2(iEncounterID,"Loading Billing");
    }
    else {}
    
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
	  String myReportType = request.getParameter("EDIT");
//out.println(iAuthID);
%>
	
    </td></tr>
    <tr>
      <td><script type="text/javascript">
	
	function setAction(){
		var actionURL = 'tNIM3_Encounter_upDoc_sub.jsp';
		<%if ( request.getParameter( "EDIT" ).equalsIgnoreCase("uphcfapro") ){%>
		actionURL += '?Rec_Bill_Pro=';
		date = document.forms["at_intake"]["Rec_Bill_Pro"].value;
		actionURL += date.replace(" ", "%20");

		actionURL += '&AmountBilledByProvider='+document.forms["at_intake"]["AmountBilledByProvider"].value;
		
		actionURL += '&HCFAinvoice='+document.forms["at_intake"]["HCFAinvoice"].value;
		<%}%>
		document.at_intake.action = actionURL;
		document.at_intake.submit();
	}
	function temp(){
		var actionURL = 'tNIM3_Encounter_upDoc_sub3.jsp?';
		
		document.at_intake.action = actionURL;
		document.at_intake.submit();
	}
	
	</script></td>
      <td><h1>  Document - Upload </h1></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><form method="post" enctype="multipart/form-data" name="at_intake">
      		<input type="file" name="fileData"/>
          <input type="hidden" name="EDIT" value="<%=myReportType%>"/>
       
        <%
            
            if ( request.getParameter( "EDIT" ).equalsIgnoreCase("uphcfapro") ){
            
            	if (isScheduler&&(myEO2.getNIM3_Encounter().isWrite("Rec_Bill_Pro",UserSecurityGroupID)))
                {%>
                            <br><br><b>Date Bill Rec From Provider&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
							<p><input maxlength=20  type=text size="30" name="Rec_Bill_Pro" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getRec_Bill_Pro())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pro&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Rec_Bill_Pro")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                <%}
	            else if (isScheduler&&(myEO2.getNIM3_Encounter().isRead("Rec_Bill_Pro",UserSecurityGroupID)))
	            {%>
	                        <p><b>Bill Rec By Provider&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
	                        <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getRec_Bill_Pro())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pro&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Rec_Bill_Pro")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>	                      
	            <%}
	            else
	            {%><%}
	            %><%
	            if ((myEO2.getNIM3_Encounter().isWrite("AmountBilledByProvider",UserSecurityGroupID)))
	            {%>
	                        <p><b>Amount Billed By Provider:&nbsp;</b></p>
	                        <p><input maxlength=10  type=text size="20" name="AmountBilledByProvider" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(myEO2.getNIM3_Encounter().getAmountBilledByProvider())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AmountBilledByProvider&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("AmountBilledByProvider")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
	            <%}
	            else if ((myEO2.getNIM3_Encounter().isRead("AmountBilledByProvider",UserSecurityGroupID)))
	            {%>
	                        <p><b>AmountBilledByProvider&nbsp;</b></p>
							<p><%=PLCUtils.getDisplayDefaultDecimalFormat2(myEO2.getNIM3_Encounter().getAmountBilledByProvider())%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AmountBilledByProvider&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("AmountBilledByProvider")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
	            <%}
	            else
	            {%><%}
	            if ((myEO2.getNIM3_Encounter().isWrite("HCFAinvoice",UserSecurityGroupID)))
	            {%>
	                        <p><b>HCFA invoice:&nbsp;</b></p>
	                        <p><input maxlength=10  type=text size="20" name="HCFAinvoice" value="<%=myEO2.getNIM3_Encounter().getHCFAinvoice()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HCFAinvoice&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("HCFAinvoice")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
	            <%}
	            else if ((myEO2.getNIM3_Encounter().isRead("HCFAinvoice",UserSecurityGroupID)))
	            {%>
	                        <p><b>HCFA invoice&nbsp;</b></p>
							<p><%=myEO2.getNIM3_Encounter().getHCFAinvoice()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HCFAinvoice&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("HCFAinvoice")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
	            <%}
	            else
	            {%><%}
            }
            else {}%>
            
          
         
        </p>
        
<input type="submit" onClick="this.disabled=true;setAction();" value="Upload" />          
</p>
      </form>      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>Please submit document as a Word Document or PDF. </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>All information is secured through 128 bit SSL Encryption.</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    </table>
<br>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
