 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    Created on 3/15/2013
    Created by: Scott Ellis
*/%>

<%@ include file="../../generic/generalDisplay.jsp" %>

<link href="//netdna.bootstrapcdn.com/bootswatch/2.3.1/cosmo/bootstrap.min.css" rel="stylesheet">

<div class="container">

<h1>CPT Wizard Demo</h1>
<a href="cpt_wizard1.jsp" class="btn btn-info btn-small pull-right"><i class="icon-refresh icon-white"></i> Start Over</a>

<div class="hero-unit">
<form action="#" id="wizard">

<%
String modality = request.getParameter("modality");
String special = request.getParameter("special");
String contrast = request.getParameter("contrast");
String bodyPart = request.getParameter("bodyPart");
String orientation = request.getParameter("orientation");
java.util.ArrayList<String> optionsList = null;
boolean showOptions = true;
if (modality==null) {
	%>
	Modality:
	<select name="modality" onChange="document.getElementById('wizard').submit();">
	<%
	optionsList = NIM3_CPTWizard.getWizardOptions();
} else if (bodyPart==null){
	%>
	<h4><%=modality%>/</h4>
	<input name="modality" type="hidden" value="<%=modality%>">
	Body Part:
	<select name="bodyPart" onChange="document.getElementById('wizard').submit();">
	<%
	optionsList = NIM3_CPTWizard.getWizardOptions(modality);
} else if (special==null){
	%>
	<h4><%=modality%>/<%=bodyPart%>/</h4>
	<input name="modality" type="hidden" value="<%=modality%>">
	<input name="bodyPart" type="hidden" value="<%=bodyPart%>">
	Special Procedure:
	<select name="special" onChange="document.getElementById('wizard').submit();">
	<%
	optionsList = NIM3_CPTWizard.getWizardOptions(modality, bodyPart);
} else if (contrast==null){
	%>
	<h4><%=modality%>/<%=bodyPart%>/<%=special%>/</h4>
	<input name="modality" type="hidden" value="<%=modality%>">
	<input name="bodyPart" type="hidden" value="<%=bodyPart%>">
	<input name="special" type="hidden" value="<%=special%>">
	Contrast:
	<select name="contrast" onChange="document.getElementById('wizard').submit();">
	<%
	optionsList = NIM3_CPTWizard.getWizardOptions(modality, bodyPart, special);
} else if (orientation==null){
	%>
	<h4><%=modality%>/<%=bodyPart%>/<%=special%>/<%=contrast%>/</h4>
	<input name="modality" type="hidden" value="<%=modality%>">
	<input name="bodyPart" type="hidden" value="<%=bodyPart%>">
	<input name="special" type="hidden" value="<%=special%>">
	<input name="contrast" type="hidden" value="<%=contrast%>">
	Orientation:
	<select name="orientation" onChange="document.getElementById('wizard').submit();">
	<%
	optionsList = NIM3_CPTWizard.getWizardOptions(modality, bodyPart, special, contrast);
} else {
	showOptions = false;
	bltCPTWizard myCW = NIM3_CPTWizard.getWizardOptions(modality, bodyPart,  special, contrast,  orientation);
	
	%>
	<h4><%=modality%>/<%=bodyPart%>/<%=special%>/<%=contrast%>/<%=orientation%>/</h4>
	<h3> You have selected the following codes:</h3>
	<dl>
		<dt><%=myCW.getCPT1()%></dt>
			<dd><%=myCW.getBP1()%></dd>
		<dt><%=myCW.getCPT2()%></dt>
			<dd><%=myCW.getBP2()%></dd>
		<dt><%=myCW.getCPT3()%></dt>
			<dd><%=myCW.getBP3()%></dd>
			<br>
	<a href="cpt_wizard1.jsp" class="btn btn-info btn-large">Start Over</a>
	<a href="search.jsp?wizard=<%=myCW.getCPTWizardID()%>" class="btn btn-success btn-large">Find Centers</a>
	
	<%

}

if (showOptions){
	boolean isOnly=false;
	if (optionsList.size()<=2 )
	{
	isOnly = true;
	}
	for (String theOption: optionsList){
	String showMe = theOption;
	%>
	<option <% if (isOnly) {%> selected <%}%> value="<%=theOption%>" ><%=showMe%></option>
	<%
	}
	%>
	</select>
	<%
	if (isOnly)
	{
	%>
	<script language="javascript">
	   document.getElementById('wizard').submit();
	</script>
	<%		
	}

	
	}
%>
</form>
</div>
<div class="well alert-info">
<h3><strong>About this demo</strong></h3>
<h4>The purpose of this demo is to illustrate a different method for determining specific CPT codes (or sets of codes in the case of a procedure such as an Arthrogram).  The idea is to present a series of <em>simple</em> questions to a scheduler, Adjuster, or NID Patient and end up with specific CPT codes.  This would also allow us to provide pricing details directly for NID patients. </h4></div>
<hr>
Copyright &copy; NextImage Direct | All Rights Reserved
</div>
