 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    Created on 3/15/2013
    Created by: Scott Ellis
*/%>

<%@ include file="../../generic/generalDisplay.jsp" %>
<style>
.badge-image {
margin-top: 5px;
}
</style>
<link href="//netdna.bootstrapcdn.com/bootswatch/2.3.1/cosmo/bootstrap.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link rel="stylesheet" type="text/css" href="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
 
<!-- jQuery -->
<script type="text/javascript" charset="utf8" src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
 
<!-- DataTables -->
<script type="text/javascript" charset="utf8" src="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>

<div class="container">

<h1>NID Search Demo</h1>
<a href="cpt_wizard1.jsp" class="btn btn-danger btn-small pull-right"><i class="icon-refresh icon-white"></i> Start Over</a>

<div class="hero-unit">

<h4>Below is a list of centers that are near you.  You can sort by the closest center or the lowest price. </h4>

<%

String zip=request.getParameter("zip");
Integer wid = new Integer(request.getParameter("wizard"));

if (zip==null || zip.equalsIgnoreCase("")) {

zip = "";
%>
<form action="search.jsp" id="wizard" class="form-search">
<p>
	Zip Code: <input type="text" value="<%=zip%>" name="zip" class="input-medium search-query">
	<input type="hidden" value="<%=wid%>" name="wizard" >
   <button class="btn btn-primary btn-small" onClick="document.getElementById('wizard').submit();">Find Centers</button></p>
</form>
<%
} else {

%>
<form action="search.jsp" id="wizard" class="form-search">
<p>
	Zip Code: <input type="text" value="<%=zip%>" name="zip" class="input-medium search-query">
	<input type="hidden" value="<%=wid%>" name="wizard" >
   <button class="btn btn-primary btn-small" onClick="document.getElementById('wizard').submit();">Find Centers</button></p>
</form>
<%
try {
	//Load the CPT Wizard object that the user selected
	bltCPTWizard myWizard = new bltCPTWizard(wid);
	
	if (myWizard==null){
	%>
		<div class="alert alert-warning">Bad Wizard (Debug: Should not happen)</div>
	<%
	}

	//Translate the Wizard ID into a CPTGroupObject2 (which contains a group of CPT codes, etc.)
	CPTGroupObject2 cgo2 = NIMUtils.CPTWizardToCPTGroupObject(myWizard);

	if (cgo2==null){
	%>
		<div class="alert alert-warning">Bad cgo2 (Debug: Should not happen)</div>
	<%
	}
	
	
	//Get a listing of centers that are 30 miles from the zip and can service the CPT Group Object 2.  
	//Note the final parameter is a boolean that tells whether the results should be limited only to centers containing all the CPT Group objects.  Default = True
	//  This means that if the user needs an US only centers that have US will show up.  
	ImagingCenterListObject ICLO = NIDUtils.getListOfCenters(zip, 30, cgo2, true, false,20);

	
	if (ICLO==null){
	%>
		<div class="alert alert-warning">Sorry, we could not find any centers within 200 miles.  Try a nearby zip code to see if that works or give us a call for more options. </div>
	<%
	} 
	if (ICLO==null||ICLO.getListOfCenters()==null){
	%>
		<div class="alert alert-warning">Bad ICLO List (Debug: Should not happen)</div>
	<%
	} else {
		%>
		<table class="table table-striped table-bordered table-hover" id="example">
		<thead>
		<tr>
			<th>Distance</th>
			<th>Features</th>
			<th>Price</th> 
			<th>Value Score</th> 
			<th>Savings</th> 
			<th></th>
		</tr> 
		</thead>
		<tbody>
		<%
			for (PracticeMaster_SearchResultsObject pm_sro: ICLO.getListOfCenters()){
					try {
					%>
					<tr>
						<td title="Going the distance">
							<%=String.format("%02d", Math.round(pm_sro.getDistance()))%> miles away
							<br>
							<%=pm_sro.getPracticeMaster().getOfficeCity().toUpperCase()%>
						</td>
						<td>
						<%
						if (pm_sro.getBadge_SuperHighField()) {
						%>
							<div class="badge badge-success" title="Has a 'Super' high field (3.0 Tesla) MRI available.  3T MRI's are capable of producing some of the highest quality images.">3T</div>
							<br>
							<img src="badge/traditional.png" class="badge-image" title="Has a traditional MRI.  These allow for higher quality images and are the traditional tube/round shape."></img>
							<br>
						<%
						} else if (pm_sro.getBadge_HighField()) {
						%>
							<div class="badge badge-info" title="Has a high field (~1.5 Tesla) MRI available.  These MRI's are capable of producing high quality images and are recommended over low-field MRIs whenever possible."><i class="icon-star icon-white"></i> 1.5T</div>
							<br>
							<img src="badge/traditional.png" class="badge-image" title="Has a traditional MRI.  These allow for higher quality images and are the traditional tube/round shape."></img>
							<br>
						<%
						} else {
						%>
							<div class="badge badge-warning" title="Has a low-field MRI (Less than 1.0 Tesla).  Low field MRIs produce lower resolution images.  Open-MRIs frequently use low-field magnets. In some cases these are required, but a high-field is preferred when possible. "><i class="icon-warning-sign icon-white"></i> &lt;1.0T</h5></div>
							<br>
						<%
						}

						if (pm_sro.getBadge_Open()) {
						%>
							<img src="badge/open.png" class="badge-image"  title="Has an OpenMRI style device.  Note these devices normally produce lower resolution images compared to a traditional high-field.  These are needed for cases of severe claustrophobia or certain body-types."></img><br>
						<%
						}
						if (pm_sro.getBadge_ACR()) {
						%>
							<img src="badge/acr.png"  class="badge-image"  title="One or more machines has been Accredited by the American College of Radiology: <%=pm_sro.getBadge_ACR_Notes("; ")%>"></img><br>
						<%
						}
						if (pm_sro.getBadge_BestValue()) {
						%>
							<div class="badge badge-success" title="Best Value!!!"><i class="icon-star icon-white"></i><i class="icon-star icon-white"></i> Best Value</div>
						<%
						}
						%>
					</td>
						<td>$<%=Math.round(pm_sro.getCustomerPrice())%> </td>
						<td><%=Math.round(pm_sro.getMACScore())%> </td>
						<td><span class="badge-success" title="Based on U&C = <%=pm_sro.getPriceUC()%>">&nbsp;&nbsp;<%=pm_sro.getSavingsPercentage()%>%&nbsp;&nbsp;</span></td>
						<td >
							<a href="choose.jsp?pid=<%=pm_sro.getPracticeMaster().getPracticeID()%>" class="btn btn-info pull-right">Select Center</a>
						</td>
					</tr>
					<%
					} catch (Exception eee) {
					%>
					<div class="well well-large alert-danger">Error [<%=eee%>]</div>
					<%
					}
			
			
			}
		%>
		
		</tbody>
		</table>
		<%
		
	}


} catch (Exception eee) {
%>
<div class="well well-large alert-danger">Error [<%=eee%>]</div>
<%
}


	
	
	
}

%>

</div>


<div class="well alert-info">
<h3><strong>About this demo</strong></h3>
<h4>The purpose of this demo is to illustrate a different method for determining specific CPT codes (or sets of codes in the case of a procedure such as an Arthrogram).  The idea is to present a series of <em>simple</em> questions to a scheduler, Adjuster, or NID Patient and end up with specific CPT codes.  This would also allow us to provide pricing details directly for NID patients. </h4></div>
<hr>
Copyright &copy; NextImage Direct | All Rights Reserved
</div>



<script>
$(document).ready(function() {
    $('#example').dataTable( {
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": false
    } );
} );
</script>



