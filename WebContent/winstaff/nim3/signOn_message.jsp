
<html>
<head>
<title>WIN|Staff Sign Off</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="winstaff.css" type="text/css">
<link rel="stylesheet" href="ui_1/style_PhysicianID.css" type="text/css">
<script language="JavaScript">
<!--
function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" text="#000000" onLoad="javascript:this.focus();setTimeout(window.close,25000)" class="tdBase" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table class=tdBase width="100%%" border="0" cellspacing="0" cellpadding="4">
  <tr > 
    <td class=title bgcolor="#003366"><font color="#FFFFFF">System Requirements:</font></td>
  </tr>
  <tr > 
    <td> 
      <p class="title" align="center">Please note:<br>
        This On-line Credentialing System requires Internet Explorer 5.0 or greater</p>
      <p >This site is NOT compatible with Macintosh systems or AOL users unless 
        using Windows Internet Explorer 5.x or greater. If you are using AOL, 
        after signing on, simply minimize AOL and use the Windows Internet Explorer 
        to access our site. If you do not have the latest version of Internet 
        Explorer you can download this here:</p>
      <p>Via the <a href="http://windowsupdate.microsoft.com" target="_blank">Windows 
        Update System</a></p>
      <p>Or <a href="http://www.microsoft.com/ie" target="_blank">download here</a></p>
    </td>
  </tr>
  <tr> 
    <td align="center" bgcolor="#003366"><a href="#" onClick="MM_callJS('window.close()')"><font color="#FFFFFF">close 
      window</font></a> </td>
  </tr>
</table>
<p class="title" align="center">&nbsp;</p>
<p class="title" align="center">&nbsp;</p>
</body>
</html>
