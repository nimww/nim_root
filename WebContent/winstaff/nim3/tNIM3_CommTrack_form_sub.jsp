<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tNIM3_CommTrack_form_sub.jsp
    Created on Jun/24/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%!
public static void email(String emailTo, String emailFrom, String theSubject, String theBody, String type) {
		
	try{
		bltEmailTransaction email = new bltEmailTransaction();
		email.setEmailTo(emailTo);
		email.setEmailFrom(emailFrom);
		email.setEmailSubject(theSubject);
		email.setEmailBody(theBody);
		email.setEmailBodyType(type);
		email.setTransactionDate(new java.util.Date());
		email.commitData();	
	} catch (Exception e){}
	}

public static void sendEmail(String emailTo, String emailFrom, String theSubject, String theBody, String bccTo) {
	email( emailTo,  emailFrom,  theSubject,  theBody, "text/plain; charset=us-ascii");
	
	String newBody = "To: "+emailTo+"<br>" +
			"From: "+emailFrom+"<br>" +
			"Subject: "+theSubject+"<br>" +
			"Body: <br>"+theBody+"<br>";

	
	email(bccTo, emailFrom, theSubject, newBody, emailType_V3.HTML_TYPE);
}

 %>

<%

//initial declaration of list class and parentID
    Integer        iCommTrackID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iCommTrackID")) 
    {
        iCommTrackID        =    (Integer)pageControllerHash.get("iCommTrackID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
        if (iCommTrackID.intValue() != iEDITID.intValue())
        {
        	accessValid = false;
        }
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("newsales")&&isReporter )
    {
			accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID


try{
	if(!request.getParameter("EmailSubject").isEmpty() && !request.getParameter("EmailBody").isEmpty()){
		
		//out.print("<script>alert('email sent');</script>");
		sendEmail(request.getParameter("EmailTo"), request.getParameter("EmailFrom"), request.getParameter("EmailSubject"), request.getParameter("EmailBody"), request.getParameter("EmailFrom"));
		
	}
} catch (Exception e){}

    bltNIM3_CommTrack        NIM3_CommTrack        =    null;
	boolean isSales = false;
    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        NIM3_CommTrack        =    new    bltNIM3_CommTrack(iCommTrackID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_CommTrack        =    new    bltNIM3_CommTrack(UserSecurityGroupID,true);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("newsales") )
    {
        NIM3_CommTrack        =    new    bltNIM3_CommTrack(UserSecurityGroupID, true);
		isSales = true;
		
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !NIM3_CommTrack.getUniqueCreateDate().equals(testObj)  )
    {
         NIM3_CommTrack.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !NIM3_CommTrack.getUniqueModifyDate().equals(testObj)  )
    {
         NIM3_CommTrack.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !NIM3_CommTrack.getUniqueModifyComments().equals(testObj)  )
    {
         NIM3_CommTrack.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CaseID")) ;
    if ( !NIM3_CommTrack.getCaseID().equals(testObj)  )
    {
         NIM3_CommTrack.setCaseID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack CaseID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("EncounterID")) ;
    if ( !NIM3_CommTrack.getEncounterID().equals(testObj)  )
    {
//         NIM3_CommTrack.setEncounterID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack EncounterID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReferralID")) ;
    if ( !NIM3_CommTrack.getReferralID().equals(testObj)  )
    {
         NIM3_CommTrack.setReferralID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack ReferralID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ServiceID")) ;
    if ( !NIM3_CommTrack.getServiceID().equals(testObj)  )
    {
         NIM3_CommTrack.setServiceID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack ServiceID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IntUserID")) ;
    if ( !NIM3_CommTrack.getIntUserID().equals(testObj)  )
    {
         NIM3_CommTrack.setIntUserID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack IntUserID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ExtUserID")) ;
    if ( !NIM3_CommTrack.getExtUserID().equals(testObj)  )
    {
         NIM3_CommTrack.setExtUserID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack ExtUserID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CommTypeID")) ;
    if ( !NIM3_CommTrack.getCommTypeID().equals(testObj)  )
    {
         NIM3_CommTrack.setCommTypeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack CommTypeID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("CommStart"))) ;
    if ( !NIM3_CommTrack.getCommStart().equals(testObj)  )
    {
         NIM3_CommTrack.setCommStart( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack CommStart not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("CommEnd"))) ;
    if ( !NIM3_CommTrack.getCommEnd().equals(testObj)  )
    {
         NIM3_CommTrack.setCommEnd( testObj,UserSecurityGroupID   );
		 if (NIM3_CommTrack.getCommEnd().before(NIM3_CommTrack.getCommStart()))
		 {
			 NIM3_CommTrack.setCommEnd(NIM3_CommTrack.getCommStart());
		 }
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack CommEnd not set. this is ok-not an error");
}

//look for ssDuration
try
{
	if (request.getParameter("ssDuration")!=null){
	    Integer testObj = new Integer(request.getParameter("ssDuration")) ;
		
        NIM3_CommTrack.setCommEnd( workingDays.getDate_AddMinutes(NIM3_CommTrack.getCommStart(),testObj)  );
	}
}
catch(Exception e)
{
     //out.println("NIM3_CommTrack CommEnd not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("MessageText")) ;
    if ( !NIM3_CommTrack.getMessageText().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageText( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageText not set. this is ok-not an error");
}

try
{
    String testObj = new String(CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName()) ;
    if ( !NIM3_CommTrack.getMessageName().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageCompany")) ;
    if ( !NIM3_CommTrack.getMessageCompany().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageCompany( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageCompany not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageEmail")) ;
    if ( !NIM3_CommTrack.getMessageEmail().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessagePhone")) ;
    if ( !NIM3_CommTrack.getMessagePhone().equals(testObj)  )
    {
         NIM3_CommTrack.setMessagePhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessagePhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageFax")) ;
    if ( !NIM3_CommTrack.getMessageFax().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageFax not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AlertStatusCode")) ;
    if ( !NIM3_CommTrack.getAlertStatusCode().equals(testObj)  )
    {
         NIM3_CommTrack.setAlertStatusCode( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack AlertStatusCode not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !NIM3_CommTrack.getComments().equals(testObj)  )
    {
         NIM3_CommTrack.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack Comments not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("Link_UserID")) ;
    if ( !NIM3_CommTrack.getLink_UserID().equals(testObj)  )
    {
         NIM3_CommTrack.setLink_UserID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack Link_UserID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageSubject")) ;
    if ( !NIM3_CommTrack.getMessageSubject().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageSubject( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageSubject not set. this is ok-not an error");
}
boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tNIM3_CommTrack","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

		NIM3_CommTrack.setIntUserID(CurrentUserAccount.getUserID());
	if (isSales){
		NIM3_CommTrack.setAlertStatusCode(new Integer(1));
	}
	
	
	NIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    NIM3_CommTrack.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        NIM3_CommTrack.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
//				nextPage = "tNIM3_CommTrack_query.jsp";
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null||true)
{
	    %><script language=Javascript>
		window.close();
//	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
