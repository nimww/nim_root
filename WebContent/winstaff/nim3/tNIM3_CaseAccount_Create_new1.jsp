<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*    " %>
<%/*
    filename: tNIM3_Referral_main_NIM3_Referral_CaseID.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
function disableInput_UA(myPrefix)
{
	document.getElementById(myPrefix + 'FirstName').disabled=true;
	document.getElementById(myPrefix + 'LastName').disabled=true;
	document.getElementById(myPrefix + 'Phone').disabled=true;
	document.getElementById(myPrefix + 'Fax').disabled=true;
	document.getElementById(myPrefix + 'Email').disabled=true;
	document.getElementById(myPrefix + 'Company').disabled=true;
	document.getElementById(myPrefix + 'FirstName').className = 'disabled';
	document.getElementById(myPrefix + 'LastName').className = 'disabled';
	document.getElementById(myPrefix + 'Phone').className = 'disabled';
	document.getElementById(myPrefix + 'Fax').className = 'disabled';
	document.getElementById(myPrefix + 'Email').className = 'disabled';
	document.getElementById(myPrefix + 'Company').className = 'disabled';
}


function loadXMLDoc(sType)
{
	var xmlhttp;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
    		document.getElementById('fuzzy_' + sType).innerHTML=xmlhttp.responseText;
			testarea
	    }
	}
	if (sType=="Case")
	{
		xmlhttp.open("GET","intake/fuzzysearch.jsp?sType=case&fz_claimnumber=" + document.getElementById('CaseClaimNumber').value + "&fz_patientln=" + document.getElementById('PatientLastName').value + "&fz_patientfn=" + document.getElementById('PatientFirstName').value ,true);
	}
	else if (sType=="ua_adj")
	{
		xmlhttp.open("GET","intake/fuzzysearch.jsp?sType=ua_adj&fz_lastname=" + document.getElementById('AdjusterLastName').value + "&fz_firstname=" + document.getElementById('AdjusterFirstName').value + "&fz_phone=" + document.getElementById('AdjusterPhone').value + "&fz_email=" + document.getElementById('AdjusterEmail').value ,true);
	}
	else if (sType=="ua_rb")
	{
		xmlhttp.open("GET","intake/fuzzysearch.jsp?sType=ua_rb&fz_lastname=" + document.getElementById('ReferredByLastName').value + "&fz_firstname=" + document.getElementById('ReferredByFirstName').value + "&fz_phone=" + document.getElementById('ReferredByPhone').value + "&fz_email=" + document.getElementById('ReferredByEmail').value ,true);
	}
	xmlhttp.send();
}
</script>






<script language="javascript">
function formatSSN()
{
     var theCount = 0;
     var theString = document.getElementById("PatientSSN").value;
     var newString = "";
     var myString = theString;
     var theLen = myString.length;
     for ( var i = 0 ; i < theLen ; i++ )
     {
     // Character codes for ints 1 - 9 are 48 - 57
          if ( (myString.charCodeAt(i) >= 48 ) && (myString.charCodeAt(i) <= 57) )
          newString = newString + myString.charAt(i);   
     }
// Now the validation to determine that the remaining string is 9 characters.
     if (newString.length == 9 )
     {
// Now the string has been stripped of other chars it can be reformatted to ###-##-#### 
          var newLen = newString.length;
          var newSSN = "";
          for ( var i = 0 ; i < newLen ; i++ )
          {
               if ( ( i == 2 ) || ( i == 4 ) )
               {
                    newSSN = newSSN + newString.charAt(i) + "-";
               }else{
                    newSSN = newSSN + newString.charAt(i);
               }
          }
          document.getElementById("PatientSSN").value = newSSN;
          return true;
     }else{
          alert("The Social Security Number you entered "+newString+" does not contian the correct number of digits");
          document.getElementById("PatientSSN").focus();
          return false;
     }
}

function formatPhone( myElement)
{
     var theCount = 0;
     var theString = document.getElementById(myElement).value;
     var newString = "";
     var myString = theString;
     var theLen = myString.length;
     for ( var i = 0 ; i < theLen ; i++ )
     {
     // Character codes for ints 1 - 9 are 48 - 57
          if ( (myString.charCodeAt(i) >= 48 ) && (myString.charCodeAt(i) <= 57) )
          newString = newString + myString.charAt(i);   
     }
// Now the validation to determine that the remaining string is 9 characters.
     if (newString.length == 10 )
     {
// Now the string has been stripped of other chars it can be reformatted to ###-##-#### 
          var newLen = newString.length;
          var newSSN = "";
          for ( var i = 0 ; i < newLen ; i++ )
          {
               if ( ( i == 2 ) || ( i == 5 ) )
               {
                    newSSN = newSSN + newString.charAt(i) + "-";
               }else{
                    newSSN = newSSN + newString.charAt(i);
               }
          }
          document.getElementById(myElement).value = newSSN;
          return true;
     }else{
          alert("The Phone Number you entered "+newString+" does not contian the correct number of digits");
          document.getElementById(myElement).focus();
          return false;
     }
}
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
</script>



    <table cellpadding=0 cellspacing=0 border=0 width=98%  >
    <tr><td width=10>&nbsp;</td><td>
<%
//initial declaration of list class and parentID

    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler)
    {
        accessValid = true;
    }
  //page security
  if (accessValid&&isScheduler)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

        %>
        <table border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase width="100%">
         <tr>
           <td class=tableColor>
        <form action="tNIM3_CaseAccount_express1_form_sub.jsp" name="tNIM3_CaseAccount_form1" method="POST">
          <%  String theClass ="tdBase";%>

            <table width="100%" cellpadding=0 cellspacing=0  >
                     <tr>
                       <td class=tdHeaderAlt>&nbsp;</td>
                       <td class="borderHighlightBlackDottedNoFont">Assigned To
                       
<select name="AssignedToID"/>
<option value="0">Unassigned/Empty</option>
    <%
{
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("select Userid,LogonUserName, contactfirstname, contactlastname from tUserAccount where accounttype ='SchedulerID' or accounttype='SchedulerID2' or accounttype='SchedulerID3' order by LogonUserName");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
					%>
					<option  value="<%=myRS.getString("UserID")%>"><%=myRS.getString("LogonUserName")%> [<%=myRS.getString("contactfirstname")%>, <%=myRS.getString("contactlastname")%>]</option>
					<%
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
%>
    </select>  
                       
                       
                       </td>
                       <td width="100%" class="borderHighlightBlackDottedNoFont">&nbsp;</td>
                     </tr>
                     <tr>
                       <td class=tdHeaderAlt>&nbsp;</td>
                       <td class=tdHeaderAlt><span class="title">Intake Worksheet</span></td>
                       <td class=tdHeaderAlt><span class="title">Intake Assisstant</span></td>
                     </tr>
                     <tr>
                       <td class=tdHeaderAlt>&nbsp;</td>
                       <td valign="top" class=tdHeaderAlt>&nbsp;</td>
                       <td valign="top" class=tdHeaderAlt>&nbsp;</td>
                     </tr>


            <tr>
              <td>&nbsp;</td>
              <td valign="top"><span class="tdHeaderAlt">Who is Calling 
                  <input maxlength="10" type="hidden" size="10" name="ReferredByContactID" id="ReferredByContactID" value="0" onchange="disableInput_UA('ReferredBy')"  />
              </span>
              
              <table border="0" cellpadding="3" cellspacing="0">            <tr>
              <td>First Name<br />                <input maxlength="100" type="text" size="40" name="ReferredByFirstName" id="ReferredByFirstName" value="" onchange="loadXMLDoc('ua_rb');" /></td>
              </tr>
            <tr>
              <td>Last Name<br />
                <input maxlength="100" type="text" size="40" name="ReferredByLastName" id="ReferredByLastName" value="" onchange="loadXMLDoc('ua_rb');" /></td>
              </tr>
            <tr>
              <td>Phone<br />
                <input maxlength="100" type="text" size="40" name="ReferredByPhone" id="ReferredByPhone" value="" onchange="loadXMLDoc('ua_rb');" /></td>
              </tr>
            <tr>
              <td>Fax<br />
                <input maxlength="100" type="text" size="40" name="ReferredByFax" id="ReferredByFax" value=""  /></td>
              </tr>
            <tr>
              <td>E-Mail<br />
                <input maxlength="100" type="text" size="40" name="ReferredByEmail" id="ReferredByEmail" value="" onchange="loadXMLDoc('ua_rb');" /></td>
              </tr>
            <tr>
              <td>Company<br />
                <input maxlength="100" type="text" size="40" name="ReferredByCompany" id="ReferredByCompany" value="" onchange="loadXMLDoc('ua_rb');" /></td>
              </tr>
</table>
              
              
              </td>
              <td valign="top"><span class="tdHeader">Similar Users to the ReferredBy:</span><br /><div id="fuzzy_ua_adj" class="divboxFuzzy1" name="fuzzy_ua_adj">Please Type in some information about this user first.</div></td>
            </tr>

                     <tr>
                       <td class=tdHeaderAlt>&nbsp;</td>
                       <td valign="top" class=tdHeaderAlt>
                       <span class="tdHeaderAlt">Case Information</span>
                       <table border="0" cellpadding="3" cellspacing="0">                     <tr>
                       <td >Claim Number
                         <br>
                         <input maxlength="100" type=text size="40" name="CaseClaimNumber" id="CaseClaimNumber" value="" onChange="loadXMLDoc('Case');"></td>
                     </tr>
                     <tr>
                       <td >Patient First Name
                         <br>
                         <input maxlength="100" type=text size="40" name="PatientFirstName" id="PatientFirstName" value="">
                       </td>
                     </tr>
                     <tr>
                       <td >Last Name<br>
                         <input maxlength="100" type=text size="40" name="PatientLastName" id="PatientLastName" value="" onChange="loadXMLDoc('Case');">
                       </p></td>
                     </tr>
</table></td>
                       <td valign="top"><span class="tdHeader">Similar Cases:</span><br /><div id="fuzzy_case" class="divboxFuzzy1" >Please enter some information about the case first</div></td>
                     </tr>


            <tr>
              <td>&nbsp;</td>
              <td valign="top"><span class="tdHeaderAlt">Adjuster Information 
                <input maxlength="10" type="hidden" size="10" name="AdjusterID" id="AdjusterID" value="" onchange="disableInput_UA('Adjuster')"  />
              </span>
              
              <table border="0" cellpadding="3" cellspacing="0">            <tr>
              <td>First Name<br />                <input maxlength="100" type="text" size="40" name="AdjusterFirstName" id="AdjusterFirstName" value="" onchange="loadXMLDoc('ua_adj');" /></td>
              </tr>
            <tr>
              <td>Last Name<br />
                <input maxlength="100" type="text" size="40" name="AdjusterLastName" id="AdjusterLastName" value="" onchange="loadXMLDoc('ua_adj');" /></td>
              </tr>
            <tr>
              <td>Phone<br />
                <input maxlength="100" type="text" size="40" name="AdjusterPhone" id="AdjusterPhone" value="" onchange="loadXMLDoc('ua_adj');" /></td>
              </tr>
            <tr>
              <td>Fax<br />
                <input maxlength="100" type="text" size="40" name="AdjusterFax" id="AdjusterFax" value=""  /></td>
              </tr>
            <tr>
              <td>E-Mail<br />
                <input maxlength="100" type="text" size="40" name="AdjusterEmail" id="AdjusterEmail" value="" onchange="loadXMLDoc('ua_adj');" /></td>
              </tr>
            <tr>
              <td>Company<br />
                <input maxlength="100" type="text" size="40" name="AdjusterCompany" id="AdjusterCompany" value="" onchange="loadXMLDoc('ua_adj');" /></td>
              </tr>
</table>
              
              
              </td>
              <td valign="top"><span class="tdHeader">Similar Users to the Adjuster:</span><br /><div id="fuzzy_ua_adj" class="divboxFuzzy1" name="fuzzy_ua_adj">Please Type in some information about this user first.</div></td>
            </tr>
            
            
<tr><td colspan="3">

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
              <select name="SubmitType" id="SubmitType">
              <option value="0">Save & Return to Dashboard</option>
              <option value="1" selected>Save & Edit Services</option>
              </select>
            <p><input  class="inputButton_md_Create" onClick = "MM_validateForm('CaseClaimNumber','','R');if (document.MM_returnValue){document.getElementById('SubmitType').value=0;this.disabled=true;submit();return false}else{this.disabled=false;return false}" type=Submit value="Save & Return" name=Submit>  
            &nbsp;&nbsp;&nbsp;
<input  class="inputButton_md_Create" onClick = "MM_validateForm('CaseClaimNumber','','R');if (document.MM_returnValue){document.getElementById('SubmitType').value=1;this.disabled=true;submit();return false}else{this.disabled=false;return false}" type=Submit value="Save & Edit Services" name=Submit>     

<%//show_pws_Saving() removed from javascript     [document.body.scrollTop=1;$('table').fadeOut('slow');show_pws_Saving();submit();return false]%>         


        <%}%>
        </td></tr></table>
        </form>



    </td>
           <td valign="top" class=tableColor><p class="title">&nbsp;</p>
             <table width="200" border="0" cellspacing="0" cellpadding="5">

             <tr>
               <td></td>
             </tr>
             <tr>
               <td></td>
             </tr>
           </table>
           <p>&nbsp;</p>
           <p>&nbsp;</p></td>
         </tr>
      </table>


        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>