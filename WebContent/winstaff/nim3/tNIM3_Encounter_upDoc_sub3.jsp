<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*, org.apache.commons.fileupload.*" %>
<%/*
    filename: tNIM3_Encounter_main_NIM3_Encounter_CaseID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0  >
    <tr><td width=10>&nbsp;</td><td>
      <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM3_Encounter")%>
      
      
      
  <%
//initial declaration of list class and parentID
    Integer        iEncounterID	=	null;
    Integer        iReferralID	=    null;
    Integer        iEDITID	=	null;
    bltNIM3_Encounter myEncounter = null;
    bltNIM3_Referral myRef = null;
    
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    
    
    boolean accessValid = false;
	
	String fileToUpload = null; 
	
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
	Integer iAuthID = null;
	
	
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
	    iEncounterID = (Integer)pageControllerHash.get("iEncounterID");
		myEncounter = new bltNIM3_Encounter(iEncounterID);
		myRef = new bltNIM3_Referral(myEncounter.getReferralID());
        accessValid = true;
    }
    else if (pageControllerHash.containsKey("iReferralID")) 
    {
	    iReferralID = (Integer)pageControllerHash.get("iReferralID");
		myRef = new bltNIM3_Referral(iReferralID);
        accessValid = true;
    }
	
    
	  
  //page security
  if (accessValid)
  {
	  bltNIM3_Encounter        NIM3_Encounter        =    null;	
	  if ( request.getParameter( "EDIT" ).equalsIgnoreCase("uphcfapro") )
	    {
	        NIM3_Encounter        =    new    bltNIM3_Encounter(iEncounterID,UserSecurityGroupID);
	        out.println("bltnim3_encounter created<br>");//delete me
	    }
	    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
	    {
	        NIM3_Encounter        =    new    bltNIM3_Encounter(UserSecurityGroupID,true);
	        out.println("bltnim3_encounter created 2<br>");//delete me
	    }
	  
	  out.println("<br>getEncounterID(): "+ NIM3_Encounter.getEncounterID());//delete me
		   
		String Enc_testChangeID = "0";

		try
		{
			out.println("<br>rec_bill_pro: "+request.getParameter("Rec_Bill_Pro"));//delete me
		    java.util.Date testObj = null ;
		    testObj = (PLCUtils.getSubDate(request.getParameter("Rec_Bill_Pro"))) ;
		    if ( !NIM3_Encounter.getRec_Bill_Pro().equals(testObj)  )
		    {
		         NIM3_Encounter.setRec_Bill_Pro( testObj,UserSecurityGroupID   );
		         out.println("<br>setRec_Bill_Pro: ");//delete me
		         Enc_testChangeID = "1";
		    }

		}
		catch(Exception e)
		{
		     //out.println("NIM3_Encounter Rec_Bill_Pro not set. this is ok-not an error");
		    
			out.println("rec bill pro error");//delete me
		}
		try
		{
			out.println("<br>amountbill: "+request.getParameter("AmountBilledByProvider"));//delete me
		    Double testObj = null ;
		    testObj = (PLCUtils.getDecimalDefault(request.getParameter("AmountBilledByProvider"))) ;
		    //if ( !NIM3_Encounter.getAmountBilledByProvider().equals(testObj)  )
		    //{
		         NIM3_Encounter.setAmountBilledByProvider( testObj,UserSecurityGroupID   );
		         out.println("<br>setAmountBilledByProvider:<br>"+ NIM3_Encounter.getAmountBilledByProvider() );//delete me
		         Enc_testChangeID = "1";
		    //}

		}
		
		catch(Exception e)
		{
		     //out.println("NIM3_Encounter AmountBilledByProvider not set. this is ok-not an error");
			out.println("amount error");//delete me
		}
		
		try
		{
			out.println("<br>HCFAinvoice: "+request.getParameter("HCFAinvoice"));//delete me
		    String testObj = null ;
		    testObj = request.getParameter("HCFAinvoice");
		    if ( !NIM3_Encounter.getAmountBilledByProvider().equals(testObj)  )
		    {
		         NIM3_Encounter.setHCFAinvoice( testObj,UserSecurityGroupID   );
		         out.println("<br>setHCFAinvoice:<br>"+ NIM3_Encounter.getHCFAinvoice() );//delete me
		         Enc_testChangeID = "1";
		    }

		}
		
		catch(Exception e)
		{
		     //out.println("NIM3_Encounter AmountBilledByProvider not set. this is ok-not an error");
			out.println("amount error");//delete me
		}
		
		
		//removed doc upload script
		
		 boolean isPassed=true;
	      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
	      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
	      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
		
			bltNIM3_CaseAccount myCase = new bltNIM3_CaseAccount(myRef.getCaseID());
			String myFileSS = "";
			String DocType = "";
		
		//declaration of Enumeration
	    
			
		// If an edit, update information; if new, append information
		
		if (Enc_testChangeID.equalsIgnoreCase("1"))
		{

			NIM3_Encounter.setUniqueModifyDate(PLCUtils.getNowDate(false));
			String UserLogonDescription = "n/a";
			if (pageControllerHash.containsKey("UserLogonDescription"))
			{
			    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
			}
		    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
		    {
			    NIM3_Encounter.setUniqueModifyComments(UserLogonDescription);
			    try
			    {
					//tempAN +="\n[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nRecord Edit";
					//NIM3_Encounter.setAuditNotes(tempAN);
			        NIM3_Encounter.commitData();
			        out.println("<script type=\"text/javascript\">window.close()</script>");
			       
			    }
			    catch (Exception e55)
			    {
			        //errorRouteMe = true;
			    }
		    }
		}
		
		
		
		

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
      
