<%@page contentType="text/html" language="java" import="java.text.DecimalFormat,com.winstaff.*,java.util.Calendar,java.text.SimpleDateFormat,java.util.Calendar,java.sql.ResultSet" %>
<!DOCTYPE HTML>
<%@ include file="../generic/CheckLogin.jsp" %>
<html>
<head>
<meta charset="UTF-8">
<title>Tysogram</title>
</head>

<body>

<%=request.getParameter("m")%><br>
<%=request.getParameter("m_cpt")%><br>
<%=request.getParameter("q")%><br>
<%=request.getParameter("c")%><br>

<%

if(request.getParameter("c").equals("4955")){
	//Set first encounter
	bltNIM3_Service NIM3_Service = new bltNIM3_Service();
	NIM3_Service.setEncounterID(new Integer(request.getParameter("eID")));
	NIM3_Service.setCPT(request.getParameter("m_cpt"));
	NIM3_Service.setCPTModifier("TC");
	NIM3_Service.setCPTBodyPart(request.getParameter("m"));
	NIM3_Service.setCPTText("Scan Only");
	NIM3_Service.setCPTQty(new Integer(request.getParameter("q")));
	NIM3_Service.setServiceStatusID(1);
	NIM3_Service.commitData();
	
	//Set 2nd encounter
	bltNIM3_Encounter NIM3_Encounter = new bltNIM3_Encounter();
	NIM3_Encounter.setReferralID(new Integer(request.getParameter("rID")));
	NIM3_Encounter.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_Encounter.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_Encounter.setAttendingPhysicianID(new Integer(52));
	NIM3_Encounter.setUniqueModifyComments("Created by Tysogram");
	NIM3_Encounter.setEncounterStatusID(new Integer(1));
	NIM3_Encounter.setSeeNetDev_SelectedPracticeID(9004);
	NIM3_Encounter.setSentTo_ReqRec_Adj(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adj(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentToAdj(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_NCM(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_NCM(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_NCM(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_RefDr(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_RefDr(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentToRefDr(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentToAdm(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm2(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm2(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_Adm2(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm3(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm3(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_Adm3(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm4(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm4(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_Adm4(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_IC(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.commitData();
	NIM3_Encounter.setScanPass("SP" +  NIM3_Encounter.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
	NIM3_Encounter.commitData();
	
	NIM3_Service = new bltNIM3_Service();
	NIM3_Service.setEncounterID(NIM3_Encounter.getUniqueID());
	NIM3_Service.setCPT(request.getParameter("m_cpt"));
	NIM3_Service.setCPTModifier("26");
	NIM3_Service.setCPTBodyPart(request.getParameter("m"));
	NIM3_Service.setCPTText("Read Only");
	NIM3_Service.setCPTQty(1);
	NIM3_Service.setServiceStatusID(1);
	NIM3_Service.commitData();
	
	NIM3_Service = new bltNIM3_Service();
	NIM3_Service.setEncounterID(NIM3_Encounter.getUniqueID());
	NIM3_Service.setCPT("73040");
	NIM3_Service.setCPTModifier("26");
	NIM3_Service.setCPTBodyPart("X-Ray Guidance - Prof (26) only");
	NIM3_Service.setCPTText("Read Only");
	NIM3_Service.setCPTQty(1);
	NIM3_Service.setServiceStatusID(1);
	NIM3_Service.commitData();
	
	NIM3_Service = new bltNIM3_Service();
	NIM3_Service.setEncounterID(NIM3_Encounter.getUniqueID());
	NIM3_Service.setCPT("23350");
	NIM3_Service.setCPTBodyPart("Injection");
	NIM3_Service.setCPTQty(1);
	NIM3_Service.setServiceStatusID(1);
	NIM3_Service.commitData();
	
	//Set 3rd encounter
	NIM3_Encounter = new bltNIM3_Encounter();
	NIM3_Encounter.setReferralID(new Integer(request.getParameter("rID")));
	NIM3_Encounter.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_Encounter.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_Encounter.setAttendingPhysicianID(new Integer(52));
	NIM3_Encounter.setUniqueModifyComments("Created by Tysogram");
	NIM3_Encounter.setEncounterStatusID(new Integer(1));
	NIM3_Encounter.setSeeNetDev_SelectedPracticeID(4954);
	NIM3_Encounter.setSentTo_ReqRec_Adj(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adj(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentToAdj(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_NCM(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_NCM(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_NCM(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_RefDr(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_RefDr(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentToRefDr(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentToAdm(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm2(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm2(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_Adm2(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm3(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm3(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_Adm3(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm4(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm4(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_Adm4(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_IC(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.commitData();
	NIM3_Encounter.setScanPass("SP" +  NIM3_Encounter.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
	NIM3_Encounter.commitData();
	
	NIM3_Service = new bltNIM3_Service();
	NIM3_Service.setEncounterID(NIM3_Encounter.getUniqueID());
	NIM3_Service.setCPT("73040");
	NIM3_Service.setCPTModifier("TC");
	NIM3_Service.setCPTBodyPart("X-Ray Guidance - Tech (TC) only");
	NIM3_Service.setCPTText("Tech Only");
	NIM3_Service.setCPTQty(1);
	NIM3_Service.setServiceStatusID(1);
	NIM3_Service.commitData();
}

else if(request.getParameter("c").equals("4169")){

	//Set first encounter
	bltNIM3_Service NIM3_Service = new bltNIM3_Service();
	NIM3_Service.setEncounterID(new Integer(request.getParameter("eID")));
	NIM3_Service.setCPT(request.getParameter("m_cpt"));
	NIM3_Service.setCPTModifier("TC");
	NIM3_Service.setCPTBodyPart(request.getParameter("m"));
	NIM3_Service.setCPTText("Scan Only");
	NIM3_Service.setCPTQty(new Integer(request.getParameter("q")));
	NIM3_Service.setServiceStatusID(1);
	NIM3_Service.commitData();
	
	//Set 2nd encounter
	bltNIM3_Encounter NIM3_Encounter = new bltNIM3_Encounter();
	NIM3_Encounter.setReferralID(new Integer(request.getParameter("rID")));
	NIM3_Encounter.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_Encounter.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_Encounter.setAttendingPhysicianID(new Integer(52));
	NIM3_Encounter.setUniqueModifyComments("Created by Tysogram");
	NIM3_Encounter.setEncounterStatusID(new Integer(1));
	NIM3_Encounter.setSeeNetDev_SelectedPracticeID(9004);
	NIM3_Encounter.setSentTo_ReqRec_Adj(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adj(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentToAdj(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_NCM(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_NCM(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_NCM(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_RefDr(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_RefDr(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentToRefDr(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentToAdm(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm2(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm2(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_Adm2(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm3(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm3(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_Adm3(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm4(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm4(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_Adm4(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_IC(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.commitData();
	NIM3_Encounter.setScanPass("SP" +  NIM3_Encounter.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
	NIM3_Encounter.commitData();
	
	NIM3_Service = new bltNIM3_Service();
	NIM3_Service.setEncounterID(NIM3_Encounter.getUniqueID());
	NIM3_Service.setCPT(request.getParameter("m_cpt"));
	NIM3_Service.setCPTModifier("26");
	NIM3_Service.setCPTBodyPart(request.getParameter("m"));
	NIM3_Service.setCPTText("Read Only");
	NIM3_Service.setCPTQty(1);
	NIM3_Service.setServiceStatusID(1);
	NIM3_Service.commitData();
	
	//Set 3rd encounter
	NIM3_Encounter = new bltNIM3_Encounter();
	NIM3_Encounter.setReferralID(new Integer(request.getParameter("rID")));
	NIM3_Encounter.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_Encounter.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_Encounter.setAttendingPhysicianID(new Integer(52));
	NIM3_Encounter.setUniqueModifyComments("Created by Tysogram");
	NIM3_Encounter.setEncounterStatusID(new Integer(1));
	NIM3_Encounter.setSentTo_ReqRec_Adj(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adj(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentToAdj(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_NCM(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_NCM(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_NCM(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_RefDr(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_RefDr(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentToRefDr(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentToAdm(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm2(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm2(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_Adm2(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm3(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm3(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_Adm3(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_ReqRec_Adm4(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_Adm4(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_RP_Adm4(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.setSentTo_SP_IC(PLCUtils.getSubDate("NA"));
	NIM3_Encounter.commitData();
	NIM3_Encounter.setScanPass("SP" +  NIM3_Encounter.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
	
	NIM3_Encounter.setSeeNetDev_Waiting(new Integer(1));
	NIM3_Encounter.setSeeNetDev_Flagged(new Integer(1));
	NIM3_Encounter.setSeeNetDev_ReqSentToNetDev(PLCUtils.getNowDate(true));
	NIM3_Encounter.setSeeNetDev_ReqSentToNetDev_UserID(CurrentUserAccount.getUserID());
	
	NIM3_Encounter.commitData();
	
	NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(new Integer(request.getParameter("eID")),"Word");
	
	bltNIM3_CommTrack working_bltNIM3_CommTrack = new bltNIM3_CommTrack();
	working_bltNIM3_CommTrack.setEncounterID(NIM3_Encounter.getUniqueID());
	working_bltNIM3_CommTrack.setReferralID(new Integer(request.getParameter("rID")));
	working_bltNIM3_CommTrack.setCaseID(myEO2.getNIM3_CaseAccount().getCaseID());
	working_bltNIM3_CommTrack.setMessageName(CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName());
	working_bltNIM3_CommTrack.setMessageCompany("NIM");
	working_bltNIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
	working_bltNIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
	working_bltNIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
	working_bltNIM3_CommTrack.setUniqueModifyComments(CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName());
	working_bltNIM3_CommTrack.setMessageText("Please add Trinity Bettendorf - IA OON");
	working_bltNIM3_CommTrack.setCommTypeID(new Integer(4));    //See ND Typ
	working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(0));  // keep open

	emailType_V3 myEM = new emailType_V3();
	myEM.setTo("netdev.managers@nextimagemedical.com");
	myEM.setFrom("server@nextimagemedical.com");
	myEM.setSubject("See NetDev Request: SP: " + myEO2.getNIM3_Encounter().getScanPass() + "[" + myEO2.getNIM3_CaseAccount().getPatientLastName() + "]");
	myEM.setBody("Please add Trinity Bettendorf - IA");
	myEM.isSendMail();
	working_bltNIM3_CommTrack.commitData();
	

	
	NIM3_Service = new bltNIM3_Service();
	NIM3_Service.setEncounterID(NIM3_Encounter.getUniqueID());
	NIM3_Service.setCPT("77002");
	NIM3_Service.setCPTBodyPart("Fluoroscopic Guidance");
	NIM3_Service.setCPTQty(1);
	NIM3_Service.setServiceStatusID(1);
	NIM3_Service.commitData();

	NIM3_Service = new bltNIM3_Service();
	NIM3_Service.setEncounterID(NIM3_Encounter.getUniqueID());
	NIM3_Service.setCPT("23730");
	NIM3_Service.setCPTBodyPart("Injection");
	NIM3_Service.setCPTQty(1);
	NIM3_Service.setServiceStatusID(1);
	NIM3_Service.commitData();




}%>



</body>
</html>