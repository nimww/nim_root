<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<!DOCTYPE html>
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NextImage Medical | Quick order submit</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<style>
td{
	vertical-align: top;
}
</style>
<script type="text/javascript">
	  
	  function validateForm()
	  {
		  var claimsNumber=document.forms["at_intake"]["CaseClaimNumber1"].value;
		  var patientDOB=document.forms["at_intake"]["PatientDOB"].value;
		  var patientFirst=document.forms["at_intake"]["PatientFirstName"].value;
		  var patientLast=document.forms["at_intake"]["PatientLastName"].value;
		  
		  if ((claimsNumber==null || claimsNumber=="") )
		    {
			  if ((patientDOB==null || patientDOB=="") )
			    {
				    alert("Please enter claim number or patient DOB.");
				    return false;
			    }
		    }
		  
		  if ((patientFirst==null || patientFirst=="") )
		    {
			    alert("Please enter patient first name.");
			    return false;
		    }	  
		  
		  if ((patientLast==null || patientLast=="") )
		    {
			    alert("Please enter patient last name.");
			    return false;
		    }

	  }
	  </script>
</head>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler) 
    {
        accessValid = true;
    }
    else if (isAdjuster) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
	}
    else if (isProvider) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
	}
	
	
  //page security
  if (accessValid)
  {
	  
	  %>

	  <% 
	  
      //java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      //java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      //pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	
	if (isAdjuster||isAdjuster2||isAdjuster3||isProvider)
	  {
		  %>
<table width="<%=MasterTableWidth%>" border="0" cellspacing="0" cellpadding="10">
    <tr>
        <td>
            <table border="0"  cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2" align="left" bordercolor="#002F63">           
                    <%
                    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
                    String ip = request.getRemoteAddr();
                    String host = request.getRemoteHost();
                    
                    
                    //fields
                    %>
                    <p class=big2>Online Imaging Referral</p>
                  <p style="margin: 0;">Please complete this Quick Order Upload page if you are uploading support documents:</p>
                  <ul style="margin: 0;">
                  <li>Insurance Carrier/Adjuster</li>
                  <li>Patient (name, DOB, DOI, address, phone numbers)</li>
                  <li>MD (name, phone, fax and RX if you have it, if not we will get it)</li>
                  <li>Test Requested (along with rule out)</li>
                  </ul>
                  <p>Otherwise, please switch to <a href="Payer_submit1.jsp">Manual Order Entry</a>.</p>
                  <p>Since you are already signed in, we have your information as follows:</p>
                  <p class="tdBase">If this information has changed please <a href="tUserAccount_form_settings.jsp">update it here</a>.</p>
                  <p class=tdBase>Name: <strong><%=CurrentUserAccount.getContactFirstName()%>&nbsp;<%=CurrentUserAccount.getContactLastName()%> </strong> @  <strong><%=new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID()).getPayerName()%></strong><br>
                    Phone: <strong><%=CurrentUserAccount.getContactPhone()%></strong><br>Fax: <strong><%=CurrentUserAccount.getContactFax()%></strong><br>Email: <strong><%=CurrentUserAccount.getContactEmail()%></strong></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<form name="at_intake" action="Payer_submit1_sub1.jsp" onsubmit="return validateForm()">
<div id="amer-form-container">

	<div id="amer-adj-link">
    	Please link me as the <label><select name="LinkType" id="LinkType">
              <%if (isAdjuster){%><option value="Adjuster" selected>Adjuster</option><%}%>
              <%if (isAdjuster){%> <option value="NCM">Nurse Case Manager</option>><%}%>
              <%if (isProvider){%> <option value="Dr">Referring Physician</option>><%}%>
              <option value="Admin" <%if (new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID()).getPayerName().contains("US Healthworks")){out.print("selected"); } %>>Case/Referral Admin</option>
		</select></label>
    </div>
    
	<%if (isAdjuster3||isProvider3)
{String myPayerUsersSelect = NIMUtils.getListOfValidUserByPayer_HTMLSelect(CurrentUserAccount.getPayerID(),0);%>
    <div style="display:none;">
    <p>As a Branch/Office Admin, you can link other users at your location to this case. <br><span class="instructionsBig1">*if users are not listed here, you may enter them at the end of the form.</span></p>
    <p>Please link <select name="Link2">      <option value="0" selected>No Additional User Links</option>
    <%=myPayerUsersSelect%></select> as the
    <label>
    <select name="LinkType2" id="LinkType2">
    <%if (isAdjuster){%><option value="Adjuster" selected>Adjuster</option><%}%>
    <%if (isAdjuster){%> <option value="NCM">Nurse Case Manager</option>><%}%>
    <%if (isProvider){%> <option value="Dr">Referring Physician</option>><%}%>
    <option value="Admin">Case/Referral Admin</option>
    </select>
    </label><br>
    Please link <select name="Link3">      <option value="0" selected>No Additional User Links</option>
    <%=myPayerUsersSelect%></select> as the
    <label>
    <select name="LinkType3" id="LinkType3">
    <%if (isAdjuster){%><option value="Adjuster" selected>Adjuster</option><%}%>
    <%if (isAdjuster){%> <option value="NCM">Nurse Case Manager</option>><%}%>
    <%if (isProvider){%> <option value="Dr">Referring Physician</option>><%}%>
    <option value="Admin">Case/Referral Admin</option>
    </select>
    </label>
    </p>
    </div>
<%}%>
      
    <div id="md-info" class="amer-two-col">
    	<div class="left">
           	<strong>Case Information</strong><br>
            Please enter claim number and/or DOB.
        </div>
        <div class="right">
         	<p><strong>Claim #:</strong></p><br><input type="text" id="clientClaimNum" name="CaseClaimNumber1" class="md-info-input-text" class="claimantInfo" autocomplete="off"><br>
        	<p><strong>DOB:</strong></p><br><input type="text" id="patientDOB" name="PatientDOB" class="md-info-input-text" class="claimantInfo" autocomplete="off">
        	
        </div>
    </div>
    <div id="iw-info" class="amer-two-col">
    	<div class="left">
            <strong>Patient Information:</strong>
        </div>
        <div class="right">
        	<p><strong>First Name: </strong></p><input type="text" id="PatientFirstName" name="PatientFirstName" class="md-info-input-text" class="claimantInfo" autocomplete="off"><br>
        	<p><strong>Last Name: </strong></p><input type="text" id="PatientLastName" name="PatientLastName" class="md-info-input-text" class="claimantInfo" autocomplete="off">
        	
        </div>
        <div style="display:none;">
            <input type="text" id="patientAddress1" name="PatientAddress1" readonly class="claimantInfo">
            <input type="text" id="patientAddress2" name="PatientAddress2" readonly class="claimantInfo">
            <input type="text" id="patientCity" name="PatientCity" readonly class="claimantInfo">
            <input type="text" id="patientState" name="PatientState" readonly class="claimantInfo"> 
            <input type="text" id="patientZip" name="PatientZIP" readonly class="claimantInfo">
            <input type="text" id="patientPhone" name="PatientHomePhone" readonly class="claimantInfo">
            <input name="PatientCellPhone" type="text" >
            
            <input type="text" id="adjusterFirstName" name="AdjusterFirstName" readonly class="claimantInfo"> 
            <input type="text" id="adjusterLastName" name="AdjusterLastName" readonly class="claimantInfo">
            <input type="text" id="casemanagerFirstName" name="casemanagerFirstName" readonly class="claimantInfo"> 
            <input type="text" id="casemanagerLastName" name="casemanagerLastName" readonly class="claimantInfo">
            
            <input type="text" id="patientDOI" name="DateOfInjury" readonly class="claimantInfo">
            <input name="patientGender" id="patientGender">
            
            <input name="refName" type=text size="20">
         	<input name="refEmail" type=text size="20">
            <input name="PayerPreAuthorizationConfirmation" type=text>
            <input name="PatientStateID" id="PatientStateID">
            <input name="PatientSSN" id="PatientSSN">

            <input type="text" id="employer" name="EmployerName" readonly class="claimantInfo">
            <input name="EmployerPhone" type="text">
            <input name="employerAddress" type="text">
            <input name="employerCity"  type="text">
            <input name="employerStateID">
            <input name="employerZIP" type="text">
			<input name="AdjusterCompany" type="text">
			<input name="AdjusterCity"  type="text">
			<input name="AdjusterPhone" type="text">
			<input name="AdjusterFax" type="text">
			<input name="AdjusterEmail" type="text">  
            <input type="text" name="ReferringPhysicianName" class="md-info-input-text" id="mdName"> 
            <input type="text" name="ReferringPhysicianPhone" class="md-info-input-text">
            <input type="text" name="ReferringPhysicianFax" class="md-info-input-text">
            <input type="text" name="ReferringPhysicianEmail" class="md-info-input-text"> 
            <textarea name="CPTText" cols="60" rows="4" class="titleSub2" onKeyDown="textAreaStop(this,500)"></textarea>        
        </div>
    </div>
    <div id="special-notes" class="amer-two-col">
    	<div class="left">
        	<p><strong>Special Notes/Instructions:</strong><br>
			(eg. Stat, Aged Report needed, Open MRI required, claimant hand carry CD/film, etc.)</p>
        	
        </div>
        <div class="right">
        	<textarea name="MessageText" cols="70" rows="10" class="titleSub2" onKeyDown="textAreaStop(this,1900)"></textarea>
        </div>
    </div>
    <div id="upload-doc" class="amer-two-col">
    	<div class="left">
        	<p><strong>Upload Documents:</strong><br>
			</p>
        </div>
        <div class="right">
        	Please upload any relevant documents such as authorizations, scripts, orders, etc. after you proceed to next step.
        </div>
    </div>
    <input type="submit" class="amer-input-button" value="Proceed to upload" name="submit" onClick="this.disable=true;"><input type="button" class="amer-input-button" value="Cancel" onclick="document.location='Payer_Home.jsp';" >
    
</div>
</form> 
          <%
	  }
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>