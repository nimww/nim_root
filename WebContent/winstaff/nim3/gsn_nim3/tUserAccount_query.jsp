<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tCompanyUserAccountLU_main_UserAccount_CompanyID.jsp
    Created on Jan/28/2003
    Type: n-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../../generic/CheckLogin.jsp" %>

<%@ include file="../../generic/generalDisplay.jsp" %>
<link href="../ui_200/style_sched.css" rel="stylesheet" type="text/css" />
<body onLoad="document.selfForm.LogonUserName.focus()">

    <table width=100% height="100%" border=1 cellpadding=30 cellspacing=0 bordercolor="#666666" >
    <tr><td width=50 background="images/nim_v3_bg.jpg">&nbsp;</td>
    
      <td valign="top">

    <p><a href="../Maint_Home.jsp">Rerturn to Main Home</a></p>
    <p><span class=title>NIMv3 - User Account Maintenance </span><br>
      <%
//initial declaration of list class and parentID
//    Integer        iCompanyID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("user1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
//    if (pageControllerHash.containsKey("iCompanyID")) 
    {
//        iCompanyID        =    (Integer)pageControllerHash.get("iCompanyID");
        accessValid = true;
    }
  //page security
  if (accessValid&& isScheduler2 )
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

      java.text.SimpleDateFormat displayDateLongSDF = new java.text.SimpleDateFormat("MMMM dd yyyy");
    pageControllerHash.put("sParentReturnPage","tUserAccount_query.jsp");
    pageControllerHash.remove("iLookupID");
    pageControllerHash.remove("iUserID");
    session.setAttribute("pageControllerHash",pageControllerHash);
%>
      
      
    </p>
    <table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p class=title>Search:</p>
<%
try
{

String myUserID = "";
String myLogonUserName = "";
String myContactLastName = "";
String myContactPhone = "";
String myContactEmail = "";
String myPLCID = "0";
String myAccountType = "";
String myReferenceID = "";
String myContactZIP = "";
String orderBy = "firstname";
int startID = 0;
int maxResults = 50;

boolean firstDisplay = false;

try
{
maxResults = Integer.parseInt(request.getParameter("maxResults"));
startID = Integer.parseInt(request.getParameter("startID"));
myUserID = request.getParameter("UserID");
myLogonUserName = request.getParameter("LogonUserName");
myContactLastName = request.getParameter("ContactLastName");
myContactPhone = request.getParameter("ContactPhone");
myContactEmail = request.getParameter("ContactEmail");
myPLCID = request.getParameter("PLCID");
myAccountType = request.getParameter("AccountType");
myReferenceID = request.getParameter("ReferenceID");
myContactZIP = request.getParameter("ContactZIP");
orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myUserID = "";
myLogonUserName = "";
myContactLastName = "";
myContactPhone = "";
myContactEmail = "";
myPLCID = "0";
myAccountType = "";
myReferenceID = "";
myContactZIP = "";
orderBy = "UserID";
}
if (orderBy == null)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myUserID = "";
myLogonUserName = "";
myContactLastName = "";
myContactPhone = "";
myContactEmail = "";
myPLCID = "0";
myAccountType = "";
myReferenceID = "";
myContactZIP = "";
orderBy = "UserID";
}


if (firstDisplay)
{
%>



<table width = 735 cellpadding=2 cellspacing=2>
<tr>
<td width=10>&nbsp;</td>

<td>
<form name="selfForm" method="POST" action="tUserAccount_query.jsp">
  <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#003333">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
        <tr>
         <td nowrap="nowrap" class=tdHeaderAlt>
         ID         </td>
         <td>
         <input name="UserID" type=text class="titleSub1">         </td>
         <td rowspan="10"><img src="../images/v2-peopleworld1.gif" /></td>
        </tr>
        <tr>
         <td nowrap="nowrap" class=tdHeaderAlt>
         Logon Name         </td>
         <td>
         <input name="LogonUserName" type=text class="titleSub1">         </td>
         </tr>
        <tr>
         <td nowrap="nowrap" class=tdHeaderAlt>
         Last Name         </td>
         <td>
         <input name="ContactLastName" type=text class="titleSub1">         </td>
         </tr>
        <tr>
         <td nowrap="nowrap" class=tdHeaderAlt>
         Phone         </td>
         <td>
         <input name="ContactPhone" type=text class="titleSub1">         </td>
         </tr>
        <tr>
         <td nowrap="nowrap" class=tdHeaderAlt>
         Email         </td>
         <td>
         <input type=text name="ContactEmail">         </td>
         </tr>
        <tr>
         <td nowrap="nowrap" class=tdHeaderAlt>
         PL Code         </td>
         <td>
              <select name="PLCID" class="inputButton_md_Stop" >
                <option value="0">0</option>
              
              </select>         </td>
         </tr>
        <tr>
         <td nowrap="nowrap" class=tdHeaderAlt>
         Type         </td>
         <td>
         <input name="AccountType" type=text class="titleSub1" value="">         </td>
         </tr>
        <tr>
         <td nowrap="nowrap" class=tdHeaderAlt>
         Ref ID         </td>
         <td>
         <input name="ReferenceID" type=text class="titleSub1">         </td>
         </tr>
        <tr>
         <td nowrap="nowrap" class=tdHeaderAlt>
         Postal Code         </td>
         <td>
         <input name="ContactZIP" type=text class="inputButton_md_Stop">         </td>
         </tr>
            <tr>
            <td nowrap="nowrap" class=tdHeaderAlt><p>Sort by:</p></td>
            <td> 
              <select name=orderBy class="titleSub1">
                <option value="UserID">ID</option>
                <option value="LogonUserName">Logon Name</option>
                <option value="ContactLastName">Last Name</option>
                <option value="ContactPhone">Phone</option>
                <option value="ContactEmail">Email</option>
                <option value="PLCID">PL Code</option>
                <option value="AccountType">Type</option>
                <option value="ReferenceID">Ref ID</option>
                <option value="ContactZIP">Postal Code</option>
              </select>            </td>
            </tr>
          <tr bgcolor="#CCCCCC"> 
            <td>&nbsp;</td>
            <td> 
              <input type=hidden name="startID" value=0>
              <input type=hidden name="maxResults" value=50>
              <input name="Submit2" type="submit" class="titleBlue" value="Submit">            </td>
            <td>&nbsp;</td>
          </tr>
        </table>
</td>
    </tr>
  </table>
  <p>&nbsp;</p>
  </form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p> 
  <%
}
else
{


//String myWhere = "where (";
//String myWhere = "where  (tCompanyUserAccountLU.CompanyID="+iCompanyID+") and (tCompanyUserAccountLU.UserID=tuserAccount.UserID) and (tUserAccount.UserID>0";
String myWhere = "where  (tCompanyUserAccountLU.CompanyID>=0) and (tCompanyUserAccountLU.UserID=tuserAccount.UserID) and (tUserAccount.UserID>0";
myWhere = "where  (tUserAccount.UserID>0 ";
boolean theFirst = false;

try
{
if (!myUserID.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "tUserAccount.UserID = '" + myUserID +"'";
theFirst = false;
}
if (!myLogonUserName.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "UPPER(LogonUserName) LIKE UPPER('%" + myLogonUserName +"%')";
theFirst = false;
}
if (!myContactLastName.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "UPPER(ContactLastName) LIKE UPPER('%" + DataControlUtils.fixApostrophe(myContactLastName) +"%')";
theFirst = false;
}
if (!myContactPhone.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "ContactPhone LIKE '%" + myContactPhone +"%'";
theFirst = false;
}
if (!myContactEmail.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "UPPER(ContactEmail) LIKE UPPER('%" + myContactEmail +"%')";
theFirst = false;
}
if (!myPLCID.equalsIgnoreCase("0"))
{
if (!myPLCID.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "PLCID = " + myPLCID +"";
theFirst = false;
}
}
if (!myAccountType.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "UPPER(AccountType) LIKE UPPER('%" + myAccountType +"%')";
theFirst = false;
}
if (!myReferenceID.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "ReferenceID = '" + myReferenceID +"'";
theFirst = false;
}
if (!myContactZIP.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "ContactZIP LIKE '%" + myContactZIP +"%'";
theFirst = false;
}
myWhere += ")";

//System.out.println(myWhere);
if (theFirst||myWhere.equalsIgnoreCase(")"))
{
myWhere = "";
}


}
catch(Exception e)
{
out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
String mySQL22 = "select  tNIM3_PayerMaster.payername, tUserAccount.Status, tUserAccount.CompanyName, tUserAccount.UserID,tUserAccount.LogonUserName,tUserAccount.ContactLastName,tUserAccount.ContactFirstName,tUserAccount.ContactPhone,tUserAccount.ContactEmail,tUserAccount.PLCID,tUserAccount.AccountType,tUserAccount.ReferenceID,tUserAccount.ContactZIP, tCompanyUserAccountLU.UserID, tCompanyUserAccountLU.LookupID from tCompanyUserAccountLU,tUserAccount LEFT JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tUserAccount.payerid " + myWhere + " order by tUserAccount." + orderBy;
mySQL22 = "select tNIM3_PayerMaster.payername,tUserAccount.* from tUserAccount LEFT JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tUserAccount.payerid  " + myWhere + "  AND tUserAccount.AccountType in ('AdjusterID','AdjusterID2','AdjusterID3','ICID','PhysicianID','PhysicianID2','PhysicianID3', 'SchedulerID','SchedulerID2','SchedulerID3','GenAdminID','ReporterID')  order by " + orderBy;
//out.print(mySQL22);
myRS = mySS.executeStatement(mySQL22);

}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
cnt++;
if (cnt>=startID&&cnt<=startID+maxResults)
{
cnt2++;

String myClass = "tdBase";
if (cnt2%2==0)
{
	myClass = "tdBaseAlt";
}
	String sStatus = myRS.getString("Status");
	



String sUserID = myRS.getString("UserID");
myMainTable +="<tr class=\"" + myClass + "\">";
myMainTable +="<td>"+cnt+"</td>";
myMainTable +="<td nowrap><a class=linkBase  href = 'tUserAccount_form_authorize.jsp?EDIT=edit&EDITID="+sUserID+"'>EDIT</a>";
//myMainTable +="<a class=linkBase  href = 'tCompanyUserAccountLU_main_LU_CompanyID_form_authorize.jsp?EDIT=del&EDITID="+sUserID+"&KM=p'><img border=0 src='ui_"+thePLCID+"/icons/remove_CompanyID.gif'></a> ";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=sUserID+"&nbsp;";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("LogonUserName")+"&nbsp;<br><strong>" + myRS.getString("payername") +"</strong>";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +="<strong>"+myRS.getString("ContactLastName")+"</strong><br>"+myRS.getString("ContactFirstName");
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("ContactPhone")+"&nbsp;";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("ContactEmail")+"&nbsp;";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +="N/A";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("AccountType");
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("ReferenceID")+"&nbsp;";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("ContactZIP")+"&nbsp;";
myMainTable +="</td>";
myMainTable +="</tr>";
}
   }
mySS.closeAll();
endCount = cnt;



if (startID>=endCount)
{
startID = endCount-maxResults;

}

if (maxResults<=0)
{
maxResults=10;
}


if (startID<=0)
{
startID=0;
}




%>
  <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</p>
<table width=100% border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
<tr>
<td width=10>&nbsp;</td>
<td>
<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
<tr>
<td>

<form name="selfForm" method="POST" action="tUserAccount_query.jsp">
  <table border=1 bordercolor="CCCCCC" cellspacing=1 width='100%' cellpadding=2>
    <tr > 
      <td colspan=3 ></td>
      <td colspan=1 align=right>
        <%if (startID>0)
{%><a href="tUserAccount_query.jsp?startID=<%=(startID-maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&UserID=<%=myUserID%>&LogonUserName=<%=myLogonUserName%>&ContactLastName=<%=myContactLastName%>&ContactPhone=<%=myContactPhone%>&ContactEmail=<%=myContactEmail%>&PLCID=<%=myPLCID%>&AccountType=<%=myAccountType%>&ReferenceID=<%=myReferenceID%>&ContactZIP=<%=myContactZIP%>"><< previous <%=maxResults%></a> 
        <%
}
else
{
%><p>&nbsp;</p><%
}
%></td>
      <td colspan=1 class=tdBase> 
        <%
if ((startID+maxResults)<endCount)
{
%>
<a href="tUserAccount_query.jsp?startID=<%=(startID+maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&UserID=<%=myUserID%>&LogonUserName=<%=myLogonUserName%>&ContactLastName=<%=myContactLastName%>&ContactPhone=<%=myContactPhone%>&ContactEmail=<%=myContactEmail%>&PLCID=<%=myPLCID%>&AccountType=<%=myAccountType%>&ReferenceID=<%=myReferenceID%>&ContactZIP=<%=myContactZIP%>"> next >> <%=maxResults%></a>         
        <%
}
else
{
%><p>&nbsp;</p><%
}


}
catch(Exception e)
{
out.println("PrevNext:"+e);
}




try{

%>
      </td>
      <td colspan=1 class=tdHeader nowrap> 
<input type=hidden name=maxResults value=<%=maxResults%> >
<input type=hidden name=startID value=0 ><p>Sort:
<select name=orderBy>
      <option value=UserID <%if (orderBy.equalsIgnoreCase("UserID")){out.println(" selected");}%> >ID</option>
      <option value=LogonUserName <%if (orderBy.equalsIgnoreCase("LogonUserName")){out.println(" selected");}%> >Logon Name</option>
      <option value=ContactLastName <%if (orderBy.equalsIgnoreCase("ContactLastName")){out.println(" selected");}%> >Last Name</option>
      <option value=ContactPhone <%if (orderBy.equalsIgnoreCase("ContactPhone")){out.println(" selected");}%> >Phone</option>
      <option value=ContactEmail <%if (orderBy.equalsIgnoreCase("ContactEmail")){out.println(" selected");}%> >Email</option>
      <option value=PLCID <%if (orderBy.equalsIgnoreCase("PLCID")){out.println(" selected");}%> >PL Code</option>
      <option value=AccountType <%if (orderBy.equalsIgnoreCase("AccountType")){out.println(" selected");}%> >Type</option>
      <option value=ReferenceID <%if (orderBy.equalsIgnoreCase("ReferenceID")){out.println(" selected");}%> >Ref ID</option>
      <option value=ContactZIP <%if (orderBy.equalsIgnoreCase("ContactZIP")){out.println(" selected");}%> >Postal Code</option>
</select></p>
      </td> 
      <td class=tdHeader colspan=1> 
        <p  align="center"> 
          <input type="Submit" name="Submit" value="Submit" align="middle">
        </p>
      </td>
    </tr>
    <tr class=tdHeader> 
      <td  colspan=1 width=50>&nbsp;      </td>
      <td  colspan=1 width=50>&nbsp;      </td>
      <td colspan=1> 
        <input type="text" name="UserID" value="<%=myUserID%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="LogonUserName" value="<%=myLogonUserName%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactLastName" value="<%=myContactLastName%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactPhone" value="<%=myContactPhone%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactEmail" value="<%=myContactEmail%>" size="10">
      </td>
      <td colspan=1> 
              <select name="PLCID" class="inputButton_md_Stop">
                <option value="0">0</option>
              </select>
      </td>
      <td colspan=1> 
        <input type="text" name="AccountType" value="<%=myAccountType%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="ReferenceID" value="<%=myReferenceID%>" size="10">
      </td>
      <td colspan=1> 
        <input name="ContactZIP" type="text" class="inputButton_md_Stop" value="<%=myContactZIP%>" size="10">
      </td>
    </tr>
    <tr class=tdHeader> 
    <td>#</td>
    <td>Actions</td>
      <td colspan=1> 
        User ID
      </td>
      <td colspan=1> 
        Logon Name
      </td>
      <td colspan=1> 
        Last Name
      </td>
      <td colspan=1> 
        Phone
      </td>
      <td colspan=1> 
        Email
      </td>
      <td colspan=1> 
        PL Code
      </td>
      <td colspan=1> 
        Type
      </td>
      <td colspan=1> 
        Ref ID
      </td>
      <td colspan=1> 
        Postal Code
      </td>
    </tr>
    <%=myMainTable%>



</table> 


<%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>



  </table>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </td>
  </tr>
</table>

    <%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</td></tr></table>

</body>