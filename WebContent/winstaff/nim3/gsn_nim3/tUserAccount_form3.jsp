
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tUserAccount_form.jsp
    JSP AutoGen on Mar/25/2002
*/%><head>
<link href="../ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<%@ include file="../../generic/CheckLogin.jsp" %>

<%@ include file="../../generic/generalDisplay.jsp" %>
<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function confirmReset()
{
	if (confirm("Are you sure you want to Reset this Password?"))
	{
		modalPost('RESETPSWD', modalWin('UserAccount_ChangePassword_reset.jsp','Reset...','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));		
//window.open('UserAccount_ChangePassword_reset.jsp','PReset','status=yes,scrollbars=yes,resizable=yes,width=500,height=500');
	}

}

function confirmReset2()
{
	if (confirm("Are you sure you want to Reset & Email this Password?\n\nThis will IMMEDIATELY send an email to the user."))
	{
		modalPost('RESETPSWD', modalWin('UserAccount_ChangePassword_reset_em.jsp','Reset...','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));		
	}

}

function confirmChange()
{
	if (confirm("You must know the current password in order to change it\n\nAlso, new passwords must be 8 digits/characters long?"))
	{
		modalPost('RESETPSWD', modalWin('UserAccount_ChangePassword.jsp','Reset...','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));		
	}

}

//-->

</script>
<script language="JavaScript">
<!--
function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}
//-->
</script>
    <title>NIM v3 User Account Edit</title>
    </head>
    
    
<blockquote>&nbsp;</blockquote>
<table width=100% height="100%" border=1 cellpadding=30 cellspacing=0 bordercolor="#666666">
    <tr><td width=50 background="images/nim_v3_bg.jpg">&nbsp;</td>
    <td valign="top">
      <p><a href="tUserAccount_query.jsp" class="titleBlue">Return to Search</a> </p>
      <p><span class=title>Edit User Account</span><br>
        
        
        <%
//initial declaration of list class and parentID
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
      java.text.SimpleDateFormat dbdfFull = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    bltUserAccount        UserAccount        =    null;

    Integer        iUserID        =    null;
//    Integer        iCompanyID =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iUserID")) 
    {
        iUserID        =    (Integer)pageControllerHash.get("iUserID");
//        iCompanyID        =    (Integer)pageControllerHash.get("iCompanyID");
        accessValid = true;    }
  //page security
  if (accessValid&& isScheduler2 )
  {

    pageControllerHash.put("sLocalChildReturnPage","tUserAccount_form3.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID


    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        UserAccount        =    new    bltUserAccount(iUserID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        UserAccount        =    new    bltUserAccount();
    }

//fields
        %>
      </p>
      <form method=POST action="tUserAccount_form3_sub.jsp" name="tUserAccount_form1">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>        <p class=tdHeader><b>UniqueID</b>[<i>UserID</i>]<%=UserAccount.getUserID()%></p>
        <table border="0" cellpadding="5" cellspacing="0" bgcolor="#DBFFDB">
          <tr>
            <td><strong>Account Created: </strong></td>
            <td><span class="tdBase"><%=displayDateSDF.format(UserAccount.getUniqueCreateDate())%></span></td>
          </tr>
          <tr>
            <td><strong>Account Last Modified:</strong></td>
            <td><span class="tdBase"><%=displayDateSDF.format(UserAccount.getUniqueModifyDate())%></span></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><span class="tdBase"><%=(UserAccount.getUniqueModifyComments())%></span></td>
          </tr>
        </table>
        <%  String theClass ="tdBase";%>
            <%
		if (false)
		{
            if ( (UserAccount.isRequired("PLCID"))&&(!UserAccount.isComplete("PLCID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("PLCID",expiredDays))&&(UserAccount.isExpiredCheck("PLCID")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>User Interface ID:</b>&nbsp;<%=UserAccount.getPLCID()%>
                <input type=text name=PLCID value="<%=UserAccount.getPLCID()%>" ></p>

            <%
            if ( (UserAccount.isRequired("StartPage"))&&(!UserAccount.isComplete("StartPage")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("StartPage",expiredDays))&&(UserAccount.isExpiredCheck("StartPage")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>Start Page:</b>&nbsp;<input size=50 type=text name=StartPage value="<%=UserAccount.getStartPage() %>" onFocus="javascript:blur()"></p>
            <%
		}
            if ( (UserAccount.isRequired("AccountType"))&&(!UserAccount.isComplete("AccountType")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("AccountType",expiredDays))&&(UserAccount.isExpiredCheck("AccountType")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                <p class=<%=theClass%> ><b>AccountType:</b>&nbsp;<input type=text name=AccountType value="<%=UserAccount.getAccountType() %>" ></p>

            
        <p class=<%=theClass%> ><b>ReferenceID:</b>&nbsp; 
          <input name="ReferenceID" type=text disabled="disabled" onFocus="javascript:blur()" value=<%=UserAccount.getReferenceID()%> size="10" maxlength="10" readonly="readonly" >        
        </p>
        <p class=<%=theClass%> ><b>Payer/Group:</b>&nbsp; 
          <input name="PayerID" type=text value=<%=UserAccount.getPayerID()%> size="10" maxlength="10"  >     &nbsp; [<%= new bltNIM3_PayerMaster(UserAccount.getPayerID()).getPayerName()%>]   
        </p>

                <p class=<%=theClass%> ><b>AccessType</b><select name="AccessType" ><jsp:include page="../../generic/tUserAccessTypeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getAccessType()%>" /></jsp:include></select></p>


                <p>
              <%
            if ( (UserAccount.isRequired("Status"))&&(!UserAccount.isComplete("Status")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("Status",expiredDays))&&(UserAccount.isExpiredCheck("Status")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            </p>
            <p class=<%=theClass%> ><b>Status</b><select name="Status" ><jsp:include page="../../generic/tUserStatusLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getStatus()%>" /></jsp:include></select></p>

            <%
            if ( (UserAccount.isRequired("LogonUserName"))&&(!UserAccount.isComplete("LogonUserName")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("LogonUserName",expiredDays))&&(UserAccount.isExpiredCheck("LogonUserName")))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <table border="1" cellspacing="0" cellpadding="3">
              <tr class="tdHeaderAlt">
                <td>&nbsp;</td>
                <td>Reports</td>
                <td>Schedule Alert</td>
                <td>Other Notifications</td>
              </tr>
              <tr class="tdHeaderAlt">
                <td>Alert me when...</td>
                <td><select name="Comm_Report_LevelUser" id="AlertDays">
                  <jsp:include page="../../generic/tUAAlertsLILong.jsp" flush="true" >
                    <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Report_LevelUser()%>" />            
                  </jsp:include>
                </select></td>
                <td><select name="Comm_Alerts_LevelUser" id="AlertDays">
                  <jsp:include page="../../generic/tUAAlertsLILong.jsp" flush="true" >
                    <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Alerts_LevelUser()%>" />            
                  </jsp:include>
                </select></td>
                <td><select name="Comm_Alerts2_LevelUser" id="AlertDays">
                  <jsp:include page="../../generic/tUAAlertsLILong.jsp" flush="true" >
                    <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Alerts2_LevelUser()%>" />            
                  </jsp:include>
                </select></td>
              </tr>
              <% if (false){%>
              <tr class="tdHeaderAlt">
                <td>If I supervise anyone, alert me of their cases when...</td>
                <td><select name="Comm_Report_LevelManager" id="AlertDays">
                  <jsp:include page="../../generic/tUAAlertsLILong.jsp" flush="true" >
                    <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Report_LevelManager()%>" />            
                  </jsp:include>
                </select></td>
                <td><select name="Comm_Alerts_LevelManager" id="AlertDays">
                  <jsp:include page="../../generic/tUAAlertsLILong.jsp" flush="true" >
                    <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Alerts_LevelManager()%>" />            
                  </jsp:include>
                </select></td>
                <td><select name="Comm_Alerts2_LevelManager" id="AlertDays">
                  <jsp:include page="../../generic/tUAAlertsLILong.jsp" flush="true" >
                    <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Alerts2_LevelManager()%>" />            
                  </jsp:include>
                </select></td>
              </tr>
              <tr class="tdHeaderAlt">
                <td>If I manage an office, alert me of all cases when...</td>
                <td><select name="Comm_Report_LevelBranch" id="AlertDays">
                  <jsp:include page="../../generic/tUAAlertsLILong.jsp" flush="true" >
                    <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Report_LevelBranch()%>" />            
                  </jsp:include>
                </select></td>
                <td><select name="Comm_Alerts_LevelBranch" id="AlertDays">
                  <jsp:include page="../../generic/tUAAlertsLILong.jsp" flush="true" >
                    <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Alerts_LevelBranch()%>" />            
                  </jsp:include>
                </select></td>
                <td><select name="Comm_Alerts2_LevelBranch" id="AlertDays">
                  <jsp:include page="../../generic/tUAAlertsLILong.jsp" flush="true" >
                    <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Alerts2_LevelBranch()%>" />            
                  </jsp:include>
                </select></td>
              </tr>
              <% }%>
            </table>
          <p class=<%=theClass%> ><b>Logon User Name: </b>
            <input name="LogonUserName" type=text class="title" value="<%=UserAccount.getLogonUserName()%>" size="60">
        </p>

<table width="50%" border="1" cellspacing="0" cellpadding="2">
  <tr>
    <td nowrap="nowrap" class="tdHeaderAlt">Logon User Password:&nbsp;</td>
    <td align="center" valign="middle"><input name="changePassword" type="button" class="inputButton_md_Stop"  onClick="MM_callJS('confirmChange();')" value="Change..."></td>
    <td align="center" valign="middle"><input name="Submit2" type="button" class="inputButton_md_Stop"  onClick="MM_callJS('confirmReset();')" value="Reset Password"></td>
    <td align="center" valign="middle"><input name="Submit2" type="button" class="inputButton_md_Action1"  onClick="MM_callJS('confirmReset2();')" value="Reset &amp; Email Password"></td>    
  </tr>
</table>
     

<table cellpadding=2 cellspacing=0 width=100%>

            <%
            if ( (UserAccount.isRequired("StartPage",UserSecurityGroupID ))&&(!UserAccount.isComplete("StartPage")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("StartPage",expiredDays))&&(UserAccount.isExpiredCheck("StartPage",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("StartPage",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("StartPage",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("AccountType",UserSecurityGroupID ))&&(!UserAccount.isComplete("AccountType")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("AccountType",expiredDays))&&(UserAccount.isExpiredCheck("AccountType",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("AccountType",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("AccountType",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("SecurityGroupID",UserSecurityGroupID ))&&(!UserAccount.isComplete("SecurityGroupID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("SecurityGroupID",expiredDays))&&(UserAccount.isExpiredCheck("SecurityGroupID",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("SecurityGroupID",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("SecurityGroupID",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("GenericSecurityGroupID",UserSecurityGroupID ))&&(!UserAccount.isComplete("GenericSecurityGroupID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("GenericSecurityGroupID",expiredDays))&&(UserAccount.isExpiredCheck("GenericSecurityGroupID",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("GenericSecurityGroupID",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("GenericSecurityGroupID",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ReferenceID",UserSecurityGroupID ))&&(!UserAccount.isComplete("ReferenceID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ReferenceID",expiredDays))&&(UserAccount.isExpiredCheck("ReferenceID",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ReferenceID",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("ReferenceID",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("AccessType",UserSecurityGroupID ))&&(!UserAccount.isComplete("AccessType")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("AccessType",expiredDays))&&(UserAccount.isExpiredCheck("AccessType",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("AccessType",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("AccessType",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("Status",UserSecurityGroupID ))&&(!UserAccount.isComplete("Status")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("Status",expiredDays))&&(UserAccount.isExpiredCheck("Status",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("Status",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("Status",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("LogonUserName",UserSecurityGroupID ))&&(!UserAccount.isComplete("LogonUserName")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("LogonUserName",expiredDays))&&(UserAccount.isExpiredCheck("LogonUserName",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("LogonUserName",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("LogonUserName",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("LogonUserPassword",UserSecurityGroupID ))&&(!UserAccount.isComplete("LogonUserPassword")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("LogonUserPassword",expiredDays))&&(UserAccount.isExpiredCheck("LogonUserPassword",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("LogonUserPassword",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("LogonUserPassword",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("AttestKeyword1",UserSecurityGroupID ))&&(!UserAccount.isComplete("AttestKeyword1")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("AttestKeyword1",expiredDays))&&(UserAccount.isExpiredCheck("AttestKeyword1",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("AttestKeyword1",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("AttestKeyword1",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("AttestKeyword2",UserSecurityGroupID ))&&(!UserAccount.isComplete("AttestKeyword2")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("AttestKeyword2",expiredDays))&&(UserAccount.isExpiredCheck("AttestKeyword2",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("AttestKeyword2",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("AttestKeyword2",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("AttestKeywordTemp1",UserSecurityGroupID ))&&(!UserAccount.isComplete("AttestKeywordTemp1")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("AttestKeywordTemp1",expiredDays))&&(UserAccount.isExpiredCheck("AttestKeywordTemp1",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("AttestKeywordTemp1",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("AttestKeywordTemp1",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("AttestKeywordTemp2",UserSecurityGroupID ))&&(!UserAccount.isComplete("AttestKeywordTemp2")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("AttestKeywordTemp2",expiredDays))&&(UserAccount.isExpiredCheck("AttestKeywordTemp2",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("AttestKeywordTemp2",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("AttestKeywordTemp2",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactFirstName",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactFirstName")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactFirstName",expiredDays))&&(UserAccount.isExpiredCheck("ContactFirstName",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactFirstName",UserSecurityGroupID )))
            {
                        %>
                     <tr >
                       <td colspan="2" valign=top bgcolor="#ACCEAC" class="title">User Information</td>
          </tr>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>First Name&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input name="ContactFirstName" type=text class="titleSub1" value="<%=UserAccount.getContactFirstName()%>" size="50" maxlength="50">
                     &nbsp;
                     <%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactFirstName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactFirstName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactFirstName",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact First Name&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactFirstName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactFirstName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactFirstName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactLastName",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactLastName")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactLastName",expiredDays))&&(UserAccount.isExpiredCheck("ContactLastName",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactLastName",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> Last Name&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input name="ContactLastName" type=text class="titleSub1" value="<%=UserAccount.getContactLastName()%>" size="50" maxlength="50">
                     &nbsp;
                     <%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactLastName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactLastName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactLastName",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact Last Name&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactLastName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactLastName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactLastName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

            <%
            if ( (UserAccount.isRequired("NIM_UserType",UserSecurityGroupID))&&(!UserAccount.isComplete("NIM_UserType")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("NIM_UserType",expiredDays))&&(UserAccount.isExpiredCheck("NIM_UserType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("NIM_UserType",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>User Type&nbsp;</b></p></td><td valign=top><p><select   name="NIM_UserType" ><jsp:include page="../../generic/tNIMUserType_String.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getNIM_UserType()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NIM_UserType&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("NIM_UserType")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("NIM_UserType",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>User Type&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getNIM_UserType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NIM_UserType&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("NIM_UserType")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (UserAccount.isRequired("Comments",UserSecurityGroupID ))&&(!UserAccount.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("Comments",expiredDays))&&(UserAccount.isExpiredCheck("Comments",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("Comments",UserSecurityGroupID )))
            {
                        %>
                       <tr><td valign=top bgcolor="#ECECEC"><p class=<%=theClass%> ><b><%=UserAccount.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top bgcolor="#ECECEC"><p><textarea onKeyDown="textAreaStop(this,200)" rows="3" name="Comments" cols="60"><%=UserAccount.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("Comments")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("Comments",UserSecurityGroupID )))
            {
                        %>
                       <tr><td valign=top bgcolor="#ECECEC"> <p class=<%=theClass%> ><b><%=UserAccount.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top bgcolor="#ECECEC"><p><%=UserAccount.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("Comments")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("ImportantNotes",UserSecurityGroupID))&&(!UserAccount.isComplete("ImportantNotes")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ImportantNotes",expiredDays))&&(UserAccount.isExpiredCheck("ImportantNotes",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ImportantNotes",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#FFD595"><p class=<%=theClass%> ><b>User Notes&nbsp;</b></p></td><td valign=top bgcolor="#FFD595"><p><textarea onKeyDown="textAreaStop(this,200)" rows="6" name="ImportantNotes" cols="60"><%=UserAccount.getImportantNotes()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ImportantNotes&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ImportantNotes")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ImportantNotes",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#FFD595"> <p class=<%=theClass%> ><b><%=UserAccount.getEnglish("ImportantNotes")%>&nbsp;</b></p></td><td valign=top bgcolor="#FFD595"><p><%=UserAccount.getImportantNotes()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ImportantNotes&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ImportantNotes")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




          <%
            if ( (UserAccount.isRequired("ImportantNotes_Alert",UserSecurityGroupID))&&(!UserAccount.isComplete("ImportantNotes_Alert")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ImportantNotes_Alert",expiredDays))&&(UserAccount.isExpiredCheck("ImportantNotes_Alert",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
          <%
            if ((UserAccount.isWrite("ImportantNotes_Alert",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#FFD595"><p class=<%=theClass%> ><b>Pop-Up User  Notes</b></p></td><td valign=top bgcolor="#FFD595"><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="ImportantNotes_Alert" cols="60"><%=UserAccount.getImportantNotes_Alert()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ImportantNotes_Alert&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ImportantNotes_Alert")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ImportantNotes_Alert",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#FFD595"> <p class=<%=theClass%> ><b><%=UserAccount.getEnglish("ImportantNotes_Alert")%>&nbsp;</b></p></td><td valign=top bgcolor="#FFD595"><p><%=UserAccount.getImportantNotes_Alert()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ImportantNotes_Alert&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ImportantNotes_Alert")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("EmailAlertNotes_Alert",UserSecurityGroupID))&&(!UserAccount.isComplete("EmailAlertNotes_Alert")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("EmailAlertNotes_Alert",expiredDays))&&(UserAccount.isExpiredCheck("EmailAlertNotes_Alert",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("EmailAlertNotes_Alert",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#FFD595"><p class=<%=theClass%> ><b>Pop-Up User Notes - for Notifications</b></p></td><td valign=top bgcolor="#FFD595"><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="EmailAlertNotes_Alert" cols="60"><%=UserAccount.getEmailAlertNotes_Alert()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EmailAlertNotes_Alert&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("EmailAlertNotes_Alert")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("EmailAlertNotes_Alert",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#FFD595"> <p class=<%=theClass%> ><b><%=UserAccount.getEnglish("EmailAlertNotes_Alert")%>&nbsp;</b></p></td><td valign=top bgcolor="#FFD595"><p><%=UserAccount.getEmailAlertNotes_Alert()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EmailAlertNotes_Alert&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("EmailAlertNotes_Alert")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>







            <%
            if ( (UserAccount.isRequired("ContactEmail",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactEmail",expiredDays))&&(UserAccount.isExpiredCheck("ContactEmail",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactEmail",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input maxlength="75" type=text size="80" name="ContactEmail" value="<%=UserAccount.getContactEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactEmail",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

            <%
            if ( (UserAccount.isRequired("CompanyName",UserSecurityGroupID ))&&(!UserAccount.isComplete("CompanyName")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("CompanyName",expiredDays))&&(UserAccount.isExpiredCheck("CompanyName",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("CompanyName",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Business/Office Name&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input name="CompanyName" type=text class="titleSub1" value="<%=UserAccount.getCompanyName()%>" size="50" maxlength="50">
                     &nbsp;
          <%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("CompanyName",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Company Name&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getCompanyName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactAddress1",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactAddress1",expiredDays))&&(UserAccount.isExpiredCheck("ContactAddress1",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactAddress1",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> Address&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input maxlength="50" type=text size="80" name="ContactAddress1" value="<%=UserAccount.getContactAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactAddress1&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactAddress1")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactAddress1",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact Address&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactAddress1&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactAddress1")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactAddress2",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactAddress2",expiredDays))&&(UserAccount.isExpiredCheck("ContactAddress2",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactAddress2",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> Address 2&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input maxlength="20" type=text size="80" name="ContactAddress2" value="<%=UserAccount.getContactAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactAddress2&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactAddress2")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactAddress2",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact Address 2&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactAddress2&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactAddress2")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactCity",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactCity")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactCity",expiredDays))&&(UserAccount.isExpiredCheck("ContactCity",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactCity",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> City&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input maxlength="30" type=text size="80" name="ContactCity" value="<%=UserAccount.getContactCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactCity&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactCity")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactCity",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact City&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactCity&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactCity")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactStateID",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactStateID",expiredDays))&&(UserAccount.isExpiredCheck("ContactStateID",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactStateID",UserSecurityGroupID )))
            {
                        %>
                        <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> State&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><select   name="ContactStateID" ><jsp:include page="../../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getContactStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactStateID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactStateID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactStateID",UserSecurityGroupID )))
            {
                        %>
                        <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact State&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><jsp:include page="../../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getContactStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactStateID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactStateID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("ContactProvince",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactProvince")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactProvince",expiredDays))&&(UserAccount.isExpiredCheck("ContactProvince",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactProvince",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> Province, District, State&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input maxlength="100" type=text size="80" name="ContactProvince" value="<%=UserAccount.getContactProvince()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactProvince&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactProvince")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactProvince",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact Province, District, State&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactProvince&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactProvince")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactZIP",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactZIP",expiredDays))&&(UserAccount.isExpiredCheck("ContactZIP",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactZIP",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> ZIP&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input maxlength="50" type=text size="80" name="ContactZIP" value="<%=UserAccount.getContactZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactZIP&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactZIP")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactZIP",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact ZIP&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactZIP&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactZIP")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactCountryID",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactCountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactCountryID",expiredDays))&&(UserAccount.isExpiredCheck("ContactCountryID",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactCountryID",UserSecurityGroupID )))
            {
                        %>
                        <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> Country&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><select   name="ContactCountryID" ><jsp:include page="../../generic/tCountryLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getContactCountryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactCountryID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactCountryID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactCountryID",UserSecurityGroupID )))
            {
                        %>
                        <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact Country&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><jsp:include page="../../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getContactCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactCountryID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactCountryID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("ContactPhone",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactPhone",expiredDays))&&(UserAccount.isExpiredCheck("ContactPhone",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactPhone",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> Phone&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input name="ContactPhone" type=text class="titleSub1" value="<%=UserAccount.getContactPhone()%>" size="50" maxlength="50">
                     &nbsp;
                     <%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactPhone&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactPhone")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>

                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> Fax&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input name="ContactFax" type=text class="titleSub1" value="<%=UserAccount.getContactFax()%>" size="50" maxlength="50">
                     &nbsp;
                     <%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactFax&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactFax")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>

                        <%
            }
            else if ((UserAccount.isRead("ContactPhone",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactPhone&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactPhone")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("UserNPI",UserSecurityGroupID))&&(!UserAccount.isComplete("UserNPI")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("UserNPI",expiredDays))&&(UserAccount.isExpiredCheck("UserNPI",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("UserNPI",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>UserNPI&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="UserNPI" value="<%=UserAccount.getUserNPI()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=UserNPI&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("UserNPI")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("UserNPI",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>UserNPI&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getUserNPI()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=UserNPI&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("UserNPI")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("UserStateLicense",UserSecurityGroupID))&&(!UserAccount.isComplete("UserStateLicense")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("UserStateLicense",expiredDays))&&(UserAccount.isExpiredCheck("UserStateLicense",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("UserStateLicense",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>UserStateLicense&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="UserStateLicense" value="<%=UserAccount.getUserStateLicense()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=UserStateLicense&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("UserStateLicense")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("UserStateLicense",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>UserStateLicense&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getUserStateLicense()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=UserStateLicense&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("UserStateLicense")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("UserStateLicenseDesc",UserSecurityGroupID))&&(!UserAccount.isComplete("UserStateLicenseDesc")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("UserStateLicenseDesc",expiredDays))&&(UserAccount.isExpiredCheck("UserStateLicenseDesc",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("UserStateLicenseDesc",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>UserStateLicenseDesc&nbsp;</b></p></td><td valign=top><p><input maxlength="10" type=text size="80" name="UserStateLicenseDesc" value="<%=UserAccount.getUserStateLicenseDesc()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=UserStateLicenseDesc&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("UserStateLicenseDesc")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("UserStateLicenseDesc",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>UserStateLicenseDesc&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getUserStateLicenseDesc()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=UserStateLicenseDesc&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("UserStateLicenseDesc")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (UserAccount.isRequired("SecretQuestion",UserSecurityGroupID ))&&(!UserAccount.isComplete("SecretQuestion")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("SecretQuestion",expiredDays))&&(UserAccount.isExpiredCheck("SecretQuestion",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("SecretQuestion",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF" class="incompleteItem"><p class=<%=theClass%> ><b>Secret Question&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF" class="incompleteItem"><p><input name="SecretQuestion" type=text class="titleSub1" value="<%=UserAccount.getSecretQuestion()%>" size="50" maxlength="50">
                     &nbsp;
                     <%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SecretQuestion&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SecretQuestion")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("SecretQuestion",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF" class="incompleteItem"><p class=<%=theClass%> ><b>Secret Question&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF" class="incompleteItem"><p><%=UserAccount.getSecretQuestion()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SecretQuestion&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SecretQuestion")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("SecretAnswer",UserSecurityGroupID ))&&(!UserAccount.isComplete("SecretAnswer")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("SecretAnswer",expiredDays))&&(UserAccount.isExpiredCheck("SecretAnswer",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("SecretAnswer",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF" class="incompleteItem"><p class=<%=theClass%> ><b>Secret Answer&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF" class="incompleteItem"><p><input name="SecretAnswer" type=text class="titleSub1" value="<%=UserAccount.getSecretAnswer()%>" size="50" maxlength="50">
                     &nbsp;
                     <%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SecretAnswer&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SecretAnswer")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("SecretAnswer",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF" class="incompleteItem"><p class=<%=theClass%> ><b>Secret Answer&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF" class="incompleteItem"><p><%=UserAccount.getSecretAnswer()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SecretAnswer&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SecretAnswer")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("SelectMRI_ID",UserSecurityGroupID))&&(!UserAccount.isComplete("SelectMRI_ID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("SelectMRI_ID",expiredDays))&&(UserAccount.isExpiredCheck("SelectMRI_ID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("SelectMRI_ID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top bgcolor="#666666"><p class=<%=theClass%> ><b>SelectMRI_ID&nbsp;</b></p></td><td valign=top bgcolor="#666666"><p><input maxlength="20" type=text size="80" name="SelectMRI_ID" value="<%=UserAccount.getSelectMRI_ID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SelectMRI_ID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SelectMRI_ID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("SelectMRI_ID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top bgcolor="#666666"><p class=<%=theClass%> ><b>SelectMRI_ID&nbsp;</b></p></td><td valign=top bgcolor="#666666"><p><%=UserAccount.getSelectMRI_ID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SelectMRI_ID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SelectMRI_ID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("SelectMRI_Notes",UserSecurityGroupID))&&(!UserAccount.isComplete("SelectMRI_Notes")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("SelectMRI_Notes",expiredDays))&&(UserAccount.isExpiredCheck("SelectMRI_Notes",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("SelectMRI_Notes",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#666666"><p class=<%=theClass%> ><b><%=UserAccount.getEnglish("SelectMRI_Notes")%>&nbsp;</b></p></td><td valign=top bgcolor="#666666"><p><textarea onKeyDown="textAreaStop(this,5000)" rows="2" name="SelectMRI_Notes" cols="40" maxlength=5000><%=UserAccount.getSelectMRI_Notes()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SelectMRI_Notes&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SelectMRI_Notes")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("SelectMRI_Notes",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#666666"> <p class=<%=theClass%> ><b><%=UserAccount.getEnglish("SelectMRI_Notes")%>&nbsp;</b></p></td><td valign=top bgcolor="#666666"><p><%=UserAccount.getSelectMRI_Notes()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SelectMRI_Notes&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SelectMRI_Notes")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (UserAccount.isRequired("SelectMRI_UserType",UserSecurityGroupID))&&(!UserAccount.isComplete("SelectMRI_UserType")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("SelectMRI_UserType",expiredDays))&&(UserAccount.isExpiredCheck("SelectMRI_UserType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("SelectMRI_UserType",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top bgcolor="#666666"><p class=<%=theClass%> ><b>SelectMRI_UserType&nbsp;</b></p></td><td valign=top bgcolor="#666666"><p><input maxlength="20" type=text size="80" name="SelectMRI_UserType" value="<%=UserAccount.getSelectMRI_UserType()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SelectMRI_UserType&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SelectMRI_UserType")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("SelectMRI_UserType",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top bgcolor="#666666"><p class=<%=theClass%> ><b>SelectMRI_UserType&nbsp;</b></p></td><td valign=top bgcolor="#666666"><p><%=UserAccount.getSelectMRI_UserType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SelectMRI_UserType&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SelectMRI_UserType")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>





          <%
            if ( (UserAccount.isRequired("AlertEmail",UserSecurityGroupID ))&&(!UserAccount.isComplete("AlertEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("AlertEmail",expiredDays))&&(UserAccount.isExpiredCheck("AlertEmail",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
          <%
            if (false&&(UserAccount.isWrite("AlertEmail",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Alert Email&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input maxlength="50" type=text size="80" name="AlertEmail" value="<%=UserAccount.getAlertEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AlertEmail&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("AlertEmail")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(UserAccount.isRead("AlertEmail",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Alert Email&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getAlertEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AlertEmail&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("AlertEmail")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


                        <tr><td valign=middle><p class=<%=theClass%> ><b>Manager/Supervisor&nbsp;</b>
                              <input <%=HTMLFormStyleButton%> name="ManagerID2"  onClick="this.disabled=false;modalPost('ManagerID', modalWin('../LI_ModalPass.jsp?ssPF=gsn_nim3/LI_tUserAccount_Search_gsn.jsp&ssOpenField=ManagerID','Search...','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));document.ManagerID_IF.location='../../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('ManagerID').value;" type="button" value="Search..."  >
                        </td><td valign=middle><div class="boxDr"><iframe name="ManagerID_IF" frameborder="0" marginheight="0" marginwidth="0" height="100%" width="100%" scrolling="no" src="../../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=<%=UserAccount.getManagerID()%>"></iframe></div><input name="ManagerID" id="ManagerID"  onFocus="this.blur();modalPost('ManagerID', modalWin('../LI_ModalPass.jsp?ssPF=gsn_nim3/LI_tUserAccount_Search_gsn.jsp&ssOpenField=ManagerID','Search...','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));document.ManagerID_IF.location='../../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('ManagerID').value;" type="hidden" value="<%=UserAccount.getManagerID()%>"></td></tr>




            <%
            if ( (UserAccount.isRequired("AlertDays",UserSecurityGroupID ))&&(!UserAccount.isComplete("AlertDays")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("AlertDays",expiredDays))&&(UserAccount.isExpiredCheck("AlertDays",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("AlertDays",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("AlertDays",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("BillingComments",UserSecurityGroupID ))&&(!UserAccount.isComplete("BillingComments")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("BillingComments",expiredDays))&&(UserAccount.isExpiredCheck("BillingComments",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("BillingComments",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("BillingComments",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("PromoCode",UserSecurityGroupID ))&&(!UserAccount.isComplete("PromoCode")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("PromoCode",expiredDays))&&(UserAccount.isExpiredCheck("PromoCode",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("PromoCode",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("PromoCode",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
//not using user company info
if (false)
{
			if ( (UserAccount.isRequired("CompanyType",UserSecurityGroupID ))&&(!UserAccount.isComplete("CompanyType")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("CompanyType",expiredDays))&&(UserAccount.isExpiredCheck("CompanyType",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("CompanyType",UserSecurityGroupID )))
            {
                        %>
                     <tr class="tdHeaderAlt">
                       <td colspan="2" valign=top class="title">This User's Hospital/Company Information  </td>
          </tr>
                     <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b>Healthcare Company  Type&nbsp;</b><br />
                     (1 = Hospital) </p></td><td valign=top bgcolor="#E8F3FB"><p><input maxlength="20" type=text size="80" name="CompanyType" value="<%=UserAccount.getCompanyType()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyType&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyType")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("CompanyType",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b>Company Type&nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><%=UserAccount.getCompanyType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyType&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyType")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("CompanyName",UserSecurityGroupID ))&&(!UserAccount.isComplete("CompanyName")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("CompanyName",expiredDays))&&(UserAccount.isExpiredCheck("CompanyName",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("CompanyName",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b>Hospital/Company Name&nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><input name="CompanyName" type=text class="titleSub1" value="<%=UserAccount.getCompanyName()%>" size="50" maxlength="50">
                     &nbsp;
                     <%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("CompanyName",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b>Company Name&nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><%=UserAccount.getCompanyName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("CompanyAddress1",UserSecurityGroupID ))&&(!UserAccount.isComplete("CompanyAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("CompanyAddress1",expiredDays))&&(UserAccount.isExpiredCheck("CompanyAddress1",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("CompanyAddress1",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b> Address 1 &nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><input maxlength="50" type=text size="80" name="CompanyAddress1" value="<%=UserAccount.getCompanyAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyAddress1&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyAddress1")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("CompanyAddress1",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b>Company Address&nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><%=UserAccount.getCompanyAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyAddress1&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyAddress1")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("CompanyAddress2",UserSecurityGroupID ))&&(!UserAccount.isComplete("CompanyAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("CompanyAddress2",expiredDays))&&(UserAccount.isExpiredCheck("CompanyAddress2",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("CompanyAddress2",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b> Address 2 &nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><input maxlength="20" type=text size="80" name="CompanyAddress2" value="<%=UserAccount.getCompanyAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyAddress2&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyAddress2")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("CompanyAddress2",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b>Company Address &nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><%=UserAccount.getCompanyAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyAddress2&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyAddress2")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("CompanyCity",UserSecurityGroupID ))&&(!UserAccount.isComplete("CompanyCity")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("CompanyCity",expiredDays))&&(UserAccount.isExpiredCheck("CompanyCity",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("CompanyCity",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b> City&nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><input maxlength="30" type=text size="80" name="CompanyCity" value="<%=UserAccount.getCompanyCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyCity&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyCity")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("CompanyCity",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b>Company City&nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><%=UserAccount.getCompanyCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyCity&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyCity")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("CompanyStateID",UserSecurityGroupID ))&&(!UserAccount.isComplete("CompanyStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("CompanyStateID",expiredDays))&&(UserAccount.isExpiredCheck("CompanyStateID",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("CompanyStateID",UserSecurityGroupID )))
            {
                        %>
                        <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b> State&nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><select   name="CompanyStateID" ><jsp:include page="../../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getCompanyStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyStateID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyStateID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("CompanyStateID",UserSecurityGroupID )))
            {
                        %>
                        <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b>Company State&nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><jsp:include page="../../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getCompanyStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyStateID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyStateID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("CompanyZIP",UserSecurityGroupID ))&&(!UserAccount.isComplete("CompanyZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("CompanyZIP",expiredDays))&&(UserAccount.isExpiredCheck("CompanyZIP",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("CompanyZIP",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b> ZIP&nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><input maxlength="50" type=text size="80" name="CompanyZIP" value="<%=UserAccount.getCompanyZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyZIP&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyZIP")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("CompanyZIP",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b>Company ZIP&nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><%=UserAccount.getCompanyZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyZIP&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyZIP")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("CompanyPhone",UserSecurityGroupID ))&&(!UserAccount.isComplete("CompanyPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("CompanyPhone",expiredDays))&&(UserAccount.isExpiredCheck("CompanyPhone",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("CompanyPhone",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b> Phone&nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><input maxlength="50" type=text size="80" name="CompanyPhone" value="<%=UserAccount.getCompanyPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyPhone&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyPhone")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("CompanyPhone",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E8F3FB"><p class=<%=theClass%> ><b>Company Phone&nbsp;</b></p></td><td valign=top bgcolor="#E8F3FB"><p><%=UserAccount.getCompanyPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CompanyPhone&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyPhone")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("BillingID",UserSecurityGroupID ))&&(!UserAccount.isComplete("BillingID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("BillingID",expiredDays))&&(UserAccount.isExpiredCheck("BillingID",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ( (UserAccount.isRequired("PHDBAcknowledgementStatus",UserSecurityGroupID ))&&(!UserAccount.isComplete("PHDBAcknowledgementStatus")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("PHDBAcknowledgementStatus",expiredDays))&&(UserAccount.isExpiredCheck("PHDBAcknowledgementStatus",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("PHDBAcknowledgementStatus",UserSecurityGroupID )))
            {
                        %>
                        <tr  >
                          <td colspan="2" valign=top bgcolor="#D582E6" class="title">Participation Details </td>
          </tr>
                        <tr><td valign=top bgcolor="#E8BAF1"><p class=<%=theClass%> ><b>Acknowledgement Received</b></p></td><td valign=top bgcolor="#E8BAF1"><p><select   name="PHDBAcknowledgementStatus" ><jsp:include page="../../generic/tUserStatusLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getPHDBAcknowledgementStatus()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PHDBAcknowledgementStatus&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("PHDBAcknowledgementStatus")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("PHDBAcknowledgementStatus",UserSecurityGroupID )))
            {
                        %>
                        <tr><td valign=top bgcolor="#E8BAF1"><p class=<%=theClass%> ><b>PHDBAcknowledgementStatus&nbsp;</b></p></td><td valign=top bgcolor="#E8BAF1"><p><jsp:include page="../../generic/tUserStatusLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getPHDBAcknowledgementStatus()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PHDBAcknowledgementStatus&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("PHDBAcknowledgementStatus")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("PHDBLetterHeadStatus",UserSecurityGroupID ))&&(!UserAccount.isComplete("PHDBLetterHeadStatus")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("PHDBLetterHeadStatus",expiredDays))&&(UserAccount.isExpiredCheck("PHDBLetterHeadStatus",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("PHDBLetterHeadStatus",UserSecurityGroupID )))
            {
                        %>
                        <tr><td valign=top bgcolor="#E8BAF1"><p class=<%=theClass%> ><b>LetterHead Received</b></p></td><td valign=top bgcolor="#E8BAF1"><p><select   name="PHDBLetterHeadStatus" ><jsp:include page="../../generic/tUserStatusLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getPHDBLetterHeadStatus()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PHDBLetterHeadStatus&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("PHDBLetterHeadStatus")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("PHDBLetterHeadStatus",UserSecurityGroupID )))
            {
                        %>
                        <tr><td valign=top bgcolor="#E8BAF1"><p class=<%=theClass%> ><b>PHDBLetterHeadStatus&nbsp;</b></p></td><td valign=top bgcolor="#E8BAF1"><p><jsp:include page="../../generic/tUserStatusLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getPHDBLetterHeadStatus()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PHDBLetterHeadStatus&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("PHDBLetterHeadStatus")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("TaxID",UserSecurityGroupID ))&&(!UserAccount.isComplete("TaxID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("TaxID",expiredDays))&&(UserAccount.isExpiredCheck("TaxID",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("TaxID",UserSecurityGroupID )))
            {
                        %>
                     <tr >
                       <td colspan="2" valign=top bgcolor="#9EEA88" class="title"> Billing Information </td>
          </tr>
                     <tr><td valign=top bgcolor="#D1F5C7"><p class=<%=theClass%> ><b>TaxID&nbsp;</b></p></td><td valign=top bgcolor="#D1F5C7"><p><input maxlength="50" type=text size="80" name="TaxID" value="<%=UserAccount.getTaxID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TaxID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("TaxID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("TaxID",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#D1F5C7"><p class=<%=theClass%> ><b>TaxID&nbsp;</b></p></td><td valign=top bgcolor="#D1F5C7"><p><%=UserAccount.getTaxID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TaxID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("TaxID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

            <%
            if ((UserAccount.isWrite("BillingID",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#D1F5C7"><p class=<%=theClass%> ><b>BillingID&nbsp;</b><br />
                     </p></td><td valign=top bgcolor="#D1F5C7"><p><input maxlength="20" type=text size="80" name="BillingID" value="<%=UserAccount.getBillingID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("BillingID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p>
                         <p>Other Users Linked to this Billing Account:</p>
                         <table width="95%" border="1" cellpadding="3" cellspacing="2" bordercolor="#333333" bgcolor="#EDF8E9">
                           <tr class="tdHeaderAlt">
                             <td bgcolor="#9EEA88">Created</td>
                             <td bgcolor="#9EEA88">First Name </td>
                             <td bgcolor="#9EEA88">Last Name </td>
                             <td bgcolor="#9EEA88">Email</td>
                             <td bgcolor="#9EEA88">Company</td>
                           </tr>
<%
	searchDB2 myBIDSS = new searchDB2();
	java.sql.ResultSet myBIDRS = null;;

	try
	{
		myBIDRS = myBIDSS.executeStatement("select * from tUserAccount where billingid>0 and billingid=" + UserAccount.getBillingID() +" order by contactlastname"); 
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
	try
	{
	   while (myBIDRS!=null&&myBIDRS.next())
	   {
%>
                           <tr>
                             <td><%=displayDateSDF.format(dbdfFull.parse(myBIDRS.getString("UniqueCreateDate")))%></td>
                             <td><%=myBIDRS.getString("ContactLastName")%></td>
                             <td><%=myBIDRS.getString("ContactFirstName")%></td>
                             <td><a href="tCompanyUserAccountLU_main2_UserAccount_CompanyID_form_authorize.jsp?EDIT=edit&amp;EDITID=<%=myBIDRS.getString("UserID")%>"><%=myBIDRS.getString("LogonUserName")%></a></td>
                             <td><%=myBIDRS.getString("CompanyName")%></td>
                           </tr>

<%	   
	   }
	   
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
%>						   
                     </table>                         <p>&nbsp; </p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("BillingID",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#D1F5C7"><p class=<%=theClass%> ><b>BillingID&nbsp;</b></p></td><td valign=top bgcolor="#D1F5C7"><p><%=UserAccount.getBillingID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillingID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("BillingID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (UserAccount.isRequired("NotUsingPHDBID",UserSecurityGroupID ))&&(!UserAccount.isComplete("NotUsingPHDBID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("NotUsingPHDBID",expiredDays))&&(UserAccount.isExpiredCheck("NotUsingPHDBID",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("NotUsingPHDBID",UserSecurityGroupID )))
            {
                        %>
                     <tr >
                       <td colspan="2" valign=top bgcolor="#999999" class="title">Hospital Survey Data</td>
          </tr>
                     <tr><td valign=top bgcolor="#CCCCCC"><p class=<%=theClass%> ><b>NotUsingPHDBID&nbsp;</b></p></td><td valign=top bgcolor="#CCCCCC"><p><input maxlength="20" type=text size="80" name="NotUsingPHDBID" value="<%=UserAccount.getNotUsingPHDBID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NotUsingPHDBID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("NotUsingPHDBID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("NotUsingPHDBID",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#CCCCCC"><p class=<%=theClass%> ><b>NotUsingPHDBID&nbsp;</b></p></td><td valign=top bgcolor="#CCCCCC"><p><%=UserAccount.getNotUsingPHDBID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NotUsingPHDBID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("NotUsingPHDBID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("NotUsingPHDBComments",UserSecurityGroupID ))&&(!UserAccount.isComplete("NotUsingPHDBComments")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("NotUsingPHDBComments",expiredDays))&&(UserAccount.isExpiredCheck("NotUsingPHDBComments",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("NotUsingPHDBComments",UserSecurityGroupID )))
            {
                        %>
                       <tr><td valign=top bgcolor="#CCCCCC"><p class=<%=theClass%> ><b><%=UserAccount.getEnglish("NotUsingPHDBComments")%>&nbsp;</b></p></td><td valign=top bgcolor="#CCCCCC"><p><textarea onKeyDown="textAreaStop(this,4000)" rows="2" name="NotUsingPHDBComments" cols="40" maxlength=4000><%=UserAccount.getNotUsingPHDBComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NotUsingPHDBComments&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("NotUsingPHDBComments")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("NotUsingPHDBComments",UserSecurityGroupID )))
            {
                        %>
                       <tr><td valign=top bgcolor="#CCCCCC"> <p class=<%=theClass%> ><b><%=UserAccount.getEnglish("NotUsingPHDBComments")%>&nbsp;</b></p></td><td valign=top bgcolor="#CCCCCC"><p><%=UserAccount.getNotUsingPHDBComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NotUsingPHDBComments&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("NotUsingPHDBComments")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=../images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            
			}
			
			
			
//not using user company info
}			
            %>
            
            




            <tr><td width=40% bgcolor="#CCCCCC">&nbsp;</td><td width=60% bgcolor="#CCCCCC">&nbsp;</td></tr>
        </table>







        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <p><select name=routePageReference class="buttonSleekLarge">
            <option value="sParentReturnPage">Save & Return</option>
            <option value="sLocalChildReturnPage" selected="selected">Save & Stay</option>
            </select></p>
            <p><input name=Submit type=Submit class="titleBlue" value=Submit>
            </p>
        <%}%>
      </form>
      <%
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>    </td>
    <td align="center" valign="top" bgcolor="#E4E7EE"><p><span class="title">Account Activity Information</span></p>
      <table width="100%" border="1" cellpadding="5" cellspacing="0" bordercolor="#666666" bgcolor="#C7CDDD">
      <tr>
        <td class="titleSub1">Last ~100 Cases</td>
      </tr>
      <tr>
        <td>
          <%
  {//localize brackets

%>
          <table width="100%" border="1" cellpadding="3" cellspacing="0" bgcolor="#F3F4F8">
            <tr>
              <td class="tdHeaderAlt">Received</td>
              <td class="tdHeaderAlt">Patient</td>
              <td class="tdHeaderAlt">Claim #</td>
              <td class="tdHeaderAlt">ScanPass</td>
              <td class="tdHeaderAlt">Type</td>
              <td class="tdHeaderAlt">Status</td>
              <td class="tdHeaderAlt">Scheduled</td>
              <td class="tdHeaderAlt">Appointment</td>
              <td class="tdHeaderAlt">Case Payer</td>
              </tr>
            <%
	searchDB2 mySSAct = new searchDB2();
	java.sql.ResultSet myRSAct = null;;
	try
	{
		String mySQL55 = "None";
		if (UserAccount.getAccountType().equalsIgnoreCase("AdjusterID")||UserAccount.getAccountType().equalsIgnoreCase("AdjusterID2")||UserAccount.getAccountType().equalsIgnoreCase("AdjusterID3"))
		{
			mySQL55 = ("select tNIM3_Encounter.timetrack_reqsched, tNIM3_PayerMaster.payername as PayerName, tNIM3_PayerMaster.payertypeid, tNIM3_CaseAccount.caseclaimnumber, tNIM3_CaseAccount.patientfirstname,tNIM3_CaseAccount.patientlastname , tNIM3_Referral.receivedate, tNIM3_Encounter.encounterstatusid, tNIM3_Encounter.encountertypeid,tNIM3_Encounter.scanpass, tNIM3_Appointment.appointmenttime from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid  INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid LEFT JOIN tNIM3_Appointment on tNIM3_Encounter.appointmentid = tNIM3_Appointment.appointmentid where (tNIM3_CaseAccount.adjusterid=" + UserAccount.getUserID() +" or tNIM3_CaseAccount.nursecasemanagerid=" + UserAccount.getUserID() +" or tNIM3_CaseAccount.caseadministratorid=" + UserAccount.getUserID() +" or tNIM3_CaseAccount.caseadministrator2id=" + UserAccount.getUserID() +" or tNIM3_CaseAccount.caseadministrator3id=" + UserAccount.getUserID() +" or tNIM3_CaseAccount.caseadministrator4id=" + UserAccount.getUserID() +") order by tNIM3_Referral.receivedate Desc");
		}
		else if (UserAccount.getAccountType().equalsIgnoreCase("PhysicianID"))
		{
			mySQL55 = ("select tNIM3_Encounter.timetrack_reqsched, tNIM3_PayerMaster.payername as PayerName, tNIM3_PayerMaster.payertypeid,tNIM3_CaseAccount.caseclaimnumber, tNIM3_CaseAccount.patientfirstname,tNIM3_CaseAccount.patientlastname , tNIM3_Referral.receivedate, tNIM3_Encounter.encounterstatusid, tNIM3_Encounter.encountertypeid,tNIM3_Encounter.scanpass, tNIM3_Appointment.appointmenttime from tNIM3_CaseAccount INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerid INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid LEFT JOIN tNIM3_Appointment on tNIM3_Encounter.appointmentid = tNIM3_Appointment.appointmentid  where tNIM3_Referral.ReferringPhysicianID=" + UserAccount.getUserID() +" order by tNIM3_Referral.receivedate Desc");
		}
//		out.println(mySQL55);
		if (mySQL55.equalsIgnoreCase("None"))
		{
		}
		else
		{
			myRSAct = mySSAct.executeStatement(mySQL55);
		}
			
		
		
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
	try
	{
	   if (myRSAct!=null)
	   {
		   while (myRSAct!=null&&myRSAct.next())
		   {
	%>
				<tr>
				  <td nowrap="nowrap"><%=PLCUtils.getDisplayDateWithTime(dbdfFull.parse(myRSAct.getString("receivedate")))%></td>
				  <td><%=myRSAct.getString("PatientLastName")%>,&nbsp;<%=myRSAct.getString("PatientFirstName")%></td>
				  <td><%=myRSAct.getString("CaseClaimNumber")%></td>
				  <td><%=myRSAct.getString("ScanPass")%></td>
				  <td><jsp:include page="../../generic/tEncounterTypeLI_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myRSAct.getString(\"EncounterTypeID\")%>"  /></jsp:include></td>
				  <td><jsp:include page="../../generic/tEncounterStatusLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myRSAct.getString(\"EncounterStatusID\")%>"  /></jsp:include></td>
				  <td nowrap="nowrap"><%=PLCUtils.getDisplayDateWithTime(dbdfFull.parse(myRSAct.getString("timetrack_reqsched")))%>&nbsp;</td>
				  <td nowrap="nowrap"><%=PLCUtils.getDisplayDateWithTime(dbdfFull.parse(myRSAct.getString("appointmenttime")))%>&nbsp;</td>
				  <td><strong><%=myRSAct.getString("PayerName")%></strong><br />Type: <%=myRSAct.getString("PayerTypeID")%></td>
				  </tr>
				<%		
		   }
	   }
	   else
	   {
 		   out.println("No Results");
	   }
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
%>
            </table>
          <%
  }//localize
%>		</td>
      </tr>
      </table></td>
    </tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_UserID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
