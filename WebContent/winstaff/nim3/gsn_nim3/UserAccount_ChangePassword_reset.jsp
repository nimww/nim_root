<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%@ include file="../../generic/CheckLogin.jsp" %>
<%@ include file="../../generic/generalDisplay.jsp" %>
<table cellpadding=0 cellspacing=0 border=0 width=700>
  <tr> 
    <td width=10>&nbsp;</td>
    <td> <span class=title>Change Password</span><br>
      <%
//initial declaration of list class and parentID
    Integer        iUserID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iUserID")) 
    {
        iUserID        =    (Integer)pageControllerHash.get("iUserID");
        accessValid = true;    
    }
  //page security
  if (accessValid&& isScheduler2 )
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
	bltUserAccount UserAccount = new bltUserAccount(iUserID);

	boolean isPassed=true;
	com.winstaff.password.Encrypt myEnc = new com.winstaff.password.Encrypt();
	
//	String P1 = Math.round(Math.random()*100) + com.winstaff.password.RandomString.generateString(4) + Math.round(Math.random()*100) + com.winstaff.password.RandomString.generateString(4);
	String P1 = ""+Math.round(Math.random()*100000000);
	if (P1.length()<8)
	{
		P1 = Math.round(Math.random()*10) + "" + Math.round(Math.random()*100000000);
	}
	isPassed = true;
	if (isPassed)
	{
		UserAccount.setLogonUserPassword( myEnc.getMD5Base64(P1) );
		UserAccount.commitData();
	%>
		<span class=tdHeader>Password Change Successful<br>New Password: <%=P1%><br><a href=# onClick="javascript:window.close()">close</a></span>
	<%
	}

	else
	{
	%>
		<span class=tdHeader>Password Not Changed, please try again<br><a href=# onClick="javascript:window.close()">close</a></span>
	<%
	}
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>
    </td>
  </tr>
</table>
