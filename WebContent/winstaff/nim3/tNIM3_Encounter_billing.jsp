


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tNIM3_Encounter_form.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear_fade.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<script language="javascript">
function removeCommas(sVal)
{
	return sVal.replace(',','');
//	return sVal;
}

</script>
<script language="javascript">
show_pws_Loading();
</script>

    <table id="bigfade" cellpadding=0 cellspacing=0 border=0 width=100%>
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tNIM3_Encounter")%>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
//        accessValid = true;
        if (iEncounterID.intValue() == iEDITID.intValue())
        {
        	accessValid = true;
        }
    }
  //page security
  if (accessValid&&isScheduler2)
  {
	java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
	java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);	java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_Encounter_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID

    NIM3_EncounterObject2        myEO2        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("nim3expbill") )
    {
//        NIM3_Encounter        =    new    bltNIM3_Encounter(iEncounterID,UserSecurityGroupID);
        myEO2        =    new    NIM3_EncounterObject2(iEncounterID,"Loading Billing");
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
//        NIM3_Encounter        =    new    bltNIM3_Encounter(UserSecurityGroupID, true);
    }
	String tempUseZip = myEO2.getNIM3_CaseAccount().getPatientZIP();
	if (myEO2.getAppointment_PracticeMaster()!=null)
	{
		if(myEO2.getAppointment_PracticeMaster().getOfficeZIP().length()>5){
			tempUseZip = myEO2.getAppointment_PracticeMaster().getOfficeZIP().substring(0, 5);
		} else{
			tempUseZip = myEO2.getAppointment_PracticeMaster().getOfficeZIP();
		}
		
	}
			

//fields
        %>
        <form action="tNIM3_EncounterService_form_sub.jsp" name="tNIM3_Encounter_form1" method="POST" >
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
    %>
        <input type="hidden" name="EDITID" value = "<%=iEDITID%>" >  

          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr>
           <td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=50%>
                        <tr>
                          <td colspan="3" valign=middle><img src="images/marker_encounter.gif" width="114" height="31" align="middle" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="inputButton_md_Create" onClick = "this.disabled=false;$('table').fadeOut('slow');show_pws_Calc();" type=Submit value="Save" name=Submit></td>
                        </tr>







            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("SentToIC",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("SentToIC")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("SentToIC",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("SentToIC",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (false&&isScheduler&&(myEO2.getNIM3_Encounter().isWrite("SentToIC",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                          <td valign=top nowrap="nowrap"><p class=<%=theClass%> ><b>Date Report Sent To Provider<br /></b><b>(mm/dd/yyyy hh:mm am/pm):</b></p></td><td colspan="2" valign=top><p><input name="SentToIC"  type=text disabled value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getSentToIC())%>" /></jsp:include>' size="30" maxlength=20 >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToIC&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("SentToIC")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&isScheduler&&(myEO2.getNIM3_Encounter().isRead("SentToIC",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top nowrap="nowrap"><p class=<%=theClass%> ><b>Sent To Provider&nbsp;<br />
                        (mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getSentToIC())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToIC&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("SentToIC")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



<tr class="tdHeaderAlt">
  <td colspan="3">Billing Dates</td></tr>



            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("SentTo_Bill_Pay",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("SentTo_Bill_Pay")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("SentTo_Bill_Pay",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("SentTo_Bill_Pay",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(myEO2.getNIM3_Encounter().isWrite("SentTo_Bill_Pay",UserSecurityGroupID)))
            {
                        %><tr><td colspan="3"><hr></td></tr>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Bill Sent To Payer&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><input maxlength=20  type=text size="30" name="SentTo_Bill_Pay" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_Bill_Pay&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("SentTo_Bill_Pay")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (isScheduler&&(myEO2.getNIM3_Encounter().isRead("SentTo_Bill_Pay",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Bill Sent To Payer&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_Bill_Pay&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("SentTo_Bill_Pay")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("Rec_Bill_Pay",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("Rec_Bill_Pay")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("Rec_Bill_Pay",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("Rec_Bill_Pay",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (false&&isScheduler&&(myEO2.getNIM3_Encounter().isWrite("Rec_Bill_Pay",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Payment Rec From Payer&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><input maxlength=20  type=text size="30" name="Rec_Bill_Pay" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getRec_Bill_Pay())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pay&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Rec_Bill_Pay")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&isScheduler&&(myEO2.getNIM3_Encounter().isRead("Rec_Bill_Pay",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Payment Rec By Payer&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getRec_Bill_Pay())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pay&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Rec_Bill_Pay")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("Rec_Bill_Pro",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("Rec_Bill_Pro")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("Rec_Bill_Pro",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("Rec_Bill_Pro",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(myEO2.getNIM3_Encounter().isWrite("Rec_Bill_Pro",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Bill Rec From Provider&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><input maxlength=20  type=text size="30" name="Rec_Bill_Pro" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getRec_Bill_Pro())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pro&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Rec_Bill_Pro")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (isScheduler&&(myEO2.getNIM3_Encounter().isRead("Rec_Bill_Pro",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Bill Rec By Provider&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getRec_Bill_Pro())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pro&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Rec_Bill_Pro")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("Pricing_Structure",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("Pricing_Structure")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("Pricing_Structure",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("Pricing_Structure",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("Pricing_Structure",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Pricing Structure&nbsp;</b></p></td><td valign=top><p><input maxlength="1" type=text size="6" name="Pricing_Structure" value="<%=myEO2.getNIM3_Encounter().getPricing_Structure()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Pricing_Structure&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Pricing_Structure")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((myEO2.getNIM3_Encounter().isRead("Pricing_Structure",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Pricing_Structure&nbsp;</b></p></td><td valign=top><p><%=myEO2.getNIM3_Encounter().getPricing_Structure()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Pricing_Structure&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Pricing_Structure")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("SeeNetDev_SelectedPracticeID",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("SeeNetDev_SelectedPracticeID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("SeeNetDev_SelectedPracticeID",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("SeeNetDev_SelectedPracticeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            
            if ((myEO2.getNIM3_Encounter().isWrite("SeeNetDev_SelectedPracticeID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>SeeNetDev<br>Selected PracticeID&nbsp;</b></p></td>
                     <td valign=top>
                     <p>
                     	<input maxlength="8" type=text size="8" id="myinputpay" name="SeeNetDev_SelectedPracticeID" value="<%=myEO2.getNIM3_Encounter().getSeeNetDev_SelectedPracticeID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SeeNetDev_SelectedPracticeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("SeeNetDev_SelectedPracticeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                     </p>
                     </td>
                     </tr>
                        <%
            }
            else if ((myEO2.getNIM3_Encounter().isRead("SeeNetDev_SelectedPracticeID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>SeeNetDev_SelectedPracticeID&nbsp;</b></p></td><td valign=top><p><%=myEO2.getNIM3_Encounter().getSeeNetDev_SelectedPracticeID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SeeNetDev_SelectedPracticeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("SeeNetDev_SelectedPracticeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("AmountBilledByProvider",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("AmountBilledByProvider")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("AmountBilledByProvider",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("AmountBilledByProvider",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("AmountBilledByProvider",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Amount Billed By Provider:&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="20" name="AmountBilledByProvider" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(myEO2.getNIM3_Encounter().getAmountBilledByProvider())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AmountBilledByProvider&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("AmountBilledByProvider")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((myEO2.getNIM3_Encounter().isRead("AmountBilledByProvider",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>AmountBilledByProvider&nbsp;</b></p></td><td valign=top><p><%=PLCUtils.getDisplayDefaultDecimalFormat2(myEO2.getNIM3_Encounter().getAmountBilledByProvider())%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AmountBilledByProvider&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("AmountBilledByProvider")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>





            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("SentTo_Bill_Pro",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("SentTo_Bill_Pro")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("SentTo_Bill_Pro",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("SentTo_Bill_Pro",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(myEO2.getNIM3_Encounter().isWrite("SentTo_Bill_Pro",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top bgcolor="#EDF7E1"><p class=<%=theClass%> ><b>Payment Sent To Provider&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top bgcolor="#EDF7E1"> </td>
                          <td valign=top>&nbsp;</td>
                </tr>
                        <tr>
                          <td valign=top bgcolor="#EDF7E1">&nbsp;</td>
                          <td valign=top bgcolor="#EDF7E1"><table width="25%" border="1" cellspacing="0" cellpadding="2">
  <tr class=tdHeaderAlt>
    <td colspan="3" align="center">Payment Sent <u>To</u> Provider</td>
    </tr>
  <tr class=tdHeaderAlt>
    <td>Check #&nbsp;</td>
    <td>Amount</td>
    <td>Date</td>
  </tr>
  <tr>
    <td><input maxlength="20" type=text size="15" name="PaidToProviderCheck1Number" id="PaidToProviderCheck1Number" value="<%=myEO2.getNIM3_Encounter().getPaidToProviderCheck1Number()%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="10" name="PaidToProviderCheck1Amount" id="PaidToProviderCheck1Amount" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(myEO2.getNIM3_Encounter().getPaidToProviderCheck1Amount())%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="15" name="PaidToProviderCheck1Date" id="PaidToProviderCheck1Date" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getPaidToProviderCheck1Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
  <tr>
    <td><input maxlength="20" type=text size="15" name="PaidToProviderCheck2Number" id="PaidToProviderCheck2Number" value="<%=myEO2.getNIM3_Encounter().getPaidToProviderCheck2Number()%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="10" name="PaidToProviderCheck2Amount" id="PaidToProviderCheck2Amount" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(myEO2.getNIM3_Encounter().getPaidToProviderCheck2Amount())%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="15" name="PaidToProviderCheck2Date" id="PaidToProviderCheck2Date" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getPaidToProviderCheck2Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
  <tr>
    <td><input maxlength="20" type=text size="15" name="PaidToProviderCheck3Number" id="PaidToProviderCheck3Number" value="<%=myEO2.getNIM3_Encounter().getPaidToProviderCheck3Number()%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="10" name="PaidToProviderCheck3Amount" id="PaidToProviderCheck3Amount" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(myEO2.getNIM3_Encounter().getPaidToProviderCheck3Amount())%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="15" name="PaidToProviderCheck3Date" id="PaidToProviderCheck3Date" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getPaidToProviderCheck3Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
</table>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                </tr>
                        <%
            }
            else if (false&&isScheduler&&(myEO2.getNIM3_Encounter().isRead("SentTo_Bill_Pro",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Payment Sent To Provider&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getSentTo_Bill_Pro())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_Bill_Pro&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("SentTo_Bill_Pro")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>








            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("DateOfService",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("DateOfService")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("DateOfService",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("DateOfService",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO2.getNIM3_Encounter().isWrite("DateOfService",UserSecurityGroupID)))
            {
                        %><tr><td colspan="3"><hr></td></tr>
                        <tr><td valign=middle nowrap="nowrap"><p class=<%=theClass%> ><b>Date Of Service<br />
        (mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=middle><p><input maxlength=20  type=text size="30" name="DateOfService" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getDateOfService())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfService&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("DateOfService")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((myEO2.getNIM3_Encounter().isRead("DateOfService",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=middle><p class=<%=theClass%> ><b>Date Of Service&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=middle><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getDateOfService())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfService&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("DateOfService")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("LODID",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("LODID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("LODID",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("LODID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(myEO2.getNIM3_Encounter().isWrite("LODID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Level of Disability&nbsp;</b></p></td><td colspan="2" valign=top><p><select   name="LODID" ><jsp:include page="../generic/tLODLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getLODID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LODID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("LODID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(myEO2.getNIM3_Encounter().isRead("LODID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Level of Disability&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/tLODLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getLODID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LODID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("LODID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

       <%
            if ( (myEO2.getNIM3_Encounter().isRequired("Comments",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("Comments",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(myEO2.getNIM3_Encounter().isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=middle><p class=<%=theClass%> ><b><%=myEO2.getNIM3_Encounter().getEnglish("Comments")%>&nbsp;</b></p></td><td colspan="2" valign=middle><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=myEO2.getNIM3_Encounter().getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(myEO2.getNIM3_Encounter().isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=middle> <p class=<%=theClass%> ><b><%=myEO2.getNIM3_Encounter().getEnglish("Comments")%>&nbsp;</b></p></td><td colspan="2" valign=middle><p><%=myEO2.getNIM3_Encounter().getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <tr><td>&nbsp;</td><td colspan="2">&nbsp;</td></tr>
            </table>
        </td></tr></table>


<%
		NIM3_BillingObject myBO = myEO2.getBillingTotals();
%>                          
                          
                          <table border="1" cellspacing="0" cellpadding="10">
 <tr>
    <td nowrap bgcolor="#CCCCCC" class="tdHeader">Expected In:&nbsp;&nbsp;</td>
    <td nowrap bgcolor="#CCCCCC" class="borderHighlightGreen"><strong>$<%=myBO.getPayer_AllowAmount()+myBO.getPayer_AllowAmount_Adjustment()%></strong></td>
    <td nowrap bgcolor="#CCCCCC" class="tdHeader">&nbsp;Expected Out:&nbsp;&nbsp;&nbsp;</td>
    <td nowrap bgcolor="#CCCCCC" class="borderHighlightGreen"><strong>$<%=myBO.getProvider_AllowAmount()%></strong></td>
    <td nowrap bgcolor="#CCCCCC" class="tdHeader">Expected Net:</td>
    <td nowrap bgcolor="#CCCCCC" class="borderHighlightGreen"><span <%if (myBO.getNetExpected()<=0){%>class="tdBaseAltRed"<%}%>><strong>$<%=myBO.getNetExpected()%></strong></span></td>
    </tr>
  <tr>
    <td nowrap bgcolor="#CCCCCC" class="tdHeader">Actual In:&nbsp;&nbsp;&nbsp;</td>
    <td nowrap bgcolor="#CCCCCC" class="borderHighlightGreen"><strong>$<%=myBO.getPayer_ActualAmount()%></strong></td>
    <td nowrap bgcolor="#CCCCCC" class="tdHeader">Actual Out:&nbsp;&nbsp;</td>
    <td nowrap bgcolor="#CCCCCC" class="borderHighlightGreen"><strong>$<%=myBO.getProvider_ActualAmount()%></strong></td>
    <td nowrap bgcolor="#CCCCCC" class="tdHeader">&nbsp;Actual Net:</td>
    <td nowrap bgcolor="#CCCCCC" class="borderHighlightGreen"><span <%if (myBO.getNetActual()<=0){%>class="tdBaseAltRed"<%}%>><strong>$<%=myBO.getNetActual()%></strong></span></td>
    </tr>                          
  </table>
  <table>
  <% java.util.List<Integer> overRideList = java.util.Arrays.asList(28050,21204,23610,19852,18538);  %>
  <tr <%=(overRideList.contains(CurrentUserAccount.getUserID()) ? "":"style=\"display:none;\"") %>><Td>Override: <input name="override" type="radio" value="false"> Yes <input name="override" type="radio" value="true" checked> No </Td></tr>
  </table>
<!-- Start -->
<%
    bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID,"","ServiceID");

//declaration of Enumeration
    bltNIM3_Service        NIM3_Service;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Service_List.elements();
    %>
 <table border="0" bordercolor="333333" cellpadding="0"  cellspacing="0" width="100%">        <tr><td>
<br />

        </td></tr>
    <%
    int iExpress=0;
    int iMaxExpress=8;
//    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tNIM3_Service"))
//    while (eList.hasMoreElements()||iExpress<iMaxExpress)
    while (eList.hasMoreElements())
    {
       iExpress++;
         %>
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        NIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
      }
      else
      {
        NIM3_Service  = new bltNIM3_Service();
        isNewRecord= true;
      }
        NIM3_Service.GroupSecurityInit(UserSecurityGroupID);
//        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tNIM3_Service")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name="recordItemStatus_<%=iExpress%>" value="new">
<%}
else
{%>
<input type=hidden name="recordItemStatus_<%=iExpress%>" value="edit">
<%}

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableColor><tr>
          <td>
        <table cellpadding=0 border=0 cellspacing=0 width=100%>




                     <tr>
                       <td colspan=2 valign=top> <b>Status&nbsp;</b><select     name="ServiceStatusID_<%=iExpress%>"  id="ServiceStatusID_<%=iExpress%>" ><jsp:include page="../generic/tServiceStatusLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Service.getServiceStatusID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ServiceStatusID&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("ServiceStatusID")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                     </tr>
                     <tr><td colspan=2 valign=top>CPT&nbsp;: <strong><%=NIM3_Service.getCPT()%></strong>&nbsp;&nbsp;CPT Modifier&nbsp; <strong><%=NIM3_Service.getCPTModifier()%></strong>&nbsp;&nbsp;QTY: <%=NIM3_Service.getCPTQty()%> &nbsp;&nbsp;CPT BodyPart&nbsp;<strong><%=NIM3_Service.getCPTBodyPart()%></strong></td></tr>


                     <tr><td colspan="2" valign=top>
<p class=<%=theClass%> ><b>Diagnosis/Rule-out&nbsp;</b><input name="CPTText_<%=iExpress%>" type=text value="<%=NIM3_Service.getCPTText()%>" size="80" maxlength="100">
                     &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CPTText&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("CPTText")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>                     
and/or

                     
                     <p class=<%=theClass%> >
                     
                     
                     
                     <b>Diag/ICD 1&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT1_<%=iExpress%>" value="<%=NIM3_Service.getdCPT1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT1&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT1")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>

                     <b>2&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT2_<%=iExpress%>" value="<%=NIM3_Service.getdCPT2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT2&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT2")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                     
                     <b>3&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT3_<%=iExpress%>" value="<%=NIM3_Service.getdCPT3()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT3&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT3")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                     
                     <b>4&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT4_<%=iExpress%>" value="<%=NIM3_Service.getdCPT4()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT4&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT4")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                     
                     
                     
                     
                     
                     </p></td></tr>
  <tr>
    <td valign=top><table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>      <p class=<%=theClass%> ><b>Bill Amount&nbsp;</b>&nbsp;$<input maxlength="10" type=text size="20" name="BillAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getBillAmount())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("BillAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
        &nbsp;&nbsp;<b>Contract/Allow Amount&nbsp;</b>&nbsp;$
        <input maxlength="10" type=text size="20" name="AllowAmount_<%=iExpress%>" id="AllowAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getAllowAmount())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AllowAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("AllowAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;&nbsp;Adjustment <input maxlength=10  type=text size="8" name="AllowAmountAdjustment_<%=iExpress%>" id="AllowAmountAdjustment_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getAllowAmountAdjustment())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AllowAmountAdjustment&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("AllowAmountAdjustment")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;&nbsp;<br><b>Received Amount&nbsp;</b>&nbsp;$<input name="ReceivedAmount_<%=iExpress%>" id="ReceivedAmount_<%=iExpress%>" type=text value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReceivedAmount())%>" size="20" maxlength="10" <% if (!isScheduler3){%>readonly="readonly"<%}%> >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReceivedAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("ReceivedAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
        &nbsp;&nbsp;<b>Pay-out Amount&nbsp;</b>&nbsp;$
        <input maxlength="10" type=text size="20" name="PaidOutAmount_<%=iExpress%>" id="PaidOutAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getPaidOutAmount())%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PaidOutAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("PaidOutAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>

</td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td class="borderHighlightGreen"
        <%bltNIM3_PayerMaster myPPM = new bltNIM3_PayerMaster(myEO2.getNIM3_CaseAccount().getPayerID());
		if(myPPM.getParentPayerID()==951){%> style="display:none;"<%}%>>
        &nbsp;&nbsp;State Fee Schedule: <strong>$<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIMUtils.getFeeSchedulePrice(new Integer(1), new Double(1.00), tempUseZip,NIM3_Service.getCPT(),NIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService()))%></strong>&nbsp;&nbsp;
        <!--xyz zip: <%=tempUseZip%> | cpt: <%=NIM3_Service.getCPT()%> | mod: <%=NIM3_Service.getCPTModifier()%> | dos: <%=myEO2.getNIM3_Encounter().getDateOfService()%>-->
        
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td class="borderHighlightGreen" align="center">&nbsp;&nbsp;Payer Allow Amount:&nbsp;&nbsp;<strong>
        <%java.util.List<Integer> tysonExceptionIC = java.util.Arrays.asList(7691,8629,7557,7559,7744,889,865,7886,10078);
    	int tysonParentID = 138; %>
        <%if(myPPM.getParentPayerID()==951){%>
        <%=NIM3_Service.getBillAmount()%>
		<%} else if((myEO2.getNIM3_PayerMaster().getParentPayerID()==tysonParentID || myEO2.getNIM3_PayerMaster().getPayerID()==tysonParentID) ){
		 	try{
		 		if(tysonExceptionIC.contains(myEO2.getAppointment_PracticeMaster().getPracticeID())){
		 			out.println("<br>Exception Price: $"+NIMUtils.getTysonExceptionRate(myEO2.getAppointment_PracticeMaster().getPracticeID(),NIMUtils.getRadiologyGroup(NIM3_Service.getCPT())));
		 		} else{%>
		 			<br>DOS: $<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIMUtils.getPayerAllowAmount(myEO2.getNIM3_CaseAccount().getPayerID(),tempUseZip, NIM3_Service.getCPT(),NIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService()))%>
			        <br>Today: $<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIMUtils.getPayerAllowAmount(myEO2.getNIM3_CaseAccount().getPayerID(),tempUseZip, NIM3_Service.getCPT(),NIM3_Service.getCPTModifier(),PLCUtils.getToday()))%>	
		 		<%}
		 	} catch(Exception e){%>
		 		<br>DOS: $<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIMUtils.getPayerAllowAmount(myEO2.getNIM3_CaseAccount().getPayerID(),tempUseZip, NIM3_Service.getCPT(),NIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService()))%>
		        <br>Today: $<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIMUtils.getPayerAllowAmount(myEO2.getNIM3_CaseAccount().getPayerID(),tempUseZip, NIM3_Service.getCPT(),NIM3_Service.getCPTModifier(),PLCUtils.getToday()))%>	
		 	<%}
        } else{%>
        <br>DOS: $<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIMUtils.getPayerAllowAmount(myEO2.getNIM3_CaseAccount().getPayerID(),tempUseZip, NIM3_Service.getCPT(),NIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService()))%>
        <br>Today: $<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIMUtils.getPayerAllowAmount(myEO2.getNIM3_CaseAccount().getPayerID(),tempUseZip, NIM3_Service.getCPT(),NIM3_Service.getCPTModifier(),PLCUtils.getToday()))%>
        <%}%>
        </strong></td>
      </tr>
    </table>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%">
    
      <table width="25%" border="1" cellspacing="0" cellpadding="2">
  <tr class=tdHeaderAlt>
    <td colspan="3" align="center">Payment Received <u>From</u> Payer</td>
    </tr>
  <tr class=tdHeaderAlt>
    <td>Received Check #&nbsp;</td>
    <td>Amount</td>
    <td>Date</td>
  </tr>
  <tr>
    <td><input maxlength="20" type=text size="20" name="ReceivedCheck1Number_<%=iExpress%>" id="ReceivedCheck1Number_<%=iExpress%>" value="<%=NIM3_Service.getReceivedCheck1Number()%>">&nbsp;</td>
    <td><input maxlength=10   type=text size="10" name="ReceivedCheck1Amount_<%=iExpress%>" id="ReceivedCheck1Amount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReceivedCheck1Amount())%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="20" name="ReceivedCheck1Date_<%=iExpress%>" id="ReceivedCheck1Date_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Service.getReceivedCheck1Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
  <tr>
    <td><input maxlength="20" type=text size="20" name="ReceivedCheck2Number_<%=iExpress%>" id="ReceivedCheck2Number_<%=iExpress%>" value="<%=NIM3_Service.getReceivedCheck2Number()%>">&nbsp;</td>
    <td><input maxlength=10   type=text size="10" name="ReceivedCheck2Amount_<%=iExpress%>" id="ReceivedCheck2Amount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReceivedCheck2Amount())%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="20" name="ReceivedCheck2Date_<%=iExpress%>" id="ReceivedCheck2Date_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Service.getReceivedCheck2Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
  <tr>
    <td><input maxlength="20" type=text size="20" name="ReceivedCheck3Number_<%=iExpress%>" id="ReceivedCheck3Number_<%=iExpress%>" value="<%=NIM3_Service.getReceivedCheck3Number()%>">&nbsp;</td>
    <td><input maxlength=10   type=text size="10" name="ReceivedCheck3Amount_<%=iExpress%>" id="ReceivedCheck3Amount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReceivedCheck3Amount())%>">&nbsp;</td>
    <td><input maxlength=10  type=text size="20" name="ReceivedCheck3Date_<%=iExpress%>" id="ReceivedCheck3Date_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Service.getReceivedCheck3Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
</table>

</td>
    <td align="center"><br>
Provider/Facility:<br>
<span class="borderHighlightGreen"><%=myEO2.getPracticeBillingTable(false)%></span></td>
  </tr>
</table>

</p>

  </td></tr>                     <tr>
                       <td valign=top>&nbsp;</td>
                       <td>&nbsp;</td>
                     </tr>


        </table>
        </td></tr></table>
        </td></tr>
        <tr><td>&nbsp;</td></tr>

        <%
    }
    %>
        <tr><td>
        <input type=hidden name=nextPage value="">
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
            <p><input class="inputButton_md_Create" onClick = "this.disabled=false;$('table').fadeOut('slow');show_pws_Calc();" type=Submit value="Save" name=Submit></p>
        <%}%>
        
        <hr>
        Audit Notes:<br>
        <textarea name="" cols="90" rows="5" readonly><%=myEO2.getNIM3_Encounter().getAuditNotes()%></textarea>
        
        </td></tr> </table>

    </table>


        </form>




<!-- End -->            
            
            
            
            
            
            
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
    </td></tr></table>
<script language="javascript">
hide_pws_Loading();
</script>

