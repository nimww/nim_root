<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>NextImage Secure Server</title>
<link href="www-images/style.css" rel="stylesheet" type="text/css" />
<link href="images/sign_in4/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#000000"  onLoad="document.getElementById('LogonUserName').focus();">
<center>
<div class="htmlBox">
    <a href="http://www.nextimagemedical.com">
        <div class="logoBox"> 
            <img src="images/sign_in4/logo1.png" width="238" height="62" border="0"><br>
        </div>
    </a>
	<div class="contentBox" style="text-align:center;">
		<span class="xbig" >Welcome to the NextImage Secure Portal    </span><br>
Information submitted here is protected through SSL Security

	</div>
	<div class="contentBox">
		<span class="big"> <strong>Sign in Below</strong></span>
		<div class="contentBox_SignIn">
            <form name="form1" method="post" action="signIn_sub.jsp" target="_top">
                User Name:<br><input name="LogonUserName" type="text" class="inputField" id="LogonUserName" size="20">
                <br>
                <br>
                Password:<br><input name="LogonUserPassword" type="password" class="inputField" size="20">
                <br>
                <br>
                <input name="Submit" type="submit" class="inputButton" value="Sign In">
            </form>
        </div>
        If you forgot your password please contact us at <STRONG>888-318-5111</STRONG> (Press 1)
	</div>
	<div class="contentBox">
		<span class="xbig">What is the NextImage Secure Portal?</span><br>
		<br>
        The NextImage Portal allows you to submit referrals without having to re-enter your name and contact information.  Once you sign in, we know who you are. <br><br> You can also view the status of referral and view the diagnostic reports once available.
<br><br>		<span class="xbig">Tell me more about the security?</span><br><br>
        We use top SSL Security to protect the information you submit here.  Depending on your browser, you may notice the <em><strong>green</strong></em> highligh on the URL at the top or a <em><strong>green</strong></em> "NextImage Medical, Inc." to the side of the URL. You'll probably notice the similarity if you visit financial institutions and other high level security sites.<br><br>You won't see it everywhere, but we felt that it's important to provide you with the top security.  So you will see here. 
	</div>
</div>
</center>
</body>
</html>
