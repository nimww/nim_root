<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*,org.apache.commons.fileupload.* " %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_icid.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
    <table cellpadding=0 cellspacing=0 border=0  >
    <tr><td width=10>&nbsp;</td><td>
  <%
	//initial declaration of list class and parentID
    boolean accessValid = false;
    Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
    String ENCID = null;
	NIM3_EncounterObject myEO = null;
	
	// Need a global definition for assignment below.
	String fileToUpload = null; 
	
   if (iSecurityCheck.intValue()!=0)
   {
    if (isIC) 
    {
		accessValid = true;
    }
  //page security
  if (accessValid)
  {
	  try
	  {

		String myFileSS = "";
		boolean isPassed=false;


		// Create a new file upload handler
		DiskFileUpload upload = new DiskFileUpload();
		
		// Set upload parameters
		
		//need to put these in config
		upload.setSizeThreshold(1900000);
		upload.setSizeMax(1900000);
//			String myRPath = "/var/lib/tomcat5/webapps/nimdox/";
		String myRPath = ConfigurationInformation.sUploadFolderDirectory;
//			upload.setRepositoryPath("c:\\web\\webapps\\nimdox\\");
		upload.setRepositoryPath(myRPath);			
		// Parse the request
		java.util.List /* FileItem */ items = upload.parseRequest(request);
		java.util.Iterator iter = items.iterator();
		java.text.SimpleDateFormat fileDF = new java.text.SimpleDateFormat("yyyy-MM-dd" );
		if (iter.hasNext()) 
		{
			FileItem item = (FileItem) iter.next();
			FileItem item_edit = (FileItem) iter.next();
			ENCID = item_edit.getString();
			boolean isPassedAccess = false;
			if (NIMUtils.isICIDAuthorizedToUploadReport(CurrentUserAccount.getReferenceID(),new Integer(ENCID)))
			{
				myEO = new NIM3_EncounterObject(new Integer(ENCID));
				isPassedAccess = true;
			}
			if (isPassedAccess)
			{
				String sFileDF = "doc_" + com.winstaff.password.RandomString.generateString(6).toLowerCase() + "_"+ fileDF.format(PLCUtils.getNowDate(false)) + "_" + myEO.NIM3_CaseAccount.getCaseCode();
				String oFileName = item.getName();
				oFileName = oFileName.substring(oFileName.lastIndexOf("\\")+1,oFileName.length());
				oFileName = oFileName.replace(' ' , '_');				
				myFileSS= sFileDF + "_" + oFileName;
				String nFileName = myRPath + myFileSS;
				fileToUpload = nFileName;				
				java.io.File uploadedFile =new java.io.File(nFileName);
				if (item.getName().indexOf(".pdf")<=0)
				{
					isPassed = false;
					%>
					<script language="javascript">
					alert("Your file does not appear to be a PDF file.  Files must be uploaded in PDF format.");
					document.location = 'IC_Home.jsp';
					</script>	
					<%	
				}
				else if (item.getSize()==0)
				{
					isPassed = false;
					%>
					<script language="javascript">
					alert("Your file appears to have a size of ZERO or is locked/open by another user.\n\nYour document has NOT been uploaded successfully.");
					document.location = 'IC_Home.jsp';
					</script>	
					<%	
				}
				else
				{
					item.write(uploadedFile);
					isPassed = true;
				}
			}
			else
			{
				out.println("Error:[host:"+REMOTE_USER_host+"][ip:"+REMOTE_USER_ip+"]:WEB:IC_UploadReport_sub.jsp:Error[ICID="+CurrentUserAccount.getReferenceID()+"|EncID="+ENCID+"]");
			}
		}
		if (isPassed)
		{
			if (true)
			{
				bltNIM3_Document myDoc = new bltNIM3_Document();
				myDoc.setFileName(myFileSS);
				myDoc.setComments("Doc[IC]");
				myDoc.setReferralID(myEO.NIM3_Referral.getReferralID());
				myDoc.setEncounterID(myEO.NIM3_Encounter.getEncounterID());
				myDoc.setUploadUserID(CurrentUserAccount.getUserID());
				myDoc.setCaseID(myEO.NIM3_CaseAccount.getCaseID());
				myDoc.commitData();
				String UserLogonDescription = "n/a";
				if (pageControllerHash.containsKey("UserLogonDescription"))
				{
					UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
				}
				//always send email since this is an IC
				if (true)
				{
					bltPracticeMaster myPM = new bltPracticeMaster(myEO.NIM3_Appointment.getProviderID());
					bltNIM3_CommTrack	working_bltNIM3_CommTrack = new bltNIM3_CommTrack();
					working_bltNIM3_CommTrack.setEncounterID(myEO.NIM3_Encounter.getEncounterID());
					working_bltNIM3_CommTrack.setReferralID(myEO.NIM3_Referral.getReferralID());
					working_bltNIM3_CommTrack.setCaseID(myEO.NIM3_CaseAccount.getCaseID());
					working_bltNIM3_CommTrack.setMessageName(CurrentUserAccount.getLogonUserName());
					working_bltNIM3_CommTrack.setMessageCompany(myPM.getPracticeName());
					working_bltNIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
					working_bltNIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
					working_bltNIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
					working_bltNIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
					working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
					
					//working_bltNIM3_CommTrack.setUniqueModifyComments(""+UserLogonDescription);
					String theMessage = "Document Uploaded by Imaging Center ["+CurrentUserAccount.getLogonUserName()+" @ " + myPM.getPracticeName() + "] to one of your cases.";
					working_bltNIM3_CommTrack.setMessageText(theMessage);
					working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
					emailType_V3 myEM = new emailType_V3();
					bltUserAccount myAT = new bltUserAccount(myEO.NIM3_CaseAccount.getAssignedToID());
					myEM.setTo(myAT.getContactEmail());
					myEM.setFrom("server@nextimagemedical.com");
					myEM.setSubject("New Document Uploaded by IC for SP: " + myEO.NIM3_Encounter.getScanPass() + "[" + myEO.NIM3_CaseAccount.getPatientFirstName() + "]");
					myEM.setBody(theMessage);
					myEM.isSendMail();
					//displayNotes = "Email sent to NetDev";
					working_bltNIM3_CommTrack.setUniqueModifyComments(UserLogonDescription);
					working_bltNIM3_CommTrack.commitData();
	
					myDoc.setEncounterID(myEO.NIM3_Encounter.getEncounterID());
					myDoc.setComments("Rp");
					myDoc.setDocName("Diagnostic Report");
					myDoc.setUniqueModifyComments(UserLogonDescription);
					myDoc.commitData();
					myEO.NIM3_Encounter.setReportFileID(myDoc.getUniqueID());
					if (myEO.NIM3_Encounter.getTimeTrack_ReqDelivered().before( new java.util.Date(1)))
					{
						myEO.NIM3_Encounter.setTimeTrack_ReqDelivered(PLCUtils.getNowDate(true));
						myEO.NIM3_Encounter.setTimeTrack_ReqDelivered_UserID(CurrentUserAccount.getUserID());
					}
					myEO.NIM3_Encounter.setUniqueModifyComments(UserLogonDescription);
					myEO.NIM3_Encounter.commitData();
					%>
					
				</td></tr>
				<tr>
				  <td>&nbsp;</td>
	  <td class="tdHeader">Please Verify Procedures for Billing: <%=myEO.NIM3_CaseAccount.getPatientLastName()%>, <%=myEO.NIM3_CaseAccount.getPatientFirstName()%></td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td><table width="100%" border="0" cellspacing="0" cellpadding="5">
				    <tr>
				      <td><p>Service Codes:</p>
			          <%=myEO.getServicePrintOut(true)%></td>
			        </tr>
			      </table></td>
	  </tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
	  </tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>
					  <input name="Submit" type="submit" value="Confirm" />
					</p>
				  </form></td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
				</table>
					
					
					
					<%
				}
			
			}
		
			if (ConfigurationInformation.storeDocumentsInDatabase) {
				
				// Take file from disc and store in db.
				DocumentDatabase dd = new DocumentDatabase();					
		        DebugLogger.println(">>> File to upload "+ fileToUpload );	
		        // Insert filename first
		        dd.insertFilename(myFileSS);
		        // Serialize file to blob and store.
		    	dd.writeFile(fileToUpload);
			}			
		}

	}
	catch (Exception eeee)
	{
		DebugLogger.println("Error:[host:"+REMOTE_USER_host+"][ip:"+REMOTE_USER_ip+"]:WEB:IC_UploadReport_sub.jsp:Error[ICID="+CurrentUserAccount.getReferenceID()+"|EncID="+ENCID+"]");
		%>
	<script language="javascript">
		alert("There was an error uploading your document\n\nThe file size likely exceeds the max size.\n\n<%=eeee%>");
//				document.location='tNIM3_Referral_main_NIM3_Referral_CaseID_4Payer.jsp';
		document.location = 'IC_Home.jsp';
		</script>		
	<%

	}








  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>