<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    filename: tnim2_Authorization_main_NIM_Authorization_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td><hr></td></tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type=button onClick = "this.disabled=true;document.location ='tNIM3_CommTrack_main_NIM3_CommTrack_CaseID_form_create.jsp?EDIT=new&KM=p&INTNext=yes'" value="Create"></td>
    </tr>
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tnim2_Authorization")%>



<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {

%>
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p class=title><strong>Comm</strong>unication <strong>Tracks</strong></p>
<%
try
{
String myExtUserID = "";
String myCommTypeID = "";
String myCommStart = "";
String myCommEnd = "";
String myMessageText = "";
String myMessageName = "";
String myMessageCompany = "";
String myMessageEmail = "";
String myMessagePhone = "";
String myMessageFax = "";
String myAlertStatusCode = "";
String myComments = "";
String orderBy = "CommStart";
int startID = 0;
int maxResults = 50;

boolean firstDisplay = false;

try
{
maxResults = Integer.parseInt(request.getParameter("maxResults"));
startID = Integer.parseInt(request.getParameter("startID"));
myExtUserID = request.getParameter("ExtUserID");
myCommTypeID = request.getParameter("CommTypeID");
myCommStart = request.getParameter("CommStart");
myCommEnd = request.getParameter("CommEnd");
myMessageText = request.getParameter("MessageText");
myMessageName = request.getParameter("MessageName");
myMessageCompany = request.getParameter("MessageCompany");
myMessageEmail = request.getParameter("MessageEmail");
myMessagePhone = request.getParameter("MessagePhone");
myMessageFax = request.getParameter("MessageFax");
myAlertStatusCode = request.getParameter("AlertStatusCode");
myComments = request.getParameter("Comments");
orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myExtUserID = "";
myCommTypeID = "";
myCommStart = "";
myCommEnd = "";
myMessageText = "";
myMessageName = "";
myMessageCompany = "";
myMessageEmail = "";
myMessagePhone = "";
myMessageFax = "";
myAlertStatusCode = "0";
myComments = "";
orderBy = "CommStart";
}
if (orderBy == null)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myExtUserID = "";
myCommTypeID = "";
myCommStart = "";
myCommEnd = "";
myMessageText = "";
myMessageName = "";
myMessageCompany = "";
myMessageEmail = "";
myMessagePhone = "";
myMessageFax = "";
myAlertStatusCode = "0";
myComments = "";
orderBy = "CommStart";
}
firstDisplay = false;

String orderBy2;
if (orderBy.equalsIgnoreCase("CommStart")||orderBy.equalsIgnoreCase("CommEnd"))
{
	orderBy2 = orderBy + " desc";
}
else
{
	orderBy2 = orderBy + "";
}

if (firstDisplay)
{
%>



<table width = 735 cellpadding=2 cellspacing=2>
<tr>
<td width=10>&nbsp;</td>
<td class=tdBase><p>&nbsp;</p>
</td>
</tr>
<tr>
<td width=10>&nbsp;</td>

<td>
<form name="form1" method="POST" action="tNIM3_CommTrack_query.jsp">
  <table width="400" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#003333">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
        <tr>
         <td class=tdHeaderAlt>
         ExtUserID
         </td>
         <td>
         <input type=text name="ExtUserID">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         CommTypeID
         </td>
         <td>
         <input type=text name="CommTypeID">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         CommStart
         </td>
         <td>
         <input type=text name="CommStart">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         CommEnd
         </td>
         <td>
         <input type=text name="CommEnd">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         MessageText
         </td>
         <td>
         <input type=text name="MessageText">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         MessageName
         </td>
         <td>
         <input type=text name="MessageName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         MessageCompany
         </td>
         <td>
         <input type=text name="MessageCompany">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         MessageEmail
         </td>
         <td>
         <input type=text name="MessageEmail">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         MessagePhone
         </td>
         <td>
         <input type=text name="MessagePhone">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         MessageFax
         </td>
         <td>
         <input type=text name="MessageFax">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         AlertStatusCode
         </td>
         <td>
         <input type=text name="AlertStatusCode">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Comments
         </td>
         <td>
         <input type=text name="Comments">
         </td>
        </tr>
            <tr>
            <td class=tdHeaderAlt><p>Sort by:</p></td>
            <td> 
              <select name=orderBy>
                <option value="ExtUserID">ExtUserID</option>
                <option value="CommTypeID">CommTypeID</option>
                <option value="CommStart">CommStart</option>
                <option value="CommEnd">CommEnd</option>
                <option value="MessageText">MessageText</option>
                <option value="MessageName">MessageName</option>
                <option value="MessageCompany">MessageCompany</option>
                <option value="MessageEmail">MessageEmail</option>
                <option value="MessagePhone">MessagePhone</option>
                <option value="MessageFax">MessageFax</option>
                <option value="AlertStatusCode">AlertStatusCode</option>
                <option value="Comments">Comments</option>
              </select>
            </td>
          </tr>
          <tr bgcolor="#CCCCCC"> 
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td> 
              <input type=hidden name="startID" value=0>
              <input type=hidden name="maxResults" value=50>
              <input type="submit" name="Submit2" value="Submit">
            </td>
            <td>&nbsp;</td>
          </tr>
        </table>
</td>
    </tr>
  </table>
  <p>&nbsp;</p>
  </form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p> 
  <%
}
else
{


String myWhere = "where (commtrackid>0 and caseid = " + iCaseID + " " ;
boolean theFirst = false;

try
{
if (!myExtUserID.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myExtUserID.indexOf("%")>=0){myWhere += "ExtUserID LIKE '" + myExtUserID +"'";}
else if (myExtUserID.indexOf("=")==0){myWhere += "ExtUserID = '" + myExtUserID.substring(1, myExtUserID.length())+"'";}
else if (myExtUserID.length()<=3){myWhere += "ExtUserID LIKE '" + myExtUserID +"%'";}
else {myWhere += "ExtUserID LIKE '%" + myExtUserID +"%'";}
theFirst = false;
}
if (!myCommTypeID.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myCommTypeID.indexOf("%")>=0){myWhere += "CommTypeID LIKE '" + myCommTypeID +"'";}
else if (myCommTypeID.indexOf("=")==0){myWhere += "CommTypeID = '" + myCommTypeID.substring(1, myCommTypeID.length())+"'";}
else if (myCommTypeID.length()<=3){myWhere += "CommTypeID LIKE '" + myCommTypeID +"%'";}
else {myWhere += "CommTypeID LIKE '%" + myCommTypeID +"%'";}
theFirst = false;
}
if (!myCommStart.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "CommStart > '" + myCommStart+"'";
theFirst = false;
}
if (!myCommEnd.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "CommEnd > '" + myCommEnd+"'";
theFirst = false;
}
if (!myMessageText.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myMessageText.indexOf("%")>=0){myWhere += "UPPER(MessageText) LIKE '" + myMessageText.toUpperCase() +"'";}
else if (myMessageText.indexOf("=")==0){myWhere += "UPPER(MessageText) = '" + myMessageText.toUpperCase().substring(1, myMessageText.length())+"'";}
else if (myMessageText.length()<=3){myWhere += "UPPER(MessageText) LIKE '" + myMessageText.toUpperCase() +"%'";}
else {myWhere += "UPPER(MessageText) LIKE '%" + myMessageText.toUpperCase() +"%'";}
theFirst = false;
}
if (!myMessageName.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myMessageName.indexOf("%")>=0){myWhere += "UPPER(MessageName) LIKE '" + myMessageName.toUpperCase() +"'";}
else if (myMessageName.indexOf("=")==0){myWhere += "UPPER(MessageName) = '" + myMessageName.toUpperCase().substring(1, myMessageName.length())+"'";}
else if (myMessageName.length()<=3){myWhere += "UPPER(MessageName) LIKE '" + myMessageName.toUpperCase() +"%'";}
else {myWhere += "UPPER(MessageName) LIKE '%" + myMessageName.toUpperCase() +"%'";}
theFirst = false;
}
if (!myMessageCompany.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myMessageCompany.indexOf("%")>=0){myWhere += "UPPER(MessageCompany) LIKE '" + myMessageCompany.toUpperCase() +"'";}
else if (myMessageCompany.indexOf("=")==0){myWhere += "UPPER(MessageCompany) = '" + myMessageCompany.toUpperCase().substring(1, myMessageCompany.length())+"'";}
else if (myMessageCompany.length()<=3){myWhere += "UPPER(MessageCompany) LIKE '" + myMessageCompany.toUpperCase() +"%'";}
else {myWhere += "UPPER(MessageCompany) LIKE '%" + myMessageCompany.toUpperCase() +"%'";}
theFirst = false;
}
if (!myMessageEmail.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myMessageEmail.indexOf("%")>=0){myWhere += "UPPER(MessageEmail) LIKE '" + myMessageEmail.toUpperCase() +"'";}
else if (myMessageEmail.indexOf("=")==0){myWhere += "UPPER(MessageEmail) = '" + myMessageEmail.toUpperCase().substring(1, myMessageEmail.length())+"'";}
else if (myMessageEmail.length()<=3){myWhere += "UPPER(MessageEmail) LIKE '" + myMessageEmail.toUpperCase() +"%'";}
else {myWhere += "UPPER(MessageEmail) LIKE '%" + myMessageEmail.toUpperCase() +"%'";}
theFirst = false;
}
if (!myMessagePhone.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myMessagePhone.indexOf("%")>=0){myWhere += "UPPER(MessagePhone) LIKE '" + myMessagePhone.toUpperCase() +"'";}
else if (myMessagePhone.indexOf("=")==0){myWhere += "UPPER(MessagePhone) = '" + myMessagePhone.toUpperCase().substring(1, myMessagePhone.length())+"'";}
else if (myMessagePhone.length()<=3){myWhere += "UPPER(MessagePhone) LIKE '" + myMessagePhone.toUpperCase() +"%'";}
else {myWhere += "UPPER(MessagePhone) LIKE '%" + myMessagePhone +"%'";}
theFirst = false;
}
if (!myMessageFax.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myMessageFax.indexOf("%")>=0){myWhere += "UPPER(MessageFax) LIKE '" + myMessageFax.toUpperCase() +"'";}
else if (myMessageFax.indexOf("=")==0){myWhere += "UPPER(MessageFax) = '" + myMessageFax.toUpperCase().substring(1, myMessageFax.length())+"'";}
else if (myMessageFax.length()<=3){myWhere += "UPPER(MessageFax) LIKE '" + myMessageFax.toUpperCase() +"%'";}
else {myWhere += "UPPER(MessageFax) LIKE '%" + myMessageFax.toUpperCase() +"%'";}
theFirst = false;
}
if (!myAlertStatusCode.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "AlertStatusCode = '" + myAlertStatusCode.toUpperCase() +"'";
theFirst = false;
}
if (!myComments.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
if (myComments.indexOf("%")>=0){myWhere += "UPPER(Comments) LIKE '" + myComments.toUpperCase() +"'";}
else if (myComments.indexOf("=")==0){myWhere += "UPPER(Comments) = '" + myComments.toUpperCase().substring(1, myComments.length())+"'";}
else if (myComments.length()<=3){myWhere += "UPPER(Comments) LIKE '" + myComments.toUpperCase() +"%'";}
else {myWhere += "UPPER(Comments) LIKE '%" + myComments.toUpperCase() +"%'";}
theFirst = false;
}
myWhere += ")";

//System.out.println(myWhere);
if (theFirst||myWhere.equalsIgnoreCase(")"))
{
myWhere = "";
}


}
catch(Exception e)
{
out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
myRS = mySS.executeStatement("select * from tNIM3_CommTrack " + myWhere + " order by " + orderBy2);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
cnt++;
if (cnt>=startID&&cnt<=startID+maxResults)
{
cnt2++;

String myClass = "tdBase";

			bltNIM3_CommTrack myCT = new bltNIM3_CommTrack (new  Integer(myRS.getString("commtrackid")));
if (cnt2%2==0)
{
myClass = "tdBaseAlt";
}
myMainTable +="<tr class="+myClass+">";
myMainTable +="<td>"+cnt+"</td>";
myMainTable +="<td>";
if (myCT.getExtUserID().intValue()==19)
{
	myMainTable +="[AT]";
}
else
{
	myMainTable +=myRS.getString("ExtUserID")+"";
}
myMainTable +="</td>";
myMainTable +="<td>";
if (myCT.getCommTypeID().intValue()==1)
{
	myMainTable +="Msg";
}
else if (myCT.getCommTypeID().intValue()==2&&myCT.getCaseID().intValue()>0)
{
	myMainTable +="<a href=\"tNIM3_CaseAccount_main_NIM2_CaseAccount_CaseID_form_authorize.jsp?EDIT=open&EDITID=" + myCT.getCaseID() +"&KM=p\">Referral</a>";
}
else
{
	myMainTable +="??";
}
myMainTable +="</td>";
myMainTable +="<td colspan=2>";
myMainTable +=displayDateTimeSDF.format(myCT.getCommStart()) + "<br>(Dur: <strong>" + Math.round((myCT.getCommEnd().getTime()-myCT.getCommStart().getTime())/1000/60) + "</strong> min)";

myMainTable +="</td>";
myMainTable +="<td colspan=2 >From: ";
myMainTable +="<strong>" + myRS.getString("MessageName")+"</strong><br>@";
myMainTable +="<strong> "+myRS.getString("MessageCompany")+"</strong>";
myMainTable +="</td>";
myMainTable +="<td colspan=3 nowrap>";
myMainTable +="P: ";
myMainTable +="<strong>" + myRS.getString("MessagePhone")+"</strong>";
myMainTable +="<br>E: ";
myMainTable +="<strong>" + myRS.getString("MessageEmail")+"</strong>";
myMainTable +="<br>F: ";
myMainTable +="<strong>" + myRS.getString("MessageFax")+"</strong>";
myMainTable +="</td>";
myMainTable +="<td colspan=1>";
//myMainTable +="<a href=\"tNIM3_CommTrack_close.jsp?EDITID=" + myCT.getUniqueID() + "\">Close</a><br>" + myRS.getString("AlertStatusCode")+"";
//myMainTable +=myRS.getString("AlertStatusCode")+"";
myMainTable +="</td>";
myMainTable +="</tr>";

myMainTable +="<tr class="+myClass+">";
myMainTable +="<td colspan=11><table cellpadding=5 width=75% class=certborderTrans><tr><td>";
myMainTable +="Message: <textarea cols=60 rows=6 disabled>" + myRS.getString("MessageText")+"</textarea>";
myMainTable +="<br>"+myRS.getString("Comments")+"";
myMainTable +="</td>";
myMainTable +="</tr></table></td></tr>";


}
   }
mySS.closeAll();
endCount = cnt;



if (startID>=endCount)
{
startID = endCount-maxResults;

}

if (maxResults<=0)
{
maxResults=10;
}


if (startID<=0)
{
startID=0;
}




%>
  <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</p>
<table width=100% border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
<tr>
<td width=10>&nbsp;</td>
<td>
<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
<tr>
<td>

<form name="selfForm" method="POST" action="tNIM3_CommTrack_query.jsp">
  <table border=0 cellspacing=2 width='100%' cellpadding=2>
    <tr > 
      <td colspan=3 class=title><p>Search</p></td>
      <td colspan=1 class=tdBase align=right>
        <%if (startID>0)
{%><a href="tNIM3_CommTrack_query.jsp?startID=<%=(startID-maxResults)%>&amp;maxResults=<%=maxResults%>&amp;orderBy=<%=orderBy%>&amp;ExtUserID=<%=myExtUserID%>&amp;CommTypeID=<%=myCommTypeID%>&amp;CommStart=<%=myCommStart%>&amp;CommEnd=<%=myCommEnd%>&amp;MessageText=<%=myMessageText%>&amp;MessageName=<%=myMessageName%>&amp;MessageCompany=<%=myMessageCompany%>&amp;MessageEmail=<%=myMessageEmail%>&amp;MessagePhone=<%=myMessagePhone%>&amp;MessageFax=<%=myMessageFax%>&amp;AlertStatusCode=<%=myAlertStatusCode%>&amp;Comments=<%=myComments%>"><< previous <%=maxResults%></a> 
          <%
}
else
{
%><p>&nbsp;</p><%
}
%></td>
      <td colspan=1 class=tdBase> 
        <%
if ((startID+maxResults)<endCount)
{
%>
<a href="tNIM3_CommTrack_query.jsp?startID=<%=(startID+maxResults)%>&amp;maxResults=<%=maxResults%>&amp;orderBy=<%=orderBy%>&amp;ExtUserID=<%=myExtUserID%>&amp;CommTypeID=<%=myCommTypeID%>&amp;CommStart=<%=myCommStart%>&amp;CommEnd=<%=myCommEnd%>&amp;MessageText=<%=myMessageText%>&amp;MessageName=<%=myMessageName%>&amp;MessageCompany=<%=myMessageCompany%>&amp;MessageEmail=<%=myMessageEmail%>&amp;MessagePhone=<%=myMessagePhone%>&amp;MessageFax=<%=myMessageFax%>&amp;AlertStatusCode=<%=myAlertStatusCode%>&amp;Comments=<%=myComments%>"> next >> <%=maxResults%></a>         
        <%
}
else
{
%><p>&nbsp;</p><%
}


}
catch(Exception e)
{
out.println("PrevNext:"+e);
}




try{

%>
      </td>
      <td colspan=4  nowrap> 
<input type=hidden name=maxResults value=<%=maxResults%> >
<input type=hidden name=startID value=0 >
<p><strong>Message:</strong>
<input name="MessageText" type="text" id="MessageText" value="<%=myMessageText%>" size="6">
</p>
<p> <strong>Comments:</strong>
<input name="Comments" type="text" id="Comments" value="<%=myComments%>" size="6">
</p>
      </td> 
      <td class=tdHeader colspan=2> 
        <p  align="center"> 
          Sort:
            <select name=orderBy id="orderBy">
              <option value=ExtUserID <%if (orderBy.equalsIgnoreCase("ExtUserID")){out.println(" selected");}%> >ExtUserID</option>
              <option value=CommTypeID <%if (orderBy.equalsIgnoreCase("CommTypeID")){out.println(" selected");}%> >CommTypeID</option>
              <option value=CommStart <%if (orderBy.equalsIgnoreCase("CommStart")){out.println(" selected");}%> >CommStart</option>
              <option value=CommEnd <%if (orderBy.equalsIgnoreCase("CommEnd")){out.println(" selected");}%> >CommEnd</option>
              <option value=MessageText <%if (orderBy.equalsIgnoreCase("MessageText")){out.println(" selected");}%> >MessageText</option>
              <option value=MessageName <%if (orderBy.equalsIgnoreCase("MessageName")){out.println(" selected");}%> >MessageName</option>
              <option value=MessageCompany <%if (orderBy.equalsIgnoreCase("MessageCompany")){out.println(" selected");}%> >MessageCompany</option>
              <option value=MessageEmail <%if (orderBy.equalsIgnoreCase("MessageEmail")){out.println(" selected");}%> >MessageEmail</option>
              <option value=MessagePhone <%if (orderBy.equalsIgnoreCase("MessagePhone")){out.println(" selected");}%> >MessagePhone</option>
              <option value=MessageFax <%if (orderBy.equalsIgnoreCase("MessageFax")){out.println(" selected");}%> >MessageFax</option>
              <option value=AlertStatusCode <%if (orderBy.equalsIgnoreCase("AlertStatusCode")){out.println(" selected");}%> >AlertStatusCode</option>
              <option value=Comments <%if (orderBy.equalsIgnoreCase("Comments")){out.println(" selected");}%> >Comments</option>
            </select>
        </p>
        <p  align="center">
          <input type="Submit" name="Submit" value="Submit" align="middle">
        </p>
      </td>
    </tr>
    <tr class=tdHeader> 
    <td>#</td>
      <td colspan=1> 
        Ext. User ID
      </td>
      <td colspan=1> 
        Type
      </td>
      <td colspan=1> 
        Start
      </td>
      <td colspan=1> 
        End
      </td>
      <td colspan=1> 
        Name
      </td>
      <td colspan=1> 
        Company
      </td>
      <td colspan=1> 
        Email
      </td>
      <td colspan=1> 
        Phone
      </td>
      <td colspan=1> 
        Fax
      </td>
      <td colspan=1> 
        Status</td>
    </tr>
    <tr class=tdHeader> 
      <td  colspan=1 width=50>&nbsp; 

      </td>
      <td colspan=1> 
        <input type="text" name="ExtUserID" value="<%=myExtUserID%>" size="2">
      </td>
      <td colspan=1> 
        <input type="text" name="CommTypeID" value="<%=myCommTypeID%>" size="2">
      </td>
      <td colspan=1 nowrap> 
        &gt;
        <input type="text" name="CommStart" value="<%=myCommStart%>" size="6">
      </td>
      <td colspan=1 nowrap> 
        &gt;
        <input type="text" name="CommEnd" value="<%=myCommEnd%>" size="6">
      </td>
      <td colspan=1> 
        <input type="text" name="MessageName" value="<%=myMessageName%>" size="6">
      </td>
      <td colspan=1> 
        <input type="text" name="MessageCompany" value="<%=myMessageCompany%>" size="6">
      </td>
      <td colspan=1> 
        <input type="text" name="MessageEmail" value="<%=myMessageEmail%>" size="6">
      </td>
      <td colspan=1> 
        <input type="text" name="MessagePhone" value="<%=myMessagePhone%>" size="4">
      </td>
      <td colspan=1> 
        <input type="text" name="MessageFax" value="<%=myMessageFax%>" size="4">
      </td>
      <td colspan=1> 
        <input type="text" name="AlertStatusCode" value="<%=myAlertStatusCode%>" size="4">
      </td>
    </tr>
<tr><td colspan=11 bgcolor="#000000">&nbsp;</td></tr>
    <%=myMainTable%>



</table> 


<%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>



  </table>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </td>
  </tr>
</table>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>
    <br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>