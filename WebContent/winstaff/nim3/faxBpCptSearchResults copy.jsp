<%@page language="java" contentType="application/json; charset=UTF-8" import="java.io.*,com.winstaff.*, java.util.*, java.sql.*,com.google.gson.Gson;"%>
<%!class uaResult {
		String value;
		String id;

		public uaResult(String value, String id) {
			this.value = value;
			this.id = id;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}
	}%>
<%
	List<uaResult> uaResultList = new ArrayList<uaResult>();

	if (request.getParameter("term") != null && !request.getParameter("term").isEmpty()) {
		String search = request.getParameter("term");

		String[] sParams = search.split(" ");
		String whereClause = "";
		boolean isFirst = true;
		for(String s : sParams){
			whereClause +=(isFirst ? " lower(description) ~ lower('"+s+"') ":" and lower(description) ~ lower('"+s+"') ");
			isFirst = false;
		}
		
		String query = "SELECT\n" +
				"	regexp_replace(regexp_replace(regexp_matches(cpt, '([7,9][0-9]{4})', 'g')::varchar,'{',''),'}','') cpt, description||' ('||cpt||')' bp\n" +
				"from tcptli\n" +
				"where " + whereClause +
				"order by cpt \n" +
				"limit 15";
		searchDB2 db = new searchDB2();

		ResultSet rs = db.executeStatement(query);

		try {
			while (rs.next()) {
				uaResultList.add(new uaResult(rs.getString(2), rs.getString(1)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closeAll();
		}
	}
	response.setHeader("Access-Control-Allow-Origin", "*");
	
	out.println(new Gson().toJson(uaResultList));
%>
