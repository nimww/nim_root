<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*,org.apache.commons.fileupload.*, org.apache.commons.fileupload.servlet.ServletFileUpload, org.apache.commons.fileupload.disk.DiskFileItemFactory, org.apache.commons.io.FilenameUtils, java.util.*, java.io.File, java.lang.Exception, java.io.BufferedReader, java.io.FileReader,java.text.*" errorPage="" %>
<!DOCTYPE html>
<head><title>Payable Upload</title></head>

<%@ include file="../generic/CheckLogin.jsp" %>

<%String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?iDelay=-1&plcID="+thePLCID;%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<%
boolean accessValid = false;
if (isScheduler){
	accessValid = true;
}
%>

<body>
<%if (accessValid){%>
	<h3>Payable Upload</h3>
    <p>

<%
boolean upload = false;

try{
	request.getParameter("file").equals("yes");
	upload = true;
}
catch(Exception e){}

if(upload){

	if (ServletFileUpload.isMultipartContent(request)){
	  ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
	  List fileItemsList = servletFileUpload.parseRequest(request);
	
	  String optionalFileName = "";
	  FileItem fileItem = null;
	  
	  Iterator it = fileItemsList.iterator();
	  while (it.hasNext()){
		FileItem fileItemTemp = (FileItem)it.next();
		if (fileItemTemp.isFormField()){
			
		  if (fileItemTemp.getFieldName().equals("filename"))
			optionalFileName = fileItemTemp.getString();
		}
		else
		  fileItem = fileItemTemp;
	  }
	
	  if (fileItem!=null){
		String fileName = fileItem.getName();
	%>
	
	<b>Uploaded File Info:</b><br/>
	Content type: <%= fileItem.getContentType() %><br/>
	Field name: <%= fileItem.getFieldName() %><br/>
	File size: <%= fileItem.getSize() %><br/><br/>
	
	<%
		/* Save the uploaded file if its size is greater than 0. */
		if (fileItem.getSize() > 0){
		if (optionalFileName.trim().equals("")){
		fileName = FilenameUtils.getName(fileName);
		}
		else{
		fileName = optionalFileName;
		}
		
		String dirName = "/var/lib/tomcat7/webapps/ROOT/WEB-INF/classes/check_run/";
		
		File saveTo = new File(dirName + fileName);
		try {
		fileItem.write(saveTo);
		
		String line = null;
		String print = "test";
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader(dirName + fileName));
		
		while ((line = bufferedReader.readLine()) != null) {
			String dataValue[] = line.replaceAll("\\$","").replaceAll("\"","").replaceAll(",","").split("\t");
			/* dataValue[] definitions:
			 * dataValue[1] - Scanpass
			 * dataValue[3] - Check Amount
			 * dataValue[2] - Check Number
			 * dataValue[4] - Check Date
			 */
			try{
				
				searchDB2 sql = new searchDB2();
				String query = "select encounterid from tnim3_encounter where scanpass = '"+dataValue[1].trim()+"' ";
				ResultSet results = sql.executeStatement(query);
				String value = "";
				
				while (results!=null&&results.next()){
					value = results.getString("encounterid");
				}
				results.close();
				
				NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(new Integer(value),"Load");
				
				if (myEO2.getNIM3_Encounter().getPaidToProviderCheck1Number().equals(dataValue[2]) || myEO2.getNIM3_Encounter().getPaidToProviderCheck2Number().equals(dataValue[2]) || myEO2.getNIM3_Encounter().getPaidToProviderCheck3Number().equals(dataValue[2])){
					out.print(dataValue[1]+" Failed - Duplicate check number.<br>");
				}
				else{
					if(myEO2.getNIM3_Encounter().getPaidToProviderCheck1Number().equals("")){
						myEO2.getNIM3_Encounter().setPaidToProviderCheck1Amount(new Double(dataValue[3]));
						myEO2.getNIM3_Encounter().setPaidToProviderCheck1Number(dataValue[2]);
						myEO2.getNIM3_Encounter().setPaidToProviderCheck1Date(new SimpleDateFormat("MM-dd-yyyy").parse(dataValue[4]));
						myEO2.getNIM3_Encounter().commitData();
						out.print(dataValue[1]+" Success -> Check 1. ");
						
						java.util.Date myCompleteDatePayOut = NIMUtils.isPayOutComplete(new Integer(value));
						if (myCompleteDatePayOut!=null&&myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidOut().before(new java.util.Date(1)))
						{
							out.print("Payment Out: Complete.<br>");
							myEO2.getNIM3_Encounter().setTimeTrack_ReqPaidOut(myCompleteDatePayOut);
							myEO2.getNIM3_Encounter().setTimeTrack_ReqPaidOut_UserID(CurrentUserAccount.getUserID());
						}
						else {out.print("Payment Out: Not Complete.<br>");}
							myEO2.getNIM3_Encounter().commitData();
						}
					else if (myEO2.getNIM3_Encounter().getPaidToProviderCheck2Number().equals("")){
						myEO2.getNIM3_Encounter().setPaidToProviderCheck2Amount(new Double(dataValue[3]));
						myEO2.getNIM3_Encounter().setPaidToProviderCheck2Number(dataValue[2]);
						myEO2.getNIM3_Encounter().setPaidToProviderCheck2Date(new SimpleDateFormat("MM-dd-yyyy").parse(dataValue[4]));
						myEO2.getNIM3_Encounter().commitData();
						out.print(dataValue[1]+" Success -> Check2. ");
						
						java.util.Date myCompleteDatePayOut = NIMUtils.isPayOutComplete(new Integer(value));
						if (myCompleteDatePayOut!=null&&myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidOut().before(new java.util.Date(1)))
						{
							out.print("Payment Out: Complete.<br>");
							myEO2.getNIM3_Encounter().setTimeTrack_ReqPaidOut(myCompleteDatePayOut);
							myEO2.getNIM3_Encounter().setTimeTrack_ReqPaidOut_UserID(CurrentUserAccount.getUserID());
						}
						else {out.print("Payment Out: Not Complete.<br>");}
							myEO2.getNIM3_Encounter().commitData();
						}
					else if (myEO2.getNIM3_Encounter().getPaidToProviderCheck3Number().equals("")){
						myEO2.getNIM3_Encounter().setPaidToProviderCheck3Amount(new Double(dataValue[3]));
						myEO2.getNIM3_Encounter().setPaidToProviderCheck3Number(dataValue[2]);
						myEO2.getNIM3_Encounter().setPaidToProviderCheck3Date(new SimpleDateFormat("MM-dd-yyyy").parse(dataValue[4]));
						myEO2.getNIM3_Encounter().commitData();
						out.print(dataValue[1]+" Success -> Check3. ");
						
						java.util.Date myCompleteDatePayOut = NIMUtils.isPayOutComplete(new Integer(value));
						if (myCompleteDatePayOut!=null&&myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidOut().before(new java.util.Date(1)))
						{
							out.print("Payment Out: Complete.<br>");
							myEO2.getNIM3_Encounter().setTimeTrack_ReqPaidOut(myCompleteDatePayOut);
							myEO2.getNIM3_Encounter().setTimeTrack_ReqPaidOut_UserID(CurrentUserAccount.getUserID());
						}
						else {out.print("Payment Out: Not Complete.<br>");}
							myEO2.getNIM3_Encounter().commitData();
						}
					 else{
						out.print(dataValue[1]+" Failed - No available check slots.<br>");
					}
					
				}
			}
			catch(Exception e){
				out.print(dataValue[1]+" Failed - Bad Scanpass.<br>"+e);
			}
		}
		bufferedReader.close();

		  }
		catch (Exception e){%>
			<b>An error occurred uploading file. - <%=e%></b>
		<%}
		}
	  }
	}
}
else{%>
	<form  action="check_run.jsp?file=yes"  method="post"  enctype="multipart/form-data">
        <input type="file" name="file1">
        <input type="hidden" size="20" name="file" value="yes">
		<br>
        <input type=submit value="Upload">
    </form>
<%}%>
    </p>
<%}
else{
	out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");//if (accessvaild)
}%>
</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>