<%@page contentType="application/pdf" language="java"
	import="com.winstaff.*"%><%@ include file="../generic/CheckLogin.jsp"%>
	<%@ page trimDirectiveWhitespaces="true" %>
<%
try
{

//initial declaration of list class and parentID
    Integer        iReferralID        =    null;
    Integer        iEncounterID        =    null;
    boolean accessValid = false;
	if (pageControllerHash.containsKey("iEncounterID")) 
    {
	    iEncounterID = (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
	  bltNIM3_Encounter myE = new bltNIM3_Encounter(iEncounterID);
	  bltNIM3_Referral myR = new bltNIM3_Referral(myE.getReferralID());
	  bltNIM3_Document myD = null;
	  if (request.getParameter("EDIT").equalsIgnoreCase("rx"))
	  {
		  myD = new bltNIM3_Document(myR.getRxFileID());	  
	  }
	  else if (request.getParameter("EDIT").equalsIgnoreCase("or"))
	  {
		  myD = new bltNIM3_Document(myR.getOrderFileID());  		  
	  }
	  else if (request.getParameter("EDIT").equalsIgnoreCase("rp"))
	  {
		  myD = new bltNIM3_Document(myE.getReportFileID());	  
	  }
	 
			
//out.println(sFileName);
//out.println(sDownloadName);
//out.println(bDownload);
//		String myRPath = "/var/lib/tomcat5/webapps/nimdox/";

		String myRPath = ConfigurationInformation.sUploadFolderDirectory;
        String sFileName        =    myRPath + myD.getFileName();

if (true)
{
	//response.setHeader("Content-Disposition", "attachment; filename="+sFileName  ); 
}
else
{
	//response.setHeader("Content-Disposition", "inline; filename=test.pdf" ); 
}


if (ConfigurationInformation.storeDocumentsInDatabase) {

	// Get BLOB data from database for this specific filename.
	DocumentDatabase db = new DocumentDatabase();
	byte [] data = db.readFile(myD.getFileName());
	response.getOutputStream().write(data);

} else {
	
	// -- Original call.  Get from file system.
	response.getOutputStream().write(DataControlUtils.FileToBytes(sFileName));
}

  }
  else
  {
   out.println("illegal");
  }

}
catch(Exception e)
{
	out.print("Error " + e);
}

%>