<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    filename: tnim2_Authorization_main_NIM_Authorization_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


<%
//initial declaration of list class and parentID
//    Integer        iPayerID        =    null;
	Integer	iUserID = null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
//    if (pageControllerHash.containsKey("iPayerID")&&isScheduler) 
    {
//        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
    }
	int iOpenID = 0;
	String sOpenField = "";
	try
	{
		iOpenID= Integer.parseInt(request.getParameter("ssOpenID"));
		sOpenField= (request.getParameter("ssOpenField"));
		if (request.getParameter("EDITID")!=null) 
		{
			iUserID        =    new Integer(request.getParameter("EDITID"));
		}
	}
	catch(Exception e)
	{
		accessValid = false;
	}

//page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltUserAccount        UserAccount        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        UserAccount        =    new    bltUserAccount(iUserID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        UserAccount        =    new    bltUserAccount(UserSecurityGroupID,true);
    }



String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !UserAccount.getUniqueCreateDate().equals(testObj)  )
    {
         UserAccount.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !UserAccount.getUniqueModifyDate().equals(testObj)  )
    {
         UserAccount.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !UserAccount.getUniqueModifyComments().equals(testObj)  )
    {
         UserAccount.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PLCID")) ;
    if ( !UserAccount.getPLCID().equals(testObj)  )
    {
         UserAccount.setPLCID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount PLCID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("StartPage")) ;
    if ( !UserAccount.getStartPage().equals(testObj)  )
    {
         UserAccount.setStartPage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount StartPage not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AccountType")) ;
    if ( !UserAccount.getAccountType().equals(testObj)  )
    {
         UserAccount.setAccountType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }
}
catch(Exception e)
{
     //out.println("UserAccount AccountType not set. this is ok-not an error");
}
if (iOpenID==1)
{
	UserAccount.setAccountType("PhysicianID"); 
}
else if (iOpenID==2)
{
	UserAccount.setAccountType("AdjusterID"); 
}



try
{
    Integer testObj = new Integer(request.getParameter("SecurityGroupID")) ;
    if ( !UserAccount.getSecurityGroupID().equals(testObj)  )
    {
         UserAccount.setSecurityGroupID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount SecurityGroupID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("GenericSecurityGroupID")) ;
    if ( !UserAccount.getGenericSecurityGroupID().equals(testObj)  )
    {
         UserAccount.setGenericSecurityGroupID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount GenericSecurityGroupID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReferenceID")) ;
    if ( !UserAccount.getReferenceID().equals(testObj)  )
    {
//         UserAccount.setReferenceID( testObj,UserSecurityGroupID   );
         UserAccount.setPayerID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ReferenceID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AccessType")) ;
    if ( !UserAccount.getAccessType().equals(testObj)  )
    {
         UserAccount.setAccessType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount AccessType not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Status")) ;
    if ( !UserAccount.getStatus().equals(testObj)  )
    {
         UserAccount.setStatus( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Status not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LogonUserName")) ;
    if ( !UserAccount.getLogonUserName().equals(testObj)  )
    {
//         UserAccount.setLogonUserName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount LogonUserName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LogonUserPassword")) ;
    if ( !UserAccount.getLogonUserPassword().equals(testObj)  )
    {
         UserAccount.setLogonUserPassword( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }
}
catch(Exception e)
{
     //out.println("UserAccount LogonUserPassword not set. this is ok-not an error");
}

String myNewPassword = "";

try
{
	com.winstaff.password.Encrypt myEnc = new com.winstaff.password.Encrypt();
    
	myNewPassword = com.winstaff.password.Encrypt.getRandomPassword(8, true);
	UserAccount.setLogonUserPassword( myEnc.getMD5Base64(myNewPassword) );
}
catch(Exception e)
{
     //out.println("UserAccount LogonUserPassword not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttestKeyword1")) ;
    if ( !UserAccount.getAttestKeyword1().equals(testObj)  )
    {
         UserAccount.setAttestKeyword1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount AttestKeyword1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttestKeyword2")) ;
    if ( !UserAccount.getAttestKeyword2().equals(testObj)  )
    {
         UserAccount.setAttestKeyword2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount AttestKeyword2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttestKeywordTemp1")) ;
    if ( !UserAccount.getAttestKeywordTemp1().equals(testObj)  )
    {
         UserAccount.setAttestKeywordTemp1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount AttestKeywordTemp1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttestKeywordTemp2")) ;
    if ( !UserAccount.getAttestKeywordTemp2().equals(testObj)  )
    {
         UserAccount.setAttestKeywordTemp2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount AttestKeywordTemp2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactFirstName")) ;
    if ( !UserAccount.getContactFirstName().equals(testObj)  )
    {
         UserAccount.setContactFirstName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactLastName")) ;
    if ( !UserAccount.getContactLastName().equals(testObj)  )
    {
         UserAccount.setContactLastName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !UserAccount.getContactEmail().equals(testObj)  )
    {
         UserAccount.setContactEmail( testObj,UserSecurityGroupID   );
         UserAccount.setLogonUserName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail2")) ;
    if ( !UserAccount.getContactEmail2().equals(testObj)  )
    {
         UserAccount.setContactEmail2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactAddress1")) ;
    if ( !UserAccount.getContactAddress1().equals(testObj)  )
    {
         UserAccount.setContactAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactAddress2")) ;
    if ( !UserAccount.getContactAddress2().equals(testObj)  )
    {
         UserAccount.setContactAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactCity")) ;
    if ( !UserAccount.getContactCity().equals(testObj)  )
    {
         UserAccount.setContactCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ContactStateID")) ;
    if ( !UserAccount.getContactStateID().equals(testObj)  )
    {
         UserAccount.setContactStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactProvince")) ;
    if ( !UserAccount.getContactProvince().equals(testObj)  )
    {
         UserAccount.setContactProvince( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactProvince not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactZIP")) ;
    if ( !UserAccount.getContactZIP().equals(testObj)  )
    {
         UserAccount.setContactZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactZIP not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ContactCountryID")) ;
    if ( !UserAccount.getContactCountryID().equals(testObj)  )
    {
         UserAccount.setContactCountryID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactCountryID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactPhone")) ;
    if ( !UserAccount.getContactPhone().equals(testObj)  )
    {
         UserAccount.setContactPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactFax")) ;
    if ( !UserAccount.getContactFax().equals(testObj)  )
    {
         UserAccount.setContactFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactMobile")) ;
    if ( !UserAccount.getContactMobile().equals(testObj)  )
    {
         UserAccount.setContactMobile( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ContactMobile not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("CompanyFax")) ;
    if ( !UserAccount.getCompanyFax().equals(testObj)  )
    {
         UserAccount.setCompanyFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyFax not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("SecretQuestion")) ;
    if ( !UserAccount.getSecretQuestion().equals(testObj)  )
    {
         UserAccount.setSecretQuestion( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount SecretQuestion not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SecretAnswer")) ;
    if ( !UserAccount.getSecretAnswer().equals(testObj)  )
    {
         UserAccount.setSecretAnswer( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount SecretAnswer not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardFullName")) ;
    if ( !UserAccount.getCreditCardFullName().equals(testObj)  )
    {
         UserAccount.setCreditCardFullName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardFullName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardNumber")) ;
    if ( !UserAccount.getCreditCardNumber().equals(testObj)  )
    {
         UserAccount.setCreditCardNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardNumber not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardType")) ;
    if ( !UserAccount.getCreditCardType().equals(testObj)  )
    {
         UserAccount.setCreditCardType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardType not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardExpMonth")) ;
    if ( !UserAccount.getCreditCardExpMonth().equals(testObj)  )
    {
         UserAccount.setCreditCardExpMonth( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardExpMonth not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardExpYear")) ;
    if ( !UserAccount.getCreditCardExpYear().equals(testObj)  )
    {
         UserAccount.setCreditCardExpYear( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardExpYear not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("CreditCardChargeDate"))) ;
    if ( !UserAccount.getCreditCardChargeDate().equals(testObj)  )
    {
         UserAccount.setCreditCardChargeDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardChargeDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("CreditCardPostDate"))) ;
    if ( !UserAccount.getCreditCardPostDate().equals(testObj)  )
    {
         UserAccount.setCreditCardPostDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardPostDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AlertEmail")) ;
    if ( !UserAccount.getAlertEmail().equals(testObj)  )
    {
         UserAccount.setAlertEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount AlertEmail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AlertDays")) ;
    if ( !UserAccount.getAlertDays().equals(testObj)  )
    {
         UserAccount.setAlertDays( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount AlertDays not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingComments")) ;
    if ( !UserAccount.getBillingComments().equals(testObj)  )
    {
         UserAccount.setBillingComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount BillingComments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PromoCode")) ;
    if ( !UserAccount.getPromoCode().equals(testObj)  )
    {
         UserAccount.setPromoCode( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount PromoCode not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CompanyType")) ;
    if ( !UserAccount.getCompanyType().equals(testObj)  )
    {
         UserAccount.setCompanyType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyType not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CompanyName")) ;
    if ( !UserAccount.getCompanyName().equals(testObj)  )
    {
         UserAccount.setCompanyName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CompanyAddress1")) ;
    if ( !UserAccount.getCompanyAddress1().equals(testObj)  )
    {
         UserAccount.setCompanyAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CompanyAddress2")) ;
    if ( !UserAccount.getCompanyAddress2().equals(testObj)  )
    {
         UserAccount.setCompanyAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CompanyCity")) ;
    if ( !UserAccount.getCompanyCity().equals(testObj)  )
    {
         UserAccount.setCompanyCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CompanyStateID")) ;
    if ( !UserAccount.getCompanyStateID().equals(testObj)  )
    {
         UserAccount.setCompanyStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CompanyZIP")) ;
    if ( !UserAccount.getCompanyZIP().equals(testObj)  )
    {
         UserAccount.setCompanyZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyZIP not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CompanyPhone")) ;
    if ( !UserAccount.getCompanyPhone().equals(testObj)  )
    {
         UserAccount.setCompanyPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !UserAccount.getComments().equals(testObj)  )
    {
         UserAccount.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("BillingID")) ;
    if ( !UserAccount.getBillingID().equals(testObj)  )
    {
         UserAccount.setBillingID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount BillingID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PHDBAcknowledgementStatus")) ;
    if ( !UserAccount.getPHDBAcknowledgementStatus().equals(testObj)  )
    {
         UserAccount.setPHDBAcknowledgementStatus( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount PHDBAcknowledgementStatus not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PHDBLetterHeadStatus")) ;
    if ( !UserAccount.getPHDBLetterHeadStatus().equals(testObj)  )
    {
         UserAccount.setPHDBLetterHeadStatus( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount PHDBLetterHeadStatus not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("TaxID")) ;
    if ( !UserAccount.getTaxID().equals(testObj)  )
    {
         UserAccount.setTaxID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount TaxID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("NotUsingPHDBID")) ;
    if ( !UserAccount.getNotUsingPHDBID().equals(testObj)  )
    {
         UserAccount.setNotUsingPHDBID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount NotUsingPHDBID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NotUsingPHDBComments")) ;
    if ( !UserAccount.getNotUsingPHDBComments().equals(testObj)  )
    {
         UserAccount.setNotUsingPHDBComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount NotUsingPHDBComments not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts_LevelUser")) ;
    if ( !UserAccount.getComm_Alerts_LevelUser().equals(testObj)  )
    {
         UserAccount.setComm_Alerts_LevelUser( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelUser not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts_LevelManager")) ;
    if ( !UserAccount.getComm_Alerts_LevelManager().equals(testObj)  )
    {
         UserAccount.setComm_Alerts_LevelManager( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelManager not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts_LevelBranch")) ;
    if ( !UserAccount.getComm_Alerts_LevelBranch().equals(testObj)  )
    {
         UserAccount.setComm_Alerts_LevelBranch( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelBranch not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts2_LevelUser")) ;
    if ( !UserAccount.getComm_Alerts2_LevelUser().equals(testObj)  )
    {
         UserAccount.setComm_Alerts2_LevelUser( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelUser not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts2_LevelManager")) ;
    if ( !UserAccount.getComm_Alerts2_LevelManager().equals(testObj)  )
    {
         UserAccount.setComm_Alerts2_LevelManager( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelManager not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts2_LevelBranch")) ;
    if ( !UserAccount.getComm_Alerts2_LevelBranch().equals(testObj)  )
    {
         UserAccount.setComm_Alerts2_LevelBranch( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelBranch not set. this is ok-not an error");
}





try
{
    Integer testObj = new Integer(request.getParameter("Comm_Report_LevelUser")) ;
    if ( !UserAccount.getComm_Report_LevelUser().equals(testObj)  )
    {
         UserAccount.setComm_Report_LevelUser( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Report_LevelUser not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Report_LevelManager")) ;
    if ( !UserAccount.getComm_Report_LevelManager().equals(testObj)  )
    {
         UserAccount.setComm_Report_LevelManager( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Report_LevelManager not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Report_LevelBranch")) ;
    if ( !UserAccount.getComm_Report_LevelBranch().equals(testObj)  )
    {
         UserAccount.setComm_Report_LevelBranch( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Report_LevelBranch not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UserNPI")) ;
    if ( !UserAccount.getUserNPI().equals(testObj)  )
    {
         UserAccount.setUserNPI( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UserNPI not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UserStateLicense")) ;
    if ( !UserAccount.getUserStateLicense().equals(testObj)  )
    {
         UserAccount.setUserStateLicense( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UserStateLicense not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UserStateLicenseDesc")) ;
    if ( !UserAccount.getUserStateLicenseDesc().equals(testObj)  )
    {
         UserAccount.setUserStateLicenseDesc( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UserStateLicenseDesc not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("ImportantNotes_Alert")) ;
    if ( !UserAccount.getImportantNotes_Alert().equals(testObj)  )
    {
         UserAccount.setImportantNotes_Alert( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ImportantNotes_Alert not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("EmailAlertNotes_Alert")) ;
    if ( !UserAccount.getEmailAlertNotes_Alert().equals(testObj)  )
    {
         UserAccount.setEmailAlertNotes_Alert( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount EmailAlertNotes_Alert not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("ImportantNotes")) ;
    if ( !UserAccount.getImportantNotes().equals(testObj)  )
    {
         UserAccount.setImportantNotes( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ImportantNotes not set. this is ok-not an error");
}




boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	UserAccount.setUniqueModifyDate(PLCUtils.getNowDate(false));
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}


	if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
		if (NIMUtils.isValidNewUser(UserAccount))
		{
		
			UserAccount.setUniqueModifyComments(UserLogonDescription);
			try
			{
				UserAccount.setUniqueModifyComments(UserLogonDescription);
				UserAccount.setUniqueCreateDate(PLCUtils.getNowDate(false));
				UserAccount.setSecurityGroupID(new Integer(2));
				UserAccount.setGenericSecurityGroupID(new Integer(1));
				UserAccount.setStatus(new Integer(2));
				UserAccount.setAccessType(new Integer(2));
				if (UserAccount.getAccountType().equalsIgnoreCase("ICID")){
					UserAccount.setStartPage("../nim3/IC_Home.jsp"); 
				} else {
					UserAccount.setStartPage("../nim3/Payer_Home.jsp"); 
				}
				UserAccount.setPLCID(new Integer(200));
				UserAccount.commitData();
	
				if (!UserAccount.getContactEmail().equalsIgnoreCase("Declined")&&NIMUtils.sendNewUserAccountEmail(UserAccount,myNewPassword,false, false))
				{
					%>
						<script language=Javascript>
                            alert("Created.");
 							window.returnValue = '<%=UserAccount.getUniqueID()%>';
							window.close();
                        </script>            
					<%
				}
				else if (!UserAccount.getContactEmail().equalsIgnoreCase("Declined"))
				{
				%>
						<script language=Javascript>
                            alert("Error: Email send failed.");
                        </script>            
				<%
				}
	
	
			}
			catch (Exception e55)
			{
				out.print("Commit Error" + e55);
				errorRouteMe = true;
			}
		}
		else
		{
				errorRouteMe = true;
			%>
				<script language=Javascript>
					alert("That email already exists, please verify that this user does not exist before adding them.");
					window.history.go( -2 );
                </script>          
                <input type="button" onClick="history.back();"  value="back">
                <a href="javascript:history.back(-1)">test</a>
            <%
		}
			
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}

nextPage = "LI_tUserAccount_Search.jsp?ssOpenID=" + iOpenID + "&ssOpenField=" + sOpenField + "&UserID=" + UserAccount.getUniqueID()  + "&LogonUserName=&ContactFirstName=" + UserAccount.getContactFirstName() + "&ContactLastName=&ContactPhone=&ContactEmail=&ContactCity=&ContactStateID=0&ContactZIP=&AccountType=&ReferenceID=&orderBy=UserID&startID=0&maxResults=50&Submit2=Submit";
//out.print(nextPage);
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
//	      document.location="<%=nextPage%>";
		window.returnValue = '<%=UserAccount.getUniqueID()%>';
		window.close();
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
y