<!DOCTYPE HTML>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%@page contentType="text/html" language="java" import="com.winstaff.*    " %>
<%/*
    filename: tNIM3_Referral_main_NIM3_Referral_CaseID.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
 <script language="javascript">	
 	function cc_all(eid, scanpass_type){
 		var dataString = "eID="+eid+"&scanpass_type="+scanpass_type;
 		$.ajax({
			type: "GET",
			url: "cc_all.jsp",
			data: dataString,
			success: function() {
				alert("Great Success!");
				location.reload();
			}
		});
 	}
	function deleteReferralDoc(arg1,arg2) {				
		if (confirm('Delete this document?')){		
			var str1 = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp?EDITID=";
			var result = str1.concat(arg1,"&action='referral'","&type='",arg2,"'");				
			document.location=result;
			return;
		} 
	}
	function deleteEncounterDoc(arg1,arg2) {				
		if (confirm('Delete this document?')){		
			var str1 = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp?EDITID=";
			var result = str1.concat(arg1,"&action='encounter'","&type='",arg2,"'");				
			document.location=result;
			return;
		} 
	}
	function email_pt(eID, spType){
		var dataString = "eid="+eID+"&spType="+spType;
		$.ajax({
				type: "POST",
				url: "email_pt.jsp",
				data: dataString,
				success: function() {
					alert("Email Comfirmation to Patient Successful.");
					$("#"+spType).removeClass("inputButton_md_Action1").addClass("inputButton_md_Default");
				},
				error: function(){
					alert("Email Comfirmation to Patient Failed.");	
				}
		});
	}
	
	$(document).ready(function(e) {
    $(".ageCommentLock").click(function(){
		if (this.checked){
			var dataString = "ageComment=1&encounterID="+$(this).val();
			$.ajax({
				type: "GET",
				url: "ageComment.jsp",
				data: dataString,
				success: function() {
					//alert("Success");
				}
			});
		}
		else{
			var dataString = "ageComment=0&encounterID="+$(this).val();
			$.ajax({
				type: "GET",
				url: "ageComment.jsp",
				data: dataString,
				success: function() {
					//alert("Success");
				}
			});
		}
	});
});		
</script>
<%--



 <script>
	function encDisplay(encounterID, display){
		if (display=='show'){
			$('#'+encounterID).show();
		}
		else {
			$('#'+encounterID).hide();
		}
	}
</script>
 --%>
<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />


<title>NextImage - Secure</title><table cellpadding=0 cellspacing=0 border=0 width=98% >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM3_Referral")%>

<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iCaseID")&&isScheduler) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }

    // -----------------------------------------------------------------------
    // Case of User deleting the active document associated with a REFERRAL
    // -----------------------------------------------------------------------
	if (request.getParameter("action")!=null&&!(request.getParameter("action").equalsIgnoreCase("referral"))){
		
		// EDITID is the current referral ID.
		String idStr = request.getParameter("EDITID");
		String type = request.getParameter("type");
		int i = new Integer(idStr).intValue();
	    bltNIM3_Referral NIM3_Referral = new bltNIM3_Referral(i);
	    
	    
	    // Need to parse type and zero out appropriate parameter
	    if (type.compareTo("'Or'")==0) {
	    	NIM3_Referral.setOrderFileID(0); 
	    } else if (type.compareTo("'Rx'")==0) {
	    	NIM3_Referral.setRxFileID(0);

	    } else {
	    	out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal delete state.")+"</p>");
	    }
		
	    NIM3_Referral.commitData();	
	    
		// Need to locate the current document and 'line that out'.
		// NIM3_Referral.setDocType(new Integer(-1));	
		searchDB2 mySS_dox = new searchDB2();
		try	{
			java.sql.ResultSet myRS_dox = null;
			String mySQL_dox = ("SELECT documentid from tnim3_document where referralid = " + NIM3_Referral.getReferralID() + " and docname = "+type+" order by uniquecreatedate desc");
			myRS_dox = mySS_dox.executeStatement(mySQL_dox);
			while(myRS_dox!=null&&myRS_dox.next()) {
				bltNIM3_Document myDoc = new bltNIM3_Document (new  Integer(myRS_dox.getString("documentid")));
				myDoc.setDocType(new Integer(-1));
				myDoc.commitData();
				// The first record is what we are after.  Leave this loop.
				break;
			}
		} catch(Exception e) {
			out.println("ResultsSet:"+e);
		}		
	}
    // -----------------------------------------------------------------------
    // Case of User deleting the active document associated with an ENCOUNTER
    // -----------------------------------------------------------------------      
	if (request.getParameter("action")!=null&&!(request.getParameter("action").equalsIgnoreCase("encounter"))){
		
		// EDITID is the current referral ID.
		String idStr = request.getParameter("EDITID");
		String type = request.getParameter("type");
		int i = new Integer(idStr).intValue();
		
		// Need to locate the current document and 'line that out'.
		// NIM3_Referral.setDocType(new Integer(-1));	
		searchDB2 mySS_dox = new searchDB2();
		try	{
			java.sql.ResultSet myRS_dox = null;
			String upperType = type.toUpperCase();
			String mySQL_dox = ("SELECT documentid from tnim3_document where encounterid = " + i + " and (comments = "+type+" OR comments = "+upperType+") order by uniquecreatedate desc");

			myRS_dox = mySS_dox.executeStatement(mySQL_dox);
			while(myRS_dox!=null&&myRS_dox.next()) {
				bltNIM3_Document myDoc = new bltNIM3_Document (new  Integer(myRS_dox.getString("documentid")));			
				myDoc.setDocType(new Integer(-1));
				myDoc.commitData();
				// The first record is what we are after.  Leave this loop.
				break;
			}
		} catch(Exception e) {
			out.println("ResultsSet:"+e);
		}		
		
		NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(new Integer(i),"In tNIM3_Referral_main_NIM3_Referral_CaseID.jsp : line 129");
  				
   		// Need to parse type and zero out appropriate parameter
   		if (type.compareTo("'Rp'")==0 || type.compareTo("'RP'")==0) {
			myEO2.getNIM3_Encounter().setReportFileID(0);
	  		myEO2.getNIM3_Encounter().commitData();				
	  	} else if (type.compareTo("'HCFA To Payer'")==0) {
   			myEO2.getNIM3_Encounter().setBillingHCFA_ToPayer_FileID(0);	
	  		myEO2.getNIM3_Encounter().commitData();	
 		} else if (type.compareTo("'HCFA From Provider'")==0) {
   			myEO2.getNIM3_Encounter().setBillingHCFA_FromProvider_FileID(0);
	  		myEO2.getNIM3_Encounter().commitData();	   			
   		} else {
  			out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal delete state.")+"</p>");
  		}
   		
	} // if (request.getParameter("action")
		
	String SelectedEncounterID = "";
	boolean isSelectedSent = false;
	if (request.getParameter("SelectedEncounterID")!=null&&!request.getParameter("SelectedEncounterID").equalsIgnoreCase(""))
	{
		SelectedEncounterID = request.getParameter("SelectedEncounterID");
		isSelectedSent = true;		
	}
	PoUtils.viewCaseLog((isSelectedSent ? new Integer(SelectedEncounterID):0), 0, iCaseID, CurrentUserAccount);
  //page security
  if (accessValid)
  {
    java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
    java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    pageControllerHash.put("sParentReturnPage","tNIM3_Referral_main_NIM3_Referral_CaseID.jsp");
    pageControllerHash.remove("iReferralID");
    pageControllerHash.remove("iEncounterID");
    pageControllerHash.put("sINTNext","tNIM3_Referral_main_NIM3_Referral_CaseID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);
	
    bltNIM3_Referral_List        bltNIM3_Referral_List        =    new    bltNIM3_Referral_List(iCaseID,"","ReferralID desc");

//declaration of Enumeration
    bltNIM3_Referral        working_bltNIM3_Referral;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Referral_List.elements();
    %>
<%
//		bltNIM3_CaseAccount myCA = new bltNIM3_CaseAccount(iCaseID);
//		bltUserAccount myAdj = new bltUserAccount(myCA.getAdjusterID());

		bltNIM3_CaseAccount NCA = new bltNIM3_CaseAccount(iCaseID);
		bltUserAccount myUAdj = new bltUserAccount(new Integer(NCA.getAdjusterID()));
//		bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(myUAdj.getPayerID());
		bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(NCA.getPayerID());
		bltNIM3_PayerMaster myPPM = new bltNIM3_PayerMaster(myPM.getParentPayerID());
		bltNIM3_PayerMaster adjPM = new bltNIM3_PayerMaster(new bltUserAccount(NCA.getAdjusterID()).getPayerID());
		
		
//		bltUserAccount myUAdm = new bltUserAccount(new Integer(NCA.getCaseAdministratorID()));
//		bltUserAccount myUNCM = new bltUserAccount(new Integer(NCA.getNurseCaseManagerID()));
//		bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(myUA.getPayerID());

		
//NCA.getAdjusterID()
		
		int tEcnt = 0;

		NIM3_BooleanAnalysisResultsObject myNBARO = NIMUtilsWorkflow.getCaseWarnings(NCA,"tNIM3_CaseAccount_PayerID_query.jsp?maxResults=20&startID=0&ssSearchMe==");

%>

	<%if (myPM.getPayerID()==139){%>
    
    <script>
	function submitTysogram(){
		var c = $("input[name=c]").val();
		var m = $("input[name=m]").val();
		var m_cpt = $("input[name=m_cpt]").val();
		var q = $("input[name=q]").val();
		var eID = $("input[name=eID]").val();
		var rID = $("input[name=rID]").val();
		
		var dataString = "c="+c+"&m="+m+"&m_cpt="+m_cpt+"&q="+q+"&eID="+eID+"&rID="+rID;
		
		if ( $('input[name=ic]').is(':checked') && $('input[name=proc]').is(':checked') && $('input[name=bside]').is(':checked') && $('input[name=bpart]').is(':checked') ){
			$.ajax({
				type: "POST",
				url: "tysogram_sub.jsp",
				data: dataString,
				success: function() {
					$("#tysogramWizard").hide();
					document.location.reload(true);
				},
				error: function(){
					alert("Tysogram Wizard Failed - Call Po");	
					$("#tysogramWizard").hide();
				}
			});
		}
		
	}
	
	function tysoID(eID, rID){
		var width = (window.innerWidth/2)-(320/2);
		var height = (window.innerHeight/2)-(147/2);
		
		$("input[name=eID]").val(eID);
		$("input[name=rID]").val(rID);
		$("#tysogramWizard").css({'left':width,'top':height}).show();
	}
	function closeTysogram(){
		$("#tysogramWizard").hide();
	}
	function mri(){
		var m_cpt;
		var q;
		var ic;
		//set IC
		$("input[name=c]").val($("input[name=ic]:checked").val());
		
		//set Qty
		if($(".b").is(":checked")){q = 2;}else{q = 1;}
		$("input[name=q]").val(q);
		
		//set Contrast 
		if($(".w").is(':checked')){	
				$('.m_cpt').text($('input[name=bpart]:checked').attr("cptw"));
				m_cpt = $('input[name=bpart]:checked').attr("cptw");
			}else{
				$('.m_cpt').text($('input[name=bpart]:checked').attr("cptwwo"));
				m_cpt = $('input[name=bpart]:checked').attr("cptwwo");
		}
		$("input[name=m_cpt]").val(m_cpt);
		
		//output selection
		if ( $('input[name=proc]').is(':checked')&&$('input[name=bside]').is(':checked')&&$('input[name=bpart]').is(':checked') ) {
			$('.m').text("MRI - "+$('input[name=bside]:checked').val()+" "+$('input[name=bpart]:checked').val()+" "+$('input[name=proc]:checked').val()+" ("+m_cpt+")");
		}
		
		//set BP 
		$("input[name=m]").val("MRI - "+$('input[name=bside]:checked').val()+" "+$('input[name=bpart]:checked').val()+" "+$('input[name=proc]:checked').val()+" ("+m_cpt+")");
	}

	</script>
    
    
	<!-- Tysogram Wizard Start -->
    <div id="tysogramWizard" style="position: fixed;width: 320px;height: 147px;background: white;border: 1px solid;padding: 10px;display:none;">
        <table>
        	<tr>
            	<td>Image Center:</td><td>Moline <input class="moline" type="radio" name="ic" onchange="mri();" value="4955"> Bettendorf <input class="moline" type="radio" name="ic" onchange="mri();" value="4169"></td>
            </tr>
            <tr>    
                <td>MRI:</td><td>With <input class="w" type="radio" name="proc" onchange="mri();" value="w/ contrast">  With/Without <input class="wwo" type="radio" name="proc" onchange="mri();" value="w_w/o contrast"></td>
            </tr>
            <tr>
                <td>Body Side:</td><td>Left: <input class="l" type="radio" name="bside" onchange="mri();" value="LT"> Both: <input class="b" type="radio" name="bside" onchange="mri();" value="Bilateral"> Right: <input class="r" type="radio" name="bside" onchange="mri();" value="RT"></td>
            </tr>
            <tr>    
                <td>Body Part:</td><td>Knee: <input class="k" type="radio" name="bpart" onchange="mri();" value="Knee" cptw="73719" cptwwo="73720"> Shoulder: <input class="s" type="radio" name="bpart" onchange="mri();" value="Shoulder" cptw="73222" cptwwo="73223"> Wrist: <input class="w" type="radio" name="bpart" onchange="mri();" value="Wrist" cptw="73222" cptwwo="73223"></td>
            </tr>
            <tr>
            	<td colspan="2"><br>
                	<span class="m"></span><br>
                    <input type="hidden" name="c">
                    <input type="hidden" name="m">
                    <input type="hidden" name="m_cpt">
                    <input type="hidden" name="q">
                    <input type="hidden" name="eID">
                    <input type="hidden" name="rID">
                    <input type="button" value="cancel" onclick="closeTysogram();">
                    <input type="button" value="submit" onclick="submitTysogram();">
        		</td>
            </tr>
        </table>
    </div>
    <!-- Tysogram Wizard End -->
    <%}%>
    
<table border="0" cellpadding="1" cellspacing="0">


  <tr>
    <td align="right" class="tdBaseAlt">Claim #:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getCaseClaimNumber()%></strong></td>
    <td rowspan="2" align="right"><img src="images/child_line1.gif" /></td>
    
    <td align="right" class="tdBaseAlt">Patient:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getPatientFirstName()%>&nbsp;<%=NCA.getPatientLastName()%></strong></td>
    <td rowspan="2"><img src="images/child_line1.gif"></td>
    
    <td align="right" class="tdBaseAlt">Address:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getPatientAddress1()%><strong><br /><%=NCA.getPatientAddress2()%></strong></strong></td>    
    <td rowspan="2"><img src="images/child_line1.gif"></td>
    
    <td align="right" class="tdBaseAlt" >Work Phone:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getPatientWorkPhone()%></strong></td>    
    <td rowspan="2"><img src="images/child_line1.gif"></td>
    
    <td align="right" class="tdBaseAlt">Assigned To:</td>
    <td class="tdBaseAlt"><strong><%=new bltUserAccount(NCA.getAssignedToID()).getLogonUserName()%></strong></td>    
    <td rowspan="2"><img src="images/child_line1.gif"></td>
    
    <td class="tdBaseAlt">Patient Availability:<br>
    <% searchDB2 mySSPTavail = new searchDB2();
	try
	{
		
		java.sql.ResultSet myRSPTavail = null;;
		String mySQLPTavail = "";
		
			mySQLPTavail = ("SELECT encounterid, tnim3_referral.referralid, patientavailability FROM tnim3_referral INNER JOIN tnim3_encounter on tnim3_encounter.referralid = tnim3_referral.referralid WHERE caseid = "+ iCaseID +" ORDER BY encounterid DESC");					
			//mySQLPTavail = ("select tnim3_encounther.patientavailability from tnim3_referral INNER JOIN tnim3_encounter on tnim3_encounter.referralid = tnim3_referral.referralid where caseid = '"+request.getParameter("EDITID")+"' order by encounterid desc;");					
		
		//out.println(mySQL);
		myRSPTavail = mySSPTavail.executeStatement(mySQLPTavail);
		boolean ptAvail = true;
		while (myRSPTavail!=null&&myRSPTavail.next())
		{			
			if(!myRSPTavail.getString("patientavailability").equals("") && ptAvail)
			{
				out.println("<strong>"+myRSPTavail.getString("patientavailability")+"</strong>");
				ptAvail = false;
			}
		}
		mySSPTavail.closeAll();
	}
	catch(Exception E){
		out.println("blah");
	}%>
    </td>
    <td class="tdBaseAlt">&nbsp;</td>
    <td rowspan="2" ><img src="images/child_line1.gif"></td>
    <td class="tdBaseAlt"><% if (myNBARO.getResult()) {%>Warnings<%}%></td>
    </tr>
  <tr>
    <td align="right" class="tdBaseAlt">Carrier:</td>
    <td class="tdBaseAlt"><strong><%=myPM.getPayerName()%></strong></td>
    <td align="right" class="tdBaseAlt">DOB:</td>
    <td class="tdBaseAlt"><strong><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NCA.getPatientDOB())%>" /></jsp:include></strong></td>
    <td align="right" class="tdBaseAlt">City, State:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getPatientCity()%>, <%=new bltStateLI(NCA.getPatientStateID()).getShortState()%> <%=NCA.getPatientZIP()%></strong></td>
    
    <td align="left" class="tdBaseAlt">Email:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getPatientEmail()%></strong></td>
    
    <td colspan="2" rowspan="3" class="tdBaseAlt"><textarea name="textarea" cols="30" rows="5" readonly class="borderHighlightBlackDottedNoFont" style="background-color:#EEE" ><%=NCA.getComments()%></textarea></td>
    <td colspan="2" rowspan="3" class="tdBaseAlt"><%if (!myPM.getImportantNotes().equalsIgnoreCase("")||!myPPM.getImportantNotes().equalsIgnoreCase("")){%><textarea name="textarea2" cols="35" rows="5" readonly class="borderHighlightBlackDottedNoFont tdBaseAlt_Action2"><%=myPM.getImportantNotes()%>
<%=myPPM.getImportantNotes()%></textarea><%}%></td>
<td rowspan="3" class="tdBaseAltRed"><%=myNBARO.getMessage("<br>")%></td>
    </tr>
  <tr>
    <td align="right" class="tdBaseAlt"><%if (myPM.getParentPayerID()!=951){%>Adjuster:<%}else{%>Referring Org:<%}%></td>
    <td class="tdBaseAlt"><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NCA.getAdjusterID()%>" /></jsp:include></div></td>
    <td align="right" >&nbsp;</td>
    <td align="right" class="tdBaseAlt">SSN:</td>
    <td class="tdBaseAlt"><strong><%=(NCA.getPatientSSN())%>
    </strong></td>
    <td>&nbsp;</td>
    <td align="right" class="tdBaseAlt">Home Phone:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getPatientHomePhone()%></strong><%if (myPM.getPayerName().indexOf("Tyson")>=0){%><br><font color="#FF0000">Note: This is a Tyson case. <br> <strong>Please do <u>not</u> call/email the Patient directly for any reason.</strong><br>If you have any questions, please contact Wendy Sumrell</font><%}%></td>
    <td>&nbsp;</td>
    <td class="tdBaseAlt">&nbsp;</td>
    <td class="tdBaseAlt">&nbsp;</td>
    </tr>
  <tr>
    <td align="right" class="tdBaseAlt">Adj. Phone:</td>
    <td class="tdBaseAlt"><strong><%=myUAdj.getContactPhone()%></strong></td>
    <td align="right" >&nbsp;</td>
    <td align="right" class="tdBaseAlt">DOI:</td>
    <td class="tdBaseAlt"><strong>
      <jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" >
        <jsp:param name="CurrentSelection" value="<%=dbdf.format(NCA.getDateOfInjury())%>" />    
        </jsp:include>
    </strong></td>
    <td>&nbsp;</td>
    <td align="right" class="tdBaseAlt">Mobile Phone:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getPatientCellPhone()%></strong></td>
    <td>&nbsp;</td>
    <td class="tdBaseAlt">&nbsp;</td>
    <td class="tdBaseAlt">&nbsp;</td>
    </tr>
</table>
	
      <%if (isScheduler&&(CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <input class="inputButton_sm_Create" type=button onClick = "if (confirm('This will create another referral for this existing case.\n\nThis usually happens when you have a new Rx or procedure order using the same Case & Claim Number\n\nTo continue, click OK.')){this.disabled=true;modalPost('EditNewEncounter', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Referral_main_NIM3_Referral_CaseID_form_create.jsp&EDIT=new&KM=p&INTNext=yes','EditNewEncounter','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));document.location = 'tNIM3_Referral_main_NIM3_Referral_CaseID.jsp';}" value="Add Referral"> 

     <%}%>
     
         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
	int Rcnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
		Rcnt++;
        String theClass = "tdBase";
        //if (altCnt%2!=0)
        {
            theClass = "tdHeaderAltDark";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_Referral  = (bltNIM3_Referral) leCurrentElement.getObject();
        working_bltNIM3_Referral.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltNIM3_Referral.isComplete())
        {
//            theClass = "incompleteItem";
        %>

        <%
        }
        else
        {
        %>
        <%
        }
        %>

        <tr class=<%=theClass%> > 


<%String theClassF = "textBase";%>


<td valign="top" nowrap="nowrap">

            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td ><img src="images/marker_referral_3.png" align="absmiddle" width="115" height="31" />  <strong  class="tdHeaderBright"><jsp:include page="../generic/tReferralTypeLI_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getReferralTypeID()%>" /></jsp:include></strong><br /><span class="tdHeaderBright">Rec'd:</span> <span class="borderHighlightGreen"><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Referral.getReceiveDate())%>" /></jsp:include></span><br /><%theClassF = "textBase";%>
  <%if ((working_bltNIM3_Referral.isExpired("ReferralTypeID",expiredDays))&&(working_bltNIM3_Referral.isExpiredCheck("ReferralTypeID"))){theClassF = "expiredFieldMain";}%>
  <%if ( (working_bltNIM3_Referral.isRequired("ReferralTypeID"))&&(!working_bltNIM3_Referral.isComplete("ReferralTypeID")) ){theClassF = "requiredFieldMain";}%>
    
  <%theClassF = "textBase";%>
  <%if ((working_bltNIM3_Referral.isExpired("ReferralStatusID",expiredDays))&&(working_bltNIM3_Referral.isExpiredCheck("ReferralStatusID"))){theClassF = "expiredFieldMain";}%>
  <%if ( (working_bltNIM3_Referral.isRequired("ReferralStatusID"))&&(!working_bltNIM3_Referral.isComplete("ReferralStatusID")) ){theClassF = "requiredFieldMain";}%>
  <span class="tdHeaderBright"> Status:&nbsp;
   <strong>   <jsp:include page="../generic/tReferralStatusLI_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getReferralStatusID()%>" /></jsp:include> </strong></span></td>
                <td align="center" valign="middle">

<%
if (false&&!working_bltNIM3_Referral.isComplete()) 
{
%>	
  <input class="inputButton_md_Test" type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Edit">
<%
}
else if (false)
{
%>	
  <input class="inputButton_md_Test" type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Edit">
<%
}
%>
        <%if (false&&(CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
      &nbsp;
  <input class="inputButton_md_Stop"  type=button onClick = "confirmDelete2('','tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p',this)" value="Delete" style="display:none;">
<% }%>&nbsp;</td>
                <td align="center" valign="middle"><%
if (isScheduler&&working_bltNIM3_Referral.getOrderFileID()==0)
{
%>
    <input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=upor&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Upload Auth">
  <%
}
else if (working_bltNIM3_Referral.getOrderFileID()==0)
{
%>
    <input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=upor&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Upload Auth">
  <%
}
else
{
%>
    <input  class="inputButton_md_Default"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=working_bltNIM3_Referral.getOrderFileID()%>&KM=p';" value="View Auth" /><br />
    <input class="inputButton_sm_Stop"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=upor&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Replace">
    <img src="images/trashcan_delete.png" onclick="deleteReferralDoc(<%=working_bltNIM3_Referral.getReferralID().intValue()%>,'Or');"/>
<%
}
%></td>
                <td align="center" valign="middle"><%
if (working_bltNIM3_Referral.getRxFileID()==0)
{
%>
    <input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=uprx&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Upload Rx">
  <%
}
else
{
%>
	<input  class="inputButton_md_Default"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=working_bltNIM3_Referral.getRxFileID()%>&KM=p';" value="View Rx" /><br />
	<input class="inputButton_sm_Stop"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=uprx&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Replace">
    <img src="images/trashcan_delete.png" onclick="deleteReferralDoc(<%=working_bltNIM3_Referral.getReferralID().intValue()%>,'Rx');"/>
  <%
}
%>&nbsp;</td>
                <td align="right" valign="middle" class="tdHeaderBright">Referred To<br />
Provider:&nbsp;</td>
                <td valign="middle"> <div class=boxDr>     <jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getAttendingPhysicianID()%>" /></jsp:include></div> </td>

<td colspan="3" align="right" valign="middle" class="tdHeaderBright">&nbsp;Referring<br />
Provider:&nbsp;</td>
                <td valign="middle"><div class="boxDr">
                  <jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" >
                    <jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getReferringPhysicianID()%>" />                
                    </jsp:include>
  </div></td>
              </tr>
            </table>
       </td>
</tr><tr>

  <td width="75%" align="left" valign="top" ><table width="100%">
<tr><td>     <%if (isScheduler&&(CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <input class="inputButton_sm_Create" type=button onClick = "if (confirm('This will create another encounter for this existing referral & case.\n\nThis usually happens when you have multiple procedures for one Rx or procedure order that must be scheduled at different locations.  This can also occur when you have to repeat/reschedule a procedure due to a patient no-show, etc.   please make sure to procuess the existing procedure accordingly.  \n\nTo continue, click OK.')){this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=newexpress&QUICKVIEW=yes&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p';}" value="Add Encounter"> 


     <%}%>
        <%

			int Ecnt=0;
			
			searchDB2 mySS = new searchDB2();
			try
			{
				
				java.sql.ResultSet myRS = null;;
				String mySQL = ("select * from tNIM3_Encounter where ReferralID = " + working_bltNIM3_Referral.getReferralID()  + " order by EncounterID desc");
				if (!isScheduler)
				{
					mySQL = ("select * from tNIM3_Encounter where ReferralID = " + working_bltNIM3_Referral.getReferralID()  + " and tNIM3_Encounter.encounterstatusid in (1,6)  order by dateofservice");					
				}
				//out.println(mySQL);
				myRS = mySS.executeStatement(mySQL);
				
				String myMainTable= " ";
				int endCount = 0;
				int cnt2=0;
				while (myRS!=null&&myRS.next())
				{
					Ecnt++;
					tEcnt++;
					NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(new Integer(myRS.getString("EncounterID")),"Case Dashboard Load");
					bltNIM3_Document working_bltNIM3_Document  =  new bltNIM3_Document(myEO2.getNIM3_Encounter().getReportFileID());
					boolean isEActive = false;
					if (NIMUtils.getEncounterIsActive(myEO2.getNIM3_Encounter()))
					{
						isEActive=true;
					}
					if (tEcnt==1)
					{
						out.print(myEO2.getCaseDashboard_AlertCode_HTML());
					}




		String sMarkerGIF = "marker_encounter_none.gif";
		String sStatusClass = "tdBaseAltRed";
		if (myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==2||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==3||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==5||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==10)
		{
			sMarkerGIF = "marker_encounter_void.gif";
			sStatusClass = "tdBaseAltRed";
		}
		else if (myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==4||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==7||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==8||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==11||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==12)
		{
			sMarkerGIF = "marker_encounter_flag.gif";
			sStatusClass = "tdBaseAltYellow";
		}
		else if (myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==1)
		{
			sMarkerGIF = "marker_encounter_active.gif";
			sStatusClass = "tdBaseAltGreen";
		}
		else if (myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==6)
		{
			sMarkerGIF = "marker_encounter_closed.gif";
			sStatusClass = "tdBase";
		}
		else if (myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==9)
		{
			sMarkerGIF = "marker_encounter_billing.gif";
			sStatusClass = "tdBase";
		}
		
		String sActionClass = "tdBase";

		if (myEO2.getNIM3_Encounter().getNextActionDate().before(new java.util.Date(1)))
		{
			sActionClass = "tdBase";
		}
		else if (myEO2.getNIM3_Encounter().getNextActionDate().before(PLCUtils.getYesterday()))
		{
			sActionClass = "tdBaseAlt_Action3";
		}
		else  if (myEO2.getNIM3_Encounter().getNextActionDate().before(PLCUtils.getToday()))
		{
			sActionClass = "tdBaseAlt_Action2";
		}
		else 
		{
			sActionClass = "tdBaseAlt_Action1";
		}
		
		boolean isReadyToSchedule = NIMUtils.isEncounterReadyToSchedule(myEO2,false, true)	;	
		boolean isEncounterProcessed = myEO2.isEncounterProcessed();
		boolean isReadyToScheduleRx = NIMUtils.isEncounterReadyToSchedule(myEO2, true, false)	;	
		boolean isCaseCriticalDone = NIMUtils.isCaseCreateCriticalDone(myEO2);
		boolean isCaseCriticalDone_NoDoc = NIMUtils.isCaseCreateCriticalDone(myEO2, false);
		
		NIM3_BillingObject myBO = myEO2.getBillingTotals();
		//Alert Checks
		boolean isReady4Alert_ReceiveConfirmation = false;
		boolean isReady4Alert_ScanPass = false;
		boolean isReady4Alert_Report = false;
		if (isCaseCriticalDone)
		{
			isReady4Alert_ReceiveConfirmation=true;
		}
		if (isCaseCriticalDone&&isReadyToScheduleRx&&myEO2.getNIM3_Encounter().getAppointmentID().intValue()>0)
		{
			isReady4Alert_ScanPass=true;
		}
		if (isCaseCriticalDone&&isReadyToScheduleRx&&myEO2.getNIM3_Encounter().getReportFileID().intValue()>0&&myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview().after(new java.util.Date(1))&&myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview_UserID().intValue()!=0)
		{
			isReady4Alert_Report=true;
		}
					
					%>
                    
		<div class=caseBox>
        <table width="100%" border="1" cellpadding="3" cellspacing="0">
        <tr class="tdBaseAlt">
          <td nowrap><img src="images/icon_no_<%=Ecnt%>.gif" width="30" height="30"></td>
          <td nowrap><span class="tdHeader"><img src="images/<%=sMarkerGIF%>" align="absmiddle" width="114" height="31" /></span><br>
<input name="showEnc" type="button" class="inputButton_sm_Default" id="showEnc" onclick="document.getElementById('ENC_<%=Rcnt%>_<%=Ecnt%>').style.display = 'table-row';" value="Expand" />&nbsp;
<input name="showEnc" type="button" class="inputButton_sm_Default" id="showEnc" onclick="document.getElementById('ENC_<%=Rcnt%>_<%=Ecnt%>').style.display = 'none';" value="Hide" />
</td>
        <td colspan="2" align="left" valign="top" nowrap><table width="100%" border="0" cellspacing="0" cellpadding="3">
  <tr class="<%=sStatusClass%>">
    <td width=50% nowrap><%=new bltEncounterStatusLI(myEO2.getNIM3_Encounter().getEncounterStatusID()).getDescriptionLong()%></td>
    <td width=25% nowrap align="right"><span class="tdBaseAlt_Action1" ><%if (myEO2.getNIM3_Encounter().getSeeNetDev_Waiting().intValue()==1){out.print("<strong>|NetDev:Waiting|</strong>");}%></span><span class="tdBaseAlt_Default" ><%if (myEO2.getNIM3_Encounter().getSeeNetDev_Flagged().intValue()==1){out.print("<strong>|NetDev|</strong>");}%></span>&nbsp;<span class="tdBaseAlt_Action2" ><%if (myEO2.getNIM3_Encounter().getisCourtesy().intValue()==1){out.print("<strong>|Courtesy Schedule|</strong>");}else if (myEO2.getNIM3_Encounter().getisCourtesy().intValue()==4){out.print("<strong>|Courtesy Pending Approval|</strong>");}else if (myEO2.getNIM3_Encounter().getisCourtesy().intValue()==3){out.print("<strong>|Courtesy Declined|</strong>");}%></span>&nbsp;<span class="tdBaseAlt_Action2" ><%if (myEO2.getNIM3_Encounter().getisRetro().intValue()==1){out.print("<strong>|Retro Auth|</strong>");}%></span></td>
    <td width=25% nowrap align="right"><%if (myEO2.getNIM3_Encounter().getisSTAT().intValue()==1){out.print("<span class=\"tdBaseAlt_Action3\"><strong>|STAT|</strong></span>");}%>&nbsp;<%if (myEO2.getNIM3_Encounter().getisVIP().intValue()==1){out.print("<span style=\"color:black;background:#90ff00\"><strong>|VIP|</strong></span>");}%></td>
    <td width=25% nowrap align="right"">&nbsp;</td>
    <td width=25% nowrap align="right""><%=new bltEncounterTypeLI(myEO2.getNIM3_Encounter().getEncounterTypeID()).getEncounterTypeLong()%></td>
  </tr>
  <tr class="<%=sActionClass%>">
    <td width=75% nowrap>Action: <%=PLCUtils.getDisplayDateWithTime(myEO2.getNIM3_Encounter().getNextActionDate())%></td>
    <td width=25% nowrap align="right"">&nbsp;</td>
    <td width=25% nowrap align="right"">&nbsp;</td>
    <td width=25% nowrap align="right"">&nbsp;</td>
    <td width=25% nowrap align="right"">&nbsp;</td>
  </tr>
</table>
<%
			if (isEActive&&myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==1)
			{
				%>
<input type="button" class="inputButton_md_Default"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=toggle_flag&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Flag">
		        <%
			}
			else if (isEActive&&myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==4)
			{
				%>
<input type="button" class="inputButton_md_Action1"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=toggle_flag&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Flag">
		        <%
			}
			else if (isEActive&&(myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==7||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==8||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==11))
			{
				%>
<input type="button" disabled class="inputButton_md_Action1"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=toggle_flag&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Flag">
		        <%
			}
			else 
			{
				%>
<input type="button" disabled class="inputButton_md_Default"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=toggle_flag&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Flag">
		        <%
			}



if (myEO2.getNIM3_Encounter().getNextActionDate().before(new java.util.Date(1)))
{
%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%> class="inputButton_md_Default"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=nad&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Action">
<%
}
else if (myEO2.getNIM3_Encounter().getNextActionDate().before(PLCUtils.getYesterday()))
{
%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%>   class="inputButton_md_Action3"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=nad&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Action">
<%
}
else  if (myEO2.getNIM3_Encounter().getNextActionDate().before(PLCUtils.getNowDate(false)))
{
%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%>   class="inputButton_md_Action2"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=nad&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Action">
<%
}
else 
{
%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%>   class="inputButton_md_Action1"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=nad&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Action">
<%
}


//old if (isScheduler&&isReadyToSchedule) 
if (isScheduler&&myEO2.getNIM3_Encounter().isComplete()&&NIMUtils.isEncounterServiceComplete(myEO2.getNIM3_Encounter().getEncounterID())) 
{
	%>
<input type="button" class="inputButton_md_Default"  onclick="this.disabled=true;modalPost('CASEID', modalWin('LI_tCaseAccountFull_edit.jsp?QUICKVIEW=yes&EDIT=nim3exp&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Search...','dialogWidth:900px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=900,height=800'));document.location=document.location;" value="Edit Data">&nbsp;<input type="button" class="inputButton_md_Default"  onclick="this.disabled=true;modalPost('CASEID', modalWin('LI_tCaseAccountFull_edit.jsp?QUICKVIEW=yes&EDIT=nim3exp_svc&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Search...','dialogWidth:900px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=900,height=800'));document.location=document.location;" value="Services">
         
         
  <%
}
else if (isScheduler) {
	%>
<input type="button" class="inputButton_md_Action1"  onclick="this.disabled=true;modalPost('CASEID', modalWin('LI_tCaseAccountFull_edit.jsp?QUICKVIEW=yes&EDIT=nim3exp&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Search...','dialogWidth:900px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=900,height=800'));document.location=document.location;" value="Edit Data">&nbsp;<input type="button" class="inputButton_md_Action1"  onclick="this.disabled=true;modalPost('CASEID', modalWin('LI_tCaseAccountFull_edit.jsp?QUICKVIEW=yes&EDIT=nim3exp_svc&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Search...','dialogWidth:900px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=900,height=800'));document.location=document.location;" value="Services">

  <%
}
%>       
<%if (myPM.getPayerID()==139&&Ecnt==1){%>
<input type="button" class="inputButton_md_Default tysoButton" id="tysogram<%=myEO2.getNIM3_Encounter().getEncounterID()%>" value="Tysogram" onClick="tysoID('<%=myEO2.getNIM3_Encounter().getEncounterID()%>','<%=myEO2.getNIM3_Encounter().getReferralID()%>');" style="display:none;">
<%}%>
<input class="inputButton_md_Stop" <% if (!isScheduler3&&(!isReady4Alert_ReceiveConfirmation||!isEActive) ){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=enc_problem&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Problem" />



        
        </td>
        <td align="left" valign="middle" nowrap><%
        
        if (working_bltNIM3_Referral.getRxFileID() == 0){
        	myEO2.getNIM3_Encounter().setTimeTrack_ReqRxReview_UserID(0);
        	myEO2.getNIM3_Encounter().commitData();	
          }
if (!myEO2.isRxReviewed()&&isEncounterProcessed&&myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==8&&isScheduler3&&working_bltNIM3_Referral.getRxFileID() != 0)
{
	%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%>  class="inputButton_md_Action2"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=rxreview&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Rx Review">
         
         
  <%
}
else if (!myEO2.isRxReviewed()&&isEncounterProcessed&&myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==8)
{
	%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%>  class="inputButton_md_Action2"  onclick="alert('This Rx/Order requires review by a Manager');" value="Rx Review">
  <%
}
//change review to proccesed vs. assigned else if (myEO2.getNIM3_Encounter().getTimeTrack_ReqRxReview_UserID().intValue()==0&&isEncounterProcessed&&NCA.getAssignedToID().intValue()==CurrentUserAccount.getUserID())
else if (!isScheduler3&&(!myEO2.isRxReviewed()&&isEncounterProcessed&&myEO2.getNIM3_Encounter().getTimeTrack_ReqProc_UserID().intValue()==CurrentUserAccount.getUserID())&&working_bltNIM3_Referral.getRxFileID() != 0 )
{
	%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%>  class="inputButton_md_Action1"  onclick="alert('This Rx/Order requires review by a different user.\n\nYou appear to be the user who Processed this case and review must be done by a difference user. ');" value="Rx Review">
  <%
}
//manager overide
else if (isScheduler3&&(!myEO2.isRxReviewed()&&isEncounterProcessed&&myEO2.getNIM3_Encounter().getTimeTrack_ReqProc_UserID().intValue()==CurrentUserAccount.getUserID())&&working_bltNIM3_Referral.getRxFileID() != 0 )
{
	%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%>  class="inputButton_md_Action1"  onclick="if (confirm('This Rx/Order normally requires review by a different user.\n\nYou appear to be the user who Processed this case.\n\nAs a Scheduler3, you can override this.\n\nClick OK to Reivew\nClick Cancel to Close ')){this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=rxreview&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';}" value="Rx Review">
  <%
}
else if (isEncounterProcessed&&working_bltNIM3_Referral.getRxFileID() != 0&&myEO2.getNIM3_Encounter().getTimeTrack_ReqRxReview_UserID() == 0)
{
	%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%>  class="inputButton_md_Action1"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=rxreview&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Rx Review">
         
         
  <%
}
else if (myEO2.isRxReviewed()&&isEncounterProcessed&&working_bltNIM3_Referral.getRxFileID() != 0)
{
	%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%>  class="inputButton_md_Default"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=rxreview&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Rx Review">
         
         
  <%
}
else if (isEncounterProcessed&&working_bltNIM3_Referral.getRxFileID() == 0)
{
	%>
<input type="button" class="inputButton_md_Action1" disabled  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=rxreview&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Rx Review">
         
         
  <%
}
else 
{
	%>
<input type="button" class="inputButton_md_Default" disabled  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=rxreview&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Rx Review">
         
         
  <%
}
%>        
<br>

<%
if (myEO2.getNIM3_Encounter().getReportFileID() == 0){
	myEO2.getNIM3_Encounter().setTimeTrack_ReqRpReview_UserID(0);
	myEO2.getNIM3_Encounter().commitData();	
}

//report review
if (myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview_UserID().intValue()==0&&myEO2.getNIM3_Encounter().getReportFileID().intValue()>0&&myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==11&&isScheduler3)
{
	%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%>  class="inputButton_md_Action2"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=rpreview&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Report Review">
         
         
  <%
}
else if (myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview_UserID().intValue()==0&&myEO2.getNIM3_Encounter().getReportFileID().intValue()>0&&myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==11)
{
	%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%>  class="inputButton_md_Action2"  onclick="alert('This Report Review requires review by a Manager');" value="Report Review">
  <%
}
else if (myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview_UserID().intValue()==0&&myEO2.getNIM3_Encounter().getReportFileID().intValue()>0)
{
	%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%>  class="inputButton_md_Action1"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=rpreview&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Report Review">
         
         
  <%
}
else if (myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview_UserID().intValue()>=0&&myEO2.getNIM3_Encounter().getReportFileID().intValue()>0)
{
	%>
<input type="button" <%if (!isEActive){out.print(" disabled ");}%>  class="inputButton_md_Default"  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=rpreview&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Report Review">
         
         
  <%
}
else if (myEO2.getNIM3_Encounter().getTimeTrack_ReqRxReview_UserID().intValue()==0&&myEO2.getNIM3_Encounter().getReportFileID().intValue()==0)
{
	%>
<input type="button" class="inputButton_md_Action1" disabled  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=rpreview&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Report Review">
         
         
  <%
}
else 
{
	%>
<input type="button" class="inputButton_md_Default" disabled  onclick="this.disabled=false;document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=rxreview&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Report Review">
         
         
  <%
}
%>        
&nbsp;</td>
        <td align="left" valign="middle" nowrap> ScanPass:&nbsp;<span class="borderHighlightGreen <%if (isSelectedSent&&SelectedEncounterID.equalsIgnoreCase(myEO2.getNIM3_Encounter().getEncounterID()+"")) {%> emphasizeText <%}%> "><%=myEO2.getNIM3_Encounter().getScanPass()%></span>&nbsp;&nbsp;<br>
        &nbsp;DOS:&nbsp;<span class="borderHighlightGreen"><jsp:include page="../generic/DateTypeConvertDT_Main.jsp" flush="true" >
          <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getDateOfService())%>" />          
          </jsp:include></span></td>
        <td colspan="4" align="center" valign="middle"><%if (myEO2.getNIM3_Encounter().getReportFileID()>0){%><table width="50%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2" align="center"><%=working_bltNIM3_Document.getDocName()%>&nbsp;</td>
    </tr>
  <tr>
    <td align="center"> 
    <input  class="inputButton_md_Default"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=myEO2.getNIM3_Encounter().getReportFileID()%>&KM=p';" value="Report" />&nbsp;</td>
    <td align="center"><% if (false){%><input class="inputButton_md_Test"   type=button onClick = "modalPost('ViewDicom', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp&EDIT=dicom_pl&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','AddCommtrack','dialogWidth:1000px;dialogHeight:600px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));this.disabled=true;document.location =document.location;" value="View Image"><%}%>&nbsp;</td>
  </tr>
<%
if (myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()!=9)
{
	%>
  <tr>
    <td align="center"><input class="inputButton_sm_Stop"  <%if (!isEActive){out.print(" disabled ");}%>    type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=upreport&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p'" value="Replace">&nbsp;</td>
    <img src="images/trashcan_delete.png" onclick="deleteEncounterDoc(<%=myEO2.getNIM3_Encounter().getEncounterID()%>,'Rp');"/>   
    <td align="center">&nbsp;</td>
  </tr>
  <%}%>
</table>
<!--
< %}else if (isScheduler&&myEO2.getNIM3_Encounter().getAppointmentID().intValue()>0&&(myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==1||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==4)){%>     
  -->
<%}else if (true){%>     

<input class="inputButton_md_Action1"  <%if (!isEActive){out.print(" disabled ");}%> type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=upreport&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p'" value="Upload Report">

<%}else if (isScheduler&&myEO2.getNIM3_Encounter().getAppointmentID().intValue()==0&&(myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==1||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==4)){%>     
<input class="inputButton_md_Action1"  disabled="disabled" type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=upreport&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p'" value="Upload Report">

<%}else {%>
<input class="inputButton_md_Default"   disabled="disabled" type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=upreport&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p'" value="Report">
<%}%>
<%if (myEO2.getNIM3_Encounter().getRequiresAgeInjury()==1){%><br>
Age Comment: <input type="checkbox" class="ageCommentLock" <%if (myEO2.getNIM3_Encounter().getAgeCommentLock()==1) {%> checked="checked" <%}%>value="<%=myEO2.getNIM3_Encounter().getEncounterID()%>">
<%}%>

</td><td align="center">

<%
if (myEO2.getNIM3_Encounter().getBillingHCFA_ToPayer_FileID()==0)
{
%>
    <input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=uphcfapay&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p'" value="U/L HCFA Payer">
  <%
}
else
{
%>
	<input  class="inputButton_md_Default"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=myEO2.getNIM3_Encounter().getBillingHCFA_ToPayer_FileID()%>&KM=p';" value="HCFA Payer" /><br />    <input class="inputButton_sm_Default"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=uphcfapay&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p'" value="Replace">
    <img src="images/trashcan_delete.png" onclick="deleteEncounterDoc(<%=myEO2.getNIM3_Encounter().getEncounterID()%>,'HCFA To Payer');"/>
<%
}
%></td><td align="center">

<%
if (myEO2.getNIM3_Encounter().getBillingHCFA_FromProvider_FileID()==0)
{
%>
    <input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=uphcfapro&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p'" value="U/L HCFA Provider">
  <%
}
else
{
%>Rec: <strong><%=PLCUtils.getDisplayDate(myEO2.getNIM3_Encounter().getRec_Bill_Pro(), false)%></strong><br>
	<input  class="inputButton_md_Default"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=myEO2.getNIM3_Encounter().getBillingHCFA_FromProvider_FileID()%>&KM=p';" value="HCFA Provider" /><br />    <input class="inputButton_sm_Default"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=uphcfapro&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p'" value="Replace">
	<img src="images/trashcan_delete.png" onclick="deleteEncounterDoc(<%=myEO2.getNIM3_Encounter().getEncounterID()%>,'HCFA From Provider');"/>
<%
}
%></td>
        </tr>
<tr><td colspan=12><table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <%=NIMUtils.aEncounterStatus_HTML(myEO2.getNIM3_Encounter().getEncounterID())%>
</td>
  </tr>
</table>
</td></tr>				


<tr id="ENC_<%=Rcnt%>_<%=Ecnt%>" <%if (isSelectedSent&&SelectedEncounterID.equalsIgnoreCase(myEO2.getNIM3_Encounter().getEncounterID()+"")){}else{%>style="display:none"<%}%>>
	<td width="75%" colspan="4" valign="top">
		<table width="100%" border="1" cellpadding="3" cellspacing="0">
		<%bltNIM3_Service        working_bltNIM3_Service;
		ListElement         leCurrentElement_Service;
		java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
		int cnt55=0;
		while (eList_Service.hasMoreElements())
		{
			cnt55++;
			leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
			working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
					String serviceClass = "tdBase";
					boolean isVoid=false;
					if (working_bltNIM3_Service.getServiceStatusID().intValue()==5)
					{
						serviceClass = "tdBaseAltVoid";
						isVoid = true;
					}
					%>
		<tr class="<%=serviceClass%>">
			<td align="center" class="tdHeaderAlt"><%=cnt55%></td>
			<td>
            	<% String CPTModifier = "";
				if (working_bltNIM3_Service.getCPTModifier().equalsIgnoreCase("26")){
					CPTModifier = "READ ONLY "; 
				} else if (working_bltNIM3_Service.getCPTModifier().equalsIgnoreCase("TC")){
					CPTModifier = "SCAN ONLY ";
				}%> 
                
				<span <%if (!isVoid){out.println(" class=\"borderHighlightGreen\" ");}%>  ><strong>Lookup:</strong> [<%=NIMUtils.getCPTText(working_bltNIM3_Service.getCPT())%>]</span><br />        
				<span <%if (!isVoid){out.println(" class=\"borderHighlightGreen\" ");}%>   ><strong>BP:</strong>[<%=CPTModifier%><%=working_bltNIM3_Service.getCPTBodyPart()%>]</span><br>
<span <%if (!isVoid){out.println(" class=\"borderHighlightGreen\" ");}%>  ><strong>RuleOut:</strong> [<%=working_bltNIM3_Service.getCPTText()%>]</span></td>
			<td>
				<%if (isVoid){out.println("<strike>");}%>CPT: <strong><%=working_bltNIM3_Service.getCPT()%></strong><br>
				Mod: <strong><%=working_bltNIM3_Service.getCPTModifier()%></strong><br>
				Qty Done: <strong><%=working_bltNIM3_Service.getCPTQty()%></strong>
				<%if (isVoid){out.println("</strike>");}%>
            </td>
			<td><%if (working_bltNIM3_Service.getServiceStatusID().intValue()==1){out.println("<span class=\"borderHighlightGreen\">Bill / Pay</span><br>(Normal)");}
				else if (working_bltNIM3_Service.getServiceStatusID().intValue()==2){out.println("<span class=\"borderHighlightGreen\">Don't Bill / Don't Pay</span>");}
				else if (working_bltNIM3_Service.getServiceStatusID().intValue()==3){out.println("<span class=\"borderHighlightGreen\">Don't Bill / Do Pay</span>");}
				else if (working_bltNIM3_Service.getServiceStatusID().intValue()==4){out.println("<span class=\"borderHighlightGreen\">Do Bill / Don't Pay</span>");}
				else if (working_bltNIM3_Service.getServiceStatusID().intValue()==5){out.println("<span class=\"borderHighlightGreen\">VOID</span>");}%>
			</td>
			<td>
				<%if (working_bltNIM3_Service.getDICOM_ShowImage().intValue()==1){%><input class="inputButton_md_Test"   type=button onClick = "modalPost('ViewDicom', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp&EDIT=dicom_pls&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&SEDITID=<%=working_bltNIM3_Service.getServiceID()%>&KM=p','AddCommtrack','dialogWidth:1000px;dialogHeight:600px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));this.disabled=true;document.location =document.location;" value="View Image"><%}%>&nbsp;
            </td>
		</tr>
		<%}%>
       
		</table>
 <%if (cnt55==0){%>
        <script>
			$(document).ready(function(e) {
                $("#tysogram<%=myEO2.getNIM3_Encounter().getEncounterID()%>").show();
            });
		</script>
        <%}%>
<!-- start enc detail-->

	<table width="100%" border="0" cellspacing="0" cellpadding="2">
    <tr>
		<td valign="top" nowrap class="tdBaseAlt2"><strong>Patient Availability:&nbsp;&nbsp;</strong></td>
        <td width="95%" valign="top" class="tdBaseAlt2"><pre><%=myEO2.getNIM3_Encounter().getPatientAvailability()%></pre></td>
	</tr>
	<tr>
		<td width="75%" colspan="2">
			<input type="button" class="inputButton_md_Test"  onclick="this.disabled=true;modalPost('Schedule', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp&EDIT=sched&PreSchedule=y&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Schedule...','dialogWidth:1400px;dialogHeight:1000px','status=yes,scrollbars=yes,resizable=yes,width=1400,height=1000'));document.location=document.location;" value="Pre Schedule">   &nbsp;&nbsp;&nbsp;      
	       
	       
	       
	        <%if (!NCA.getPayerID().equals(adjPM.getPayerID()))
	   		{%>
	        	<input type="button" class="inputButton_md_Action1"  onclick="alert('This encounter\'s adjuster/carrier does not match. ');" value="Schedule">   <br>
	        	<span style="color:#f2f2f2">c id: <%=NCA.getPayerID() %> ap id: <%=adjPM.getPayerID() %>     </span>  
			<%}
	        else if (isScheduler && myEO2.getNIM3_Referral().getReferralTypeID().intValue()==3&&myEO2.getNIM3_Encounter().getAppointmentID()==0&&isReadyToScheduleRx&&isEActive&&myEO2.getNIM3_Encounter().getisCourtesy()!=3&&myEO2.getNIM3_Encounter().getisCourtesy()!=4 ){%>
	        	<h1><input type="button" class="inputButton_md_Action1"  onclick="this.disabled=true;modalPost('Schedule', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp&EDIT=sched&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Schedule...','dialogWidth:1400px;dialogHeight:1000px','status=yes,scrollbars=yes,resizable=yes,width=1400,height=1000'));document.location=document.location;" value="Schedule"></h1>            
	        	
	        <%}	else if (myEO2.getNIM3_Encounter().getisCourtesy()==3)
	  		{%>
	        	<input type="button" class="inputButton_md_Action1"  onclick="alert('This encounter has been declined by NetDev/Billing. ');" value="Schedule">                      
         	<%}
	   		else if (myEO2.getNIM3_Encounter().getisCourtesy()==4 )
	   		{%>
	            <input type="button" class="inputButton_md_Action1"  onclick="alert('This encounter has is awaiting Courtesy Approval. ');" value="Schedule">                      
         	<%}
	   		else if (isScheduler && myEO2.getNIM3_Referral().getReferralTypeID().intValue()==3&&myEO2.getNIM3_Encounter().getAppointmentID()==0&&isReadyToSchedule&&isEActive)
	   		{%>
				<input type="button" class="inputButton_md_Action1"  onclick="alert('This encounter awaiting Rx Review. ');" value="Schedule">                      
        	<%}
	   		else if (isScheduler && myEO2.getNIM3_Referral().getReferralTypeID().intValue()==3&&myEO2.getNIM3_Encounter().getAppointmentID()==0&&isEActive)
	   		{%>
				<input type="button" class="inputButton_md_Action1"  onclick="alert('This encounter is not yet ready to schedule.  Check the status bar for yellow items to see what items need to be done. ');" value="Schedule">                      
        	<%}
	   		else
	   		{%>
				<input type="button" disabled class="inputButton_md_Action1"  onclick="alert('This encounter is not yet ready to schedule.  Check the status bar for yellow items to see what items need to be done. ');" value="Schedule">                      
        	<%}

			if (isScheduler &&myEO2.getNIM3_Encounter().getAppointmentID()>0){%>       
				<table width="100%" border="0" cellpadding="3" cellspacing="0" class="borderHighlight1" >
           			<tr class="tdHeader">
		            	<td  class="tdHeader">&nbsp;</td>
		            	<td align="center"  class="borderHighlightGreen"><%
							bltNIM3_Appointment myNALU = new bltNIM3_Appointment(myEO2.getNIM3_Encounter().getAppointmentID());
							bltPracticeMaster myPracM = new bltPracticeMaster(myNALU.getProviderID());
							//bltICMaster myICM = new bltICMaster ();
      						java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
				            java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
				            java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
				            java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
				            java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
				            java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);
				            java.text.SimpleDateFormat displayDateHourMin = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHourMin);
							java.util.Date dbAppointment = myNALU.getAppointmentTime();
							String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
							String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
							String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
							String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
							String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
							String sAppointmentHour = displayDateHour.format((dbAppointment));
							String sAppointmentHourMin = displayDateHourMin.format((dbAppointment));
							%>
							<%=sAppointmentTimeDayWeek%>&nbsp;<%=sAppointmentTimeMonth%>&nbsp;<%=sAppointmentTimeDay%>&nbsp;<%=sAppointmentTimeYear%> at <%=sAppointmentHourMin%>&nbsp;
						</td>
             			<td>
							<%if (  isEActive&&isReadyToScheduleRx&&false ){%>
								<input type="button" class="inputButton_md_Stop"   onclick="this.disabled=true;modalPost('Schedule', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp&EDIT=sched&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Schedule...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.location=document.location;" value="Re-Schedule">
							<%}
							else if (  isEActive&&isReadyToScheduleRx ){%>
								<input type="button" class="inputButton_md_Stop"  onclick="alert('To reschedule this appointment, please use the Problem Wizard.');" value="Re-Schedule">                      
							<%}
							else{%>
								<input type="button" class="inputButton_md_Stop"  onclick="alert('Information appears to be missing that is critical to re-schedule.  Please review the above status bar or see a manager to resolve this.');" value="Re-Schedule">                      
							<%}%>
							  &nbsp;
						</td>
					</tr>
					<tr class="borderHighlightGreen">
						<td colspan="3" class="borderHighlightGreen"></td>
					</tr>
					<tr>
						<td>
							<strong><%=myPracM.getPracticeName()%></strong> <br />
						    [ID:&nbsp;
						<%if (isGenAdmin){%>
							<a target="editResults"  href="/winstaff/app/AdminPracticeAll_query_auth.jsp?EDIT=practice&EDITID=<%=myPracM.getPracticeID()%>&EDIT2ID=0"><%=myPracM.getPracticeID()%></a>				
						<%} 
						else {%>
						<%=myPracM.getPracticeID()%>
						<%}%>]
						</td>
              			<td><% if (isScheduler){%><%=myPracM.getOfficeAddress1()%><br /><%=myPracM.getOfficeAddress2()%><br />
			               <%=myPracM.getOfficeCity()%>, <%=new bltStateLI(myPracM.getOfficeStateID()).getShortState()%> <%=myPracM.getOfficeZIP()%><% }%>
			            </td>
			            <td align="center" valign="middle"><% if (isScheduler){%><a href="http://maps.google.com/maps?q=<%=myPracM.getOfficeAddress1()%>,<%=myPracM.getOfficeZIP()%>" target="_blank">View Map</a>&nbsp;<% }%></td>
			        </tr>
			        <tr>
			            <td class="tdHeader">Phone: <%=myPracM.getOfficePhone()%>&nbsp;</td>
			            <td class="tdHeader">Fax: <%=myPracM.getOfficeFaxNo()%>&nbsp;</td>
			            <td>&nbsp;</td>
			        </tr>
			       <%if (isAccounting){ %>
			        <tr>
			         	<td>&nbsp;</td>
			        </tr>
			        <tr>
			            <td>
			            	<strong>Billing Info:</strong> <br />
			            	<%=new bltPracticeContractStatusLI(myEO2.getAppointment_PracticeMaster().getContractingStatusID()).getDescriptionLong() %><br /></td>
			            <td>
			            	<%=myPracM.getBillingAddress1()%> <%=myPracM.getBillingAddress2()%><br />
			                <%=myPracM.getBillingCity()%>, <%=new bltStateLI(myPracM.getBillingStateID()).getShortState()%> <%=myPracM.getBillingZIP()%></td>
			            <td align="center" valign="middle"><a href="http://maps.google.com/maps?q=<%=myPracM.getBillingAddress1()%>,<%=myPracM.getBillingZIP()%>" target="_blank">View Map</a>&nbsp;</td>
			       </tr>
			       <tr>
			            <td>Phone: <%=myPracM.getBillingPhone()%>&nbsp;</td>
			            <td>Fax: <%=myPracM.getBillingFax()%>&nbsp;</td>
			            <td>&nbsp;</td>
			       </tr>
			        <%}%>
			       </table>
		<%}%>
	</td>
     </tr>
     <tr>
       <td colspan="2">
       
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td><p class="tdHeader">CommTrack:<br>
			<input type="button" class="inputButton_sm_Create"  onclick="this.disabled=true;modalPost('CASEID', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp&QUICKVIEW=yes&EDIT=addct&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Add CT...','dialogWidth:1000px;dialogHeight:1000px','status=yes,scrollbars=yes,resizable=yes,width=900,height=800'));document.location=document.location;" value="Add">&nbsp;
			<input name="showCT" type="button" class="inputButton_sm_Default" id="expandButton_<%=myEO2.getNIM3_Encounter().getEncounterID()%>" onclick="expandAll_<%=myEO2.getNIM3_Encounter().getEncounterID()%>()" value="Expand All" />&nbsp;
			<input name="showCT" type="button" class="inputButton_sm_Default" id="collapseButton_<%=myEO2.getNIM3_Encounter().getEncounterID()%>" onclick="collapseAll_<%=myEO2.getNIM3_Encounter().getEncounterID()%>()" value="Collapse All" style="display:none;"/>&nbsp;
			<input name="showCT" type="button" class="inputButton_sm_Default" onclick="hideCommTrack_<%=myEO2.getNIM3_Encounter().getEncounterID()%>()" value="Toggle CommTrack" /> 

			</p></td>
  </tr>
  <script type="text/javascript">
		$(document).ready(function(){
			$('.commBox_<%=myEO2.getNIM3_Encounter().getEncounterID()%>').children('.commBoxTitle').click(function(){
				$(this).next('div.commBoxMsg').toggle();
				$(this).children('span.expand').toggle();
				$(this).children('span.collapse').toggle();
				$(this).children('span.msgTrunc').toggle();
			});
		});

		function expandAll_<%=myEO2.getNIM3_Encounter().getEncounterID()%>(){
			$('.commBox_<%=myEO2.getNIM3_Encounter().getEncounterID()%> div.commBoxMsg').slideDown("fast");
			$('.commBox_<%=myEO2.getNIM3_Encounter().getEncounterID()%> span.msgTrunc').hide();
			$('.commBox_<%=myEO2.getNIM3_Encounter().getEncounterID()%> span.expand').hide();
			$('.commBox_<%=myEO2.getNIM3_Encounter().getEncounterID()%> span.collapse').show();
			$('#expandButton_<%=myEO2.getNIM3_Encounter().getEncounterID()%>').hide();
			$('#collapseButton_<%=myEO2.getNIM3_Encounter().getEncounterID()%>').show();
		}
		function collapseAll_<%=myEO2.getNIM3_Encounter().getEncounterID()%>(){
			$('.commBox_<%=myEO2.getNIM3_Encounter().getEncounterID()%> div.commBoxMsg').slideUp("fast");
			$('.commBox_<%=myEO2.getNIM3_Encounter().getEncounterID()%> span.msgTrunc').show();
			$('.commBox_<%=myEO2.getNIM3_Encounter().getEncounterID()%> span.expand').show();
			$('.commBox_<%=myEO2.getNIM3_Encounter().getEncounterID()%> span.collapse').hide();
			$('#expandButton_<%=myEO2.getNIM3_Encounter().getEncounterID()%>').show();
			$('#collapseButton_<%=myEO2.getNIM3_Encounter().getEncounterID()%>').hide();		
		}
		function hideCommTrack_<%=myEO2.getNIM3_Encounter().getEncounterID()%>(){
			$('#comTrack_<%=myEO2.getNIM3_Encounter().getEncounterID()%>').toggle();
		}
		</script>
  <tr id="comTrack_<%=myEO2.getNIM3_Encounter().getEncounterID()%>" <%if (isAccounting){%>style="display:none;"<%}%>>
    <td>
    
		    <%
	searchDB2 mySS_ct = new searchDB2();
	try
	{
		java.sql.ResultSet myRS_ct = null;
//		String mySQL_ct = ("SELECT commtrackid from tnim3_commtrack where (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=0) OR (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=" + myEO2.getNIM3_Encounter().getEncounterID() +") order by CommEnd desc");
		String mySQL_ct = ("SELECT commtrackid from tnim3_commtrack where (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=0) OR (encounterid=" + myEO2.getNIM3_Encounter().getEncounterID() +") order by CommStart desc");
		//out.println(mySQL);

		myRS_ct = mySS_ct.executeStatement(mySQL_ct);
		int i2cnt_ct=0;
		while(myRS_ct!=null&&myRS_ct.next())
		{
			i2cnt_ct++;
			bltNIM3_CommTrack myCT = new bltNIM3_CommTrack (new  Integer(myRS_ct.getString("commtrackid")));
			%>
		
		<div class="commBox_<%=myEO2.getNIM3_Encounter().getEncounterID()%> commBox gradient">
			<div class="commBoxTitle">
			<span class="expand left"><strong>[+]&nbsp;</strong></span>
			<span class="collapse left display-none"><strong>[-]&nbsp;</strong></span>
			<span class="timeStamp left bold" >
				<%if (myCT.getCommEnd().after(NIMUtils.getBeforeTime()))
				{
					out.print(displayDateTimeSDF.format(myCT.getCommStart()));
				}
				else 
				{
					out.print("Start: " + displayDateTimeSDF.format(myCT.getCommStart()) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Item Still Open&nbsp;&nbsp;&nbsp;Started: <strong>" + PLCUtils.getHoursTill_Display(myCT.getCommStart(), false)+ "  (" + PLCUtils.getDaysTill_Display(myCT.getCommStart(), true) + ")</strong>");
				}%>
			</span>

			<span class="msgName left bold">&nbsp;&nbsp;<%=myCT.getMessageName()%> </span>
			<span class="msgTrunc left">&nbsp;&nbsp;
			<%if(myCT.getMessageText().length()<80){
				out.print(myCT.getMessageText().substring(0,myCT.getMessageText().length()));
			}
			else{
				out.print(myCT.getMessageText().substring(0,80)+"...");
			}%> </span>
			<%-- <span class="msgCompany display-none"><%=myCT.getMessageCompany()%></span> --%>
			<%-- <span class="noteType right">
			<%if (myCT.getCommTypeID()==3){%>Notification<%}
		    else if (myCT.getCommTypeID()==4){%>See NetDev Request<%}
		    else if (myCT.getCommTypeID()==2){%>Referral/MSG<%}
		    else if (myCT.getCommTypeID()==1){%>MSG<%}
		    else if (myCT.getCommTypeID()==0){%>Note<%}%>
		    </span> --%>
			</div>
		<div class="commBoxMsg">
				<%if (myCT.getCommTypeID()==4)
		  		{%>
		  		<span style="color:blue">
		 		<Strong>Response:</Strong><br><%=myCT.getComments().replaceAll("(\r\n|\r|\n|\n\r)", "<br>")%>
		  		<Br><br>
		  		<%}%>
		  		</span>
		  		<%=myCT.getMessageText().replaceAll("(\r\n|\r|\n|\n\r)", "<br>")%>
		  		
		  		
		  		<%
				searchDB2 mySS_cr = new searchDB2();
				try {
					java.sql.ResultSet myRS_cr = null;
					//String mySQL_ct = ("SELECT commtrackid from tnim3_commtrack where (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=0) OR (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=" + myEO2.getNIM3_Encounter().getEncounterID() +") order by CommEnd desc");
					String mySQL_cr = ("SELECT commreferenceid from tnim3_commreference where commreferenceid = " + myCT.getCommReferenceID());
					//out.println(mySQL);
					myRS_cr = mySS_cr.executeStatement(mySQL_cr);
					int i2cnt_cr=0;
					if(myRS_cr!=null)
					{
						while(myRS_cr!=null&&myRS_cr.next())
						{
							i2cnt_cr++;
							bltNIM3_CommReference myCR = new bltNIM3_CommReference (new  Integer(myRS_cr.getString("commreferenceid")));
							if (myCR.getCRTypeID()==1)
							{
							%>	
							<a target="_blank" href="tNIM3_CommReference_view.jsp?EDITID=<%=myCR.getCommReferenceID()%>">View Notification</a>
							<%
							}
							else
							{
							%>	
							Other
							<%
							}
						}
					}
					else
					{
							%>	
							None
							<%
					}
				} catch (Exception mySS_cr_Error) {
		            DebugLogger.println("JSP:tNIM3_Referral_main_NIM3_Referral_CaseID.jsp:mySS_cr.executeStatement(mySQL_cr): [Error]" + mySS_cr_Error.toString());
				} finally {
					mySS_cr.closeAll();
				}
					
				%>
			</div>	
		</div>
		<hr>
		<%-- <table width="100%"  border="0" cellspacing="0" cellpadding="3" class="certborderPrintInt">
  <tr class=tdBaseAlt4Blue>
    <td class=tdBaseAlt4Blue>Type:</td>
    <td class=tdBaseAlt4Blue><%
    if (myCT.getCommTypeID()==3){%>Notification<%}
    else if (myCT.getCommTypeID()==4){%>See NetDev Request<%}
    if (myCT.getCommTypeID()==2){%>Referral/MSG<%}
    if (myCT.getCommTypeID()==1){%>MSG<%}
    if (myCT.getCommTypeID()==0){%>Note<%}%></td>
    <td>&nbsp;</td>
  </tr>
  <tr class=tdBaseAlt>
    <td>From:</td>
		<td width="75%" nowrap><strong><%=myCT.getMessageName()%></strong> @ <strong><%=myCT.getMessageCompany()%></strong></td>
		<td width="25%" align="right"><%if (myCT.getEncounterID().intValue()==myEO2.getNIM3_Encounter().getEncounterID().intValue())
		{
			out.println("E");
		}else
		{
			out.println("R");
		}%>&nbsp;</td>
  </tr>
  <%
	String myClass_CT_Duration = "";
	int hoursSince = Math.abs(PLCUtils.getHoursTill(myCT.getCommStart()));
	if (myCT.getCommEnd().after(NIMUtils.getBeforeTime()))
	{
			myClass_CT_Duration = "tdBaseAlt2";
	}
	else
	{
		if (hoursSince>48)
		{
			myClass_CT_Duration = "tdBaseAlt_Action3";
		}
		else if (hoursSince>24 || myEO2.isEscalatedSTAT())
		{
			myClass_CT_Duration = "tdBaseAlt_Action2";
		}
		else if (hoursSince>12 )
		{
			myClass_CT_Duration = "tdBaseAlt_Action1";
		}
		else
		{
			myClass_CT_Duration = "tdBaseAlt2";
		}
	}
  
  
  %>
  <tr class="<%=myClass_CT_Duration%>" class=tdBase>
    <td>Time:</td>
    <td>
<%				if (myCT.getCommEnd().after(NIMUtils.getBeforeTime()))
				{
					out.print(displayDateTimeSDF.format(myCT.getCommStart()) + "&nbsp;&nbsp;&nbsp;(Dur: <strong>" + Math.round((myCT.getCommEnd().getTime()-myCT.getCommStart().getTime())/1000/60) + "</strong> min)");
				}
				else 
				{
					out.print("Start: " + displayDateTimeSDF.format(myCT.getCommStart()) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Item Still Open&nbsp;&nbsp;&nbsp;Started: <strong>" + PLCUtils.getHoursTill_Display(myCT.getCommStart(), false)+ "  (" + PLCUtils.getDaysTill_Display(myCT.getCommStart(), true) + ")</strong>");
				}
%>				
</td>
    <td>Status:<jsp:include page="../generic/tCommTrackAlertStatusCodeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myCT.getAlertStatusCode()%>" /><jsp:param name="UseColor" value="y" /></jsp:include>
     </td>
  </tr>
  <%
  if (myCT.getCommTypeID()==4)
  {
  %>
  <tr bgcolor="#CCCCCC">
    <td colspan="3">Response:<br><%=myCT.getComments().replaceAll("(\r\n|\r|\n|\n\r)", "<br>")%></td>
  </tr>
  <%
  }
  %>
  <tr bgcolor="#FFFFFF">
    <td colspan="3"><%=myCT.getMessageText().replaceAll("(\r\n|\r|\n|\n\r)", "<br>")%></td>
  </tr>
  <tr bgcolor="#DDDDDD">
    <td>CommRef:</td>
    <td colspan="2"><%
		searchDB2 mySS_cr = new searchDB2();
		try {
			java.sql.ResultSet myRS_cr = null;
	//		String mySQL_ct = ("SELECT commtrackid from tnim3_commtrack where (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=0) OR (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=" + myEO2.getNIM3_Encounter().getEncounterID() +") order by CommEnd desc");
			String mySQL_cr = ("SELECT commreferenceid from tnim3_commreference where commreferenceid = " + myCT.getCommReferenceID());
			//out.println(mySQL);
			myRS_cr = mySS_cr.executeStatement(mySQL_cr);
			int i2cnt_cr=0;
			if(myRS_cr!=null)
			{
				while(myRS_cr!=null&&myRS_cr.next())
				{
					i2cnt_cr++;
					bltNIM3_CommReference myCR = new bltNIM3_CommReference (new  Integer(myRS_cr.getString("commreferenceid")));
					if (myCR.getCRTypeID()==1)
					{
					%>	
					<a target="_blank" href="tNIM3_CommReference_view.jsp?EDITID=<%=myCR.getCommReferenceID()%>">View Notification</a>
					<%
					}
					else
					{
					%>	
					Other
					<%
					}
				}
			}
			else
			{
					%>	
					None
					<%
			}
		} catch (Exception mySS_cr_Error) {
            DebugLogger.println("JSP:tNIM3_Referral_main_NIM3_Referral_CaseID.jsp:mySS_cr.executeStatement(mySQL_cr): [Error]" + mySS_cr_Error.toString());
		} finally {
			mySS_cr.closeAll();
		}
			
	%></td>
  </tr>
</table> --%>
		
		<%-- <table width="100%"  border="0" cellspacing="0" cellpadding="3" class="certborderPrintInt">
  <tr class=tdBaseAlt4Blue>
    <td class=tdBaseAlt4Blue>Type:</td>
    <td class=tdBaseAlt4Blue><%
    if (myCT.getCommTypeID()==3){%>Notification<%}
    else if (myCT.getCommTypeID()==4){%>See NetDev Request<%}
    if (myCT.getCommTypeID()==2){%>Referral/MSG<%}
    if (myCT.getCommTypeID()==1){%>MSG<%}
    if (myCT.getCommTypeID()==0){%>Note<%}%></td>
    <td>&nbsp;</td>
  </tr>
  <tr class=tdBaseAlt>
    <td>From:</td>
		<td width="75%" nowrap><strong><%=myCT.getMessageName()%></strong> @ <strong><%=myCT.getMessageCompany()%></strong></td>
		<td width="25%" align="right"><%if (myCT.getEncounterID().intValue()==myEO2.getNIM3_Encounter().getEncounterID().intValue())
		{
			out.println("E");
		}else
		{
			out.println("R");
		}%>&nbsp;</td>
  </tr>
  <%
	String myClass_CT_Duration = "";
	int hoursSince = Math.abs(PLCUtils.getHoursTill(myCT.getCommStart()));
	if (myCT.getCommEnd().after(NIMUtils.getBeforeTime()))
	{
			myClass_CT_Duration = "tdBaseAlt2";
	}
	else
	{
		if (hoursSince>48)
		{
			myClass_CT_Duration = "tdBaseAlt_Action3";
		}
		else if (hoursSince>24 || myEO2.isEscalatedSTAT())
		{
			myClass_CT_Duration = "tdBaseAlt_Action2";
		}
		else if (hoursSince>12 )
		{
			myClass_CT_Duration = "tdBaseAlt_Action1";
		}
		else
		{
			myClass_CT_Duration = "tdBaseAlt2";
		}
	}
  
  
  %>
  <tr class="<%=myClass_CT_Duration%>" class=tdBase>
    <td>Time:</td>
    <td>
<%				if (myCT.getCommEnd().after(NIMUtils.getBeforeTime()))
				{
					out.print(displayDateTimeSDF.format(myCT.getCommStart()) + "&nbsp;&nbsp;&nbsp;(Dur: <strong>" + Math.round((myCT.getCommEnd().getTime()-myCT.getCommStart().getTime())/1000/60) + "</strong> min)");
				}
				else 
				{
					out.print("Start: " + displayDateTimeSDF.format(myCT.getCommStart()) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Item Still Open&nbsp;&nbsp;&nbsp;Started: <strong>" + PLCUtils.getHoursTill_Display(myCT.getCommStart(), false)+ "  (" + PLCUtils.getDaysTill_Display(myCT.getCommStart(), true) + ")</strong>");
				}
%>				
</td>
    <td>Status:<jsp:include page="../generic/tCommTrackAlertStatusCodeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myCT.getAlertStatusCode()%>" /><jsp:param name="UseColor" value="y" /></jsp:include>
     </td>
  </tr>
  <%
  if (myCT.getCommTypeID()==4)
  {
  %>
  <tr bgcolor="#CCCCCC">
    <td colspan="3">Response:<br><%=myCT.getComments().replaceAll("(\r\n|\r|\n|\n\r)", "<br>")%></td>
  </tr>
  <%
  }
  %>
  <tr bgcolor="#FFFFFF">
    <td colspan="3"><%=myCT.getMessageText().replaceAll("(\r\n|\r|\n|\n\r)", "<br>")%></td>
  </tr>
  <tr bgcolor="#DDDDDD">
    <td>CommRef:</td>
    <td colspan="2"><%
		searchDB2 mySS_cr = new searchDB2();
		try {
			java.sql.ResultSet myRS_cr = null;
	//		String mySQL_ct = ("SELECT commtrackid from tnim3_commtrack where (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=0) OR (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=" + myEO2.getNIM3_Encounter().getEncounterID() +") order by CommEnd desc");
			String mySQL_cr = ("SELECT commreferenceid from tnim3_commreference where commreferenceid = " + myCT.getCommReferenceID());
			//out.println(mySQL);
			myRS_cr = mySS_cr.executeStatement(mySQL_cr);
			int i2cnt_cr=0;
			if(myRS_cr!=null)
			{
				while(myRS_cr!=null&&myRS_cr.next())
				{
					i2cnt_cr++;
					bltNIM3_CommReference myCR = new bltNIM3_CommReference (new  Integer(myRS_cr.getString("commreferenceid")));
					if (myCR.getCRTypeID()==1)
					{
					%>	
					<a target="_blank" href="tNIM3_CommReference_view.jsp?EDITID=<%=myCR.getCommReferenceID()%>">View Notification</a>
					<%
					}
					else
					{
					%>	
					Other
					<%
					}
				}
			}
			else
			{
					%>	
					None
					<%
			}
		} catch (Exception mySS_cr_Error) {
            DebugLogger.println("JSP:tNIM3_Referral_main_NIM3_Referral_CaseID.jsp:mySS_cr.executeStatement(mySQL_cr): [Error]" + mySS_cr_Error.toString());
		} finally {
			mySS_cr.closeAll();
		}
			
	%></td>
  </tr>
</table> --%>
			<%
		}
		mySS_ct.closeAll();
	}
	catch(Exception e)
	{
			out.println("ResultsSet:"+e);
	}
	finally 
	{
		mySS_ct.closeAll();
	}
%>

    
    
    </td>
  </tr>
</table>

</td>
     </tr>
     <tr>
       <td colspan="2">


</td>
     </tr>
   </table>


<!-- end enc detail-->
                          
                          
                          
                          
                          
   					    </td>
						<td valign="top" align="center" colspan="8">

<table width="0%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td valign="top">
  <!-- Conf Actions Start -->
      
  <%
String but_class = "inputButton_md_Default";
%><table  border="2" align="center" cellpadding="2" cellspacing="0">
    <tr>
      <td nowrap bgcolor="#CCCCCC" class="tdHeader" >Confirmation Actions</td>
      <td align="center" nowrap bgcolor="#EDDBAB"><strong>Order Conf.</strong></td>
      <td align="center" nowrap bgcolor="#AAD5EE"><strong>ScanPass</strong></td>
      <td align="center" nowrap bgcolor="#BBEBAD"><strong>Report</strong></td>
      </tr>
    <tr>
      <td bgcolor="#333333">&nbsp;</td>
      <td align="center" bgcolor="#333333"><button type="button" onclick="cc_all(<%=myEO2.getNIM3_Encounter().getEncounterID()%>,1)">CC ALL Conf</button></td>
      <td align="center" bgcolor="#333333"><button type="button" onclick="cc_all(<%=myEO2.getNIM3_Encounter().getEncounterID()%>,2)">CC ALL SP</button></td>
      <td align="center" bgcolor="#333333"><button type="button" onclick="cc_all(<%=myEO2.getNIM3_Encounter().getEncounterID()%>,3)">CC ALL RP</button></td>
      </tr>
    <tr style="display:none;">
      <td bgcolor="#CCCCCC">&nbsp;</td>
      <td align="center" bgcolor="#EDDBAB"><%
	
	
if (false)
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (working_bltNIM3_Referral.getReferralStatusID().intValue()!=1||!isReady4Alert_ReceiveConfirmation||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=emaila_rc_all&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="CC All" />&nbsp;</td>
      <td align="center" bgcolor="#AAD5EE"><%
if (false )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (working_bltNIM3_Referral.getReferralStatusID().intValue()!=1||!isReady4Alert_ScanPass||!isEActive){out.print(" disabled ");}%> type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=emaila_sp_all&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="CC All" />&nbsp;</td>

      <td align="center" bgcolor="#BBEBAD"><%
if (false )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (working_bltNIM3_Referral.getReferralStatusID().intValue()!=1||!isReady4Alert_Report||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=emaila_rp_all&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="CC All" />&nbsp;</td>
      </tr>
    
    <tr <%if (myPM.getParentPayerID()!=951){%>style="display:none;"<%}%>>
      <td bgcolor="#CCCCCC"><strong>Patient<div class=boxDr><%=NCA.getPatientFirstName()%>&nbsp;<%=NCA.getPatientLastName()%></div>
        </strong>
      </td>
      <td align="center" bgcolor="#EDDBAB">
      	<%
			if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_Pt().before(NIMUtils.getBeforeTime()))
			{
				but_class = "inputButton_md_Action1";
			}
			else
			{
				but_class = "inputButton_md_Default";
			}
			%>
      <input class="<%=but_class%>" id="email_rc_pt" <% if (!isCaseCriticalDone_NoDoc||!isEActive||myEO2.getNIM3_CaseAccount().getPatientEmail().equals("")){out.print(" disabled ");}%> type="button" onclick = "email_pt('<%=myEO2.getNIM3_Encounter().getEncounterID()%>','email_rc_pt');" value="Send"/></td>     
      <td align="center" bgcolor="#AAD5EE">
			<%
			if (myEO2.getNIM3_Encounter().getSentTo_SP_Pt().before(NIMUtils.getBeforeTime()))
			{
				but_class = "inputButton_md_Action1";
			}
			else
			{
				but_class = "inputButton_md_Default";
			}
			%>
        
        <input class="<%=but_class%>" id="email_sp_pt" <% if (!isReady4Alert_ScanPass||myEO2.getNIM3_Encounter().getCCTransactionID().equals(0)||myEO2.getNIM3_CaseAccount().getPatientEmail().equals("")){out.print(" disabled ");}%> type="button" onclick = "email_pt('<%=myEO2.getNIM3_Encounter().getEncounterID()%>','email_sp_pt');" value="Send"/></td>
      <td align="center" bgcolor="#BBEBAD">&nbsp;</td>
      </tr>
    
    <tr>
      <td bgcolor="#CCCCCC"><strong><%if (myPM.getParentPayerID()==951){%>Referring Org<%}else{%>Adjuster<%}%><br>      <div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getCase_Adjuster().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
        </strong>
	    <input class="inputButton_md_Default" type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_adj&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p&FLAG=email';" value="email" />
        </td>
      <td align="center" bgcolor="#EDDBAB"><%
	
	
if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adj().before(NIMUtils.getBeforeTime())&&!myEO2.isAlertSkip_Adj() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isCaseCriticalDone_NoDoc||myEO2.isAlertSkip_Adj()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_adj&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" <%if (myPM.getParentPayerID()==951){%>style="display:none;"<%}%>/>        
      <td align="center" bgcolor="#AAD5EE"><%
if (myEO2.getNIM3_Encounter().getSentTo_SP_Adj().before(NIMUtils.getBeforeTime())&&!myEO2.isAlert2Skip_Adj() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        
        <input class="<%=but_class%>" <% if (!isReady4Alert_ScanPass||myEO2.isAlert2Skip_Adj()||!isEActive){out.print(" disabled ");}%> type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_sp_adj&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send"/></td>
      <td align="center" bgcolor="#BBEBAD"><%
if (myEO2.getNIM3_Encounter().getSentToAdj().before(NIMUtils.getBeforeTime())&&!myEO2.isReportSkip_Adj() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isReady4Alert_Report||myEO2.isReportSkip_Adj()||!isEActive&&myEO2.getNIM3_Encounter().getAppointmentID()>0){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rp_adj&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" <%if (myPM.getParentPayerID()==951){%>style="display:none;"<%}%>/></td>
      </tr>

<%
if (myEO2.getNIM3_CaseAccount().getNurseCaseManagerID()>0)
{
%>
    <tr>
      <td bgcolor="#CCCCCC"><strong>Nurse Case Manager<br><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getCase_NurseCaseManager().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
        </strong>
	    <input class="inputButton_md_Default" type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_ncm&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p&FLAG=email';" value="email" />
        </td>
      <td align="center" bgcolor="#EDDBAB"><%
if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_NCM().before(NIMUtils.getBeforeTime())&&!myEO2.isAlertSkip_NCM() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isCaseCriticalDone_NoDoc||myEO2.isAlertSkip_NCM()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_ncm&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      <td align="center" bgcolor="#AAD5EE"><%
if (myEO2.getNIM3_Encounter().getSentTo_SP_NCM().before(NIMUtils.getBeforeTime())&&!myEO2.isAlert2Skip_NCM() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isReady4Alert_ScanPass||myEO2.isAlert2Skip_NCM()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_sp_ncm&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      <td align="center" bgcolor="#BBEBAD"><%
if (myEO2.getNIM3_Encounter().getSentTo_RP_NCM().before(NIMUtils.getBeforeTime())&&!myEO2.isReportSkip_NCM() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isReady4Alert_Report||myEO2.isReportSkip_NCM()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rp_ncm&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      </tr>
<%
}
if (myEO2.getNIM3_CaseAccount().getCaseAdministratorID()>0)
{
%>
    
    <tr>
      <td bgcolor="#CCCCCC"><strong>Case Admin<br><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getCase_Admin().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
        </strong>
	    <input class="inputButton_md_Default" type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_adm&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p&FLAG=email';" value="email" />  
        </td>
      <td align="center" bgcolor="#EDDBAB"><%
if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adm().before(NIMUtils.getBeforeTime())&&!myEO2.isAlertSkip_Adm() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isCaseCriticalDone_NoDoc||myEO2.isAlertSkip_Adm()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_adm&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      <td align="center" bgcolor="#AAD5EE"><%
if (myEO2.getNIM3_Encounter().getSentTo_SP_Adm().before(NIMUtils.getBeforeTime())&&!myEO2.isAlert2Skip_Adm() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isReady4Alert_ScanPass||myEO2.isAlert2Skip_Adm()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_sp_adm&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      <td align="center" bgcolor="#BBEBAD"><%
if (myEO2.getNIM3_Encounter().getSentToAdm().before(NIMUtils.getBeforeTime())&&!myEO2.isReportSkip_Adm() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isReady4Alert_Report||myEO2.isReportSkip_Adm()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rp_adm&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      </tr>
 <%
}
if (myEO2.getNIM3_CaseAccount().getCaseAdministrator2ID()>0)
{
%>
   
    <tr>
      <td bgcolor="#CCCCCC"><strong>Case Admin 2<br><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getCase_Admin2().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
        </strong>
	    <input class="<%=but_class%>"  type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_adm2&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p&FLAG=email';" value="email" />
        </td>
      <td align="center" bgcolor="#EDDBAB"><%
if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adm2().before(NIMUtils.getBeforeTime())&&!myEO2.isAlertSkip_Adm2())
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isCaseCriticalDone_NoDoc||myEO2.isAlertSkip_Adm2()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_adm2&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      <td align="center" bgcolor="#AAD5EE"><%
if (myEO2.getNIM3_Encounter().getSentTo_SP_Adm2().before(NIMUtils.getBeforeTime())&&!myEO2.isAlert2Skip_Adm2() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isReady4Alert_ScanPass||myEO2.isAlert2Skip_Adm2()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_sp_adm2&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      <td align="center" bgcolor="#BBEBAD"><%
if (myEO2.getNIM3_Encounter().getSentTo_RP_Adm2().before(NIMUtils.getBeforeTime())&&!myEO2.isReportSkip_Adm2() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isReady4Alert_Report||myEO2.isReportSkip_Adm2()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rp_adm2&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      </tr>
 <%
}
if (myEO2.getNIM3_CaseAccount().getCaseAdministrator3ID()>0)
{
%>

    <tr>
      <td bgcolor="#CCCCCC"><strong>Case Admin 3<br><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getCase_Admin3().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
        </strong>
	    <input class="inputButton_md_Default" type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_adm3&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p&FLAG=email';" value="email" />
        </td>
      <td align="center" bgcolor="#EDDBAB"><%
if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adm3().before(NIMUtils.getBeforeTime())&&!myEO2.isAlertSkip_Adm3())
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isCaseCriticalDone_NoDoc||myEO2.isAlertSkip_Adm3()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_adm3&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      <td align="center" bgcolor="#AAD5EE"><%
if (myEO2.getNIM3_Encounter().getSentTo_SP_Adm3().before(NIMUtils.getBeforeTime())&&!myEO2.isAlert2Skip_Adm3() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isReady4Alert_ScanPass||myEO2.isAlert2Skip_Adm3()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_sp_adm3&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      <td align="center" bgcolor="#BBEBAD"><%
if (myEO2.getNIM3_Encounter().getSentTo_RP_Adm3().before(NIMUtils.getBeforeTime())&&!myEO2.isReportSkip_Adm3() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isReady4Alert_Report||myEO2.isReportSkip_Adm3()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rp_adm3&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      </tr>
    <%
}
if (myEO2.getNIM3_CaseAccount().getCaseAdministrator4ID()>0)
{
%>
 
    <tr>
      <td bgcolor="#CCCCCC"><strong>Case Admin 4<br><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getCase_Admin4().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
        </strong>
	    <input class="<%=but_class%>" type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_adm4&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p&FLAG=email';" value="email" />
        </td>
      <td align="center" bgcolor="#EDDBAB"><%
if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_Adm4().before(NIMUtils.getBeforeTime())&&!myEO2.isAlertSkip_Adm4())
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isCaseCriticalDone_NoDoc||myEO2.isAlertSkip_Adm4()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_adm4&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      <td align="center" bgcolor="#AAD5EE"><%
if (myEO2.getNIM3_Encounter().getSentTo_SP_Adm4().before(NIMUtils.getBeforeTime())&&!myEO2.isAlert2Skip_Adm4() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isReady4Alert_ScanPass||myEO2.isAlert2Skip_Adm4()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_sp_adm4&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      <td align="center" bgcolor="#BBEBAD"><%
if (myEO2.getNIM3_Encounter().getSentTo_RP_Adm4().before(NIMUtils.getBeforeTime())&&!myEO2.isReportSkip_Adm4() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>" <% if (!isReady4Alert_Report||myEO2.isReportSkip_Adm4()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rp_adm4&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      </tr>  
<%
}
%>
    <tr>
      <td bgcolor="#333333">&nbsp;</td>
      <td align="center" bgcolor="#333333">&nbsp;</td>
      <td align="center" bgcolor="#333333">&nbsp;</td>
      <td align="center" bgcolor="#333333">&nbsp;</td>
      </tr>
    <tr>
      <td bgcolor="#CCCCCC"><strong >Referring Provider<br><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getReferral_ReferringDoctor().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
        </strong>
	    <input class="inputButton_md_Default" type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_refdr&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p&FLAG=email';" value="email" />
        </td>
      <td align="center" bgcolor="#EDDBAB"><%
if (myEO2.getNIM3_Encounter().getSentTo_ReqRec_RefDr().before(NIMUtils.getBeforeTime())&&!myEO2.isAlertSkip_RefDr() )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>"     <% if (!isReady4Alert_ReceiveConfirmation||myEO2.isAlertSkip_RefDr()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rc_refdr&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      <td align="center" bgcolor="#AAD5EE"><%
if (myEO2.getNIM3_Encounter().getSentTo_SP_RefDr().before(NIMUtils.getBeforeTime()) &&!myEO2.isAlert2Skip_RefDr())
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>"     <% if (!isReady4Alert_ScanPass||myEO2.isAlert2Skip_RefDr()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_sp_refdr&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      <td align="center" bgcolor="#BBEBAD"><%
if (myEO2.getNIM3_Encounter().getSentToRefDr().before(NIMUtils.getBeforeTime()) &&!myEO2.isReportSkip_RefDr())
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
        <input class="<%=but_class%>"     <% if (!isReady4Alert_Report||myEO2.isReportSkip_RefDr()||!isEActive){out.print(" disabled ");}%>   type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_rp_refdr&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      </tr>
    <tr>
      <td bgcolor="#CCCCCC"><strong>IC/Provider</strong></td>
      <td align="center" bgcolor="#EDDBAB">&nbsp;</td>
      <td align="center" bgcolor="#AAD5EE"><%
if (myEO2.getNIM3_Encounter().getSentTo_SP_IC().before(NIMUtils.getBeforeTime()) )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
  <strong></strong>      <input class="<%=but_class%>"   <% if (!isReady4Alert_ScanPass||!isEActive){out.print(" disabled ");}%>    type="button" onclick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=send_sp_ic&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Send" /></td>
      
      <td align="center" bgcolor="#BBEBAD">&nbsp;</td>
      </tr>
  </table>
</td>
    <td valign="top">

<% if (isScheduler2)
{
%>
<center><input type="button" class="inputButton_md_Default"  onclick="this.disabled=true;modalPost('CASEID', modalWin('LI_tCaseAccountFull_edit.jsp?QUICKVIEW=yes&EDIT=nim3track&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Search...','dialogWidth:1400px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1400,height=800'));document.location=document.location;" value="Tracking Info"><hr>
<input type="button" class="inputButton_md_Default"  onclick="this.disabled=true;modalPost('CASEID', modalWin('LI_tCaseAccountFull_edit.jsp?QUICKVIEW=yes&EDIT=nim3expbill&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Search...','dialogWidth:1400px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1400,height=800'));document.location=document.location;" value="Billing"> </center>
<%}
%>


<% if (true&&isScheduler3)
{
%>
<hr>
<center><input type="button" class="inputButton_md_Default"  onclick="this.disabled=true;modalPost('CASEID', modalWin('LI_tCaseAccountFull_edit.jsp?QUICKVIEW=yes&EDIT=nim3expbill2&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Search...','dialogWidth:1400px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1400,height=800'));document.location=document.location;" value="Billing [2]"> </center>
<center><a  href="tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?QUICKVIEW=yes&EDIT=nim3expbill2&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p">Test</a></center>
<hr>
<% 
}
%>



<% if (isScheduler2)
{
%>


<table border="2" align="center" cellpadding="2" cellspacing="0">
  <tr>
    <td bgcolor="#CCCCCC" class="tdHeader" >Billing&nbsp;</td>
    <td align="center" bgcolor="#AAD5EE">Billed</td>
    <td align="center" bgcolor="#AAD5EE">Paid</td>
  </tr>
  <tr>
    <td colspan="3" nowrap bgcolor="#CCCCCC">Expected In: <strong>$<%=myBO.Round2(myBO.getPayer_AllowAmount()+myBO.getPayer_AllowAmount_Adjustment())%></strong>&nbsp;&nbsp;&nbsp;Expected Out: <strong>$<%=myBO.getProvider_AllowAmount()%></strong>&nbsp;&nbsp;&nbsp;Expected Net: <span <%if (myBO.getNetExpected()<=0){%>class="tdBaseAltRed"<%}%>><strong>$<%=myBO.getNetExpected()%></strong></span></td>
    </tr>
  <tr>
    <td colspan="3" nowrap bgcolor="#CCCCCC">Actual In: <strong>$<%=myBO.getPayer_ActualAmount()%></strong>&nbsp;&nbsp;&nbsp;Actual Out: <strong>$<%=myBO.getProvider_ActualAmount()%></strong>&nbsp;&nbsp;&nbsp;Actual Net: <span <%if (myBO.getNetActual()<=0){%>class="tdBaseAltRed"<%}%>><strong>$<%=myBO.getNetActual()%></strong></span></td>
    </tr>
  <tr>
    <td bgcolor="#CCCCCC"><strong>Payer</strong></td>
    <td align="center" bgcolor="#AAD5EE"><%
if (myEO2.getNIM3_Encounter().getSentTo_Bill_Pay().before(new java.util.Date(1)) )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}

%>  <% if (myEO2.getNIM3_Encounter().getPricing_Structure()==2) {%><strong><span class="instructions">Tier 2</span></strong><br><%} %>
      <input class="<%=but_class%>" <% if (myEO2.getNIM3_Appointment()==null||!isCaseCriticalDone||!isEActive||(myBO.getNetExpected()<=0&&!isScheduler3)){out.print(" disabled ");}%>    type=button onClick = "if (<%=myBO.getNetExpected()%><=0){if (confirm('Upside Down - Are you sure you want to print Bill?')){this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=print_bill_pay&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';}}else{this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=print_bill_pay&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';}" value="Print"></td>
    <td align="center" bgcolor="#AAD5EE">
      <%
if (!NIMUtils.isValidEmail(myUAdj.getContactEmail())&&myEO2.getNIM3_Encounter().getSentTo_SP_Adj().before(new java.util.Date(1)) )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
      <input class="<%=but_class%>"  disabled="disabled"  type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=print_sp_adj&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p'" value="Rec'd">
</td>
    </tr>
  <tr>
    <td bgcolor="#CCCCCC"><strong>Provider</strong></td>
    <td align="center" bgcolor="#AAD5EE"><%
if (NIMUtils.isValidEmail(myEO2.getReferral_ReferringDoctor().getContactEmail())&&myEO2.getNIM3_Encounter().getSentTo_SP_RefDr().before(new java.util.Date(1)) )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}
%>
      <input class="<%=but_class%>" disabled="disabled"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=print_sp_adj&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p'" value="Rec'd"></td>
    <td align="center" bgcolor="#AAD5EE"><%
if (myEO2.getNIM3_Encounter().getTimeTrack_ReqPaidOut().before( new java.util.Date(1)) )
{
	but_class = "inputButton_md_Action1";
}
else
{
	but_class = "inputButton_md_Default";
}

%>
      <input class="<%=but_class%>" <% if (!isCaseCriticalDone||!isEActive){out.print(" disabled ");}%>    type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=print_rem_pro&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p'" value="Print">
  </td>
  </tr>

  </table>

<%
}%>

</td>
  </tr>
    <tr>
  <td>
  <table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td><p class="tdHeader">All Documents:<br>
			<input class="inputButton_sm_Create"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=updoc&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p'" value="Add Doc"> </p></td>
  </tr>
  <tr id="AllDox_<%=myEO2.getNIM3_Encounter().getEncounterID()%>">
    <td>
		<table width="100%"  border="0" cellspacing="0" cellpadding="3" class="certborderPrintInt">
<tr class="tdBaseAlt2">
<td nowrap>Action</td>
<td nowrap>Description</td>
<td nowrap>Date Added</td>
<td nowrap>User ID</td>
<td nowrap>&nbsp;</td>
<td nowrap>Remove</td>
</tr>    
		    <%
	searchDB2 mySS_dox = new searchDB2();
	try
	{
		java.sql.ResultSet myRS_dox = null;
		String mySQL_dox = ("SELECT documentid from tnim3_document where (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=0) OR (encounterid=" + myEO2.getNIM3_Encounter().getEncounterID() +")  order by uniquecreatedate desc");
		//out.println(mySQL);
  
		myRS_dox = mySS_dox.executeStatement(mySQL_dox);
		int i2cnt_dox=0;
		while(myRS_dox!=null&&myRS_dox.next())
		{
			i2cnt_dox++;
			bltNIM3_Document myDoc = new bltNIM3_Document (new  Integer(myRS_dox.getString("documentid")));
			String myDoxClass = "tdBase";
			boolean noRemove=false;
			if (myDoc.getDocType().intValue()==-1)
			{
				myDoxClass = "tdBaseAltStrikeRemove";
			}
			else if (myDoc.getDocumentID().intValue()==myEO2.getNIM3_Encounter().getReportFileID().intValue()||myDoc.getDocumentID().intValue()==working_bltNIM3_Referral.getRxFileID().intValue()||myDoc.getDocumentID().intValue()==working_bltNIM3_Referral.getOrderFileID().intValue()||myDoc.getDocumentID().intValue()==myEO2.getNIM3_Encounter().getBillingHCFA_ToPayer_FileID().intValue()||myDoc.getDocumentID().intValue()==myEO2.getNIM3_Encounter().getBillingHCFA_FromProvider_FileID().intValue())
			{
				myDoxClass = "tdBaseAltGreen";
				noRemove = true;
			}
			%>

  <tr class="<%=myDoxClass%>">
		<td nowrap><input  class="inputButton_sm_Default"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=myDoc.getDocumentID()%>&KM=p';" value="View" /></td>
		<td nowrap><strong><%=myDoc.getDocName()%> [<% if (myDoc.getComments().equals("Order")){out.println("Auth");}else{out.println(myDoc.getComments());}%>]</strong></td>
		<td nowrap><strong><jsp:include page="../generic/DateTypeConvertDT_Main.jsp" flush="true" >
          <jsp:param name="CurrentSelection" value="<%=dbdf.format(myDoc.getUniqueCreateDate())%>" />          
          </jsp:include></strong></td>
		<td width="75%"  nowrap><strong><%=myDoc.getUploadUserID()%></strong></td>
		<td width="25%" align="right"><%if (myDoc.getEncounterID().intValue()==myEO2.getNIM3_Encounter().getEncounterID().intValue())
		{
			out.println("E");
		}
		else
		{
			out.println("R");
		}%>&nbsp;</td>
		<td>
		<!-- 
			< %if (true){%><input class="inputButton_sm_Default"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=remdoc&EDITID=< %=myDoc.getDocumentID()%>&KM=p';" value="Remove" />< %}%>
		 -->	
			<%if (!noRemove){%>
				<input class="inputButton_sm_Default"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=remdoc&EDITID=<%=myDoc.getDocumentID()%>&KM=p';" value="Remove" />
			<%}%>

		</td>
  </tr>
			<%
		}
		mySS_dox.closeAll();
	}
	catch(Exception e)
	{
			out.println("ResultsSet:"+e);
	}
	finally {
		mySS_dox.closeAll();
	}
%>

</table>
    
    
    </td>
  </tr>
</table>
  </td>
  </tr>
</table>

<!-- Conf Actions End -->&nbsp;</td>
					  </tr>
<tr><td colspan="12">




</td></tr>

					<%
				}
				mySS.closeAll();
			}
			catch(Exception e)
			{
				out.println("ResultsSet:"+e);
			}
			finally {
				mySS.closeAll();
			}
		%>
        </table>
        <%
			if (Ecnt==0&&working_bltNIM3_Referral.getRxFileID()>0)
			{
				%>
                	<input class="inputButton_md_Action1" type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=openflow&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Add Encounter">
                <%
			}
		%>
	</div> 
</td></tr></table>


</td>


 </tr>
        <%
    }//end while
	
			if (tEcnt==1&&Rcnt==1)
			{
				%>
<%
			}
	
       }//end of if
       else 
       {
           %>
           <tr><td colspan=3><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tNIM3_Referral")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tNIM3_Referral")%>"))
           {
               document.location="tNIM3_Referral_main_NIM3_Referral_CaseID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>