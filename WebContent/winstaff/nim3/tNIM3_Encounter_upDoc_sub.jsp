<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*, org.apache.commons.fileupload.*" %>
<%/*
    filename: tNIM3_Encounter_main_NIM3_Encounter_CaseID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0  >
    <tr><td width=10>&nbsp;</td><td>
      <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM3_Encounter")%>
      
      
      
  <%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    Integer        iReferralID        =    null;
	bltNIM3_Encounter myEncounter = null;
	bltNIM3_Referral myRef = null;
	
	String fileToUpload = null; 
	
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
	Integer iAuthID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
	    iEncounterID = (Integer)pageControllerHash.get("iEncounterID");
		myEncounter = new bltNIM3_Encounter(iEncounterID);
		myRef = new bltNIM3_Referral(myEncounter.getReferralID());
        accessValid = true;
    }
    else if (pageControllerHash.containsKey("iReferralID")) 
    {
	    iReferralID = (Integer)pageControllerHash.get("iReferralID");
		myRef = new bltNIM3_Referral(iReferralID);
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
	  boolean isPassed=true;
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
	
		bltNIM3_CaseAccount myCase = new bltNIM3_CaseAccount(myRef.getCaseID());
		String myFileSS = "";
		String DocType = "";
		String outDocType="";

   try
	    {
//		  out.println("Test1: " + iHCOID);

			// Create a new file upload handler
			DiskFileUpload upload = new DiskFileUpload();
			
			// Set upload parameters
			
			//need to put these in config
			upload.setSizeThreshold(1900000);
			upload.setSizeMax(1900000);
//			String myRPath = "/var/lib/tomcat5/webapps/nimdox/";
			String myRPath = ConfigurationInformation.sUploadFolderDirectory;
//			upload.setRepositoryPath("c:\\web\\webapps\\nimdox\\");
			upload.setRepositoryPath(myRPath);			
			// Parse the request
			java.util.List /* FileItem */ items = upload.parseRequest(request);
			java.util.Iterator iter = items.iterator();
 	        java.text.SimpleDateFormat fileDF = new java.text.SimpleDateFormat("yyyy-MM-dd" );
			String sFileDF = "doc_" + com.winstaff.password.RandomString.generateString(6).toLowerCase() + "_"+ fileDF.format(PLCUtils.getNowDate(false)) + "_" + myCase.getCaseCode();
			if (iter.hasNext()) 
			{
			    FileItem item = (FileItem) iter.next();
				String oFileName = item.getName();
				oFileName = oFileName.substring(oFileName.lastIndexOf("\\")+1,oFileName.length());
				oFileName = oFileName.replace(' ' , '_');				
				myFileSS= sFileDF + "_" + oFileName;
				String nFileName = myRPath + myFileSS;
				fileToUpload = nFileName;
				java.io.File uploadedFile =new java.io.File(nFileName);
				if (item.getName().indexOf(".pdf")<=0&&item.getName().indexOf(".tiff")<=0&&item.getName().indexOf(".tif")<=0)
				{
					isPassed = false;
					%>
					<script language="javascript">
					alert("Your file does not appear to be a PDF or TIF file.  Files must be uploaded in PDF format.");
	//				document.location='tNIM3_Referral_main_NIM3_Referral_CaseID_4Payer.jsp';
					window.close();
					</script>	
                    <%	
				}
				else if (item.getSize()==0)
				{
					isPassed = false;
					%>
					<script language="javascript">
					alert("Your file appears to have a size of ZERO or is locked/open by another user.\n\nYour document has NOT been uploaded successfully.");
	//				document.location='tNIM3_Referral_main_NIM3_Referral_CaseID_4Payer.jsp';
					window.close();
					</script>	
                    <%	
				}
				else
				{
				    	item.write(uploadedFile);								
				}
			}
			if (isPassed)
			{
				if (iter.hasNext()) 
				{
					FileItem item = (FileItem) iter.next();
					DocType = item.getString();
					
					if(DocType.equals("uphcfapro")){outDocType="HCFA (Provider)";}
					if(DocType.equals("uphcfapay")){outDocType="HCFA (Payer)"; }
					if(DocType.equals("upor")){outDocType="Auth"; }
					if(DocType.equals("uprx")){outDocType="Rx"; }
					if(DocType.equals("upreport")){outDocType="Report"; }
								
					
					
				}
	
				boolean isDone = false;
				bltNIM3_Document myDoc = new bltNIM3_Document();
				myDoc.setFileName(myFileSS);
				myDoc.setComments("Doc");
				myDoc.setReferralID(myRef.getReferralID());
				if (iEncounterID!=null)
				{
					myDoc.setEncounterID(iEncounterID);
				}
				myDoc.setUploadUserID(CurrentUserAccount.getUserID());
				myDoc.setCaseID(myCase.getCaseID());
				myDoc.commitData();
				String UserLogonDescription = "n/a";
				if (pageControllerHash.containsKey("UserLogonDescription"))
				{
					UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
				}
				
				//if current user uploading is not the same as the person the case is assigned to, then make send email and make commtrack
				if (CurrentUserAccount.getUserID().intValue()!=myCase.getAssignedToID().intValue())
				{
			        bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
					bltNIM3_CommTrack	working_bltNIM3_CommTrack = new bltNIM3_CommTrack();
					if (iEncounterID!=null)
					{
						working_bltNIM3_CommTrack.setEncounterID(iEncounterID);
					}
					working_bltNIM3_CommTrack.setReferralID(myRef.getReferralID());
					working_bltNIM3_CommTrack.setCaseID(myCase.getCaseID());
					working_bltNIM3_CommTrack.setMessageName(CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName());
					working_bltNIM3_CommTrack.setMessageCompany(myPM.getPayerName());
					working_bltNIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
					working_bltNIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
					working_bltNIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
					working_bltNIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
		            working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
					
					//working_bltNIM3_CommTrack.setUniqueModifyComments(""+UserLogonDescription);
					String theMessage = outDocType+" Uploaded by another user ["+CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName()+"] to one of your cases.";
					working_bltNIM3_CommTrack.setMessageText(theMessage);
					working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
					emailType_V3 myEM = new emailType_V3();
					bltUserAccount myAT = new bltUserAccount(myCase.getAssignedToID());
					myEM.setTo(myAT.getContactEmail());
					myEM.setFrom("server@nextimagemedical.com");
					if (myEncounter!=null)
					{
						myEM.setSubject(outDocType+" Uploaded for SP: " + myEncounter.getScanPass() + "["+ myCase.getPatientLastName() +", "+myCase.getPatientFirstName() + "]");
					}
					else 
					{
						myEM.setSubject(outDocType+" Uploaded for Claim#: " + myCase.getCaseClaimNumber() + "["+ myCase.getPatientLastName() +", "+myCase.getPatientFirstName() + "]");
					}
					myEM.setBody(theMessage);
					myEM.isSendMail();
					//displayNotes = "Email sent to NetDev";
					working_bltNIM3_CommTrack.setUniqueModifyComments(UserLogonDescription);
					working_bltNIM3_CommTrack.commitData();
				}
				
				
				if (DocType.equalsIgnoreCase("upreport"))
				{
					myDoc.setEncounterID(myEncounter.getEncounterID());
					myDoc.setComments("Rp");
					myDoc.setDocName("Diagnostic Report");
					myDoc.setUniqueModifyComments(UserLogonDescription);
					myDoc.commitData();
					myEncounter.setReportFileID(myDoc.getUniqueID());
//					myEncounter.commitData();
					if (myEncounter.getTimeTrack_ReqDelivered().before( new java.util.Date(1)))
					{
						myEncounter.setTimeTrack_ReqDelivered(PLCUtils.getNowDate(true));
						myEncounter.setTimeTrack_ReqDelivered_UserID(CurrentUserAccount.getUserID());
					}
					myEncounter.setUniqueModifyComments(UserLogonDescription);
					myEncounter.commitData();
					isDone = true;
				}
				else if (DocType.equalsIgnoreCase("uprx"))
				{
					myDoc.setComments("Rx");
					myDoc.setDocName("Rx");
					myDoc.setUniqueModifyComments(UserLogonDescription);
					myDoc.commitData();
					myRef.setRxFileID(myDoc.getUniqueID());
					myRef.setUniqueModifyComments(UserLogonDescription);
					myRef.commitData();
					NIMUtils.processAllEncounters(myRef.getReferralID(), CurrentUserAccount);
					isDone = true;
				}
				else if (DocType.equalsIgnoreCase("upor"))
				{
					myDoc.setComments("Order");
					myDoc.setDocName("Or");
					myDoc.setUniqueModifyComments(UserLogonDescription);
					myDoc.commitData();
					myRef.setOrderFileID(myDoc.getUniqueID());
					myRef.setUniqueModifyComments(UserLogonDescription);
					myRef.commitData();
					NIMUtils.processAllEncounters(myRef.getReferralID(), CurrentUserAccount);
					isDone = true;
				}
				else if (DocType.equalsIgnoreCase("uphcfapay"))
				{
					myDoc.setComments("HCFA To Payer");
					myDoc.setDocName("HCFA");
					myDoc.setUniqueModifyComments(UserLogonDescription);
					myDoc.commitData();
					myEncounter.setBillingHCFA_ToPayer_FileID(myDoc.getUniqueID());
					if (!myEncounter.getSentTo_Bill_Pay().after(NIMUtils.getBeforeTime()))
					{
						myEncounter.setSentTo_Bill_Pay(PLCUtils.getNowDate(true));
					}
					myEncounter.setUniqueModifyComments(UserLogonDescription);
					myEncounter.commitData();
        

					isDone = true;
				}
				else if (DocType.equalsIgnoreCase("uphcfapro"))
				{
					myDoc.setComments("HCFA From Provider");
					myDoc.setDocName("HCFA");
					myDoc.setUniqueModifyComments(UserLogonDescription);
					myDoc.commitData();
					myEncounter.setBillingHCFA_FromProvider_FileID(myDoc.getUniqueID());
					if (!myEncounter.getRec_Bill_Pro().after(NIMUtils.getBeforeTime()))
					{
				        myEncounter.setRec_Bill_Pro(PLCUtils.getNowDate(true));
					}
					myEncounter.setUniqueModifyComments(UserLogonDescription);
					if (myEncounter.getEncounterTypeID()==NIMUtils.ENCOUNTER_TYPE_EMG)
					{
						myEncounter.setEncounterStatusID(new Integer(9));
						myEncounter.commitData();
						bltNIM3_CommTrack myCT = new bltNIM3_CommTrack();
						myCT.setUniqueModifyComments("JSP:Encounter_upDoc_sub:System Generated");
						myCT.setReferralID(myEncounter.getReferralID());
						myCT.setEncounterID(myEncounter.getEncounterID());
						myCT.setCommStart(PLCUtils.getNowDate(false));
						myCT.setCommEnd(PLCUtils.getNowDate(false));
						myCT.setAlertStatusCode(new Integer(1));
						myCT.setMessageName("System Generated");
						myCT.setMessageText("HCFA Uploaded for EMG. Encounter Moved to Billing");
						myCT.commitData();
					}

						
					myEncounter.commitData();
					isDone = true;
					String dateRec = request.getParameter("Rec_Bill_Pro");
				    String amount = request.getParameter("AmountBilledByProvider");
				    String hcfainvoice = request.getParameter("HCFAinvoice");%>
				    <form method="get" action="tNIM3_Encounter_upDoc_sub3.jsp" name="at_intake">
	                    <input type="hidden" name="EDIT" value="uphcfapro"/>
        				<input type="hidden" name="EDITID" value = "<%=pageControllerHash.get("iReferralID")%>" > 
						<input maxlength=20  type=hidden size="30" name="Rec_Bill_Pro" value='<%=dateRec%>' >                  
	                    <input maxlength=10  type=hidden size="20" name="AmountBilledByProvider" value="<%=amount%>">
                        <input maxlength=10  type=hidden size="20" name="HCFAinvoice" value="<%=hcfainvoice%>">
					</form>
				    <script language="javascript">
						document.at_intake.submit();	
					</script>	
				
				<%}
				pageControllerHash.put("iDocumentID",myDoc.getUniqueID());
				session.setAttribute("pageControllerHash",pageControllerHash);
				
				if (isDone&&isAdjuster||isAdjuster2||isAdjuster3||isProvider)
				{
					%>
					<script language="javascript">
					alert("Document Uploaded");
					document.location='tNIM3_Referral_main_NIM3_Referral_CaseID_4Payer.jsp';
					//window.close();
					</script>		
					<%
				}
				else if (isDone&&isScheduler)
				{
					%>
					<script language="javascript">
					alert("Document Uploaded");
					//document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp';
					window.close();
					</script>		
					<%
				}
				else
				{
					%>
					
				</td></tr>
				<tr>
				  <td>&nbsp;</td>
				  <td><h1>  Document Uploaded</h1></td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td><form id="form1" name="form1" method="post" action="tNIM3_Encounter_upDoc_sub2.jsp">
					<p>Please choose a report type:</p>
					<p>
					  <label>
						<select name="DocType" id="DocType">
						  <option>Billing - Misc</option>
						  <option>Tier 3 Auth</option>
						  <option>Capability Report</option>
						  <option>Chart Notes</option>
						  <option>FCE</option>
						  <option>Medical Report</option>
						  <option>OTA</option>					
						  <option>Other</option>
						  <option>IC Fax</option>					
						  <option>Full Fax</option>
						</select>
					  </label>
					</p>
					<p>
					  <input name="Submit" type="submit" value="Continue" />
					</p>
				  </form></td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
				</table>
					
				
					<%
					
				}

				if (ConfigurationInformation.storeDocumentsInDatabase) {
					
					// Take file from disc and store in db.
					DocumentDatabase dd = new DocumentDatabase();					
			        DebugLogger.println(">>> File to upload "+ fileToUpload );	
			        // Insert filename first
			        dd.insertFilename(myFileSS);
			        // Serialize file to blob and store.
			    	dd.writeFile(fileToUpload);
				}		
					
			}

	}
	catch (Exception eeee)
	{
				%>
				<script language="javascript">
				alert("There was an error uploading your document\n\nThe file size likely exceeds the max size.\n\n<%=eeee%>");
//				document.location='tNIM3_Referral_main_NIM3_Referral_CaseID_4Payer.jsp';
				window.close();
				</script>		
				<%
		out.println("******" + eeee + "*****");
	}

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
      
