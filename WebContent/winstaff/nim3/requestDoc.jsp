<%@page contentType="application/pdf" language="java"
	import="com.winstaff.*"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%
	try {

		String inFileName = request.getParameter("doc");

		String myRPath = ConfigurationInformation.sUploadFolderDirectory;
		String sFileName = myRPath + inFileName;

		// Get from file system.
		response.getOutputStream().write(
				DataControlUtils.FileToBytes(sFileName));

	} catch (Exception e) {
		out.print("Error " + e);
	}
%>