<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tNIM3_CaseAccount_form.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<script language="javascript">
function formatSSN()
{
     var theCount = 0;
     var theString = document.getElementById("PatientSSN").value;
     var newString = "";
     var myString = theString;
     var theLen = myString.length;
     for ( var i = 0 ; i < theLen ; i++ )
     {
     // Character codes for ints 1 - 9 are 48 - 57
          if ( (myString.charCodeAt(i) >= 48 ) && (myString.charCodeAt(i) <= 57) )
          newString = newString + myString.charAt(i);   
     }
// Now the validation to determine that the remaining string is 9 characters.
     if (newString.length == 9 )
     {
// Now the string has been stripped of other chars it can be reformatted to ###-##-#### 
          var newLen = newString.length;
          var newSSN = "";
          for ( var i = 0 ; i < newLen ; i++ )
          {
               if ( ( i == 2 ) || ( i == 4 ) )
               {
                    newSSN = newSSN + newString.charAt(i) + "-";
               }else{
                    newSSN = newSSN + newString.charAt(i);
               }
          }
          document.getElementById("PatientSSN").value = newSSN;
          return true;
     }else{
          alert("The Social Security Number you entered "+newString+" does not contian the correct number of digits");
          document.getElementById("PatientSSN").focus();
          return false;
     }
}

function formatPhone( myElement)
{
     var theCount = 0;
     var theString = document.getElementById(myElement).value;
     var newString = "";
     var myString = theString;
     var theLen = myString.length;
     for ( var i = 0 ; i < theLen ; i++ )
     {
     // Character codes for ints 1 - 9 are 48 - 57
          if ( (myString.charCodeAt(i) >= 48 ) && (myString.charCodeAt(i) <= 57) )
          newString = newString + myString.charAt(i);   
     }
// Now the validation to determine that the remaining string is 9 characters.
     if (newString.length == 10 )
     {
// Now the string has been stripped of other chars it can be reformatted to ###-##-#### 
          var newLen = newString.length;
          var newSSN = "";
          for ( var i = 0 ; i < newLen ; i++ )
          {
               if ( ( i == 2 ) || ( i == 5 ) )
               {
                    newSSN = newSSN + newString.charAt(i) + "-";
               }else{
                    newSSN = newSSN + newString.charAt(i);
               }
          }
          document.getElementById(myElement).value = newSSN;
          return true;
     }else{
          alert("The Phone Number you entered "+newString+" does not contian the correct number of digits");
          document.getElementById(myElement).focus();
          return false;
     }
}
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
</script>



<%

	String sQUICKVIEW = "no";
	if ( request.getParameter( "QUICKVIEW" ) != null && request.getParameter( "QUICKVIEW" ).equalsIgnoreCase("yes") )
    {
        sQUICKVIEW        =    "yes";
    }




String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
	tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear_fade.jsp?plcID="+thePLCID;
if (sQUICKVIEW.equalsIgnoreCase("yes"))
{
}
else
{
%>	<p class=title>Case Create</p><hr><%
}
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">

<script language="javascript">
show_pws_Loading();
</script>



    <table cellpadding=0 cellspacing=0 border=0  >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tNIM3_CaseAccount")%>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    Integer        iCaseID        =    null;
    Integer        iReferralID        =    null;
    Integer        iEDITID        =    null;

    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iCaseID")&&pageControllerHash.containsKey("iEncounterID")&&pageControllerHash.containsKey("iReferralID")) 
    {
		iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
		iReferralID        =    (Integer)pageControllerHash.get("iReferralID");
        accessValid = true;
 //       if (request.getParameter( "EDIT" ).equalsIgnoreCase("nim3exp")&&iEncounterID.intValue() != iEDITID.intValue())
        {
//        	accessValid = false;
        }
    //    else if (iCaseID.intValue() != iEDITID.intValue())
        {
//        	accessValid = false;
        }
    }
  //page security
  if (accessValid&&isScheduler)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_CaseAccount_express1_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID

    bltNIM3_CaseAccount        NIM3_CaseAccount        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        NIM3_CaseAccount        =    new    bltNIM3_CaseAccount(iCaseID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("nim3exp_svc") )
    {
        NIM3_CaseAccount        =    new    bltNIM3_CaseAccount(iCaseID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_CaseAccount        =    new    bltNIM3_CaseAccount(UserSecurityGroupID, true);
    }

//fields
        %>
        <form action="tNIM3_CaseAccount_express1_form_sub.jsp" name="tNIM3_CaseAccount_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
    %>
        <input type="hidden" name="EDITID" value = "<%=iEDITID%>" >  
        <input type="hidden" name="QUICKVIEW" value = "<%=sQUICKVIEW%>" >  

          <%  String theClass ="tdBase";%>
        <table border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr>
           <td class=tableColor>
            <table cellpadding=0 cellspacing=0 width=50% >
                     <tr>
                       <td class=tdHeaderAlt>&nbsp;</td>
                       <td class=tdHeaderAlt align="left">&nbsp;  <%if (sQUICKVIEW.equalsIgnoreCase("yes")){%>
                         <input  class="inputButton_md_Create" onClick = "MM_validateForm('CaseClaimNumber','','R');if (document.MM_returnValue){this.disabled=true;$('table').fadeOut('slow');show_pws_Saving();submit();return false}else{this.disabled=false;return false}" type=Submit value="Save" name=Submit3>
<input class="inputButton_md_Default" onClick = "this.disabled=true;window.close();" type=button value="Close" name=Submit2><%}%>                         </td>
                       <td class=tdHeaderAlt></td>
                       
                     </tr>
                     <tr>
                       <td class=tdHeaderAlt>&nbsp;</td>
                       <td class=tdHeaderAlt>&nbsp;</td>
                       <td class=tdHeaderAlt>&nbsp;</td>
                     </tr>
                     <tr>
                       <td class=tdHeaderAlt>&nbsp;</td>
                       <td class=tdHeaderAlt>Case Information</td>
                       <td class=tdHeaderAlt align="right">&nbsp;</td>
                     </tr>


<%
//if (isScheduler&&NIM3_CaseAccount.getPayerID().intValue()==50)
if (isScheduler2)
{
%>
<%
}
%>



            <%
            if ( (NIM3_CaseAccount.isRequired("CaseClaimNumber",UserSecurityGroupID))&&(!NIM3_CaseAccount.isComplete("CaseClaimNumber")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CaseAccount.isExpired("CaseClaimNumber",expiredDays))&&(NIM3_CaseAccount.isExpiredCheck("CaseClaimNumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_CaseAccount.isWrite("CaseClaimNumber",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=middle>&nbsp;</td>
                     <td valign=middle><p class=<%=theClass%> ><b>Claim Number&nbsp;</b></p></td><td valign=middle><p><input maxlength="100" type=text size="40" name="CaseClaimNumber" id="CaseClaimNumber" value="<%=NIM3_CaseAccount.getCaseClaimNumber()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CaseClaimNumber&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=NIM3_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM3_CaseAccount.getEnglish("CaseClaimNumber")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_CaseAccount.isRead("CaseClaimNumber",UserSecurityGroupID)))
            {
                        %>
                         <tr>
                           <td valign=middle>&nbsp;</td>
                         <td valign=middle><p class=<%=theClass%> ><b>Claim Number&nbsp;</b></p></td><td valign=middle><p><%=NIM3_CaseAccount.getCaseClaimNumber()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CaseClaimNumber&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=NIM3_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM3_CaseAccount.getEnglish("CaseClaimNumber")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>





            <%

    bltNIM3_Referral  NIM3_Referral        =    new    bltNIM3_Referral(iReferralID,UserSecurityGroupID);



if ( (NIM3_Referral.isRequired("Comments",UserSecurityGroupID))&&(!NIM3_Referral.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Referral.isExpired("Comments",expiredDays))&&(NIM3_Referral.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_Referral.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                         <td valign=middle >&nbsp;</td>
                         <td valign=middle ><p class=<%=theClass%> ><b>Referral Notes&nbsp;</b></p>
                         <p class=<%=theClass%> ></p></td><td valign=middle ><p><textarea onKeyDown="textAreaStop(this,2500)" id="tReferral_Comments" rows="8" name="tReferral_Comments" cols="60"><%=NIM3_Referral.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=NIM3_Referral.getReferralID()%>&amp;sFieldNameDisp=<%=NIM3_Referral.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Referral.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                         <td valign=middle >&nbsp;</td>
                       <td valign=middle > <p class=<%=theClass%> ><b><%=NIM3_Referral.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=middle ><p><%=NIM3_Referral.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=NIM3_Referral.getReferralID()%>&amp;sFieldNameDisp=<%=NIM3_Referral.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


<%//End Referral Info%>






<%//start tEncounterService%>


<%


    bltNIM3_Encounter NIM3_Encounter        =    new    bltNIM3_Encounter(iEncounterID,UserSecurityGroupID);


%>

       <%
            if ( (NIM3_Encounter.isRequired("Comments",UserSecurityGroupID))&&(!NIM3_Encounter.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Encounter.isExpired("Comments",expiredDays))&&(NIM3_Encounter.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_Encounter.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                         <td valign=middle>&nbsp;</td>
                       <td valign=middle><p class=<%=theClass%> ><b>ScanPass Comments:&nbsp;</b></p></td><td valign=middle><p><textarea onKeyDown="textAreaStop(this,2000)" rows="3" name="tEncounter_Comments" cols="60"><%=NIM3_Encounter.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_Encounter.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                         <td valign=middle>&nbsp;</td>
                       <td valign=middle> <p class=<%=theClass%> ><b><%=NIM3_Encounter.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=middle><p><%=NIM3_Encounter.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td colspan="3">            
            
<!-- Start -->
<%
    bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID,"","ServiceID");

//declaration of Enumeration
    bltNIM3_Service        NIM3_Service;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Service_List.elements();
    %>

         <table border="0" bordercolor="333333" cellpadding="0"  cellspacing="0" width="100%">        <tr><td>
<br />

        </td></tr>
    <%
    int iExpress=0;
	int iExpressMax = 8;
    while (eList.hasMoreElements()||iExpress<iExpressMax)
    {
       iExpress++;
	   if (iExpress==1||true)
	   {
         %>
            <tr id="enc_more_td1_<%=iExpress%>" style="display:block">
         <%
	   }
	   else 
	   {
         %>
            <tr id="enc_more_td1_<%=iExpress%>" style="display:none">
         <%
	   }
         %>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td  >
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        NIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
      }
      else
      {
        NIM3_Service  = new bltNIM3_Service();
        isNewRecord= true;
      }
        NIM3_Service.GroupSecurityInit(UserSecurityGroupID);
//        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tNIM3_Service")%> #<%=iExpress%></span>
                  </td></tr></table>
            </td></tr>
		<%
	   if (iExpress==1||true)
	   {
         %>
            <tr id="enc_more_td2_<%=iExpress%>" style="display:block">
         <%
	   }
	   else 
	   {
         %>
            <tr id="enc_more_td2_<%=iExpress%>" style="display:none">
         <%
	   }
         %>
                     <td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

  {

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableColor><tr>
          <td>
        <table cellpadding=0 border=0 cellspacing=0 width=100%  >




                     <tr>
                       <td colspan=2 valign=top nowrap>
                       
                        <b>Status&nbsp;2</b>
                        <select   name="ServiceStatusID_<%=iExpress%>"  id="ServiceStatusID_<%=iExpress%>" ><jsp:include page="../generic/tServiceStatusLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Service.getServiceStatusID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ServiceStatusID&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("ServiceStatusID")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                        
&nbsp;&nbsp;&nbsp;&nbsp;                        
                        
<b>Service Type: &nbsp;</b><select disabled="disabled" name="ServiceTypeID_<%=iExpress%>" id="ServiceTypeID_<%=iExpress%>" ><jsp:include page="../generic/tServiceTypeLI.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Service.getServiceTypeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ServiceTypeID&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("ServiceTypeID")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                        
                        </td>
                     </tr>
                     <tr><td colspan=2 valign=top nowrap><p class=<%=theClass%> ><b>CPT*&nbsp;</b>                       
                           <input maxlength="5" type=text size="10" name="CPT_<%=iExpress%>" id="CPT_<%=iExpress%>" value="<%=NIM3_Service.getCPT()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CPT&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("CPT")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                       
<b>CPT Modifier&nbsp;</b> <input maxlength="2" type=text size="10" name="CPTModifier_<%=iExpress%>" value="<%=NIM3_Service.getCPTModifier()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CPTModifier&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("CPTModifier")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                       
                       &nbsp;&nbsp;<b>Qty&nbsp;</b><input maxlength="2" type=text size="4" name="CPTQty_<%=iExpress%>" value="<%=NIM3_Service.getCPTQty()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CPTQty&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("CPTQty")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;&nbsp;  QuickList: <select name=qlCPT class="inputButton_md_Create" style="font-size:11px" onChange="document.getElementById('CPT_<%=iExpress%>').value=this.value;document.getElementById('CPTBodyPart_<%=iExpress%>').value=this[this.selectedIndex].text;">
                     <%=NIMUtils.getCPTOptionList(NIM3_Service.getCPT())%>
                     </select> 
                        </p></td></tr>
                     <tr align="left"><td colspan="2" valign=top><p class=<%=theClass%> ><b>CPT BodyPart*&nbsp;</b>           
                           <input maxlength="100" type=text size="60" name="CPTBodyPart_<%=iExpress%>" id="CPTBodyPart_<%=iExpress%>" value="<%=NIM3_Service.getCPTBodyPart()%>">
                       &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CPTBodyPart&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("CPTBodyPart")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>




                     <tr><td colspan="2" valign=top>
                     
<p class=<%=theClass%> ><b>Diagnosis/Rule-out*&nbsp;</b>
  <input name="CPTText_<%=iExpress%>" type=text value="<%=NIM3_Service.getCPTText()%>" size="80" maxlength="100">
                     &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CPTText&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("CPTText")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>                     
and/or

                     
                     
                     <b>Diag/ICD 1&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT1_<%=iExpress%>" value="<%=NIM3_Service.getdCPT1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT1&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT1")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>

                     <b>2&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT2_<%=iExpress%>" value="<%=NIM3_Service.getdCPT2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT2&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT2")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                     
                     <b>3&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT3_<%=iExpress%>" value="<%=NIM3_Service.getdCPT3()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT3&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT3")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                     
                     <b>4&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT4_<%=iExpress%>" value="<%=NIM3_Service.getdCPT4()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT4&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT4")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                     
                     
                     
                     
                     
                     </p></td></tr>
                     
                     
<tr><td><table width="100%" border="1" cellspacing="1" cellpadding="2">
            <%
            if ( (NIM3_Service.isRequired("DICOM_AccessionNumber",UserSecurityGroupID))&&(!NIM3_Service.isComplete("DICOM_AccessionNumber")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Service.isExpired("DICOM_AccessionNumber",expiredDays))&&(NIM3_Service.isExpiredCheck("DICOM_AccessionNumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Service.isWrite("DICOM_AccessionNumber",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b>DICOM_Accession#<%=NIM3_Service.getEnglish("DICOM_AccessionNumber")%>&nbsp;</b></p></td><td valign=top><p>
                         <input name="DICOM_AccessionNumber_<%=iExpress%>" type="text" onkeydown="textAreaStop(this,200)" value="<%=NIM3_Service.getDICOM_AccessionNumber()%>" size="40" maxlength="200" />
                         &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DICOM_AccessionNumber&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("DICOM_AccessionNumber")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Service.isRead("DICOM_AccessionNumber",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=NIM3_Service.getEnglish("DICOM_AccessionNumber")%>&nbsp;</b></p></td><td valign=top><p><%=NIM3_Service.getDICOM_AccessionNumber()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DICOM_AccessionNumber&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("DICOM_AccessionNumber")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_Service.isRequired("DICOM_ShowImage",UserSecurityGroupID))&&(!NIM3_Service.isComplete("DICOM_ShowImage")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Service.isExpired("DICOM_ShowImage",expiredDays))&&(NIM3_Service.isExpiredCheck("DICOM_ShowImage",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Service.isWrite("DICOM_ShowImage",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>DICOM_ShowImage&nbsp;</b></p></td><td valign=top><p><select   name="DICOM_ShowImage_<%=iExpress%>" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Service.getDICOM_ShowImage()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DICOM_ShowImage&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("DICOM_ShowImage")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Service.isRead("DICOM_ShowImage",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>DICOM_ShowImage&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Service.getDICOM_ShowImage()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DICOM_ShowImage&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("DICOM_ShowImage")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_Service.isRequired("DICOM_MRN",UserSecurityGroupID))&&(!NIM3_Service.isComplete("DICOM_MRN")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Service.isExpired("DICOM_MRN",expiredDays))&&(NIM3_Service.isExpiredCheck("DICOM_MRN",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Service.isWrite("DICOM_MRN",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>DICOM_MRN&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="DICOM_MRN_<%=iExpress%>" value="<%=NIM3_Service.getDICOM_MRN()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DICOM_MRN&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("DICOM_MRN")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Service.isRead("DICOM_MRN",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>DICOM_MRN&nbsp;</b></p></td><td valign=top><p><%=NIM3_Service.getDICOM_MRN()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DICOM_MRN&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("DICOM_MRN")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_Service.isRequired("DICOM_PN",UserSecurityGroupID))&&(!NIM3_Service.isComplete("DICOM_PN")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Service.isExpired("DICOM_PN",expiredDays))&&(NIM3_Service.isExpiredCheck("DICOM_PN",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Service.isWrite("DICOM_PN",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>DICOM_PN&nbsp;</b></p></td><td valign=top><p><input maxlength="90" type=text size="80" name="DICOM_PN_<%=iExpress%>" value="<%=NIM3_Service.getDICOM_PN()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DICOM_PN&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("DICOM_PN")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Service.isRead("DICOM_PN",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>DICOM_PN&nbsp;</b></p></td><td valign=top><p><%=NIM3_Service.getDICOM_PN()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DICOM_PN&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("DICOM_PN")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_Service.isRequired("DICOM_STUDYID",UserSecurityGroupID))&&(!NIM3_Service.isComplete("DICOM_STUDYID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Service.isExpired("DICOM_STUDYID",expiredDays))&&(NIM3_Service.isExpiredCheck("DICOM_STUDYID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Service.isWrite("DICOM_STUDYID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>DICOM_STUDYID&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="DICOM_STUDYID_<%=iExpress%>" value="<%=NIM3_Service.getDICOM_STUDYID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DICOM_STUDYID&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("DICOM_STUDYID")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Service.isRead("DICOM_STUDYID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>DICOM_STUDYID&nbsp;</b></p></td><td valign=top><p><%=NIM3_Service.getDICOM_STUDYID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DICOM_STUDYID&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("DICOM_STUDYID")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


</table>
</td></tr>                     
                     
                     
                     

                     <% if (false) {%><tr><td valign=top><p class=<%=theClass%> ><b>Bill Amount&nbsp;</b>&nbsp;$<input maxlength="10" type=text size="20" name="BillAmount_<%=iExpress%>" value="<%=NIM3_Service.getBillAmount()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("BillAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;&nbsp;<b>Allow Amount&nbsp;</b>&nbsp;$<input maxlength="10" type=text size="20" name="AllowAmount_<%=iExpress%>" value="<%=NIM3_Service.getAllowAmount()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AllowAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("AllowAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;&nbsp;<br><b>Received Amount&nbsp;</b>&nbsp;$<input maxlength="10" type=text size="20" name="ReceivedAmount_<%=iExpress%>" value="<%=NIM3_Service.getReceivedAmount()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReceivedAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("ReceivedAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;&nbsp;<b>Paid-out Amount&nbsp;</b>&nbsp;$<input maxlength="10" type=text size="20" name="PaidOutAmount_<%=iExpress%>" value="<%=NIM3_Service.getPaidOutAmount()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PaidOutAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("PaidOutAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p> </td></tr>
                     
                     
                     
                     
                      <% } %>




        </table>
        </td></tr></table>
        </td></tr>
        <tr><td>&nbsp;</td></tr>
        <%
  }%>

        <%
    }
    %>


<%//end tEncounterService%>

</div>



            </table>
<br>
<input type="button" id="enc_more_button" value="Show More Service Procedures" onClick="<%

//	int iExpressMax = 5;
	iExpress=0;
    while (iExpress<iExpressMax)
    {
		iExpress++;
		%>document.getElementById('enc_more_td1_<%=iExpress%>').style.display = 'block';document.getElementById('enc_more_td2_<%=iExpress%>').style.display = 'block';<%
	}


%>document.getElementById('enc_more_td1_2').style.display = 'block';document.getElementById('enc_more_button').disabled = true;">
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tNIM3_CaseAccount")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tNIM3_CaseAccount")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input  class="inputButton_md_Create" onClick = "MM_validateForm('CaseClaimNumber','','R');if (document.MM_returnValue){this.disabled=true;document.body.scrollTop=1;$('table').fadeOut('slow');show_pws_Saving();submit();return false}else{this.disabled=false;return false}" type=Submit value="Save" name=Submit>  
            <%if (sQUICKVIEW.equalsIgnoreCase("yes")){%><input class="inputButton_md_Default" onClick = "this.disabled=true;window.close();" type=button value="Close" name=Submit2><%}%>  </p>
        <%}%>
        </td></tr></table>
        </form>

        <hr>
        Audit Notes:<br>
        <textarea name="" cols="90" rows="5" readonly><%=NIM3_CaseAccount.getAuditNotes()%></textarea>

        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>
<script language="javascript">
hide_pws_Loading();
</script>
