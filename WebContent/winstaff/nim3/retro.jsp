<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Retro</title>
<link href="//netdna.bootstrapcdn.com/bootswatch/2.3.1/spacelab/bootstrap.min.css" rel="stylesheet"> 

</head>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_report.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0) {
		if (isScheduler)  {
			accessValid = true;
		}
		if (accessValid)	{
		%>
			<div class="container-fluid">
				<div class="well well-small alert-info">
					<h2>NextImageGrid: Retro Worklist</h2>
				</div>
				<div class="row-fluid">
					<table id="worklist" class="table table-bordered table-striped table-hover table-condensed">
						<thead>
							<tr>
								<th>Payer</th>
								<th>Patient</th>
								<th>Client</th>
								<th>Claim</th>
								<th>CPT [Mod]</th>
								<th>Billing TIN</th>
								<th>Billing  Name</th>
								<th>ZIP</th>
								<th>Rendering TIN</th>
								<th>Rendering Name</th>
								<th>ZIP</th>
								<th>Matched?</th>
								<th>NIM TIN</th>
								<th>NIM Name</th>
								<th>ZIP</th>
								<th>Cost</th>
								<th>Payer Allow</th>
								<th>NIM Charge</th>
								<th>NIM Savings</th>
								<th>Profit</th>
								<th>Allows Retro</th>
								<th>Savings</th>
								<th>NIM Accepted</th>
							</tr>
						</thead>
						<tbody>
						<% 
						searchDB2 mySS = new searchDB2();
						java.sql.ResultSet myRS = null;
						try {
							String mySQL = "SELECT * from tnim3_RetroWorklist";
							db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(mySQL);
							myRS = mySS.executePreparedStatement(myDPSO);
							while (myRS!=null&&myRS.next()) {
								bltNIM3_RetroWorklist working_bltNIM3_RetroWorklist = new bltNIM3_RetroWorklist(myRS.getInt("retroworklistid"));
							%>
								<tr>
									<td><%=working_bltNIM3_RetroWorklist.getPayerID()%></td>
									<td><%=working_bltNIM3_RetroWorklist.getPatientLastName()%>, <%=working_bltNIM3_RetroWorklist.getPatientFirstName()%></td>
									<td><%=working_bltNIM3_RetroWorklist.getClientName()%></td>
									<td><%=working_bltNIM3_RetroWorklist.getClientClaimNum()%></td>
									<td><%=working_bltNIM3_RetroWorklist.getCPTCode()%> [<%=working_bltNIM3_RetroWorklist.getModifier1()%>]</td>
									<td><%=working_bltNIM3_RetroWorklist.getBillingProviderTaxID()%></td>
									<td><%=working_bltNIM3_RetroWorklist.getBillingProviderLastName()%></td>
									<td><%=working_bltNIM3_RetroWorklist.getBillingProviderZip()%></td>
									<td><%=working_bltNIM3_RetroWorklist.getRenderingProviderTaxID()%></td>
									<td><%=working_bltNIM3_RetroWorklist.getRenderingProviderLastName()%></td>
									<td><%=working_bltNIM3_RetroWorklist.getRenderingProviderZip()%></td>
									<td><jsp:include page="../generic/tYesNoLILong_translate_bs.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_RetroWorklist.getNIM_ProviderMatchFound()%>" /></jsp:include>							
									</td>
									<td><%=working_bltNIM3_RetroWorklist.getNIM_ProviderTaxID()%></td>
									<td><%=working_bltNIM3_RetroWorklist.getNIM_ProviderLastName()%></td>
									<td><%=working_bltNIM3_RetroWorklist.getNIM_ProviderZip()%></td>
									<td><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency(working_bltNIM3_RetroWorklist.getNIM_ContractProviderCost())%></td>
									<td><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency(working_bltNIM3_RetroWorklist.getPayerAllowance())%></td>
									<td><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency(working_bltNIM3_RetroWorklist.getNetworkAllowance())%></td>
									<td><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency(working_bltNIM3_RetroWorklist.getNetworkSavings())%></td>
									<td><%
										if (working_bltNIM3_RetroWorklist.getNIM_ProviderMatchPositiveSavings()==1){
											out.print(PLCUtils.getDisplayDefaultDecimalFormatCurrency(working_bltNIM3_RetroWorklist.getNetworkAllowance() - working_bltNIM3_RetroWorklist.getNIM_ContractProviderCost()));
										} else {
											out.print("0.00");
										}
										%></td>
									<td><jsp:include page="../generic/tYesNoLILong_translate_bs.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_RetroWorklist.getNIM_ProviderMatchAcceptsRetro()%>" /></jsp:include>							
									</td>
									<td><jsp:include page="../generic/tYesNoLILong_translate_bs.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_RetroWorklist.getNIM_ProviderMatchPositiveSavings()%>" /></jsp:include>							
									</td>
									<td><jsp:include page="../generic/tYesNoLILong_translate_bs.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_RetroWorklist.getNIM_AcceptClaim()%>" /></jsp:include>							
									</td>
								</tr>
							<%
							}
							mySS.closeAll();
						}
						catch(Exception e) {
							mySS.closeAll();
						} finally {
							mySS.closeAll();
						}
						%>
						</tbody>
						
					</table>
				</div>
				
			</div>
		<%
		} else	{
			out.print ("no access");
		}
   }  else  {
		out.print ("no sec");
   }

   %>
</table>
</body>
</html>

<!-- DataTables CSS -->
<link rel="stylesheet" type="text/css" href="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
 
<!-- jQuery -->
<script type="text/javascript" charset="utf8" src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
 
<!-- DataTables -->
<script type="text/javascript" charset="utf8" src="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#worklist').dataTable(	 {
        "sPaginationType": "full_numbers"
    } );
} );
</script>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>