<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*    " %>
<%/*
    filename: tNIM3_Referral_main_NIM3_Referral_CaseID.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />


<table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM3_Referral")%>



<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
      pageControllerHash.put("sParentReturnPage","tNIM3_Referral_main_NIM3_Referral_CaseID.jsp");
    pageControllerHash.remove("iReferralID");
    pageControllerHash.remove("iEncounterID");
    pageControllerHash.put("sINTNext","tNIM3_Referral_main_NIM3_Referral_CaseID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltNIM3_Referral_List        bltNIM3_Referral_List        =    new    bltNIM3_Referral_List(iCaseID,"","ReferralDate");

//declaration of Enumeration
    bltNIM3_Referral        working_bltNIM3_Referral;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Referral_List.elements();
    %>
      <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
        <input class="inputButton_md_Create" type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_create.jsp?EDIT=new&KM=p&INTNext=yes'" value="Add Referral"><br /><br />


     <%}%>
         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_Referral  = (bltNIM3_Referral) leCurrentElement.getObject();
        working_bltNIM3_Referral.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltNIM3_Referral.isComplete())
        {
            theClass = "incompleteItem";
        %>

        <%
        }
        else
        {
        %>
        <%
        }
        %>

        <tr class=<%=theClass%> > 


<%String theClassF = "textBase";%>


<td valign="top" nowrap="nowrap">

            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td colspan="4"><img src="images/marker_referral.gif" align="absmiddle" width="90" height="31" />  <strong><jsp:include page="../generic/tReferralTypeLI_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getReferralTypeID()%>" /></jsp:include></strong><br />
  <jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Referral.getReferralDate())%>" /></jsp:include></span></td>
              </tr>
              <tr>
                <td colspan="4">

<%
if (working_bltNIM3_Referral.getAttendingPhysicianID()==0)
{
%>	
  <input class="inputButton_md_Action1" type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Edit">
<%
}
else
{
%>	
  <input class="inputButton_md_Default" type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=edit&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Edit">
<%
}
%>
        <%if (false&&(CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
      &nbsp;
  <input class="inputButton_md_Stop"  type=button onClick = "confirmDelete2('','tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=del&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p',this)" value="Delete">
<% }%></td>
              </tr>
              <tr>
                <td colspan="4"><%
if (isScheduler&&working_bltNIM3_Referral.getOrderFileID()==0)
{
%>
    <input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=upor&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Upload Order">
  <%
}
else if (working_bltNIM3_Referral.getOrderFileID()==0)
{
%>
    <input class="inputButton_md_Default"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=upor&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Upload Order">
  <%
}
else
{
%>
    <input  class="inputButton_md_Create"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=working_bltNIM3_Referral.getOrderFileID()%>&KM=p';" value="View Order" />&nbsp;&nbsp;
    <input class="inputButton_md_Stop"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=upor&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Replace">
  <%
}
%> </td>
              </tr>
              <tr>
                <td colspan="4"><%
if (working_bltNIM3_Referral.getRxFileID()==0)
{
%>
    <input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=uprx&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Upload Rx">
  <%
}
else
{
%>
    <input  class="inputButton_md_Create"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=working_bltNIM3_Referral.getRxFileID()%>&KM=p';" value="View Rx" />&nbsp;&nbsp;
    <input class="inputButton_md_Stop"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=uprx&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Replace">
  <%
}
%> </td>
              </tr>
              <tr>
                <td colspan="4"><%theClassF = "textBase";%>
  <%if ((working_bltNIM3_Referral.isExpired("ReferralTypeID",expiredDays))&&(working_bltNIM3_Referral.isExpiredCheck("ReferralTypeID"))){theClassF = "expiredFieldMain";}%>
  <%if ( (working_bltNIM3_Referral.isRequired("ReferralTypeID"))&&(!working_bltNIM3_Referral.isComplete("ReferralTypeID")) ){theClassF = "requiredFieldMain";}%>
    
  <%theClassF = "textBase";%>
  <%if ((working_bltNIM3_Referral.isExpired("ReferralStatusID",expiredDays))&&(working_bltNIM3_Referral.isExpiredCheck("ReferralStatusID"))){theClassF = "expiredFieldMain";}%>
  <%if ( (working_bltNIM3_Referral.isRequired("ReferralStatusID"))&&(!working_bltNIM3_Referral.isComplete("ReferralStatusID")) ){theClassF = "requiredFieldMain";}%>
   Status:&nbsp;
   <strong>   <jsp:include page="../generic/tReferralStatusLI_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getReferralStatusID()%>" /></jsp:include> </strong></td>
              </tr>
              <tr>
                <td align="right">Referred To Provider:&nbsp;</td>
                <td> <div class=boxDr>     <jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getAttendingPhysicianID()%>" /></jsp:include></div> </td>
                <td align="right">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="right">&nbsp;Referring Provider:&nbsp;</td>
                <td><div class="boxDr">
                  <jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" >
                    <jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getReferringPhysicianID()%>" />                
                    </jsp:include>
                </div></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table>
       </td>


  <td width="75%" align="left" valign="top" ><input class="inputButton_md_Create" type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=openflow&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Encounters"><br />
<table width="100%">
<tr><td>
		<div class=caseBox>
        <table width="100%" border="1" cellpadding="3" cellspacing="0">
        <tr class="tdHeaderAlt">
        <td>DOS</td>
        <td>Service</td>
        <td>Report</td>
        <td>Type</td>
        </tr>
        <%

			int cnt=0;
			
			try
			{
				searchDB2 mySS = new searchDB2();
				
				java.sql.ResultSet myRS = null;;
				String mySQL = ("select * from tNIM3_Encounter where ReferralID = " + working_bltNIM3_Referral.getReferralID()  + " order by dateofservice");
				//out.println(mySQL);
				myRS = mySS.executeStatement(mySQL);
				
				String myMainTable= " ";
				int endCount = 0;
				int cnt2=0;
				while (myRS!=null&&myRS.next())
				{
					cnt++;
					bltNIM3_Encounter working_bltNIM3_Encounter  =  new bltNIM3_Encounter(new Integer(myRS.getString("EncounterID")));
					bltNIM3_Document working_bltNIM3_Document  =  new bltNIM3_Document(working_bltNIM3_Encounter.getReportFileID());
					%>
					<tr>
						<td><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Encounter.getDateOfService())%>" /></jsp:include></td>
						<td>
						
			<table width="100%" border="1" cellpadding="3" cellspacing="0">
			<%
	
				searchDB2 mySS_cpt = new searchDB2();
				
				java.sql.ResultSet myRS_cpt = null;;
				
				try
				{
					String mySQL_cpt = ("select * from tNIM3_Service where EncounterID = " + working_bltNIM3_Encounter.getEncounterID()  + " AND (CPT>'') order by ServiceID");
					//out.println(mySQL);
					myRS_cpt = mySS_cpt.executeStatement(mySQL_cpt);
				}
				catch(Exception e)
				{
					out.println("ResultsSet:"+e);
				}
				
				while (myRS_cpt!=null&&myRS_cpt.next())
				{
	//				cnt++;
					bltNIM3_Service working_bltNIM3_Service  =  new bltNIM3_Service(new Integer(myRS_cpt.getString("ServiceID")));
					%>
					<tr>
						<td colspan="2">CPT: <strong><%=working_bltNIM3_Service.getCPT()%></strong><br />
	ICD: <strong><%=working_bltNIM3_Service.getdCPT1()%></strong> <%=working_bltNIM3_Service.getdCPT2()%> <strong><%=working_bltNIM3_Service.getdCPT3()%></strong> <%=working_bltNIM3_Service.getdCPT4()%><br />
	  <%=NIMUtils.getCPTText(working_bltNIM3_Service.getCPT())%>&nbsp;</td>
						</tr>
					<%
				}
				mySS_cpt.closeAll();
			%>
			</table>
						
						
						
						
						</td>
						<td><%if (working_bltNIM3_Encounter.getReportFileID()>0){%><input  class="inputButton_md_Create"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=working_bltNIM3_Encounter.getReportFileID()%>&KM=p';" value="View Report" /><%}else{%>Not Available<%}%></td>
						<td><%=working_bltNIM3_Document.getDocName()%>&nbsp;</td>
					 </tr>
<tr><td colspan=4><table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <%=NIMUtils.aEncounterStatus_HTML(working_bltNIM3_Encounter.getEncounterID())%>
</td>
  </tr>
</table>
</td></tr>
					<%
				}
				mySS.closeAll();
			}
			catch(Exception e)
			{
				out.println("ResultsSet:"+e);
			}
		%>
        </table>
        <%
			if (cnt==0&&working_bltNIM3_Referral.getRxFileID()>0)
			{
				%>
                	<input class="inputButton_md_Action1" type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=openflow&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Add Encounter">
                <%
			}
		%>
	</div> 
</td></tr></table>



<hr />
			<p class="tdHeader">CommTrack:<br>
			  <a href="tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=addct&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p">Add CommTrack</a> </p>
			<div class="scroll400box">
		    <%
	try
	{
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;
		String mySQL = ("SELECT commtrackid from tnim3_commtrack where referralid = " + working_bltNIM3_Referral.getReferralID() + "order by CommEnd desc");
		//out.println(mySQL);
  
		myRS = mySS.executeStatement(mySQL);
		int i2cnt=0;
		while(myRS!=null&&myRS.next())
		{
			i2cnt++;
			bltNIM3_CommTrack myCT = new bltNIM3_CommTrack (new  Integer(myRS.getString("commtrackid")));
			%>

		<table width="100%"  border="0" cellspacing="0" cellpadding="3" class="certborder">
  <tr class=tdBaseAlt>
    <td>From:</td>
		<td><strong><%=myCT.getMessageName()%></strong> @ <strong><%=myCT.getMessageCompany()%></strong></td>
  </tr>
  <tr bgcolor="#EEEEEE" class=tdBase>
    <td>Time:</td>
    <td><%=displayDateTimeSDF.format(myCT.getCommStart())%> (Dur: <strong><%= Math.round((myCT.getCommEnd().getTime()-myCT.getCommStart().getTime())/1000/60) %></strong> min)</td>
    </tr>
  <tr bgcolor="#FFFFFF">
    <td colspan="2"><%=myCT.getMessageText()%></td>
  </tr>
</table>
<br>
			<%
		}
		mySS.closeAll();
	}
	catch(Exception e)
	{
			out.println("ResultsSet:"+e);
	}
%>










</td>


 </tr><tr><td colspan=2>&nbsp;</td></tr>
        <%
    }//end while
       }//end of if
       else 
       {
           %>
           <tr><td colspan=3><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tNIM3_Referral")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tNIM3_Referral")%>"))
           {
               document.location="tNIM3_Referral_main_NIM3_Referral_CaseID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>