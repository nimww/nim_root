<<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<html>
<head>

<title>NextImage Medical - Secure Login</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">td img {display: block;}</style>
<!--Fireworks CS3 Dreamweaver CS3 target.  Created Sun Jun 08 15:05:38 GMT-0700 (Pacific Daylight Time) 2008-->
<script language="JavaScript1.2" type="text/javascript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_nbGroup(event, grpName) { //v6.0
var i,img,nbArr,args=MM_nbGroup.arguments;
  if (event == "init" && args.length > 2) {
    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
      img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;
      if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();
      nbArr[nbArr.length] = img;
      for (i=4; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
        if (!img.MM_up) img.MM_up = img.src;
        img.src = img.MM_dn = args[i+1];
        nbArr[nbArr.length] = img;
    } }
  } else if (event == "over") {
    document.MM_nbOver = nbArr = new Array();
    for (i=1; i < args.length-1; i+=3) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = (img.MM_dn && args[i+2]) ? args[i+2] : ((args[i+1])?args[i+1] : img.MM_up);
      nbArr[nbArr.length] = img;
    }
  } else if (event == "out" ) {
    for (i=0; i < document.MM_nbOver.length; i++) { img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }
  } else if (event == "down") {
    nbArr = document[grpName];
    if (nbArr) for (i=0; i < nbArr.length; i++) { img=nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }
    document[grpName] = nbArr = new Array();
    for (i=2; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = img.MM_dn = (args[i+1])? args[i+1] : img.MM_up;
      nbArr[nbArr.length] = img;
  } }
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

//-->
</script>
<link href="www-images/style.css" rel="stylesheet" type="text/css" />
</head>
<body bgcolor="#001f3e" onLoad="MM_preloadImages('www-images/images/index_link_home_f2.gif','www-images/images/index_link_home_f3.gif','www-images/images/index_link_network_f2.gif','www-images/images/index_link_network_f3.gif','www-images/images/index_link_service_f2.gif','www-images/images/index_link_service_f3.gif','www-images/images/index_link_technology_f2.gif','www-images/images/index_link_technology_f3.gif','www-images/images/index_link_about_f2.gif','www-images/images/index_link_about_f3.gif','www-images/images/index_link_contact_f2.gif','www-images/images/index_link_contact_f3.gif','www-images/images/index_link_login_f2.gif','www-images/images/index_link_login_f3.gif')">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" height="100%">
<!-- fwtable fwsrc="index1.png" fwpage="Page 1" fwbase="index-nav.jpg" fwstyle="Dreamweaver" fwdocid = "689853260" fwnested="1" -->
  <tr valign="top">
   <td id="left">&nbsp;</td>
   <td width="804" valign="top"><table width="804" height="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="images/index_bg_repeat.gif" bgcolor="#FFFFFF" >
	  <tr>
	   <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="804">
		  <tr>
		   <td><a href="http://www.nextimagemedical.com/home.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_home','www-images/images/index_link_home_f2.gif','www-images/images/index_link_home_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_home','www-images/images/index_link_home_f3.gif',1)"><img name="index_link_home" src="www-images/images/index_link_home.gif" width="88" height="23" border="0" id="index_link_home" alt="" /></a></td>
		   <td><a href="http://www.nextimagemedical.com/network.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_network','www-images/images/index_link_network_f2.gif','www-images/images/index_link_network_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_network','www-images/images/index_link_network_f3.gif',1)"><img name="index_link_network" src="www-images/images/index_link_network.gif" width="191" height="23" border="0" id="index_link_network" alt="" /></a></td>
		   <td><a href="http://www.nextimagemedical.com/service.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_service','www-images/images/index_link_service_f2.gif','www-images/images/index_link_service_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_service','www-images/images/index_link_service_f3.gif',1)"><img name="index_link_service" src="www-images/images/index_link_service.gif" width="105" height="23" border="0" id="index_link_service" alt="" /></a></td>
		   <td><a href="http://www.nextimagemedical.com/technology.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_technology','www-images/images/index_link_technology_f2.gif','www-images/images/index_link_technology_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_technology','www-images/images/index_link_technology_f3.gif',1)"><img name="index_link_technology" src="www-images/images/index_link_technology.gif" width="136" height="23" border="0" id="index_link_technology" alt="" /></a></td>
		   <td><a href="http://www.nextimagemedical.com/about.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_about','www-images/images/index_link_about_f2.gif','www-images/images/index_link_about_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_about','www-images/images/index_link_about_f3.gif',1)"><img name="index_link_about" src="www-images/images/index_link_about.gif" width="90" height="23" border="0" id="index_link_about" alt="" /></a></td>
		   <td><a href="http://www.nextimagemedical.com/contact.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_contact','www-images/images/index_link_contact_f2.gif','www-images/images/index_link_contact_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_contact','www-images/images/index_link_contact_f3.gif',1)"><img name="index_link_contact" src="www-images/images/index_link_contact.gif" width="106" height="23" border="0" id="index_link_contact" alt="" /></a></td>
		   <td><a href="http://www.nextimagemedical.com/login.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_login','www-images/images/index_link_login_f2.gif','www-images/images/index_link_login_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_login','www-images/images/index_link_login_f3.gif',1)"><img name="index_link_login" src="www-images/images/index_link_login.gif" width="88" height="23" border="0" id="index_link_login" alt="" /></a></td>
		  </tr>
		</table></td>
	  </tr>
	  <tr>
	   <td><img name="index_top" src="www-images/images/index_top.jpg" width="804" height="240" border="0" id="index_top" alt="" /></td>
	  </tr>
	  <tr>
	   <td background="images/index_bg_repeat.gif"><table width="100%" border="0" cellspacing="0" cellpadding="15">
         <tr>
           <td bgcolor="#FFFFFF"><%
java.util.Hashtable pageControllerHash = null;
session.setAttribute("pageControllerHash",pageControllerHash);
%>             <table border="0" align="center" cellpadding="0" cellspacing="0">
               <tr>
                 <td colspan="2" align="left" bordercolor="#002F63" bgcolor="#FFFFFF">           
<%
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
	  Integer UserSecurityGroupID = new Integer(2);
String ip = request.getRemoteAddr();
String host = request.getRemoteHost();

    bltNIM3_CommTrack        NIM3_CommTrack        =       new    bltNIM3_CommTrack();
	NIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setExtUserID(new Integer(0));
	NIM3_CommTrack.setComments("On-Line Submit -> Start (never close)\nIP: " + ip + "\nHost: " + host);
	NIM3_CommTrack.commitData();

//fields
        %>
  <p class=xbig>Online Imaging Referral</p>
  <p class=titleSub1>If your existing form already contains this information, you can simply fax this to us at <strong>888-596-8680</strong></p>
  <form name="at_intake" action="of_sub.jsp">
    <input type="hidden" name="commID" value="<%=NIM3_CommTrack.getUniqueID()%>" />
    <table border="1" cellpadding="3" cellspacing="2" class="tdBaseAlt">
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Your Name</strong></td>
        <td bgcolor="#CCCCCC"><input name="MessageName" type=text class="tdHeader" size="20" maxlength="80"></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Company</strong></td>
        <td bgcolor="#CCCCCC"><input name="MessageCompany" type=text class="tdHeader" size="20" maxlength="80"></td>
        </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Phone</strong></td>
        <td bgcolor="#CCCCCC"><input name="MessagePhone" type=text class="tdHeader" size="20" maxlength="50"></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Fax</strong></td>
        <td bgcolor="#CCCCCC"><input name="MessageFax" type=text class="tdHeader" size="20" maxlength="50"></td>
        </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Email</strong></td>
        <td colspan="3" bgcolor="#CCCCCC"><input name="MessageEmail" type=text class="tdHeader" size="20" maxlength="50"></td>
        </tr>
      </table>
    <table border="1" cellpadding="3" cellspacing="2" class="tdBaseAlt">
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong> Adjuster Name<br>      
          <input name="button" type="button" class="titleSub2" id="button" onClick="document.forms.at_intake.PayerPhone.value = document.forms.at_intake.MessagePhone.value; document.forms.at_intake.AdjusterName.value = document.forms.at_intake.MessageName.value; document.forms.at_intake.PayerName.value = document.forms.at_intake.MessageCompany.value; document.forms.at_intake.PayerFax.value = document.forms.at_intake.MessageFax.value;" value="Same As Above">
        </strong></td>
        <td bgcolor="#CCCCCC"><input name="AdjusterName" type=text class="tdHeader" size="20" maxlength="100"></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Insurance <br>
          or Administrative<br>
          Company</strong></td>
        <td bgcolor="#CCCCCC"><input name="PayerName" type=text class="tdHeader" size="20" maxlength="100"></td>
        </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Phone</strong></td>
        <td bgcolor="#CCCCCC"><input name="PayerPhone" type=text class="tdHeader" size="20" maxlength="50"></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Fax</strong></td>
        <td bgcolor="#CCCCCC"><input name="PayerFax" type=text class="tdHeader" size="20" maxlength="50"></td>
        </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Claim #</strong></td>
        <td bgcolor="#CCCCCC"><input name="CaseClaimNumber1" type=text class="tdHeader" size="20" maxlength="100"></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Preauthorization#</strong></td>
        <td bgcolor="#CCCCCC"><input name="PayerPreAuthorizationConfirmation" type=text class="tdHeader" size="20" maxlength="100"></td>
        </tr>
      <tr>
        <td colspan="4" bgcolor="#CCCCCC" class="xbig">Patient Information</td>
        </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>First</strong></td>
        <td bgcolor="#CCCCCC"><input name="PatientFirstName" type="text" class="tdHeader" id="PatientFirstName" size="20" maxlength="100" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Last</strong></td>

        <td bgcolor="#CCCCCC"><input name="PatientLastName" type="text" class="tdHeader" id="PatientLastName" size="20" maxlength="100" /></td>
        </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>SSN</strong></td>
        <td bgcolor="#CCCCCC"><input name="PatientSSN" type="text" class="tdHeader" id="PatientSSN" size="20" maxlength="20" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>DOB</strong></td>
        <td bgcolor="#CCCCCC"><input name="PatientDOB"  type="text" class="tdHeader" id="PatientDOB" size="20" maxlength="10" /></td>
        </tr>
      <tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Address</strong></td>
        <td bgcolor="#CCCCCC"><input name="PatientAddress1" type="text" class="tdHeader" id="PatientAddress1" size="20" maxlength="200" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>City</strong></td>
        <td bgcolor="#CCCCCC"><input name="PatientCity"  type="text" class="tdHeader" id="PatientCity" size="20" maxlength="100" /></td>
        </tr>
      <tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>State</strong></td>
        <td bgcolor="#CCCCCC"><select   name="PatientStateID" id="PatientStateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="0" /></jsp:include></select></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>ZIP</strong></td>
        <td bgcolor="#CCCCCC"><input name="PatientZIP"  type="text" class="tdHeader" id="PatientZIP" size="20" maxlength="20" /></td>
        </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Phone</strong></td>
        <td bgcolor="#CCCCCC"><input name="PatientHomePhone" type="text" class="tdHeader" size="20" maxlength="50" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Mobile</strong></td>
        <td bgcolor="#CCCCCC"><input name="PatientCellPhone" type="text" class="tdHeader" size="20" maxlength="50" /></td>
      </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Employer</strong></td>
        <td bgcolor="#CCCCCC"><input name="EmployerName" type="text" class="tdHeader" id="EmployerName" size="20" maxlength="50" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Employer Phone</strong></td>
        <td bgcolor="#CCCCCC"><input name="EmployerPhone" type="text" class="tdHeader" id="EmployerPhone" size="20" maxlength="50" /></td>
      </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Date of Injury</strong></td>
        <td bgcolor="#CCCCCC"><input name="DateOfInjury" type="text" class="tdHeader" id="DateOfInjury" size="20" maxlength="50" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2">&nbsp;</td>
        <td bgcolor="#CCCCCC">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Ordering Physician<br>
          Full Name</strong></td>
        <td bgcolor="#CCCCCC"><input name="ReferringPhysicianName" type=text class="tdHeader" size="20" maxlength="100"></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Phone</strong></td>
        <td bgcolor="#CCCCCC"><input name="ReferringPhysicianPhone" type=text class="tdHeader" size="20" maxlength="100"></td>
      </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong> Procedure &amp; Body Part:</strong></td>
        <td colspan="3" bgcolor="#CCCCCC"><textarea name="CPTText" cols="60" rows="4" class="titleSub2" onKeyDown="textAreaStop(this,500)"></textarea></td>
        </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Additional information:</strong></td>
        <td colspan="3" bgcolor="#CCCCCC"><textarea name="MessageText" cols="70" rows="10" class="titleSub2" onKeyDown="textAreaStop(this,1900)"><%=NIM3_CommTrack.getMessageText()%></textarea></td>
        </tr>
      <tr>
        <td colspan="4" bgcolor="#CCCCCC" class="tdHeaderBright"><label>
          <input name="button2" type="submit" class="xbig" id="button2" value="Submit" />
          </label></td>
        </tr>
      </table>
    </form>
           

&nbsp;</td>
               </tr>
               </table></td>
         </tr>
       </table></td>
	  </tr>
	  <tr>
	   <td bgcolor="#FFFFFF">&nbsp;</td>
	  </tr>
	  <tr>
	    <td height="100%" valign="bottom"><table width="100%" border="0" cellspacing="0" cellpadding="15">
          <tr>
            <td bgcolor="#FFFFFF">copyright &copy; 2009 NextImage Medical, Inc. </td>
          </tr>
        </table></td>
	    </tr>
	</table></td>
   <td id="right">&nbsp;</td>
  </tr>
</table>
</body>
</html>
