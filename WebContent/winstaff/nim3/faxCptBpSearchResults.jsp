<%@page language="java" contentType="application/json; charset=UTF-8" import="java.io.*,com.winstaff.*, java.util.*, java.sql.*,com.google.gson.Gson;"%>
<%!class uaResult {
		String value;
		String id;

		public uaResult(String value, String id) {
			this.value = value;
			this.id = id;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}
	}%>
<%
	List<uaResult> uaResultList = new ArrayList<uaResult>();

	if (request.getParameter("term") != null && !request.getParameter("term").isEmpty()) {
		String search = request.getParameter("term");

		String query = "select cpt, description||' ('||cpt||')' from tcptli where cpt = '"+search+"'";
		searchDB2 db = new searchDB2();

		ResultSet rs = db.executeStatement(query);

		try {
			while (rs.next()) {
				uaResultList.add(new uaResult(rs.getString(2), rs.getString(1)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closeAll();
		}
	}
	response.setHeader("Access-Control-Allow-Origin", "*");
	
	out.println(new Gson().toJson(uaResultList));
%>
