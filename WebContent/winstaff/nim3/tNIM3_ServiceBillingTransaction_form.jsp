<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.*, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltNIM3_ServiceBillingTransaction" %>
<%/*

    filename: out\jsp\tNIM3_ServiceBillingTransaction_form.jsp
    Created on Mar/09/2011
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_1/style.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<table cellpadding=0 cellspacing=0 border=0 >
    <tr><td width=10>&nbsp;</td><td>



<%
//initial declaration of list class and parentID
    Integer        iServiceBillingTransactionID        =    null;
    Integer        iEDITID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
		accessValid = true;
  //page security
  if (accessValid&&isScheduler2)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
		if ( request.getParameter( "EDITID" ) != null )
		{
			iEDITID        =    new Integer(request.getParameter ("EDITID"));
		}
		else
		{
			iEDITID        =    new Integer(0);
		}

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_ServiceBillingTransaction_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID

    bltNIM3_ServiceBillingTransaction        NIM3_ServiceBillingTransaction        =    null;

	if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_ServiceBillingTransaction        =    new    bltNIM3_ServiceBillingTransaction(UserSecurityGroupID, true);
    }

//fields
        %>
        <form action="tNIM3_ServiceBillingTransaction_form_sub.jsp" name="tNIM3_ServiceBillingTransaction_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
    %>
        <input type="hidden" name="EDITID" value = "<%=iEDITID%>" >  

          <%  String theClass ="tdBase";%>
        <table border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
        
        <tr>
          <td class="titleSub1">Add Billing Transaction for Service ID: <%=iEDITID%></td></tr>
        
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0>
            <%
            if ( (NIM3_ServiceBillingTransaction.isRequired("ServiceBillingTransactionTypeID",UserSecurityGroupID))&&(!NIM3_ServiceBillingTransaction.isComplete("ServiceBillingTransactionTypeID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_ServiceBillingTransaction.isExpired("ServiceBillingTransactionTypeID",expiredDays))&&(NIM3_ServiceBillingTransaction.isExpiredCheck("ServiceBillingTransactionTypeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
<%
            if ((NIM3_ServiceBillingTransaction.isWrite("ServiceBillingTransactionTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ServiceBillingTransaction Type ID&nbsp;</b></p></td><td valign=top><p><select   name="ServiceBillingTransactionTypeID"  id="ServiceBillingTransactionTypeID" ><jsp:include page="../generic/tServiceBillingTransactionTypeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ServiceBillingTransactionTypeID&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("ServiceBillingTransactionTypeID")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_ServiceBillingTransaction.isRead("ServiceBillingTransactionTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ServiceBillingTransaction Type ID&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tServiceBillingTransactionTypeLI_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ServiceBillingTransactionTypeID&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("ServiceBillingTransactionTypeID")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_ServiceBillingTransaction.isRequired("ExportedDate",UserSecurityGroupID))&&(!NIM3_ServiceBillingTransaction.isComplete("ExportedDate")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_ServiceBillingTransaction.isExpired("ExportedDate",expiredDays))&&(NIM3_ServiceBillingTransaction.isExpiredCheck("ExportedDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_ServiceBillingTransaction.isWrite("ExportedDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ExportedDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="20" name="ExportedDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_ServiceBillingTransaction.getExportedDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExportedDate&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("ExportedDate")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_ServiceBillingTransaction.isRead("ExportedDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ExportedDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_ServiceBillingTransaction.getExportedDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExportedDate&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("ExportedDate")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_ServiceBillingTransaction.isRequired("TransactionTitle",UserSecurityGroupID))&&(!NIM3_ServiceBillingTransaction.isComplete("TransactionTitle")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_ServiceBillingTransaction.isExpired("TransactionTitle",expiredDays))&&(NIM3_ServiceBillingTransaction.isExpiredCheck("TransactionTitle",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_ServiceBillingTransaction.isWrite("TransactionTitle",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>TransactionTitle&nbsp;</b></p></td><td valign=top><p><input name="TransactionTitle" type=text id="TransactionTitle" value="<%=NIM3_ServiceBillingTransaction.getTransactionTitle()%>" size="50" maxlength="90">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TransactionTitle&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("TransactionTitle")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_ServiceBillingTransaction.isRead("TransactionTitle",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>TransactionTitle&nbsp;</b></p></td><td valign=top><p><%=NIM3_ServiceBillingTransaction.getTransactionTitle()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TransactionTitle&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("TransactionTitle")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_ServiceBillingTransaction.isRequired("TransactionNote",UserSecurityGroupID))&&(!NIM3_ServiceBillingTransaction.isComplete("TransactionNote")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_ServiceBillingTransaction.isExpired("TransactionNote",expiredDays))&&(NIM3_ServiceBillingTransaction.isExpiredCheck("TransactionNote",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_ServiceBillingTransaction.isWrite("TransactionNote",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>TransactionNote&nbsp;</b></p></td><td valign=top><p><input maxlength="90" type=text size="50" name="TransactionNote" value="<%=NIM3_ServiceBillingTransaction.getTransactionNote()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TransactionNote&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("TransactionNote")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_ServiceBillingTransaction.isRead("TransactionNote",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>TransactionNote&nbsp;</b></p></td><td valign=top><p><%=NIM3_ServiceBillingTransaction.getTransactionNote()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TransactionNote&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("TransactionNote")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_ServiceBillingTransaction.isRequired("TransactionAmount",UserSecurityGroupID))&&(!NIM3_ServiceBillingTransaction.isComplete("TransactionAmount")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_ServiceBillingTransaction.isExpired("TransactionAmount",expiredDays))&&(NIM3_ServiceBillingTransaction.isExpiredCheck("TransactionAmount",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
  <%
            if ((NIM3_ServiceBillingTransaction.isWrite("TransactionAmount",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>TransactionAmount:&nbsp;</b></p></td><td valign=top><p><input name="TransactionAmount"  type=text id="TransactionAmount" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_ServiceBillingTransaction.getTransactionAmount())%>" size="10" maxlength=10>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TransactionAmount&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("TransactionAmount")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_ServiceBillingTransaction.isRead("TransactionAmount",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>TransactionAmount&nbsp;</b></p></td><td valign=top><p><%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_ServiceBillingTransaction.getTransactionAmount())%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TransactionAmount&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("TransactionAmount")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_ServiceBillingTransaction.isRequired("ReferenceNumber",UserSecurityGroupID))&&(!NIM3_ServiceBillingTransaction.isComplete("ReferenceNumber")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_ServiceBillingTransaction.isExpired("ReferenceNumber",expiredDays))&&(NIM3_ServiceBillingTransaction.isExpiredCheck("ReferenceNumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_ServiceBillingTransaction.isWrite("ReferenceNumber",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ReferenceNumber&nbsp;</b></p></td><td valign=top><p><input maxlength="60" type=text size="20" name="ReferenceNumber" value="<%=NIM3_ServiceBillingTransaction.getReferenceNumber()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferenceNumber&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("ReferenceNumber")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_ServiceBillingTransaction.isRead("ReferenceNumber",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ReferenceNumber&nbsp;</b></p></td><td valign=top><p><%=NIM3_ServiceBillingTransaction.getReferenceNumber()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferenceNumber&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("ReferenceNumber")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_ServiceBillingTransaction.isRequired("ReferenceDate",UserSecurityGroupID))&&(!NIM3_ServiceBillingTransaction.isComplete("ReferenceDate")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_ServiceBillingTransaction.isExpired("ReferenceDate",expiredDays))&&(NIM3_ServiceBillingTransaction.isExpiredCheck("ReferenceDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_ServiceBillingTransaction.isWrite("ReferenceDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ReferenceDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="20" name="ReferenceDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_ServiceBillingTransaction.getReferenceDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferenceDate&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("ReferenceDate")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_ServiceBillingTransaction.isRead("ReferenceDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>ReferenceDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_ServiceBillingTransaction.getReferenceDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferenceDate&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("ReferenceDate")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_ServiceBillingTransaction.isRequired("ReferenceNotes",UserSecurityGroupID))&&(!NIM3_ServiceBillingTransaction.isComplete("ReferenceNotes")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_ServiceBillingTransaction.isExpired("ReferenceNotes",expiredDays))&&(NIM3_ServiceBillingTransaction.isExpiredCheck("ReferenceNotes",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_ServiceBillingTransaction.isWrite("ReferenceNotes",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=NIM3_ServiceBillingTransaction.getEnglish("ReferenceNotes")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="ReferenceNotes" cols="40" maxlength=200><%=NIM3_ServiceBillingTransaction.getReferenceNotes()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferenceNotes&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("ReferenceNotes")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                <%
            }
            else if (false&&(NIM3_ServiceBillingTransaction.isRead("ReferenceNotes",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=NIM3_ServiceBillingTransaction.getEnglish("ReferenceNotes")%>&nbsp;</b></p></td><td valign=top><p><%=NIM3_ServiceBillingTransaction.getReferenceNotes()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferenceNotes&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("ReferenceNotes")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_ServiceBillingTransaction.isRequired("Detail_Item_Qty",UserSecurityGroupID))&&(!NIM3_ServiceBillingTransaction.isComplete("Detail_Item_Qty")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_ServiceBillingTransaction.isExpired("Detail_Item_Qty",expiredDays))&&(NIM3_ServiceBillingTransaction.isExpiredCheck("Detail_Item_Qty",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_ServiceBillingTransaction.isWrite("Detail_Item_Qty",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Detail_Item_Qty&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="Detail_Item_Qty" value="<%=NIM3_ServiceBillingTransaction.getDetail_Item_Qty()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Detail_Item_Qty&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("Detail_Item_Qty")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                <%
            }
            else if (false&&(NIM3_ServiceBillingTransaction.isRead("Detail_Item_Qty",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Detail_Item_Qty&nbsp;</b></p></td><td valign=top><p><%=NIM3_ServiceBillingTransaction.getDetail_Item_Qty()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Detail_Item_Qty&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("Detail_Item_Qty")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_ServiceBillingTransaction.isRequired("Generated_UserID",UserSecurityGroupID))&&(!NIM3_ServiceBillingTransaction.isComplete("Generated_UserID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_ServiceBillingTransaction.isExpired("Generated_UserID",expiredDays))&&(NIM3_ServiceBillingTransaction.isExpiredCheck("Generated_UserID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_ServiceBillingTransaction.isWrite("Generated_UserID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Generated_UserID&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="Generated_UserID" value="<%=NIM3_ServiceBillingTransaction.getGenerated_UserID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Generated_UserID&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("Generated_UserID")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_ServiceBillingTransaction.isRead("Generated_UserID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Generated_UserID&nbsp;</b></p></td><td valign=top><p><%=NIM3_ServiceBillingTransaction.getGenerated_UserID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Generated_UserID&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("Generated_UserID")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_ServiceBillingTransaction.isRequired("Comments",UserSecurityGroupID))&&(!NIM3_ServiceBillingTransaction.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_ServiceBillingTransaction.isExpired("Comments",expiredDays))&&(NIM3_ServiceBillingTransaction.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_ServiceBillingTransaction.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=NIM3_ServiceBillingTransaction.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=NIM3_ServiceBillingTransaction.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                <%
            }
            else if (false&&(NIM3_ServiceBillingTransaction.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=NIM3_ServiceBillingTransaction.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=NIM3_ServiceBillingTransaction.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_ServiceBillingTransaction&amp;sRefID=<%=NIM3_ServiceBillingTransaction.getServiceBillingTransactionID()%>&amp;sFieldNameDisp=<%=NIM3_ServiceBillingTransaction.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_ServiceBillingTransaction','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tNIM3_ServiceBillingTransaction")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tNIM3_ServiceBillingTransaction")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input name=Add type=Button id="Add" onClick = "if (confirm('Once a transaction is written to AR/AP it can\'t be undone.\n\nAny changes would need to be added as additional adjustments.\n\nOK to Continue\nor Cancel')){if (document.getElementById('ServiceBillingTransactionTypeID').value!=0&&document.getElementById('TransactionTitle').value>''&&document.getElementById('TransactionAmount').value!=0){submit();}else{alert('Please complete the form');}}" value="Add Record" <%=HTMLFormStyleButton%>></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



