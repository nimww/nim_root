<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*,java.util.List, java.util.Arrays" %>
<%/*
    filename: out\jsp\tNIM3_Encounter_form_sub.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
        if (iEncounterID.intValue() != iEDITID.intValue())
        {
			out.print("Error - Collision.  Do you have multiple windows open?");
        	accessValid = false;
        }
    }
  //page security
  if (accessValid)
  {
	  
/*	  
	  //quick debug write out
		java.util.Enumeration en = request.getParameterNames();
        
		com.winstaff.DebugLogger.printLine("----------------- Debugging tNIM3_EncounterService_form_sub.jsp -----START---------");
        while (en.hasMoreElements()) 
		{
            String paramName = (String) en.nextElement();
            com.winstaff.DebugLogger.printLine("[" + paramName + "] = [" + request.getParameter(paramName) + "]");
        }	  
		com.winstaff.DebugLogger.printLine("----------------- Debugging tNIM3_EncounterService_form_sub.jsp -----END---------");
*/	  
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltNIM3_Encounter        NIM3_Encounter        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("nim3track")||request.getParameter( "EDIT" ).equalsIgnoreCase("nim3expbill")||request.getParameter( "EDIT" ).equalsIgnoreCase("nim3expbill2") )
    {
        NIM3_Encounter        =    new    bltNIM3_Encounter(iEncounterID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_Encounter        =    new    bltNIM3_Encounter(UserSecurityGroupID,true);
    }

      String tempAN =	"";//NIM3_Encounter.getAuditNotes();
	  String tempAN_Status = "";
String Enc_testChangeID = "0";
java.util.Date EarliestCheckIn = null;
java.util.Date EarliestCheckOut = null;
NIM3_EncounterObject2 myEO2 = null;
myEO2 = new NIM3_EncounterObject2(NIM3_Encounter.getEncounterID(),"Sync Billing on Save");
try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !NIM3_Encounter.getUniqueCreateDate().equals(testObj)  )
    {
         NIM3_Encounter.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !NIM3_Encounter.getUniqueModifyDate().equals(testObj)  )
    {
         NIM3_Encounter.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !NIM3_Encounter.getUniqueModifyComments().equals(testObj)  )
    {
         NIM3_Encounter.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReferralID")) ;
    if ( !NIM3_Encounter.getReferralID().equals(testObj)  )
    {
         NIM3_Encounter.setReferralID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferralID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("EncounterTypeID")) ;
    if ( !NIM3_Encounter.getEncounterTypeID().equals(testObj)  )
    {
         NIM3_Encounter.setEncounterTypeID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter EncounterTypeID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ScanPass")) ;
    if ( !NIM3_Encounter.getScanPass().equals(testObj)  )
    {
         NIM3_Encounter.setScanPass( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ScanPass not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AppointmentID")) ;
    if ( !NIM3_Encounter.getAppointmentID().equals(testObj)  )
    {
         NIM3_Encounter.setAppointmentID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter AppointmentID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AttendingPhysicianID")) ;
    if ( !NIM3_Encounter.getAttendingPhysicianID().equals(testObj)  )
    {
         NIM3_Encounter.setAttendingPhysicianID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter AttendingPhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReferringPhysicianID")) ;
    if ( !NIM3_Encounter.getReferringPhysicianID().equals(testObj)  )
    {
         NIM3_Encounter.setReferringPhysicianID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateOfService"))) ;
    if ( !NIM3_Encounter.getDateOfService().equals(testObj)  )
    {
         NIM3_Encounter.setDateOfService( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter DateOfService not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("LODID")) ;
    if ( !NIM3_Encounter.getLODID().equals(testObj)  )
    {
         NIM3_Encounter.setLODID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter LODID not set. this is ok-not an error");
}



try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("NextActionDate"))) ;
    if ( !NIM3_Encounter.getNextActionDate().equals(testObj)  )
    {
         NIM3_Encounter.setNextActionDate( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter NextActionDate not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("NextActionTaskID")) ;
    if ( !NIM3_Encounter.getNextActionTaskID().equals(testObj)  )
    {
         NIM3_Encounter.setNextActionTaskID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter NextActionTaskID not set. this is ok-not an error");
}



try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_DataProc_RefDr"))) ;
    if ( !NIM3_Encounter.getSentTo_DataProc_RefDr().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_DataProc_RefDr( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_DataProc_RefDr not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_DataProc_Adj"))) ;
    if ( !NIM3_Encounter.getSentTo_DataProc_Adj().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_DataProc_Adj( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_DataProc_Adj not set. this is ok-not an error");
}




try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentToRefDr"))) ;
    if ( !NIM3_Encounter.getSentToRefDr().equals(testObj)  )
    {
         NIM3_Encounter.setSentToRefDr( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentToRefDr not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentToAdj"))) ;
    if ( !NIM3_Encounter.getSentToAdj().equals(testObj)  )
    {
         NIM3_Encounter.setSentToAdj( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentToAdj not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentToIC"))) ;
    if ( !NIM3_Encounter.getSentToIC().equals(testObj)  )
    {
         NIM3_Encounter.setSentToIC( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentToIC not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_SP_RefDr"))) ;
    if ( !NIM3_Encounter.getSentTo_SP_RefDr().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_SP_RefDr( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_SP_RefDr not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_SP_Adj"))) ;
    if ( !NIM3_Encounter.getSentTo_SP_Adj().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_SP_Adj( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_SP_Adj not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_SP_IC"))) ;
    if ( !NIM3_Encounter.getSentTo_SP_IC().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_SP_IC( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_SP_IC not set. this is ok-not an error");
}


try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_Bill_Pay"))) ;
    if ( !NIM3_Encounter.getSentTo_Bill_Pay().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_Bill_Pay( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_Bill_Pay not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("Rec_Bill_Pro"))) ;
    if ( !NIM3_Encounter.getRec_Bill_Pro().equals(testObj)  )
    {
         NIM3_Encounter.setRec_Bill_Pro( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter Rec_Bill_Pro not set. this is ok-not an error");
}



try
{
    Integer testObj = new Integer(request.getParameter("ReportFileID")) ;
    if ( !NIM3_Encounter.getReportFileID().equals(testObj)  )
    {
         NIM3_Encounter.setReportFileID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReportFileID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DICOMFileID")) ;
    if ( !NIM3_Encounter.getDICOMFileID().equals(testObj)  )
    {
         NIM3_Encounter.setDICOMFileID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter DICOMFileID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !NIM3_Encounter.getComments().equals(testObj)  )
    {
         NIM3_Encounter.setComments( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter Comments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AuditNotes")) ;
    if ( !NIM3_Encounter.getAuditNotes().equals(testObj)  )
    {
         NIM3_Encounter.setAuditNotes( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter AuditNotes not set. this is ok-not an error");
}
try
{
    Integer testObj = new Integer(request.getParameter("EncounterStatusID")) ;
    if ( !NIM3_Encounter.getEncounterStatusID().equals(testObj)  )
    {
         NIM3_Encounter.setEncounterStatusID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter EncounterStatusID not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqRxReview_RCodeID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqRxReview_RCodeID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqRxReview_RCodeID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqRxReview_RCodeID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TimeTrack_ReqRxReview"))) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqRxReview().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqRxReview( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqRxReview not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqRpReview_RCodeID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqRpReview_RCodeID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqRpReview_RCodeID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqRpReview_RCodeID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TimeTrack_ReqRpReview"))) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqRpReview().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqRpReview( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqRpReview not set. this is ok-not an error");
}





try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqRxReview_UserID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqRxReview_UserID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqRxReview_UserID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqRxReview_UserID not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("PaidToProviderCheck1Number")) ;
    if ( !NIM3_Encounter.getPaidToProviderCheck1Number().equals(testObj)  )
    {
         NIM3_Encounter.setPaidToProviderCheck1Number( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter PaidToProviderCheck1Number not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("PaidToProviderCheck1Amount"))) ;
    if ( !NIM3_Encounter.getPaidToProviderCheck1Amount().equals(testObj)  )
    {
         NIM3_Encounter.setPaidToProviderCheck1Amount( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter PaidToProviderCheck1Amount not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("PaidToProviderCheck1Date"))) ;
    if ( !NIM3_Encounter.getPaidToProviderCheck1Date().equals(testObj)  )
    {
         NIM3_Encounter.setPaidToProviderCheck1Date( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter PaidToProviderCheck1Date not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PaidToProviderCheck2Number")) ;
    if ( !NIM3_Encounter.getPaidToProviderCheck2Number().equals(testObj)  )
    {
         NIM3_Encounter.setPaidToProviderCheck2Number( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter PaidToProviderCheck2Number not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("PaidToProviderCheck2Amount"))) ;
    if ( !NIM3_Encounter.getPaidToProviderCheck2Amount().equals(testObj)  )
    {
         NIM3_Encounter.setPaidToProviderCheck2Amount( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter PaidToProviderCheck2Amount not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("PaidToProviderCheck2Date"))) ;
    if ( !NIM3_Encounter.getPaidToProviderCheck2Date().equals(testObj)  )
    {
         NIM3_Encounter.setPaidToProviderCheck2Date( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter PaidToProviderCheck2Date not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PaidToProviderCheck3Number")) ;
    if ( !NIM3_Encounter.getPaidToProviderCheck3Number().equals(testObj)  )
    {
         NIM3_Encounter.setPaidToProviderCheck3Number( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter PaidToProviderCheck3Number not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("PaidToProviderCheck3Amount"))) ;
    if ( !NIM3_Encounter.getPaidToProviderCheck3Amount().equals(testObj)  )
    {
         NIM3_Encounter.setPaidToProviderCheck3Amount( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter PaidToProviderCheck3Amount not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("PaidToProviderCheck3Date"))) ;
    if ( !NIM3_Encounter.getPaidToProviderCheck3Date().equals(testObj)  )
    {
         NIM3_Encounter.setPaidToProviderCheck3Date( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter PaidToProviderCheck3Date not set. this is ok-not an error");
}


try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_ReqRec_RefDr"))) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_RefDr().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_RefDr( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_RefDr not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_ReqRec_RefDr_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_RefDr_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_RefDr_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_RefDr_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_ReqRec_Adj"))) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_Adj().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_Adj( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_Adj not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_ReqRec_Adj_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_Adj_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_Adj_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_Adj_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TimeTrack_ReqRec"))) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqRec().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqRec( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqRec not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqRec_UserID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqRec_UserID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqRec_UserID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqRec_UserID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TimeTrack_ReqCreated"))) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqCreated().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqCreated( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqCreated not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqCreated_UserID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqCreated_UserID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqCreated_UserID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqCreated_UserID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TimeTrack_ReqProc"))) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqProc().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqProc( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqProc not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqProc_UserID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqProc_UserID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqProc_UserID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqProc_UserID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TimeTrack_ReqSched"))) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqSched().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqSched( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqSched not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqSched_UserID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqSched_UserID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqSched_UserID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqSched_UserID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TimeTrack_ReqDelivered"))) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqDelivered().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqDelivered( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqDelivered not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqDelivered_UserID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqDelivered_UserID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqDelivered_UserID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqDelivered_UserID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TimeTrack_ReqPaidIn"))) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqPaidIn().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqPaidIn( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqPaidIn not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqPaidIn_UserID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqPaidIn_UserID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqPaidIn_UserID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqPaidIn_UserID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TimeTrack_ReqPaidOut"))) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqPaidOut().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqPaidOut( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqPaidOut not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqPaidOut_UserID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqPaidOut_UserID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqPaidOut_UserID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqPaidOut_UserID not set. this is ok-not an error");
}

//order matters here


try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("Rec_Bill_Pay"))) ;
    if ( !NIM3_Encounter.getRec_Bill_Pay().equals(testObj)  )
    {
         NIM3_Encounter.setRec_Bill_Pay( testObj,UserSecurityGroupID   );
		 //test timetrack
		 if (NIM3_Encounter.getTimeTrack_ReqPaidIn().before(new java.util.Date(1)) )
		 {
			 NIM3_Encounter.setTimeTrack_ReqPaidIn(testObj);
			 NIM3_Encounter.setTimeTrack_ReqPaidIn_UserID(CurrentUserAccount.getUserID());
		 }
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter Rec_Bill_Pay not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_Bill_Pro"))) ;
    if ( !NIM3_Encounter.getSentTo_Bill_Pro().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_Bill_Pro( testObj,UserSecurityGroupID   );
		 //test timetrack
		 if (NIM3_Encounter.getTimeTrack_ReqPaidOut().before(new java.util.Date(1)) )
		 {
			 NIM3_Encounter.setTimeTrack_ReqPaidOut(testObj,UserSecurityGroupID);
			 NIM3_Encounter.setTimeTrack_ReqPaidOut_UserID(CurrentUserAccount.getUserID());
		 }
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_Bill_Pro not set. this is ok-not an error");
}





try
{
    Integer testObj = new Integer(request.getParameter("ProviderInvoiceID")) ;
    if ( !NIM3_Encounter.getProviderInvoiceID().equals(testObj)  )
    {
         NIM3_Encounter.setProviderInvoiceID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ProviderInvoiceID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqRec_RCodeID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqRec_RCodeID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqRec_RCodeID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqRec_RCodeID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqCreated_RCodeID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqCreated_RCodeID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqCreated_RCodeID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqCreated_RCodeID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqProc_RCodeID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqProc_RCodeID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqProc_RCodeID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqProc_RCodeID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqSched_RCodeID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqSched_RCodeID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqSched_RCodeID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqSched_RCodeID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqDelivered_RCodeID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqDelivered_RCodeID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqDelivered_RCodeID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqDelivered_RCodeID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqPaidIn_RCodeID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqPaidIn_RCodeID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqPaidIn_RCodeID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqPaidIn_RCodeID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqPaidOut_RCodeID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqPaidOut_RCodeID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqPaidOut_RCodeID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqPaidOut_RCodeID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqApproved_RCodeID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqApproved_RCodeID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqApproved_RCodeID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqApproved_RCodeID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TimeTrack_ReqApproved"))) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqApproved().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqApproved( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqApproved not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqApproved_UserID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqApproved_UserID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqApproved_UserID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqApproved_UserID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_ReqRec_Adm"))) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_Adm().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_Adm( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_Adm not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_ReqRec_Adm_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_Adm_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_Adm_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_Adm_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_SP_Adm"))) ;
    if ( !NIM3_Encounter.getSentTo_SP_Adm().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_SP_Adm( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_SP_Adm not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_SP_Adm_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_SP_Adm_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_SP_Adm_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_SP_Adm_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentToAdm"))) ;
    if ( !NIM3_Encounter.getSentToAdm().equals(testObj)  )
    {
         NIM3_Encounter.setSentToAdm( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentToAdm not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentToAdm_CRID")) ;
    if ( !NIM3_Encounter.getSentToAdm_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentToAdm_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentToAdm_CRID not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("isSTAT")) ;
    if ( !NIM3_Encounter.getisSTAT().equals(testObj)  )
    {
         NIM3_Encounter.setisSTAT( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter isSTAT not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("RequiresFilms")) ;
    if ( !NIM3_Encounter.getRequiresFilms().equals(testObj)  )
    {
         NIM3_Encounter.setRequiresFilms( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter RequiresFilms not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("HasBeenRescheduled")) ;
    if ( !NIM3_Encounter.getHasBeenRescheduled().equals(testObj)  )
    {
         NIM3_Encounter.setHasBeenRescheduled( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter HasBeenRescheduled not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_SP_Adm2"))) ;
    if ( !NIM3_Encounter.getSentTo_SP_Adm2().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_SP_Adm2( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_SP_Adm2 not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_SP_Adm2_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_SP_Adm2_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_SP_Adm2_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_SP_Adm2_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_RP_Adm2"))) ;
    if ( !NIM3_Encounter.getSentTo_RP_Adm2().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_RP_Adm2( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_RP_Adm2 not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_RP_Adm2_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_RP_Adm2_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_RP_Adm2_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_RP_Adm2_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_ReqRec_Adm2"))) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_Adm2().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_Adm2( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_Adm2 not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_ReqRec_Adm2_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_Adm2_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_Adm2_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_Adm2_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_SP_Adm3"))) ;
    if ( !NIM3_Encounter.getSentTo_SP_Adm3().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_SP_Adm3( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_SP_Adm3 not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_SP_Adm3_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_SP_Adm3_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_SP_Adm3_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_SP_Adm3_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_RP_Adm3"))) ;
    if ( !NIM3_Encounter.getSentTo_RP_Adm3().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_RP_Adm3( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_RP_Adm3 not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_RP_Adm3_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_RP_Adm3_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_RP_Adm3_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_RP_Adm3_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_ReqRec_Adm3"))) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_Adm3().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_Adm3( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_Adm3 not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_ReqRec_Adm3_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_Adm3_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_Adm3_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_Adm3_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_SP_Adm4"))) ;
    if ( !NIM3_Encounter.getSentTo_SP_Adm4().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_SP_Adm4( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_SP_Adm4 not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_SP_Adm4_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_SP_Adm4_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_SP_Adm4_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_SP_Adm4_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_RP_Adm4"))) ;
    if ( !NIM3_Encounter.getSentTo_RP_Adm4().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_RP_Adm4( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_RP_Adm4 not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_RP_Adm4_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_RP_Adm4_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_RP_Adm4_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_RP_Adm4_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_ReqRec_Adm4"))) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_Adm4().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_Adm4( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_Adm4 not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_ReqRec_Adm4_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_Adm4_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_Adm4_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_Adm4_CRID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqInitialAppointment_RCodeID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqInitialAppointment_RCodeID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqInitialAppointment_RCodeID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqInitialAppointment_RCodeID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("TimeTrack_ReqInitialAppointment"))) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqInitialAppointment().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqInitialAppointment( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqInitialAppointment not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("TimeTrack_ReqInitialAppointment_UserID")) ;
    if ( !NIM3_Encounter.getTimeTrack_ReqInitialAppointment_UserID().equals(testObj)  )
    {
         NIM3_Encounter.setTimeTrack_ReqInitialAppointment_UserID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter TimeTrack_ReqInitialAppointment_UserID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_SP_NCM"))) ;
    if ( !NIM3_Encounter.getSentTo_SP_NCM().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_SP_NCM( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_SP_NCM not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_SP_NCM_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_SP_NCM_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_SP_NCM_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_SP_NCM_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_RP_NCM"))) ;
    if ( !NIM3_Encounter.getSentTo_RP_NCM().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_RP_NCM( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_RP_NCM not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_RP_NCM_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_RP_NCM_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_RP_NCM_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_RP_NCM_CRID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("SentTo_ReqRec_NCM"))) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_NCM().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_NCM( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_NCM not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("SentTo_ReqRec_NCM_CRID")) ;
    if ( !NIM3_Encounter.getSentTo_ReqRec_NCM_CRID().equals(testObj)  )
    {
         NIM3_Encounter.setSentTo_ReqRec_NCM_CRID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SentTo_ReqRec_NCM_CRID not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("AmountBilledByProvider"))) ;
    if ( !NIM3_Encounter.getAmountBilledByProvider().equals(testObj)  )
    {
         NIM3_Encounter.setAmountBilledByProvider( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter AmountBilledByProvider not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("Pricing_Structure")) ;
    if ( !NIM3_Encounter.getPricing_Structure().equals(testObj)  )
    {
         NIM3_Encounter.setPricing_Structure( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter Pricing_Structure not set. this is ok-not an error");
}





















List<String> pidl = new java.util.ArrayList<String>();
String query = "select practiceid\n" +
		"from tpracticemaster\n" +
		"order by practiceid desc";
searchDB2 ss = new searchDB2();
java.sql.ResultSet rs = ss.executeStatement(query);
while(rs.next()){
	pidl.add(rs.getString("practiceid"));
}
ss.closeAll();
//out.println("My PID list:\t"+pidl);


try
{
    Integer testObj = new Integer(request.getParameter("SeeNetDev_SelectedPracticeID")) ;
    if ( !NIM3_Encounter.getSeeNetDev_SelectedPracticeID().equals(testObj) && pidl.contains(testObj.toString()))
    {
         NIM3_Encounter.setSeeNetDev_SelectedPracticeID( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter SeeNetDev_SelectedPracticeID not set. this is ok-not an error");
}



























//start 


    bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID, "","ServiceID");

//declaration of Enumeration
    bltNIM3_Service        NIM3_Service;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Service_List.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tNIM3_Service"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        NIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
      }
      else
      {
        NIM3_Service  = new bltNIM3_Service();
        NIM3_Service.setEncounterID(iEncounterID);
        isNewRecord= true;
      }
        NIM3_Service.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !NIM3_Service.getUniqueCreateDate().equals(testObj)  )
    {
         NIM3_Service.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !NIM3_Service.getUniqueModifyDate().equals(testObj)  )
    {
         NIM3_Service.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !NIM3_Service.getUniqueModifyComments().equals(testObj)  )
    {
         NIM3_Service.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("EncounterID_"+iExpress)) ;
    if ( !NIM3_Service.getEncounterID().equals(testObj)  )
    {
         NIM3_Service.setEncounterID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service EncounterID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ServiceTypeID_"+iExpress)) ;
    if ( !NIM3_Service.getServiceTypeID().equals(testObj)  )
    {
         NIM3_Service.setServiceTypeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ServiceTypeID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateOfService_"+iExpress))) ;
    if ( !NIM3_Service.getDateOfService().equals(testObj)  )
    {
         NIM3_Service.setDateOfService( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service DateOfService not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AttendingPhysicianID_"+iExpress)) ;
    if ( !NIM3_Service.getAttendingPhysicianID().equals(testObj)  )
    {
         NIM3_Service.setAttendingPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service AttendingPhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReferringPhysicianID_"+iExpress)) ;

    if ( !NIM3_Service.getReferringPhysicianID().equals(testObj)  )
    {
         NIM3_Service.setReferringPhysicianID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReferringPhysicianID not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("ServiceStatusID_"+iExpress)) ;
    if ( !NIM3_Service.getServiceStatusID().equals(testObj)  )
    {
         NIM3_Service.setServiceStatusID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ServiceStatusID not set. this is ok-not an error");
}




try
{
    String testObj = new String(request.getParameter("CPT_"+iExpress)) ;
    if ( !NIM3_Service.getCPT().equals(testObj)  )
    {
         NIM3_Service.setCPT( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service CPT not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CPTModifier_"+iExpress)) ;
    if ( !NIM3_Service.getCPTModifier().equals(testObj)  )
    {
         NIM3_Service.setCPTModifier( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service CPTModifier not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CPTBodyPart_"+iExpress)) ;
    if ( !NIM3_Service.getCPTBodyPart().equals(testObj)  )
    {
         NIM3_Service.setCPTBodyPart( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service CPTBodyPart not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("dCPT1_"+iExpress)) ;
    if ( !NIM3_Service.getdCPT1().equals(testObj)  )
    {
         NIM3_Service.setdCPT1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service dCPT1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("dCPT2_"+iExpress)) ;
    if ( !NIM3_Service.getdCPT2().equals(testObj)  )
    {
         NIM3_Service.setdCPT2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service dCPT2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("dCPT3_"+iExpress)) ;
    if ( !NIM3_Service.getdCPT3().equals(testObj)  )
    {
         NIM3_Service.setdCPT3( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service dCPT3 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("dCPT4_"+iExpress)) ;
    if ( !NIM3_Service.getdCPT4().equals(testObj)  )
    {
         NIM3_Service.setdCPT4( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service dCPT4 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CPTText_"+iExpress)) ;
    if ( !NIM3_Service.getCPTText().equals(testObj)  )
    {
         NIM3_Service.setCPTText( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service CPTText not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CPTWizardID_"+iExpress)) ;
    if ( !NIM3_Service.getCPTWizardID().equals(testObj)  )
    {
         NIM3_Service.setCPTWizardID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service CPTWizardID not set. this is ok-not an error");
}



try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("BillAmount_"+iExpress))) ;
    if ( !NIM3_Service.getBillAmount().equals(testObj)  )
    {
         NIM3_Service.setBillAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service BillAmount not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("AllowAmount_"+iExpress))) ;
    if ( !NIM3_Service.getAllowAmount().equals(testObj)  )
    {
    	Double tempFS_Calc = NIM3_Service.getCPTQty() *  NIMUtils.getFeeSchedulePrice(1,1.0,myEO2.getAppointment_PracticeMaster().getOfficeZIP(),NIM3_Service.getCPT(),NIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService());
    	
    	List<Integer> approvedList = Arrays.asList(19852,21204,18538,18545,35141,35142,27124,35167,145,32477,1056);
    	String UserLogonDescription = "n/a";
    	if (pageControllerHash.containsKey("UserLogonDescription"))
    	{
    	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
    	}
    	if(PoUtils.arrayContains(UserLogonDescription, approvedList) && testObj>tempFS_Calc){
    		NIM3_Service.setAllowAmount( testObj,UserSecurityGroupID );
    	} else if(PoUtils.arrayContains(UserLogonDescription, approvedList) && testObj<tempFS_Calc){
    		NIM3_Service.setAllowAmount( testObj,UserSecurityGroupID );
    	} else{
    		NIM3_Service.setAllowAmount( 0d,UserSecurityGroupID );
    	}
    	
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service AllowAmount not set. this is ok-not an error");
}

try
{
	Double testObj = null ;
    if ( !NIM3_Service.getPaidOutAmount().equals(testObj)  )
    {
        testObj = (PLCUtils.getDecimalDefault(request.getParameter("PaidOutAmount_"+iExpress))) ;
		Double tempFS_Calc = NIM3_Service.getCPTQty() *  NIMUtils.getFeeSchedulePrice(1,1.0,myEO2.getAppointment_PracticeMaster().getOfficeZIP(),NIM3_Service.getCPT(),NIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService());
    	
    	List<Integer> approvedList = Arrays.asList(19852,21204,18538,18545,35141,35142,27124,35167,31494,145,32477,1056);//35141,35142 THIS IS JENNY, JENNY AS NET DEV AND AS NID-JENNY USER ACCOUNT
    	String UserLogonDescription = "n/a";
    	if (pageControllerHash.containsKey("UserLogonDescription"))
    	{
    	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
    	}
    	if(PoUtils.arrayContains(UserLogonDescription, approvedList) && testObj>tempFS_Calc){
    		NIM3_Service.setPaidOutAmount( testObj,UserSecurityGroupID   );
    	} else if(testObj<=tempFS_Calc){
    		NIM3_Service.setPaidOutAmount( testObj,UserSecurityGroupID );
    	} else{
    		NIM3_Service.setPaidOutAmount( 0d,UserSecurityGroupID );
    	}
    	
        testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service PaidOutAmount not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReceivedCheck1Number_"+iExpress)) ;
    if ( !NIM3_Service.getReceivedCheck1Number().equals(testObj)  )
    {
         NIM3_Service.setReceivedCheck1Number( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReceivedCheck1Number not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("ReceivedCheck1Amount_"+iExpress))) ;
    if ( !NIM3_Service.getReceivedCheck1Amount().equals(testObj)  )
    {
         NIM3_Service.setReceivedCheck1Amount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReceivedCheck1Amount not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ReceivedCheck1Date_"+iExpress))) ;
    if ( !NIM3_Service.getReceivedCheck1Date().equals(testObj)  )
    {
         NIM3_Service.setReceivedCheck1Date( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReceivedCheck1Date not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("ReceivedCheck2Number_"+iExpress)) ;
    if ( !NIM3_Service.getReceivedCheck2Number().equals(testObj)  )
    {
         NIM3_Service.setReceivedCheck2Number( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReceivedCheck2Number not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("ReceivedCheck2Amount_"+iExpress))) ;
    if ( !NIM3_Service.getReceivedCheck2Amount().equals(testObj)  )
    {
         NIM3_Service.setReceivedCheck2Amount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReceivedCheck2Amount not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ReceivedCheck2Date_"+iExpress))) ;
    if ( !NIM3_Service.getReceivedCheck2Date().equals(testObj)  )
    {
         NIM3_Service.setReceivedCheck2Date( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReceivedCheck2Date not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReceivedCheck3Number_"+iExpress)) ;
    if ( !NIM3_Service.getReceivedCheck3Number().equals(testObj)  )
    {
         NIM3_Service.setReceivedCheck3Number( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReceivedCheck3Number not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("ReceivedCheck3Amount_"+iExpress))) ;
    if ( !NIM3_Service.getReceivedCheck3Amount().equals(testObj)  )
    {
         NIM3_Service.setReceivedCheck3Amount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReceivedCheck3Amount not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ReceivedCheck3Date_"+iExpress))) ;
    if ( !NIM3_Service.getReceivedCheck3Date().equals(testObj)  )
    {
         NIM3_Service.setReceivedCheck3Date( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }
}
catch(Exception e)
{
     //out.println("NIM3_Service ReceivedCheck3Date not set. this is ok-not an error");
}
if (testChangeID.equalsIgnoreCase("1"))
{
         NIM3_Service.setReceivedAmount( NIM3_Service.getReceivedCheck1Amount() + NIM3_Service.getReceivedCheck2Amount() + NIM3_Service.getReceivedCheck3Amount(),UserSecurityGroupID   );
}


try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("AllowAmountAdjustment_"+iExpress))) ;
    if ( !NIM3_Service.getAllowAmountAdjustment().equals(testObj)  )
    {
         NIM3_Service.setAllowAmountAdjustment( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service AllowAmountAdjustment not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("ReportFileID_"+iExpress)) ;
    if ( !NIM3_Service.getReportFileID().equals(testObj)  )
    {
         NIM3_Service.setReportFileID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReportFileID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !NIM3_Service.getComments().equals(testObj)  )
    {
         NIM3_Service.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service Comments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AuditNotes_"+iExpress)) ;
    if ( !NIM3_Service.getAuditNotes().equals(testObj)  )
    {
         NIM3_Service.setAuditNotes( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service AuditNotes not set. this is ok-not an error");
}


try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Reference_ProviderContractAmount_"+iExpress))) ;
    if ( !NIM3_Service.getReference_ProviderContractAmount().equals(testObj)  )
    {
         NIM3_Service.setReference_ProviderContractAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service Reference_ProviderContractAmount not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Reference_ProviderBilledAmount_"+iExpress))) ;
    if ( !NIM3_Service.getReference_ProviderBilledAmount().equals(testObj)  )
    {
         NIM3_Service.setReference_ProviderBilledAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service Reference_ProviderBilledAmount not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Reference_FeeScheduleAmount_"+iExpress))) ;
    if ( !NIM3_Service.getReference_FeeScheduleAmount().equals(testObj)  )
    {
         NIM3_Service.setReference_FeeScheduleAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service Reference_FeeScheduleAmount not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Reference_UCAmount_"+iExpress))) ;
    if ( !NIM3_Service.getReference_UCAmount().equals(testObj)  )
    {
         NIM3_Service.setReference_UCAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service Reference_UCAmount not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Reference_PayerBillAmount_"+iExpress))) ;
    if ( !NIM3_Service.getReference_PayerBillAmount().equals(testObj)  )
    {
         NIM3_Service.setReference_PayerBillAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service Reference_PayerBillAmount not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Reference_PayerContractAmount_"+iExpress))) ;
    if ( !NIM3_Service.getReference_PayerContractAmount().equals(testObj)  )
    {
         NIM3_Service.setReference_PayerContractAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service Reference_PayerContractAmount not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingNotes_"+iExpress)) ;
    if ( !NIM3_Service.getBillingNotes().equals(testObj)  )
    {
         NIM3_Service.setBillingNotes( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service BillingNotes not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("InvoiceNotes_Payer_"+iExpress)) ;
    if ( !NIM3_Service.getInvoiceNotes_Payer().equals(testObj)  )
    {
         NIM3_Service.setInvoiceNotes_Payer( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service InvoiceNotes_Payer not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("InvoiceNotes_Provider_"+iExpress)) ;
    if ( !NIM3_Service.getInvoiceNotes_Provider().equals(testObj)  )
    {
         NIM3_Service.setInvoiceNotes_Provider( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service InvoiceNotes_Provider not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CPTQty_Bill_"+iExpress)) ;
    if ( !NIM3_Service.getCPTQty_Bill().equals(testObj)  )
    {
         NIM3_Service.setCPTQty_Bill( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service CPTQty_Bill not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CPTQty_Pay_"+iExpress)) ;
    if ( !NIM3_Service.getCPTQty_Pay().equals(testObj)  )
    {
         NIM3_Service.setCPTQty_Pay( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service CPTQty_Pay not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("CPTQty_"+iExpress)) ;
    if ( !NIM3_Service.getCPTQty().equals(testObj)  )
    {
         NIM3_Service.setCPTQty( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service CPTQty not set. this is ok-not an error");
}





boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tNIM3_Service","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    NIM3_Service.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
	        NIM3_Service.commitData();
	        if (!NIM3_Service.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }
    }
}       }



}//while




//end 

boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tNIM3_Encounter","next");
	                sRefreshDoc = "";
	            }




// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (Enc_testChangeID.equalsIgnoreCase("1"))
{

	NIM3_Encounter.setUniqueModifyDate(PLCUtils.getNowDate(false));
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    NIM3_Encounter.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
			tempAN +="\n[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nRecord Edit";
			/* NIM3_Encounter.setAuditNotes(tempAN); */
	        NIM3_Encounter.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }
}

	//start calc Billing...
if (request.getParameter( "EDIT" ).equalsIgnoreCase("nim3track") )
{
}
else
{
	try
	{
		%>
		<table align="center" border="1"  bordercolor="#000000" cellspacing="0" cellpadding="5">
	  <tr>
		<td>Calculation Summary:</td>
	  </tr>
	  <tr>
		<td><%out.print(NIMUtils.ReCalcServiceBilling(NIM3_Encounter.getEncounterID()));%></td>
	  </tr>
	  <tr>
		<td>Calculation Summary [V2]:</td>
	  </tr>
	  <tr>
		<td><%out.print(NIMUtils.ReCalcServiceBilling2(NIM3_Encounter.getEncounterID()));%></td>
	  </tr>
	  <tr>
		<td><%
//		out.print("Synchronization Currently Disabled");
		out.print("Synchronization Start");
		
		myEO2.synchronizeBilling_One_to_Two();
		out.print("<br>Synchronization Complete");
		%></td>
	  </tr>
	</table>
	
		<%
		
	}
	catch(Exception eee)
	{
	}




//test B:IN done
java.util.Date myCompleteDatePayIn = NIMUtils.isPayInComplete(NIM3_Encounter.getEncounterID());
if (myCompleteDatePayIn!=null&&NIM3_Encounter.getTimeTrack_ReqPaidIn().before(new java.util.Date(1)))
{
	out.print("**Payment In: Complete and Matched - Date Received Set to: [" +  myCompleteDatePayIn + "]");
	NIM3_Encounter.setTimeTrack_ReqPaidIn(myCompleteDatePayIn);
	NIM3_Encounter.setTimeTrack_ReqPaidIn_UserID(CurrentUserAccount.getUserID());
//	NIM3_Encounter.commitData();
	Enc_testChangeID = "2";
}
else
{
	out.print("Pay In not complete");
}
											

//test B:OUT done
java.util.Date myCompleteDatePayOut = NIMUtils.isPayOutComplete(NIM3_Encounter.getEncounterID());
if (myCompleteDatePayOut!=null&&NIM3_Encounter.getTimeTrack_ReqPaidOut().before(new java.util.Date(1)))
{
	out.print("Payment Out: Complete and Matched - Date Paid Set to: [" +  myCompleteDatePayOut + "]");
	NIM3_Encounter.setTimeTrack_ReqPaidOut(myCompleteDatePayOut);
	NIM3_Encounter.setTimeTrack_ReqPaidOut_UserID(CurrentUserAccount.getUserID());
	Enc_testChangeID = "2";
}
else
{
	out.print("Pay Out not complete");
}

if (Enc_testChangeID.equalsIgnoreCase("2"))
{
	NIM3_Encounter.commitData();
}


}//end calc billing


String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("nim3expbill2") )
	{
	%><script language=Javascript>
	      </script><hr><input value="Continue" name="closeme" onClick="document.location='tNIM3_ServiceBilling.jsp?EDIT=nim3expbill2&EDITID=<%=iEncounterID%>';" type="button" />
    <%
    //response.sendRedirect(nextPage+"?EDIT=edit");
	}
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("nim3expbill") )
	{
	%><script language=Javascript>
	      </script><hr><input value="Continue" name="closeme" onClick="document.location='tNIM3_Encounter_billing.jsp?EDIT=nim3expbill&EDITID=<%=iEncounterID%>';" type="button" />
    <%
    //response.sendRedirect(nextPage+"?EDIT=edit");
	}
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("nim3track") )
	{
	%><script language=Javascript>
	      </script><hr><input value="Continue" name="closeme" onClick="document.location='tNIM3_Encounter_track.jsp?EDIT=nim3track&EDITID=<%=iEncounterID%>';" type="button" />
    <%
    //response.sendRedirect(nextPage+"?EDIT=edit");
	}
    else 
	{
	%><script language=Javascript>
	      </script><hr><input value="Close" name="closeme" onClick="window.close();" type="button" />
    <%
    //response.sendRedirect(nextPage+"?EDIT=edit");
	}
}
else
{
	out.print("no route");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
