<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    filename: tnim2_Authorization_main_NIM_Authorization_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>
<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<base target=_self>
<%
//initial declaration of list class and parentID
    Integer        iPayerID        =    null;
    Integer        iCaseID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPayerID")&&pageControllerHash.containsKey("iCaseID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
//bltNIM3_CaseAccount NCA = new bltNIM3_CaseAccount(iCaseID);
%>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p class=title>Modality Wizard<br>
<%
try
{
	
	



}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>

    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><label>
      </label>
      <table width="100%" border="1" cellspacing="5" cellpadding="5">
        <tr>
          <td>MRI</td>
          <td><input type="button" name="button" id="button" value="MRI W/ Contrast" onclick="document.location='LI_ModalityWizard_f2-mri.jsp?mt=mriw';" /></td>
          <td><input type="button" name="button2" id="button2" value="MRI W/O Contrast" onclick="document.location='LI_ModalityWizard_f2-mri.jsp?mt=mriwo';" /></td>
          <td><input type="button" name="button3" id="button3" value="MRI W &amp; W/O Contrast" onclick="document.location='LI_ModalityWizard_f2-mri.jsp?mt=mriwwo';" /></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>MRA</td>
          <td><input type="button" name="button4" id="button4" value="MRA" onclick="document.location='LI_ModalityWizard_f2-mri.jsp';" /></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>CT</td>
          <td><input type="button" name="button" id="button" value="CT W/ Contrast" onclick="document.location='LI_ModalityWizard_f2-mri.jsp';" /></td>
          <td><input type="button" name="button2" id="button2" value="CT W/O Contrast" onclick="document.location='LI_ModalityWizard_f2-mri.jsp';" /></td>
          <td><input type="button" name="button3" id="button3" value="CT W &amp; W/O Contrast" onclick="document.location='LI_ModalityWizard_f2-mri.jsp';" /></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Other</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <label><br />
    </label></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div id=modtype></div>&nbsp;</td>
  </tr>
</table>

<%



  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_ClearID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
