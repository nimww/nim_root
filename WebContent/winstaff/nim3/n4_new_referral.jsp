<%@page import="java.io.*,com.winstaff.*, java.util.*, java.text.SimpleDateFormat,com.google.gson.Gson;"%>
<%!class incomingService {	
	public String cpt = "";
	public String bp = "";
	public String qty = "";
	public String cptModifier = "";
	public String ro = "";
	public String diag1 = "";
	public String diag2 = "";
	public String diag3 = "";
	public String diag4 = "";
	public incomingService(String cpt, String bp, String qty,String cptModifier,String ro , String diag1, String diag2, String diag3, String diag4) {
		this.cpt = cpt;
		this.bp = bp;
		this.qty = qty;
		this.cptModifier = cptModifier;
		this.ro = ro;
		this.diag1 = diag1;
		this.diag2 = diag2;
		this.diag3 = diag3;
		this.diag4 = diag4;
	}
	public String getCpt() {
		return cpt;
	}
	public void setCpt(String cpt) {
		this.cpt = cpt;
	}
	public String getBp() {
		return bp;
	}
	public void setBp(String bp) {
		this.bp = bp;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getCptModifier() {
		return cptModifier;
	}
	public void setCptModifiert(String cptModifier) {
		this.cptModifier = cptModifier;
	}
	public String getRo() {
		return ro;
	}
	public void setRo(String ro) {
		this.ro = ro;
	}
	public void setDiag1(String diag1) {
		this.diag1 = diag1;
	}
	public String getDiag1() {
		return diag1;
	}
	public String getDiag2() {
		return diag2;
	}
	public void setDiag2(String diag2) {
		this.diag2 = diag2;
	}
	public String getDiag3() {
		return diag3;
	}
	public void setDiag3(String diag3) {
		this.diag3 = diag3;
	}
	public String getDiag4() {
		return diag4;
	}
	public void setDiag4(String diag4) {
		this.diag4 = diag4;
	}
}


/* class sendUserAlertEmail{
	public void email(String emailTo, String emailFrom, String theSubject, String theBody) {
		bltEmailTransaction et = new bltEmailTransaction();
		et.setEmailTo(emailTo);
		et.setEmailFrom(emailFrom);
		et.setEmailSubject(theSubject);
		et.setEmailBody(theBody);
		et.setEmailBodyType(emailType_V3.HTML_TYPE);
		et.setTransactionDate(new java.util.Date());
		et.commitData();
	} 
	 
}*/
	%>
	
	<%
	
	 String schedulerfullname = "";
	 String caseClaimNumber = "";
	 String NIM3_LINK_ADJUSTER = "";
	 String NIM3_adjusterID="";
	 String user_othernotification1="";
	 String user_schedulenotification1="";
	 String user_reportnotification1="";
	 String user_first1 = "";
	 String user_last1 = "";
	 String user_1address1 = "";
	 String user_2address1 = "";
	 String user_city1 = "";
	 String user_State1 = "";
	 String user_zip1 = "";
	 String user_email1 = "";
	 String user_phone1 = "";
	 String user_fax1 = "";
	 String user_linkUser1 = "";
	 String user_Type1 = "";
	 String PAYER_NOTES = "";
	
	 String NIM3_schedulerID="";
	
	 String NIM3_ncmID="";
	 String user_othernotification2="";
	 String user_schedulenotification2="";
	 String user_reportnotification2="";
	 String user_first2 = "";
	 String user_last2 = "";
	 String user_1address2 = "";
	 String user_2address2 = "";
	 String user_city2 = "";
	 String user_State2= "";
	 String user_zip2 = "";
	 String user_email2 = "";
	 String user_phone2 = "";
	 String user_fax2 = "";
	 String user_linkUser2 = "";
	 String user_Type2 = "";
	 
	 String user_othernotification3="";
	 String user_schedulenotification3="";
	 String user_reportnotification3="";
	 String user_first3 = "";
	 String user_last3 = "";
	 String user_1address3 = "";
	 String user_2address3 = "";
	 String user_city3 = "";
	 String user_State3="";
	 String user_zip3 = "";
	 String user_email3 = "";
	 String user_phone3 = "";
	 String user_fax3 = "";
	 String user_linkUser3 = "";
	 String user_Type3 = "";
	 String NIM3_ADMIN_1_ID = "";
	 String NIM3_ADMIN_2_ID = "";
	 String NIM3_ADMIN_3_ID = "";
	 String NIM3_ADMIN_4_ID = "";
	
	 String casenotes = "";
	
	 String NIM3_mdID="";
	 String user_othernotification4="";
	 String user_schedulenotification4="";
	 String user_reportnotification4="";
	 String user_first4 = "";
	 String user_last4 = "";
	 String user_1address4 = "";
	 String user_2address4 = "";
	 String user_city4 = "";
	 String user_State4= "";
	 String user_zip4 = "";
	 String user_email4 = "";
	 String user_phone4 = "";
	 String user_fax4 = "";
	 String user_linkUser4 = "";
	 String user_Type4 = "";
	
	 String NIM3_referral_source = "";
	 String REFERRAL_STATUS = "";
	 String REFERRAL_METHOD = "";
	 String ENCOUNTER_TYPE = "";
	 String REFERRAL_SOURCE = "";
	 String REFERRAL_NOTES = "";
	
	 String patientfirst = "";
	 String patientlast = "";
	 String patientaddress = "";
	 String patientaddress2 = "";
	 String patientcity = "";
	 String patientstate = "";
	 String patientzip = "";
	 String patientemail = "";
	 String patientphone = "";
	 String patientcellphone = "";
	 String patientdob = "";
	 String patientssn = "";
	
	 String patientavailability = "";
	
	 String employername = "";
	 String employerfax = "";
	 String employerphone = "";
	 
	 String attorneyfirstname = "";
	 String attorneylastname = "";
	 String attorneyphone = "";
	 String attorneyfax = "";
	 String INJURY_DESCRIPTION = "";
	
	 String claustrophobic = "";
	 String height = "";
	 String weight = "";
	 String gender = "";
	 String doi = "";
	 String onmeds = "";
	 String physicaltherapy = "";
	 String ptdesc = "";
	 String onmedsdescreption = "";
	 String metalinbody = "";
	 String metalinbodydescreption = "";
	 String previousimage = "";
	 String previousimagedescreption = "";
	 String pregnant = "";
	 String pregnantdescreption = "";
	 String requirehandcarrycd = "";
	 String requiresfilms = "";
	 String requiresageinjury = "";
	 String isstat = "";
	 String isretro = "";
	 List<incomingService> incomingServiceList = new ArrayList<incomingService>();
	 
	 boolean isGood = true;
	 try{
		 user_othernotification1 = request.getParameter("user_othernotification1");
		 user_schedulenotification1 = request.getParameter("user_schedulenotification1");
		 user_reportnotification1 = request.getParameter("user_reportnotification1"); 
		 caseClaimNumber = request.getParameter("caseClaimNumber");
		 NIM3_LINK_ADJUSTER = request.getParameter("NIM3_LINK_ADJUSTER");
		 NIM3_adjusterID = request.getParameter("NIM3_adjusterID");
		 user_first1 = request.getParameter("user_first1");
		 user_last1 = request.getParameter("user_last1");
		 user_1address1 = request.getParameter("user_1address1");
		 user_2address1 = request.getParameter("user_2address1");
		 user_city1 = request.getParameter("user_city1");
		 user_State1 = request.getParameter("user_State1");
		 user_zip1 = request.getParameter("user_zip1");
		 user_email1 = request.getParameter("user_email1");
		 user_phone1 = request.getParameter("user_phone1");
		 user_fax1 = request.getParameter("user_fax1");
		 user_linkUser1 = request.getParameter("user_linkUser1");
		 user_Type1 = request.getParameter("user_Type1");
		 PAYER_NOTES = request.getParameter("PAYER_NOTES");
		 
		 NIM3_schedulerID = request.getParameter("NIM3_schedulerID");
		 
		 user_othernotification2 = request.getParameter("user_othernotification2");
		 user_schedulenotification2 = request.getParameter("user_schedulenotification2");
		 user_reportnotification2 = request.getParameter("user_reportnotification2"); 
		 NIM3_ncmID = request.getParameter("NIM3_ncmID");
		 user_first2 = request.getParameter("user_first2");
		 user_last2 = request.getParameter("user_last2");
		 user_1address2 = request.getParameter("user_1address2");
		 user_2address2 = request.getParameter("user_2address2");
		 user_city2 = request.getParameter("user_city2");
		 user_State2 = request.getParameter("user_State2");
		 user_zip2 = request.getParameter("user_zip2");
		 user_email2 = request.getParameter("user_email2");
		 user_phone2 = request.getParameter("user_phone2");
		 user_fax2 = request.getParameter("user_fax2");
		 user_linkUser2 = request.getParameter("user_linkUser2");
		 user_Type2 = request.getParameter("user_Type2");
		 
		 user_othernotification3 = request.getParameter("user_othernotification3");
		 user_schedulenotification3 = request.getParameter("user_schedulenotification3");
		 user_reportnotification3 = request.getParameter("user_reportnotification3"); 
		 user_first3 = request.getParameter("user_first3");
		 user_last3 = request.getParameter("user_last3");
		 user_1address3 = request.getParameter("user_1address3");
		 user_2address3 = request.getParameter("user_2address3");
		 user_city3 = request.getParameter("user_city3");
		 user_State3 = request.getParameter("user_State3");
		 user_zip3 = request.getParameter("user_zip3");
		 user_email3 = request.getParameter("user_email3");
		 user_phone3 = request.getParameter("user_phone3");
		 user_fax3 = request.getParameter("user_fax3");
		 user_linkUser3 = request.getParameter("user_linkUser3");
		 user_Type3 = request.getParameter("user_Type3");
		 NIM3_ADMIN_1_ID = request.getParameter("NIM3_ADMIN_1_ID");
		 NIM3_ADMIN_2_ID = request.getParameter("NIM3_ADMIN_2_ID");
		 NIM3_ADMIN_3_ID = request.getParameter("NIM3_ADMIN_3_ID");
		 NIM3_ADMIN_4_ID = request.getParameter("NIM3_ADMIN_4_ID");
		 
		 casenotes = request.getParameter("casenotes");
		 
		 user_othernotification4 = request.getParameter("user_othernotification4");
		 user_schedulenotification4 = request.getParameter("user_schedulenotification4");
		 user_reportnotification4 = request.getParameter("user_reportnotification4"); 
		 NIM3_mdID = request.getParameter("NIM3_mdID");
		 user_first4 = request.getParameter("user_first4");
		 user_last4 = request.getParameter("user_last4");
		 user_1address4 = request.getParameter("user_1address4");
		 user_2address4 = request.getParameter("user_2address4");
		 user_city4 = request.getParameter("user_city4");
		 user_State4 = request.getParameter("user_State4");
		 user_zip4 = request.getParameter("user_zip4");
		 user_email4 = request.getParameter("user_email4");
		 user_phone4 = request.getParameter("user_phone4");
		 user_fax4 = request.getParameter("user_fax4");
		 user_linkUser4 = request.getParameter("user_linkUser4");
		 user_Type4 = request.getParameter("user_Type4");
		 
		 NIM3_referral_source = request.getParameter("NIM3_referral_source");
		 REFERRAL_STATUS = request.getParameter("REFERRAL_STATUS");
		 REFERRAL_METHOD = request.getParameter("REFERRAL_METHOD");
		 ENCOUNTER_TYPE = request.getParameter("ENCOUNTER_TYPE");
		 REFERRAL_SOURCE = request.getParameter("REFERRAL_SOURCE");
		 REFERRAL_NOTES = request.getParameter("REFERRAL_NOTES");
		 
		 patientfirst = request.getParameter("patientfirst");
		 patientlast = request.getParameter("patientlast");
		 patientaddress = request.getParameter("patientaddress");
		 patientaddress2 = request.getParameter("patientaddress2");
		 patientcity = request.getParameter("patientcity");
		 patientstate = request.getParameter("patientstate");
		 patientzip = request.getParameter("patientzip");
		 patientemail = request.getParameter("patientemail");
		 patientphone = request.getParameter("patientphone");
		 patientcellphone = request.getParameter("patientcellphone");
		 patientdob = request.getParameter("patientdob");
		 patientssn = request.getParameter("patientssn");
		 
		 patientavailability = request.getParameter("patientavailability");
		 
		 employername = request.getParameter("employername");
		 employerfax = request.getParameter("employerfax");
		 employerphone = request.getParameter("employerphone");
		 attorneyfirstname = request.getParameter("attorneyfirstname");
		 attorneylastname = request.getParameter("attorneylastname");
		 attorneyphone = request.getParameter("attorneyphone");
		 attorneyfax = request.getParameter("attorneyfax");
		 INJURY_DESCRIPTION = request.getParameter("INJURY_DESCRIPTION");
 		 }catch(Exception e){// END OF IS GOOD
 			isGood = false;
		 }
	 
	 for(int x = 1; x < 6; x++){
		 	String cpt = request.getParameter("services[" + x + "].cpt");
			String bp = request.getParameter("services[" + x + "].bp");
			String qty = request.getParameter("services[" + x + "].qty");
			String cptModifier = request.getParameter("services[" + x + "].cptModifier");
			String ro = request.getParameter("services[" + x + "].ro");
					String diag1 = request.getParameter("services[" + x + "].diag1");
					String diag2 = request.getParameter("services[" + x + "].diag2");
					String diag3 = request.getParameter("services[" + x + "].diag3");
					String diag4 = request.getParameter("services[" + x + "].diag4");
			if(cpt != null && !cpt.trim().isEmpty() ){
				 incomingServiceList.add(new incomingService(cpt, bp, qty, cptModifier, ro, diag1, diag2, diag3, diag4));
			 }
	 }
		
		 claustrophobic = request.getParameter("claustrophobic");
		 height = request.getParameter("height");
		 weight = request.getParameter("weight");
		 gender = request.getParameter("gender");
		 doi = request.getParameter("doi");
		 onmeds = request.getParameter("onmeds");
		 onmedsdescreption = request.getParameter("onmedsdescreption");
		 physicaltherapy = request.getParameter("physicaltherapy");
		 ptdesc = request.getParameter("ptdesc");
		 metalinbody = request.getParameter("metalinbody");
		 metalinbodydescreption = request.getParameter("metalinbodydescreption");
		 previousimage = request.getParameter("previousimage");
		 previousimagedescreption = request.getParameter("previousimagedescreption");
		 pregnant = request.getParameter("pregnant");
		 pregnantdescreption = request.getParameter("pregnantdescreption");
		 requirehandcarrycd = request.getParameter("requirehandcarrycd");
		 requiresfilms = request.getParameter("requiresfilms");
		 requiresageinjury = request.getParameter("requiresageinjury");
		 isstat = request.getParameter("isstat");
		 isretro = request.getParameter("isretro");
		 
		 if(isGood){
			bltNIM3_CaseAccount ca = new bltNIM3_CaseAccount();
			bltNIM3_Referral rf = new bltNIM3_Referral();
			bltNIM3_Encounter en = new bltNIM3_Encounter();
			bltNIM3_CommTrack ct = new bltNIM3_CommTrack();			
			bltUserAccount ua = new bltUserAccount();
			SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy");
			java.util.Date now = new java.util.Date();
			
			int cid = ca.getUniqueID();
			int eid = en.getUniqueID();
			if(user_othernotification1 != null && !user_othernotification1.trim().isEmpty() && user_schedulenotification1 != null && !user_schedulenotification1.trim().isEmpty() && user_reportnotification1 != null && !user_reportnotification1.trim().isEmpty() && user_first1 != null && !user_first1.trim().isEmpty() && user_last1 != null && !user_last1.trim().isEmpty() && user_1address1 != null && !user_1address1.trim().isEmpty() && user_city1 != null && !user_city1.trim().isEmpty() && user_State1 != null && !user_State1.trim().isEmpty() && user_zip1 != null && !user_zip1.trim().isEmpty() && user_phone1 != null && !user_phone1.trim().isEmpty() && user_linkUser1 != null && !user_linkUser1.isEmpty()){
				
				try{
		 			if(user_othernotification1 != null && !user_othernotification1.trim().isEmpty()){
		 				ua.setComm_Alerts2_LevelUser(new Integer(user_othernotification1));
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
				try{
		 			if(user_schedulenotification1 != null && !user_schedulenotification1.trim().isEmpty()){
		 				ua.setComm_Alerts_LevelUser(new Integer(user_schedulenotification1));
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
				try{
		 			if(user_reportnotification1 != null && !user_reportnotification1.trim().isEmpty()){
		 				ua.setComm_Report_LevelUser(new Integer(user_reportnotification1));
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_first1 != null && !user_first1.trim().isEmpty()){
		 				ua.setContactFirstName(user_first1);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_last1 != null && !user_last1.trim().isEmpty()){
		 				ua.setContactLastName(user_last1);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_1address1 != null && !user_1address1.trim().isEmpty()){
		 				ua.setContactAddress1(user_1address1);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_2address1 != null && !user_2address1.trim().isEmpty()){
		 				ua.setContactAddress2(user_2address1);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_city1 != null && !user_city1.trim().isEmpty()){
		 				ua.setContactCity(user_city1);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_State1 != null && !user_State1.trim().isEmpty()){
		 				ua.setContactStateID(new Integer(user_State1));
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_zip1 != null && !user_zip1.trim().isEmpty()){
		 				ua.setContactZIP(user_zip1);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_email1 != null && !user_email1.trim().isEmpty()){
		 				ua.setContactEmail(user_email1);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_phone1 != null && !user_phone1.trim().isEmpty()){
		 				ua.setContactPhone(user_phone1);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_fax1 != null && !user_fax1.trim().isEmpty()){
		 				ua.setContactFax(user_fax1);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_Type1 != null && !user_Type1.trim().isEmpty()){
		 				ua.setAccountType(user_Type1);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_linkUser1 != null && !user_linkUser1.trim().isEmpty()){
		 				ua.setPayerID(new Integer(user_linkUser1));
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		ua.setAccessType(1);
		 		ua.setStatus(2);
		 		ua.setNIM_UserType("Adj");
		 		ua.setUniqueCreateDate(now);
		 		ua.setUniqueModifyComments("Created by N4");
		 		ua.commitData();
			}
			
				if(user_othernotification2 != null && !user_othernotification2.trim().isEmpty() && user_schedulenotification2 != null && !user_schedulenotification2.trim().isEmpty() && user_reportnotification2 != null && !user_reportnotification2.trim().isEmpty() && user_first2 != null && !user_first2.trim().isEmpty() && user_last2 != null && !user_last2.trim().isEmpty() && user_2address2 != null && !user_2address2.trim().isEmpty() && user_city2 != null && !user_city2.trim().isEmpty() && user_State2 != null && !user_State2.trim().isEmpty() && user_zip2 != null && !user_zip2.trim().isEmpty() && user_phone2 != null && !user_phone2.trim().isEmpty() && user_linkUser2 != null && !user_linkUser2.isEmpty()){
				
				try{
		 			if(user_othernotification2 != null && !user_othernotification2.trim().isEmpty()){
		 				ua.setAlertDays(new Integer(user_othernotification2));
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
				try{
		 			if(user_schedulenotification2 != null && !user_schedulenotification2.trim().isEmpty()){
		 				ua.setAlertDays(new Integer(user_schedulenotification2));
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
				try{
		 			if(user_reportnotification2 != null && !user_reportnotification2.trim().isEmpty()){
		 				ua.setAlertDays(new Integer(user_reportnotification2));
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_first2 != null && !user_first2.trim().isEmpty()){
		 				ua.setContactFirstName(user_first2);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_last2 != null && !user_last2.trim().isEmpty()){
		 				ua.setContactLastName(user_last2);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_1address2 != null && !user_1address2.trim().isEmpty()){
		 				ua.setContactAddress1(user_1address2);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_2address2 != null && !user_2address2.trim().isEmpty()){
		 				ua.setContactAddress2(user_2address2);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_city2 != null && !user_city2.trim().isEmpty()){
		 				ua.setContactCity(user_city2);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_State2 != null && !user_State2.trim().isEmpty()){
		 				ua.setContactStateID(new Integer(user_State2));
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_zip2 != null && !user_zip2.trim().isEmpty()){
		 				ua.setContactZIP(user_zip2);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_email2 != null && !user_email2.trim().isEmpty()){
		 				ua.setContactEmail(user_email2);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_phone2 != null && !user_phone2.trim().isEmpty()){
		 				ua.setContactPhone(user_phone2);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_fax2 != null && !user_fax2.trim().isEmpty()){
		 				ua.setContactFax(user_fax2);
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_Type2 != null && !user_Type2.trim().isEmpty()){
		 				ua.setAccountType(user_Type2);//SET DEFAULT TO ??????
		 			}
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			if(user_linkUser2 != null && !user_linkUser2.trim().isEmpty()){
		 				ua.setPayerID(new Integer(user_linkUser2));
		 			}
		 		}catch(Exception e){
		 			ua.setPayerID(0);
		 			e.printStackTrace();
		 		}
		 		try{
		 			ua.setUniqueCreateDate(now);
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		try{
		 			ua.setUniqueModifyComments("Created by N4");
		 		}catch(Exception e){
		 			e.printStackTrace();
		 		}
		 		ua.setAccessType(1);
		 		ua.setStatus(2);
		 		ua.setNIM_UserType("NCM");
		 		ua.setUniqueCreateDate(now);
		 		ua.setUniqueModifyComments("Created by N4");
		 		ua.commitData();
			}
				
				
				if(user_othernotification3 != null && !user_othernotification3.trim().isEmpty() && user_schedulenotification3 != null && !user_schedulenotification3.trim().isEmpty() && user_reportnotification3 != null && !user_reportnotification3.trim().isEmpty() && user_first3 != null && !user_first3.trim().isEmpty() && user_last3 != null && !user_last2.trim().isEmpty() && user_1address3 != null && !user_2address3.trim().isEmpty() && user_city3 != null && !user_city3.trim().isEmpty() && user_State3 != null && !user_State3.trim().isEmpty() && user_zip3 != null && !user_zip3.trim().isEmpty() && user_phone3 != null && !user_phone3.trim().isEmpty() && user_linkUser3 != null && !user_linkUser3.isEmpty()){
					
					try{
			 			if(user_othernotification3 != null && !user_othernotification3.trim().isEmpty()){
			 				ua.setAlertDays(new Integer(user_othernotification3));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
					try{
			 			if(user_schedulenotification3 != null && !user_schedulenotification3.trim().isEmpty()){
			 				ua.setAlertDays(new Integer(user_schedulenotification3));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
					try{
			 			if(user_reportnotification3 != null && !user_reportnotification3.trim().isEmpty()){
			 				ua.setAlertDays(new Integer(user_reportnotification3));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_first3 != null && !user_first3.trim().isEmpty()){
			 				ua.setContactFirstName(user_first3);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_last3 != null && !user_last3.trim().isEmpty()){
			 				ua.setContactLastName(user_last3);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_1address3 != null && !user_1address3.trim().isEmpty()){
			 				ua.setContactAddress1(user_1address3);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_2address3 != null && !user_2address3.trim().isEmpty()){
			 				ua.setContactAddress2(user_2address3);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_city3 != null && !user_city3.trim().isEmpty()){
			 				ua.setContactCity(user_city3);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_State3 != null && !user_State3.trim().isEmpty()){
			 				ua.setContactStateID(new Integer(user_State3));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_zip3 != null && !user_zip3.trim().isEmpty()){
			 				ua.setContactZIP(user_zip3);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_email3 != null && !user_email3.trim().isEmpty()){
			 				ua.setContactEmail(user_email3);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_phone3 != null && !user_phone3.trim().isEmpty()){
			 				ua.setContactPhone(user_phone3);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_fax3 != null && !user_fax3.trim().isEmpty()){
			 				ua.setContactFax(user_fax3);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_Type3 != null && !user_Type3.trim().isEmpty()){
			 				ua.setAccountType(user_Type3);//SET DEFAULT TO ??????
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_linkUser3 != null && !user_linkUser3.trim().isEmpty()){
			 				ua.setPayerID(new Integer(user_linkUser3));
			 			}
			 		}catch(Exception e){
			 			ua.setPayerID(0);
			 			e.printStackTrace();
			 		}
			 		try{
			 			ua.setUniqueCreateDate(now);
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			ua.setUniqueModifyComments("Created by N4");
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		ua.setAccessType(1);
			 		ua.setStatus(2);
			 		ua.setNIM_UserType("Oth");
			 		ua.setUniqueCreateDate(now);
			 		ua.setUniqueModifyComments("Created by N4");
			 		ua.commitData();
				}
				
					if(user_othernotification4 != null && !user_othernotification4.trim().isEmpty() && user_schedulenotification4 != null && !user_schedulenotification4.trim().isEmpty() && user_reportnotification4 != null && !user_reportnotification4.trim().isEmpty() && user_first4 != null && !user_first4.trim().isEmpty() && user_last4 != null && !user_last4.trim().isEmpty() && user_1address4 != null && !user_2address4.trim().isEmpty() && user_city4 != null && !user_city4.trim().isEmpty() && user_State4 != null && !user_State4.trim().isEmpty() && user_zip4 != null && !user_zip4.trim().isEmpty() && user_phone4 != null && !user_phone4.trim().isEmpty() && user_linkUser4 != null && !user_linkUser4.isEmpty()){
					
					try{
			 			if(user_othernotification4 != null && !user_othernotification4.trim().isEmpty()){
			 				ua.setAlertDays(new Integer(user_othernotification4));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
					try{
			 			if(user_schedulenotification4 != null && !user_schedulenotification4.trim().isEmpty()){
			 				ua.setAlertDays(new Integer(user_schedulenotification4));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
					try{
			 			if(user_reportnotification4 != null && !user_reportnotification4.trim().isEmpty()){
			 				ua.setAlertDays(new Integer(user_reportnotification4));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_first4 != null && !user_first4.trim().isEmpty()){
			 				ua.setContactFirstName(user_first4);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_last4 != null && !user_last4.trim().isEmpty()){
			 				ua.setContactLastName(user_last4);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_1address4 != null && !user_1address4.trim().isEmpty()){
			 				ua.setContactAddress1(user_1address4);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_2address4 != null && !user_2address4.trim().isEmpty()){
			 				ua.setContactAddress2(user_2address4);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_city4 != null && !user_city4.trim().isEmpty()){
			 				ua.setContactCity(user_city4);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_State4 != null && !user_State4.trim().isEmpty()){
			 				ua.setContactStateID(new Integer(user_State4));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_zip4 != null && !user_zip4.trim().isEmpty()){
			 				ua.setContactZIP(user_zip4);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_email4 != null && !user_email4.trim().isEmpty()){
			 				ua.setContactEmail(user_email4);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_phone4 != null && !user_phone4.trim().isEmpty()){
			 				ua.setContactPhone(user_phone4);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_fax4 != null && !user_fax4.trim().isEmpty()){
			 				ua.setContactFax(user_fax4);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_Type4 != null && !user_Type4.trim().isEmpty()){
			 				ua.setAccountType(user_Type4);//SET DEFAULT TO ??????
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(user_linkUser4 != null && !user_linkUser4.trim().isEmpty()){
			 				ua.setPayerID(new Integer(user_linkUser4));
			 			}
			 		}catch(Exception e){
			 			ua.setPayerID(0);
			 			e.printStackTrace();
			 		}
			 		try{
			 			ua.setUniqueCreateDate(now);
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			ua.setUniqueModifyComments("Created by N4");
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		ua.setAccessType(1);
			 		ua.setStatus(2);
			 		ua.setNIM_UserType("Phys");
			 		ua.setUniqueCreateDate(now);
			 		ua.setUniqueModifyComments("Created by N4");
			 		ua.commitData();
				} 
					
				
					
					try{
						if(caseClaimNumber != null && !caseClaimNumber.trim().isEmpty()){
							ca.setCaseClaimNumber(caseClaimNumber);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(NIM3_adjusterID != null && !NIM3_adjusterID.trim().isEmpty()){
							ca.setAdjusterID(new Integer(NIM3_adjusterID));
						}
					}catch(Exception e){
						ca.setAdjusterID(0);
						e.printStackTrace();
					}
					try{
						if(NIM3_LINK_ADJUSTER != null && !NIM3_LINK_ADJUSTER.trim().isEmpty()){
							ca.setPayerID(new Integer(NIM3_LINK_ADJUSTER));
						}
					}catch(Exception e){
						ca.setPayerID(new Integer(0));
						e.printStackTrace();
					}
					if(REFERRAL_STATUS != null && !REFERRAL_STATUS.trim().isEmpty()){
						ca.setCaseStatusID(new Integer(REFERRAL_STATUS));
					}
					try{
			 			if(PAYER_NOTES != null && !PAYER_NOTES.trim().isEmpty()){
			 				ca.setComments(PAYER_NOTES);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
					
			 		try{
			 			if(NIM3_schedulerID != null && !NIM3_schedulerID.trim().isEmpty()){
			 				ca.setAssignedToID(new Integer(NIM3_schedulerID));
			 			}
			 		}catch(Exception e){
			 			ca.setAssignedToID(27773);
			 			e.printStackTrace();//NEED  TO  SET TO UNASSIGNED Q!!!!
			 		}
			 		
			 		try{
			 			if(NIM3_referral_source != null && !NIM3_referral_source.trim().isEmpty()){
			 				ca.setReferredByContactID(new Integer(NIM3_referral_source));
			 			}
					}catch(Exception e){
						ca.setReferredByContactID(new Integer(0));
						e.printStackTrace();
					}
			 		try{
			 			if(REFERRAL_STATUS != null && !REFERRAL_STATUS.trim().isEmpty()){
			 				ca.setCaseStatusID(new Integer(REFERRAL_STATUS));
			 			}
					}catch(Exception e){
						ca.setCaseStatusID(new Integer(0));
					}
			 		try{
			 			if(casenotes != null && !casenotes.trim().isEmpty()){
			 				ca.setComments(casenotes);
			 			}
					}catch(Exception e){
						ca.setComments("Case Created Via N4 ");
					}
			 		
			 		try{
			 			if(patientfirst != null && !patientfirst.trim().isEmpty()){
			 				ca.setPatientFirstName(patientfirst);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(patientfirst != null && !patientlast.trim().isEmpty()){
			 				ca.setPatientLastName(patientlast);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(patientaddress != null && !patientaddress.trim().isEmpty()){
			 				ca.setPatientAddress1(patientaddress);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(patientaddress2 != null && !patientaddress2.trim().isEmpty()){
			 				ca.setPatientAddress2(patientaddress2);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(patientcity != null && !patientcity.trim().isEmpty()){
			 				ca.setPatientCity(patientcity);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(patientstate != null && !patientstate.trim().isEmpty()){
			 				ca.setPatientStateID(new Integer(patientstate));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(patientzip != null && !patientzip.trim().isEmpty()){
			 				ca.setPatientZIP(patientzip);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(patientemail != null && !patientemail.trim().isEmpty()){
			 				ca.setPatientEmail(patientemail);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(patientphone != null && !patientphone.trim().isEmpty()){
			 				ca.setPatientHomePhone(patientphone);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(patientcellphone != null && !patientcellphone.trim().isEmpty()){
			 				ca.setPatientCellPhone(patientcellphone);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		
			 		try{
			 			if(patientdob != null && !patientdob.trim().isEmpty()){
			 				ca.setPatientDOB((new SimpleDateFormat("mm/dd/yyyy").parse(patientdob)));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		 try{
			 			if(patientssn != null && !patientssn.trim().isEmpty()){
			 				ca.setPatientSSN(patientssn);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(employername != null && !employername.trim().isEmpty()){
			 				ca.setEmployerName(employername);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(employerphone != null && !employerphone.trim().isEmpty()){
			 				ca.setEmployerPhone(employerphone);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(employerfax != null && !employerfax.trim().isEmpty()){
			 				ca.setEmployerFax(employerfax);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(attorneyfirstname != null && !attorneyfirstname.trim().isEmpty()){
			 				ca.setAttorneyFirstName(attorneyfirstname);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(attorneylastname != null && !attorneylastname.trim().isEmpty()){
			 				ca.setAttorneyLastName(attorneylastname);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(attorneyphone != null && !attorneyphone.trim().isEmpty()){
			 				ca.setAttorneyWorkPhone(attorneyphone);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(attorneyfax != null && !attorneyfax.trim().isEmpty()){
			 				ca.setAttorneyFax(attorneyfax);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(INJURY_DESCRIPTION != null && !INJURY_DESCRIPTION.trim().isEmpty()){
			 				ca.setInjuryDescription(INJURY_DESCRIPTION);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(claustrophobic != null && !claustrophobic.trim().isEmpty()){
			 				ca.setPatientIsClaus(new Integer(claustrophobic));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(height != null && !height.trim().isEmpty()){
			 				ca.setPatientHeight(height);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(weight != null && !weight.trim().isEmpty()){
			 				ca.setPatientWeight(new Integer(weight));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(gender != null && !gender.trim().isEmpty()){
			 				ca.setPatientGender(gender.toUpperCase());
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(doi != null && !doi.trim().isEmpty()){
			 				ca.setDateOfInjury((new SimpleDateFormat("mm/dd/yyyy").parse(doi)));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(onmeds != null && !onmeds.trim().isEmpty()){
			 				ca.setIsCurrentlyOnMeds(new Integer(onmeds));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(onmedsdescreption != null && !onmedsdescreption.trim().isEmpty()){
			 				ca.setIsCurrentlyOnMedsDesc(onmedsdescreption);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(metalinbody != null && !metalinbody.trim().isEmpty()){
			 				ca.setPatientHasMetalInBody(new Integer(metalinbody));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(metalinbodydescreption != null && !metalinbodydescreption.trim().isEmpty()){
			 				ca.setPatientHasMetalInBodyDesc(metalinbodydescreption);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(previousimage != null && !previousimage.trim().isEmpty()){
			 				ca.setPatientHasPreviousMRIs(new Integer(previousimage));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(previousimagedescreption != null && !previousimagedescreption.trim().isEmpty()){
			 				ca.setPatientHasPreviousMRIsDesc(previousimagedescreption);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(pregnant != null && !pregnant.trim().isEmpty()){
			 				ca.setPatientIsPregnant(new Integer(pregnant));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(pregnantdescreption != null && !pregnantdescreption.trim().isEmpty()){
			 				ca.setPatientIsPregnantDesc(pregnantdescreption);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(NIM3_ncmID != null && !NIM3_ncmID.trim().isEmpty()){
			 				ca.setNurseCaseManagerID(new Integer(NIM3_ncmID));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(NIM3_ADMIN_1_ID != null && !NIM3_ADMIN_1_ID.trim().isEmpty()){
			 				ca.setCaseAdministratorID(new Integer(NIM3_ADMIN_1_ID));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(NIM3_ADMIN_2_ID != null && !NIM3_ADMIN_2_ID.trim().isEmpty()){
			 				ca.setCaseAdministrator2ID(new Integer(NIM3_ADMIN_2_ID));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(NIM3_ADMIN_3_ID != null && !NIM3_ADMIN_3_ID.trim().isEmpty()){
			 				ca.setCaseAdministrator2ID(new Integer(NIM3_ADMIN_3_ID));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(NIM3_ADMIN_4_ID != null && !NIM3_ADMIN_4_ID.trim().isEmpty()){
			 				ca.setCaseAdministrator3ID(new Integer(NIM3_ADMIN_4_ID));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		
			 		ca.setAssignedToID(new Integer(NIM3_schedulerID));
			 		ca.setUniqueCreateDate(now);
			 		ca.setUniqueModifyDate(now);
			 		ca.setUniqueModifyComments("Created by N4");
			 		ca.commitData();
			 		ca.commitData();
			 		ca.setCaseCode("CP" + ca.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random() * 100.0D) + com.winstaff.password.RandomString.generateString(2).toUpperCase());
			 		ca.commitData();
			 		if(ca.getUniqueID() != null){
			 			rf.setCaseID(ca.getUniqueID());
			 		}
			 		try{
			 			if(!REFERRAL_NOTES.trim().isEmpty() && REFERRAL_NOTES.trim() != null){
			 				rf.setComments(REFERRAL_NOTES);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			rf.setUniqueCreateDate(now);
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(!REFERRAL_STATUS.trim().isEmpty() && REFERRAL_STATUS.trim() != null){
			 				rf.setReferralStatusID(new Integer(REFERRAL_STATUS));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(!REFERRAL_SOURCE.trim().isEmpty() && REFERRAL_SOURCE.trim() != null){
			 				rf.setReferralMethod(REFERRAL_SOURCE);
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			rf.setAttendingPhysicianID(52);
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(!NIM3_referral_source.trim().isEmpty() && NIM3_referral_source.trim() != null){
			 				rf.setReferredByContactID(new Integer(NIM3_referral_source));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(!NIM3_mdID.trim().isEmpty() && NIM3_mdID != null){
			 				rf.setReferringPhysicianID(new Integer(NIM3_mdID));
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			 rf.setReferralDate(now);
			 			
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		rf.setUniqueModifyComments("Created by N4");
			 		rf.setReceiveDate(now);
			 		rf.setReferralTypeID(3);
			 		rf.setUniqueCreateDate(now);
			 		rf.setUniqueModifyComments("N4");
			 		rf.commitData();
			 		try{
			 			en.setEncounterStatusID(1);
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(rf.getUniqueID() != null){
			 				en.setReferralID(rf.getUniqueID());
			 			}
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		try{
			 			if(!REFERRAL_NOTES.trim().isEmpty() && REFERRAL_NOTES.trim() != null || !casenotes.trim().isEmpty() && casenotes.trim() != null){
			 				 en.setComments(REFERRAL_NOTES + " " + casenotes);
			 			}
			 		}catch (Exception e){
			 		    	e.printStackTrace();
			 		}
			 		try{
			 			if(!patientavailability.trim().isEmpty() && patientavailability.trim() != null){
			 				en.setPatientAvailability(patientavailability);
			 			}
			 		}catch (Exception e){
			 		    	e.printStackTrace();
			 		}
			 		try{
			 			en.setUniqueModifyComments("Created by N4");
			 		}catch (Exception e){
			 		    	e.printStackTrace();
			 		}
			 		try{
			 			if(!requirehandcarrycd.trim().isEmpty() && requirehandcarrycd.trim() != null){
			 				en.setRequiresHandCarryCD(new Integer(requirehandcarrycd));
			 			}
			 		}catch (Exception e){
			 		    	e.printStackTrace();
			 		}
			 		try{
			 			if(!requiresfilms.trim().isEmpty() && requiresfilms.trim() != null){
			 				en.setRequiresFilms(new Integer(requiresfilms));
			 			}
			 		}catch (Exception e){
			 		    	e.printStackTrace();
			 		}
			 		try{
			 			if(!requiresageinjury.trim().isEmpty() && requiresageinjury.trim() != null){
			 				en.setRequiresAgeInjury(new Integer(requiresageinjury));
			 			}
			 		}catch (Exception e){
			 		    	e.printStackTrace();
			 		}
			 		try{
			 			if(!isstat.trim().isEmpty() && isstat.trim() != null){
			 				en.setisSTAT(new Integer(isstat));
			 			}
			 		}catch (Exception e){
			 		    	e.printStackTrace();
			 		}
			 		try{
			 			if(!isretro.trim().isEmpty() && isretro.trim() != null){
			 				en.setisRetro(new Integer(isretro));
			 			} 
			 		}catch (Exception e){
			 		    	e.printStackTrace();
			 		}
			 		try{
			 			en.setEncounterTypeID(new Integer(ENCOUNTER_TYPE));
			 		}catch(Exception e){
			 			e.printStackTrace();
			 		}
			 		en.setUniqueCreateDate(now);
			 		en.setUniqueModifyComments("n4");
			 		en.setUniqueModifyDate(now);
			 		en.commitData();
			 		en.setScanPass("SP" + en.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random() * 100) + com.winstaff.password.RandomString.generateString(2).toUpperCase());
			 		en.commitData();
			 		for(incomingService incoming : incomingServiceList){
			 			bltNIM3_Service sr = new bltNIM3_Service();
			 			if(!incoming.getCpt().trim().isEmpty() && incoming.getCpt().trim() != null){
			 				sr.setCPT(incoming.getCpt());
			 			}
							
						if(!incoming.getQty().trim().isEmpty() && incoming.getQty().trim() != null){
							sr.setCPTQty(new Integer(incoming.getQty()));
			 			}
						
						if(!incoming.getBp().trim().isEmpty() && incoming.getBp().trim() != null){
							sr.setCPTBodyPart(incoming.getBp());
			 			}
						
						if(!incoming.getCptModifier().trim().isEmpty() && incoming.getCptModifier().trim() != null){
							sr.setCPTModifier(incoming.getCptModifier());
						}
						
						if(!incoming.getRo().trim().isEmpty() && incoming.getRo().trim() != null){
							sr.setCPTText(incoming.getRo());
						}
						
						if(!incoming.getDiag1().trim().isEmpty() && incoming.getDiag1() != null){
							sr.setdCPT1(incoming.getDiag1());
						}
						if(!incoming.getDiag2().trim().isEmpty() && incoming.getDiag2() != null){
							sr.setdCPT2(incoming.getDiag2());						
						}
						if(!incoming.getDiag3().trim().isEmpty() && incoming.getDiag3() != null){
							sr.setdCPT3(incoming.getDiag3());
						}
						if(!incoming.getDiag4().trim().isEmpty() && incoming.getDiag4() != null){
							sr.setdCPT4(incoming.getDiag4());
						}
						
						sr.setServiceTypeID(3);
						sr.setServiceStatusID(1);
						sr.setEncounterID(en.getUniqueID());
						sr.setUniqueModifyComments("Created by N4");
						sr.setUniqueCreateDate(PLCUtils.getNowDate(false));
						sr.setUniqueModifyDate(PLCUtils.getNowDate(false));
						
						sr.commitData();
			 		} 
			 			ct.setCaseID(ca.getUniqueID());//FINISH SETTING COMMTRACKS
			 			ct.setIntUserID(ca.getAssignedToID());
			 			ct.setMessageName(schedulerfullname);
			 			ct.setCaseID(ca.getUniqueID());
			 			ct.setReferralID(rf.getUniqueID());
			 			ct.setEncounterID(en.getUniqueID());
			 			ct.setUniqueCreateDate(now);
			 			ct.setMessageSubject("[Generated via N4]");
			 			ct.setMessageText("Generated via N4" + now + ", Physical Therapy Status: " + physicaltherapy + ", Therapy description: " + ptdesc);
			 			ct.setMessageCompany("N4 Generated");
			 			ct.setMessageSubject(" SM..");
			 			ct.setCommStart(now);
			 			ct.setCommEnd(now);
			 			ct.commitData();
			 			out.println("Please Wait redirecting you now...");
			 			out.println(doi + " " + patientdob);
			 			
			 	%>
			 	<script>
			 	document.location.href = "http://localhost:8080/n4/ca/<%=ca.getUniqueID()%>/<%=en.getUniqueID()%>";
				</script>
			 	<% 		
			 			
		 }
		
	%>
	<!-- public static void main(String[]args){
			 		String theBody = "sdfsdfsdf";
			 			String theSubject = "N4 Alert, new user has been entered user table. Action needed.";
			 			email("maryjane.demille@nextimagemedical.com","support@nextimagemedical.com",theSubject,theBody);
			 	 } -->