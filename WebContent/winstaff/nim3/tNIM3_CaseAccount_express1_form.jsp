<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<link rel="stylesheet" type="text/css" href="../app/js/anytime/anytime.css" />

<script type="text/javascript" src="../app/js/prototype/prototype-1.6.0.3.js" ></script>
<script type="text/javascript" src="../app/js/anytime/anytime.js" ></script>

<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tNIM3_CaseAccount_form.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<script language="javascript">
var selectClick = false;
function selectClickFunc(){
	window.selectClick = true;	
}
function selectItemByValue(elmnt, value){
    for(var i=0; i < elmnt.options.length; i++)
    {
      if(elmnt.options[i].value == value)
        elmnt.selectedIndex = i;
    }
}
function changePayer(value){
	if (selectClick){
	 	var selectPayer = document.getElementById('PayerIDSelect');
	 	selectItemByValue(selectPayer, value);
	}
}
function formatSSN()
{
     var theCount = 0;
     var theString = document.getElementById("PatientSSN").value;
     var newString = "";
     var myString = theString;
     var theLen = myString.length;
     for ( var i = 0 ; i < theLen ; i++ )
     {
     // Character codes for ints 1 - 9 are 48 - 57
          if ( (myString.charCodeAt(i) >= 48 ) && (myString.charCodeAt(i) <= 57) )
          newString = newString + myString.charAt(i);   
     }
// Now the validation to determine that the remaining string is 9 characters.
     if (newString.length == 9 )
     {
// Now the string has been stripped of other chars it can be reformatted to ###-##-#### 
          var newLen = newString.length;
          var newSSN = "";
          for ( var i = 0 ; i < newLen ; i++ )
          {
               if ( ( i == 2 ) || ( i == 4 ) )
               {
                    newSSN = newSSN + newString.charAt(i) + "-";
               }else{
                    newSSN = newSSN + newString.charAt(i);
               }
          }
          document.getElementById("PatientSSN").value = newSSN;
          return true;
     }else{
          alert("The Social Security Number you entered "+newString+" does not contian the correct number of digits");
          document.getElementById("PatientSSN").focus();
          return false;
     }
}
function formatPhone( myElement)
{
     var theCount = 0;
     var theString = document.getElementById(myElement).value;
     var newString = "";
     var myString = theString;
     var theLen = myString.length;
     for ( var i = 0 ; i < theLen ; i++ )
     {
     // Character codes for ints 1 - 9 are 48 - 57
          if ( (myString.charCodeAt(i) >= 48 ) && (myString.charCodeAt(i) <= 57) )
          newString = newString + myString.charAt(i);   
     }
// Now the validation to determine that the remaining string is 9 characters.
     if (newString.length == 10 )
     {
// Now the string has been stripped of other chars it can be reformatted to ###-##-#### 
          var newLen = newString.length;
          var newSSN = "";
          for ( var i = 0 ; i < newLen ; i++ )
          {
               if ( ( i == 2 ) || ( i == 5 ) )
               {
                    newSSN = newSSN + newString.charAt(i) + "-";
               }else{
                    newSSN = newSSN + newString.charAt(i);
               }
          }
          document.getElementById(myElement).value = newSSN;
          return true;
     }else{
          alert("The Phone Number you entered "+newString+" does not contian the correct number of digits");
          document.getElementById(myElement).focus();
          return false;
     }
}
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
</script>

<script>
function referralSourceOverride(type){
	if (type=="reg"){
		document.getElementById('ReferredByContactID_IF').style.display = 'inline';
	}
	else if (type=="other"){
		document.getElementById('ReferredByContactID_IF').style.display = 'none';
	}
}

function dateValidation(type, value){
	var pattern = /^\d{2}\/\d{2}\/\d{4}$/;
	if (type=="PatientDOB"){
		typeTrans = "Patient Date of birth";
	}
	if (type=="DateOfInjury"){
		typeTrans = "Patient Date of Injury";
	}
	if(!value.match(pattern)){
		alert("Check date format for "+typeTrans+".\n"+value);
		document.getElementById(type).focus();
	}

}

</script>

<%

	String sQUICKVIEW = "no";
	if ( request.getParameter( "QUICKVIEW" ) != null && request.getParameter( "QUICKVIEW" ).equalsIgnoreCase("yes") )
    {
        sQUICKVIEW        =    "yes";
    }




String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
//	tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear_fade.jsp?plcID="+thePLCID;
	tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
if (sQUICKVIEW.equalsIgnoreCase("yes"))
{
}
else
{
%>	<p class=title>Case Create</p><hr><%
}
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">

<script language="javascript">
//show_pws_Loading();   //conflicts with Receive Time
</script>



    <table cellpadding=0 cellspacing=0 border=0 width=98%  >
    <tr><td width=10>&nbsp;</td><td>
<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    Integer        iCaseID        =    null;
    Integer        iReferralID        =    null;
    Integer        iEDITID        =    null;

    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iCaseID")&&pageControllerHash.containsKey("iEncounterID")&&pageControllerHash.containsKey("iReferralID")) 
    {
		iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
		iReferralID        =    (Integer)pageControllerHash.get("iReferralID");
        accessValid = true;
 //       if (request.getParameter( "EDIT" ).equalsIgnoreCase("nim3exp")&&iEncounterID.intValue() != iEDITID.intValue())
        {
//        	accessValid = false;
        }
    //    else if (iCaseID.intValue() != iEDITID.intValue())
        {
//        	accessValid = false;
        }
    }
  //page security
  if (accessValid&&isScheduler)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_CaseAccount_express1_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID
	NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"Edit Page");


//fields
        %>
        <table border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase width="100%">
         <tr>
           <td class=tableColor>
        <form action="tNIM3_CaseAccount_express1_form_sub.jsp" name="tNIM3_CaseAccount_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
    %>
        <input type="hidden" name="EDITID" value = "<%=iEDITID%>" >  
        <input type="hidden" name="QUICKVIEW" value = "<%=sQUICKVIEW%>" >  

          <%  String theClass ="tdBase";%>

            <table width="100%" cellpadding=0 cellspacing=0  >
           
                     
                     <tr>
                       <td >&nbsp;</td>
                       <td >&nbsp;</td>
                       <td align="right">&nbsp;  <%if (sQUICKVIEW.equalsIgnoreCase("yes")){%>
                         <input  class="inputButton_md_Create" onClick = "MM_validateForm('CaseClaimNumber','','R');if (document.MM_returnValue){this.disabled=true;submit();return false}else{this.disabled=false;return false}" type=Submit value="Save" name=Submit3>
<input class="inputButton_md_Default" onClick = "this.disabled=true;window.close();" type=button value="Close" name=Submit2><%}%>                         </td>
                     </tr>
                     <tr>
                       <td >&nbsp;</td>
                       <td class="borderHighlightBlackDottedNoFont" colspan="2">Assigned To
                       
<select name="AssignedToID"/>
<option value="0">Unassigned/Empty</option>
    <%
{
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("SELECT\n" +
					"	ua.Userid,\n" +
					"	ua.LogonUserName,\n" +
					"	ua.contactfirstname,\n" +
					"	ua.contactlastname\n" +
					"FROM\n" +
					"	tUserAccount ua\n" +
					"WHERE\n" +
					"	(\n" +
					"		accounttype = 'SchedulerID'\n" +
					"		OR accounttype = 'SchedulerID2'\n" +
					"		OR accounttype = 'SchedulerID3'\n" +
					"		OR accounttype = 'SchedulerID4'\n" +
					"	)\n" +
					"AND ua.status = 2\n" +
					"ORDER BY\n" +
					"	LogonUserName");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
				if (new Integer(myRS.getString("UserID")).intValue()==myEO2.getNIM3_CaseAccount().getAssignedToID().intValue())
				{
					%>
					<option selected value="<%=myRS.getString("UserID")%>"><%=myRS.getString("LogonUserName")%> [<%=myRS.getString("contactfirstname")%>, <%=myRS.getString("contactlastname")%>]</option>
					<%
				}
				else
				{
					%>
					<option  value="<%=myRS.getString("UserID")%>"><%=myRS.getString("LogonUserName")%> [<%=myRS.getString("contactfirstname")%>, <%=myRS.getString("contactlastname")%>]</option>
					<%
				}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
%>
    </select>          &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AssignedToID&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("AssignedToID")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>             
                       
                       
                       </td>
                     </tr>
                     
                     <tr>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                     </tr>
                     <tr>
                       <td>&nbsp;</td>
                       <td>Case Information</td>
                       <td>&nbsp;</td>
                     </tr>
					          <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("AdjusterID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                          <td valign=middle>&nbsp;</td>
                          <td valign=middle><p class=<%=theClass%> ><b>Adjuster&nbsp;</b>
<input class="inputButton_md_Action1" name="AdjusterID2" type="button"   onClick="this.disabled=false;modalPost('AdjusterID', modalWin('LI_tUserAccount_Search.jsp?','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.AdjusterID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('AdjusterID').value;selectClickFunc();" value="Search..." <%=HTMLFormStyleButton%>  >
                        </td><td valign=middle><div class="boxDr"><iframe name="AdjusterID_IF" frameborder="0" marginheight="0" marginwidth="0"  class="boxDrSize" scrolling="no" src="../generic/LI_tUserAccount_Search_translate_wrap.jsp?CurrentSelection=<%=myEO2.getNIM3_CaseAccount().getAdjusterID()%>"></iframe></div><input name="AdjusterID" id="AdjusterID"  onFocus="this.blur();modalPost('AdjusterID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=AdjusterID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.AdjusterID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('AdjusterID').value;" type="hidden" value="<%=myEO2.getNIM3_CaseAccount().getAdjusterID()%>">&nbsp;&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AdjusterID&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("AdjusterID")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                        </tr>
                        <%
            }
            %>
                     

<%
//if (isScheduler&&myEO2.getNIM3_CaseAccount().getPayerID().intValue()==50)
if (isScheduler2)
{
%>
<tr><td colspan="3" valign=middle>
    <%-- <select disabled class="instructionsBig1" id="PayerIDSelect" name="PayerID" onChange="alert('Altering the Payer for this Case requires that the Case be Saved immediately.\n\nPlease click continue to do so.  \n\nYou can re-EDIT once saved.\n\nYou will need to ensure that the Adjuster for this case is changed accordingly.');document.getElementById('SubmitType').value=0;document.getElementById('tNIM3_CaseAccount_form1').submit();" />--%>
    <select class="instructionsBig1" id="PayerIDSelect" name="PayerID" />
    <option value="50">Please Select...</option>
    <%
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("select PayerID, PayerName, SalesDivision from tNIM3_PayerMaster where PayerTypeID = 2 order by payername");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
				%>
                <option value="<%=myRS.getString("PayerID")%>" <% if (myRS.getString("PayerID").equalsIgnoreCase(myEO2.getNIM3_CaseAccount().getPayerID().toString())){out.print (" selected ");}	  %>  ><%=myRS.getString("PayerName")%> [<%=myRS.getString("SalesDivision")%>]</option>
                <%
			}
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
		mySS.closeAll();
	
	%>
    </select>
    
</td></tr>
<%
}
%>

            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("CaseStatusID",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("CaseStatusID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("CaseStatusID",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("CaseStatusID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
			if (false&&(myEO2.getNIM3_CaseAccount().isRead("CaseStatusID",UserSecurityGroupID)))
            {
                        %>
                        <tr class="inputButton_sm_Test">
                          <td valign=middle>&nbsp;</td>
                          <td valign=middle ><p class=<%=theClass%> ><b>Case Status&nbsp;</b></p></td><td valign=middle><p><jsp:include page="../generic/tCaseAccountStatusLI_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_CaseAccount().getCaseStatusID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CaseStatusID&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("CaseStatusID")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
			%>

            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("CaseClaimNumber",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("CaseClaimNumber")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("CaseClaimNumber",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("CaseClaimNumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("CaseClaimNumber",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=middle>&nbsp;</td>
                     <td valign=middle><p class=<%=theClass%> ><b>Claim Number&nbsp;</b></p></td><td valign=middle><p><input maxlength="100" type=text size="40" name="CaseClaimNumber" id="CaseClaimNumber" value="<%=myEO2.getNIM3_CaseAccount().getCaseClaimNumber()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CaseClaimNumber&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("CaseClaimNumber")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("CaseCode",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("CaseCode")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("CaseCode",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("CaseCode",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isRead("CaseCode",UserSecurityGroupID)))
            {
                        %>
                         <tr style="display:none">
                           <td valign=middle>&nbsp;</td>
                         <td valign=middle><p class=<%=theClass%> ><b>CaseCode&nbsp;</b></p></td><td valign=middle><p><%=myEO2.getNIM3_CaseAccount().getCaseCode()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CaseCode&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("CaseCode")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                         </tr>
                        <%
            }
            %>




            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("AdjusterID",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("AdjusterID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("AdjusterID",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("AdjusterID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("DateOfInjury",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("DateOfInjury")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("DateOfInjury",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("DateOfInjury",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("DateOfInjury",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                          <td valign=middle>&nbsp;</td>
                        <td valign=middle><p class=<%=theClass%> ><b>Date Of Injury&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=middle><p><input maxlength=10  type=text size="20" name="DateOfInjury" id="DateOfInjury" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_CaseAccount().getDateOfInjury())%>" /></jsp:include>' onchange="dateValidation('DateOfInjury',this.value);">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfInjury&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("DateOfInjury")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                        </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientFirstName",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientFirstName")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientFirstName",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientFirstName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientFirstName",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b>Patient First Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="60" name="PatientFirstName" id="PatientFirstName" value="<%=myEO2.getNIM3_CaseAccount().getPatientFirstName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientFirstName&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientFirstName")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientLastName",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientLastName")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientLastName",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientLastName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientLastName",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b>Last Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="60" name="PatientLastName" id="PatientLastName" value="<%=myEO2.getNIM3_CaseAccount().getPatientLastName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientLastName&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientLastName")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientDOB",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientDOB")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientDOB",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientDOB",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientDOB",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                          <td valign=top>&nbsp;</td>
                        <td valign=top><p class=<%=theClass%> ><b> DOB&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="40" name="PatientDOB"  id="PatientDOB" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_CaseAccount().getPatientDOB())%>" /></jsp:include>' onchange="dateValidation('PatientDOB',this.value)">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientDOB&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientDOB")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                        </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientSSN",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientSSN")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientSSN",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientSSN",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientSSN",UserSecurityGroupID)))
            {
                        %>
                   <tr>
                     <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b> SSN&nbsp;</b></p></td><td valign=top><p><input maxlength="50" onChange="formatSSN();" type=text size="20" name="PatientSSN" id="PatientSSN" value="<%=myEO2.getNIM3_CaseAccount().getPatientSSN()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientSSN&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientSSN")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%> 
                     <label>
                           <input type="button" name="buttonact2" class="inputButton_md_Default" id="buttonact2" value="Not Given" onClick="document.getElementById('PatientSSN').value='Not Given';">
                     </label>
              </p></td>
              </tr>
                     
                     
                     
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientGender",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientGender")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientGender",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientGender",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientGender",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b> Gender&nbsp;</b></p></td><td valign=top><p><input maxlength="1" type=text size="60" name="PatientGender"  id="PatientGender" value="<%=myEO2.getNIM3_CaseAccount().getPatientGender()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientGender&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientGender")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientAddress1",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientAddress1",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientAddress1",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b> Address1&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="60" name="PatientAddress1" id="PatientAddress1" value="<%=myEO2.getNIM3_CaseAccount().getPatientAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientAddress1&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientAddress1")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientAddress2",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientAddress2",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientAddress2",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b> Address2&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="60" name="PatientAddress2" id="PatientAddress2" value="<%=myEO2.getNIM3_CaseAccount().getPatientAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientAddress2&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientAddress2")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientCity",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientCity")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientCity",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientCity",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b> City&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="60" name="PatientCity" disabled id="PatientCity" value="<%=myEO2.getNIM3_CaseAccount().getPatientCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCity&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientCity")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientStateID",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientStateID",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                          <td valign=top>&nbsp;</td>
                          <td valign=top><p class=<%=theClass%> ><b> State&nbsp;</b></p></td><td valign=top><p><select disabled  name="PatientStateID" id="PatientStateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_CaseAccount().getPatientStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientStateID&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientStateID")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientZIP",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientZIP",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b> ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="60" name="PatientZIP"  id="PatientZIP" value="<%=myEO2.getNIM3_CaseAccount().getPatientZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientZIP&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientZIP")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHomePhone",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHomePhone")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHomePhone",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHomePhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHomePhone",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b> Home Phone&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="60" name="PatientHomePhone" id="PatientHomePhone" value="<%=myEO2.getNIM3_CaseAccount().getPatientHomePhone()%>" onChange="formatPhone('PatientHomePhone');"> &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHomePhone&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHomePhone")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientWorkPhone",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientWorkPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientWorkPhone",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientWorkPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientWorkPhone",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b> Work Phone&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="60" name="PatientWorkPhone" id="PatientWorkPhone" value="<%=myEO2.getNIM3_CaseAccount().getPatientWorkPhone()%>"  onChange="formatPhone('PatientWorkPhone');" >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientWorkPhone&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientWorkPhone")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientCellPhone",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientCellPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientCellPhone",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientCellPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientCellPhone",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b> Cell Phone&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="60" name="PatientCellPhone"  id="PatientCellPhone" value="<%=myEO2.getNIM3_CaseAccount().getPatientCellPhone()%>"  onChange="formatPhone('PatientCellPhone');">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCellPhone&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientCellPhone")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("MobilePhoneCarrier",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("MobilePhoneCarrier")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("MobilePhoneCarrier",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("MobilePhoneCarrier",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("MobilePhoneCarrier",UserSecurityGroupID)))
            {
                        %>
                     <tr style="display:none">                      <td valign=top>&nbsp;</td>
<td valign=top><p class=<%=theClass%> ><b>Mobile Phone Carrier&nbsp;</b></p></td><td valign=top><p>
<select   name="MobilePhoneCarrier" id="MobilePhoneCarrier" ><jsp:include page="../generic/tMobileCarrierLI.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_CaseAccount().getMobilePhoneCarrier()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MobilePhoneCarrier&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("MobilePhoneCarrier")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }

            %>


            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientEmail",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientEmail",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientEmail",UserSecurityGroupID)))
            {
                        %>
                       <tr>                       <td valign=top>&nbsp;</td>
<td valign=top><p class=<%=theClass%> ><b><%=myEO2.getNIM3_CaseAccount().getEnglish("PatientEmail")%>&nbsp;</b></p></td><td valign=top><p><input name="PatientEmail" size="60" type=text value="<%=myEO2.getNIM3_CaseAccount().getPatientEmail()%>"> &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientEmail&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientEmail")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("EmployerName",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("EmployerName")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("EmployerName",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("EmployerName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("EmployerName",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b>Employer&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="60" name="EmployerName" id="EmployerName" value="<%=myEO2.getNIM3_CaseAccount().getEmployerName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EmployerName&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("EmployerName")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>





            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("EmployerPhone",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("EmployerPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("EmployerPhone",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("EmployerPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("EmployerPhone",UserSecurityGroupID)))
            {
                        %>
                     <tr style="display:none">
                       <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b>Employer Phone&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="60" name="EmployerPhone" value="<%=myEO2.getNIM3_CaseAccount().getEmployerPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EmployerPhone&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("EmployerPhone")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("EmployerFax",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("EmployerFax")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("EmployerFax",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("EmployerFax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("EmployerFax",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=top>&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b>Employer Fax&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="60" name="EmployerFax" value="<%=myEO2.getNIM3_CaseAccount().getEmployerFax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EmployerFax&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("EmployerFax")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>

 <tr>
                        <td valign=middle>&nbsp;</td>
                        
                        <td valign=middle><p class=<%=theClass%> ><b>Employer/Location&nbsp;</b>&nbsp;<input <%=HTMLFormStyleButton%> name="EmployerLocationID2"  onClick="this.disabled=false;modalPost('EmployerLocationID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=EmployerLocationID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.EmployerLocationID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('EmployerLocationID').value;" type="button" value="Search..."  ></p></td><td valign=top><div class="boxDr"><iframe name="EmployerLocationID_IF" frameborder="0" marginheight="0" marginwidth="0" class="boxDrSize" scrolling="no" src="../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=<%=myEO2.getNIM3_CaseAccount().getEmployerLocationID()%>"></iframe></div><input name="EmployerLocationID" id="EmployerLocationID"  onFocus="this.blur();modalPost('EmployerLocationID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=EmployerLocationID','Search...','dialogWidth:800px;dialogHeight:400px','status=yes,scrollbars=yes,resizable=yes,width=800,height=400'));document.EmployerLocationID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('EmployerLocationID').value;" type="hidden" value="<%=myEO2.getNIM3_CaseAccount().getEmployerLocationID()%>">&nbsp;&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EmployerLocationID&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("EmployerLocationID")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                        </tr>

            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("InjuryDescription",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("InjuryDescription")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("InjuryDescription",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("InjuryDescription",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("InjuryDescription",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                         <td valign=middle>&nbsp;</td>
                       <td valign=middle><p class=<%=theClass%> ><b>Injury Description:&nbsp;</b></p></td><td valign=middle><p><textarea onKeyDown="textAreaStop(this,500)" rows="3" name="InjuryDescription" cols="40"><%=myEO2.getNIM3_CaseAccount().getInjuryDescription()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InjuryDescription&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("InjuryDescription")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                       </tr>
                        <%
            }
            %>





                        <tr>
                          <td valign=top colspan="3" class="tdHeader">Pre-Screen Questions <%if (true||NIMUtils.isPayerTyson(myEO2.getNIM3_CaseAccount().getPayerID())){%>&nbsp;&nbsp;&nbsp;&nbsp;<input name="Skip" value="Skip" onClick="document.getElementById('PatientIsClaus').value = 3;document.getElementById('PatientIsHWRatio').value = 3;document.getElementById('PatientHasImplants').value = 3;document.getElementById('PatientHasMetalInBody').value = 3;document.getElementById('PatientHasAllergies').value = 3;document.getElementById('PatientHasRecentSurgery').value = 3;document.getElementById('PatientHasPreviousMRIs').value = 3;document.getElementById('PatientHasKidneyLiverHypertensionDiabeticConditions').value = 3;document.getElementById('PatientIsPregnant').value = 3;document.getElementById('PatientHeight').value = 'N/A';" type="button"><%}%></td>
                          </tr>


            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientIsClaus",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientIsClaus")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientIsClaus",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientIsClaus",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientIsClaus",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                          <td valign=top>&nbsp;</td>
                          <td valign=top><p class=<%=theClass%> ><b>Severely Claustrophobic</b></p></td><td valign=top><p><select  name="PatientIsClaus" id="PatientIsClaus" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_CaseAccount().getPatientIsClaus()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientIsClaus&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientIsClaus")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientIsHWRatio",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientIsHWRatio")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientIsHWRatio",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientIsHWRatio",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientIsHWRatio",UserSecurityGroupID)))
            {
                        %>
                        <tr style="display:none">
                          <td valign=top>&nbsp;</td>
                          <td valign=top><p class=<%=theClass%> ><b>H/W Ratio Open MRI</b></p></td><td valign=top><p><select   name="PatientIsHWRatio"  id="PatientIsHWRatio" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="3" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientIsHWRatio&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientIsHWRatio")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>




            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHasImplants",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHasImplants")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHasImplants",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHasImplants",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHasImplants",UserSecurityGroupID)))
            {
                        %>
                        <tr style="display:none">                          <td valign=top>&nbsp;</td>
<td valign=top><p class=<%=theClass%> ><b>Has Implants&nbsp;</b></p></td><td valign=top><p><select   name="PatientHasImplants"   id="PatientHasImplants" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="3" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHasImplants&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHasImplants")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
</tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHasImplantsDesc",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHasImplantsDesc")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHasImplantsDesc",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHasImplantsDesc",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHasImplantsDesc",UserSecurityGroupID)))
            {
                        %>
                     <tr style="display:none"> <td valign=top>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>PatientHasImplantsDesc&nbsp;</b></p></td><td valign=top><p><input maxlength="70" type=text size="50" name="PatientHasImplantsDesc"  id="PatientHasImplantsDesc" value="3">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHasImplantsDesc&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHasImplantsDesc")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>

			
			<%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("IsCurrentlyOnMeds",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("IsCurrentlyOnMeds")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("IsCurrentlyOnMeds",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("IsCurrentlyOnMeds",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("IsCurrentlyOnMeds",UserSecurityGroupID)))
            {
                        %>
                        <tr> <td valign=top>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Currently On Meds&nbsp;</b></p></td><td valign=top><p><select   name="IsCurrentlyOnMeds" id="IsCurrentlyOnMeds" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_CaseAccount().getIsCurrentlyOnMeds()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsCurrentlyOnMeds&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("IsCurrentlyOnMeds")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("IsCurrentlyOnMedsDesc",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("IsCurrentlyOnMedsDesc")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("IsCurrentlyOnMedsDesc",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("IsCurrentlyOnMedsDesc",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("IsCurrentlyOnMedsDesc",UserSecurityGroupID)))
            {
                        %>
              <tr><td valign=top>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>What Kind of Meds?&nbsp;</b></p></td><td valign=top><p><input maxlength="70" type=text size="50" name="IsCurrentlyOnMedsDesc" id="IsCurrentlyOnMedsDesc" value="<%=myEO2.getNIM3_CaseAccount().getIsCurrentlyOnMedsDesc()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IsCurrentlyOnMedsDesc&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("IsCurrentlyOnMedsDesc")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>
			
            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHasMetalInBody",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHasMetalInBody")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHasMetalInBody",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHasMetalInBody",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHasMetalInBody",UserSecurityGroupID)))
            {
                        %>
                        <tr>                          <td valign=top>&nbsp;</td>
<td valign=top><p class=<%=theClass%> ><b>Has Metal In Body&nbsp;</b></p></td><td valign=top><p><select   name="PatientHasMetalInBody"  id="PatientHasMetalInBody" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_CaseAccount().getPatientHasMetalInBody()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHasMetalInBody&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHasMetalInBody")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
</tr>
                        <%
            }
            %>

			
            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHasMetalInBodyDesc",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHasMetalInBodyDesc")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHasMetalInBodyDesc",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHasMetalInBodyDesc",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHasMetalInBodyDesc",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>PatientHasMetalInBodyDesc&nbsp;</b></p></td><td valign=top><p><input maxlength="70" type=text size="50" name="PatientHasMetalInBodyDesc" id="PatientHasMetalInBodyDesc" value="<%=myEO2.getNIM3_CaseAccount().getPatientHasMetalInBodyDesc()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHasMetalInBodyDesc&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHasMetalInBodyDesc")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>

			
			

            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHasAllergies",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHasAllergies")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHasAllergies",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHasAllergies",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHasAllergies",UserSecurityGroupID)))
            {
                        %>
                       <tr style="display:none">                         <td valign=top>&nbsp;</td>
<td valign=top><p class=<%=theClass%> ><b>Has Allergies&nbsp;</b></p></td><td valign=top><p><select   name="PatientHasAllergies"   id="PatientHasAllergies" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="3" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHasAllergies&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHasAllergies")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
</tr>
                        <%
            }
            %>

			
	            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHasAllergiesDesc",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHasAllergiesDesc")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHasAllergiesDesc",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHasAllergiesDesc",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHasAllergiesDesc",UserSecurityGroupID)))
            {
                        %>
                    <tr style="display:none"> <td valign=top>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>PatientHasAllergiesDesc&nbsp;</b></p></td><td valign=top><p><input maxlength="70" type=text size="50" name="PatientHasAllergiesDesc" id="PatientHasAllergiesDesc" value="<%=myEO2.getNIM3_CaseAccount().getPatientHasAllergiesDesc()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHasAllergiesDesc&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHasAllergiesDesc")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>

		
			

            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHasRecentSurgery",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHasRecentSurgery")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHasRecentSurgery",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHasRecentSurgery",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHasRecentSurgery",UserSecurityGroupID)))
            {
                        %>
                        <tr style="display:none">                          <td valign=top>&nbsp;</td>
<td valign=top><p class=<%=theClass%> ><b>Has Had Recent Surgery&nbsp;</b></p></td><td valign=top><p><select   name="PatientHasRecentSurgery"  id="PatientHasRecentSurgery" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="3" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHasRecentSurgery&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHasRecentSurgery")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
</tr>
                        <%
            }
            %>


      <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHasRecentSurgeryDesc",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHasRecentSurgeryDesc")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHasRecentSurgeryDesc",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHasRecentSurgeryDesc",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHasRecentSurgeryDesc",UserSecurityGroupID)))
            {
                        %>
                     <tr style="display:none"> <td valign=top>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>PatientHasRecentSurgeryDesc&nbsp;</b></p></td><td valign=top><p><input maxlength="70" type=text size="50" name="PatientHasRecentSurgeryDesc"  id="PatientHasRecentSurgeryDesc" value="<%=myEO2.getNIM3_CaseAccount().getPatientHasRecentSurgeryDesc()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHasRecentSurgeryDesc&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHasRecentSurgeryDesc")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>

			
			
			
            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHasPreviousMRIs",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHasPreviousMRIs")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHasPreviousMRIs",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHasPreviousMRIs",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHasPreviousMRIs",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top>&nbsp;</td>
<td valign=top><p class=<%=theClass%> ><b>Has Had Previous Imaging&nbsp;</b></p></td><td valign=top><p><select   name="PatientHasPreviousMRIs"   id="PatientHasPreviousMRIs" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_CaseAccount().getPatientHasPreviousMRIs()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHasPreviousMRIs&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHasPreviousMRIs")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
</tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHasPreviousMRIsDesc",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHasPreviousMRIsDesc")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHasPreviousMRIsDesc",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHasPreviousMRIsDesc",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHasPreviousMRIsDesc",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>What Imaging, How long ago?&nbsp;</b></p></td><td valign=top><p><input maxlength="70" type=text size="50" name="PatientHasPreviousMRIsDesc" id="PatientHasPreviousMRIsDesc" value="<%=myEO2.getNIM3_CaseAccount().getPatientHasPreviousMRIsDesc()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHasPreviousMRIsDesc&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHasPreviousMRIsDesc")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>


			
			
			

            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHasKidneyLiverHypertensionDiabeticConditions",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHasKidneyLiverHypertensionDiabeticConditions")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHasKidneyLiverHypertensionDiabeticConditions",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHasKidneyLiverHypertensionDiabeticConditions",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHasKidneyLiverHypertensionDiabeticConditions",UserSecurityGroupID)))
            {
                        %>
                        <tr style="display:none"> <td valign=top>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Patient Has Kidney/ Liver/<br>
                        Hypertension/ Diabetic Conditions&nbsp;</b></p></td><td valign=top><p><select   name="PatientHasKidneyLiverHypertensionDiabeticConditions"   id="PatientHasKidneyLiverHypertensionDiabeticConditions" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="3" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHasKidneyLiverHypertensionDiabeticConditions&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHasKidneyLiverHypertensionDiabeticConditions")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>

			

            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHasKidneyLiverHypertensionDiabeticConditionsDesc",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHasKidneyLiverHypertensionDiabeticConditionsDesc")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHasKidneyLiverHypertensionDiabeticConditionsDesc",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHasKidneyLiverHypertensionDiabeticConditionsDesc",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHasKidneyLiverHypertensionDiabeticConditionsDesc",UserSecurityGroupID)))
            {
                        %>
                    <tr style="display:none"><td valign=top>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Patient Has Kidney/ Liver/<br>
Hypertension/ Diabetic Conditions Desc</b></p></td><td valign=top><p><input maxlength="70" type=text size="50" name="PatientHasKidneyLiverHypertensionDiabeticConditionsDesc" value="<%=myEO2.getNIM3_CaseAccount().getPatientHasKidneyLiverHypertensionDiabeticConditionsDesc()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHasKidneyLiverHypertensionDiabeticConditionsDesc&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHasKidneyLiverHypertensionDiabeticConditionsDesc")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
</tr>
                        <%
            }
            %>

			
			

            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientIsPregnant",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientIsPregnant")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientIsPregnant",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientIsPregnant",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientIsPregnant",UserSecurityGroupID)))
            {
                        %>
                        <tr> <td valign=top>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Patient Is Pregnant&nbsp;</b></p></td><td valign=top><p><select   name="PatientIsPregnant" id="PatientIsPregnant" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_CaseAccount().getPatientIsPregnant()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientIsPregnant&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientIsPregnant")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientIsPregnantDesc",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientIsPregnantDesc")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientIsPregnantDesc",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientIsPregnantDesc",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientIsPregnantDesc",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>PatientIsPregnantDesc&nbsp;</b></p></td><td valign=top><p><input maxlength="70" type=text size="50" name="PatientIsPregnantDesc" value="<%=myEO2.getNIM3_CaseAccount().getPatientIsPregnantDesc()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientIsPregnantDesc&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientIsPregnantDesc")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>

            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientHeight",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientHeight")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientHeight",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientHeight",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientHeight",UserSecurityGroupID)))
            {
                        %>
                     <tr> <td valign=top>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Patient Height&nbsp;</b></p></td><td valign=top><p><select name="PatientHeight" id="PatientHeight"><jsp:include page="../generic/tPatientHeightLI.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_CaseAccount().getPatientHeight()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHeight&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientHeight")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("PatientWeight",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("PatientWeight")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("PatientWeight",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("PatientWeight",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("PatientWeight",UserSecurityGroupID)))
            {
                        %>
                     <tr> <td valign=top>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Patient Weight (lbs)&nbsp;</b></p></td><td valign=top><p><input type=text size="5" maxlength="3" name="PatientWeight" value="<%=myEO2.getNIM3_CaseAccount().getPatientWeight()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientWeight&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("PatientWeight")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>



        <tr>
          <td valign=middle colspan="2" class="tdHeader">Additional Case Info:</td>
        </tr>





            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("Comments",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("Comments",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                         <td valign=middle>&nbsp;</td>
                       <td valign=middle><p class=<%=theClass%> ><b>Case Notes: &nbsp;</b></p></td><td valign=middle><p><textarea name="tCaseAccount_Comments" cols="40" rows="7" id="tCaseAccount_Comments" onKeyDown="textAreaStop(this,2000)"><%=myEO2.getNIM3_CaseAccount().getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                       </tr>
                        <%
            }
            %>




            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("NurseCaseManagerID",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("NurseCaseManagerID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("NurseCaseManagerID",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("NurseCaseManagerID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("NurseCaseManagerID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                          <td valign=middle nowrap>&nbsp;</td>
                          <td valign=middle nowrap><b> Nurse Case Manager</b>
<input <%=HTMLFormStyleButton%> name="NurseCaseManagerID2"  onClick="this.disabled=false;modalPost('NurseCaseManagerID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=NurseCaseManagerID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.NurseCaseManagerID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('NurseCaseManagerID').value;" type="button" value="Search..."  >&nbsp;&nbsp;</td><td valign=middle><div class="boxDr" ><iframe name="NurseCaseManagerID_IF" frameborder="0" marginheight="0" marginwidth="0" class="boxDrSize"  scrolling="no" src="../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=<%=myEO2.getNIM3_CaseAccount().getNurseCaseManagerID()%>"></iframe></div><input name="NurseCaseManagerID" id="NurseCaseManagerID"  onFocus="this.blur();modalPost('NurseCaseManagerID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=NurseCaseManagerID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.NurseCaseManagerID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('NurseCaseManagerID').value;" type="hidden" value="<%=myEO2.getNIM3_CaseAccount().getNurseCaseManagerID()%>">&nbsp;&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NurseCaseManagerID&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("NurseCaseManagerID")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
</tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("ReferredByContactID",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("ReferredByContactID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("ReferredByContactID",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("ReferredByContactID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(myEO2.getNIM3_CaseAccount().isWrite("ReferredByContactID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                        <td valign=middle>&nbsp;</td>
                        <td valign=middle><p class=<%=theClass%> ><b>Referral Source  &nbsp;</b>&nbsp;<input <%=HTMLFormStyleButton%> name="ReferredByContactID2"  onClick="this.disabled=false;modalPost('ReferredByContactID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=ReferredByContactID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.ReferredByContactID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('ReferredByContactID').value;" type="button" value="Search..."  ></p></td>
                        <td valign=top>
                        <div  class="boxDr">
                        	<iframe name="ReferredByContactID_IF" frameborder="0" marginheight="0" marginwidth="0" class="boxDrSize"  scrolling="no" src="../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=<%=myEO2.getNIM3_Referral().getReferredByContactID()%>" style="display:none;"></iframe>
                        	
                        
                        </div>
                        
                        <input name="ReferredByContactID" id="ReferredByContactID"  onFocus="this.blur();modalPost('ReferredByContactID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=ReferredByContactID','Search...','dialogWidth:800px;dialogHeight:400px','status=yes,scrollbars=yes,resizable=yes,width=800,height=400'));document.ReferredByContactID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('ReferredByContactID').value;" type="hidden" value="<%=myEO2.getNIM3_Referral().getReferredByContactID()%>">
                        &nbsp;&nbsp;
						
						<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferredByContactID&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=myEO2.getNIM3_Referral().getReferralID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Referral().getEnglish("ReferredByContactID")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                        </td>
                        </tr>
                        <%
            }
            %>




            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("CaseAdministratorID",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("CaseAdministratorID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("CaseAdministratorID",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("CaseAdministratorID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("CaseAdministratorID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                        <td valign=middle>&nbsp;</td>
                        
                        <td valign=middle><p class=<%=theClass%> ><b>Case Administrator&nbsp;</b>&nbsp;<input <%=HTMLFormStyleButton%> name="CaseAdministratorID2"  onClick="this.disabled=false;modalPost('CaseAdministratorID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=CaseAdministratorID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.CaseAdministratorID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('CaseAdministratorID').value;" type="button" value="Search..."  ></p></td><td valign=top><div class="boxDr"><iframe name="CaseAdministratorID_IF" frameborder="0" marginheight="0" marginwidth="0" class="boxDrSize" scrolling="no" src="../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=<%=myEO2.getNIM3_CaseAccount().getCaseAdministratorID()%>"></iframe></div><input name="CaseAdministratorID" id="CaseAdministratorID"  onFocus="this.blur();modalPost('CaseAdministratorID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=CaseAdministratorID','Search...','dialogWidth:800px;dialogHeight:400px','status=yes,scrollbars=yes,resizable=yes,width=800,height=400'));document.CaseAdministratorID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('CaseAdministratorID').value;" type="hidden" value="<%=myEO2.getNIM3_CaseAccount().getCaseAdministratorID()%>">&nbsp;&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CaseAdministratorID&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("CaseAdministratorID")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                        </tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("CaseAdministrator2ID",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("CaseAdministrator2ID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("CaseAdministrator2ID",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("CaseAdministrator2ID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("CaseAdministrator2ID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                        <td valign=middle>&nbsp;</td>
                        
                        <td valign=middle><p class=<%=theClass%> ><b>Case Administrator 2&nbsp;</b>&nbsp;<input <%=HTMLFormStyleButton%> name="CaseAdministrator2ID2"  onClick="this.disabled=false;modalPost('CaseAdministrator2ID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=CaseAdministrator2ID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.CaseAdministrator2ID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('CaseAdministrator2ID').value;" type="button" value="Search..."  ></p></td><td valign=top><div class="boxDr"><iframe name="CaseAdministrator2ID_IF" frameborder="0" marginheight="0" marginwidth="0" class="boxDrSize" scrolling="no" src="../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=<%=myEO2.getNIM3_CaseAccount().getCaseAdministrator2ID()%>"></iframe></div><input name="CaseAdministrator2ID" id="CaseAdministrator2ID"  onFocus="this.blur();modalPost('CaseAdministrator2ID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=CaseAdministrator2ID','Search...','dialogWidth:800px;dialogHeight:400px','status=yes,scrollbars=yes,resizable=yes,width=800,height=400'));document.CaseAdministrator2ID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('CaseAdministrator2ID').value;" type="hidden" value="<%=myEO2.getNIM3_CaseAccount().getCaseAdministrator2ID()%>">&nbsp;&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CaseAdministrator2ID&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("CaseAdministrator2ID")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                        </tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("CaseAdministrator3ID",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("CaseAdministrator3ID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("CaseAdministrator3ID",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("CaseAdministrator3ID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("CaseAdministrator3ID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                        <td valign=middle>&nbsp;</td>
                        
                        <td valign=middle><p class=<%=theClass%> ><b>Case Administrator 3&nbsp;</b>&nbsp;<input <%=HTMLFormStyleButton%> name="CaseAdministrator3ID2"  onClick="this.disabled=false;modalPost('CaseAdministrator3ID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=CaseAdministrator3ID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.CaseAdministrator3ID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('CaseAdministrator3ID').value;" type="button" value="Search..."  ></p></td><td valign=top><div class="boxDr"><iframe name="CaseAdministrator3ID_IF" frameborder="0" marginheight="0" marginwidth="0" class="boxDrSize" scrolling="no" src="../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=<%=myEO2.getNIM3_CaseAccount().getCaseAdministrator3ID()%>"></iframe></div><input name="CaseAdministrator3ID" id="CaseAdministrator3ID"  onFocus="this.blur();modalPost('CaseAdministrator3ID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=CaseAdministrator3ID','Search...','dialogWidth:800px;dialogHeight:400px','status=yes,scrollbars=yes,resizable=yes,width=800,height=400'));document.CaseAdministrator3ID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('CaseAdministrator3ID').value;" type="hidden" value="<%=myEO2.getNIM3_CaseAccount().getCaseAdministrator3ID()%>">&nbsp;&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CaseAdministrator3ID&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("CaseAdministrator3ID")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                        </tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("CaseAdministrator4ID",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("CaseAdministrator4ID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("CaseAdministrator4ID",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("CaseAdministrator4ID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("CaseAdministrator4ID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                        <td valign=middle>&nbsp;</td>
                        
                        <td valign=middle><p class=<%=theClass%> ><b>Case Administrator 4&nbsp;</b>&nbsp;<input <%=HTMLFormStyleButton%> name="CaseAdministrator4ID2"  onClick="this.disabled=false;modalPost('CaseAdministrator4ID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=CaseAdministrator4ID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.CaseAdministrator4ID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('CaseAdministrator4ID').value;" type="button" value="Search..."  ></p></td><td valign=top><div class="boxDr"><iframe name="CaseAdministrator4ID_IF" frameborder="0" marginheight="0" marginwidth="0" class="boxDrSize" scrolling="no" src="../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=<%=myEO2.getNIM3_CaseAccount().getCaseAdministrator4ID()%>"></iframe></div><input name="CaseAdministrator4ID" id="CaseAdministrator4ID"  onFocus="this.blur();modalPost('CaseAdministrator4ID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=CaseAdministrator4ID','Search...','dialogWidth:800px;dialogHeight:400px','status=yes,scrollbars=yes,resizable=yes,width=800,height=400'));document.CaseAdministrator4ID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('CaseAdministrator4ID').value;" type="hidden" value="<%=myEO2.getNIM3_CaseAccount().getCaseAdministrator4ID()%>">&nbsp;&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CaseAdministrator4ID&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("CaseAdministrator4ID")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                        </tr>
                        <%
            }
            %>


        <tr style="display:none">
          <td valign=middle colspan="3" class="tdHeader">Additional Links (Will not receive alerts):</td>
        </tr>





            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("BillToContactID",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("BillToContactID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("BillToContactID",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("BillToContactID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("BillToContactID",UserSecurityGroupID)))
            {
                        %>
                          
                        <tr style="display:none">
                        <td valign=middle>&nbsp;</td>
                        <td valign=middle nowrap><p class=<%=theClass%> ><b>Bill To&nbsp;</b><input <%=HTMLFormStyleButton%> name="BillToContactID2"  onClick="this.disabled=false;modalPost('BillToContactID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=BillToContactID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.BillToContactID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('BillToContactID').value;" type="button" value="Search..."  ></p></td><td valign=top><div class="boxDr"><iframe name="BillToContactID_IF" frameborder="0" marginheight="0" marginwidth="0" class="boxDrSize"  scrolling="no" src="../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=<%=myEO2.getNIM3_CaseAccount().getBillToContactID()%>"></iframe></div><input name="BillToContactID" id="BillToContactID"  onFocus="this.blur();modalPost('BillToContactID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=BillToContactID','Search...','dialogWidth:800px;dialogHeight:400px','status=yes,scrollbars=yes,resizable=yes,width=800,height=400'));document.BillToContactID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('BillToContactID').value;" type="hidden" value="<%=myEO2.getNIM3_CaseAccount().getBillToContactID()%>">&nbsp;&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillToContactID&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("BillToContactID")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                        </tr>
                        <%
            }
            %>

            <%
            if ( (myEO2.getNIM3_CaseAccount().isRequired("URContactID",UserSecurityGroupID))&&(!myEO2.getNIM3_CaseAccount().isComplete("URContactID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_CaseAccount().isExpired("URContactID",expiredDays))&&(myEO2.getNIM3_CaseAccount().isExpiredCheck("URContactID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_CaseAccount().isWrite("URContactID",UserSecurityGroupID)))
            {
                        %>
                        <tr style="display:none">
                        <td valign=middle>&nbsp;</td>
                        <td valign=middle><p class=<%=theClass%> ><b>UR Contact&nbsp;</b>&nbsp;<input <%=HTMLFormStyleButton%> name="URContactID2"  onClick="this.disabled=false;modalPost('URContactID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=URContactID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.URContactID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('URContactID').value;" type="button" value="Search..."  ></td><td valign=top><p><div  class="boxDr"><iframe name="URContactID_IF" frameborder="0" marginheight="0" marginwidth="0" class="boxDrSize"  scrolling="no" src="../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=<%=myEO2.getNIM3_CaseAccount().getURContactID()%>"></iframe></div><input name="URContactID" id="URContactID"  onFocus="this.blur();modalPost('URContactID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=URContactID','Search...','dialogWidth:800px;dialogHeight:400px','status=yes,scrollbars=yes,resizable=yes,width=800,height=400'));document.URContactID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('URContactID').value;" type="hidden" value="<%=myEO2.getNIM3_CaseAccount().getURContactID()%>">&nbsp;&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=URContactID&amp;sTableName=tNIM3_CaseAccount&amp;sRefID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_CaseAccount().getEnglish("URContactID")%>&amp;sTableNameDisp=tNIM3_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                        </tr>
                        <%
            }
            %>


                     <tr>
                       <td colspan=3 class=tdHeaderAlt><hr>Referral Information</td>
                     </tr>

<%//Start Referral Info%>






            <%
            if ( (myEO2.getNIM3_Referral().isRequired("ReferralTypeID",UserSecurityGroupID))&&(!myEO2.getNIM3_Referral().isComplete("ReferralTypeID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Referral().isExpired("ReferralTypeID",expiredDays))&&(myEO2.getNIM3_Referral().isExpiredCheck("ReferralTypeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
            if (false&&(myEO2.getNIM3_Referral().isRead("ReferralTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                          <td valign=middle >&nbsp;</td>
                          <td valign=middle ><p class=<%=theClass%> ><b>Type&nbsp;</b></p></td><td valign=middle ><p><jsp:include page="../generic/tReferralTypeLI_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Referral().getReferralTypeID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferralTypeID&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=myEO2.getNIM3_Referral().getReferralID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Referral().getEnglish("ReferralTypeID")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_Referral().isRequired("ReferralStatusID",UserSecurityGroupID))&&(!myEO2.getNIM3_Referral().isComplete("ReferralStatusID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Referral().isExpired("ReferralStatusID",expiredDays))&&(myEO2.getNIM3_Referral().isExpiredCheck("ReferralStatusID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Referral().isWrite("ReferralStatusID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                          <td valign=middle >&nbsp;</td>
                          
                          <td valign=middle ><p class=<%=theClass%> ><b>Is this Referral Approved?&nbsp;</b></p></td><td valign=middle ><p><select   name="ReferralStatusID" id= "ReferralStatusID" ><jsp:include page="../generic/tReferralStatusLI.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Referral().getReferralStatusID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferralStatusID&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=myEO2.getNIM3_Referral().getReferralID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Referral().getEnglish("ReferralStatusID")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a></p></td>
                          <!-- 
                          <td valign=middle ><p class=< %=theClass%> ><b>Is this Referral Approved?&nbsp;</b></p></td><td valign=middle ><p><select   name="ReferralStatusID" id= "ReferralStatusID" ><jsp:include page="../generic/tReferralStatusLI.jsp" flush="true" ><jsp:param name="CurrentSelection" value="< %=myEO2.getAutoApproveReferral()%>" /></jsp:include></select>&nbsp;< %if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferralStatusID&amp;sTableName=tNIM3_Referral&amp;sRefID=< %=myEO2.getNIM3_Referral().getReferralID()%>&amp;sFieldNameDisp=< %=myEO2.getNIM3_Referral().getEnglish("ReferralStatusID")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                          -->
              </tr>
                        <%
            }
            %>


          <%
            if ( (myEO2.getNIM3_Referral().isRequired("ReferralMethod",UserSecurityGroupID))&&(!myEO2.getNIM3_Referral().isComplete("ReferralMethod")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Referral().isExpired("ReferralMethod",expiredDays))&&(myEO2.getNIM3_Referral().isExpiredCheck("ReferralMethod",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Referral().isWrite("ReferralMethod",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                          <td valign=middle >&nbsp;</td>
                     <td valign=top><p class=<%=theClass%> ><b>Referral Method&nbsp;</b></p></td><td valign=top><p><select   name="ReferralMethod" id= "ReferralMethod" ><jsp:include page="../generic/tReferralMethod_String.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Referral().getReferralMethod()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferralMethod&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=myEO2.getNIM3_Referral().getReferralID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Referral().getEnglish("ReferralMethod")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((myEO2.getNIM3_Referral().isRead("ReferralMethod",UserSecurityGroupID)))
            {
                        %>
                         <tr>
                          <td valign=middle >&nbsp;</td>
                          <td valign=top><p class=<%=theClass%> ><b>Referral Method&nbsp;</b></p></td><td valign=top><p><%=myEO2.getNIM3_Referral().getReferralMethod()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferralMethod&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=myEO2.getNIM3_Referral().getReferralID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Referral().getEnglish("ReferralMethod")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>





            <%
            if ( (myEO2.getNIM3_Referral().isRequired("ReferringPhysicianID",UserSecurityGroupID))&&(!myEO2.getNIM3_Referral().isComplete("ReferringPhysicianID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Referral().isExpired("ReferringPhysicianID",expiredDays))&&(myEO2.getNIM3_Referral().isExpiredCheck("ReferringPhysicianID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Referral().isWrite("ReferringPhysicianID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                          <td valign=middle nowrap="nowrap">&nbsp;</td>
                          <td valign=middle nowrap="nowrap"><Span class=<%=theClass%> ><b>Referring Provider</b></Span>
                        
<input name="ReferringPhysicianID2" type="button" class="inputButton_md_Action1"  onclick="this.disabled=false;modalPost('ReferringPhysicianID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=ReferringPhysicianID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.ReferringPhysicianID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('ReferringPhysicianID').value;" value="Search..." <%=HTMLFormStyleButton%> />



                </td><td valign=middle ><div class=boxDr><iframe name="ReferringPhysicianID_IF" frameborder="0" marginheight="0" marginwidth="0" class=boxDrSize scrolling="no" src="../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=<%=myEO2.getNIM3_Referral().getReferringPhysicianID()%>"></iframe></div><input name="ReferringPhysicianID" id="ReferringPhysicianID"  onFocus="this.blur();modalPost('ReferringPhysicianID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=ReferringPhysicianID','Search...','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));top.ReferringPhysicianID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('ReferringPhysicianID').value;" type="hidden" value="<%=myEO2.getNIM3_Referral().getReferringPhysicianID()%>">&nbsp;&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferringPhysicianID&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=myEO2.getNIM3_Referral().getReferralID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Referral().getEnglish("ReferringPhysicianID")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_Referral().isRequired("ReferredByContactID",UserSecurityGroupID))&&(!myEO2.getNIM3_Referral().isComplete("ReferredByContactID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Referral().isExpired("ReferredByContactID",expiredDays))&&(myEO2.getNIM3_Referral().isExpiredCheck("ReferredByContactID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Referral().isWrite("ReferredByContactID",UserSecurityGroupID)))
            {
				String class_refsource = "inputButton_md_Action1";
				if (myEO2.getNIM3_Referral().getReferredByContactID()>0)
				{
					class_refsource = "inputButton_sm_Default";
				}
				
                        %>
                        <tr>
                        <td valign=middle>&nbsp;</td>
                        <td valign=middle><p class=<%=theClass%> ><b>Referral Source &nbsp;</b>&nbsp;<input <%=HTMLFormStyleButton%> name="ReferredByContactID2"  onClick="this.disabled=false;modalPost('ReferredByContactID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=ReferredByContactID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.ReferredByContactID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('ReferredByContactID').value;" type="button" value="Search..."  >
                          <br>
                          <input class="<%=class_refsource%>" name="ReferredByContactID_quick1"  onClick="referralSourceOverride('reg');document.getElementById('ReferredByContactID').value=document.getElementById('AdjusterID').value;document.ReferredByContactID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('ReferredByContactID').value;" type="button" value="Adj"  >
                          &nbsp;<input class="<%=class_refsource%>" name="ReferredByContactID_quick2"  onClick="referralSourceOverride('reg');document.getElementById('ReferredByContactID').value=document.getElementById('NurseCaseManagerID').value;document.ReferredByContactID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('ReferredByContactID').value;" type="button" value="NCM"  >
                          &nbsp;<input class="<%=class_refsource%>" name="ReferredByContactID_quick3"  onClick="referralSourceOverride('reg');document.getElementById('ReferredByContactID').value=document.getElementById('ReferringPhysicianID').value;document.ReferredByContactID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('ReferredByContactID').value;" type="button" value="RefDr"  >
                         
                          <br>
                        </p></td><td valign=top>
                        
                        <div  class="boxDr">
                            <iframe name="ReferredByContactID_IF" id="ReferredByContactID_IF" frameborder="0" marginheight="0" marginwidth="0" class="boxDrSize"  scrolling="no" src="../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=<%=myEO2.getNIM3_Referral().getReferredByContactID()%>"> </iframe>
                            
                        
                        </div>
                        <input name="ReferredByContactID" id="ReferredByContactID"  onFocus="this.blur();modalPost('ReferredByContactID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=ReferredByContactID','Search...','dialogWidth:800px;dialogHeight:400px','status=yes,scrollbars=yes,resizable=yes,width=800,height=400'));document.ReferredByContactID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('ReferredByContactID').value;" type="hidden" value="<%=myEO2.getNIM3_Referral().getReferredByContactID()%>">&nbsp;&nbsp;
						
						<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferredByContactID&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=myEO2.getNIM3_Referral().getReferralID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Referral().getEnglish("ReferredByContactID")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                        </tr>
                        <%
            }
            %>





            <%
            if ( (myEO2.getNIM3_Referral().isRequired("AttendingPhysicianID",UserSecurityGroupID))&&(!myEO2.getNIM3_Referral().isComplete("AttendingPhysicianID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Referral().isExpired("AttendingPhysicianID",expiredDays))&&(myEO2.getNIM3_Referral().isExpiredCheck("AttendingPhysicianID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(myEO2.getNIM3_Referral().isWrite("AttendingPhysicianID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                          <td valign=middle nowrap="nowrap">&nbsp;</td>
        <td valign=middle nowrap="nowrap"><b>Referred To</b>
            <strong>Provider</strong><br />
<input <%=HTMLFormStyleButton%> name="AttendingPhysicianID2"  onclick="this.disabled=false;modalPost('AttendingPhysicianID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=AttendingPhysicianID','Search...','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));top.AttendingPhysicianID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('AttendingPhysicianID').value;" type="button" value="Search..." />
    </td><td valign=middle ><div class=boxDr><iframe name="AttendingPhysicianID_IF" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" src="../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=<%=myEO2.getNIM3_Referral().getAttendingPhysicianID()%>"></iframe></div><input name="AttendingPhysicianID" id="AttendingPhysicianID"  onFocus="this.blur();modalPost('AttendingPhysicianID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=AttendingPhysicianID','Search...','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));top.AttendingPhysicianID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('AttendingPhysicianID').value;" type="hidden" value="<%=myEO2.getNIM3_Referral().getAttendingPhysicianID()%>">&nbsp;&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AttendingPhysicianID&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=myEO2.getNIM3_Referral().getReferralID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Referral().getEnglish("AttendingPhysicianID")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
    </tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_Referral().isRequired("PreAuthorizationConfirmation",UserSecurityGroupID))&&(!myEO2.getNIM3_Referral().isComplete("PreAuthorizationConfirmation")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Referral().isExpired("PreAuthorizationConfirmation",expiredDays))&&(myEO2.getNIM3_Referral().isExpiredCheck("PreAuthorizationConfirmation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Referral().isWrite("PreAuthorizationConfirmation",UserSecurityGroupID)))
            {
                        %>
                     <tr style="display:none">
                       <td valign=middle >&nbsp;</td>
                     <td valign=middle ><p class=<%=theClass%> ><b>PreAuth&nbsp;</b> #</p></td><td valign=middle ><p><input maxlength="100" type=text size="40" name="PreAuthorizationConfirmation" value="<%=myEO2.getNIM3_Referral().getPreAuthorizationConfirmation()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PreAuthorizationConfirmation&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=myEO2.getNIM3_Referral().getReferralID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Referral().getEnglish("PreAuthorizationConfirmation")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                     </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_Referral().isRequired("ReceiveDate",UserSecurityGroupID))&&(!myEO2.getNIM3_Referral().isComplete("ReceiveDate")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Referral().isExpired("ReceiveDate",expiredDays))&&(myEO2.getNIM3_Referral().isExpiredCheck("ReceiveDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO2.getNIM3_Referral().isWrite("ReceiveDate",UserSecurityGroupID)))
            {

//      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
	String dNAD = "t";
	if (myEO2.getNIM3_Referral().getReceiveDate().before(NIMUtils.getBeforeTime()))
	{
		
	}
	else
	{
		dNAD = displayDateTimeSDF.format(myEO2.getNIM3_Referral().getReceiveDate());
	}

%>

              <tr>
                          <td valign=top>&nbsp;</td>
                          <td valign=top nowrap="nowrap"><b>Receive Date:&nbsp;</b></td><td valign=top><p>
                            <input maxlength=20  type=text size="60" name="ReceiveDate" id="ReceiveDate"   value="<%=dNAD%>"   >   
                            
                            <script type="text/javascript">
    new ATWidget( "ReceiveDate", { format: "%m/%d/%Y %h:%i %p" } );
                           </script>
                            
                            &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReceiveDate&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=myEO2.getNIM3_Referral().getReferralID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Referral().getEnglish("ReceiveDate")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                            
                          </td>
              </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_Referral().isRequired("ReferralDate",UserSecurityGroupID))&&(!myEO2.getNIM3_Referral().isComplete("ReferralDate")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Referral().isExpired("ReferralDate",expiredDays))&&(myEO2.getNIM3_Referral().isExpiredCheck("ReferralDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO2.getNIM3_Referral().isWrite("ReferralDate",UserSecurityGroupID)))
            {
                        %>
                       <tr style="display:none">
                          <td valign=middle nowrap="nowrap">&nbsp;</td>
                          <td valign=middle nowrap="nowrap"><p class=<%=theClass%> ><b>Original Dr Referral Date<br />
(mm/dd/yyyy hh:mm am/pm):&nbsp;</b></p></td><td valign=middle ><p><input maxlength=20  type=text size="40" name="ReferralDate" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Referral().getReferralDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferralDate&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=myEO2.getNIM3_Referral().getReferralID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Referral().getEnglish("ReferralDate")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
</tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_Referral().isRequired("Comments",UserSecurityGroupID))&&(!myEO2.getNIM3_Referral().isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Referral().isExpired("Comments",expiredDays))&&(myEO2.getNIM3_Referral().isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Referral().isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                         <td valign=middle >&nbsp;</td>
                         <td valign=middle ><p class=<%=theClass%> ><b>Referral Notes&nbsp;</b></p>
                         <p class=<%=theClass%> ></p></td><td valign=middle ><p><textarea onKeyDown="textAreaStop(this,2500)" id="tReferral_Comments" rows="8" name="tReferral_Comments" cols="40"><%=myEO2.getNIM3_Referral().getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Referral&amp;sRefID=<%=myEO2.getNIM3_Referral().getReferralID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Referral().getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Referral','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                       </tr>
                        <%
            }
            %>


<%//End Referral Info%>






<%//start tEncounterService%>

                     <tr>
                       <td colspan=3 class=tdHeaderAlt><hr>Encounter Information</td>
                     </tr>



            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("EncounterTypeID",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("EncounterTypeID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("EncounterTypeID",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("EncounterTypeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("EncounterTypeID",UserSecurityGroupID)))
            {
                        %>
                        <tr>                       <td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Encounter Type (Primary)&nbsp;</b></p></td><td valign=top><p><select   name="EncounterTypeID" ><jsp:include page="../generic/tEncounterTypeLI.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getEncounterTypeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EncounterTypeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("EncounterTypeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("EncounterStatusID",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("EncounterStatusID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("EncounterStatusID",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("EncounterStatusID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("EncounterStatusID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                        
                       <td valign=middle>&nbsp;</td>
                        
                        <td valign=top><p class=<%=theClass%> ><b>Encounter Status&nbsp;</b></p></td><td valign=top><p><%if(isScheduler4){%><select   name="EncounterStatusID" ><jsp:include page="../generic/tEncounterStatusLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getEncounterStatusID()%>" /></jsp:include></select><%}else{%><jsp:include page="../generic/tEncounterStatusLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getEncounterStatusID()%>" /></jsp:include><%}%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EncounterStatusID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("EncounterStatusID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>




            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("VoidLeakReasonID",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("VoidLeakReasonID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("VoidLeakReasonID",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("VoidLeakReasonID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("VoidLeakReasonID",UserSecurityGroupID)))
            {
                        %>
                        <tr>
                        
                       <td valign=middle>&nbsp;</td>
                        
                        <td valign=top><p class=<%=theClass%> ><b>&nbsp;&nbsp;&nbsp;Void Reason:&nbsp;</b></p></td><td valign=top><p>&nbsp;&nbsp;&nbsp;<%if(isScheduler2){%><select   name="VoidLeakReasonID" ><jsp:include page="../generic/tVoidLeakReasonLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getVoidLeakReasonID()%>" /></jsp:include></select><%}else{%><jsp:include page="../generic/tVoidLeakReasonLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getVoidLeakReasonID()%>" /></jsp:include><%}%> &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=VoidLeakReasonID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("VoidLeakReasonID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>


<tr><td colspan="3"><hr></td></tr>


            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("isCourtesy",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("isCourtesy")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("isCourtesy",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("isCourtesy",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (isScheduler4&&(myEO2.getNIM3_Encounter().isWrite("isCourtesy",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>is Courtesy Schedule&nbsp;</b></p></td><td valign=top><p><select   name="isCourtesy" ><jsp:include page="../generic/tCourtesyLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getisCourtesy()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isCourtesy&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("isCourtesy")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            else 
            {
                        %>
                        <tr><td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>is Courtesy Schedule&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCourtesyLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getisCourtesy()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isCourtesy&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("isCourtesy")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;&nbsp;&nbsp;* If you need to change this status, please use the Problem Wizard.</p></td>
              </tr>
                        <%
            }
            %>




            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("Pricing_Structure",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("Pricing_Structure")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("Pricing_Structure",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("Pricing_Structure",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (isScheduler4&&(myEO2.getNIM3_Encounter().isWrite("Pricing_Structure",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Pricing_Structure&nbsp;</b></p></td><td valign=top><p><input maxlength="2" type=text size="4" name="Pricing_Structure" value="<%=myEO2.getNIM3_Encounter().getPricing_Structure()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Pricing_Structure&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Pricing_Structure")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((myEO2.getNIM3_Encounter().isRead("Pricing_Structure",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Pricing_Structure&nbsp;</b></p></td><td valign=top><p><%=myEO2.getNIM3_Encounter().getPricing_Structure()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Pricing_Structure&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Pricing_Structure")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>





            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("isSTAT",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("isSTAT")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("isSTAT",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("isSTAT",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("isSTAT",UserSecurityGroupID)))
            {
                        %>
                        <tr> <td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>is STAT?&nbsp;</b></p></td><td valign=top><p><select   name="isSTAT" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getisSTAT()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isSTAT&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("isSTAT")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            
            
            if ( (myEO2.getNIM3_Encounter().isRequired("isVIP",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("isVIP")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("isVIP",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("isVIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("isVIP",UserSecurityGroupID)))
            {
                        %>
                        <tr> <td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>is VIP?&nbsp;</b></p></td><td valign=top><p><select   name="isVIP" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getisVIP()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isVIP&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("isVIP")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            
            

			if ( (myEO2.getNIM3_Encounter().isRequired("isRetro",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("isRetro")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("isRetro",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("isRetro",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("isRetro",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>is Retro Auth&nbsp;</b></p></td><td valign=top><p><%if(isScheduler3){%><select   name="isRetro" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getisRetro()%>" /></jsp:include></select><%}else{%><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getisRetro()%>" /></jsp:include><%}%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=isRetro&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("isRetro")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("RequiresOnlineImage",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("RequiresOnlineImage")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("RequiresOnlineImage",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("RequiresOnlineImage",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("RequiresOnlineImage",UserSecurityGroupID)))
            {
                        %>
                        <tr> <td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Requires Online Image&nbsp;</b></p></td><td valign=top><p><select   name="RequiresOnlineImage" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getRequiresOnlineImage()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=RequiresOnlineImage&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("RequiresOnlineImage")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
			%>



            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("RequiresFilms",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("RequiresFilms")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("RequiresFilms",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("RequiresFilms",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("RequiresFilms",UserSecurityGroupID)))
            {
                        %>
                        <tr> <td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Requires Films?&nbsp;</b></p></td><td valign=top><p><select   name="RequiresFilms" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getRequiresFilms()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=RequiresFilms&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("RequiresFilms")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>
            
            
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("RequiresHandCarryCD",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Requires Hand Carry CD&nbsp;</b></p></td><td valign=top><p><select   name="RequiresHandCarryCD" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getRequiresHandCarryCD()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=RequiresHandCarryCD&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("RequiresHandCarryCD")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((myEO2.getNIM3_Encounter().isRead("RequiresHandCarryCD",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>RequiresHandCarryCD&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getRequiresHandCarryCD()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=RequiresHandCarryCD&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("RequiresHandCarryCD")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

            


            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("HasBeenRescheduled",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("HasBeenRescheduled")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("HasBeenRescheduled",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("HasBeenRescheduled",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (isScheduler3&&(myEO2.getNIM3_Encounter().isWrite("HasBeenRescheduled",UserSecurityGroupID)))
            {
                        %>
                        <tr> <td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Has Been Rescheduled?&nbsp;</b></p></td><td valign=top><p><%if(isScheduler3){%><select   name="HasBeenRescheduled" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getHasBeenRescheduled()%>" /></jsp:include></select><%}else{%><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getHasBeenRescheduled()%>" /></jsp:include><%}%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=HasBeenRescheduled&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("HasBeenRescheduled")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("RequiresAgeInjury",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("RequiresAgeInjury")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("RequiresAgeInjury",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("RequiresAgeInjury",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("RequiresAgeInjury",UserSecurityGroupID)))
            {
                        %>
                        <tr> <td valign=middle>&nbsp;</td><td valign=top><p class=<%=theClass%> ><b>Requires Injury Aging?&nbsp;</b></p></td><td valign=top><p><select   name="RequiresAgeInjury" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getNIM3_Encounter().getRequiresAgeInjury()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=RequiresAgeInjury&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("RequiresAgeInjury")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
              </tr>
                        <%
            }
            %>






            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("ScanPass",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("ScanPass")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("ScanPass",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("ScanPass",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
			if ((myEO2.getNIM3_Encounter().isRead("ScanPass",UserSecurityGroupID)))
            {
                        %>
                         <tr>
                           <td valign=middle>&nbsp;</td>
                         <td valign=middle><p class=<%=theClass%> ><b>ScanPass&nbsp;</b></p></td><td valign=middle><p><%=myEO2.getNIM3_Encounter().getScanPass()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ScanPass&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("ScanPass")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                         </tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("DateOfService",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("DateOfService")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("DateOfService",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("DateOfService",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO2.getNIM3_Encounter().isWrite("DateOfService",UserSecurityGroupID)))
            {
                        %><tr><td colspan="3"><hr></td></tr>
                        <tr>
                          <td valign=middle nowrap="nowrap">&nbsp;</td>
                          <td valign=middle nowrap="nowrap"><p class=<%=theClass%> ><b>Date Of Service<br />
        (mm/dd/yyyy):&nbsp;</b></p></td><td valign=middle><p><input maxlength=20  type=text size="30" name="DateOfService" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getDateOfService())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfService&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("DateOfService")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
        </tr>
                        <%
            }
            %>

            <%
            //if ( (myEO2.getNIM3_Encounter().issetMessageNameRequired("PatientAvailability",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("PatientAvailability")) )
            if ( false&&(!myEO2.getNIM3_Encounter().isComplete("PatientAvailability")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("PatientAvailability",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("PatientAvailability",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("PatientAvailability",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                         <td valign=middle>&nbsp;</td>
                       <td valign=top><p class=<%=theClass%> ><b><%=myEO2.getNIM3_Encounter().getEnglish("PatientAvailability")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="PatientAvailability" cols="40" maxlength=200><%=myEO2.getNIM3_Encounter().getPatientAvailability()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientAvailability&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("PatientAvailability")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((myEO2.getNIM3_Encounter().isRead("PatientAvailability",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                         <td valign=middle>&nbsp;</td>
                       <td valign=top> <p class=<%=theClass%> ><b><%=myEO2.getNIM3_Encounter().getEnglish("PatientAvailability")%>&nbsp;</b></p></td><td valign=top><p><%=myEO2.getNIM3_Encounter().getPatientAvailability()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientAvailability&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("PatientAvailability")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



       <%
            if ( (myEO2.getNIM3_Encounter().isRequired("Comments",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("Comments",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr>
                         <td valign=middle>&nbsp;</td>
                       <td valign=middle><p class=<%=theClass%> ><b>ScanPass Comments:&nbsp;</b></p></td><td valign=middle><p><textarea onKeyDown="textAreaStop(this,2000)" rows="3" name="tEncounter_Comments" cols="40"><%=myEO2.getNIM3_Encounter().getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                       </tr>
                        <%
            }
            %>



            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td><td>&nbsp;</td>
              </tr>
<tr><td colspan="3">

        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tNIM3_CaseAccount")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tNIM3_CaseAccount")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
              <select name="SubmitType" id="SubmitType">
              <option value="0">Save & Return to Dashboard</option>
              <option value="1" selected>Save & Edit Services</option>
              </select>
            <p><input  class="inputButton_md_Create" onClick = "MM_validateForm('CaseClaimNumber','','R');if (document.MM_returnValue){document.getElementById('SubmitType').value=0;this.disabled=true;submit();return false}else{this.disabled=false;return false}" type=Submit value="Save & Return" name=Submit>  
            &nbsp;&nbsp;&nbsp;
<input  class="inputButton_md_Create" onClick = "MM_validateForm('CaseClaimNumber','','R');if (document.MM_returnValue){document.getElementById('SubmitType').value=1;this.disabled=true;submit();return false}else{this.disabled=false;return false}" type=Submit value="Save & Edit Services" name=Submit>     

<%//show_pws_Saving() removed from javascript     [document.body.scrollTop=1;$('table').fadeOut('slow');show_pws_Saving();submit();return false]%>         


            &nbsp;&nbsp;&nbsp;&nbsp;
            
            
            <%if (sQUICKVIEW.equalsIgnoreCase("yes")){%><input class="inputButton_md_Default" onClick = "this.disabled=true;window.close();" type=button value="Close" name=Submit2><%}%>  </p>
        <%}%>
        </td></tr></table>
        </form>

        <hr>
        Audit Notes:<br>
        <textarea name="" cols="60" rows="5" readonly><%=myEO2.getNIM3_CaseAccount().getAuditNotes()%></textarea>



    </td>
           <td valign="top" class=tableColor><table width="200" border="0" cellspacing="0" cellpadding="5">

             <tr>
               <td <%if (!myEO2.getCaseDashboard_CombinedNotes("<hr>").equalsIgnoreCase("")){%>class="tdBaseAlt_Action2"<%}%> ><%=myEO2.getCaseDashboard_CombinedNotes("<hr>")%></td>
             </tr>
           </table></td>
         </tr>
      </table>


        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

<script language="javascript">
//hide_pws_Loading();  //conflicts with receive date
</script>
