<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%
String MREF = "";
boolean isPop = false;
String addWhere = "";
if ( request.getParameter( "search1" ) != null&&request.getParameter( "value1" )!=null&&!request.getParameter( "search1" ).equalsIgnoreCase("")&&!request.getParameter( "value1" ).equalsIgnoreCase("") )
{
	addWhere = " AND \"public\".tadminpracticelu." + request.getParameter( "search1" ) +  " = '" + request.getParameter( "value1" ) + "' "; 
}

if ( request.getParameter( "MREF" ) != null&&!request.getParameter( "MREF" ).equalsIgnoreCase("") )
{
	isPop = true;
	MREF        =    request.getParameter ("MREF");
	if (new Integer(MREF).intValue()<300)
	{
		MREF="300";
	}
	%>
	<meta http-equiv="refresh" content="<%=MREF%>">
    <%
}
else
{
	MREF = "";
}
%> 

<title>NIM3 Start</title>
<style type="text/css">
<!--
th 
{
	font-family: Verdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #FFF;
	background-color: #BDD2E7;
	padding: 6px;
	text-decoration:none !important;
}

  .normal { background-color: #FFFFFF }
  .tdAlt { background-color: #CCCCCC }
  .highlight { background-color: #8888FF }	
</style>

<!--
td 
{
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
}

.tdAlt 
{
	background:#EEE;
}

-->
</style>
</head>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_report.jsp?plcID="+thePLCID;
if (isPop)
{
	tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
}
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td>
<%
java.util.Vector myList = new java.util.Vector();
com.winstaff.AutoReportObject myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("Volume For Current Month & Past Month");
//old 2/4/13 myARO.setsSQL("SELECT  Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"CaseID\") as Volume, to_char(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate\",'Dy') ||  ' - ' || \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" AS dDay  FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\"  where    (\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" = case when (date_part ('month',timezone('PST'::text, now()) ))  = 1 then (date_part ('year',timezone('PST'::text, now()) )-1) END  and  \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" = case when (date_part ('month',timezone('PST'::text, now()) ))  = 1 then 12 END )   or   (\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\"=(date_part ('year',timezone('PST'::text, now()) ))  AND  (\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\"=(date_part ('month',timezone('PST'::text, now()) )-1) OR  \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\"=(date_part ('month',timezone('PST'::text, now()) )))  )     GROUP BY \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\",dDay  order by \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\"");
myARO.setsSQL("SELECT  Count(\"public\".\"vMQ4_Encounter_Void_BP\".\"EncounterID\") as Volume,  to_char(\"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate\",'Dy') ||  ' - ' || \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iDay\" AS dDay   FROM \"public\".\"vMQ4_Encounter_Void_BP\"   where    (\"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iYear\" =  case  	when (date_part ('month',timezone('PST'::text, now()) ))  = 1  	then (date_part ('year',timezone('PST'::text, now()) )-1)  END  and   \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\" = case when (date_part ('month',timezone('PST'::text, now()) ))  = 1 then 12 END )   or    ( 	\"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iYear\"=(date_part ('year',timezone('PST'::text, now()) ))  AND   	( 		\"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\"=(date_part ('month',timezone('PST'::text, now()) )-1) OR   		\"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\"=(date_part ('month',timezone('PST'::text, now()) )) 	)   ) GROUP BY \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iDay\",dDay   order by \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iDay\"");


myARO.setDisplayChart(true);
myARO.setHideLabel(false);
myARO.setAutoReportCode("volume1");
myARO.setChartLabelField("dDay");
myARO.setChartDataField("Volume");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_COLCHART);
myList.add(myARO);



myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("NIMCRM: Activity Report (All-Time)");
myARO.setsSQL("SELECT \"public\".tuseraccount.contactfirstname || ' ' || \"public\".tuseraccount.contactlastname as \"Create_FullName\", (CASE when commtypeid in (12,14, 16) THEN 'L2' ELSE 'L1' END) as \"Interaction_Level\", Count(\"public\".tnim3_commtrack.link_userid), \"public\".tuseraccount.userid FROM \"public\".tuseraccount  INNER JOIN \"public\".tnim3_commtrack ON \"public\".tnim3_commtrack.intuserid = \"public\".tuseraccount.userid where commtypeid >=10 and userid<>163 and status=2 GROUP BY \"public\".tuseraccount.contactfirstname, \"public\".tuseraccount.contactlastname, \"public\".tuseraccount.userid, \"Interaction_Level\"  order by \"Create_FullName\", \"Interaction_Level\"");
myARO.setDisplayChart(false);
myARO.setHideLabel(false);
myARO.setAutoReportCode("nimcrm_activity_alltime");
myARO.setChartLabelField("iDay");
myARO.setChartDataField("Volume");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_COLCHART);
myList.add(myARO);



myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("Volume By Day By Region (Last/Current Month))");
myARO.setsSQL("SELECT\n" +
"	to_char(\n" +
"		\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate\",\n" +
"		'Dy'\n" +
"	)|| ' - ' || \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" AS dday,\n" +
"	\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_SalesDivision\" AS \"Region\",\n" +
"	COUNT(\n" +
"		\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"CaseID\"\n" +
"	)AS volume\n" +
"FROM\n" +
"	\"public\".\"vMQ4_Encounter_NoVoid_BP\"\n" +
"WHERE\n" +
"	\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" =(\n" +
"		date_part(\n" +
"			'year',\n" +
"			timezone('PST' :: TEXT, now())\n" +
"		)\n" +
"	)\n" +
"AND(\n" +
"	\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" =(\n" +
"		date_part(\n" +
"			'month',\n" +
"			timezone('PST' :: TEXT, now())\n" +
"		)- 1\n" +
"	)\n" +
"	OR \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" =(\n" +
"		date_part(\n" +
"			'month',\n" +
"			timezone('PST' :: TEXT, now())\n" +
"		)\n" +
"	)\n" +
")\n" +
"GROUP BY\n" +
"	\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\",\n" +
"	\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\",\n" +
"	\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\",\n" +
"	\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_SalesDivision\",\n" +
"	dDay\n" +
"ORDER BY\n" +
"	\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\",\n" +
"\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\",\n" +
"		\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_SalesDivision\"");
myARO.setDisplayChart(true);
myARO.setHideLabel(false);
myARO.setAutoReportCode("volume1_reg");
myARO.setChartLabelField("dDay");
myARO.setChartDataField("Volume");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_COLCHART);
myList.add(myARO);







//Using MQ4
myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("MQ4: Chartis CA Cases");
myARO.setsSQL("SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Appointment_AppointmentTime_display\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PracticeID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Practice_Name\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Status\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Type\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PatientLastName\" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" where \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerID\" in (214,216,523) order by  \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Appointment_AppointmentTime_display\" DESC");
myARO.setDisplayChart(false);
myARO.setHideLabel(false);
myARO.setAutoReportCode("mq4_chartisca");
myARO.setChartLabelField("iDay");
myARO.setChartDataField("Volume");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_COLCHART);
myList.add(myARO);


//Using MQ4
myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("MQ4: Active Aging Cases");
myARO.setsSQL("SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Parent_PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_UserID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_FirstName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_LastName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PatientLastName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_DateOfService_display\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Status\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_TimeTrack_ReqDelivered_display\" AS \"ReportReceived\" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" where \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_StatusID\" not in (6,9) and  \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ReqAging\" = 1 ORDER BY \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_DateOfService\" ASC");
myARO.setDisplayChart(false);
myARO.setHideLabel(false);
myARO.setAutoReportCode("mq4_activeaging");
myARO.setChartLabelField("iDay");
myARO.setChartDataField("Volume");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_COLCHART);
myList.add(myARO);






//Using MQ4
myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("MQ4: Johns Easterns Cases");
myARO.setsSQL("SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Parent_PayerID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Parent_PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"CaseClaimNumber\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_UserID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_FirstName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_LastName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_Email\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PatientLastName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_DateOfService_display\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Status\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ReqAging\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Service_first_CPT\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Service_first_CPTBodyPart\" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" where \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\" like '%Johns Eastern%' order by \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Parent_PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_LastName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_FirstName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"CaseClaimNumber\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PatientLastName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_DateOfService_display\"");
myARO.setDisplayChart(false);
myARO.setHideLabel(false);
myARO.setAutoReportCode("mq4_je1");
myARO.setChartLabelField("iDay");
myARO.setChartDataField("Volume");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_COLCHART);
myList.add(myARO);






//Using MQ4
myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("MQ4: Volume By Division");
myARO.setsSQL("SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_SalesDivision\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" as iDay, Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") as Volume FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" GROUP BY \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_SalesDivision\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" order by  \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" desc, \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" desc, \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" desc");
myARO.setDisplayChart(true);
myARO.setHideLabel(false);
myARO.setAutoReportCode("mq4_volume");
myARO.setChartLabelField("iDay");
myARO.setChartDataField("Volume");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_COLCHART);
myList.add(myARO);
/*
//Using MQ4
myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("Actuals to Plan, by Region");
myARO.setsSQL("SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_SalesDivision\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\", Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") as Volume FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" WHERE \"Referral_ReceiveDate_iYear\" = EXTRACT(YEAR FROM CURRENT_DATE) AND \"Payer_SalesDivision\" !='' AND \"Payer_SalesDivision\" != '0' AND \"Payer_SalesDivision\" != 'NIM' AND \"Payer_SalesDivision\" != 'MSC' GROUP BY \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_SalesDivision\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" order by \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" desc");
myARO.setDisplayChart(true);
myARO.setHideLabel(false);
myARO.setAutoReportCode("actuals_to_plan");
myARO.setChartLabelField("iDay");
myARO.setChartDataField("Volume");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_COLCHART);
myList.add(myARO);

*/
myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("Actuals to Plan, by Region");
myARO.setsSQL("SELECT \"public\".\"vMQ4_Encounter_Void_BP\".\"Payer_SalesDivision\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\", Count(\"public\".\"vMQ4_Encounter_Void_BP\".\"Encounter_ScanPass\") as Volume FROM \"public\".\"vMQ4_Encounter_Void_BP\" WHERE \"Referral_ReceiveDate_iYear\" = EXTRACT(YEAR FROM CURRENT_DATE) AND \"Payer_SalesDivision\" !='' AND \"Payer_SalesDivision\" != '0' AND \"Payer_SalesDivision\" != 'NIM' AND \"Payer_SalesDivision\" != 'MSC' GROUP BY \"public\".\"vMQ4_Encounter_Void_BP\".\"Payer_SalesDivision\", \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\" order by \"public\".\"vMQ4_Encounter_Void_BP\".\"Referral_ReceiveDate_iMonth\" desc");
myARO.setDisplayChart(true);
myARO.setHideLabel(false);
myARO.setAutoReportCode("actuals_to_plan");
myARO.setChartLabelField("iDay");
myARO.setChartDataField("Volume");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_COLCHART);
myList.add(myARO);





/* archive
myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("Total Cases By Month (2010)");
myARO.setsSQL("select * FROM \"public\".\"vRP_Sch_TotalCasesByMonth2010\" where count>?");
myARO.getPreparedStatementObject().addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,"0"));
myARO.setDisplayChart(true);
myARO.setAutoReportCode("volume2");
myARO.setHideLabel(false);
myARO.setChartLabelField("sMonth_TimeTrack_ReqRec");
myARO.setChartDataField("count");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_COLCHART);
myList.add(myARO);
*/

myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("Total Cases By Month (2010)");
myARO.setsSQL("SELECT to_char(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate\", 'Month'::text) as sMonth, Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" where  \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\"=? GROUP BY \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\",sMonth order by \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\"");
myARO.getPreparedStatementObject().addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,"2010"));
myARO.setDisplayChart(true);
myARO.setAutoReportCode("mVolume2010");
myARO.setHideLabel(false);
myARO.setChartLabelField("sMonth");
myARO.setChartDataField("count");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_COLCHART);
myList.add(myARO);



myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("Total Cases By Month (2011)");
myARO.setsSQL("SELECT to_char(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate\", 'Month'::text) as sMonth, Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" where  \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\"=? GROUP BY \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\",sMonth order by \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\"");
myARO.getPreparedStatementObject().addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,"2011"));
myARO.setDisplayChart(true);
myARO.setAutoReportCode("mVolume2011");
myARO.setHideLabel(false);
myARO.setChartLabelField("sMonth");
myARO.setChartDataField("count");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_COLCHART);
myList.add(myARO);






myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("Service Breakdown");
myARO.setsSQL("select themodality as \"Service\", SUM(\"public\".\"vRP_Total_ByPayer_ByMod\".\"sum\") AS \"iTotal\"  FROM \"public\".\"vRP_Total_ByPayer_ByMod\" group by themodality order by themodality");
myARO.setDisplayChart(true);
myARO.setAutoReportCode("service2");
myARO.setHideLabel(false);
myARO.setChartLabelField("Service");
myARO.setChartDataField("iTotal");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_COLCHART);
myList.add(myARO);


myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("Service Breakdown by Payer");
myARO.setsSQL("select * FROM \"public\".\"vRP_Total_ByPayer_ByMod\"");
myARO.setDisplayChart(false);
myARO.setAutoReportCode("service1");
myARO.setHideLabel(false);
myList.add(myARO);



myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("Active Case Workload");
myARO.setsSQL("SELECT * FROM \"public\".\"vRP_Sch_CaseWorkload\"");
myARO.setDisplayChart(true);
myARO.setHideLabel(true);
myARO.setChartLabelField("ChartDisplay");
myARO.setChartDataField("TotalCases");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_BARCHART);
//myList.add(myARO);


myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("Case Summary");
myARO.setsSQL("SELECT * FROM \"public\".\"vRP_CaseSummary\"");
myARO.setDisplayChart(false);
myARO.setHideLabel(false);
//myList.add(myARO);






myARO = new com.winstaff.AutoReportObject();
myARO.setAutoReportCode("ndovr");
myARO.setTitle("ND: NetDev: Overview");
myARO.setsSQL("SELECT tstateli.stateid, tstateli.shortstate as State, vptotal.icount AS \"Total\", vpcont.icount AS \"Contracted\", vpneg.icount AS \"Negotiating\", to_char(vpcontavg.davgmri_wo,'999') AS avg_mri_wo, vpcontavg.dminmri_wo AS min_mri_wo, vpcontavg.dmaxmri_wo AS max_mri_wo FROM ((((tstateli JOIN (SELECT \"vMQ_Practices\".\"sOfficeState\" AS sofficestate, count(\"vMQ_Practices\".contractingstatusid) AS icount FROM \"vMQ_Practices\" GROUP BY \"vMQ_Practices\".\"sOfficeState\") vptotal ON (((vptotal.sofficestate)::text = (tstateli.shortstate)::text))) JOIN (SELECT \"vMQ_Practices\".\"sOfficeState\" AS sofficestate, count(\"vMQ_Practices\".contractingstatusid) AS icount FROM \"vMQ_Practices\" WHERE (\"vMQ_Practices\".contractingstatusid = ANY (ARRAY[2, 8, 9, 10])) GROUP BY \"vMQ_Practices\".\"sOfficeState\") vpcont ON (((vpcont.sofficestate)::text = (tstateli.shortstate)::text))) JOIN (SELECT \"vMQ_Practices\".\"sOfficeState\" AS sofficestate, count(\"vMQ_Practices\".contractingstatusid) AS icount FROM \"vMQ_Practices\" WHERE (\"vMQ_Practices\".contractingstatusid = ANY (ARRAY[1, 5])) GROUP BY \"vMQ_Practices\".\"sOfficeState\") vpneg ON (((vpneg.sofficestate)::text = (tstateli.shortstate)::text))) JOIN (SELECT \"vMQ_Practices\".\"sOfficeState\" AS sofficestate, avg(\"vMQ_Practices\".price_mod_mri_wo) AS davgmri_wo, min(\"vMQ_Practices\".price_mod_mri_wo) AS dminmri_wo, max(\"vMQ_Practices\".price_mod_mri_wo) AS dmaxmri_wo FROM \"vMQ_Practices\" WHERE ((\"vMQ_Practices\".contractingstatusid = ANY (ARRAY[2, 8, 9, 10])) AND (\"vMQ_Practices\".price_mod_mri_wo > (0)::numeric)) GROUP BY \"vMQ_Practices\".\"sOfficeState\") vpcontavg ON (((vpcontavg.sofficestate)::text = (tstateli.shortstate)::text))) ORDER BY tstateli.shortstate;");
myARO.setDisplayChart(true);
myARO.setHideLabel(false);
myARO.setChartLabelField("State");
myARO.setChartDataField("Contracted");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_PIECHART);
myList.add(myARO);



myARO = new com.winstaff.AutoReportObject();
myARO.setAutoReportCode("nd_pending");
myARO.setTitle("ND: Pending Approvals");
myARO.setsSQL("SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_display\", \"public\".tnim3_encounter.seenetdev_reqsenttonetdev, (   \"public\".tnim3_encounter.seenetdev_reqsenttonetdev::date - \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_display\"::date - count_full_weekend_days(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate\"::date,\"public\".tnim3_encounter.seenetdev_reqsenttonetdev::DATE) ) AS \"Days_Before_NetDev\", (now()::date - \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_display\"::date - count_full_weekend_days(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate\"::date, now()::DATE) ) AS \"Days_Total\"  FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" INNER JOIN \"public\".tnim3_encounter ON \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"EncounterID\" = \"public\".tnim3_encounter.encounterid where  \"public\".tnim3_encounter.seenetdev_waiting = 1 order by  \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_display\" ");
myARO.setDisplayChart(false);
myARO.setHideLabel(false);
myARO.setChartType(com.winstaff.AutoReportObject.CHART_PIECHART);

myList.add(myARO);





myARO = new com.winstaff.AutoReportObject();
myARO.setAutoReportCode("ndrefpract");
myARO.setTitle("ND: Referrals by Practice");
myARO.setsSQL("SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PracticeID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Practice_Name\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Practice_City\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Practice_State\", Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") AS \"TotalReferrals\" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" where \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PracticeID\" > 0  GROUP BY \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PracticeID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Practice_Name\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Practice_City\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Practice_State\"  ORDER BY  \"TotalReferrals\" DESC, \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Practice_Name\" ASC");
myARO.setDisplayChart(false);
myARO.setHideLabel(false);
myARO.setChartType(com.winstaff.AutoReportObject.CHART_PIECHART);

myList.add(myARO);

myARO = new com.winstaff.AutoReportObject();
myARO.setAutoReportCode("ndrefadmin");
myARO.setTitle("ND: Referrals by Vendor");
myARO.setsSQL("SELECT \"public\".tadminmaster.adminid, \"public\".tadminmaster.\"name\", Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") AS \"TotalReferrals\" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" Inner Join \"public\".tpracticemaster ON \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PracticeID\" = \"public\".tpracticemaster.practiceid Inner Join \"public\".tadminpracticelu ON \"public\".tadminpracticelu.practiceid = \"public\".tpracticemaster.practiceid Inner Join \"public\".tadminmaster ON \"public\".tadminpracticelu.adminid = \"public\".tadminmaster.adminid WHERE \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PracticeID\" >  0 AND \"public\".tadminpracticelu.relationshiptypeid = 2 " + addWhere + " GROUP BY \"public\".tadminmaster.adminid, \"public\".tadminmaster.\"name\" ORDER BY \"TotalReferrals\" DESC");
myARO.setDisplayChart(false);
myARO.setHideLabel(false);
myARO.setChartType(com.winstaff.AutoReportObject.CHART_PIECHART);

myList.add(myARO);


myARO = new com.winstaff.AutoReportObject();
myARO.setAutoReportCode("ndrefscc");
myARO.setTitle("ND: Referrals State, County, City [Practice Location]");
myARO.setsSQL("select  \"public\".zip_code.state_prefix, \"public\".zip_code.county, \"public\".tpracticemaster.officecity, count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" Left Join \"public\".tpracticemaster ON \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PracticeID\" = \"public\".tpracticemaster.practiceid Left Join \"public\".zip_code on substr(\"public\".tpracticemaster.officezip,0,6) = substr(\"public\".zip_code.zip_code,0,6) group by  \"public\".zip_code.county, \"public\".zip_code.state_prefix, \"public\".tpracticemaster.officecity order by  \"public\".zip_code.state_prefix, \"public\".zip_code.county, \"public\".tpracticemaster.officecity");
myARO.setDisplayChart(false);
myARO.setHideLabel(false);
myARO.setChartType(com.winstaff.AutoReportObject.CHART_PIECHART);

myList.add(myARO);


myARO = new com.winstaff.AutoReportObject();
myARO.setAutoReportCode("ndrefscc2");
myARO.setTitle("ND: Referrals State, County, City By Year/Month [Practice Location]");
myARO.setsSQL("SELECT \"public\".zip_code.state_prefix, \"public\".zip_code.county, \"public\".tpracticemaster.officecity, Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\"), \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" Left Join \"public\".tpracticemaster ON \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PracticeID\" = \"public\".tpracticemaster.practiceid Left Join \"public\".zip_code on substr(\"public\".tpracticemaster.officezip,0,6) = substr(\"public\".zip_code.zip_code,0,6) group by  \"public\".zip_code.county, \"public\".zip_code.state_prefix, \"public\".tpracticemaster.officecity,\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" order by  \"public\".zip_code.state_prefix, \"public\".zip_code.county, \"public\".tpracticemaster.officecity,\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\",\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\"");
myARO.setDisplayChart(false);
myARO.setHideLabel(false);
myARO.setChartType(com.winstaff.AutoReportObject.CHART_PIECHART);

myList.add(myARO);








myARO = new com.winstaff.AutoReportObject();
myARO.setAutoReportCode("sppaystate");
myARO.setTitle("SP: Payer Breakdown: Service State");
myARO.setsSQL("SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Practice_State\", Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" GROUP BY \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Practice_State\" Order BY  \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Practice_State\"");
myARO.setDisplayChart(false);
myARO.setHideLabel(false);
myARO.setChartType(com.winstaff.AutoReportObject.CHART_PIECHART);

myList.add(myARO);






%>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isReporter) 
    {
        accessValid = true;
    }
	if (accessValid)
	{
		try
		{

			String myRID = request.getParameter("rid");
			//start
			String sTitle = "";
			String sSQL = "";
			com.winstaff.AutoReportObject theARO = null;
			java.util.Vector myLables = new java.util.Vector();
			java.util.Vector myDatas = new java.util.Vector();
			int myLabel = -1;
			int myData = -1;
			%>
			<form action="report_auto2.jsp" method="GET" id="autoform">
            <input type="hidden" name="MREF" value="<%=MREF%>">
			<select name="rid" onChange="document.getElementById('autoform').submit();">
			  <option value="-1">Please Select</option>
			<%
			for (int i5=0;i5<myList.size();i5++)
			{
				com.winstaff.AutoReportObject tempARO = (com.winstaff.AutoReportObject) myList.elementAt(i5);
				String myValue = i5+"";
				if (tempARO.getAutoReportCode()!=null)
				{
					myValue = tempARO.getAutoReportCode();
				}
				if (myRID.equalsIgnoreCase(myValue))
				{
					%><option selected value="<%=myValue%>"><%=tempARO.getTitle()%></option><%
					theARO = (com.winstaff.AutoReportObject) myList.elementAt(i5);
					sSQL = theARO.getsSQL();
					sTitle = theARO.getTitle();
				}
				else
				{
					%><option value="<%=myValue%>"><%=tempARO.getTitle()%></option><%
				}
			}
			%>
			</select>
Last Updated: <%=PLCUtils.getDisplayDateWithTime(PLCUtils.getNowDate(false))%>
			<h1><%=sTitle%></h1>
	  <%
				searchDB2 mySS = new searchDB2();
			//	String mySQL = ("SELECT LogonUserName, UniqueModifyDate from tUserAccount");
//				out.print(theARO.getPreparedStatementObject().getSQL());
				java.sql.ResultSet result = null;
				java.sql.ResultSetMetaData rsmd = null;    
				result = mySS.executePreparedStatement(theARO.getPreparedStatementObject());
				int columns=0;
				try 
				{
				   rsmd = result.getMetaData();
				   columns = rsmd.getColumnCount();
				}
				catch (SQLException e) 
				{
					out.println("Error occurred " + e);
				}
				
%>
                <%if (!request.getParameter("rid").equals("actuals_to_plan")){ %>
                <select name="search1">
                <%
                                    for (int i=1; i<=columns; i++) 
                                    {
                                         if (theARO.isHideLabel()&&rsmd.getColumnLabel(i).equalsIgnoreCase(theARO.getChartLabelField()))
                                         {
                                         }
                                         else
                                         {
                                            out.write("<option value="+ rsmd.getColumnLabel(i) +">" + rsmd.getColumnLabel(i) + "</option>");
                                         }
                                    }
                %>
                </select> : <input name="value1" >
                <br>
                <select name="search2">
                <%
                                    for (int i=1; i<=columns; i++) 
                                    {
                                         if (theARO.isHideLabel()&&rsmd.getColumnLabel(i).equalsIgnoreCase(theARO.getChartLabelField()))
                                         {
                                         }
                                         else
                                         {
                                            out.write("<option>" + rsmd.getColumnLabel(i) + "</option>");
                                         }
                                    }
                %>
                </select> : <input name="value2" >
                <br>
                <select name="search3">
                <%
                                    for (int i=1; i<=columns; i++) 
                                    {
                                         if (theARO.isHideLabel()&&rsmd.getColumnLabel(i).equalsIgnoreCase(theARO.getChartLabelField()))
                                         {
                                         }
                                         else
                                         {
                                            out.write("<option>" + rsmd.getColumnLabel(i) + "</option>");
                                         }
                                    }
                %>
                </select> : <input name="value3" >
				<%}%>
<input name="" type="submit" value="Submit">
</form>
            


<%				
				
				
					if (theARO.isDisplayChart() && !request.getParameter("rid").equals("actuals_to_plan"))
					{
					   %>

            <hr> <div id="visualization" 
     style="width: 800px; height: 300px;">
    </div><hr>
    <%
					}
					%>
			
			  <% // write out the header cells containing the column labels
				 try 
				 {
					if (!request.getParameter("rid").equals("actuals_to_plan")){%>
						<table cellspacing="3" border="1">
			  			<tr>
						<%
						for (int i=1; i<=columns; i++) 
						{
							 if (theARO.isHideLabel()&&rsmd.getColumnLabel(i).equalsIgnoreCase(theARO.getChartLabelField()))
							 {
							 }
							 else
							 {
								out.write("<th>" + rsmd.getColumnLabel(i) + "</th>");
							 }
							 if (rsmd.getColumnLabel(i).equalsIgnoreCase(theARO.getChartLabelField()))
							 {
								 myLabel = i;
							 }
							 if (rsmd.getColumnLabel(i).equalsIgnoreCase(theARO.getChartDataField()))
							 {
								 myData = i;
							 }
						}%>
						</tr>
					<%}%>
<% // now write out one row for each entry in the database table
			  			
					if (!request.getParameter("rid").equals("actuals_to_plan")){
						int i5=0;
						while (result.next()) 
						{
							i5++;
							if (i5%2==0)
							{
							   out.write("<tr class=tdAlt   onMouseOver=\"this.className='highlight'\" onMouseOut=\"this.className='tdAlt'\" >");
							}
							else
							{
							   out.write("<tr class=normal onMouseOver=\"this.className='highlight'\" onMouseOut=\"this.className='normal'\" >");
							}
						   for (int i=1; i<=columns; i++) 
						   {
							 if (theARO.isHideLabel()&&i==myLabel)
							 {
							 }
							 else
							 {
								 out.write("<td>&nbsp;" + result.getString(i) + "</td>");
							 }
							 if (i==myLabel)
							 {
								 myLables.add(result.getString(i));
							 }
							 if (i==myData)
							 {
								 myDatas.add(result.getString(i));
							 }
						   }
						   out.write("</tr>");
						}
					}
					//end
					
					else if (request.getParameter("rid").equals("actuals_to_plan"))
					{
						String actual[][] = new String [9][13];
						int total=0;
						int region=0;
						int month=0;
						double monthTemp=0;
						double q1=0;
						double q2=0;
						double q3=0;
						double q4=0;
						double plan = 50;
						int temp=0;
						double west=0;
						double southeast=0;
						double south=0;
						
						double[] westPlan = {549,514,556,656,778,739,889,976,928,1085,935,924};
						int[] westMin = {467,437,473,558,661,628,756,830,789,922,795,785};
						double[] southEastPlan = {416,389,421,479,568,539,625,686,652,784,675,668};
						int[] southEastMin ={354,331,358,407,483,458,531,583,554,666,574,568};
						double[] southPlan = {475,445,481,568,673,639,745,818,778,904,779,770};
						int[] southMin = {404,378,409,483,572,543,633,695,661,768,662,655};
						
						java.text.DecimalFormat dOne = new java.text.DecimalFormat("#,##0.0");
						java.text.DecimalFormat dNone = new java.text.DecimalFormat("#,##0");
						java.util.Calendar cal = java.util.Calendar.getInstance();
						java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("M");
						while (result.next()) 
						{
						   for (int i=1; i<=columns; i++) 
						   {
								if (i==1){
									//west
									if (result.getString(i).equals("W1")){
										region = 0;
									}
									//west
									else if (result.getString(i).equals("W2")){
										region = 1;
									}
									//west
									else if (result.getString(i).equals("W3")){
										region = 2;
									}
									//southeast
									else if (result.getString(i).equals("SE")){
										region = 3;
									}
									//southeast
									else if (result.getString(i).equals("SE2")){
										region = 4;
									}
									else if (result.getString(i).equals("NE")){
										region = 5;
									}
									else if (result.getString(i).equals("NE2")){
										region = 6;
									}
									//south
									else if (result.getString(i).equals("S")){
										region = 7;
									}
									//south
									else if (result.getString(i).equals("S2")){
										region = 8;
									}
								}
								else if (i==2){
									month = Integer.parseInt(result.getString(i));
								}
								else if (i==3){
									actual[region][month] = result.getString(i);
								}
						   }
						}%>
						
						<table style="border:1px solid">
                        	<thead><tr>
								<th>&nbsp;</th>
                                <th>Jan</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Apr</th>
                                <th>May</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Aug</th>
                                <th>Sep</th>
                                <th>Oct</th>
                                <th>Nov</th>
                                <th>Dec</th>
                                <th>Total</th>
							</tr></thead>
							<tbody>
                            
                            <!-- West -->
                            	<tr><td align="center" colspan=14><h3>West</h3></td></tr>
                                <tr>
                                    <td>Plan:</td>
									<% total=0;
									for(month=0;month<12;month++){%>
                                    <td><%out.print(dNone.format(westPlan[month]));total+=westPlan[month];%>
                                    </td>
                                    <%}%>
                                    <td><%=total%></td>
                                </tr>
                                <tr>
                                    <td>Min:</td>
									<% total=0;
									for(month=0;month<12;month++){%>
                                    <td><%out.print(dNone.format(westMin[month]));total+=westMin[month];%>
                                    </td>
                                    <%}%>
                                    <td><%=total%></td>
                                </tr>
                                <tr>
                                	<td>Actual:</td>
                            		<%total=0;
									for(month=1;month<13;month++){ monthTemp=0;%>
                                	<td>
									<%for (region=0;region<3;region++){
										if( actual[region][month]!=null){
											out.print("<!--acutal["+region+"]["+month+"] = "+actual[region][month]+"-->");
											monthTemp+=Double.parseDouble(actual[region][month]);
											total+=Integer.parseInt(actual[region][month]);
											if (month==1 || month==2 || month==3){q1+=Double.parseDouble(actual[region][month]);}
											if (month==4 || month==5 || month==6){q2+=Double.parseDouble(actual[region][month]);}
											if (month==7 || month==8 || month==9){q3+=Double.parseDouble(actual[region][month]);}
											if (month==10 || month==11 || month==12){q4+=Double.parseDouble(actual[region][month]);}
										}
									}
									out.print(dNone.format(monthTemp));
									%>
									</td>
									<%}%>
                                    <td><%=total%></td>
                                </tr>
                                 <tr>
                                	<td>% of Plan:</td>
                            		<%
									for(month=1;month<13;month++){ monthTemp=0;%>
                                	<td>
									<%for (region=0;region<3;region++){
										if( actual[region][month]!=null){
											monthTemp+=Double.parseDouble(actual[region][month]);
										}
									}
									out.print(dNone.format(monthTemp/westPlan[month-1]*100)+"%");
									%>
									</td>
									<%}%>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                	<td>Quarterly:</td>
                            		<%
									for(month=1;month<13;month++){%>
                                	<%
									if(month==3 && 3 <=Integer.parseInt(df.format(cal.getTime()))){%>
                                    	<td colspan="3" align="right">
											<%=dOne.format(q1/(westPlan[0]+westPlan[1]+westPlan[2])*100)%>%
										</td>
									<% }
									else if(month==6 && 6 <=Integer.parseInt(df.format(cal.getTime()))){%>
										<td colspan="3" align="right">
											<%=dOne.format(q2/(westPlan[3]+westPlan[4]+westPlan[5])*100)%>%
										</td>
									<% }
									else if(month==9 && 9 <=Integer.parseInt(df.format(cal.getTime()))){%>
										<td colspan="3" align="right">
											<%=dOne.format(q3/(westPlan[6]+westPlan[7]+westPlan[8])*100)%>%
										</td>
									<% }
									else if(month==12 && 12 <=Integer.parseInt(df.format(cal.getTime()))){%>
										<td colspan="3" align="right">
											<%=dOne.format(q4/(westPlan[9]+westPlan[10]+westPlan[11])*100)%>%
										</td>
									<% }
									
									%>
									<%}%>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr><td colspan=14>&nbsp;</td></tr>
                                <!-- end West -->
                                
                                <!-- SouthEast -->
                            	<tr><td align="center" colspan=14><h3>South East</h3></td></tr>
                                <%q1=0;q2=0;q3=0;q4=0; %>
                                <tr>
                                   <td>Plan:</td>
									<% total=0;
									for(month=0;month<12;month++){%>
                                    <td><% out.print(dNone.format(southEastPlan[month]));total+=southEastPlan[month];%>
                                    </td>
                                    <%}%>
                                    <td><%=total%></td>
                                </tr>
                                <tr>
                                    <td>Min:</td>
									<% total=0;
									for(month=0;month<12;month++){%>
                                    <td><%out.print(dNone.format(southEastMin[month]));total+=southEastMin[month];%>
                                    </td>
                                    <%}%>
                                    <td><%=total%></td>
                                </tr>
                                <tr>
                                	
                                	<td>Actual:</td>
                            		<%total=0;
									for(month=1;month<13;month++){ monthTemp=0;%>
                                	<td>
									<%for (region=3;region<5;region++){
										out.print("<!--acutal["+region+"]["+month+"] = "+actual[region][month]+"-->");
										if( actual[region][month]!=null){
											monthTemp+=Double.parseDouble(actual[region][month]);
											total+=Integer.parseInt(actual[region][month]);
											if (month==1 || month==2 || month==3){q1+=Double.parseDouble(actual[region][month]);}
											if (month==4 || month==5 || month==6){q2+=Double.parseDouble(actual[region][month]);}
											if (month==7 || month==8 || month==9){q3+=Double.parseDouble(actual[region][month]);}
											if (month==10 || month==11 || month==12){q4+=Double.parseDouble(actual[region][month]);}
										}
									}
									out.print(dNone.format(monthTemp));
									%>
									</td>
									<%}%>
                                    <td><%=total%></td>
                                </tr>
                                 <tr>
                                	<td>% of Plan:</td>
                            		<%
									for(month=1;month<13;month++){ monthTemp=0;%>
                                	<td>
									<%for (region=3;region<5;region++){
										if( actual[region][month]!=null){
											monthTemp+=Double.parseDouble(actual[region][month]);
										}
									}
									out.print(dNone.format(monthTemp/southEastPlan[month-1]*100)+"%");
									%>
									</td>
									<%}%>
                                    <td>&nbsp;</td>
                                </tr>
								<tr>
                                	<td>Quarterly:</td>
                            		<%
									for(month=1;month<13;month++){%>
                                	<%
									if(month==3 && 3 <=Integer.parseInt(df.format(cal.getTime()))){%>
                                    	<td colspan="3" align="right">
											<%=dOne.format(q1/(southEastPlan[0]+southEastPlan[1]+southEastPlan[2])*100)%>%
										</td>
									<% }
									else if(month==6 && 6 <=Integer.parseInt(df.format(cal.getTime()))){%>
										<td colspan="3" align="right">
											<%=dOne.format(q2/(southEastPlan[3]+southEastPlan[4]+southEastPlan[5])*100)%>%
										</td>
									<% }
									else if(month==9 && 9 <=Integer.parseInt(df.format(cal.getTime()))){%>
										<td colspan="3" align="right">
											<%=dOne.format(q3/(southEastPlan[6]+southEastPlan[7]+southEastPlan[8])*100)%>%
										</td>
									<% }
									else if(month==12 && 12 <=Integer.parseInt(df.format(cal.getTime()))){%>
										<td colspan="3" align="right">
											<%=dOne.format(q4/(southEastPlan[9]+southEastPlan[10]+southEastPlan[11])*100)%>%
										</td>
									<% }
									
									%>
									<%}%>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr><td colspan=14>&nbsp;</td></tr>
                                <!-- End SouthEast -->
                                
                                <!-- South -->
                            	<tr><td align="center" colspan=14><h3>South</h3></td></tr>
                                <%q1=0;q2=0;q3=0;q4=0; %>
                                <tr>
                                    <td>Plan:</td>
									<% total=0;
									for(month=0;month<12;month++){%>
                                    <td><% out.print(dNone.format(southPlan[month]));total+=southPlan[month];%>
                                    </td>
                                    <%}%>
                                    <td><%=total%></td>
                                </tr>
                                 <tr>
                                    <td>Min:</td>
									<% total=0;
									for(month=0;month<12;month++){%>
                                    <td><%out.print(dNone.format(southMin[month]));total+=southMin[month];%>
                                    </td>
                                    <%}%>
                                    <td><%=total%></td>
                                </tr>
                                <tr>
                                	
                                	<td>Actual:</td>
                            		<%total=0;
									for(month=1;month<13;month++){ monthTemp=0;%>
                                	<td>
									<%
									for (region=7;region<9;region++){
										if( actual[region][month]!=null){
											out.print("<!--acutal["+region+"]["+month+"] = "+actual[region][month]+"-->");
											monthTemp+=Double.parseDouble(actual[region][month]);
											total+=Integer.parseInt(actual[region][month]);
											if (month==1 || month==2 || month==3){q1+=Double.parseDouble(actual[region][month]);}
											if (month==4 || month==5 || month==6){q2+=Double.parseDouble(actual[region][month]);}
											if (month==7 || month==8 || month==9){q3+=Double.parseDouble(actual[region][month]);}
											if (month==10 || month==11 || month==12){q4+=Double.parseDouble(actual[region][month]);}
										}
									}
									out.print(dNone.format(monthTemp));
									%>
									</td>
									<%}%>
                                    <td><%=total%></td>
                                </tr>
                                 <tr>
                                	<td>% of Plan:</td>
                            		<%
									for(month=1;month<13;month++){ monthTemp=0;%>
                                	<td>
									<%
									for (region=7;region<9;region++){
										if( actual[region][month]!=null){
											monthTemp+=Double.parseDouble(actual[region][month]);
										}
									}
									out.print(dNone.format(monthTemp/southPlan[month-1]*100)+"%");
									%>
									</td>
									<%}%>
                                    <td>&nbsp;</td>
                                </tr>
								<tr>
                                	<td>Quarterly:</td>
                            		<%
									for(month=1;month<13;month++){%>
                                	<%
									if(month==3 && 3 <=Integer.parseInt(df.format(cal.getTime()))){%>
                                    	<td colspan="3" align="right">
											<%=dOne.format(q1/(southPlan[0]+southPlan[1]+southPlan[2])*100)%>%
										</td>
									<% }
									else if(month==6 && 6 <=Integer.parseInt(df.format(cal.getTime()))){%>
										<td colspan="3" align="right">
											<%=dOne.format(q2/(southPlan[3]+southPlan[4]+southPlan[5])*100)%>%
										</td>
									<% }
									else if(month==9 && 9 <=Integer.parseInt(df.format(cal.getTime()))){%>
										<td colspan="3" align="right">
											<%=dOne.format(q3/(southPlan[6]+southPlan[7]+southPlan[8])*100)%>%
										</td>
									<% }
									else if(month==12 && 12 <=Integer.parseInt(df.format(cal.getTime()))){%>
										<td colspan="3" align="right">
											<%=dOne.format(q4/(southPlan[9]+southPlan[10]+southPlan[11])*100)%>%
										</td>
									<% }
									
									%>
									<%}%>
                                    <td>&nbsp;</td>
                                </tr>
                                
                                <!-- End South -->
                                <tr><td colspan="14">&nbsp;</td></tr>
                                
                            </tbody>
                        </table>
					<%}
//google chart example	
					if (theARO.isDisplayChart() && !request.getParameter("rid").equals("actuals_to_plan"))
					{
					   %>
						<script type="text/javascript" src="https://www.google.com/jsapi">
                        </script>
                        <script type="text/javascript">
                          google.load('visualization', '1', 
                            {packages: ['<%=theARO.getChartType()%>']});
                        </script>
                        <script type="text/javascript">
                          function drawVisualization() {
                            // Create and populate the data table.
                            var data = new google.visualization.DataTable();
                            data.addColumn('string', 'Label');
                            data.addColumn('number', 'Total');
                            data.addRows([
							<%
							   for (int i6=0; i6<myLables.size(); i6++) 
							   {
								   String tempLabel = (String) myLables.elementAt(i6);
								   String tempData = (String) myDatas.elementAt(i6);
									 %>['<%=tempLabel%>',{v:<%=tempData%>}]<%
									 if (i6!=myLables.size()-1)
									 {
										 out.print(", ");
									 }
									 
							   }
							%>  
                            ]);
                          
                            // Create and draw the visualization.
							<%
							if (theARO.getChartType().equalsIgnoreCase(com.winstaff.AutoReportObject.CHART_PIECHART))
							{%>new google.visualization.PieChart(<%
							}
							else if (theARO.getChartType().equalsIgnoreCase(com.winstaff.AutoReportObject.CHART_BARCHART))
							{%>new google.visualization.BarChart(<%
							}
							else if (theARO.getChartType().equalsIgnoreCase(com.winstaff.AutoReportObject.CHART_COLCHART))
							{%>new google.visualization.ColumnChart(<%
							}%>
                              document.getElementById('visualization')).
                                draw(data, {is3D:true});
                          }
                          google.setOnLoadCallback(drawVisualization);
                        </script>                       
                       <%
					}
			
					// close the connection and the statement
					mySS.closeAll();
				 } // end of the try block
				 catch (SQLException e) 
				 {
					out.println("Error " + e);
				 }
			
		 } // end of the try block
		 catch (Exception e55) 
		 {
			out.println("<hr>Error Main: " + e55);
		 }
	}
	else
	{
		out.print ("no access");
	}
   }
   else
   {
		out.print ("no sec");
   }

   %>
</table>

</td>
  </tr>
</table>

</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
if (!isPop)
{
	%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>    
    <%
}

%>
