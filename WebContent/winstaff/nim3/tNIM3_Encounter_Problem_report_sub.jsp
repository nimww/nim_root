<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
<%@ include file="../generic/generalDisplay.jsp" %>



<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

    <table cellpadding=5 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM_Encounter")%>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat("MM/dd/yyyy");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
		String sFlag = request.getParameter("EDIT");
	    String theMessage = request.getParameter("problemReport");
	    Integer iExtraCode = null;
		try{
	    	iExtraCode = new Integer (request.getParameter("iExtraCode"));
		} catch (java.lang.NumberFormatException ne){
		}
		String UserLogonDescription = "n/a";
		if (pageControllerHash.containsKey("UserLogonDescription"))
		{
			UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
		}
		String displayNotes = NIMUtils.processEncounterProblem(iEncounterID, sFlag, theMessage,  CurrentUserAccount.getUserID(), UserLogonDescription, iExtraCode);
%>
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td class="tdHeader"><%=displayNotes%></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="right"><input type="button" class="inputButton_md_Create" onclick="this.disabled=true;window.close();" value="Continue" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr> 
    </table>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

