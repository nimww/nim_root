<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*

    filename: out\jsp\tNIM3_CommTrack_form.jsp
    Created on Jun/24/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


<table cellpadding=0 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tNIM3_CommTrack")%>



<%
//initial declaration of list class and parentID
    Integer        iCommTrackID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iCommTrackID")) 
    {
        iCommTrackID        =    (Integer)pageControllerHash.get("iCommTrackID");
        accessValid = true;
        if (iCommTrackID.intValue() != iEDITID.intValue())
        {
        	accessValid = false;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_CommTrack_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID

    bltNIM3_CommTrack        NIM3_CommTrack        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        NIM3_CommTrack        =    new    bltNIM3_CommTrack(iCommTrackID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_CommTrack        =    new    bltNIM3_CommTrack(UserSecurityGroupID, true);
    }

//fields
        %>
<p class=title>Add CommTrack</p>
        <form action="tNIM3_CommTrack_form_sub.jsp" name="tNIM3_CommTrack_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
    %>
        <input type="hidden" name="EDITID" value = "<%=iEDITID%>" >  
        <input type="hidden" name="AlertStatusCode" value = "1" >  

          <%  String theClass ="tdBase";%>
          <h3><%=iCommTrackID%></h3>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>

            <%
            if ( (NIM3_CommTrack.isRequired("IntUserID",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("IntUserID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("IntUserID",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("IntUserID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
  <%
            if (false&&(NIM3_CommTrack.isWrite("IntUserID",UserSecurityGroupID)))
            {
                        %>
                     <tr>
                       <td valign=top><b>From </b>                         <p class=<%=theClass%> ><b></b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="IntUserID" value="<%=NIM3_CommTrack.getIntUserID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IntUserID&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("IntUserID")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_CommTrack.isRead("IntUserID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>From:&nbsp;</b></p></td><td valign=top><p><%=CurrentUserAccount.getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=IntUserID&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("IntUserID")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("ExtUserID",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("ExtUserID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("ExtUserID",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("ExtUserID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
  <%
            if (false&&(NIM3_CommTrack.isWrite("ExtUserID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>To</b></p></td><td valign=top><p><select name="ExtUserID"><%

com.winstaff.searchDB2 mySS = new com.winstaff.searchDB2();
java.sql.ResultSet myRS = null;;
try
{
	String mySQL = ("select UserID, LogonUsername, ContactFirstname, ContactLastName,  PayerName from tUserAccount INNER JOIN tNIM3_PayerMaster on tUserAccount.ReferenceID = tNIM3_PayerMaster.PayerID  where accounttype in ('PayerID','ProviderID') order by PayerID");
	myRS = mySS.executeStatement(mySQL);
}
catch(Exception e)
{
	out.println("ResultsSet:"+e);
}
try
{
		%>
		<option value=0>NA</option>
       <%
   while (myRS!=null&&myRS.next())
   {
		%>
		<option value=<%=myRS.getString("UserID")%>><%=myRS.getString("ContactFirstName")%> <%=myRS.getString("ContactLastName")%> (@ <%=myRS.getString("PayerName")%>)</option>
       <%
   }
}
catch(Exception e)
{
	out.println("ResultsSet:"+e);
}
mySS.closeAll();   

					 
					 %></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExtUserID&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("ExtUserID")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_CommTrack.isRead("ExtUserID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ExtUserID&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getExtUserID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExtUserID&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("ExtUserID")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("CommTypeID",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("CommTypeID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("CommTypeID",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("CommTypeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_CommTrack.isWrite("CommTypeID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>CommTypeID&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="CommTypeID" value="<%=NIM3_CommTrack.getCommTypeID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CommTypeID&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("CommTypeID")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_CommTrack.isRead("CommTypeID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>CommTypeID&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getCommTypeID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CommTypeID&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("CommTypeID")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("CommStart",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("CommStart")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("CommStart",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("CommStart",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

<%
            if (false&&(NIM3_CommTrack.isWrite("CommStart",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>CommStart&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="CommStart" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_CommTrack.getCommStart())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CommStart&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("CommStart")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_CommTrack.isRead("CommStart",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date/Time</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_CommTrack.getCommStart())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CommStart&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("CommStart")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("CommEnd",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("CommEnd")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("CommEnd",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("CommEnd",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (false&&(NIM3_CommTrack.isWrite("CommEnd",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>CommEnd&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="CommEnd" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_CommTrack.getCommEnd())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CommEnd&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("CommEnd")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_CommTrack.isRead("CommEnd",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>CommEnd&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_CommTrack.getCommEnd())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CommEnd&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("CommEnd")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("MessageText",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("MessageText")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("MessageText",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("MessageText",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_CommTrack.isWrite("MessageText",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=NIM3_CommTrack.getEnglish("MessageText")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,5000)" rows="12" name="MessageText" cols="60"><%=NIM3_CommTrack.getMessageText()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageText&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageText")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_CommTrack.isRead("MessageText",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=NIM3_CommTrack.getEnglish("MessageText")%>&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getMessageText()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageText&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageText")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("MessageName",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("MessageName")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("MessageName",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("MessageName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_CommTrack.isWrite("MessageName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>MessageName&nbsp;</b></p></td><td valign=top><p><input maxlength="80" type=text size="80" name="MessageName" value="<%=NIM3_CommTrack.getMessageName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageName&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageName")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_CommTrack.isRead("MessageName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>MessageName&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getMessageName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageName&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageName")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("MessageCompany",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("MessageCompany")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("MessageCompany",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("MessageCompany",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_CommTrack.isWrite("MessageCompany",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>MessageCompany&nbsp;</b></p></td><td valign=top><p><input maxlength="80" type=text size="80" name="MessageCompany" value="<%=NIM3_CommTrack.getMessageCompany()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageCompany&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageCompany")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_CommTrack.isRead("MessageCompany",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>MessageCompany&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getMessageCompany()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageCompany&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageCompany")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("MessageEmail",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("MessageEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("MessageEmail",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("MessageEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_CommTrack.isWrite("MessageEmail",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>MessageEmail&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="MessageEmail" value="<%=NIM3_CommTrack.getMessageEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageEmail&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageEmail")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_CommTrack.isRead("MessageEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>MessageEmail&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getMessageEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageEmail&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageEmail")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("MessagePhone",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("MessagePhone")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("MessagePhone",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("MessagePhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_CommTrack.isWrite("MessagePhone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>MessagePhone&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="MessagePhone" value="<%=NIM3_CommTrack.getMessagePhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessagePhone&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessagePhone")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_CommTrack.isRead("MessagePhone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>MessagePhone&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getMessagePhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessagePhone&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessagePhone")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("MessageFax",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("MessageFax")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("MessageFax",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("MessageFax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_CommTrack.isWrite("MessageFax",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>MessageFax&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="MessageFax" value="<%=NIM3_CommTrack.getMessageFax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageFax&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageFax")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_CommTrack.isRead("MessageFax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>MessageFax&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getMessageFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageFax&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageFax")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("AlertStatusCode",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("AlertStatusCode")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("AlertStatusCode",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("AlertStatusCode",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_CommTrack.isWrite("AlertStatusCode",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>AlertStatusCode&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="AlertStatusCode" value="<%=NIM3_CommTrack.getAlertStatusCode()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AlertStatusCode&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("AlertStatusCode")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_CommTrack.isRead("AlertStatusCode",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>AlertStatusCode&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getAlertStatusCode()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AlertStatusCode&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("AlertStatusCode")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("Comments",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("Comments",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_CommTrack.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=NIM3_CommTrack.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=NIM3_CommTrack.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_CommTrack.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=NIM3_CommTrack.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tNIM3_CommTrack")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tNIM3_CommTrack")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input name=Submit type=Submit class="inputButton_md_Create" id="Submit" onClick = "this.disabled=true;submit();" value="Save" ></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_Clear.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>