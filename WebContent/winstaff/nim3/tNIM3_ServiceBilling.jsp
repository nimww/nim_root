


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tNIM3_Encounter_form.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear_fade.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<script language="javascript">

function removeCommas(sVal)
{
	return sVal.replace(',','');
//	return sVal;
}

</script>
<script language="javascript">
show_pws_Loading();
</script>

    <table id="bigfade" cellpadding=0 cellspacing=0 border=0>
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl_form","tNIM3_Encounter")%>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
//        accessValid = true;
        if (iEncounterID.intValue() == iEDITID.intValue())
        {
        	accessValid = true;
        }
    }
  //page security
  if (accessValid&&isScheduler)
  {
	java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
	java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);	java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_Encounter_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID

    NIM3_EncounterObject2        myEO2        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("nim3expbill2") )
    {
        myEO2        =    new    NIM3_EncounterObject2(iEncounterID,"Billing 2.0");
    }
	NIM3_BillingObject2 myBO2 = myEO2.getBillingTotals2();
	

//fields
        %>
        <form action="tNIM3_EncounterService_form_sub.jsp" name="tNIM3_Encounter_form1" method="POST" >
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
    %>
        <input type="hidden" name="EDITID" value = "<%=iEDITID%>" >  

          <%  String theClass ="tdBase";%>
        <table border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr>
           <td class=tableColor>


    <table cellpadding=0 cellspacing=0>
                        <tr>
                          <td colspan="3" valign=middle><img src="images/marker_encounter.gif" width="114" height="31" align="middle" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="inputButton_md_Create" onClick = "this.disabled=false;$('table').fadeOut('slow');show_pws_Calc();" type=Submit value="Update" name=Submit>&nbsp;&nbsp;<input class="inputButton_md_Default" onClick = "window.close();" type=Submit value="Close" name=Submit3></td>
                        </tr>

                <tr class="tableColor">
      <td colspan="3"><table border="0" cellspacing="0" cellpadding="5">
                            <tr class="tdHeaderAltDark">
                              <td class="tdHeaderAltDark">Reference Summary</td>
                              <td class="inputButton_sm_Stop">&nbsp;</td>
                              <td class="tdHeaderAltDark">AR/AP Summary</td>
                            </tr>
                            <tr>
                              <td valign="top"><table border="0" cellspacing="0" cellpadding="3">
                                <tr>
                                  <td>Payer Allow amount</td>
                                  <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getReference_Payer_AllowAmount())%></td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>Provider Contract Amount</td>
                                  <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getReference_Provider_AllowAmount())%></td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>Net Expected Margin</td>
                                  <td class="borderHighlightGreen"><strong><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getNetReferenceExpected())%></strong></td>
                                  <td  class="borderHighlightGreen"><strong><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency(myBO2.getNetReferenceExpected_AsPercent()*100)%>%</strong></td>
                                </tr>
                              </table>
                            
 <strong>Detailed Analysis</strong>&nbsp;<input name="button1" type="button" id="button1" value="Show" onClick="document.getElementById('encounter_detailedanalysis').style.display = 'block';">&nbsp;<input name="button1" type="button" id="button1" value="Hide" onClick="document.getElementById('encounter_detailedanalysis').style.display = 'none';">

                          <div id="encounter_detailedanalysis" style="display:none;background-color:#CCC;padding:5px;">                                
                                
                                <table border="0" cellspacing="0" cellpadding="5">
                                <tr>
                                  <td>Fee Schedule Amount</td>
                                  <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getReference_FeeScheduleAmount())%></td>
                                  <td>U&amp;C Amount</td>
                                  <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getReference_UCAmount())%></td>
                                  <td>Provider Billed Amount</td>
                                  <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getReference_Provider_BillAmount())%></td>
                                </tr>
                                <tr>
                                  <td>Cost Savings (FS)</td>
                                  <td class="borderHighlightGreen"><strong><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getReferenceCostSavings_FeeSchedule())%></strong></td>
                                  <td>Cost Savings (UC)</td>
                                  <td class="borderHighlightGreen"><strong><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getReferenceCostSavings_UC())%></strong></td>
                                  <td>Expected Cost Savings (Billed)</td>
                                  <td class="borderHighlightGreen"><strong><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getReferenceCostSavings_ProviderBilled())%></strong></td>
                                </tr>
                                <tr>
                                  <td colspan="6" class="inputButton_sm_Stop">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td colspan="6"><%
								  if (myEO2.getNIM3_Encounter().getSentTo_Bill_Pay().before(NIMUtils.getBeforeTime()))
								  {
									  %>
                                      We have not yet sent any bills to the Payer.
                                      <%
								  }
								  else 
								  {
									  %>
                                      We have sent a bill to the Payer: <%=PLCUtils.getDaysTill_Display(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay(),false)%>.
                                      <%
								  }
								  if (myEO2.getNIM3_Encounter().getRec_Bill_Pro().before(NIMUtils.getBeforeTime()))
								  {
									  %>
                                      <br>We have not yet received a bill (HFCA) from Provider.
                                      <%
								  }
								  else 
								  {
									  %>
                                      <br>We have received a bill (HFCA) from Provider: <%=PLCUtils.getDaysTill_Display(myEO2.getNIM3_Encounter().getRec_Bill_Pro(),false)%>.
                                      <%
								  }
								  
								  %></td>
                                </tr>
                              </table>
                              
</div>
                              
</td>
                              <td class="inputButton_sm_Stop">&nbsp;</td>
                              <td><table border="0" cellspacing="0" cellpadding="5">
                                <tr>
                                  <td> Revenue</td>
                                  <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getNetActualRevenue())%></td>
                                  <td>Cost of Sales</td>
                                  <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getNetActualCOS())%></td>
                                </tr>
                                <tr>
                                  <td>AR Payment</td>
                                  <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getActual_ARPayment())%></td>
                                  <td>AP Payment</td>
                                  <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getActual_APPayment())%></td>
                                </tr>
                                <tr>
                                  <td>AR Remaining</td>
                                  <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getARActualRemaining())%></td>
                                  <td>AP Remaining</td>
                                  <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getAPActualRemaining())%></td>
                                </tr>
                                <tr>
                                  <td>Net Expected</td>
                                  <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getNetActualExpected())%></td>
                                  <td>Net Realized</td>
                                  <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(myBO2.getNetActualRealized())%></td>
                                </tr>
                                <tr>
                                  <td colspan="4">Status: <%=myBO2.getStatus("<br>")%></td>
                                </tr>
                              </table>
                              </td>
                            </tr>
                          </table></td>
                </tr>
                <tr class="tdHeaderAlt">
  <td colspan="3" class="tdHeaderAltDark">Billing Dates</td></tr>



            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("SentTo_Bill_Pay",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("SentTo_Bill_Pay")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("SentTo_Bill_Pay",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("SentTo_Bill_Pay",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(myEO2.getNIM3_Encounter().isWrite("SentTo_Bill_Pay",UserSecurityGroupID)))
            {
                        %><tr><td colspan="3"><hr></td></tr>
                        <tr><td valign=top nowrap><p class=<%=theClass%> ><b>Original Date Bill Sent To Payer&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" width="90%" valign=top><p><input maxlength=20  type=text size="20" name="SentTo_Bill_Pay" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_Bill_Pay&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("SentTo_Bill_Pay")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;<%=PLCUtils.getDaysTill_Display(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay(),false)%></p></td></tr>
                        <%
            }
            else if (isScheduler&&(myEO2.getNIM3_Encounter().isRead("SentTo_Bill_Pay",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top nowrap><p class=<%=theClass%> ><b>Bill Sent To Payer&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getSentTo_Bill_Pay())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_Bill_Pay&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("SentTo_Bill_Pay")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("Rec_Bill_Pay",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("Rec_Bill_Pay")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("Rec_Bill_Pay",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("Rec_Bill_Pay",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (false&&isScheduler&&(myEO2.getNIM3_Encounter().isWrite("Rec_Bill_Pay",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top nowrap><p class=<%=theClass%> ><b>Final Date Payment Rec From Payer&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><input maxlength=20  type=text size="20" name="Rec_Bill_Pay" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getRec_Bill_Pay())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pay&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Rec_Bill_Pay")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&isScheduler&&(myEO2.getNIM3_Encounter().isRead("Rec_Bill_Pay",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top nowrap><p class=<%=theClass%> ><b>Payment Rec By Payer&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getRec_Bill_Pay())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pay&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Rec_Bill_Pay")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("Rec_Bill_Pro",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("Rec_Bill_Pro")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("Rec_Bill_Pro",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("Rec_Bill_Pro",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(myEO2.getNIM3_Encounter().isWrite("Rec_Bill_Pro",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top nowrap><p class=<%=theClass%> ><b>Date Bill Rec From Provider&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><input maxlength=20  type=text size="20" name="Rec_Bill_Pro" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getRec_Bill_Pro())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pro&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Rec_Bill_Pro")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;<%=PLCUtils.getDaysTill_Display(myEO2.getNIM3_Encounter().getRec_Bill_Pro(),false)%></p></td></tr>
                        <%
            }
            else if (isScheduler&&(myEO2.getNIM3_Encounter().isRead("Rec_Bill_Pro",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top nowrap><p class=<%=theClass%> ><b>Original Bill Rec By Provider&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=top><p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getRec_Bill_Pro())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Rec_Bill_Pro&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Rec_Bill_Pro")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("AmountBilledByProvider",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("AmountBilledByProvider")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("AmountBilledByProvider",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("AmountBilledByProvider",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO2.getNIM3_Encounter().isWrite("AmountBilledByProvider",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top nowrap><p class=<%=theClass%> ><b>Amount Billed By Provider:&nbsp;</b></p></td><td valign=top colspan="2"><p><input name="AmountBilledByProvider"  type=text disabled value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(myEO2.getNIM3_Encounter().getAmountBilledByProvider())%>" size="20" maxlength=10 readonly="readonly">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AmountBilledByProvider&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("AmountBilledByProvider")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((myEO2.getNIM3_Encounter().isRead("AmountBilledByProvider",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top nowrap><p class=<%=theClass%> ><b>AmountBilledByProvider&nbsp;</b></p></td><td valign=top colspan="2"><p><%=PLCUtils.getDisplayDefaultDecimalFormat2(myEO2.getNIM3_Encounter().getAmountBilledByProvider())%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AmountBilledByProvider&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("AmountBilledByProvider")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                         
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

<tr>
                  <td colspan="3" valign=top>&nbsp;

 <strong>Billing 1.0 Reference</strong>&nbsp;<input name="button1" type="button" id="button1" value="Show" onClick="document.getElementById('encounter_archive').style.display = 'block';">&nbsp;<input name="button1" type="button" id="button1" value="Hide" onClick="document.getElementById('encounter_archive').style.display = 'none';">

<div id="encounter_archive" style="display:none">
<table border=5 cellpadding="0" cellspacing="0">

                        <tr>
                        <td valign=top bgcolor="#EDF7E1"><span class="tdBaseAltRed">ArchivedData</span></td>
                      </tr>
                        <tr>
                          <td valign=top bgcolor="#EDF7E1"><table width="25%" border="1" cellspacing="0" cellpadding="2">
                            <tr class=tdHeaderAlt>
                              <td colspan="3" align="center">Payment Sent <u>To</u> Provider</td>
                            </tr>
                            <tr class=tdHeaderAlt>
                              <td>Check #&nbsp;</td>
                              <td>Amount</td>
                              <td>Date</td>
                            </tr>
                            <tr>
                              <td><input name="PaidToProviderCheck1Number" type=text disabled id="PaidToProviderCheck1Number" value="<%=myEO2.getNIM3_Encounter().getPaidToProviderCheck1Number()%>" size="15" maxlength="20" readonly="readonly">&nbsp;</td>
                              <td><input name="PaidToProviderCheck1Amount"  type=text disabled id="PaidToProviderCheck1Amount" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(myEO2.getNIM3_Encounter().getPaidToProviderCheck1Amount())%>" size="10" maxlength=10 readonly="readonly">&nbsp;</td>
                              <td><input name="PaidToProviderCheck1Date"  type=text disabled id="PaidToProviderCheck1Date" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getPaidToProviderCheck1Date())%>" /></jsp:include>' size="15" maxlength=10 readonly="readonly" >&nbsp;</td>
                            </tr>
                            <tr>
                              <td><input name="PaidToProviderCheck2Number" type=text disabled id="PaidToProviderCheck2Number" value="<%=myEO2.getNIM3_Encounter().getPaidToProviderCheck2Number()%>" size="15" maxlength="20" readonly="readonly">&nbsp;</td>
                              <td><input name="PaidToProviderCheck2Amount"  type=text disabled id="PaidToProviderCheck2Amount" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(myEO2.getNIM3_Encounter().getPaidToProviderCheck2Amount())%>" size="10" maxlength=10 readonly="readonly">&nbsp;</td>
                              <td><input name="PaidToProviderCheck2Date"  type=text disabled id="PaidToProviderCheck2Date" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getPaidToProviderCheck2Date())%>" /></jsp:include>' size="15" maxlength=10 readonly="readonly" >&nbsp;</td>
                            </tr>
                            <tr>
                              <td><input name="PaidToProviderCheck3Number" type=text disabled id="PaidToProviderCheck3Number" value="<%=myEO2.getNIM3_Encounter().getPaidToProviderCheck3Number()%>" size="15" maxlength="20" readonly="readonly">&nbsp;</td>
                              <td><input name="PaidToProviderCheck3Amount"  type=text disabled id="PaidToProviderCheck3Amount" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(myEO2.getNIM3_Encounter().getPaidToProviderCheck3Amount())%>" size="10" maxlength=10 readonly="readonly">&nbsp;</td>
                              <td><input name="PaidToProviderCheck3Date"  type=text disabled id="PaidToProviderCheck3Date" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getPaidToProviderCheck3Date())%>" /></jsp:include>' size="15" maxlength=10 readonly="readonly" >&nbsp;</td>
                            </tr>
</table>&nbsp;</td>
                      </tr>
                </table>
                
                </div>
                </td></tr>








            <%
            if ( (myEO2.getNIM3_Encounter().isRequired("DateOfService",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("DateOfService")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("DateOfService",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("DateOfService",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO2.getNIM3_Encounter().isWrite("DateOfService",UserSecurityGroupID)))
            {
                        %><tr><td colspan="3"><hr></td></tr>
                        <tr><td valign=middle nowrap="nowrap"><p class=<%=theClass%> ><b>Official Date Of Service<br />
        (mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=middle><p><input maxlength=20  type=text size="30" name="DateOfService" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getDateOfService())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfService&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("DateOfService")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((myEO2.getNIM3_Encounter().isRead("DateOfService",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=middle><p class=<%=theClass%> ><b>Date Of Service&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td colspan="2" valign=middle><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getDateOfService())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfService&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("DateOfService")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>
       <%
            if ( (myEO2.getNIM3_Encounter().isRequired("Comments",UserSecurityGroupID))&&(!myEO2.getNIM3_Encounter().isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO2.getNIM3_Encounter().isExpired("Comments",expiredDays))&&(myEO2.getNIM3_Encounter().isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(myEO2.getNIM3_Encounter().isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=middle><p class=<%=theClass%> ><b><%=myEO2.getNIM3_Encounter().getEnglish("Comments")%>&nbsp;</b></p></td><td colspan="2" valign=middle><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=myEO2.getNIM3_Encounter().getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(myEO2.getNIM3_Encounter().isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=middle> <p class=<%=theClass%> ><b><%=myEO2.getNIM3_Encounter().getEnglish("Comments")%>&nbsp;</b></p></td><td colspan="2" valign=middle><p><%=myEO2.getNIM3_Encounter().getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&amp;sFieldNameDisp=<%=myEO2.getNIM3_Encounter().getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <tr><td>&nbsp;</td><td colspan="2">&nbsp;</td></tr>
            </table>
        </td></tr></table>
        <!-- Start -->
<%
    bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID,"","ServiceID");

//declaration of Enumeration
    bltNIM3_Service        NIM3_Service;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Service_List.elements();
    %>
 <table border="0" bordercolor="333333" cellpadding="0"  cellspacing="0">        <tr><td>
<br />

        </td></tr>
    <%
    int iExpress=0;
    int iMaxExpress=8;
//    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tNIM3_Service"))
//    while (eList.hasMoreElements()||iExpress<iMaxExpress)
    while (eList.hasMoreElements())
    {
       iExpress++;
         %>
            <tr>
              <td width="50%">
                   <table border="0" bordercolor="333333" cellpadding="0" class=tdHeaderAlt cellspacing="0" width="100%">
                   <tr> 
                   	<td rowspan="2"><img src=express/left-corner.gif></td>
                   	<td width=100%><img width=100% height=2 src=express/small-line.gif></td>
                   	<td rowspan="2" align=right><img src=express/right-corner.gif></td>
                   </tr>
                     <tr> 
                       <td>
         <%

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        NIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
      }
      else
      {
        NIM3_Service  = new bltNIM3_Service();
        isNewRecord= true;
      }
        NIM3_Service.GroupSecurityInit(UserSecurityGroupID);
//        String theClass = "tdHeader";
        %>
               <span class=<%=theClass%> ><b><%=ConfigurationMessages.getDataCategory("tNIM3_Service")%> #<%=iExpress%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[SID: <%=NIM3_Service.getServiceID()%>]</span>
                  </td></tr></table>
            </td></tr>
                     <tr><td>
<%String theClassF = "textBase";%>
<%
if (isNewRecord)
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="new">
<%}
else
{%>
<input type=hidden name=recordItemStatus_<%=iExpress%>="edit">
<%}

        %>

          <%  theClass ="tdBase";%>
        <table width=100% border=1 cellpadding=3 cellspacing=0 bordercolor=#333333 class=tableColor><tr>
          <td>
        <table cellpadding=0 border=0 cellspacing=0 width=100%>




                     <tr>
                       <td colspan=4 valign=top> <b>Status&nbsp;</b><select     name="ServiceStatusID_<%=iExpress%>"  id="ServiceStatusID_<%=iExpress%>" ><jsp:include page="../generic/tServiceStatusLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_Service.getServiceStatusID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ServiceStatusID&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("ServiceStatusID")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;</td>
                     </tr>
                     <tr>
                       <td colspan=4 valign=top><span class="<%=theClass%>"><b>CPT*&nbsp;</b>
                           <input maxlength="5" type=text size="10" name="CPT_<%=iExpress%>" id="CPT_<%=iExpress%>" value="<%=NIM3_Service.getCPT()%>">
                           &nbsp;
                           <%if (isShowAudit){%>
                           <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CPT&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("CPT")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                           <%}%>
                           <b>CPT Modifier&nbsp;</b>
                           <input maxlength="10" type=text size="10" name="CPTModifier_<%=iExpress%>" value="<%=NIM3_Service.getCPTModifier()%>">
                           &nbsp;
                           <%if (isShowAudit){%>
                           <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CPTModifier&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("CPTModifier")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                           <%}%>
&nbsp;</span>&nbsp;&nbsp;CPT BodyPart&nbsp;<strong><%=NIM3_Service.getCPTBodyPart()%></strong></td></tr>


                     <tr><td colspan="4" valign=top>
<p class=<%=theClass%> ><b>Diagnosis/Rule-out&nbsp;</b><input name="CPTText_<%=iExpress%>" type=text value="<%=NIM3_Service.getCPTText()%>" size="40" maxlength="100">
                     &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CPTText&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("CPTText")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
<p class=<%=theClass%> ><b>Diag/ICD 1&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT1_<%=iExpress%>" value="<%=NIM3_Service.getdCPT1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT1&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT1")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>

                     <b>2&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT2_<%=iExpress%>" value="<%=NIM3_Service.getdCPT2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT2&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT2")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                     
                     <b>3&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT3_<%=iExpress%>" value="<%=NIM3_Service.getdCPT3()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT3&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT3")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                     
                     <b>4&nbsp;</b><input maxlength="100" type=text size="10" name="dCPT4_<%=iExpress%>" value="<%=NIM3_Service.getdCPT4()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=dCPT4&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("dCPT4")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                     
                     
                     
                     
                     
                     </p>
                     <hr/></td></tr>
  <tr>
    <td width="50%" valign=top><table border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td class="tdHeaderAltDark">Reference Data<span class="tdHeaderBright"></span></td>
      </tr>
      <tr>
        <td>
        
        
        <table border="1" cellspacing="3" cellpadding="2">
          <tr>
            <td colspan="2" align="right" valign=middle nowrap>Quantity</td>
            <td><input maxlength="2" type=text size="4" name="CPTQty_<%=iExpress%>" value="<%=NIM3_Service.getCPTQty()%>"></td>
            <td><input maxlength="2" type=text size="4" name="CPTQty_Bill_<%=iExpress%>" value="<%=NIM3_Service.getCPTQty_Bill()%>"></td>
            <td><input maxlength="2" type=text size="4" name="CPTQty_Pay_<%=iExpress%>" value="<%=NIM3_Service.getCPTQty_Pay()%>"></td>
          </tr>
          <tr class="tdHeaderAlt">
            <td>Type</td>
            <td>Unit</td>
            <td>Actual</td>
            <td>Bill</td>
            <td>Pay</td>
          </tr>
          <tr>
            <td valign=middle nowrap><span class="<%=theClass%>"><b>Fee Schedule Amount:&nbsp;</b></span></td>
            <td valign=middle nowrap><p>
              <input maxlength=10  type=text size="5" name="Reference_FeeScheduleAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReference_FeeScheduleAmount())%>">
              &nbsp;
              <%if (isShowAudit){%>
              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Reference_FeeScheduleAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("Reference_FeeScheduleAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
              <%}%>
              </p></td>
            <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(NIM3_Service.getReference_FeeScheduleAmount() * NIM3_Service.getCPTQty() )%></td>
            <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(NIM3_Service.getReference_FeeScheduleAmount() * NIM3_Service.getCPTQty_Bill() )%></td>
            <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(NIM3_Service.getReference_FeeScheduleAmount() * NIM3_Service.getCPTQty_Pay() )%></td>
          </tr>
          <tr>
            <td valign=middle nowrap><span class="<%=theClass%>"><b>U&amp;C Amount:&nbsp;</b></span></td>
            <td valign=middle nowrap><p>
              <input maxlength=10  type=text size="5" name="Reference_UCAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReference_UCAmount())%>">
              &nbsp;
              <%if (isShowAudit){%>
              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Reference_UCAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("Reference_UCAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
              <%}%>
            </p></td>
            <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(NIM3_Service.getReference_UCAmount() * NIM3_Service.getCPTQty() )%></td>
            <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(NIM3_Service.getReference_UCAmount() * NIM3_Service.getCPTQty_Bill() )%></td>
            <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(NIM3_Service.getReference_UCAmount() * NIM3_Service.getCPTQty_Pay() )%></td>
            </tr>
          <tr>
            <td valign=middle nowrap><span class="<%=theClass%>"><b>Payer Allow Amount:&nbsp;</b></span></td>
            <td valign=middle nowrap><p>
              <input maxlength=10  type=text size="5" name="Reference_PayerContractAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReference_PayerContractAmount())%>">
              &nbsp;
              <%if (isShowAudit){%>
              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Reference_PayerContractAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("Reference_PayerContractAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
              <%}%>
            </p></td>
            <td class="inputButton_sm_Stop">&nbsp;</td>
            <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(NIM3_Service.getReference_PayerContractAmount() * NIM3_Service.getCPTQty_Bill() )%></td>
            <td class="inputButton_sm_Stop">&nbsp;</td>
            </tr>
          <tr>
            <td valign=middle nowrap><span class="<%=theClass%>"><b>Payer Bill Amount:&nbsp;</b></span></td>
            <td valign=middle nowrap><p>
              <input maxlength=10  type=text size="5" name="Reference_PayerBillAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReference_PayerBillAmount())%>">
              &nbsp;
              <%if (isShowAudit){%>
              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Reference_PayerBillAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("Reference_PayerBillAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
              <%}%>
            </p></td>
            <td class="inputButton_sm_Stop">&nbsp;</td>
            <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(NIM3_Service.getReference_PayerBillAmount() * NIM3_Service.getCPTQty_Bill() )%></td>
            <td class="inputButton_sm_Stop">&nbsp;</td>
            </tr>
          <tr>
            <td valign=middle nowrap><p class=<%=theClass%> ><b>Provider Contract Amount:&nbsp;</b></p></td>
            <td valign=middle nowrap><p>
              <input name="Reference_ProviderContractAmount_<%=iExpress%>"  type=text id="Reference_ProviderContractAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReference_ProviderContractAmount())%>" size="5" maxlength=10>
              &nbsp;
              <%if (isShowAudit){%>
              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Reference_ProviderContractAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("Reference_ProviderContractAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
              <%}%>
              </p></td>
            <td class="inputButton_sm_Stop">&nbsp;</td>
            <td class="inputButton_sm_Stop">&nbsp;</td>
            <td class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(NIM3_Service.getReference_ProviderContractAmount() * NIM3_Service.getCPTQty_Pay() )%></td>
          </tr>
          <tr>
            <td valign=middle nowrap><p class=<%=theClass%> ><b>Provider Billed Amount:&nbsp;</b></p></td>
            <td valign=middle nowrap class="borderHighlightGreen"><%=PLCUtils.getDisplayDefaultDecimalFormatCurrency1(NIM3_Service.getReference_ProviderBilledAmount() / NIM3_Service.getCPTQty_Pay() )%>
            </td>
            <td class="inputButton_sm_Stop">&nbsp;</td>
            <td class="inputButton_sm_Stop">&nbsp;</td>
            <td><p>
              <input name="Reference_ProviderBilledAmount_<%=iExpress%>"  type=text id="Reference_ProviderBilledAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReference_ProviderBilledAmount())%>" size="4" maxlength=10>
              &nbsp;
              <%if (isShowAudit){%>
              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Reference_ProviderBilledAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("Reference_ProviderBilledAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
              <%}%>
            </p>           
</td>
          </tr>
        </table>
</td>
      </tr>
      <tr>
        <td class="tdHeaderAltDark">Billing Transactions&nbsp;&nbsp;&nbsp;          <input class="inputButton_sm_Create" onClick = "if (confirm('Please save your data first')){this.disabled=true;modalPost('AddBilling', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_ServiceBillingTransaction_form.jsp&hideCase=y&EDIT=new&EDITID=<%=NIM3_Service.getServiceID()%>','UploadDoc','dialogWidth:600px;dialogHeight:400px','status=yes,scrollbars=yes,resizable=yes,width=1000,height=800'));document.location=document.location}" type=Submit value="ADD" name=Submit2></td>
      </tr>
      <tr >
        <td><table border="1" cellspacing="0" cellpadding="1" bgcolor="#EEEEEE" bordercolor="#999999">
          <tr class="tdHeaderAlt">
            <td align="center" nowrap>#</td>
            <td nowrap>Type</td>
            <td nowrap>Created</td>
            <td nowrap>Title</td>
            <td nowrap>Amount</td>
            <td nowrap>Note</td>
            <td nowrap>Ref #</td>
            <td nowrap>Ref Date</td>
            <td nowrap>Export</td>
            <td nowrap>UID</td>
          </tr>
       
			<%
            bltNIM3_ServiceBillingTransaction_List        bltNIM3_ServiceBillingTransaction_List        =    new    bltNIM3_ServiceBillingTransaction_List(NIM3_Service.getServiceID());
            
            //declaration of Enumeration
                bltNIM3_ServiceBillingTransaction        working_bltNIM3_ServiceBillingTransaction;
                ListElement        leCurrentElement_SBT;
                java.util.Enumeration eList_SBT = bltNIM3_ServiceBillingTransaction_List.elements();
                int altCnt = 0;
				Double TotalRevenue = 0.0;
				Double TotalAR = 0.0;
				Double TotalCOS = 0.0;
				Double TotalAP = 0.0;
				
                if (eList_SBT.hasMoreElements())
                {
                 while (eList_SBT.hasMoreElements())
                 {
                    altCnt++;
                    String theClass_SBT = "tdBase";
                    if (altCnt%2!=0)
                    {
                        theClass_SBT = "tdBaseAlt";
                    }
                    leCurrentElement_SBT    = (ListElement) eList_SBT.nextElement();
                    working_bltNIM3_ServiceBillingTransaction  = (bltNIM3_ServiceBillingTransaction) leCurrentElement_SBT.getObject();
                    working_bltNIM3_ServiceBillingTransaction.GroupSecurityInit(UserSecurityGroupID);
					if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.REVENUE_TYPE||working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.REVENUEADJUSTMENT_TYPE)
					{
						TotalRevenue+=working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
					}
					else if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.COS_TYPE||working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.COSADJUSTMENT_TYPE)
					{
						TotalCOS+=working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
					}
					else if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.ARPAYMENT_TYPE)
					{
						TotalAR+=working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
					}
					else if (working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()==NIM3_BillingObject2.APPAYMENT_TYPE)
					{
						TotalAP+=working_bltNIM3_ServiceBillingTransaction.getTransactionAmount();
					}
					%>
					  <tr class="<%=theClass_SBT%>">
						<td align="center"><%=altCnt%></td>
						<td><jsp:include page="../generic/tServiceBillingTransactionTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_ServiceBillingTransaction.getServiceBillingTransactionTypeID()%>" /></jsp:include>
						&nbsp;</td>
						<td><%=PLCUtils.getDisplayDate(working_bltNIM3_ServiceBillingTransaction.getUniqueCreateDate(),true)%>&nbsp;</td>
						<td><%=working_bltNIM3_ServiceBillingTransaction.getTransactionTitle()%>&nbsp;</td>
						<td><%=PLCUtils.getDisplayDefaultDecimalFormat2(working_bltNIM3_ServiceBillingTransaction.getTransactionAmount())%></td>
						<td><%=working_bltNIM3_ServiceBillingTransaction.getTransactionNote()%>&nbsp;</td>
						<td><%=working_bltNIM3_ServiceBillingTransaction.getReferenceNumber()%>&nbsp;</td>
						<td><%=PLCUtils.getDisplayDate(working_bltNIM3_ServiceBillingTransaction.getReferenceDate(),false)%>&nbsp;</td>
						<td><%=PLCUtils.getDisplayDate(working_bltNIM3_ServiceBillingTransaction.getExportedDate(),false)%>&nbsp;</td>
						<td><%=working_bltNIM3_ServiceBillingTransaction.getGenerated_UserID()%>&nbsp;</td>
					  </tr>
                    <%
				 }
					%>
					  <tr >
<td colspan=2>	Total Revenue: <strong><%=PLCUtils.getDisplayDefaultDecimalFormat2(TotalRevenue)%></strong><br>
										Total AR/P: <strong><%=PLCUtils.getDisplayDefaultDecimalFormat2(TotalAR)%></strong><br>
										Remaining AR: <strong><%=PLCUtils.getDisplayDefaultDecimalFormat2(TotalRevenue-TotalAR)%></strong>
                        </td>
<td colspan=2>	Total COS: <strong><%=PLCUtils.getDisplayDefaultDecimalFormat2(TotalCOS)%></strong><br>
										Total AP/P: <strong><%=PLCUtils.getDisplayDefaultDecimalFormat2(TotalAP)%></strong><br>
										Remaining AP: <strong><%=PLCUtils.getDisplayDefaultDecimalFormat2(TotalCOS-TotalAP)%></strong>
                        </td>
						<td colspan=6>	<span class="borderHighlightBlackDottedNoFont tdBaseAltGreen">Net Expected Gain: <strong><%=PLCUtils.getDisplayDefaultDecimalFormat2(TotalRevenue-TotalCOS)%></strong>
						</span>							
                        </td>
					  </tr>
                    <%
				}
				%>


</table></td>
      </tr>
    </table></td>
    <td valign=top>&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td valign=top> 
    <strong>Billing 1.0 Reference</strong>&nbsp;<input name="button1" type="button" id="button1" value="Show" onClick="document.getElementById('archive_<%=iExpress%>').style.display = 'block';">&nbsp;<input name="button1" type="button" id="button1" value="Hide" onClick="document.getElementById('archive_<%=iExpress%>').style.display = 'none';">
    <div id="archive_<%=iExpress%>"   style="display:none;background-color:#DDD;padding:5px;">
      <p class=<%=theClass%> ><span class="tdBaseAltRed">ArchivedData</span>
    
      <p class=<%=theClass%> ><b>Bill Amount&nbsp;</b>&nbsp;$<input name="BillAmount_<%=iExpress%>" type=text disabled value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getBillAmount())%>" size="10" maxlength="10" readonly="readonly">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=BillAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("BillAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
        &nbsp;&nbsp;<b>Contract/Allow Amount&nbsp;</b>&nbsp;$
        <input name="AllowAmount_<%=iExpress%>" type=text disabled id="AllowAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getAllowAmount())%>" size="10" maxlength="10" readonly="readonly">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AllowAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("AllowAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;&nbsp;Adjustment <input name="AllowAmountAdjustment_<%=iExpress%>"  type=text disabled id="AllowAmountAdjustment_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getAllowAmountAdjustment())%>" size="8" maxlength=10 readonly="readonly">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AllowAmountAdjustment&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("AllowAmountAdjustment")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>&nbsp;&nbsp;<br><b>Received Amount&nbsp;</b>&nbsp;$<input name="ReceivedAmount_<%=iExpress%>" type=text disabled id="ReceivedAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReceivedAmount())%>" size="10" maxlength="10" readonly="readonly" <% if (!isScheduler3){%>readonly="readonly"<%}%> >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReceivedAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("ReceivedAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
        &nbsp;&nbsp;<b>Pay-out Amount&nbsp;</b>&nbsp;$
        <input name="PaidOutAmount_<%=iExpress%>" type=text disabled id="PaidOutAmount_<%=iExpress%>" value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getPaidOutAmount())%>" size="10" maxlength="10" readonly="readonly">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PaidOutAmount&amp;sTableName=tNIM3_Service&amp;sRefID=<%=NIM3_Service.getServiceID()%>&amp;sFieldNameDisp=<%=NIM3_Service.getEnglish("PaidOutAmount")%>&amp;sTableNameDisp=tNIM3_Service','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
        
      <table width="25%" border="1" cellspacing="0" cellpadding="2">
  <tr class=tdHeaderAlt>
    <td colspan="3" align="center">Payment Received <u>From</u> Payer</td>
    </tr>
  <tr class=tdHeaderAlt>
    <td>Received Check #&nbsp;</td>
    <td>Amount</td>
    <td>Date</td>
  </tr>
  <tr>
    <td><input name="ReceivedCheck1Number_<%=iExpress%>" type=text disabled id="ReceivedCheck1Number_<%=iExpress%>" value="<%=NIM3_Service.getReceivedCheck1Number()%>" size="10" maxlength="20" readonly="readonly">&nbsp;</td>
    <td><input name="ReceivedCheck1Amount_<%=iExpress%>"  type=text disabled id="ReceivedCheck1Amount_<%=iExpress%>"  value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReceivedCheck1Amount())%>" size="10" maxlength=10 readonly="readonly">&nbsp;</td>
    <td><input maxlength=10  type=text size="20" name="ReceivedCheck1Date_<%=iExpress%>" id="ReceivedCheck1Date_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Service.getReceivedCheck1Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
  <tr>
    <td><input name="ReceivedCheck2Number_<%=iExpress%>" type=text disabled id="ReceivedCheck2Number_<%=iExpress%>" value="<%=NIM3_Service.getReceivedCheck2Number()%>" size="10" maxlength="20" readonly="readonly">&nbsp;</td>
    <td><input name="ReceivedCheck2Amount_<%=iExpress%>"  type=text disabled id="ReceivedCheck2Amount_<%=iExpress%>"  value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReceivedCheck2Amount())%>" size="10" maxlength=10 readonly="readonly">&nbsp;</td>
    <td><input maxlength=10  type=text size="20" name="ReceivedCheck2Date_<%=iExpress%>" id="ReceivedCheck2Date_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Service.getReceivedCheck2Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
  <tr>
    <td><input name="ReceivedCheck3Number_<%=iExpress%>" type=text disabled id="ReceivedCheck3Number_<%=iExpress%>" value="<%=NIM3_Service.getReceivedCheck3Number()%>" size="10" maxlength="20" readonly="readonly">&nbsp;</td>
    <td><input name="ReceivedCheck3Amount_<%=iExpress%>"  type=text disabled id="ReceivedCheck3Amount_<%=iExpress%>"  value="<%=PLCUtils.getDisplayDefaultDecimalFormat2(NIM3_Service.getReceivedCheck3Amount())%>" size="10" maxlength=10 readonly="readonly">&nbsp;</td>
    <td><input maxlength=10  readonly="readonly" disabled type=text size="20" name="ReceivedCheck3Date_<%=iExpress%>" id="ReceivedCheck3Date_<%=iExpress%>" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Service.getReceivedCheck3Date())%>" /></jsp:include>' >&nbsp;</td>
  </tr>
</table>
</p>
</div>
  </td></tr>                     <tr>
                       <td valign=top>&nbsp;</td>
                       <td valign=top>&nbsp;</td>
                       <td valign=top>&nbsp;</td>
                       <td>&nbsp;</td>
                     </tr>


        </table>
        </td></tr></table>
        </td></tr>
        <tr><td>&nbsp;</td></tr>

        <%
    }
    %>
        <tr><td>
        <input type=hidden name=nextPage value="">
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
            <p><input class="inputButton_md_Create" onClick = "this.disabled=false;$('table').fadeOut('slow');show_pws_Calc();" type=Submit value="Update" name=Submit></p>
        <%}%>
        
        <hr>
        Audit Notes:<br>
        <textarea name="" cols="90" rows="5" readonly="readonly"><%=myEO2.getNIM3_Encounter().getAuditNotes()%></textarea>
        
        </td></tr> </table>

        </form>
    </table>





<!-- End -->            
            
            
            
            
            
            
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
    </td></tr></table>
<script language="javascript">
hide_pws_Loading();
</script>

