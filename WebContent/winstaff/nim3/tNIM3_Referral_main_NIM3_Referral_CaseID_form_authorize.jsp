<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*    " %>
<%/*
    filename: out\jsp\tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
<%@ include file="../generic/generalDisplay.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (isScheduler&&pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }
    else if (pageControllerHash.containsKey("iAdjusterID")&&pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
//        iAdjusterID        =    (Integer)pageControllerHash.get("iAdjusterID");
		isAdjuster = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID2")&&pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster2 = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID3")&&pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster3 = true;
        accessValid = true;
	}
	else if (isProvider&&pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
		isProvider = true;
	}



//page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }
    bltNIM3_Referral_List        bltNIM3_Referral_List        =    new    bltNIM3_Referral_List(iCaseID,"ReferralID="+requestID,"");

//declaration of Enumeration
    bltNIM3_Referral        working_bltNIM3_Referral;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Referral_List.elements();
    %>
    <%
    if (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_Referral  = (bltNIM3_Referral) leCurrentElement.getObject();
        pageControllerHash.put("iReferralID",working_bltNIM3_Referral.getReferralID());
        pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
	boolean noResponse = false;
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tNIM3_Referral_form.jsp?nullParam=null"+parameterPassString    ;
        if (request.getParameter("EDIT").equalsIgnoreCase("del"))
        {
            targetRedirect = "tNIM3_Referral_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("addct"))
        {
            targetRedirect = "tNIM3_CommTrack_main_NIM3_CommTrack_CaseID_form_create.jsp?nullParam=null"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("uprx")||request.getParameter("EDIT").equalsIgnoreCase("upor"))
        {
//            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize")   ;
//            targetRedirect = "tNIM3_Referral_upRx.jsp";  ;
			if (isScheduler)
			{
	            targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp"   ;
				%>
				<script language="javascript">
				modalPost('UploadRX', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_upDoc.jsp&nullParam=null<%=parameterPassString%>','UploadRX','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));
				document.location = '<%=targetRedirect%>';
				</script>
				<%
			noResponse = true;
			}
			else if (isAdjuster||isAdjuster2||isAdjuster3||isProvider)
			{
	            targetRedirect = "tNIM3_Encounter_upDoc.jsp?nobox=yes&nullParam=null"+parameterPassString    ;
			}


        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("openflow"))
        {
            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize")   ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("newexpress"))
        {
				bltNIM3_Encounter  NIME = new bltNIM3_Encounter();

				NIME.setReferralID(working_bltNIM3_Referral.getUniqueID());
//				NIME.setReferringPhysicianID(CurrentUserAccount.getUniqueID());
				String tempAN = "[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nCreated (express)";
				NIME.setAuditNotes(tempAN);
				NIME.setEncounterStatusID(new Integer(1));
				NIME.commitData();
				NIME.setScanPass("SP" +  NIME.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());								
				NIME.commitData();
				pageControllerHash.put("iEncounterID",NIME.getUniqueID());
				targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp";

					%>
				<script language="javascript">
				modalPost('EditNewEncounter', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_CaseAccount_express1_form.jsp&QUICKVIEW=yes&EDIT=nim3exp&EDITID=<%=NIME.getUniqueID()%>&KM=p','EditNewEncounter','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));
				document.location = '<%=targetRedirect%>';
				</script>
				<%
			noResponse = true;
}
        session.setAttribute("pageControllerHash",pageControllerHash);
		
		if (!noResponse)
		{
	        response.sendRedirect(targetRedirect);
		}
//        response.sendRedirect(targetRedirect);
    }
    else
    {
   out.println("invalid where query");
    }

  }
  else
  {
   out.println("illegal");
  }
}
else
{
out.println("Your Security Level does not permit you to View this.");
}
%>




