<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
<%@ include file="../generic/generalDisplay.jsp" %>



<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

    <table cellpadding=5 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM_Encounter")%>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat("MM/dd/yyyy");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
	    String theMessage = request.getParameter("problemReport");
		String UserLogonDescription = "n/a";
        bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
		if (pageControllerHash.containsKey("UserLogonDescription"))
		{
			UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
		}
		String displayNotes = "";
		
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);
		myEO.NIM3_Encounter.setSeeNetDev_Waiting(new Integer(1));
		myEO.NIM3_Encounter.setSeeNetDev_Flagged(new Integer(1));
		myEO.NIM3_Encounter.setSeeNetDev_ReqSentToNetDev(PLCUtils.getNowDate(true));
		myEO.NIM3_Encounter.setSeeNetDev_ReqSentToNetDev_UserID(CurrentUserAccount.getUserID());
		
		myEO.NIM3_Encounter.commitData();
        bltNIM3_CommTrack        working_bltNIM3_CommTrack = new bltNIM3_CommTrack();
        working_bltNIM3_CommTrack.setEncounterID(iEncounterID);
        working_bltNIM3_CommTrack.setReferralID(myEO.NIM3_Encounter.getReferralID());
        working_bltNIM3_CommTrack.setCaseID(myEO.NIM3_CaseAccount.getUniqueID());
        working_bltNIM3_CommTrack.setMessageName(CurrentUserAccount.getLogonUserName());
        working_bltNIM3_CommTrack.setMessageCompany(myPM.getPayerName());
        working_bltNIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
        working_bltNIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
        working_bltNIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
//        working_bltNIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));  //no more end date
        working_bltNIM3_CommTrack.setUniqueModifyComments(""+UserLogonDescription);
		working_bltNIM3_CommTrack.setMessageText(theMessage);
		working_bltNIM3_CommTrack.setCommTypeID(new Integer(4));    //See ND Typ
        working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(0));  // keep open
		emailType_V3 myEM = new emailType_V3();
		myEM.setTo("netdev.managers@nextimagemedical.com");
		myEM.setFrom("server@nextimagemedical.com");
		myEM.setSubject("See NetDev Request: SP: " + myEO.NIM3_Encounter.getScanPass() + "[" + myEO.NIM3_CaseAccount.getPatientLastName() + "]");
		myEM.setBody(theMessage);
		myEM.isSendMail();
		displayNotes = "Email sent to NetDev";
        working_bltNIM3_CommTrack.commitData();
		
		
		
		
%>
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td class="tdHeader"><%=displayNotes%></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="left"><input type="button" class="inputButton_md_Default" onclick="this.disabled=true;window.close();" value="Continue" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_nad.jsp';" value="Edit Next Action Date" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr> 
    </table>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

