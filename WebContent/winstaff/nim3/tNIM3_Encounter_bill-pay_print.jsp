<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="java.text.DecimalFormat ,com.winstaff.*"%>
<%
	/*

	 filename: out\jsp\tmyEO.NIM3_Encounter_form.jsp
	 Created on May/28/2008
	 Created by: Scott Ellis
	 */
%>

<%@ include file="../generic/CheckLogin.jsp"%>

<%
	String tnIncludeFN = "ui_" + thePLCID
			+ "\\top-nav_Clear.jsp?plcID=" + thePLCID;
%>
<%@ include file="../generic/generalDisplay.jsp"%>

<jsp:include page="<%=tnIncludeFN%>" flush="true"></jsp:include>




<%
	//initial declaration of list class and parentID
	Integer iEncounterID = null;
	boolean accessValid = false;
	// required for Type2
	String sKeyMasterReference = null;

	Integer iSecurityCheck = SecurityCheck.CheckItem("nim1",
			UserSecurityGroupID);
	if (iSecurityCheck.intValue() != 0) {
		if (pageControllerHash.containsKey("iEncounterID")) {
			iEncounterID = (Integer) pageControllerHash
					.get("iEncounterID");
			accessValid = true;
		}
		//page security
		if (accessValid) {
			java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(
					PLCUtils.String_dbdf);
			java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(
					PLCUtils.String_dbdft);
			java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(
					PLCUtils.String_displayDateSDF1);
			java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat(
					"MM/dd/yyyy hh:mm a");

			pageControllerHash.put("sLocalChildReturnPage",
					"tmyEO.NIM3_Encounter_form.jsp");
			//initial declaration of list class and parentID
			//    bltmyEO.NIM3_Encounter        myEO.NIM3_Encounter        =    null;
			NIM3_EncounterObject myEO = null;

			if (request.getParameter("EDIT").equalsIgnoreCase("edit")
					|| request.getParameter("EDIT").substring(0, 5)
							.equalsIgnoreCase("print")
					|| request.getParameter("EDIT").equalsIgnoreCase(
							"sched")) {
				myEO = new NIM3_EncounterObject(iEncounterID);
			}

			bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(
					myEO.NIM3_CaseAccount.getPayerID());
			bltUserAccount myBT = new bltUserAccount(
					myEO.NIM3_CaseAccount.getBillToContactID());
			bltNIM3_PayerMaster myBTPM = new bltNIM3_PayerMaster(
					myBT.getPayerID());

			bltPracticeMaster myPracM = new bltPracticeMaster(
					myEO.NIM3_Appointment.getProviderID());

			//bltICMaster myICM = new bltICMaster (myEO.NIM3_Appointment.getProviderID());
			java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(
					PLCUtils.String_displayDateDay);
			java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(
					PLCUtils.String_displayDateDayWeek);
			java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(
					PLCUtils.String_displayDateMonth);
			java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(
					PLCUtils.String_displayDateYear);
			java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(
					PLCUtils.String_displayDateHour);
			java.util.Date dbAppointment = myEO.NIM3_Appointment
					.getAppointmentTime();
			String sAppointmentTime = displayDateTimeSDF
					.format((dbAppointment));
			String sAppointmentTimeDay = displayDateDay
					.format((dbAppointment));
			String sAppointmentTimeDayWeek = displayDateDayWeek
					.format((dbAppointment));
			String sAppointmentTimeMonth = displayDateMonth
					.format((dbAppointment));
			String sAppointmentTimeYear = displayDateYear
					.format((dbAppointment));
			String sAppointmentHour = displayDateHour
					.format((dbAppointment));

			//fields
%>
<%
	String theClass = "tdBase";
%>
<table cellpadding=0 cellspacing=0 border=0 width=100%>
	<tr class=title>
		<td width="10">&nbsp;</td>
		<td><table width="100%" border="0" class="certborderPrint" cellspacing="0" cellpadding="10">
				<tr>
					<td><table width="100%" border="2" cellpadding="5" cellspacing="0" class="certborderPrintInt">
							<tr>
								<td colspan="2" valign="top" align="right" class="big3"><p>
										<img src="images/logo1.jpg" width="250" height="70"/><br /> 
									</p></td>
							</tr>
							<tr>
								<td colspan="2" valign="top"><font size="5" style="font-size: medium;">Billing Invoice for ScanPass: <strong><%=myEO.NIM3_Encounter.getScanPass()%></strong></font></td>
							</tr>
							<tr>
								<td colspan="2" valign="top">Patient Information</td>
							</tr>
							<tr>
								<td colspan="2" valign="top"><p>
										
									<table>
										<tr>
											<td>Patient: <strong><%=myEO.NIM3_CaseAccount.getPatientFirstName()%> <%=myEO.NIM3_CaseAccount.getPatientLastName()%></strong><br /><strong><%=myEO.NIM3_CaseAccount.getPatientAddress1()%> <%=myEO.NIM3_CaseAccount.getPatientAddress2()%><br /> <%=myEO.NIM3_CaseAccount.getPatientCity()%>, <%=new bltStateLI(myEO.NIM3_CaseAccount
							.getPatientStateID()).getShortState()%> <%=myEO.NIM3_CaseAccount.getPatientZIP()%></strong></td>
											<td>
												<p style="margin-left:40px">
													Gender: <strong><%=(myEO.NIM3_CaseAccount.getPatientGender())%></strong><br /> DOB: <strong><%=displayDateSDF.format(myEO.NIM3_CaseAccount
							.getPatientDOB())%></strong><Br>&nbsp;
												</p>
											</td>
											<td>
												<p style="margin-left:40px">
													Date of Injury: <strong> <%
 	if (myEO.NIM3_CaseAccount.getDateOfInjury().after(
 					NIMUtils.getBeforeTime())) {
 				out.print(displayDateSDF.format(myEO.NIM3_CaseAccount
 						.getDateOfInjury()));
 			} else {
 				out.print("Unknown");
 			}
 %>
													</strong><br /> Employer: <strong><%=myEO.NIM3_CaseAccount.getEmployerName()%></strong><Br>&nbsp;
												</p>
											</td>
										<tr>
									</table></td>
							</tr>
							<tr>
								<td colspan="2" valign="top">

									<table border="1" cellpadding="3" cellspacing="0">
										<tr class="tdHeaderAlt">
											<td>Date Of Service</td>
											<td>CPT</td>
											<td>Mod</td>
											<td>Bill Amount</td>
											<td>Allow Amount</td>
											<td>Desc</td>
											<td>Diagnosis/ICD</td>
										</tr>
										<%
											searchDB2 mySS = new searchDB2();

													java.sql.ResultSet myRS = null;;

													try {
														String mySQL = ("select * from tNIM3_Service where EncounterID = "
																+ myEO.NIM3_Encounter.getEncounterID() + " order by ServiceID");
														//out.println(mySQL);
														myRS = mySS.executeStatement(mySQL);
													} catch (Exception e) {
														out.println("ResultsSet:" + e);
													}

													String myMainTable = " ";
													int endCount = 0;
													int cnt = 0;
													int cnt2 = 0;
													while (myRS != null && myRS.next()) {
														cnt++;
														bltNIM3_Service working_bltNIM3_Service = new bltNIM3_Service(
																new Integer(myRS.getString("ServiceID")));
										%>
										<tr>
											<td><%=displayDateSDF.format(myEO.NIM3_Encounter
								.getDateOfService())%></td>
											<td><%=working_bltNIM3_Service.getCPT()%></td>
											<td><%=working_bltNIM3_Service.getCPTModifier()%></td>
											<td>$<%=PLCUtils
								.getDisplayDefaultDecimalFormat2(working_bltNIM3_Service
										.getBillAmount())%></td>
											<td>$<%=PLCUtils
								.getDisplayDefaultDecimalFormat2(working_bltNIM3_Service
										.getAllowAmount())%></td>
											<td><%=NIMUtils.getCPTText(working_bltNIM3_Service
								.getCPT())%><br /> BP: <%=NIMUtils.getCPTText(working_bltNIM3_Service
								.getCPTBodyPart())%></td>
											<td><strong><%=working_bltNIM3_Service.getdCPT1()%></strong> <%=working_bltNIM3_Service.getdCPT2()%> <strong><%=working_bltNIM3_Service.getdCPT3()%></strong> <%=working_bltNIM3_Service.getdCPT4()%>&nbsp;</td>
										</tr>
										<%
											}
										%>
									</table>

								</td>
							</tr>
							<tr>
								<td colspan="2" valign="top"><p>Place of Service:</p> <strong><%=myPracM.getPracticeName()%></strong><br /> <%=myPracM.getOfficeAddress1()%><br /> <%=myPracM.getOfficeCity()%>, <%=new bltStateLI(myPracM.getOfficeStateID())
							.getShortState()%> <%=myPracM.getOfficeZIP()%><br /> <a href="http://maps.google.com/maps?q=<%=myPracM.getOfficeAddress1()%>,<%=myPracM.getOfficeZIP()%>" target="_blank">View Map</a><br />
									Phone: <%=myPracM.getOfficePhone()%><br /> <%=myPracM.getPracticeID()%></td>
							</tr>
							<tr>
								<td valign="top"><p>Bill To Insurance Information:</p> <%
 	NIM3_AddressObject N3AO = myEO.getWhoShouldIBillTo();
 %>
									<p>
										<strong><%=N3AO.getAddressName()%></strong><br />
										<%=N3AO.getAddress1()%><br />
										<%=N3AO.getAddress2()%><br />
										<%=N3AO.getCity()%>,
										<%=N3AO.getState()%>
										<%=N3AO.getZIP()%>



										<br /> WC Claim #:
										<%=myEO.NIM3_CaseAccount.getCaseClaimNumber()%><br /> Authorization #:
										<%=myEO.NIM3_Referral
							.getPreAuthorizationConfirmation()%><br /> Phone:<%=N3AO.getPhone()%><br /> Fax:
										<%=N3AO.getFax()%><br>
										NPI: <%=myEO.getAppointment_PracticeMaster().getNPINumber()%>
										
										</p>
									<p>&nbsp;</p></td>
								<td valign="top">Referring Physician: <strong><%=myEO.Referral_ReferringDoctor
							.getContactFirstName()%> <%=myEO.Referral_ReferringDoctor
							.getContactLastName()%><br />Phone: <strong><%=myEO.Referral_ReferringDoctor.getContactPhone()%></strong><br /> Fax: <strong><%=myEO.Referral_ReferringDoctor.getContactFax()%></strong></strong>&nbsp;<br>
							<strong>NPI: </strong> <%=myEO.Referral_ReferringDoctor.getUserNPI()%>
								</td>
							</tr>
							<tr>
								<td valign="top"><p>
										<strong>NextImage Medical, Inc.</strong><br /> Attn: Next Image Medical, Inc.<br /> P.O. Box 749462<br /> Los Angeles, Ca 90074-9462
									</p></td>
								<td valign="top"><strong>Phone: 858-847-9185</strong> | Fax: 888-596-8680<br /> NPI No.: <strong>1881861359</strong><br /> Tax ID&nbsp; : <strong>205673072</strong></td>
							</tr>
						</table></td>
				</tr>
			</table></td>
	</tr>
	<%
		} else {
				out.println("<p class=instructions>"
						+ ConfigurationMessages.getMessage("ERRORIllegal")
						+ "</p>");
			}
		} else {
			out.println("<p class=instructions>"
					+ ConfigurationMessages
							.getMessage("ERRORSecurityNoAccess") + "</p>");
		}
	%>

</table>
<script language="javascript">
	window.print();
</script>


