<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
<%@ include file="../generic/generalDisplay.jsp" %>



<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

    <table cellpadding=5 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM_Encounter")%>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat("MM/dd/yyyy");
    boolean accessValid = false;
    String customEmail = "false";
    Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
  //page security

  if (accessValid)
  {
		String sFlag = request.getParameter("EDIT");
		String sFlag_email = "";
		String sFlag_emaila = "";
		String sFlag_fax = "";
		boolean isProviderAlert = false;
		boolean isReport = false;
		boolean isLevel2 = false;
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);

		if (request.getParameter("customEmail").equals("1")) {
			customEmail = "true";
		}
				
		bltUserAccount myUA = null;
		if (sFlag.equalsIgnoreCase("send_sp_adj"))
		{
			sFlag_email = "email_sp_adj";
			sFlag_emaila = "email_sp_adj";
			sFlag_fax = "fax_sp_adj";
			myUA = myEO.getCase_Adjuster();
		}
		else if (sFlag.equalsIgnoreCase("send_sp_ncm"))
		{
			sFlag_email = "email_sp_ncm";
			sFlag_emaila = "email_sp_ncm";
			sFlag_fax = "fax_sp_ncm";
			myUA = myEO.getCase_NurseCaseManager();
		}
/*
x send_sp_adj
x send_sp_ncm
x send_sp_adm
x send_sp_ic
x send_rp_refdr
x send_rp_ncm
x send_rp_adj
x send_rp_adm
send_rc_refdr
send_rc_adj
send_rc_ncm
send_rc_adm
x send_sp_refdr
*/
		else if (sFlag.equalsIgnoreCase("send_sp_ic"))
		{
			sFlag_email = "email_sp_ic";
			sFlag_emaila = "email_sp_ic";
			sFlag_fax = "fax_sp_ic";
			isProviderAlert = true;
		}
		else if (sFlag.equalsIgnoreCase("email_rc_pt"))
		{
			sFlag_email = "email_rc_pt";
			sFlag_emaila = "email_rc_pt";
			sFlag_fax = "email_rc_pt";
		}
		else if (sFlag.equalsIgnoreCase("send_sp_refdr"))
		{
			sFlag_email = "email_sp_refdr";
			sFlag_emaila = "email_sp_refdr";
			sFlag_fax = "fax_sp_refdr";
			myUA = myEO.getReferral_ReferringDoctor();
		}
		else if (sFlag.equalsIgnoreCase("send_sp_adm"))
		{
			sFlag_email = "email_sp_adm";
			sFlag_emaila = "email_sp_adm";
			sFlag_fax = "fax_sp_adm";
			myUA = myEO.Case_Admin;
		}
		else if (sFlag.equalsIgnoreCase("send_sp_adm2"))
		{
			sFlag_email = "email_sp_adm2";
			sFlag_emaila = "email_sp_adm2";
			sFlag_fax = "fax_sp_adm2";
			myUA = myEO.Case_Admin2;
		}
		else if (sFlag.equalsIgnoreCase("send_sp_adm3"))
		{
			sFlag_email = "email_sp_adm3";
			sFlag_emaila = "email_sp_adm3";
			sFlag_fax = "fax_sp_adm3";
			myUA = myEO.Case_Admin3;
		}
		else if (sFlag.equalsIgnoreCase("send_sp_adm4"))
		{
			sFlag_email = "email_sp_adm4";
			sFlag_emaila = "email_sp_adm4";
			sFlag_fax = "fax_sp_adm4";
			myUA = myEO.Case_Admin4;
		}
		else if (sFlag.equalsIgnoreCase("send_rp_adj"))
		{
			sFlag_email = "email_rp_adj";
			sFlag_emaila = "emaila_rp_adj";
			sFlag_fax = "fax_rp_adj";
			myUA = myEO.getCase_Adjuster();
			isReport = true;
		}
		else if (sFlag.equalsIgnoreCase("send_rp_adm"))
		{
			sFlag_email = "email_rp_adm";
			sFlag_emaila = "emaila_rp_adm";
			sFlag_fax = "fax_rp_adm";
			myUA = myEO.Case_Admin;
			isReport = true;
		}
		else if (sFlag.equalsIgnoreCase("send_rp_adm2"))
		{
			sFlag_email = "email_rp_adm2";
			sFlag_emaila = "emaila_rp_adm2";
			sFlag_fax = "fax_rp_adm2";
			myUA = myEO.Case_Admin2;
			isReport = true;
		}
		else if (sFlag.equalsIgnoreCase("send_rp_adm3"))
		{
			sFlag_email = "email_rp_adm3";
			sFlag_emaila = "emaila_rp_adm3";
			sFlag_fax = "fax_rp_adm3";
			myUA = myEO.Case_Admin3;
			isReport = true;
		}
		else if (sFlag.equalsIgnoreCase("send_rp_adm4"))
		{
			sFlag_email = "email_rp_adm4";
			sFlag_emaila = "emaila_rp_adm4";
			sFlag_fax = "fax_rp_adm4";
			myUA = myEO.Case_Admin4;
			isReport = true;
		}
		else if (sFlag.equalsIgnoreCase("send_rp_ncm"))
		{
			sFlag_email = "email_rp_ncm";
			sFlag_emaila = "emaila_rp_ncm";
			sFlag_fax = "fax_rp_ncm";
			myUA = myEO.getCase_NurseCaseManager();
			isReport = true;
		}
		else if (sFlag.equalsIgnoreCase("send_rp_refdr"))
		{
			sFlag_email = "email_rp_refdr";
			sFlag_emaila = "emaila_rp_refdr";
			sFlag_fax = "fax_rp_refdr";
			myUA = myEO.getReferral_ReferringDoctor();
			isReport = true;
		}


		else if (sFlag.equalsIgnoreCase("send_rc_adj"))
		{
			sFlag_email = "email_rc_adj";
			sFlag_emaila = "email_rc_adj";
			sFlag_fax = "fax_rc_adj";
			myUA = myEO.getCase_Adjuster();
			isLevel2 = true;
		}
		else if (sFlag.equalsIgnoreCase("send_rc_adm"))
		{
			sFlag_email = "email_rc_adm";
			sFlag_emaila = "email_rc_adm";
			sFlag_fax = "fax_rc_adm";
			myUA = myEO.Case_Admin;
			isLevel2 = true;
		}
		else if (sFlag.equalsIgnoreCase("send_rc_adm2"))
		{
			sFlag_email = "email_rc_adm2";
			sFlag_emaila = "email_rc_adm2";
			sFlag_fax = "fax_rc_adm2";
			myUA = myEO.Case_Admin2;
			isLevel2 = true;
		}
		else if (sFlag.equalsIgnoreCase("send_rc_adm3"))
		{
			sFlag_email = "email_rc_adm3";
			sFlag_emaila = "email_rc_adm3";
			sFlag_fax = "fax_rc_adm3";
			myUA = myEO.Case_Admin3;
			isLevel2 = true;
		}
		else if (sFlag.equalsIgnoreCase("send_rc_adm4"))
		{
			sFlag_email = "email_rc_adm4";
			sFlag_emaila = "email_rc_adm4";
			sFlag_fax = "fax_rc_adm4";
			myUA = myEO.Case_Admin4;
			isLevel2 = true;
		}
		else if (sFlag.equalsIgnoreCase("send_rc_ncm"))
		{
			sFlag_email = "email_rc_ncm";
			sFlag_emaila = "email_rc_ncm";
			sFlag_fax = "fax_rc_ncm";
			myUA = myEO.getCase_NurseCaseManager();
			isLevel2 = true;
		}
		else if (sFlag.equalsIgnoreCase("send_rc_refdr"))
		{
			sFlag_email = "email_rc_refdr";
			sFlag_emaila = "email_rc_refdr";
			sFlag_fax = "fax_rc_refdr";
			myUA = myEO.getReferral_ReferringDoctor();
			isLevel2 = true;
		}
		else if (sFlag.indexOf("_all")>0)
		{
			%>
            <script language="javascript">
				//alert('tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag%>');
				document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag%>';
			</script>
            <%
			myUA = myEO.getCase_Adjuster();
		}




%>
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td><h1> Send Notification</h1></td>
    </tr>
    <%
	if (myUA!=null&&!myUA.getEmailAlertNotes_Alert().equalsIgnoreCase(""))
	{
	%>
    <tr class="tdBaseAlt_Action2">
      <td>&nbsp;</td>
      <td class="titleSub1"><%=myUA.getEmailAlertNotes_Alert()%></td>
    </tr>
    <%
	}
	%>
    <%
	if (!myEO.getEmailImportantNotes_Payer("<br>").equalsIgnoreCase(""))
	{
	%>
    <tr class="tdBaseAlt_Action2">
      <td>&nbsp;</td>
      <td class="titleSub1"><%=myEO.getEmailImportantNotes_Payer("<br>")%></td>
    </tr>
    <%
	}
	%>
    <tr>
      <td>&nbsp;</td>
      <td>

<%
	boolean hasPreference = false;
if (isReport)
{
	%><span class="tdHeader">User Report Alert Preference: [
      <jsp:include page="../generic/tUAAlertsLILong_translate.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myUA.getComm_Report_LevelUser()%>" />
                </jsp:include>]</span><Br /><%
	if (myUA.getComm_Report_LevelUser().intValue()==NIMUtils.ALERT_PREF_SECURE_EMAIL)
	{
		hasPreference = true;
	%>
	<input <%if (!NIMUtils.isValidEmail(myUA.getContactEmail())){out.print( " disabled " );}%> type="button" class="inputButton_lg_Create" onclick="this.disabled=true;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag_email%>&customEmail=<%=customEmail%>';" value="Email" />
	<%
	}
	else if (myUA.getComm_Report_LevelUser().intValue()==NIMUtils.ALERT_PREF_EMAIL_ATTACHMENT)
	{
		hasPreference = true;
	%>
	<input <%if (!NIMUtils.isValidEmail(myUA.getContactEmail())){out.print( " disabled " );}%>  type="button" class="inputButton_lg_Create" onclick="this.disabled=true;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag_emaila%>&customEmail=<%=customEmail%>';" value="Email w/ Attachment" />
	<%
	}
	else if (myUA.getComm_Report_LevelUser().intValue()==NIMUtils.ALERT_PREF_FAX)
	{
		hasPreference = true;
	%>
	<input <%if (!NIMUtils.isValidPhone(myUA.getContactFax())){out.print( " disabled " );}%>   type="button" class="inputButton_lg_Create" onclick="this.disabled=true;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag_fax%>&customEmail=<%=customEmail%>';" value="Fax" />
	<%
	}
}
else if (isProviderAlert)
{
%>
<input type="button" class="inputButton_lg_Create" onclick="this.disabled=true;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag_fax%>&customEmail=<%=customEmail%>';" value="Fax" />
<%
}
else if (isLevel2)
{
%>
<span class="tdHeader">User Notification Preference: [
      <jsp:include page="../generic/tUAAlertsLILong_translate.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myUA.getComm_Alerts2_LevelUser()%>" />
                </jsp:include>]</span><Br /><%	
	
	if (myUA.getComm_Alerts2_LevelUser().intValue()==NIMUtils.ALERT_PREF_SECURE_EMAIL)
	{
		hasPreference = true;
	%>
	<input <%if (!NIMUtils.isValidEmail(myUA.getContactEmail())){out.print( " disabled " );}%>  type="button" class="inputButton_lg_Create" onclick="this.disabled=true;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag_email%>&customEmail=<%=customEmail%>';" value="Email" />
	<%
	}
	else if (myUA.getComm_Alerts2_LevelUser().intValue()==NIMUtils.ALERT_PREF_EMAIL_ATTACHMENT)
	{
		hasPreference = true;
	%>
	<input <%if (!NIMUtils.isValidEmail(myUA.getContactEmail())){out.print( " disabled " );}%>  type="button" class="inputButton_lg_Create" onclick="this.disabled=true;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag_emaila%>&customEmail=<%=customEmail%>';" value="Email w/ Attachment" />
	<%
	}
	else if (myUA.getComm_Alerts2_LevelUser().intValue()==NIMUtils.ALERT_PREF_FAX)
	{
		hasPreference = true;
	%>
	<input <%if (!NIMUtils.isValidPhone(myUA.getContactFax())){out.print( " disabled " );}%>   type="button" class="inputButton_lg_Create" onclick="this.disabled=true;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag_fax%>&customEmail=<%=customEmail%>';" value="Fax" />
	<%
	}
}
else 
{
%>
<span class="tdHeader">User Notification Preference: [
      <jsp:include page="../generic/tUAAlertsLILong_translate.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myUA.getComm_Alerts_LevelUser()%>" />
                </jsp:include>]</span><Br /><%	
	
	if (myUA.getComm_Alerts_LevelUser().intValue()==NIMUtils.ALERT_PREF_SECURE_EMAIL)
	{
		hasPreference = true;
	%>
	<input <%if (!NIMUtils.isValidEmail(myUA.getContactEmail())){out.print( " disabled " );}%>  type="button" class="inputButton_lg_Create" onclick="this.disabled=true;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag_email%>&customEmail=<%=customEmail%>';" value="Email" />
	<%
	}
	else if (myUA.getComm_Alerts_LevelUser().intValue()==NIMUtils.ALERT_PREF_EMAIL_ATTACHMENT)
	{
		hasPreference = true;
	%>
	<input <%if (!NIMUtils.isValidEmail(myUA.getContactEmail())){out.print( " disabled " );}%>  type="button" class="inputButton_lg_Create" onclick="this.disabled=true;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag_emaila%>&customEmail=<%=customEmail%>';" value="Email w/ Attachment" />
	<%
	}
	else if (myUA.getComm_Alerts_LevelUser().intValue()==NIMUtils.ALERT_PREF_FAX)
	{
		hasPreference = true;
	%>
	<input <%if (!NIMUtils.isValidPhone(myUA.getContactFax())){out.print( " disabled " );}%>   type="button" class="inputButton_lg_Create" onclick="this.disabled=true;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag_fax%>&customEmail=<%=customEmail%>';" value="Fax" />
	<%
	}
}
%>                
                
                
      </td>
    </tr>
    <%
	if (!isProviderAlert&&!hasPreference)
	{
	%>
    <tr>
      <td>&nbsp;</td>
      <td>
        <table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="instructionsBig1">Send using Alternate Method:</td>
            <td width="40" class="instructionsBig1">&nbsp;</td>
            <td rowspan="4" ><p class="tdHeader">Contact Information on File for this User:</p>
            <p>Name:<strong><%=myUA.getContactFirstName()%> <%=myUA.getContactLastName()%></strong><br />
              Email Address:<strong><%=myUA.getContactEmail()%></strong> <br />
              Fax: <strong><%=myUA.getContactFax()%></strong>
            </p></td>
          </tr>
          <%if (false){ //disabled 09/20/2012 - Per Christine: Adj cannot view their msg when email sent this way%>
          <tr>
            <td><input <%if (!NIMUtils.isValidEmail(myUA.getContactEmail())){out.print( " disabled " );}%> type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag_email%>&customEmail=<%=customEmail%>';" value="Email" /></td>
            <td>&nbsp;</td>
          </tr>
          <%} %>
          <tr>
            <td><input <%if (!NIMUtils.isValidEmail(myUA.getContactEmail())){out.print( " disabled " );}%> type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag_emaila%>&customEmail=<%=customEmail%>';" value="Email w/ Attachment" /></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td><input <%if (!NIMUtils.isValidPhone(myUA.getContactFax())){out.print( " disabled " );}%>  type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=<%=sFlag_fax%>&customEmail=<%=customEmail%>';" value="Fax" /></td>
            <td>&nbsp;</td>
          </tr>
        </table>
      <p>&nbsp;</p></td>
    </tr>
    <%
	}
	%>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="button" class="inputButton_md_Stop" onclick="this.disabled=true;alert('Alert NOT Sent');window.close();" value="Cancel" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr> 
    </table>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

