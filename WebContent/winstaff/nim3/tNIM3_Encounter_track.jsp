<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tNIM3_Encounter_form.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear_fade.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<script language="javascript">
show_pws_Loading();
</script>

    <table id="bigfade" cellpadding=0 cellspacing=0 border=0 width=100%>
    <tr><td width=10>&nbsp;</td><td>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
//        accessValid = true;
        if (iEncounterID.intValue() == iEDITID.intValue())
        {
        	accessValid = true;
        }
    }
  //page security
  if (accessValid&&isScheduler2)
  {
	java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
	java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);	java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_Encounter_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID

    NIM3_EncounterObject        myEO        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("nim3track") )
    {
        myEO        =    new    NIM3_EncounterObject(iEncounterID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        myEO        =    new    NIM3_EncounterObject();
    }

//fields
        %>
        <form action="tNIM3_EncounterService_form_sub.jsp" name="tNIM3_Encounter_form1" method="POST" >
            <input type=hidden name=routePageReference value="sParentReturnPage">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
    %>
        <input type="hidden" name="EDITID" value = "<%=iEDITID%>" >  

          <%  String theClass ="tdBase";%>
      <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr>
           <td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=50%>
                        <tr>
                          <td colspan="3" valign=middle><img src="images/marker_encounter.gif" width="114" height="31" align="middle" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="inputButton_md_Create" onClick = "this.disabled=false;$('table').fadeOut('slow');show_pws_Calc();submit();" type=Submit value="Save" name=Submit></td>
                        </tr>



<tr>
  <td colspan="3">

<table border="1" cellspacing="0" cellpadding="2" class="tableCanvasLightGrey">
  <tr class="tdHeaderBright">
    <td colspan="2">TimeTrack Dates (workflow times)</td>
    <td>User</td>
    <td>RCode</td>
    <td>to Ref Dr</td>
    <td>to Case Admin</td>
    <td>to  Case Admin 2</td>
    <td>to  Case Admin 3</td>
    <td>to  Case Admin 4</td>
    <td>to Adjuster</td>
    <td>to NCM</td>
    <td>to Provider</td>
    </tr>



            <%
            if ( (myEO.NIM3_Encounter.isRequired("TimeTrack_ReqRec",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("TimeTrack_ReqRec")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("TimeTrack_ReqRec",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("TimeTrack_ReqRec",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO.NIM3_Encounter.isWrite("TimeTrack_ReqRec",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Received</b></p></td><td valign=top><p><input name="TimeTrack_ReqRec"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getTimeTrack_ReqRec())%>" /></jsp:include>' size="17" maxlength=20  >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRec&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqRec")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(myEO.NIM3_Encounter.getTimeTrack_ReqRec_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRec_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqRec_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
            <%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_ReqRec_RefDr",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_ReqRec_RefDr")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_ReqRec_RefDr",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_RefDr",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

<td valign=top><p><select   name="TimeTrack_ReqRec_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getTimeTrack_ReqRec_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRec_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqRec_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>


                        <td valign=top>
                                  <%
            if ((myEO.NIM3_Encounter.isWrite("SentTo_ReqRec_RefDr",UserSecurityGroupID)))
            {
                        %>
<p><input maxlength=20  type=text size="17" name="SentTo_ReqRec_RefDr" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_RefDr())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_RefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_RefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if ((myEO.NIM3_Encounter.isRead("SentTo_ReqRec_RefDr",UserSecurityGroupID)))
            {
                        %>
                       <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_RefDr())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_RefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_RefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>
</td>
                        <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_ReqRec_Adm",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_ReqRec_Adm")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_ReqRec_Adm",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_Adm",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((myEO.NIM3_Encounter.isWrite("SentTo_ReqRec_Adm",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <input maxlength=20  type=text size="17" name="SentTo_ReqRec_Adm" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_Adm())%>" /></jsp:include>' >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else if ((myEO.NIM3_Encounter.isRead("SentTo_ReqRec_Adm",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                              <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_Adm())%>" />                          
                            </jsp:include>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %></td>
                        <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_ReqRec_Adm2",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_ReqRec_Adm2")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_ReqRec_Adm2",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_Adm2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((myEO.NIM3_Encounter.isWrite("SentTo_ReqRec_Adm2",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <input maxlength=20  type=text size="17" name="SentTo_ReqRec_Adm2" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_Adm2())%>" /></jsp:include>' >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm2&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm2")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else if ((myEO.NIM3_Encounter.isRead("SentTo_ReqRec_Adm2",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                              <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_Adm2())%>" />                          
                            </jsp:include>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm2&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm2")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %></td>
                        <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_ReqRec_Adm3",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_ReqRec_Adm3")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_ReqRec_Adm3",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_Adm3",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((myEO.NIM3_Encounter.isWrite("SentTo_ReqRec_Adm3",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <input maxlength=20  type=text size="17" name="SentTo_ReqRec_Adm3" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_Adm3())%>" /></jsp:include>' >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm3&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm3")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else if ((myEO.NIM3_Encounter.isRead("SentTo_ReqRec_Adm3",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                              <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_Adm3())%>" />                          
                            </jsp:include>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm3&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm3")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %></td>
                        <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_ReqRec_Adm4",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_ReqRec_Adm4")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_ReqRec_Adm4",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_Adm4",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((myEO.NIM3_Encounter.isWrite("SentTo_ReqRec_Adm4",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <input maxlength=20  type=text size="17" name="SentTo_ReqRec_Adm4" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_Adm4())%>" /></jsp:include>' >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm4&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm4")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else if ((myEO.NIM3_Encounter.isRead("SentTo_ReqRec_Adm4",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                              <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_Adm4())%>" />                          
                            </jsp:include>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adm4&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_Adm4")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %></td>
                        <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_ReqRec_Adj",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_ReqRec_Adj")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_ReqRec_Adj",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_Adj",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                          <%
            if ((myEO.NIM3_Encounter.isWrite("SentTo_ReqRec_Adj",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <input maxlength=20  type=text size="17" name="SentTo_ReqRec_Adj" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_Adj())%>" /></jsp:include>' >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_Adj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else if ((myEO.NIM3_Encounter.isRead("SentTo_ReqRec_Adj",UserSecurityGroupID)))
            {
                        %>
                          <p>
                            <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                              <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_Adj())%>" />                          
                            </jsp:include>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_Adj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_Adj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p>
                          <%
            }
            else
            {
                        %>
                          <%
            }
            %></td>


                          <td valign=top>
            <%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_ReqRec_NCM",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_ReqRec_NCM")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_ReqRec_NCM",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_ReqRec_NCM",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO.NIM3_Encounter.isWrite("SentTo_ReqRec_NCM",UserSecurityGroupID)))
            {
                        %>
<p><input maxlength=20  type=text size="17" name="SentTo_ReqRec_NCM" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_NCM())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_NCM&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_NCM")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if ((myEO.NIM3_Encounter.isRead("SentTo_ReqRec_NCM",UserSecurityGroupID)))
            {
                        %>
                        <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_ReqRec_NCM())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_ReqRec_NCM&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_ReqRec_NCM")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

</td>
                          <td valign=top>&nbsp;</td>
                        </tr>
                        <%
            }
            else if ((myEO.NIM3_Encounter.isRead("TimeTrack_ReqRec",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (myEO.NIM3_Encounter.isRequired("TimeTrack_ReqCreated",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("TimeTrack_ReqCreated")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("TimeTrack_ReqCreated",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("TimeTrack_ReqCreated",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO.NIM3_Encounter.isWrite("TimeTrack_ReqCreated",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Created</b></p></td><td valign=top><p><input name="TimeTrack_ReqCreated"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getTimeTrack_ReqCreated())%>" /></jsp:include>' size="17" maxlength=20  >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqCreated&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqCreated")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(myEO.NIM3_Encounter.getTimeTrack_ReqCreated_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqCreated_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqCreated_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
                        <%
            }
            else if ((myEO.NIM3_Encounter.isRead("TimeTrack_ReqCreated",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



<td valign=top><p><select   name="TimeTrack_ReqCreated_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getTimeTrack_ReqCreated_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqCreated_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqCreated_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>                          <td valign=top>&nbsp;</td>
<td valign=top>&nbsp;</td>
<td valign=top>&nbsp;</td>
<td valign=top>&nbsp;</td>
<td valign=top>&nbsp;</td>
<td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>

                        </tr>


            <%
            if ( (myEO.NIM3_Encounter.isRequired("TimeTrack_ReqProc",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("TimeTrack_ReqProc")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("TimeTrack_ReqProc",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("TimeTrack_ReqProc",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO.NIM3_Encounter.isWrite("TimeTrack_ReqProc",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Processed</b></p></td><td valign=top><p><input name="TimeTrack_ReqProc"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getTimeTrack_ReqProc())%>" /></jsp:include>' size="17" maxlength=20  >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqProc&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqProc")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(myEO.NIM3_Encounter.getTimeTrack_ReqProc_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqProc_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqProc_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>
            <%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_DataProc_RefDr",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_DataProc_RefDr")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_DataProc_RefDr",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_DataProc_RefDr",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

<td valign=top><p><select   name="TimeTrack_ReqProc_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getTimeTrack_ReqProc_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqProc_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqProc_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>

                          <td valign=top>

            <%
            if (false&&(myEO.NIM3_Encounter.isWrite("SentTo_DataProc_RefDr",UserSecurityGroupID)))
            {
                        %>
                        <p><input maxlength=20  type=text size="17" name="SentTo_DataProc_RefDr" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_DataProc_RefDr())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_DataProc_RefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_DataProc_RefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if (false&&(myEO.NIM3_Encounter.isRead("SentTo_DataProc_RefDr",UserSecurityGroupID)))
            {
                        %>
                       <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_DataProc_RefDr())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_DataProc_RefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_DataProc_RefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_DataProc_Adj",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_DataProc_Adj")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_DataProc_Adj",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_DataProc_Adj",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (false&&(myEO.NIM3_Encounter.isWrite("SentTo_DataProc_Adj",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_DataProc_Adj" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_DataProc_Adj())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_DataProc_Adj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_DataProc_Adj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (false&&(myEO.NIM3_Encounter.isRead("SentTo_DataProc_Adj",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_DataProc_Adj())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_DataProc_Adj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_DataProc_Adj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %>
  &nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><strong>RX Review</strong></td>
                          <td valign=top><p>
                            <input name="TimeTrack_ReqRxReview"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getTimeTrack_ReqRxReview())%>" /></jsp:include>' size="17" maxlength=20  >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRxReview&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqRxReview")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p></td>
                          <td valign=top><%=new bltUserAccount(myEO.NIM3_Encounter.getTimeTrack_ReqRxReview_UserID()).getLogonUserName()%>&nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRxReview_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqRxReview_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%></td>
                          <td valign=top><p>
                            <select   name="TimeTrack_ReqRxReview_RCodeID" >
                              <jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getTimeTrack_ReqRxReview_RCodeID()%>" />                        
                              </jsp:include>
                            </select>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRxReview_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqRxReview_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p></td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                        <%
            }
            else if ((myEO.NIM3_Encounter.isRead("TimeTrack_ReqProc",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



<tr>
<td><strong>Approved</strong></td>


<td valign=top><p><input name="TimeTrack_ReqApproved"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getTimeTrack_ReqApproved())%>" /></jsp:include>' size="17" maxlength=20  >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqApproved&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqApproved")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>





<td valign=top><%=new bltUserAccount(myEO.NIM3_Encounter.getTimeTrack_ReqApproved_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqApproved_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqApproved_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>


<td valign=top><p><select   name="TimeTrack_ReqApproved_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getTimeTrack_ReqApproved_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqApproved_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqApproved_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>

<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>

            <%
            if ( (myEO.NIM3_Encounter.isRequired("TimeTrack_ReqSched",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("TimeTrack_ReqSched")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("TimeTrack_ReqSched",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("TimeTrack_ReqSched",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO.NIM3_Encounter.isWrite("TimeTrack_ReqSched",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Scheduled</b></p></td><td valign=top><p><input name="TimeTrack_ReqSched"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getTimeTrack_ReqSched())%>" /></jsp:include>' size="17" maxlength=20  >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqSched&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqSched")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(myEO.NIM3_Encounter.getTimeTrack_ReqSched_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqSched_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqSched_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td>

            <%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_SP_RefDr",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_SP_RefDr")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_SP_RefDr",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_SP_RefDr",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>


<td valign=top><p><select   name="TimeTrack_ReqSched_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getTimeTrack_ReqSched_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqSched_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqSched_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>

                          <td valign=top>

            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentTo_SP_RefDr",UserSecurityGroupID)))
            {
                        %>
               <input maxlength=20  type=text size="17" name="SentTo_SP_RefDr" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_RefDr())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_RefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_RefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%>
                        <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentTo_SP_RefDr",UserSecurityGroupID)))
            {
                        %>
                        <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_RefDr())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_RefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_RefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

</td>
                          <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_SP_Adm",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_SP_Adm")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_SP_Adm",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_SP_Adm",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentTo_SP_Adm",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_SP_Adm" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_Adm())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_Adm")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentTo_SP_Adm",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_Adm())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_Adm")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_SP_Adm2",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_SP_Adm2")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_SP_Adm2",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_SP_Adm2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentTo_SP_Adm2",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_SP_Adm2" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_Adm2())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm2&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_Adm2")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentTo_SP_Adm2",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_Adm2())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm2&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_Adm2")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_SP_Adm3",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_SP_Adm3")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_SP_Adm3",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_SP_Adm3",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentTo_SP_Adm3",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_SP_Adm3" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_Adm3())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm3&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_Adm3")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentTo_SP_Adm3",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_Adm3())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm3&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_Adm3")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_SP_Adm4",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_SP_Adm4")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_SP_Adm4",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_SP_Adm4",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentTo_SP_Adm4",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_SP_Adm4" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_Adm4())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm4&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_Adm4")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentTo_SP_Adm4",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_Adm4())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adm4&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_Adm4")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_SP_Adj",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_SP_Adj")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_SP_Adj",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_SP_Adj",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentTo_SP_Adj",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_SP_Adj" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_Adj())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_Adj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentTo_SP_Adj",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_Adj())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_Adj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_Adj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top>
            <%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_SP_NCM",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_SP_NCM")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_SP_NCM",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_SP_NCM",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentTo_SP_NCM",UserSecurityGroupID)))
            {
                        %>
                    <p><input maxlength=20  type=text size="17" name="SentTo_SP_NCM" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_NCM())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_NCM&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_NCM")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentTo_SP_NCM",UserSecurityGroupID)))
            {
                        %>
                       <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_NCM())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_NCM&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_NCM")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

</td>
                          <td valign=top>
            <%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_SP_IC",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_SP_IC")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_SP_IC",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_SP_IC",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentTo_SP_IC",UserSecurityGroupID)))
            {
                        %>
                       <p><input maxlength=20  type=text size="17" name="SentTo_SP_IC" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_IC())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_IC&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_IC")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentTo_SP_IC",UserSecurityGroupID)))
            {
                        %>
                        <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_SP_IC())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_SP_IC&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_SP_IC")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

</td>
                        </tr>

                        <tr>
                          <td valign=top ><p class=<%=theClass%> ><strong>Initial Appointment</strong></p></td>
                          <td valign=top><p>
                            <input name="TimeTrack_ReqInitialAppointment"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getTimeTrack_ReqInitialAppointment())%>" /></jsp:include>' size="17" maxlength=20             >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqInitialAppointment&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqInitialAppointment")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p></td>
                          <td valign=top><%=new bltUserAccount(myEO.NIM3_Encounter.getTimeTrack_ReqInitialAppointment_UserID()).getLogonUserName()%>&nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqInitialAppointment_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqInitialAppointment_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%></td>
                          <td valign=top><p>
                            <select   name="TimeTrack_ReqInitialAppointment_RCodeID" >                              <jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getTimeTrack_ReqInitialAppointment_RCodeID()%>" />                          
                              </jsp:include>
                            </select>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqInitialAppointment_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqInitialAppointment_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p></td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                        </tr>
                        <%
            }
            else if ((myEO.NIM3_Encounter.isRead("TimeTrack_ReqInitialAppointment",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>







            <%
            if ( (myEO.NIM3_Encounter.isRequired("TimeTrack_ReqDelivered",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("TimeTrack_ReqDelivered")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("TimeTrack_ReqDelivered",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("TimeTrack_ReqDelivered",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO.NIM3_Encounter.isWrite("TimeTrack_ReqDelivered",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Report Delivered</b></p></td><td valign=top><p><input name="TimeTrack_ReqDelivered"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getTimeTrack_ReqDelivered())%>" /></jsp:include>' size="17" maxlength=20  >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqDelivered&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqDelivered")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(myEO.NIM3_Encounter.getTimeTrack_ReqDelivered_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqDelivered_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqDelivered_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>

            <%
            if ( (myEO.NIM3_Encounter.isRequired("SentToRefDr",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentToRefDr")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentToRefDr",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentToRefDr",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>


<td valign=top><p><select   name="TimeTrack_ReqDelivered_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getTimeTrack_ReqDelivered_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqDelivered_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqDelivered_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                          <td valign=top>

            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentToRefDr",UserSecurityGroupID)))
            {
                        %><p><input maxlength=20  type=text size="17" name="SentToRefDr" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentToRefDr())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToRefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentToRefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentToRefDr",UserSecurityGroupID)))
            {
                        %>
                        <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentToRefDr())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToRefDr&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentToRefDr")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

</td>
                          <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentToAdm",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentToAdm")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentToAdm",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentToAdm",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentToAdm",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentToAdm" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentToAdm())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToAdm&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentToAdm")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentToAdm",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentToAdm())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToAdm&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentToAdm")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_RP_Adm2",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_RP_Adm2")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_RP_Adm2",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_RP_Adm2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentTo_RP_Adm2",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_RP_Adm2" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_RP_Adm2())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_Adm2&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_RP_Adm2")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentTo_RP_Adm2",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_RP_Adm2())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_Adm2&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_RP_Adm2")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_RP_Adm3",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_RP_Adm3")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_RP_Adm3",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_RP_Adm3",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentTo_RP_Adm3",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_RP_Adm3" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_RP_Adm3())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_Adm3&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_RP_Adm3")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentTo_RP_Adm3",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_RP_Adm3())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_Adm3&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_RP_Adm3")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_RP_Adm4",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_RP_Adm4")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_RP_Adm4",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_RP_Adm4",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentTo_RP_Adm4",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentTo_RP_Adm4" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_RP_Adm4())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_Adm4&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_RP_Adm4")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentTo_RP_Adm4",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_RP_Adm4())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_Adm4&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_RP_Adm4")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top><%
            if ( (myEO.NIM3_Encounter.isRequired("SentToAdj",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentToAdj")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentToAdj",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentToAdj",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
                            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentToAdj",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <input maxlength=20  type=text size="17" name="SentToAdj" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentToAdj())%>" /></jsp:include>' >
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToAdj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentToAdj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentToAdj",UserSecurityGroupID)))
            {
                        %>
                            <p>
                              <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentToAdj())%>" />                            
                              </jsp:include>
                              &nbsp;
                              <%if (isShowAudit){%>
                              <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentToAdj&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentToAdj")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                              <%}%>
                            </p>
                            <%
            }
            else
            {
                        %>
                            <%
            }
            %></td>
                          <td valign=top>

            <%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_RP_NCM",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_RP_NCM")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_RP_NCM",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_RP_NCM",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentTo_RP_NCM",UserSecurityGroupID)))
            {
                        %>
                        <p><input maxlength=20  type=text size="17" name="SentTo_RP_NCM" value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_RP_NCM())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_NCM&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_RP_NCM")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else if (isScheduler&&(myEO.NIM3_Encounter.isRead("SentTo_RP_NCM",UserSecurityGroupID)))
            {
                        %>
                        <p><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getSentTo_RP_NCM())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SentTo_RP_NCM&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("SentTo_RP_NCM")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

</td>
                          <td valign=top>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><strong>Report Review</strong></td>
                          <td valign=top><p>
                            <input name="TimeTrack_ReqRpReview"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getTimeTrack_ReqRpReview())%>" /></jsp:include>' size="17" maxlength=20  >
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRpReview&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqRpReview")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p></td>
                          <td valign=top><%=new bltUserAccount(myEO.NIM3_Encounter.getTimeTrack_ReqRpReview_UserID()).getLogonUserName()%>&nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRpReview_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqRpReview_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%></td>
                          <td valign=top><p>
                            <select   name="TimeTrack_ReqRpReview_RCodeID" >
                              <jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" >
                                <jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getTimeTrack_ReqRpReview_RCodeID()%>" />
                              </jsp:include>
                            </select>
                            &nbsp;
                            <%if (isShowAudit){%>
                            <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqRpReview_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqRpReview_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                            <%}%>
                          </p></td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                        <%
            }
            else if ((myEO.NIM3_Encounter.isRead("TimeTrack_ReqDelivered",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (myEO.NIM3_Encounter.isRequired("TimeTrack_ReqPaidIn",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("TimeTrack_ReqPaidIn")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("TimeTrack_ReqPaidIn",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("TimeTrack_ReqPaidIn",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO.NIM3_Encounter.isWrite("TimeTrack_ReqPaidIn",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Payment - In</b></p></td><td valign=top><p><input name="TimeTrack_ReqPaidIn"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getTimeTrack_ReqPaidIn())%>" /></jsp:include>' size="17" maxlength=20  >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqPaidIn&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqPaidIn")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(myEO.NIM3_Encounter.getTimeTrack_ReqPaidIn_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqPaidIn_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqPaidIn_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                        <%
            }
            else if ((myEO.NIM3_Encounter.isRead("TimeTrack_ReqPaidIn",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

<td valign=top><p><select   name="TimeTrack_ReqPaidIn_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getTimeTrack_ReqPaidIn_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqPaidIn_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqPaidIn_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>



                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                        </tr>




            <%
            if ( (myEO.NIM3_Encounter.isRequired("TimeTrack_ReqPaidOut",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("TimeTrack_ReqPaidOut")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("TimeTrack_ReqPaidOut",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("TimeTrack_ReqPaidOut",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO.NIM3_Encounter.isWrite("TimeTrack_ReqPaidOut",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Payment - Out</b></p></td><td valign=top><p><input name="TimeTrack_ReqPaidOut"  type=text value='<jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO.NIM3_Encounter.getTimeTrack_ReqPaidOut())%>" /></jsp:include>' size="17" maxlength=20  >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqPaidOut&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqPaidOut")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td><td valign=top><%=new bltUserAccount(myEO.NIM3_Encounter.getTimeTrack_ReqPaidOut_UserID()).getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqPaidOut_UserID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqPaidOut_UserID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                        <%
            }
            else if ((myEO.NIM3_Encounter.isRead("TimeTrack_ReqPaidOut",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



<td valign=top><p><select   name="TimeTrack_ReqPaidOut_RCodeID" ><jsp:include page="../generic/tTimeTrack_RCodeLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getTimeTrack_ReqPaidOut_RCodeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TimeTrack_ReqPaidOut_RCodeID&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=myEO.NIM3_Encounter.getEncounterID()%>&amp;sFieldNameDisp=<%=myEO.NIM3_Encounter.getEnglish("TimeTrack_ReqPaidOut_RCodeID")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                          <td valign=top>&nbsp;</td>
                        </tr>



</table>


</td></tr>







            <%
            if ( (myEO.NIM3_Encounter.isRequired("AmountBilledByProvider",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("AmountBilledByProvider")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("AmountBilledByProvider",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("AmountBilledByProvider",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((myEO.NIM3_Encounter.isWrite("AmountBilledByProvider",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else if ((myEO.NIM3_Encounter.isRead("AmountBilledByProvider",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>





            <%
            if ( (myEO.NIM3_Encounter.isRequired("SentTo_Bill_Pro",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("SentTo_Bill_Pro")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("SentTo_Bill_Pro",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("SentTo_Bill_Pro",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (isScheduler&&(myEO.NIM3_Encounter.isWrite("SentTo_Bill_Pro",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else if (false&&isScheduler&&(myEO.NIM3_Encounter.isRead("SentTo_Bill_Pro",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>








            <%
            if ( (myEO.NIM3_Encounter.isRequired("DateOfService",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("DateOfService")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("DateOfService",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("DateOfService",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((myEO.NIM3_Encounter.isWrite("DateOfService",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else if ((myEO.NIM3_Encounter.isRead("DateOfService",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (myEO.NIM3_Encounter.isRequired("LODID",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("LODID")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("LODID",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("LODID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(myEO.NIM3_Encounter.isWrite("LODID",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else if (false&&(myEO.NIM3_Encounter.isRead("LODID",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

       <%
            if ( (myEO.NIM3_Encounter.isRequired("Comments",UserSecurityGroupID))&&(!myEO.NIM3_Encounter.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((myEO.NIM3_Encounter.isExpired("Comments",expiredDays))&&(myEO.NIM3_Encounter.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(myEO.NIM3_Encounter.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else if (false&&(myEO.NIM3_Encounter.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>
            </table>
        </td></tr>
<tr>
  <td><h2>Case Story:</h2>
    <ul><%=myEO.getEncounterStory("<li>")%></ul></td></tr>
        
        </table>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
    </table>
<script language="javascript">
hide_pws_Loading();
</script>

