
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
 
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
    <table cellpadding=0 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td><hr></td></tr>
    <tr><td width=10>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM_Encounter")%>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat("MM/dd/yyyy");
    boolean accessValid = false;
	boolean customEmail = false;    
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
		String sFlag = request.getParameter("EDIT");
		String newText = request.getParameter("APPEND");
		String custom = request.getParameter("customEmail");
		out.print(newText);
		if (custom.compareToIgnoreCase("'true'")==0) {
			customEmail= true;		
		}
		
		// Strip leading and trailing '
		String text = newText.substring(1,newText.length()-1);

		String UserLogonDescription = "n/a";
		if (pageControllerHash.containsKey("UserLogonDescription"))
		{
			UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
		}
		String myResponse = NIMUtils.sendAlertEmail(iEncounterID,sFlag,text,customEmail,CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName());
		
		out.print("<h1>" + myResponse + "</h1>");

%>
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td><script language="javascript"><%if (myResponse.equalsIgnoreCase("1")){%>window.close();<%}%></script></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr> 
    </table>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
