<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    filename: out\jsp\tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }
    bltNIM3_Document_List        bltNIM3_Document_List        =    new    bltNIM3_Document_List(iCaseID,"DocumentID="+requestID,"");

//declaration of Enumeration
    bltNIM3_Document        working_bltNIM3_Document;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Document_List.elements();
    %>
    <%
	if (requestID==NIMUtils.EncounterBypassRXID)
	{
        working_bltNIM3_Document  = new bltNIM3_Document(requestID);
        pageControllerHash.put("iDocumentID",working_bltNIM3_Document.getDocumentID());
        pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
		pageControllerHash.put("sFileName",working_bltNIM3_Document.getFileName());
		pageControllerHash.put("sDownloadName", com.winstaff.password.RandomString.generateString(4).toLowerCase() + Math.round(Math.random()*1000)+com.winstaff.password.RandomString.generateString(4).toLowerCase() + "_" + working_bltNIM3_Document.getFileName());
		pageControllerHash.put("bDownload", new Boolean(true));
		session.setAttribute("pageControllerHash",pageControllerHash);
		response.sendRedirect("fileRetrieve.jsp");
	}
    else if (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_Document  = (bltNIM3_Document) leCurrentElement.getObject();
        pageControllerHash.put("iDocumentID",working_bltNIM3_Document.getDocumentID());
        pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tNIM3_Document_form.jsp?nullParam=null"+parameterPassString    ;
        if (request.getParameter("EDIT").equalsIgnoreCase("del"))
        {
            targetRedirect = "tNIM3_Document_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("openflow"))
        {
            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Document_main_NIM3_Document_CaseID_form_authorize")   ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("remdoc"))
        {
			working_bltNIM3_Document.setDocType(new Integer(-1));
			working_bltNIM3_Document.commitData();
            targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp"   ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("view"))
        {
			pageControllerHash.put("sFileName",working_bltNIM3_Document.getFileName());
			pageControllerHash.put("sDownloadName", com.winstaff.password.RandomString.generateString(4).toLowerCase() + Math.round(Math.random()*1000)+com.winstaff.password.RandomString.generateString(4).toLowerCase() + "_" + working_bltNIM3_Document.getFileName());
			pageControllerHash.put("bDownload", new Boolean(true));
	        session.setAttribute("pageControllerHash",pageControllerHash);
            targetRedirect = "fileRetrieve.jsp"   ;
        }
        response.sendRedirect(targetRedirect);
    }
    else
    {
   out.println("invalid where query");
    }

  }
  else
  {
   out.println("illegal");
  }
}
else
{
out.println("Your Security Level does not permit you to View this.");
}
%>




