<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<link rel="stylesheet" type="text/css" href="../app/js/anytime/anytime.css" />
    <script type="text/javascript" src="../app/js/prototype/prototype-1.6.0.3.js" ></script>
    <script type="text/javascript" src="../app/js/anytime/anytime.js" ></script>
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
<%@ include file="../generic/generalDisplay.jsp" %>



<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

        <form action="tNIM3_Encounter_nad_sub.jsp" name="tNIM3_Appointment_form1" method="POST">
    <table cellpadding=5 cellspacing=0 border=0 width=100% >
    <tr>
      <td width=10>&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM_Encounter")%>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
		bltNIM3_Encounter NIM3_Encounter = new bltNIM3_Encounter(iEncounterID);

      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");



//    pageControllerHash.put("sLocalChildReturnPage","tImaging_schedule.jsp?nullParam=null&KM=p&EDIT=sched");
	String dNAD = "t+1";
	if (NIM3_Encounter.getNextActionDate().before(NIMUtils.getBeforeTime()))
	{
		
	}
	else
	{
		dNAD = displayDateTimeSDF.format(NIM3_Encounter.getNextActionDate());
	}
    session.setAttribute("pageControllerHash",pageControllerHash);
                        %>
                     <tr>
                       <td valign=top>&nbsp;</td>
                       <td colspan="2" valign=top nowrap="nowrap" class="title">Next Action Date</td>
                     </tr>
                     <tr>
                       <td valign=top>&nbsp;</td>
                       <td valign=top nowrap="nowrap"><b>Next Action Notes&nbsp;</b></td><td valign=top><p>
                           <textarea name="NextActionNotes" cols="60" rows="4"><%=NIM3_Encounter.getNextActionNotes()%></textarea>
                       &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NextActionNotes&amp;sTableName=tNIM3_Encounter&amp;sRefID=<%=NIM3_Encounter.getAppointmentID()%>&amp;sFieldNameDisp=<%=NIM3_Encounter.getEnglish("NextActionNotes")%>&amp;sTableNameDisp=tNIM3_Encounter','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>


                        <tr>
                          <td valign=top>&nbsp;</td>
                          <td valign=top nowrap="nowrap"><b>Next Action Date:&nbsp;</b></td><td valign=top><p>
<input maxlength=20 class="inputButton_lg_Action1"  type=text size="40" name="NextActionDate"  id="NextActionDate" value='<%=dNAD%>' id="f_date_b"  >     <script type="text/javascript">
    new ATWidget( "NextActionDate", { format: "%m/%d/%Y %h:%i %p" } );
                           </script>





                        <tr>
                          <td>&nbsp;</td>
                          <td nowrap="nowrap">&nbsp;</td>
                          <td><input name="Submit3" type="Button" onclick="javascript:document.getElementById('NextActionDate').value='t+1';" class="inputButton_md_Default" value="Tomorrow" />&nbsp;&nbsp;&nbsp;<input name="Submit3" type="Button" onclick="javascript:document.getElementById('NextActionDate').value='tam';" class="inputButton_md_Default" value="Tomorrow 7am" />&nbsp;&nbsp;&nbsp;<input name="Submit4" type="Button" onclick="javascript:document.getElementById('NextActionDate').value='h+1';" class="inputButton_md_Default" value="1 Hour" />&nbsp;&nbsp;&nbsp;<input name="Submit4" type="Button" onclick="javascript:document.getElementById('NextActionDate').value='h+2';" class="inputButton_md_Default" value="2 Hours" />&nbsp;&nbsp;&nbsp;<input name="Submit4" type="Button" onclick="javascript:document.getElementById('NextActionDate').value='h+4';" class="inputButton_md_Default" value="4 Hours" /></td>
                        </tr>
            <tr>
              <td>&nbsp;</td>
            <td nowrap="nowrap"><input name=Submit type=Submit class="inputButton_md_Create" value="Continue" /></td>
            <td><input name="Submit2" type="Submit" onclick="javascript:document.getElementById('NextActionDate').value='';" class="inputButton_md_Stop" value="Clear NextAction" /></td></tr>
						
						<tr>
						  <td>                          
					    </td>
						<tr>
						  <td>                          
					    </tr>



            </table>


<input type="hidden" name="EDITID" value="<%=iEncounterID%>" />
<input type="hidden" name="EDIT" value="nad" />
          <p>&nbsp;</p>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
