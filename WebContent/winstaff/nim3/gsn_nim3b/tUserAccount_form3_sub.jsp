<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tUserAccount_form_sub.jsp
    JSP AutoGen on Mar/19/2002
*/%>

<%@ include file="../../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iUserID        =    null;
//    Integer        iCompanyID        =    null;
    boolean accessValid = false;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
	boolean isSalesView = false;
    if (pageControllerHash.containsKey("iUserID")) 
    {
        iUserID        =    (Integer)pageControllerHash.get("iUserID");
//        iCompanyID        =    (Integer)pageControllerHash.get("iCompanyID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
        if (iUserID.intValue() != iEDITID.intValue())
        {
			out.print("<h1>Error: Collision<hr>Do you have multiple windows open?</h1>");
        	accessValid = false;
        }
    } else if (pageControllerHash.containsKey("iUserID_Reporter")) 
    {
        iUserID        =    (Integer)pageControllerHash.get("iUserID_Reporter");
		isSalesView = true;
//        iCompanyID        =    (Integer)pageControllerHash.get("iCompanyID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
        if (iUserID.intValue() != iEDITID.intValue())
        {
			out.print("<h1>Error: Collision<hr>Do you have multiple windows open?</h1>");
        	accessValid = false;
        }
    }
  //page security
  if (accessValid&& (isScheduler2||isReporter) )
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateSDF2 = new java.text.SimpleDateFormat("MMM/dd/yyyy");
      java.text.SimpleDateFormat displayDateSDF3 = new java.text.SimpleDateFormat("MMMM dd yyyy");

//initial declaration of list class and parentID

    bltUserAccount        UserAccount        =    null;


    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit")||request.getParameter( "EDIT" ).equalsIgnoreCase("salesview") )
    {
        UserAccount        =    new    bltUserAccount(iUserID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        UserAccount        =    new    bltUserAccount();
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    try
    {
        testObj = (displayDateSDF1.parse(request.getParameter("UniqueCreateDate"))) ;
    }
    catch(Exception sdf1)
    {

        try
        {
            testObj = (displayDateSDF2.parse(request.getParameter("UniqueCreateDate"))) ;
        }
        catch(Exception sdf2)
        {

            try
            {
                testObj = (displayDateSDF3.parse(request.getParameter("UniqueCreateDate"))) ;
            }
            catch(Exception sdf3)
            {
                if (request.getParameter("UniqueCreateDate").equalsIgnoreCase("today")) 
                {
                    testObj = PLCUtils.getNowDate(false) ;
                }
                else if (request.getParameter("UniqueCreateDate").equalsIgnoreCase("current")) 
                {
                    testObj = displayDateSDF1.parse("01/02/1800") ;
                }
                else if (request.getParameter("UniqueCreateDate").equalsIgnoreCase("na")) 
                {
                    testObj = displayDateSDF1.parse("01/03/1800") ;
                }
                else
                {
                    testObj = displayDateSDF1.parse("01/01/1800") ;
                }
            }

        }

    }

    if ( !UserAccount.getUniqueCreateDate().equals(testObj)  )
    {
         UserAccount.setUniqueCreateDate( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    try
    {
        testObj = (displayDateSDF1.parse(request.getParameter("UniqueModifyDate"))) ;
    }
    catch(Exception sdf1)
    {

        try
        {
            testObj = (displayDateSDF2.parse(request.getParameter("UniqueModifyDate"))) ;
        }
        catch(Exception sdf2)
        {

            try
            {
                testObj = (displayDateSDF3.parse(request.getParameter("UniqueModifyDate"))) ;
            }
            catch(Exception sdf3)
            {
                if (request.getParameter("UniqueModifyDate").equalsIgnoreCase("today")) 
                {
                    testObj = new java.util.Date() ;
                }
                else if (request.getParameter("UniqueModifyDate").equalsIgnoreCase("current")) 
                {
                    testObj = displayDateSDF1.parse("01/02/1800") ;
                }
                else if (request.getParameter("UniqueModifyDate").equalsIgnoreCase("na")) 
                {
                    testObj = displayDateSDF1.parse("01/03/1800") ;
                }
                else
                {
                    testObj = displayDateSDF1.parse("01/01/1800") ;
                }
            }

        }

    }

    if ( !UserAccount.getUniqueModifyDate().equals(testObj)  )
    {
         UserAccount.setUniqueModifyDate( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount UniqueModifyDate not set. this is ok-not an error");
}


//new stuff
try
{
    Integer testObj = new Integer(request.getParameter("SecurityGroupID")) ;
    if ( !UserAccount.getSecurityGroupID().equals(testObj)  )
    {
         UserAccount.setSecurityGroupID( testObj );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount SecurityGroupID not set. this is ok-not an error");
}

//new stuff
try
{
    Integer testObj = new Integer(request.getParameter("PayerID")) ;
    if ( !UserAccount.getPayerID().equals(testObj)  )
    {
         UserAccount.setPayerID( testObj );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount SecurityGroupID not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("GenericSecurityGroupID")) ;
    if ( !UserAccount.getGenericSecurityGroupID().equals(testObj)  )
    {
         UserAccount.setGenericSecurityGroupID( testObj  );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount GenericSecurityGroupID not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts_LevelUser")) ;
    if ( !UserAccount.getComm_Alerts_LevelUser().equals(testObj)  )
    {
         UserAccount.setComm_Alerts_LevelUser( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelUser not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts_LevelManager")) ;
    if ( !UserAccount.getComm_Alerts_LevelManager().equals(testObj)  )
    {
         UserAccount.setComm_Alerts_LevelManager( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelManager not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts_LevelBranch")) ;
    if ( !UserAccount.getComm_Alerts_LevelBranch().equals(testObj)  )
    {
         UserAccount.setComm_Alerts_LevelBranch( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelBranch not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts2_LevelUser")) ;
    if ( !UserAccount.getComm_Alerts2_LevelUser().equals(testObj)  )
    {
         UserAccount.setComm_Alerts2_LevelUser( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelUser not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts2_LevelManager")) ;
    if ( !UserAccount.getComm_Alerts2_LevelManager().equals(testObj)  )
    {
         UserAccount.setComm_Alerts2_LevelManager( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelManager not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Alerts2_LevelBranch")) ;
    if ( !UserAccount.getComm_Alerts2_LevelBranch().equals(testObj)  )
    {
         UserAccount.setComm_Alerts2_LevelBranch( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Alerts_LevelBranch not set. this is ok-not an error");
}



try
{
    Integer testObj = new Integer(request.getParameter("Comm_Report_LevelUser")) ;
    if ( !UserAccount.getComm_Report_LevelUser().equals(testObj)  )
    {
         UserAccount.setComm_Report_LevelUser( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Report_LevelUser not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Report_LevelManager")) ;
    if ( !UserAccount.getComm_Report_LevelManager().equals(testObj)  )
    {
         UserAccount.setComm_Report_LevelManager( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Report_LevelManager not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Comm_Report_LevelBranch")) ;
    if ( !UserAccount.getComm_Report_LevelBranch().equals(testObj)  )
    {
         UserAccount.setComm_Report_LevelBranch( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comm_Report_LevelBranch not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("SecretQuestion")) ;
    if ( !UserAccount.getSecretQuestion().equals(testObj)  )
    {
         UserAccount.setSecretQuestion( testObj);
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount SecretQuestion not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SecretAnswer")) ;
    if ( !UserAccount.getSecretAnswer().equals(testObj)  )
    {
         UserAccount.setSecretAnswer( testObj);
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount SecretAnswer not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ManagerID")) ;
    if ( !UserAccount.getManagerID().equals(testObj)  )
    {
         UserAccount.setManagerID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ManagerID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardFullName")) ;
    if ( !UserAccount.getCreditCardFullName().equals(testObj)  )
    {
         UserAccount.setCreditCardFullName( testObj);
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardFullName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardNumber")) ;
    if ( !UserAccount.getCreditCardNumber().equals(testObj)  )
    {
         UserAccount.setCreditCardNumber( testObj);
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardNumber not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardType")) ;
    if ( !UserAccount.getCreditCardType().equals(testObj)  )
    {
         UserAccount.setCreditCardType( testObj);
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardType not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardExpMonth")) ;
    if ( !UserAccount.getCreditCardExpMonth().equals(testObj)  )
    {
         UserAccount.setCreditCardExpMonth( testObj);
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardExpMonth not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardExpYear")) ;
    if ( !UserAccount.getCreditCardExpYear().equals(testObj)  )
    {
         UserAccount.setCreditCardExpYear( testObj);
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardExpYear not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("CreditCardChargeDate"))) ;
    if ( !UserAccount.getCreditCardChargeDate().equals(testObj)  )
    {
         UserAccount.setCreditCardChargeDate( testObj);
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardChargeDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("CreditCardPostDate"))) ;
    if ( !UserAccount.getCreditCardPostDate().equals(testObj)  )
    {
         UserAccount.setCreditCardPostDate( testObj);
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CreditCardPostDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AlertEmail")) ;
    if ( !UserAccount.getAlertEmail().equals(testObj)  )
    {
         UserAccount.setAlertEmail( testObj);
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount AlertEmail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AlertDays")) ;
    if ( !UserAccount.getAlertDays().equals(testObj)  )
    {
         UserAccount.setAlertDays( testObj);
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount AlertDays not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingComments")) ;
    if ( !UserAccount.getBillingComments().equals(testObj)  )
    {
         UserAccount.setBillingComments( testObj);
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount BillingComments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PromoCode")) ;
    if ( !UserAccount.getPromoCode().equals(testObj)  )
    {
         UserAccount.setPromoCode( testObj);
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount PromoCode not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !UserAccount.getUniqueModifyComments().equals(testObj)  )
    {
         UserAccount.setUniqueModifyComments( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PLCID")) ;
    if ( !UserAccount.getPLCID().equals(testObj)  )
    {
         UserAccount.setPLCID( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount PLCID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("StartPage")) ;
    if ( !UserAccount.getStartPage().equals(testObj)  )
    {
         UserAccount.setStartPage( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount StartPage not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AccountType")) ;
    if ( !UserAccount.getAccountType().equals(testObj)  )
    {
         UserAccount.setAccountType( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount AccountType not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReferenceID")) ;
    if ( !UserAccount.getReferenceID().equals(testObj)  )
    {
         UserAccount.setReferenceID( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount ReferenceID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AccessType")) ;
    if ( !UserAccount.getAccessType().equals(testObj)  )
    {
         UserAccount.setAccessType( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount AccessType not set. this is ok-not an error");
}

//send e-mail
         boolean bStatusChanged = false;
try
{
    Integer testObj = new Integer(request.getParameter("Status")) ;
    if ( !UserAccount.getStatus().equals(testObj)  )
    {
         UserAccount.setStatus( testObj   );
         testChangeID = "1";
         if (UserAccount.getStatus().intValue()==2)
	 {
	         bStatusChanged = true;
         }
    }
}
catch(Exception e)
{
     out.println("UserAccount Status not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LogonUserName")) ;
    if ( !UserAccount.getLogonUserName().equals(testObj)  )
    {
         UserAccount.setLogonUserName( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount LogonUserName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LogonUserPassword")) ;
    if ( !UserAccount.getLogonUserPassword().equals(testObj)  )
    {
//         UserAccount.setLogonUserPassword( myEnc.getMD5Base64(testObj)   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount LogonUserPassword not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttestKeyword1")) ;
    if ( !UserAccount.getAttestKeyword1().equals(testObj)  )
    {
         UserAccount.setAttestKeyword1( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount AttestKeyword1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttestKeyword2")) ;
    if ( !UserAccount.getAttestKeyword2().equals(testObj)  )
    {
         UserAccount.setAttestKeyword2( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount AttestKeyword2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactFirstName")) ;
    if ( !UserAccount.getContactFirstName().equals(testObj)  )
    {
         UserAccount.setContactFirstName( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount ContactFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactLastName")) ;
    if ( !UserAccount.getContactLastName().equals(testObj)  )
    {
         UserAccount.setContactLastName( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount ContactLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !UserAccount.getContactEmail().equals(testObj)  )
    {
         UserAccount.setContactEmail( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail2")) ;
    if ( !UserAccount.getContactEmail2().equals(testObj)  )
    {
         UserAccount.setContactEmail2( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount ContactEmail2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactAddress1")) ;
    if ( !UserAccount.getContactAddress1().equals(testObj)  )
    {
         UserAccount.setContactAddress1( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount ContactAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactAddress2")) ;
    if ( !UserAccount.getContactAddress2().equals(testObj)  )
    {
         UserAccount.setContactAddress2( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount ContactAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactCity")) ;
    if ( !UserAccount.getContactCity().equals(testObj)  )
    {
         UserAccount.setContactCity( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount ContactCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ContactStateID")) ;
    if ( !UserAccount.getContactStateID().equals(testObj)  )
    {
         UserAccount.setContactStateID( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount ContactStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactZIP")) ;
    if ( !UserAccount.getContactZIP().equals(testObj)  )
    {
         UserAccount.setContactZIP( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount ContactZIP not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactPhone")) ;
    if ( !UserAccount.getContactPhone().equals(testObj)  )
    {
         UserAccount.setContactPhone( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount ContactPhone not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("ContactFax")) ;
    if ( !UserAccount.getContactFax().equals(testObj)  )
    {
         UserAccount.setContactFax( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount ContactFax not set. this is ok-not an error");
}







try
{
    Integer testObj = new Integer(request.getParameter("CompanyType")) ;
    if ( !UserAccount.getCompanyType().equals(testObj)  )
    {
         UserAccount.setCompanyType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyType not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CompanyName")) ;
    if ( !UserAccount.getCompanyName().equals(testObj)  )
    {
         UserAccount.setCompanyName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CompanyAddress1")) ;
    if ( !UserAccount.getCompanyAddress1().equals(testObj)  )
    {
         UserAccount.setCompanyAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CompanyAddress2")) ;
    if ( !UserAccount.getCompanyAddress2().equals(testObj)  )
    {
         UserAccount.setCompanyAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CompanyCity")) ;
    if ( !UserAccount.getCompanyCity().equals(testObj)  )
    {
         UserAccount.setCompanyCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CompanyStateID")) ;
    if ( !UserAccount.getCompanyStateID().equals(testObj)  )
    {
         UserAccount.setCompanyStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CompanyZIP")) ;
    if ( !UserAccount.getCompanyZIP().equals(testObj)  )
    {
         UserAccount.setCompanyZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyZIP not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CompanyPhone")) ;
    if ( !UserAccount.getCompanyPhone().equals(testObj)  )
    {
         UserAccount.setCompanyPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount CompanyPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !UserAccount.getComments().equals(testObj)  )
    {
         UserAccount.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Comments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("BillingID")) ;
    if ( !UserAccount.getBillingID().equals(testObj)  )
    {
         UserAccount.setBillingID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount BillingID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PHDBAcknowledgementStatus")) ;
    if ( !UserAccount.getPHDBAcknowledgementStatus().equals(testObj)  )
    {
         UserAccount.setPHDBAcknowledgementStatus( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount PHDBAcknowledgementStatus not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PHDBLetterHeadStatus")) ;
    if ( !UserAccount.getPHDBLetterHeadStatus().equals(testObj)  )
    {
         UserAccount.setPHDBLetterHeadStatus( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount PHDBLetterHeadStatus not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("TaxID")) ;
    if ( !UserAccount.getTaxID().equals(testObj)  )
    {
         UserAccount.setTaxID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount TaxID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("NotUsingPHDBID")) ;
    if ( !UserAccount.getNotUsingPHDBID().equals(testObj)  )
    {
         UserAccount.setNotUsingPHDBID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount NotUsingPHDBID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NotUsingPHDBComments")) ;
    if ( !UserAccount.getNotUsingPHDBComments().equals(testObj)  )
    {
         UserAccount.setNotUsingPHDBComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount NotUsingPHDBComments not set. this is ok-not an error");
}





try
{
    String testObj = new String(request.getParameter("UserNPI")) ;
    if ( !UserAccount.getUserNPI().equals(testObj)  )
    {
         UserAccount.setUserNPI( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UserNPI not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UserStateLicense")) ;
    if ( !UserAccount.getUserStateLicense().equals(testObj)  )
    {
         UserAccount.setUserStateLicense( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UserStateLicense not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UserStateLicenseDesc")) ;
    if ( !UserAccount.getUserStateLicenseDesc().equals(testObj)  )
    {
         UserAccount.setUserStateLicenseDesc( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount UserStateLicenseDesc not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("ImportantNotes_Alert")) ;
    if ( !UserAccount.getImportantNotes_Alert().equals(testObj)  )
    {
         UserAccount.setImportantNotes_Alert( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ImportantNotes_Alert not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("EmailAlertNotes_Alert")) ;
    if ( !UserAccount.getEmailAlertNotes_Alert().equals(testObj)  )
    {
         UserAccount.setEmailAlertNotes_Alert( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount EmailAlertNotes_Alert not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("ImportantNotes")) ;
    if ( !UserAccount.getImportantNotes().equals(testObj)  )
    {
         UserAccount.setImportantNotes( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ImportantNotes not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !UserAccount.getComments().equals(testObj)  )
    {
         UserAccount.setComments( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount Comments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NIM_UserType")) ;
    if ( !UserAccount.getNIM_UserType().equals(testObj)  )
    {
         UserAccount.setNIM_UserType( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount NIM_UserType not set. this is ok-not an error");
}



try
{
    Integer testObj = new Integer(request.getParameter("TotalMonthlyMRIs")) ;
    if ( !UserAccount.getTotalMonthlyMRIs().equals(testObj)  )
    {
         UserAccount.setTotalMonthlyMRIs( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount TotalMonthlyMRIs not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ExpectedMonthlyMRIs")) ;
    if ( !UserAccount.getExpectedMonthlyMRIs().equals(testObj)  )
    {
         UserAccount.setExpectedMonthlyMRIs( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount ExpectedMonthlyMRIs not set. this is ok-not an error");
}




try
{
    Integer testObj = new Integer(request.getParameter("Internal_PrimaryContact")) ;
    if ( !UserAccount.getInternal_PrimaryContact().equals(testObj)  )
    {
         UserAccount.setInternal_PrimaryContact( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount Internal_PrimaryContact not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("LastContacted"))) ;
    if ( !UserAccount.getLastContacted().equals(testObj)  )
    {
         UserAccount.setLastContacted( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount LastContacted not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateOfBirth"))) ;
    if ( !UserAccount.getDateOfBirth().equals(testObj)  )
    {
         UserAccount.setDateOfBirth( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount DateOfBirth not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PersonalInsights")) ;
    if ( !UserAccount.getPersonalInsights().equals(testObj)  )
    {
         UserAccount.setPersonalInsights( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount PersonalInsights not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AllowAsPrimaryContact")) ;
    if ( !UserAccount.getAllowAsPrimaryContact().equals(testObj)  )
    {
         UserAccount.setAllowAsPrimaryContact( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("UserAccount AllowAsPrimaryContact not set. this is ok-not an error");
}


// If an edit, update information; if new, append information
if (testChangeID.equalsIgnoreCase("1"))
{
	UserAccount.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    UserAccount.setUniqueModifyComments(""+UserLogonDescription);

//START-verify this change
/*
boolean isVerified = false;
    bltCompanyAdminLU_List_LU_CompanyID        bltCompanyAdminLU_List_LU_CompanyID        =    new    bltCompanyAdminLU_List_LU_CompanyID(iCompanyID);

if (UserAccount.getAccountType().equals("AdminID"))
{
//if Admin
//declaration of Enumeration
    bltCompanyAdminLU        working_bltCompanyAdminLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltCompanyAdminLU_List_LU_CompanyID.elements();

    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltCompanyAdminLU  = (bltCompanyAdminLU) leCurrentElement.getObject();
        if (working_bltCompanyAdminLU.getAdminID()==UserAccount.getReferenceID())
	{
		isVerified=true;
	}
    }
}
else if (UserAccount.getAccountType().equals("PhysicianID"))
{
//if Physician
//declaration of Enumeration
    bltCompanyAdminLU        working_bltCompanyAdminLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltCompanyAdminLU_List_LU_CompanyID.elements();
    java.util.Vector myAdminsV = new java.util.Vector();
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltCompanyAdminLU  = (bltCompanyAdminLU) leCurrentElement.getObject();
        myAdminsV.addElement(working_bltCompanyAdminLU.getAdminID());
    }
    String newWhere = "";
    if (myAdminsV!=null)
    {
        if (myAdminsV.size()>0)
	{
	    newWhere = "AdminID="+(Integer)myAdminsV.elementAt(0) +" ";
	}
        for (int i=1 ;i<myAdminsV.size();i++)
        {
            newWhere +="OR AdminID="+(Integer)myAdminsV.elementAt(i) +" ";
        }
    }

    searchDB2 mySDB = new searchDB2();
    java.sql.ResultSet myRS = mySDB.executeStatement("select * from tAdminPhysicianLU where ("+newWhere+") order by PhysicianID");

    while (myRS!=null&&myRS.next())
    {
	Integer iPhys = new Integer(myRS.getString("PhysicianID"));
        if (iPhys==UserAccount.getReferenceID())
	{
		isVerified=true;
	}
    }
    mySDB.closeAll();
}
else if (UserAccount.getAccountType().equals("PracticeID"))
{
//if Practice
//declaration of Enumeration
    bltCompanyAdminLU        working_bltCompanyAdminLU;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltCompanyAdminLU_List_LU_CompanyID.elements();
    java.util.Vector myAdminsV = new java.util.Vector();
    while (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltCompanyAdminLU  = (bltCompanyAdminLU) leCurrentElement.getObject();
        myAdminsV.addElement(working_bltCompanyAdminLU.getAdminID());
    }
    String newWhere = "";
    if (myAdminsV!=null)
    {
        if (myAdminsV.size()>0)
	{
	    newWhere = "AdminID="+(Integer)myAdminsV.elementAt(0) +" ";
	}
        for (int i=1 ;i<myAdminsV.size();i++)
        {
            newWhere +="OR AdminID="+(Integer)myAdminsV.elementAt(i) +" ";
        }
    }

    searchDB2 mySDB = new searchDB2();
    java.sql.ResultSet myRS = mySDB.executeStatement("select * from tAdminPracticeLU where ("+newWhere+") order by PracticeID");

    while (myRS!=null&&myRS.next())
    {
	Integer iPhys = new Integer(myRS.getString("PracticeID"));
        if (iPhys==UserAccount.getReferenceID())
	{
		isVerified=true;
	}
        
    }
    mySDB.closeAll();
}


*/
//FINISH-verify this change

//check login page:
if (UserAccount.getAccountType().equalsIgnoreCase("ICID"))
{
	UserAccount.setStartPage("../nim3/IC_Home.jsp");
}
else if (UserAccount.getAccountType().equalsIgnoreCase("GenAdminID"))
{
	UserAccount.setStartPage("../app/AdminPracticeAll_home.jsp");
}
else if (UserAccount.getAccountType().equalsIgnoreCase("ReporterID"))
{
	UserAccount.setStartPage("../nim3/Reports_Home.jsp");
}
else
{
	UserAccount.setStartPage("../nim3/Payer_Home.jsp");
}


	    UserAccount.commitData();


if (UserAccount.getAccountType().equals("PhysicianID")&&bStatusChanged)
{

            bltPhysicianMaster pm = new bltPhysicianMaster(UserAccount.getReferenceID());
	    if (pm.getIsAttestedPending().intValue()==1)
	    {
	       pm.setIsAttestedPending(new Integer(2));
	       pm.setIsAttested(new Integer(1));
	       pm.setAttestDate(pm.getAttestDatePending());
	       pm.setAttestationComments("Account Approved");
	       pm.commitDataForced();
	    }

		    pageControllerHash.put("iUserID-Email",UserAccount.getUniqueID());
		    session.setAttribute("pageControllerHash",pageControllerHash);
		%>
			<script language="JavaScript">
			  window.open('tUserAccount3_PhysActive-Email.jsp','PResdfset','status=yes,scrollbars=yes,resizable=yes,width=500,height=500');
			</script>
		<%


}




    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
}
if (nextPage!=null&&isSalesView)
{
		%>
			<script language="JavaScript">
			 document.location='<%=nextPage%>?EDIT=salesview&EDITID=<%=UserAccount.getUserID()%>';
			</script>
		<%
}
else if (nextPage!=null)
{
		%>
			<script language="JavaScript">
			 document.location='<%=nextPage%>?EDIT=edit&EDITID=<%=UserAccount.getUserID()%>';
			</script>
		<%
}
        %><title>GSN - V2</title>
Successful save

  <%
  }
  else
  {
   //response.sendRedirect("/errorhandler.jsp");
  }
  %>
