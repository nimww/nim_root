<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    filename: tnim2_Authorization_main_NIM_Authorization_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>
<%@ include file="../../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<%@ include file="../../generic/generalDisplay.jsp" %>

<base target=_self>
<%
//initial declaration of list class and parentID
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
    {
        accessValid = true;
    }
  //page security
  if (accessValid&& isScheduler2 )
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
%>


<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p class=title>Search Accounts<br>
<%
try
{

String myUserID = "";
String myLogonUserName = "";
String myContactFirstName = "";
String myContactLastName = "";
String myContactPhone = "";
String myContactEmail = "";
String myContactCity = "";
String myContactStateID = "0";
String myContactZIP = "";
String myAccountType = "";
String myPayerName = "";
String orderBy = "firstname";
int startID = 0;
int maxResults = 50;
int iOpenID = 0;
String sOpenField = "";

boolean firstDisplay = false;

try
{
sOpenField= (request.getParameter("ssOpenField"));
//iOpenID= Integer.parseInt(request.getParameter("ssOpenID"));
maxResults = Integer.parseInt(request.getParameter("maxResults"));
startID = Integer.parseInt(request.getParameter("startID"));
//myUserID = request.getParameter("UserID").toLowerCase();
myLogonUserName = request.getParameter("LogonUserName").toLowerCase();
myContactFirstName = request.getParameter("ContactFirstName").toLowerCase();
myContactLastName = request.getParameter("ContactLastName").toLowerCase();
myContactPhone = request.getParameter("ContactPhone").toLowerCase();
myContactEmail = request.getParameter("ContactEmail").toLowerCase();
myContactCity = request.getParameter("ContactCity").toLowerCase();
myContactStateID = request.getParameter("ContactStateID").toLowerCase();
myContactZIP = request.getParameter("ContactZIP").toLowerCase();
myAccountType = request.getParameter("AccountType").toLowerCase();
myPayerName = request.getParameter("PayerName").toLowerCase();
orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myUserID = "";
myLogonUserName = "";
myContactFirstName = "";
myContactLastName = "";
myContactPhone = "";
myContactEmail = "";
myContactCity = "";
myContactStateID = "0";
myContactZIP = "";
myAccountType = "";
myPayerName = "";
orderBy = "UserID";
}
if (orderBy == null)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myUserID = "";
myLogonUserName = "";
myContactFirstName = "";
myContactLastName = "";
myContactPhone = "";
myContactEmail = "";
myContactCity = "";
myContactStateID = "0";
myContactZIP = "";
myAccountType = "";
myPayerName = "";
orderBy = "UserID";
}


%>
      <a href="../tNIM3_UserAccount_create.jsp?ssOpenID=<%=iOpenID%>&amp;ssOpenField=<%=sOpenField%>" target="_self">Create Record</a></p>
<%
firstDisplay= false;
if (firstDisplay )
{
%>



<table width = 735 cellpadding=2 cellspacing=2>
<tr>
<td width=10>&nbsp;</td>
<td class=tdBase><p>&nbsp;</p>
</td>
</tr>
<tr>
<td width=10>&nbsp;</td>

<td>
<form name="form1" method="POST" action="LI_tUserAccount_Search_gsn.jsp" target="_self">
<input type="hidden" name="ssOpenField" value ="<%=sOpenField%>">
  <table width="400" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#003333">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
        <tr>
         <td class=tdHeaderAlt>
         ID
         </td>
         <td>
         <input type=text name="UserID">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         UserName</td>
         <td>
         <input type=text name="LogonUserName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Last Name
         </td>
         <td>
         <input type=text name="ContactFirstName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         First Name
         </td>
         <td>
         <input type=text name="ContactLastName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Phone
         </td>
         <td>
         <input type=text name="ContactPhone">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Email
         </td>
         <td>
         <input type=text name="ContactEmail">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         City
         </td>
         <td>
         <input type=text name="ContactCity">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         State
         </td>
         <td>
              <select name="ContactStateID">
                <jsp:include page="../../generic/tStateLILong.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="0" />
                </jsp:include>
              </select>
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         ZIP
         </td>
         <td>
         <input type=text name="ContactZIP">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Type
         </td>
         <td>
         <input type=text name="AccountType">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Ref ID
         </td>
         <td>
         <input type=text name="PayerName">
         </td>
        </tr>
            <tr>
            <td class=tdHeaderAlt><p>Sort by:</p></td>
            <td> 
              <select name=orderBy>
                <option value="UserID">ID</option>
                <option value="LogonUserName">Logon Name</option>
                <option value="ContactFirstName">Last Name</option>
                <option value="ContactLastName">Last Name</option>
                <option value="ContactPhone">Phone</option>
                <option value="ContactEmail">Email</option>
                <option value="ContactCity">Postal Code</option>
                <option value="ContactStateID">State</option>
                <option value="ContactZIP">Postal Code</option>
                <option value="AccountType">Type</option>
                <option value="PayerName">Ref ID</option>
              </select>
            </td>
          </tr>
          <tr bgcolor="#CCCCCC"> 
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td> 
              <input type=hidden name="startID" value=0>
              <input type=hidden name="maxResults" value=50>
              <input type="submit" name="Submit2" value="Submit">
            </td>
            <td>&nbsp;</td>
          </tr>
        </table>
</td>
    </tr>
  </table>
  <p>&nbsp;</p>
  </form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p> 
  <%
}
else 
{


String myWhere = "";
	myWhere = "where (TRUE ";
boolean theFirst = true;

try
{
if (!myUserID.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
myWhere += "UserID = '" + myUserID +"'";
theFirst = false;
}
if (!myLogonUserName.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myLogonUserName.indexOf("%")>=0){myWhere += "lower(LogonUserName) LIKE '" + myLogonUserName +"'";}
else if (myLogonUserName.indexOf("=")==0){myWhere += "lower(LogonUserName) = '" + myLogonUserName.substring(1, myLogonUserName.length())+"'";}
else if (myLogonUserName.length()<=3){myWhere += "lower(LogonUserName) LIKE '" + myLogonUserName +"%'";}
else {myWhere += "lower(LogonUserName) LIKE '%" + myLogonUserName +"%'";}
theFirst = false;
}
if (!myContactFirstName.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myContactFirstName.indexOf("%")>=0){myWhere += "lower(ContactFirstName) LIKE '" + myContactFirstName +"'";}
else if (myContactFirstName.indexOf("=")==0){myWhere += "lower(ContactFirstName) = '" + myContactFirstName.substring(1, myContactFirstName.length())+"'";}
else if (myContactFirstName.length()<=3){myWhere += "lower(ContactFirstName) LIKE '" + myContactFirstName +"%'";}
else {myWhere += "lower(ContactFirstName) LIKE '%" + myContactFirstName +"%'";}
theFirst = false;
}
if (!myContactLastName.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myContactLastName.indexOf("%")>=0){myWhere += "lower(ContactLastName) LIKE '" + myContactLastName +"'";}
else if (myContactLastName.indexOf("=")==0){myWhere += "lower(ContactLastName) = '" + myContactLastName.substring(1, myContactLastName.length())+"'";}
else if (myContactLastName.length()<=3){myWhere += "lower(ContactLastName) LIKE '" + myContactLastName +"%'";}
else {myWhere += "lower(ContactLastName) LIKE '%" + myContactLastName +"%'";}
theFirst = false;
}
if (!myContactPhone.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myContactPhone.indexOf("%")>=0){myWhere += "lower(ContactPhone) LIKE '" + myContactPhone +"'";}
else if (myContactPhone.indexOf("=")==0){myWhere += "lower(ContactPhone) = '" + myContactPhone.substring(1, myContactPhone.length())+"'";}
else if (myContactPhone.length()<=3){myWhere += "lower(ContactPhone) LIKE '" + myContactPhone +"%'";}
else {myWhere += "lower(ContactPhone) LIKE '%" + myContactPhone +"%'";}
theFirst = false;
}
if (!myContactEmail.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myContactEmail.indexOf("%")>=0){myWhere += "lower(ContactEmail) LIKE '" + myContactEmail +"'";}
else if (myContactEmail.indexOf("=")==0){myWhere += "lower(ContactEmail) = '" + myContactEmail.substring(1, myContactEmail.length())+"'";}
else if (myContactEmail.length()<=3){myWhere += "lower(ContactEmail) LIKE '" + myContactEmail +"%'";}
else {myWhere += "lower(ContactEmail) LIKE '%" + myContactEmail +"%'";}
theFirst = false;
}
if (!myContactCity.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myContactCity.indexOf("%")>=0){myWhere += "lower(ContactCity) LIKE '" + myContactCity +"'";}
else if (myContactCity.indexOf("=")==0){myWhere += "lower(ContactCity) = '" + myContactCity.substring(1, myContactCity.length())+"'";}
else if (myContactCity.length()<=3){myWhere += "lower(ContactCity) LIKE '" + myContactCity +"%'";}
else {myWhere += "lower(ContactCity) LIKE '%" + myContactCity +"%'";}
theFirst = false;
}
if (!myContactStateID.equalsIgnoreCase("0"))
{
if (!myContactStateID.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
myWhere += "ContactStateID = " + myContactStateID +"";
theFirst = false;
}
}
if (!myContactZIP.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myContactZIP.indexOf("%")>=0){myWhere += "lower(ContactZIP) LIKE '" + myContactZIP +"'";}
else if (myContactZIP.indexOf("=")==0){myWhere += "lower(ContactZIP) = '" + myContactZIP.substring(1, myContactZIP.length())+"'";}
else if (myContactZIP.length()<=3){myWhere += "lower(ContactZIP) LIKE '" + myContactZIP +"%'";}
else {myWhere += "lower(ContactZIP) LIKE '%" + myContactZIP +"%'";}
theFirst = false;
}
if (!myAccountType.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myAccountType.indexOf("%")>=0){myWhere += "lower(AccountType) LIKE '" + myAccountType +"'";}
else if (myAccountType.indexOf("=")==0){myWhere += "lower(AccountType) = '" + myAccountType.substring(1, myAccountType.length())+"'";}
else if (myAccountType.length()<=3){myWhere += "lower(AccountType) LIKE '" + myAccountType +"%'";}
else {myWhere += "lower(AccountType) LIKE '%" + myAccountType +"%'";}
theFirst = false;
}
if (!myPayerName.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myPayerName.indexOf("%")>=0){myWhere += "lower(PayerName) LIKE '" + myPayerName +"'";}
else if (myPayerName.indexOf("=")==0){myWhere += "lower(PayerName) = '" + myPayerName.substring(1, myPayerName.length())+"'";}
else if (myPayerName.length()<=3){myWhere += "lower(PayerName) LIKE '" + myPayerName +"%'";}
else {myWhere += "lower(PayerName) LIKE '%" + myPayerName +"%'";}
theFirst = false;
}
myWhere += ")";

//System.out.println(myWhere);
if (theFirst||myWhere.equalsIgnoreCase(")"))
{
//	myWhere = "where (AccountType in ('AdjusterID','PhysicianID'))";
//	myWhere += ")";
}


}
catch(Exception e)
{
out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
String mysql=("select tUserAccount.*, PayerName from tUserAccount INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.PayerID = tUserAccount.PayerID " + myWhere + " order by " + orderBy);
//out.print(mysql);
myRS = mySS.executeStatement(mysql);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
cnt++;
if (cnt>=startID&&cnt<=startID+maxResults)
{
cnt2++;

String myClass = "tdBase";
if (cnt2%2==0)
{
myClass = "tdBaseAlt";
}
myMainTable +="<tr class="+myClass+">";
if (true)
{
	myMainTable +="<td><input class=\"inputButton_md_Create\" onClick = \"javascript:window.returnValue = '" + myRS.getString("UserID") + "';window.close();\" type=\"Button\" name=\"Select\" value=\"Select\" align=\"middle\"></td>";
}
else
{
	myMainTable +="<td><a href=\"#\">Select Error</a></td>";
}
myMainTable +="<td>";
myMainTable +=myRS.getString("PayerName")+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("ContactFirstName")+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("ContactLastName")+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("ContactPhone")+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("ContactEmail")+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("ContactCity")+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=(new bltStateLI (new Integer(myRS.getString("ContactStateID")) ).getShortState()+"");
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("ContactZIP")+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("LogonUserName")+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=myRS.getString("AccountType")+"";
myMainTable +="</td>";
myMainTable +="</tr>";
}
   }
mySS.closeAll();
endCount = cnt;



if (startID>=endCount)
{
startID = endCount-maxResults;

}

if (maxResults<=0)
{
maxResults=10;
}


if (startID<=0)
{
startID=0;
}




%>
  <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</p>
<table width=780 border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
<tr>
<td width=10>&nbsp;</td>
<td>
<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
<tr>
<td>

<form name="selfForm" method="POST" action="LI_tUserAccount_Search_gsn.jsp">
  <table border=0 cellspacing=1 width='100%' cellpadding=2>
    <tr > 
      <td colspan=4 class=tdHeader><p>Search Here:</p></td>
      <td colspan=1 class=tdBase align=right>
        <%if (startID>0)
{%><a href="LI_tUserAccount_Search_gsn.jsp?startID=<%=(startID-maxResults)%>&amp;maxResults=<%=maxResults%>&amp;orderBy=<%=orderBy%>&amp;UserID=<%=myUserID%>&amp;LogonUserName=<%=myLogonUserName%>&amp;ContactFirstName=<%=myContactFirstName%>&amp;ContactLastName=<%=myContactLastName%>&amp;ContactPhone=<%=myContactPhone%>&amp;ContactEmail=<%=myContactEmail%>&amp;ContactCity=<%=myContactCity%>&amp;ContactStateID=<%=myContactStateID%>&amp;ContactZIP=<%=myContactZIP%>&amp;AccountType=<%=myAccountType%>&amp;PayerName=<%=myPayerName%>"><< previous <%=maxResults%></a> 
          <%
}
else
{
%><p>&nbsp;</p><%
}
%></td>
      <td colspan=1 class=tdBase> 
        <%
if ((startID+maxResults)<endCount)
{
%>
<a href="LI_tUserAccount_Search_gsn.jsp?startID=<%=(startID+maxResults)%>&amp;maxResults=<%=maxResults%>&amp;orderBy=<%=orderBy%>&amp;UserID=<%=myUserID%>&amp;LogonUserName=<%=myLogonUserName%>&amp;ContactFirstName=<%=myContactFirstName%>&amp;ContactLastName=<%=myContactLastName%>&amp;ContactPhone=<%=myContactPhone%>&amp;ContactEmail=<%=myContactEmail%>&amp;ContactCity=<%=myContactCity%>&amp;ContactStateID=<%=myContactStateID%>&amp;ContactZIP=<%=myContactZIP%>&amp;AccountType=<%=myAccountType%>&amp;PayerName=<%=myPayerName%>"> next >> <%=maxResults%></a>         
        <%
}
else
{
%><p>&nbsp;</p><%
}


}
catch(Exception e)
{
out.println("PrevNext:"+e);
}




try{

%>
      </td>
      <td colspan=3 class=tdHeader nowrap> 
<input type=hidden name=ssOpenID value ="<%=iOpenID%>">
<input type=hidden name=ssOpenField value ="<%=sOpenField%>">
<input type=hidden name=maxResults value=<%=maxResults%> >
<input type=hidden name=startID value=0 ><p>Sort:
<select name=orderBy>
      <option value=UserID <%if (orderBy.equalsIgnoreCase("UserID")){out.println(" selected");}%> >ID</option>
      <option value=LogonUserName <%if (orderBy.equalsIgnoreCase("LogonUserName")){out.println(" selected");}%> >Logon Name</option>
      <option value=ContactFirstName <%if (orderBy.equalsIgnoreCase("ContactFirstName")){out.println(" selected");}%> >Last Name</option>
      <option value=ContactLastName <%if (orderBy.equalsIgnoreCase("ContactLastName")){out.println(" selected");}%> >Last Name</option>
      <option value=ContactPhone <%if (orderBy.equalsIgnoreCase("ContactPhone")){out.println(" selected");}%> >Phone</option>
      <option value=ContactEmail <%if (orderBy.equalsIgnoreCase("ContactEmail")){out.println(" selected");}%> >Email</option>
      <option value=ContactCity <%if (orderBy.equalsIgnoreCase("ContactCity")){out.println(" selected");}%> >Postal Code</option>
      <option value=ContactStateID <%if (orderBy.equalsIgnoreCase("ContactStateID")){out.println(" selected");}%> >State</option>
      <option value=ContactZIP <%if (orderBy.equalsIgnoreCase("ContactZIP")){out.println(" selected");}%> >Postal Code</option>
      <option value=AccountType <%if (orderBy.equalsIgnoreCase("AccountType")){out.println(" selected");}%> >Type</option>
      <option value=PayerName <%if (orderBy.equalsIgnoreCase("PayerName")){out.println(" selected");}%> >Ref ID</option>
</select></p>
      </td> 
      <td class=tdHeader colspan=1> 
        <p  align="center"> 
          <input class="inputButton_md_Create" type="Submit" name="Submit" value="Submit" align="middle">
        </p>
      </td>
    </tr>
    <tr class=tdHeader> 
      <td  colspan=1 width=50>&nbsp; 

      </td>
      <td colspan=1> 
        <input type="text" name="PayerName" value="<%=myPayerName%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactFirstName" value="<%=myContactFirstName%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactLastName" value="<%=myContactLastName%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactPhone" value="<%=myContactPhone%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactEmail" value="<%=myContactEmail%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactCity" value="<%=myContactCity%>" size="10">
      </td>
      <td colspan=1> 
              <select name="ContactStateID">
                <jsp:include page="../../generic/tStateLIShort.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myContactStateID%>" />
                </jsp:include>
              </select>
      </td>
      <td colspan=1> 
        <input type="text" name="ContactZIP" value="<%=myContactZIP%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="LogonUserName" value="<%=myLogonUserName%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="AccountType" value="<%=myAccountType%>" size="5">
      </td>
    </tr>
    <tr class=tdHeader> 
    <td>Action</td>
      <td colspan=1> 
        Company
      </td>
      <td colspan=1>First</td>
      <td colspan=1> 
        Last</td>
      <td colspan=1> 
        Phone
      </td>
      <td colspan=1> 
        Email
      </td>
      <td colspan=1>City</td>
      <td colspan=1> 
        State
      </td>
      <td colspan=1> 
        ZIP</td>
      <td colspan=1> 
        Logon Name
      </td>
      <td colspan=1> 
        Type
      </td>
    </tr>
    <%=myMainTable%>



</table> 


<%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>



  </table>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </td>
  </tr>
</table>

<%



  }
  else
  {
   out.println("<p class=instructions>T2: "+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
%>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_ClearID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
