<%@page language="java" import="com.winstaff.*"%>
<%/*

    filename: out\jsp\tNIM3_PayerMaster_form.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../../generic/CheckLogin.jsp" %>

<link href="../ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "../ui_"+thePLCID+"\\top-nav_maint.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../../generic/generalDisplay.jsp" %>
<body onLoad="document.getElementById('ContactLastName').focus();">

    <table width=98% height="100%" cellpadding=30 cellspacing=0 border=0 >
    <tr><td width=50 background="../gsn_nim3/images/nim_v3_bg.jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign="top">
<%
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
  //page security
  if (isScheduler2)
  {

%>

<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p class=title>User Search
        <br />
<input name="button" type="submit" class="inputButton_md_Default" id="button" onClick="document.location='../Maint_Home.jsp';" value="Return to Maintenance" />
      </p>
<%
try
{

String myParentPayerID = "";
String myParentPayerName = "";
String myPayerID = "";
String myPayerName = "";
String myUserID = "";
String myLogonUserName = "";
String myContactLastName = "";
String myContactFirstName = "";
String myContactPhone = "";
String myContactEmail = "";
String myAccountType = "";
String orderBy = "firstname";
int startID = 0;
int maxResults = 50;

boolean firstDisplay = false;

try
{
	myPayerID = request.getParameter("PayerID").toLowerCase();
	maxResults = Integer.parseInt(request.getParameter("maxResults"));
	startID = Integer.parseInt(request.getParameter("startID"));
	myParentPayerID = request.getParameter("ParentPayerID").toLowerCase();
	myParentPayerName = request.getParameter("ParentPayerName").toLowerCase();
	myPayerName = request.getParameter("PayerName").toLowerCase();
	myUserID = request.getParameter("UserID").toLowerCase();
	myLogonUserName = request.getParameter("LogonUserName").toLowerCase();
	myContactLastName = request.getParameter("ContactLastName").toLowerCase();
	myContactFirstName = request.getParameter("ContactFirstName").toLowerCase();
	myContactPhone = request.getParameter("ContactPhone").toLowerCase();
	myContactEmail = request.getParameter("ContactEmail").toLowerCase();
	myAccountType = request.getParameter("AccountType").toLowerCase();
	orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
	maxResults = 50;
	startID = 0;
	firstDisplay = true;
	myParentPayerID = "";
	myPayerID = "";
	myParentPayerName = "";
	myPayerName = "";
	myUserID = "";
	myLogonUserName = "";
	myContactLastName = "";
	myContactFirstName = "";
	myContactPhone = "";
	myContactEmail = "";
	myAccountType = "";
	orderBy = "ParentPayerID";
	myPayerID = request.getParameter("PayerID");
	if (myPayerID==null||myPayerID.equalsIgnoreCase("")){
		myPayerID = "";
	} else {
		myPayerID = myPayerID.toLowerCase();
		firstDisplay = false;
	}
}


if (firstDisplay)
{
%>



<table cellpadding=2 cellspacing=2>
<tr>
<td width=10>&nbsp;</td>

<td>
<form name="form1"  method="POST" action="tUserAccount_query.jsp">
  <table width="400" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#003333">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
        <tr>
         <td class=tdHeaderAlt>
         Parent PayerID
         </td>
         <td>
         <input type=text name="ParentPayerID">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Parent Payer Name
         </td>
         <td>
         <input type=text name="ParentPayerName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         PayerID
         </td>
         <td>
         <input type=text name="PayerID">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Payer Name
         </td>
         <td>
         <input type=text name="PayerName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         UserID
         </td>
         <td>
         <input type=text name="UserID">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Logon Name
         </td>
         <td>
         <input type=text name="LogonUserName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Last Name
         </td>
         <td>
         <input id="ContactLastName" type=text name="ContactLastName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         First Name
         </td>
         <td>
         <input type=text name="ContactFirstName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Phone
         </td>
         <td>
         <input type=text name="ContactPhone">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Email
         </td>
         <td>
         <input type=text name="ContactEmail">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Account Type
         </td>
         <td>
         <input type=text name="AccountType">
         </td>
        </tr>
            <tr>
            <td class=tdHeaderAlt><p>Sort by:</p></td>
            <td> 
              <select name=orderBy>
                <option value="ParentPayerID">Parent PayerID</option>
                <option value="ParentPayerName">Parent Payer Name</option>
                <option value="PayerID">PayerID</option>
                <option value="PayerName">Payer Name</option>
                <option value="UserID">UserID</option>
                <option value="LogonUserName">Logon Name</option>
                <option selected="selected" value="ContactLastName">Last Name</option>
                <option value="ContactFirstName">First Name</option>
                <option value="ContactPhone">Phone</option>
                <option value="ContactEmail">Email</option>
                <option value="AccountType">Account Type</option>
              </select>
            </td>
          </tr>
          <tr bgcolor="#CCCCCC"> 
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td> 
              <input type=hidden name="startID" value=0>
              <input type=hidden name="maxResults" value=50>
              <input type="submit" name="Submit2" value="Submit">
            </td>
            <td>&nbsp;</td>
          </tr>
        </table>
</td>
    </tr>
  </table>
  </form>
  <%
}
else
{


db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(null);String myWhere = "where (";
boolean theFirst = true;

try
{
if (!myParentPayerID.equalsIgnoreCase(""))
{
    if (!myParentPayerID.equalsIgnoreCase(""))
    {
        if (!theFirst) { myWhere+=" and ";}
        myWhere += "ParentPayer.PayerID = ?";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myParentPayerID));
        theFirst = false;
    }
}
if (!myParentPayerName.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myParentPayerName.indexOf("%")>=0)
    {
        myWhere += "lower(ParentPayer.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myParentPayerName));
    }
    else if (myParentPayerName.length()<=3)
    {
        myWhere += "lower(ParentPayer.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myParentPayerName + "%"));
    }
    else
    {
        myWhere += "lower(ParentPayer.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myParentPayerName + "%"));
    }
    theFirst = false;
}
if (!myPayerID.equalsIgnoreCase(""))
{
    if (!myPayerID.equalsIgnoreCase(""))
    {
        if (!theFirst) { myWhere+=" and ";}
        myWhere += "tnim3_payermaster.PayerID = ?";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myPayerID));
        theFirst = false;
    }
}
if (!myPayerName.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myPayerName.indexOf("%")>=0)
    {
        myWhere += "lower(tnim3_payermaster.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPayerName));
    }
    else if (myPayerName.length()<=3)
    {
        myWhere += "lower(tnim3_payermaster.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPayerName + "%"));
    }
    else
    {
        myWhere += "lower(tnim3_payermaster.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myPayerName + "%"));
    }
    theFirst = false;
}
if (!myUserID.equalsIgnoreCase(""))
{
    if (!myUserID.equalsIgnoreCase(""))
    {
        if (!theFirst) { myWhere+=" and ";}
        myWhere += "tUserAccount.UserID = ?";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myUserID));
        theFirst = false;
    }
}
if (!myLogonUserName.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myLogonUserName.indexOf("%")>=0)
    {
        myWhere += "lower(tUserAccount.LogonUserName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myLogonUserName));
    }
    else if (myLogonUserName.length()<=3)
    {
        myWhere += "lower(tUserAccount.LogonUserName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myLogonUserName + "%"));
    }
    else
    {
        myWhere += "lower(tUserAccount.LogonUserName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myLogonUserName + "%"));
    }
    theFirst = false;
}
if (!myContactLastName.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myContactLastName.indexOf("%")>=0)
    {
        myWhere += "lower(tUserAccount.ContactLastName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactLastName));
    }
    else if (myContactLastName.length()<=3)
    {
        myWhere += "lower(tUserAccount.ContactLastName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactLastName + "%"));
    }
    else
    {
        myWhere += "lower(tUserAccount.ContactLastName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactLastName + "%"));
    }
    theFirst = false;
}
if (!myContactFirstName.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myContactFirstName.indexOf("%")>=0)
    {
        myWhere += "lower(tUserAccount.ContactFirstName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactFirstName));
    }
    else if (myContactFirstName.length()<=3)
    {
        myWhere += "lower(tUserAccount.ContactFirstName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactFirstName + "%"));
    }
    else
    {
        myWhere += "lower(tUserAccount.ContactFirstName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactFirstName + "%"));
    }
    theFirst = false;
}
if (!myContactPhone.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myContactPhone.indexOf("%")>=0)
    {
        myWhere += "lower(tUserAccount.ContactPhone) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactPhone));
    }
    else if (myContactPhone.length()<=3)
    {
        myWhere += "lower(tUserAccount.ContactPhone) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactPhone + "%"));
    }
    else
    {
        myWhere += "lower(tUserAccount.ContactPhone) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactPhone + "%"));
    }
    theFirst = false;
}
if (!myContactEmail.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myContactEmail.indexOf("%")>=0)
    {
        myWhere += "lower(tUserAccount.ContactEmail) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactEmail));
    }
    else if (myContactEmail.length()<=3)
    {

        myWhere += "lower(tUserAccount.ContactEmail) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactEmail + "%"));
    }
    else
    {
        myWhere += "lower(tUserAccount.ContactEmail) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactEmail + "%"));
    }
    theFirst = false;
}
if (!myAccountType.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myAccountType.indexOf("%")>=0)
    {
        myWhere += "lower(tUserAccount.AccountType) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myAccountType));
    }
    else if (myAccountType.length()<=3)
    {
        myWhere += "lower(tUserAccount.AccountType) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myAccountType + "%"));
    }
    else
    {
        myWhere += "lower(tUserAccount.AccountType) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myAccountType + "%"));
    }
    theFirst = false;
}
myWhere += ")";

//System.out.println(myWhere);
if (theFirst||myWhere.equalsIgnoreCase(")"))
{
    myWhere = "";
}


}
catch(Exception e)
{
    out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;
String mySQL = "SELECT tuseraccount.logonusername, tuseraccount.status, tuseraccount.accounttype, tuseraccount.userid, tuseraccount.contactfirstname, tuseraccount.contactlastname, tuseraccount.contactemail, tuseraccount.contactphone, tnim3_payermaster.payerid as PayerID, tnim3_payermaster.payername as PayerName, ParentPayer.payerid as ParentPayerID, ParentPayer.payername as ParentPayerName FROM tuseraccount Left JOIN tnim3_payermaster ON tuseraccount.payerid = tnim3_payermaster.payerid Left JOIN tnim3_payermaster AS ParentPayer ON tnim3_payermaster.parentpayerid = ParentPayer.payerid " + myWhere + " order by " + orderBy + " LIMIT " + (maxResults+1) + " OFFSET " + startID + "";

try
{
   myDPSO.setSQL(mySQL);
   //out.print(mySQL);
   myRS = mySS.executePreparedStatement(myDPSO);
}
catch(Exception e)
{
    out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

    int endCount = 0;

    int cnt=0;
    int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
        cnt++;
        if (cnt<=maxResults)
        {
            cnt2++;
            String myClass = "tdBase";
            if (cnt2%2==0)
            {
                myClass = "tdBaseAlt";
            }
			if (myRS.getInt("Status")==3){
				myClass = "tdBaseAltVoid tdBaseFade1";
			}
            myMainTable +="<tr class="+myClass+">";
            myMainTable +="<td>";
			myMainTable +="<a class=tdHeader  href = 'tUserAccount_form_authorize.jsp?EDIT=edit&EDITID="+myRS.getString("UserID")+"'>EDIT</a>";
            myMainTable +="</td>";
            myMainTable +="<td>"+cnt+"</td>";
            myMainTable +="<td>";
			myMainTable +="<a class=linkBase href = '#' onclick=\"document.getElementById('ParentPayerID').value ='"+myRS.getString("ParentPayerID")+"';document.getElementById('searchForm').submit();\" >";
            myMainTable +=myRS.getString("ParentPayerID")+"";
            myMainTable +="</a>";
            myMainTable +="</td>";
            myMainTable +="<td>";
			myMainTable +="<a class=linkBase href = '#' onclick=\"document.getElementById('ParentPayerName').value ='"+myRS.getString("ParentPayerName")+"';document.getElementById('searchForm').submit();\" >";
            myMainTable +=myRS.getString("ParentPayerName")+"";
            myMainTable +="</a>";
            myMainTable +="</td>";
            myMainTable +="<td>";
			myMainTable +="<a class=linkBase href = '#' onclick=\"document.getElementById('PayerID').value ='"+myRS.getString("PayerID")+"';document.getElementById('searchForm').submit();\" >";
            myMainTable +=myRS.getString("PayerID")+"";
            myMainTable +="</a>";
            myMainTable +="</td>";
            myMainTable +="<td>";
			myMainTable +="<a class=linkBase href = '#' onclick=\"document.getElementById('PayerName').value ='"+myRS.getString("PayerName")+"';document.getElementById('searchForm').submit();\" >";
            myMainTable +=myRS.getString("PayerName")+"";
            myMainTable +="</a>";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("UserID")+"";
            myMainTable +="</td>";
            myMainTable +="<td>";
			myMainTable +=myRS.getString("LogonUserName")+"";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("ContactLastName")+"";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("ContactFirstName")+"";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("ContactPhone")+"";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("ContactEmail")+"";
            myMainTable +="</td>";
            myMainTable +="<td>";
			myMainTable +="<a class=linkBase href = '#' onclick=\"document.getElementById('AccountType').value ='"+myRS.getString("AccountType")+"';document.getElementById('searchForm').submit();\" >";
            myMainTable +=myRS.getString("AccountType")+"";
            myMainTable +="</a>";
            myMainTable +="</td>";
            myMainTable +="</tr>";
        }
   }
    mySS.closeAll();
    endCount = cnt;

    if (maxResults<=0)
    {
        maxResults=10;
    }

    if (startID<=0)
    {
        startID=0;
    }




%>
  <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</p>
<table width=100% border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
<tr>
<td width=10>&nbsp;</td>
<td>
<table width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#003333">
<tr>
<td>

<form name="selfForm" id="searchForm" method="POST" action="tUserAccount_query.jsp">
  <table border=0 cellspacing=1 width='100%' cellpadding=4>
    <tr > 
      <td colspan=3 class=tdHeader><p>Instructions Here:</p></td>
      <td colspan=1 class=tdBase align=right nowrap="nowrap">
        <%if (startID>0)
{%><a href="tUserAccount_query.jsp?startID=<%=(startID-maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&ParentPayerID=<%=myParentPayerID%>&ParentPayerName=<%=myParentPayerName%>&PayerID=<%=myPayerID%>&PayerName=<%=myPayerName%>&UserID=<%=myUserID%>&LogonUserName=<%=myLogonUserName%>&ContactLastName=<%=myContactLastName%>&ContactFirstName=<%=myContactFirstName%>&ContactPhone=<%=myContactPhone%>&ContactEmail=<%=myContactEmail%>&AccountType=<%=myAccountType%>"><< previous <%=maxResults%></a> 
          <%
}
else
{
%><p>&nbsp;</p><%
}
%></td>
      <td colspan=1 class=tdBase nowrap="nowrap"> 
        <%
if (cnt>cnt2)
{
%>
<a href="tUserAccount_query.jsp?startID=<%=(startID+maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&ParentPayerID=<%=myParentPayerID%>&ParentPayerName=<%=myParentPayerName%>&PayerID=<%=myPayerID%>&PayerName=<%=myPayerName%>&UserID=<%=myUserID%>&LogonUserName=<%=myLogonUserName%>&ContactLastName=<%=myContactLastName%>&ContactFirstName=<%=myContactFirstName%>&ContactPhone=<%=myContactPhone%>&ContactEmail=<%=myContactEmail%>&AccountType=<%=myAccountType%>"> next >> <%=maxResults%></a>         
        <%
}
else
{
%><p>&nbsp;</p><%
}


}
catch(Exception e)
{
out.println("PrevNext:"+e);
}




try{

%>
      </td>
      <td colspan=1 class=tdHeader nowrap> 
<input type=hidden name=maxResults value=<%=maxResults%> >
<input type=hidden name=startID value=0 ><p>Sort:
<select name=orderBy>
      <option value=ParentPayerID <%if (orderBy.equalsIgnoreCase("ParentPayerID")){out.println(" selected");}%> >Parent PayerID</option>
      <option value=ParentPayerName <%if (orderBy.equalsIgnoreCase("ParentPayerName")){out.println(" selected");}%> >Parent Payer Name</option>
      <option value=PayerID <%if (orderBy.equalsIgnoreCase("PayerID")){out.println(" selected");}%> >PayerID</option>
      <option value=PayerName <%if (orderBy.equalsIgnoreCase("PayerName")){out.println(" selected");}%> >Payer Name</option>
      <option value=UserID <%if (orderBy.equalsIgnoreCase("UserID")){out.println(" selected");}%> >UserID</option>
      <option value=LogonUserName <%if (orderBy.equalsIgnoreCase("LogonUserName")){out.println(" selected");}%> >Logon Name</option>
      <option value=ContactLastName <%if (orderBy.equalsIgnoreCase("ContactLastName")){out.println(" selected");}%> >Last Name</option>
      <option value=ContactFirstName <%if (orderBy.equalsIgnoreCase("ContactFirstName")){out.println(" selected");}%> >First Name</option>
      <option value=ContactPhone <%if (orderBy.equalsIgnoreCase("ContactPhone")){out.println(" selected");}%> >Phone</option>
      <option value=ContactEmail <%if (orderBy.equalsIgnoreCase("ContactEmail")){out.println(" selected");}%> >Email</option>
      <option value=AccountType <%if (orderBy.equalsIgnoreCase("AccountType")){out.println(" selected");}%> >Account Type</option>
</select></p>
      </td> 
      <td class=tdHeader colspan=1> 
        <p  align="center"> 
          <input type="Submit" name="Submit" value="Submit" align="middle">
        </p>
      </td>
    </tr>
    <tr class=tdHeader> 
      <td  colspan=1 >&nbsp;     </td>
      <td  colspan=1 width=50>&nbsp;     </td>
      <td colspan=1> 
        <input type="text" name="ParentPayerID"  id="ParentPayerID" value="<%=myParentPayerID%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="ParentPayerName"  id="ParentPayerName" value="<%=myParentPayerName%>" size="15">
      </td>
      <td colspan=1> 
        <input type="text" name="PayerID"  id="PayerID" value="<%=myPayerID%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="PayerName"  id="PayerName" value="<%=myPayerName%>" size="15">
      </td>
      <td colspan=1> 
        <input type="text" name="UserID" value="<%=myUserID%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="LogonUserName" value="<%=myLogonUserName%>" size="15">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactLastName" value="<%=myContactLastName%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactFirstName" value="<%=myContactFirstName%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactPhone" value="<%=myContactPhone%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactEmail" value="<%=myContactEmail%>" size="15">
      </td>
      <td colspan=1> 
        <input type="text" name="AccountType"  id="AccountType" value="<%=myAccountType%>" size="10">
      </td>
    </tr>
    <tr class=tdHeaderAlt> 
    <td nowrap="nowrap">Action</td>
    <td nowrap="nowrap">#</td>
      <td colspan=1 nowrap="nowrap"> 
        Parent PayerID
      </td>
      <td colspan=1 nowrap="nowrap"> 
        Parent Payer Name
      </td>
      <td colspan=1 nowrap="nowrap"> 
        PayerID
      </td>
      <td colspan=1 nowrap="nowrap"> 
        Payer Name
      </td>
      <td colspan=1 nowrap="nowrap"> 
        UserID
      </td>
      <td colspan=1 nowrap="nowrap"> 
        Logon Name
      </td>
      <td colspan=1 nowrap="nowrap"> 
        Last Name
      </td>
      <td colspan=1 nowrap="nowrap"> 
        First Name
      </td>
      <td colspan=1 nowrap="nowrap"> 
        Phone
      </td>
      <td colspan=1 nowrap="nowrap"> 
        Email
      </td>
      <td colspan=1 nowrap="nowrap"> 
        Account Type
      </td>
    </tr>
    <%=myMainTable%>



</table> 


<%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>



  </table>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table> 
    </td>
  </tr>
</table>





        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>



    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>