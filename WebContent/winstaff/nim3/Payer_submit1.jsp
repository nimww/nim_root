<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
</head>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler) 
    {
        accessValid = true;
    }
    else if (isAdjuster) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
	}
    else if (isProvider) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
	}
	
	
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	
	if (isAdjuster||isAdjuster2||isAdjuster3||isProvider)
	  {
		  %>
<table width="<%=MasterTableWidth%>" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td><table border="0"  cellpadding="0" cellspacing="0">
               <tr>
                 <td colspan="2" align="left" bordercolor="#002F63">           
<%
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
String ip = request.getRemoteAddr();
String host = request.getRemoteHost();


//fields
        %>
  <p class=big2>Online Imaging Referral</p>
  <p class=big>Since you are already signed in, we have your information as follows:</p>
  <% bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
  if(CurrentUserAccount.getPayerID()==308 || myPM.getParentPayerID()==307){}
  else{%>
	  <p>If you are uploading support documents you may use <a href="Payer_submit_quick.jsp">Quick order upload.</a></p>
  <%}%>
  <p class=tdBase>Name: <strong><%=CurrentUserAccount.getContactFirstName()%>&nbsp;<%=CurrentUserAccount.getContactLastName()%></strong> @  <strong><%=new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID()).getPayerName()%></strong><br>
    Phone: <strong><%=CurrentUserAccount.getContactPhone()%></strong><br>Fax: <strong><%=CurrentUserAccount.getContactFax()%></strong><br>Email: <strong><%=CurrentUserAccount.getContactEmail()%></strong></p>
<p class=tdBase>If this information has changed please <a href="tUserAccount_form_settings.jsp">update it here</a>.</p>
  <form name="at_intake" action="Payer_submit1_sub1.jsp">
    <table width="600" border="1" cellpadding="3" cellspacing="2" class="tdBaseAlt">
      <tr>
        <td colspan="4" bgcolor="#CCCCCC" class="tdBaseAlt2">Please link me as the 
          <label>
            <select name="LinkType" id="LinkType">
              <%if (isAdjuster){%><option value="Adjuster" selected>Adjuster</option><%}%>
              <%if (isAdjuster){%> <option value="NCM">Nurse Case Manager</option>><%}%>
              <%if (isProvider){%> <option value="Dr">Referring Physician</option>><%}%>
              <option value="Admin">Case/Referral Admin</option>
              </select>
            </label></td>
      </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Your Name</strong></td>
        <td bgcolor="#CCCCCC"><input name="refName" type=text class="tdHeader" size="20" maxlength="100"></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Your Email</strong></td>
        <td bgcolor="#CCCCCC"><input name="refEmail" type=text class="tdHeader" size="20" maxlength="100"></td>
      </tr>PayerPreAuthorizationConfirmation
      <%
	  if (isAdjuster3||isProvider3)
	  {
		  String myPayerUsersSelect = NIMUtils.getListOfValidUserByPayer_HTMLSelect(CurrentUserAccount.getPayerID(),0);
	%>
      <tr>
        <td colspan="4" bgcolor="#CCCCCC" class="tdBaseAlt2"><p>As a Branch/Office Admin, you can link other users at your location to this case. <br>
            <span class="instructionsBig1">*if users are not listed here, you may enter them at the end of the form.</span></p>
          <p>Please link <select name="Link2">      <option value="0" selected>No Additional User Links</option>
<%=myPayerUsersSelect%></select> as the
            <label>
    <select name="LinkType2" id="LinkType2">
	  <%if (isAdjuster){%><option value="Adjuster" selected>Adjuster</option><%}%>
      <%if (isAdjuster){%> <option value="NCM">Nurse Case Manager</option>><%}%>
      <%if (isProvider){%> <option value="Dr">Referring Physician</option>><%}%>
      <option value="Admin">Case/Referral Admin</option>
      </select>
  </label><br>PayerPreAuthorizationConfirmation
Please link <select name="Link3">      <option value="0" selected>No Additional User Links</option>
<%=myPayerUsersSelect%></select> as the
            <label>
    <select name="LinkType3" id="LinkType3">
	  <%if (isAdjuster){%><option value="Adjuster" selected>Adjuster</option><%}%>
      <%if (isAdjuster){%> <option value="NCM">Nurse Case Manager</option>><%}%>
      <%if (isProvider){%> <option value="Dr">Referring Physician</option>><%}%>
      <option value="Admin">Case/Referral Admin</option>
      </select>
  </label>
          </p></td>
      </tr>
      <%
	  }
	  %>
      
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Claim #</strong></td>
        <td bgcolor="#CCCCCC"><input name="CaseClaimNumber1" type=text class="tdHeader" size="20" maxlength="100"></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Preauthorization#</strong></td>
        <td bgcolor="#CCCCCC"><input name="PayerPreAuthorizationConfirmation" type=text class="tdHeader" size="20" maxlength="100"></td>
      </tr>
      <tr>
        <td colspan="4" bgcolor="#CCCCCC" class="tdHeaderAlt">Patient Information</td>
        </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>First</strong></td>
        <td bgcolor="#CCCCCC"><input name="PatientFirstName" type="text" class="tdHeader" id="PatientFirstName" size="20" maxlength="100" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Last</strong></td>
        
        <td bgcolor="#CCCCCC"><input name="PatientLastName" type="text" class="tdHeader" id="PatientLastName" size="20" maxlength="100" /></td>
        </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>SSN</strong></td>
        <td bgcolor="#CCCCCC"><input name="PatientSSN" type="text" class="tdHeader" id="PatientSSN" size="20" maxlength="20" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>DOB</strong></td>
        <td bgcolor="#CCCCCC"><input name="PatientDOB"  type="text" class="tdHeader" id="PatientDOB" size="20" maxlength="10" /></td>
        </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2">&nbsp;</td>
        <td bgcolor="#CCCCCC">&nbsp;</td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Date of Injury</strong></td>
        <td bgcolor="#CCCCCC"><input name="DateOfInjury" type="text" class="tdHeader" id="DateOfInjury" size="20" maxlength="50" /></td>
        </tr>  
      <tr>
          <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Address</strong></td>
          <td bgcolor="#CCCCCC"><input name="PatientAddress1" type="text" class="tdHeader" id="PatientAddress1" size="20" maxlength="200" /></td>
          <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>City</strong></td>
          <td bgcolor="#CCCCCC"><input name="PatientCity"  type="text" class="tdHeader" id="PatientCity" size="20" maxlength="100" /></td>
          </tr>
      <tr>
          <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>State</strong></td>
          <td bgcolor="#CCCCCC"><select   name="PatientStateID" id="PatientStateID" ><jsp:include page="../generic/tStateLIShort.jsp" flush="true" ><jsp:param name="CurrentSelection" value="0" /></jsp:include></select></td>
          <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>ZIP</strong></td>
          <td bgcolor="#CCCCCC"><input name="PatientZIP"  type="text" class="tdHeader" id="PatientZIP" size="20" maxlength="20" /></td>
          </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Phone</strong></td>
        <td bgcolor="#CCCCCC"><input name="PatientHomePhone" type="text" class="tdHeader" size="20" maxlength="50" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Mobile/Alt #</strong></td>
        <td bgcolor="#CCCCCC"><input name="PatientCellPhone" type="text" class="tdHeader" size="20" maxlength="50" /></td>
        </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Employer</strong></td>
        <td bgcolor="#CCCCCC"><input name="EmployerName" type="text" class="tdHeader" id="EmployerName" size="20" maxlength="50" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Employer Phone</strong></td>
        <td bgcolor="#CCCCCC"><input name="EmployerPhone" type="text" class="tdHeader" id="EmployerPhone" size="20" maxlength="50" /></td>
        </tr>
      <tr>
          <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Employer Address</strong></td>
          <td bgcolor="#CCCCCC"><input name="employerAddress" type="text" class="tdHeader" id="employerAddress" size="20" maxlength="200" /></td>
          <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Employer City</strong></td>
          <td bgcolor="#CCCCCC"><input name="employerCity"  type="text" class="tdHeader" id="employerCity" size="20" maxlength="100" /></td>
          </tr>
      <tr>
          <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Employer State</strong></td>
          <td bgcolor="#CCCCCC"><select   name="employerStateID" id="employerStateID" ><jsp:include page="../generic/tStateLIShort.jsp" flush="true" ><jsp:param name="CurrentSelection" value="0" /></jsp:include></select></td>
          <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Employer ZIP</strong></td>
          <td bgcolor="#CCCCCC"><input name="employerZIP"  type="text" class="tdHeader" id="employerZIP" size="20" maxlength="20" /></td>
          </tr>
      <!--<tr>
        <td colspan="2" bgcolor="#CCCCCC" class="tdHeaderAlt">Patient is claustrophoic: <input type="checkbox" name="isClaustro" value="Yes"></td>
        <td colspan="2" bgcolor="#CCCCCC" class="tdHeaderAlt">Patient is over 300 lbs: <input type="checkbox" name="isOver300" value="Yes"></td>
      </tr> -->
      <tr>
        <td colspan="4" bgcolor="#CCCCCC" class="tdHeaderAlt">Ordering/Referring Physician</td>
      </tr>
      <%if (isProvider){%>
      <tr>
        <td colspan="4" bgcolor="#CCCCCC" class="tdBaseAlt2">If you are not the referring physician and have not linked one above, please fill out the following fields:</td>
      </tr>
      <%}%>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Full Name</strong></td>
        <td bgcolor="#CCCCCC"><input name="ReferringPhysicianName" type=text class="tdHeader" size="20" maxlength="100"></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Phone</strong></td>
        <td bgcolor="#CCCCCC"><input name="ReferringPhysicianPhone" type=text class="tdHeader" size="20" maxlength="100"></td>
        </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Email</strong></td>
        <td bgcolor="#CCCCCC"><input name="ReferringPhysicianEmail" type=text class="tdHeader" id="ReferringPhysicianEmail" size="20" maxlength="100"></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Fax</strong></td>
        <td bgcolor="#CCCCCC"><input name="ReferringPhysicianFax" type=text class="tdHeader" id="ReferringPhysicianFax" size="20" maxlength="100"></td>
        </tr>
      <tr>
        <td colspan="4" bgcolor="#CCCCCC" class="tdHeaderAlt">Adjuster Information</td>
      </tr>
      <%if (isAdjuster){%>
      <tr>
        <td colspan="4" bgcolor="#CCCCCC" class="tdBaseAlt2">If you are not the adjuster and have not linked one above, please fill out the following fields:</td>
      </tr>
	  <%}%>      
      <tr>
        <td nowrap bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Adjuster First</strong></td>
        <td bgcolor="#CCCCCC"><input name="AdjusterFirstName" type="text" class="tdHeader" id="PatientFirstName3" size="20" maxlength="100" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Last</strong></td>
        <td bgcolor="#CCCCCC"><input name="AdjusterLastName" type="text" class="tdHeader" id="PatientLastName3" size="20" maxlength="100" /></td>
      </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Company</strong></td>
        <td bgcolor="#CCCCCC"><input name="AdjusterCompany" type="text" class="tdHeader" id="PatientAddress3" size="20" maxlength="200" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>City/Branch</strong></td>
        <td bgcolor="#CCCCCC"><input name="AdjusterCity"  type="text" class="tdHeader" id="PatientCity3" size="20" maxlength="100" /></td>
      </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Phone</strong></td>
        <td bgcolor="#CCCCCC"><input name="AdjusterPhone" type="text" class="tdHeader" id="AdjusterPhone" size="20" maxlength="50" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Fax</strong></td>
        <td bgcolor="#CCCCCC"><input name="AdjusterFax" type="text" class="tdHeader" id="AdjusterFax" size="20" maxlength="50" /></td>
      </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Email</strong></td>
        <td bgcolor="#CCCCCC"><input name="AdjusterEmail" type="text" class="tdHeader" id="AdjusterEmail" size="20" maxlength="50" /></td>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2">&nbsp;</td>
        <td bgcolor="#CCCCCC">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="4" bgcolor="#CCCCCC" class="tdHeaderAlt">Procedure Information</td>
      </tr>
      <!-- <tr>
        <td colspan="2" bgcolor="#CCCCCC" class="tdHeaderAlt"><span style="color:red">STAT:</span> <input type="checkbox" name="isSTAT" value="Yes"></td>
        <td colspan="2" bgcolor="#CCCCCC" class="tdHeaderAlt">Patient hand carry: <input type="checkbox" name="handCarryCD" value="Yes"> CDs / <input type="checkbox" name="handCarryFilms" value="Yes"> Films</td>
      </tr>-->
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong> Procedure <br>
          &amp; Body Part:</strong></td>
        <td colspan="3" bgcolor="#CCCCCC"><textarea name="CPTText" cols="60" rows="4" class="titleSub2" onKeyDown="textAreaStop(this,500)"></textarea></td>
      </tr>
      <tr>
        <td bgcolor="#CCCCCC" class="tdBaseAlt2"><strong>Additional&nbsp;information:<br>
            <br>
            <span class="instructions">If there are any other users that need to be associated with this case (Nurse Case Managers, Case Admins, etc.) Please list them here with Name, Phone, Fax, Email, Company. </span></strong></td>
        <td colspan="3" bgcolor="#CCCCCC"><textarea name="MessageText" cols="70" rows="10" class="titleSub2" onKeyDown="textAreaStop(this,1900)"></textarea></td>
        </tr>
      <tr>
        <td colspan="4" bgcolor="#CCCCCC" class="tdHeaderBright"><label>
          <input name="button2" type="submit" class="inputButton_lg_Create" id="button2" value="Submit" />
          </label></td>
        </tr>
    </table>
    </form>
           

&nbsp;</td>
               </tr>
               </table>&nbsp;</td>
  </tr>
</table>
          
          <%
	  }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>