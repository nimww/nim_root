<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="java.text.DecimalFormat ,com.winstaff.*" %>
<%/*

    filename: out\jsp\tNIM3_Encounter_form.jsp
    Created on May/28/2008
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<%@ include file="../generic/generalDisplay.jsp" %>


<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_Encounter_form.jsp");
//initial declaration of list class and parentID
    bltNIM3_Encounter        NIM3_Encounter        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit")||request.getParameter( "EDIT" ).substring(0,5).equalsIgnoreCase("print")||request.getParameter( "EDIT" ).equalsIgnoreCase("sched") )
    {
        NIM3_Encounter        =    new    bltNIM3_Encounter(iEncounterID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_Encounter        =    new    bltNIM3_Encounter(UserSecurityGroupID, true);
    }
bltNIM3_Referral NIMR = new bltNIM3_Referral(NIM3_Encounter.getReferralID());
bltNIM3_CaseAccount NCA = new bltNIM3_CaseAccount(NIMR.getCaseID());
bltUserAccount RefDr = new bltUserAccount(NIMR.getReferringPhysicianID());
Integer iAppointmentID = null;
if (request.getParameter("APPID")!=null)
{
	iAppointmentID = new Integer(request.getParameter("APPID"));
}
else
{
	iAppointmentID = NIM3_Encounter.getAppointmentID();
}
bltNIM3_Appointment myNALU = new bltNIM3_Appointment(iAppointmentID);

bltPracticeMaster myPracM = new bltPracticeMaster(myNALU.getProviderID());

//bltICMaster myICM = new bltICMaster (myNALU.getProviderID());
            java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
            java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
            java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
            java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
            java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);
			java.util.Date dbAppointment = myNALU.getAppointmentTime();
			String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
			String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
			String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
			String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
			String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
			String sAppointmentHour = displayDateHour.format((dbAppointment));




//fields
        %>
          <%  String theClass ="tdBase";%>
    <link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />
<table cellpadding=0 cellspacing=0 border=0 width=100% >
    <tr class=title>
      <td>&nbsp;</td>
      <td><table width="100%" border="0" class="certborderPrint" cellspacing="0" cellpadding="10">
        <tr>
          <td><table width="100%" border="2" cellpadding="5" cellspacing="0" class = "certborderPrintInt">
            <tr >
              <td colspan="2" valign="top" class="big3"><p><img src="images/logo1.jpg" width="250" height="70" /><br />
                  Medical Diagnostics Confirmation</p></td>
              </tr>
            <tr>
              <td colspan="2" valign="top"><font size="5">ScanPass: <strong><%=NIM3_Encounter.getScanPass()%></strong></font></td>
              </tr>
            <tr>
              <td valign="top"><p><span class="printBase">Patient: <strong><%=NCA.getPatientFirstName()%> <%=NCA.getPatientLastName()%></strong><br />
                <strong><%=NCA.getPatientAddress1()%> <%=NCA.getPatientAddress2()%><br />
                  <%=NCA.getPatientCity()%>, <%=new bltStateLI(NCA.getPatientStateID()).getShortState()%> <%=NCA.getPatientZIP()%></strong></span></p>
                <p><span class="printBase">Employer: <strong><%=NCA.getEmployerName()%></strong></span></p></td>
              <td valign="top"><span class="printBase">
                DOB: <strong><%=displayDateSDF.format(NCA.getPatientDOB())%></strong><br />
                Date of Injury: <strong><%=displayDateSDF.format(NCA.getDateOfInjury())%></strong><br />
                Home: <strong><%=NCA.getPatientHomePhone()%></strong><br />
                Cell: <strong><%=NCA.getPatientCellPhone()%></strong></span></td>
            </tr>
            <tr>
              <td colspan="2" valign="top"><span class="printBase"> Notes:<br />
                <%=NIM3_Encounter.getComments().replaceAll("(\r\n|\r|\n|\n\r)", "<br>")%>
              
<table border="1" cellpadding="3" cellspacing="0">
        <tr class="tdHeaderAlt">
        <td>CPT</td>
        <td>Desc</td>
        <td>Diagnosis/ICD</td>
        </tr>
        <%

			searchDB2 mySS = new searchDB2();
			
			java.sql.ResultSet myRS = null;;
			
			try
			{
				String mySQL = ("select * from tNIM3_Service where EncounterID = " +   NIM3_Encounter.getEncounterID() +  " AND (CPT>'') order by ServiceID");
				//out.println(mySQL);
				myRS = mySS.executeStatement(mySQL);
			}
			catch(Exception e)
			{
				out.println("ResultsSet:" + e);
			}
			
			String myMainTable= " ";
			int endCount = 0;
			int cnt=0;
			int cnt2=0;
			while (myRS!=null&&myRS.next())
			{
				cnt++;
				bltNIM3_Service working_bltNIM3_Service  =  new bltNIM3_Service(new Integer(myRS.getString("ServiceID")));
				%>
                <tr>
                	<td nowrap="nowrap"><%=working_bltNIM3_Service.getCPT()%></td>
                	<td nowrap="nowrap">BP: <%=NIMUtils.getCPTText(working_bltNIM3_Service.getCPTBodyPart())%></td>
                    <td nowrap="nowrap"><strong><%=working_bltNIM3_Service.getCPTText()%><br /><%=working_bltNIM3_Service.getdCPT1()%></strong> <%=working_bltNIM3_Service.getdCPT2()%> <strong><%=working_bltNIM3_Service.getdCPT3()%></strong> <%=working_bltNIM3_Service.getdCPT4()%>&nbsp;</td>
                 </tr>
                <%
			}
		%>
        </table>              
      <br />
*Please verify all procedure information with attached Rx from Referring Doctor</span>
        

</td>

              </tr>
            <tr>
            <tr>
              <td valign="top"><p><span class="printBase">Your   appointment is confirmed at:</span></p>
                <span class="printBase"><strong><%=myPracM.getPracticeName()%></strong><br />
                <%=myPracM.getOfficeAddress1()%><br />
                <%=myPracM.getOfficeCity()%>, <%=new bltStateLI(myPracM.getOfficeStateID()).getShortState()%> <%=myPracM.getOfficeZIP()%><br />
                <a href="http://maps.google.com/maps?q=<%=myPracM.getOfficeAddress1()%>,<%=myPracM.getOfficeZIP()%>" target="_blank">View Map</a><br />
                Phone: <%=myPracM.getOfficePhone()%></span></td>
              <td valign="top"><p><span class="printBase"><strong><%=sAppointmentTimeDayWeek%> <%=sAppointmentTime%></strong></span></p></td>
            </tr>
            <tr>
              <td valign="top"><span class="printBase">Referring Physician: <strong><%=RefDr.getContactFirstName()%> <%=RefDr.getContactLastName()%><br />
                </strong></span></td>
              <td valign="top"><span class="printBase">Phone: <strong><%=RefDr.getContactPhone()%></strong><br />
                Fax: <strong><%=RefDr.getContactFax()%></strong></span></td>
            </tr>
            <tr>
              <td colspan="2" valign="top"><p><span class="printBase"><strong>Instructions for Patient</strong></span></p>                <ol>
                  <li><span class="printBase">Please arrive 15 minutes before your appointment time to ensure proper preparation.</span></li>
                  <li><span class="printBase">If you have any questions, please call us at <strong>(888) 318 - 5111</strong></span></li>
                </ol></td>
            </tr>
            <tr>
              <td colspan="2" valign="top"><p><span class="printBase"><strong>Instructions for Diagnostics Facility:</strong></span></p>
                <ol>
                  <li><span class="printBase">Please  call patient prior to appointment to confirm scheduled time and any other needs</span></li>
                  <li><span class="printBase">Fax  the medical report within 24 hours of procedure to (888) 596-8680.</span></li>
                  <li><span class="printBase">Mail  all claims on CMS1500 to:</span></li>
                </ol><center>
                  <span class="printBase">NextImage Medical, Inc.<br />
                  Attn: Medical Claims<br />
                  3390 Carmel Mountain Road Suite 150<br />
                San Diego, CA 92121</span>
                </center></td>
              </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
    <tr class=title><td width=10>&nbsp;</td><td><h1>&nbsp;</h1></td></tr>
    <tr><td width=10>&nbsp;</td><td>
        <table width=100% border=0 bordercolor=#333333 cellpadding=5 cellspacing=0 >
         <tr><td >

        </td></tr>
      
        </table>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>
<script language="javascript">window.print();</script>


