<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="java.text.DecimalFormat ,com.winstaff.*" %>
<%/*

    filename: out\jsp\tNIM3_Encounter_form.jsp
    Created on May/28/2008
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>





<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_Encounter_form.jsp");
//initial declaration of list class and parentID

    bltNIM3_Encounter        NIM3_Encounter        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit")||request.getParameter( "EDIT" ).equalsIgnoreCase("sched") )
    {
        NIM3_Encounter        =    new    bltNIM3_Encounter(iEncounterID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_Encounter        =    new    bltNIM3_Encounter(UserSecurityGroupID, true);
    }
bltNIM3_Referral NIMR = new bltNIM3_Referral(NIM3_Encounter.getReferralID());
bltNIM3_CaseAccount NCA = new bltNIM3_CaseAccount(NIMR.getCaseID());
//fields
        %>
          <%  String theClass ="tdBase";%>
          <link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />
          
    <table cellpadding=0 cellspacing=0 border=0  >
    <tr class=title><td width=10>&nbsp;</td><td class=title>Schedule Procedure</td></tr>
    <tr><td width=10>&nbsp;</td><td>
        <table  border=0 bordercolor=#333333 cellpadding=5 cellspacing=0 >
         <tr>
           <td >
		   <%
		   Integer iAppointmentID = null;
if (request.getParameter("EDITID")!=null)
{
	iAppointmentID = new Integer(request.getParameter("EDITID"));
}
bltNIM3_Appointment myNALU = new bltNIM3_Appointment(iAppointmentID);
bltPracticeMaster myPracM = new bltPracticeMaster(myNALU.getProviderID());

//bltICMaster myICM = new bltICMaster (myNALU.getProviderID());
int iDed = 0;;
try
{
	iDed = (new Integer(NCA.getPatientCCAccountNumber2())).intValue();
}
catch (Exception e)
{
	iDed = 0;
}
int iDedT = 2000;
int iDedLeft = iDedT-iDed;
int iProc = 450;
int iHSA = iProc;
int iPay = 0;
if ( (iDedLeft-iProc)<0)
{
	iHSA = iDedLeft;
	iPay = iProc - iDedLeft;
}

            java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
            java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
            java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
            java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
            java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);
			java.text.SimpleDateFormat displayDateHourMin = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHourMin);
			java.util.Date dbAppointment = myNALU.getAppointmentTime();
			String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
			String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
			String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
			String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
			String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
			String sAppointmentHourMin = displayDateHourMin.format((dbAppointment));

		   %>
           <table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr class=tdHeaderAlt>
    <td class=tdHeaderAlt>You have Chosen the following Imaging Facility:</td>
  </tr>
  <tr>
    <td><strong><%=myPracM.getPracticeName()%></strong><br />
				<%=myPracM.getOfficeAddress1()%><br />
			<%=myPracM.getOfficeCity()%>, <%=new bltStateLI(myPracM.getOfficeStateID()).getShortState()%> <%=myPracM.getOfficeZIP()%><br />
<a href="http://maps.google.com/maps?q=<%=myPracM.getOfficeAddress1()%>,<%=myPracM.getOfficeZIP()%>" target="_blank">View Map</a><br />
Phone: <%=myPracM.getOfficePhone()%><br />

<%=myPracM.getPracticeID()%></td>
  </tr>
  <tr class=tdHeaderAlt>
    <td class=tdHeaderAlt>Appointment Date/Time:</td>
  </tr>
  <tr>
    <td class="borderHighlightGreen">
<%=sAppointmentTimeDayWeek%>&nbsp;<%=sAppointmentTimeMonth%>&nbsp;<%=sAppointmentTimeDay%>&nbsp;<%=sAppointmentTimeYear%> at <%=sAppointmentHourMin%>    </td>
  </tr>
</table></td>
  </tr>
  <tr>
    <td><input name="confirm" type="button" onclick="this.disable=true;document.location='tImaging_schedule_sub2.jsp?EDIT=sched&EDITID=<%=myNALU.getAppointmentID()%>';" value="Confirm" class="inputButton_md_Create" /></td>
  </tr>
  <tr>
    <td><a href="tImaging_schedule.jsp">Return</a></td>
  </tr>
</table>
</td>
         </tr>
         <tr>         </tr>
    </table>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


