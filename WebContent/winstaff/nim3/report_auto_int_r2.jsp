<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<%@ include file="../generic/generalDisplay.jsp" %>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>NIM3 Start</title>
<style type="text/css">
<!--
th 
{
	font-family: Verdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #FFF;
	background-color: #036;
}

  .normal { background-color: #FFFFFF }
  .tdAlt { background-color: #CCCCCC }
  .highlight { background-color: #8888FF }	
</style>


</head>

<body>
<% 
	String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_report.jsp?plcID="+thePLCID;
%>

<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>

<table border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td class="title"><p>Payer Stats (2013)</p>
      <p><span class="tdBaseAltYellow">Please verify this data and report any errors or inconsistencies. </span><br>
    </p></td>
  </tr>
  <tr>
    <td>

<%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isReporter) 
    {
        accessValid = true;
    }
	if (accessValid)
	{
	boolean isAdj = false;
	boolean isShowAll = false;
	boolean isShowMonth = true;
	String ssPID = "";
	String ssShowAll = "t";
		try
		{
			if (request.getParameter("PID")!=null&&!request.getParameter("PID").equalsIgnoreCase(""))
			{
				isAdj = true;
				ssPID = request.getParameter("PID");
			}

			if (pageControllerHash.containsKey("showall")) 
			{
				ssShowAll = (String) pageControllerHash.get("showall");
			}
			if (request.getParameter("showall")!=null&&!request.getParameter("showall").equalsIgnoreCase(""))
			{
				ssShowAll = request.getParameter("showall");
				if (ssShowAll.equalsIgnoreCase("T"))
				{
					pageControllerHash.put("showall","T");
				}
				else if (ssShowAll.equalsIgnoreCase("M"))
				{
					pageControllerHash.put("showall","M");
				}
				else if (ssShowAll.equalsIgnoreCase("A"))
				{
					pageControllerHash.put("showall","A");
				}
			  session.setAttribute("pageControllerHash",pageControllerHash);
			}
			if (ssShowAll.equalsIgnoreCase("a"))
			{
				isShowAll=true;
			}
			else if (ssShowAll.equalsIgnoreCase("m"))
			{
				isShowMonth=true;
			}
			else if (ssShowAll.equalsIgnoreCase("t"))
			{
				isShowMonth=false;
				isShowAll=false; 
			}
			searchDB2 mySS = new searchDB2();
			String mySQL = ("SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerID\" AS \"PID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\" AS \"Name\",\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\" AS \"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_SalesDivision\" AS \"Sales\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" AS \"iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" AS \"iMonth\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" AS \"iDay\", Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") AS \"iCount\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Type\" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" GROUP BY \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Payer_SalesDivision\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Type\" order by \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" , \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" , \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" ");
			if (isAdj)
			{
				mySQL = ("SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_UserID\" AS \"PID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_FirstName\" || ' ' || \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"UA_Adjuster_LastName\" AS \"Name\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" AS \"iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" AS \"iMonth\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\" AS \"iDay\", Count(\"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_ScanPass\") AS \"iCount\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Type\" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" where \"PayerID\"="+ssPID+" GROUP BY  \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PayerName\"  ,\"PID\", \"Name\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Encounter_Type\" order by \"PID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iYear\" , \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iMonth\" , \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Referral_ReceiveDate_iDay\"");
			}
			java.sql.ResultSet result = null;
			result = mySS.executeStatement(mySQL);
			java.util.Hashtable myPayers = new java.util.Hashtable();
			int i5=0;
			java.util.Calendar cal = java.util.Calendar.getInstance();
			int nowDay = cal.get(java.util.Calendar.DATE);
			int nowMonth = cal.get(java.util.Calendar.MONTH) + 1;
			int nowYear = cal.get(java.util.Calendar.YEAR);
			/*
				Note:  	For Variable Names:
						2010 = Last Year
						2011 = Current Year
			
			*/
			while (result.next()) 
			{
				i5++;
				String IPID_ID = (result.getString("PID"));
				String SMOD = (result.getString("Encounter_Type"));
				String pNAME = (result.getString("Name"));
				String pPayerNAME = (result.getString("PayerName"));
				String pDIV = "";
				if (!isAdj)
				{
					pDIV = (result.getString("Sales"));
				}
				String IPID = pNAME + " ["+IPID_ID+"]";
				java.util.Hashtable myNumbers = null;
				int[] tempARMonth_MR2011 = new int[13];
				int[] tempARMonth_CT2011 = new int[13];
				int[] tempARMonth_EMG2011 = new int[13];
				int[] tempARMonth_Oth2011 = new int[13];
				Integer tempATotal_MR2011= new Integer(0);
				Integer tempATotal_CT2011= new Integer(0);
				Integer tempATotal_EMG2011= new Integer(0);
				Integer tempATotal_Oth2011= new Integer(0);

				int[] tempARMonth_MR2010 = new int[13];
				int[] tempARMonth_CT2010 = new int[13];
				int[] tempARMonth_EMG2010 = new int[13];
				int[] tempARMonth_Oth2010 = new int[13];
				Integer tempATotal_MR2010 = new Integer(0);
				Integer tempATotal_CT2010 = new Integer(0);
				Integer tempATotal_EMG2010 = new Integer(0);
				Integer tempATotal_Oth2010 = new Integer(0);

				Integer tempTDay_MR= new Integer(0);
				Integer tempTDay_CT= new Integer(0);
				Integer tempTDay_EMG= new Integer(0);
				Integer tempTDay_Oth= new Integer(0);
				if (myPayers.containsKey(IPID))
				{
//					out.print("<br>Using Hash for " + IPID);
					myNumbers = (java.util.Hashtable) myPayers.get(IPID);
					tempARMonth_MR2011 = (int[])myNumbers.get("arMonth_MR2011");
					tempARMonth_CT2011 = (int[])myNumbers.get("arMonth_CT2011");
					tempARMonth_EMG2011 = (int[])myNumbers.get("arMonth_EMG2011");
					tempARMonth_Oth2011 = (int[])myNumbers.get("arMonth_Oth2011");
					tempATotal_MR2011= (Integer)myNumbers.get("aTotal_MR2011");
					tempATotal_CT2011= (Integer)myNumbers.get("aTotal_CT2011");
					tempATotal_EMG2011= (Integer)myNumbers.get("aTotal_EMG2011");
					tempATotal_Oth2011= (Integer)myNumbers.get("aTotal_Oth2011");

					tempARMonth_MR2010 = (int[])myNumbers.get("arMonth_MR2010");
					tempARMonth_CT2010 = (int[])myNumbers.get("arMonth_CT2010");
					tempARMonth_EMG2010 = (int[])myNumbers.get("arMonth_EMG2010");
					tempARMonth_Oth2010 = (int[])myNumbers.get("arMonth_Oth2010");
					tempATotal_MR2010 = (Integer)myNumbers.get("aTotal_MR2010");
					tempATotal_CT2010 = (Integer)myNumbers.get("aTotal_CT2010");
					tempATotal_EMG2010 = (Integer)myNumbers.get("aTotal_EMG2010");
					tempATotal_Oth2010 = (Integer)myNumbers.get("aTotal_Oth2010");
					
					tempTDay_MR= (Integer)myNumbers.get("tDay_MR");
					tempTDay_CT= (Integer)myNumbers.get("tDay_CT");
					tempTDay_EMG= (Integer)myNumbers.get("tDay_EMG");
					tempTDay_Oth= (Integer)myNumbers.get("tDay_Oth");
				}
				else if (true)
				{
//					out.print("<br>Creating Hash for " + IPID);
					myNumbers = new java.util.Hashtable();
					myNumbers.put("Name",pNAME);
					myNumbers.put("Sales",pDIV);
					myNumbers.put("IPID_ID",IPID_ID);
					myNumbers.put("PayerName",pPayerNAME);
				}
				int iYear = 0;
				int iDay = 0;
				int iMonth = 0;
				int iCount = 0;
				try
				{
					iYear = new Integer(result.getString("iyear"));
					iDay = new Integer(result.getString("iday"));
					iMonth = new Integer(result.getString("imonth"));
					iCount = new Integer(result.getString("icount"));
				}
				catch (Exception eee)
				{
//					out.print("caught[use]:"+eee);								  
				}
				if (SMOD.equalsIgnoreCase("MR"))
				{
					if (iYear==nowYear)
					{
						tempATotal_MR2011 += iCount;
						tempARMonth_MR2011[iMonth] += iCount;
						if (iMonth==nowMonth)
						{
							if (iDay==nowDay)
							{
								tempTDay_MR += iCount;
							}
						}
					}
					else if (iYear==nowYear-1)
					{
						tempATotal_MR2010 += iCount;
						tempARMonth_MR2010[iMonth] += iCount;
					}
				}
				else if (SMOD.equalsIgnoreCase("CT"))
				{
					if (iYear==nowYear)
					{
						tempATotal_CT2011 += iCount;
						tempARMonth_CT2011[iMonth] += iCount;
						if (iMonth==nowMonth)
						{
							if (iDay==nowDay)
							{
								tempTDay_CT += iCount;
							}
						}
					}
					else if (iYear==nowYear-1)
					{
						tempATotal_CT2010 += iCount;
						tempARMonth_CT2010[iMonth] += iCount;
					}
				}
				else if (SMOD.equalsIgnoreCase("EMG"))
				{
					if (iYear==nowYear)
					{
						tempATotal_EMG2011 += iCount;
						tempARMonth_EMG2011[iMonth] += iCount;
						if (iMonth==nowMonth)
						{
							if (iDay==nowDay)
							{
								tempTDay_EMG += iCount;
							}
						}
					}
					else if (iYear==nowYear-1)
					{
						tempATotal_EMG2010 += iCount;
						tempARMonth_EMG2010[iMonth] += iCount;
					}
				}
				else 
				{
					if (iYear==nowYear)
					{
						tempATotal_Oth2011 += iCount;
						tempARMonth_Oth2011[iMonth] += iCount;
						if (iMonth==nowMonth)
						{
							if (iDay==nowDay)
							{
								tempTDay_Oth += iCount;
							}
						}
					}
					else if (iYear==nowYear-1)
					{
						tempATotal_Oth2010 += iCount;
						tempARMonth_Oth2010[iMonth] += iCount;
					}
				}
				myNumbers.put("arMonth_MR2011",tempARMonth_MR2011);
				myNumbers.put("arMonth_CT2011",tempARMonth_CT2011);
				myNumbers.put("arMonth_EMG2011",tempARMonth_EMG2011);
				myNumbers.put("arMonth_Oth2011",tempARMonth_Oth2011);
				myNumbers.put("aTotal_MR2011",tempATotal_MR2011);
				myNumbers.put("aTotal_CT2011",tempATotal_CT2011);
				myNumbers.put("aTotal_EMG2011",tempATotal_EMG2011);
				myNumbers.put("aTotal_Oth2011",tempATotal_Oth2011);

				myNumbers.put("arMonth_MR2010",tempARMonth_MR2010);
				myNumbers.put("arMonth_CT2010",tempARMonth_CT2010);
				myNumbers.put("arMonth_EMG2010",tempARMonth_EMG2010);
				myNumbers.put("arMonth_Oth2010",tempARMonth_Oth2010);
				myNumbers.put("aTotal_MR2010",tempATotal_MR2010);
				myNumbers.put("aTotal_CT2010",tempATotal_CT2010);
				myNumbers.put("aTotal_EMG2010",tempATotal_EMG2010);
				myNumbers.put("aTotal_Oth2010",tempATotal_Oth2010);
				
				myNumbers.put("tDay_MR",tempTDay_MR);
				myNumbers.put("tDay_CT",tempTDay_CT);
				myNumbers.put("tDay_EMG",tempTDay_EMG);
				myNumbers.put("tDay_Oth",tempTDay_Oth);
				myPayers.put(IPID,myNumbers);
			}
			if (isAdj)
			{	
				%>
                Showing Adjusters: <a href="report_auto_int_r2.jsp"><< Back</a><br><br>
                <%
			}
			if (isShowAll)
			{
			%>
            Current: Showing All<br>
        <input name="Show Only Those Who Referred Today" type="submit" class="tdBaseAltYellow" id="Show Only Those Who Referred Today" value="Show Only Those Who Referred Today" onClick="document.location='report_auto_int_r2.jsp?PID=<%=ssPID%>&showall=t';">  &nbsp;&nbsp;         <input name="Show Only Those Who Referred Month" type="submit" class="tdBaseAltYellow" id="Show Only Those Who Referred This Month" value="Show Only Those Who Referred This Month" onClick="document.location='report_auto_int_r2.jsp?PID=<%=ssPID%>&showall=m';">  

	  <% 
			}
			else if (isShowMonth)
			{
			%>
            Current: Show Only Those Who Referred This Month<br>        <input name="Show All" type="submit" class="tdBaseAltYellow" id="Show All" value="Show All" onClick="document.location='report_auto_int_r2.jsp?PID=<%=ssPID%>&showall=a';">     
        <input name="Show Only Those Who Referred Today" type="submit" class="tdBaseAltYellow" id="Show Only Those Who Referred Today" value="Show Only Those Who Referred Today" onClick="document.location='report_auto_int_r2.jsp?PID=<%=ssPID%>&showall=t';">  &nbsp;&nbsp;         
	  <% 
			}
			else
			{
			%>
            Current: Show Only Those Who Referred Today<br>  
        <input name="Show All" type="submit" class="tdBaseAltYellow" id="Show All" value="Show All" onClick="document.location='report_auto_int_r2.jsp?PID=<%=ssPID%>&showall=a';">     
<input name="Show Only Those Who Referred Month" type="submit" class="tdBaseAltYellow" id="Show Only Those Who Referred Month" value="Show Only Those Who Referred This Month" onClick="document.location='report_auto_int_r2.jsp?PID=<%=ssPID%>&showall=m';">  
	  <% 
			}
			%>
			<a href="#">Show Top 20</a>

			<table bgcolor="#FFFFFF" border="1" cellpadding="3" cellspacing="0" bordercolor="#666666"> 
            <%
			// now write out one row for each entry in the database table
			java.util.Vector eList = new java.util.Vector(myPayers.keySet());
			java.util.Collections.sort(eList);
			java.util.Iterator it = eList.iterator();
			int cnt=0;
			if (it.hasNext())
			{
				 while (it.hasNext())
				 {
					String element =  (String)it.next();
					java.util.Hashtable myNumbers = (java.util.Hashtable) myPayers.get(element);
					String tempIPID = (String) myNumbers.get("IPID_ID");
					String tempName = (String) myNumbers.get("Name");
					String tempDiv = (String) myNumbers.get("Sales");
					String tempPayerName = (String) myNumbers.get("PayerName");
					int[] tempARMonth_MR2010 = (int[])myNumbers.get("arMonth_MR2010");
					int[] tempARMonth_CT2010 = (int[])myNumbers.get("arMonth_CT2010");
					int[] tempARMonth_EMG2010 = (int[])myNumbers.get("arMonth_EMG2010");
					int[] tempARMonth_Oth2010 = (int[])myNumbers.get("arMonth_Oth2010");
					Integer tempATotal_MR2010= (Integer)myNumbers.get("aTotal_MR2010");
					Integer tempATotal_CT2010= (Integer)myNumbers.get("aTotal_CT2010");
					Integer tempATotal_EMG2010= (Integer)myNumbers.get("aTotal_EMG2010");
					Integer tempATotal_Oth2010= (Integer)myNumbers.get("aTotal_Oth2010");

					int[] tempARMonth_MR2011 = (int[])myNumbers.get("arMonth_MR2011");
					int[] tempARMonth_CT2011 = (int[])myNumbers.get("arMonth_CT2011");
					int[] tempARMonth_EMG2011 = (int[])myNumbers.get("arMonth_EMG2011");
					int[] tempARMonth_Oth2011 = (int[])myNumbers.get("arMonth_Oth2011");
					Integer tempATotal_MR2011= (Integer)myNumbers.get("aTotal_MR2011");
					Integer tempATotal_CT2011= (Integer)myNumbers.get("aTotal_CT2011");
					Integer tempATotal_EMG2011= (Integer)myNumbers.get("aTotal_EMG2011");
					Integer tempATotal_Oth2011= (Integer)myNumbers.get("aTotal_Oth2011");

					Integer tempTDay_MR= (Integer)myNumbers.get("tDay_MR");
					Integer tempTDay_CT= (Integer)myNumbers.get("tDay_CT");
					Integer tempTDay_EMG= (Integer)myNumbers.get("tDay_EMG");
					Integer tempTDay_Oth= (Integer)myNumbers.get("tDay_Oth");
					int monthtotal = tempARMonth_MR2011[nowMonth] + tempARMonth_CT2011[nowMonth] + tempARMonth_EMG2011[nowMonth] + tempARMonth_Oth2011[nowMonth];
					String addClass_td = "";
					if ((tempTDay_MR + tempTDay_CT + tempTDay_EMG + tempTDay_Oth)>0)
					{
						addClass_td = "class = \"tdBaseAlt_Create\"";
					}
					if (isShowAll||(tempTDay_MR + tempTDay_CT + tempTDay_EMG + tempTDay_Oth)>0||(isShowMonth&&monthtotal>0))
					{
						if (cnt%6==0)
						{
						%>
							<tr class="tdHeaderAlt">
							  <td rowspan="2">Name</td>
							  <td rowspan="2"><%
							if (isAdj)
							{
								out.print("Payer");
							}
							else
							{
								out.print("Sales");
							}
							%></td>
							  <td rowspan="2">Today</td>
							  <td rowspan="2">All Time</td>
							  <td class="tdBaseAlt4Blue">&nbsp;</td>
							  <td colspan="14" align="center" class="tdBaseAltGreen"><span class="titleSub1">Current Year: 2013</span></td>
							  <td class="tdBaseAlt4Blue">&nbsp;</td>
							  <td colspan="14" align="center" class="titleSub1">Last Year: 2012</td>
							  <td class="tdBaseAlt4Blue">&nbsp;</td>
						    </tr>
							<tr class="tdHeaderAlt">
							<td class="tdBaseAlt4Blue">&nbsp;</td>
							<td>Year: 2013</td>
							<td>&nbsp;</td>
							<td>Jan&nbsp;</td>
							<td>Feb&nbsp;</td>
							<td>Mar&nbsp;</td>
							<td>Apr&nbsp;</td>
							<td>May&nbsp;</td>
							<td>Jun&nbsp;</td>
							<td>Jul&nbsp;</td>
							<td>Aug&nbsp;</td>
							<td>Sep&nbsp;</td>
							<td>Oct&nbsp;</td>
							<td>Nov&nbsp;</td>
							<td>Dec&nbsp;</td>
							  <td class="tdBaseAlt4Blue">&nbsp;</td>
							<td>Year: 2012</td>
							<td>&nbsp;</td>
							<td>Jan&nbsp;</td>
							<td>Feb&nbsp;</td>
							<td>Mar&nbsp;</td>
							<td>Apr&nbsp;</td>
							<td>May&nbsp;</td>
							<td>Jun&nbsp;</td>
							<td>Jul&nbsp;</td>
							<td>Aug&nbsp;</td>
							<td>Sep&nbsp;</td>
							<td>Oct&nbsp;</td>
							<td>Nov&nbsp;</td>
							<td>Dec&nbsp;</td>
							  <td class="tdBaseAlt4Blue">&nbsp;</td>
							</tr>
						<%
						}
						cnt++;
						bltUserAccount myAdj = new bltUserAccount();
						if (isAdj)
						{
							myAdj = new bltUserAccount(new Integer(tempIPID));
						}
						String myTDClass="tdBase";
						if (cnt%2==0)
						{
//							myTDClass = "tdBaseAlt2";
						}
					%>
						<tr class="<%=myTDClass%>">
						<td ><%if (!isAdj){%><span class="tdHeader"><a href="report_auto_int_r2.jsp?PID=<%=tempIPID%>"><%=element%></a></span><%}else{%><span class="tdHeader"><%=element%></span><br><br>Phone: <strong><%=myAdj.getContactPhone()%></strong><br>Fax: <strong><%=myAdj.getContactFax()%></strong><br>
						  Email: <strong>
						  <% if (NIMUtils.isValidEmail(myAdj.getContactEmail())){%><a href="mailto:<%=myAdj.getContactEmail()%>"><%=myAdj.getContactEmail()%></a><%}else{%><%=myAdj.getContactEmail()%><%}%></strong><%}%></td>
						<td><%
						if (isAdj)
                        {
                            out.print(tempPayerName);
                        }
						else
                        {
                            out.print(tempDiv);
                        }
						%></td>
						<td <%=addClass_td%>>
                           <span style="color:#F33">MR: <%=tempTDay_MR%></span><br>
						  <span  style="color:#339">CT: <%=tempTDay_CT%></span><br>
						  <span  style="color:#939">EMG: <%=tempTDay_EMG%></span><br>
						  <span  style="color:#066">Other: <%=tempTDay_Oth%></span><hr>
						  <strong>Total: <%=tempTDay_MR + tempTDay_CT + tempTDay_EMG + tempTDay_Oth%></strong></td>
						<td>
                          <span style="color:#F33">MR: <%=tempATotal_MR2010+tempATotal_MR2011%></span><br>
						  <span  style="color:#339">CT: <%=tempATotal_CT2010+tempATotal_CT2011%></span><br>
						  <span  style="color:#939">EMG: <%=tempATotal_EMG2010+tempATotal_EMG2011%></span><br>
						  <span  style="color:#066">Other: <%=tempATotal_Oth2010+tempATotal_Oth2011%></span><hr>
						  <strong>Total: <%=tempATotal_MR2010 + tempATotal_CT2010 + tempATotal_EMG2010 + tempATotal_Oth2010 + tempATotal_MR2011 + tempATotal_CT2011 + tempATotal_EMG2011 + tempATotal_Oth2011%></strong></td>
							  <td class="tdBaseAlt4Blue">&nbsp;</td>
						<td>
                          <span style="color:#F33">MR: <%=tempATotal_MR2011%></span><br>
						  <span  style="color:#339">CT: <%=tempATotal_CT2011%></span><br>
						  <span  style="color:#939">EMG: <%=tempATotal_EMG2011%></span><br>
						  <span  style="color:#066">Other: <%=tempATotal_Oth2011%></span><hr>
						  <strong>Total: <%=tempATotal_MR2011 + tempATotal_CT2011 + tempATotal_EMG2011 + tempATotal_Oth2011%></strong></td>
						<td align="right">
                        <span style="color:#F33">MR:</span><br>
                        <span  style="color:#339">CT:</span><br>
                        <span  style="color:#939">EMG:</span><br>
                        <span  style="color:#066">Other:</span><hr><strong>Total:</strong></td>
						<%
                        for (int i=1;i<13; i++)
                        {
                            String addClass = "";
                            if (i==nowMonth&&(tempARMonth_MR2011[i] + tempARMonth_CT2011[i] + tempARMonth_EMG2011[i] + tempARMonth_Oth2011[i])>0)
                            {
                                addClass = "class = \"tdBaseAlt_Action1\"";
                            }
                            else if ((tempARMonth_MR2011[i] + tempARMonth_CT2011[i] + tempARMonth_EMG2011[i] + tempARMonth_Oth2011[i])>0)
                            {
                                addClass = "class = \"tdBaseAlt2\"";
                            }
                        %>
                            
                        <td <%=addClass%>>
                        <span  style="color:#F33"><%=tempARMonth_MR2011[i]%></span><br>
                        <span  style="color:#339"><%=tempARMonth_CT2011[i]%></span><br>
                        <span  style="color:#939"><%=tempARMonth_EMG2011[i]%></span><br>
                        <span  style="color:#066"><%=tempARMonth_Oth2011[i]%></span><hr>
		<strong><%=tempARMonth_MR2011[i] + tempARMonth_CT2011[i] + tempARMonth_EMG2011[i] + tempARMonth_Oth2011[i]%></strong></td>
						<%
                        }
                        %>
							  <td class="tdBaseAlt4Blue">&nbsp;</td>
<td align="right">
                          <span style="color:#F33">MR: <%=tempATotal_MR2010%></span><br>
						  <span  style="color:#339">CT: <%=tempATotal_CT2010%></span><br>
						  <span  style="color:#939">EMG: <%=tempATotal_EMG2010%></span><br>
						  <span  style="color:#066">Other: <%=tempATotal_Oth2010%></span><hr>
						  <strong>Total: <%=tempATotal_MR2010 + tempATotal_CT2010 + tempATotal_EMG2010 + tempATotal_Oth2010%></strong></td>
						<td align="right">
                        <span style="color:#F33">MR:</span><br>
                        <span  style="color:#339">CT:</span><br>
                        <span  style="color:#939">EMG:</span><br>
                        <span  style="color:#066">Other:</span><hr><strong>Total:</strong></td>
						<%
                        for (int i=1;i<13; i++)
                        {
                            String addClass = "";
							if ((tempARMonth_MR2010[i] + tempARMonth_CT2010[i] + tempARMonth_EMG2010[i] + tempARMonth_Oth2010[i])>0)
                            {
                                addClass = "class = \"tdBaseAlt2\"";
                            }
                        %>
                            
                        <td <%=addClass%>>
                        <span  style="color:#F33"><%=tempARMonth_MR2010[i]%></span><br>
                        <span  style="color:#339"><%=tempARMonth_CT2010[i]%></span><br>
                        <span  style="color:#939"><%=tempARMonth_EMG2010[i]%></span><br>
                        <span  style="color:#066"><%=tempARMonth_Oth2010[i]%></span><hr>
		<strong><%=tempARMonth_MR2010[i] + tempARMonth_CT2010[i] + tempARMonth_EMG2010[i] + tempARMonth_Oth2010[i]%></strong></td>
						<%
                        }
                        %>							  <td class="tdBaseAlt4Blue">&nbsp;</td>
      						</tr>
                    <%
					}
				 }
			}

			mySS.closeAll();
			
		} // end of the try block
		catch (Exception e55) 
		{
			out.println("<hr>Error Main: " + e55);
		}
	}
	else
	{
		out.print ("<h1>Interactive Reports are currently undergoing maintenance.</h1>");
	}
   }
   else
   {
		out.print ("no sec");
   }

   %>
</table>


</td>
  </tr>
</table>

</body>
</html>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
{
	%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>    
    <%
}

%>
