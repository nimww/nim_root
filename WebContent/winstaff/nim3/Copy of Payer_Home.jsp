<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">



</head>
<SCRIPT language="JavaScript1.2">
function openStatus()
{
	testwindow= window.open ("Home_Status.jsp", "HomeStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(0,0);
}

function openAuto(myAuto)
{
	testwindow= window.open ("report_auto2.jsp?rid=" + myAuto + "&MREF=300", "AutoStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(100,100);
}

</SCRIPT>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?iDelay=-1&plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler) 
    {
        accessValid = true;
    }
    else if (pageControllerHash.containsKey("iAdjusterID")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID        =    (Integer)pageControllerHash.get("iAdjusterID");
		isAdjuster = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID2")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster2 = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID3")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster3 = true;
        accessValid = true;
	}
    else if (isProvider&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
	}
	
	
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	
		if (isScheduler)
	  {
		  String theScheduler = CurrentUserAccount.getUserID().toString();
		  String myAT_Where = "";
		  if ( request.getParameter("theScheduler")!=null&&!request.getParameter("theScheduler").equalsIgnoreCase("") )
		  {
			  theScheduler = request.getParameter("theScheduler");
		  }
		  if (!theScheduler.equalsIgnoreCase("0"))
		  {
			  myAT_Where =  " AND tNIM3_CaseAccount.assignedtoid = " + theScheduler + " ";
		  }
		  
	  %>
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td ><p class="title">Welcome to NextImage Grid Version <%=ConfigurationInformation.applicationVersion%> (<%=ConfigurationInformation.serverName%>)</p></td>
      </tr>
      <tr>
        <td ><table width="100%" border="2" cellspacing="0" cellpadding="10">
         <%
		 if (isScheduler2)
		 {
			 %>
          <tr class="tdHeaderAlt">
            <td colspan="2" valign="top"><form action="#" method="POST" name="SchedulerSelect" class="tdHeader">
              Welcome Scheduler Admin (L2+). You are viewing the worklist for:
                  <select name="theScheduler" class="titleSub1" onChange="document.forms.SchedulerSelect.submit()">
                <option value="0">Show All</option>
                <%
{
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("select Userid,LogonUserName, contactfirstname, contactlastname from tUserAccount where accounttype ='SchedulerID' or accounttype='SchedulerID2' or accounttype='SchedulerID3' or accounttype='GenAdminID' order by LogonUserName");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
				if (myRS.getString("UserID").equalsIgnoreCase(theScheduler))
				{
					%>
                <option selected value="<%=myRS.getString("UserID")%>"><%=myRS.getString("LogonUserName")%> [<%=myRS.getString("contactfirstname")%>, <%=myRS.getString("contactlastname")%>]</option>
                <%
				}
				else
				{
					%>
                <option  value="<%=myRS.getString("UserID")%>"><%=myRS.getString("LogonUserName")%> [<%=myRS.getString("contactfirstname")%>, <%=myRS.getString("contactlastname")%>]</option>
                <%
				}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
%>
              </select>
            </form>
            
            <input name="Button" type="button" class="inputButton_md_Test" disabled id="Button" onClick="openStatus()" value="Open Status Window">&nbsp;&nbsp;&nbsp;<input name="Button2" type="button" class="inputButton_md_Test"  id="Button2" onClick="openAuto('volume1')" value="Open Daily Chart">
&nbsp;&nbsp;&nbsp;<input name="Button2" type="button" class="inputButton_md_Test"  id="Button2" onClick ="modalWin('Payer_Home_CaseLoad.jsp','QuickView...', 'dialogWidth:500px;dialogHeight:500px', 'status=yes,scrollbars=yes,resizable=yes,width=500,height=500');return false;" value="Case Load Analysis">         
            
            
            </td>
            </tr>
           <%}%> 
          <tr>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr class="tdHeaderAlt">
                <td class="tdHeaderAltDark">Case Load</td>
                </tr>
              <tr >
                <td class="tdBase"><table border="1" cellspacing="0" cellpadding="1">
                  <tr class="tdHeaderAlt">
                    <td colspan="9" class="tdHeaderAltDark">Needs to be Scheduled</td>
                  </tr>
                  <tr class="tdHeaderAlt">
                    <td>Scheduler</td>
                    <td>Total Active</td>
                    <td>Not Scheduled</td>
                    <td>Not Scheduled >2days</td>
                    <td>Past Appt</td>
                  </tr>
                  <%
if (false)
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getCaseLoad(); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		NIM_CaseLoadObject myCaseLoadObject = (NIM_CaseLoadObject) myWorklist_Vector.elementAt(i);
		%>
                  <tr >
                    <td align="center"><%=myCaseLoadObject.getName()%></td>
                    <td align="center"><%=myCaseLoadObject.getTotalActive()%></td>
                    <td align="center"><%=myCaseLoadObject.getTotalNotScheduled()%></td>
                    <td align="center"><%=myCaseLoadObject.getTotalPastDue()%></td>
                    <td align="center"><%=myCaseLoadObject.getTotalAfterAppt()%></td>
                  </tr>
                  <%
	}

}
				%>
                </table></td>
                </tr>
              </table></td>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr class="tdHeaderAlt">
                <td colspan="3" class="tdHeaderAltDark">Next Action Alerts - Due</td>
                </tr>
              <tr class="tdHeaderAlt">
                <td>Patient</td>
                <td>ScanPass</td>
                <td>Action Due</td>
                </tr>
              <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_NAD,myAT_Where); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"load");

		String theClass2 = "tdBase";
		if (myEO2.getNIM3_Encounter().getNextActionDate().before(PLCUtils.getYesterday()))
		{
			theClass2 = "tdBaseAlt_Action3";
		}
		else if (myEO2.getNIM3_Encounter().getNextActionDate().before(PLCUtils.getToday()))
		{
			theClass2 = "tdBaseAlt_Action2";
		}
			
			%>
              <tr >
                <td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&SelectedEncounterID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p"><%=myEO2.getNIM3_CaseAccount().getPatientLastName()%>, <%=myEO2.getNIM3_CaseAccount().getPatientFirstName()%></a></td>
                <td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&SelectedEncounterID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p"><%=myEO2.getNIM3_Encounter().getScanPass()%></a></td>
                <td  class=<%=theClass2%>><%=PLCUtils.getDisplayDateWithTime(myEO2.getNIM3_Encounter().getNextActionDate())%></td>
                </tr>
              <%
	}
}
				%>
              </table></td>
            </tr>
          <tr>
            <td valign="top"><table width="100%" border="1" cellspacing="0" cellpadding="1">
              <tr class="tdHeaderAlt">
                <td colspan="11" class="tdHeaderAltDark">Needs to be Scheduled</td>
                </tr>
              <tr class="tdHeaderAlt">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Payer</td>
                <td>Scheduler</td>
                <td>Patient</td>
                <td>State</td>
                <td>ScanPass</td>
                <td>Status</td>
                <td>Hours Since Rec</td>
                </tr>
              <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_NOT_SCHEDULED,myAT_Where); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"load");
		long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myEO2.getNIM3_Referral().getReceiveDate().getTime())/(60*60*1000);
		String theClass2 = "tdBase";
		if (iHoursPassed<2)
		{
			theClass2 = "tdBaseFade1";
		}
		else if (iHoursPassed<4)
		{
			theClass2 = "tdBase";
		}
		else if (iHoursPassed<24)
		{
			theClass2 = "tdBaseAlt_Action1";
		}
		else if (iHoursPassed<48)
		{
			theClass2 = "tdBaseAlt_Action2";
		}
		else
		{
			theClass2 = "tdBaseAlt_Action3";
		}
		String NADClass = "tdBase";
		String NADImg = "images/action.gif";
		if (myEO2.getNIM3_Encounter().getNextActionDate().before(PLCUtils.getToday()))
		{
			//NADClass = "tdBaseAlt_Action3";
			NADImg = "images/no-action.gif";
		}
		String NADNotes = "Due: " + PLCUtils.getDisplayDateWithTime(myEO2.getNIM3_Encounter().getNextActionDate());
		if (myEO2.getNIM3_Encounter().getNextActionDate().before(NIMUtils.getBeforeTime()))
		{
			NADNotes = "No Action Date Set";
		}
		else
		{
			NADNotes += "\\nAction:\\n" + DataControlUtils.Text2OneLine(myEO2.getNIM3_Encounter().getNextActionNotes());
		}
		
		String tempStatus = "";
//		boolean tempisReadySchedule = NIMUtils.isEncounterReadyToSchedule(iEncounterID,true, true); 
		boolean tempisReadySchedule = NIMUtils.isEncounterReadyToSchedule(myEO2,true, true); 
		if (tempisReadySchedule&&myEO2.getNIM3_Encounter().getSeeNetDev_Waiting()==1)
		{
			tempStatus = "Ready to Schedule - Waiting on NetDev";
		}
		else if (tempisReadySchedule)
		{
			tempStatus = "Ready to Schedule";
		}
		else
		{
			tempStatus = "Incomplete Data";
			if (myEO2.getNIM3_CaseAccount().getPatientCellPhone().equalsIgnoreCase("")&&myEO2.getNIM3_CaseAccount().getPatientHomePhone().equalsIgnoreCase(""))
			{
				tempStatus += " | Missing Patient Phone #";
			}
			if (myEO2.getNIM3_Referral().getRxFileID()==0)
			{
				tempStatus += " | No RX";
			}
			else if (myEO2.getNIM3_Referral().getRxFileID()==NIMUtils.EncounterBypassRXID)
			{
				tempStatus += " | Using Bypass RX";
			}
			if (!myEO2.isPreScreenComplete())
			{
				tempStatus += " | PreScreen Incomplete";
			}
			if (!myEO2.isRxReviewed())
			{
				tempStatus += " | No Rx Review";
			}
			if (!myEO2.isScanPassAlertsComplete(1))
			{
				tempStatus += " | Receipt Alerts not complete";
			}


		}		
		
		
		%>
		  <tr >
                <td align="center" class="<%=NADClass%>"><img src="<%=NADImg%>" onClick="alert('<%=NADNotes%>');"></td>
            <td align="center"><% if (myEO2.getNIM3_Encounter().getisSTAT()==1){out.println("<span class=\"tdBaseAltRed\">STAT</span>");}else{%>&nbsp;<%}%></td>
            <td align="center"><% if (myEO2.getNIM3_Encounter().getSeeNetDev_Waiting()==1){out.println("<span class=\"tdBaseAltRed\">NetDev</span>");}else{%>&nbsp;<%}%></td>
            <td align="center" width="25"><a href="#" onClick ="modalWin('tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=quickview&EDITID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&SelectedEncounterID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','QuickView...','dialogWidth:1000px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1000,height=800');return false;"><img src="images/zoom_plus.png" width="25" height="25" border="0"></a>
            

</td>
            <td><%=myEO2.getNIM3_PayerMaster().getPayerName()%></td>
            <td><%=myEO2.getCase_AssignedTo().getContactFirstName()%></td>
			<td nowrap><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&SelectedEncounterID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p"><%=myEO2.getNIM3_CaseAccount().getPatientLastName()%>, <%=myEO2.getNIM3_CaseAccount().getPatientFirstName()%></a></td>
			<td><%=myEO2.getPatientAddressObject().getState()%></td>
			<td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&SelectedEncounterID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p"><%=myEO2.getNIM3_Encounter().getScanPass()%></a></td>
			<td ><%=tempStatus%></td>
			<td class=<%=theClass2%>><% if (iHoursPassed>10000){out.print("Invalid Receive Date");}else{out.print(iHoursPassed + " hours");}%><%if (isScheduler4){out.print("<br>Working Days: " + com.winstaff.workingDays.getWorkingDays(myEO2.getNIM3_Referral().getReceiveDate(),PLCUtils.getNowDate(false)));}%></td>

			</tr>
		  <%
	}

}
				%>
              </table></td>
            <td valign="top"><table width="100%" border="1" cellspacing="0" cellpadding="2">
              <tr class="tdHeaderAlt">
                <td colspan="9" class="tdHeaderAltDark">Appointment Follow-up &amp; Report Delivery</td>
                </tr>
              <tr class="tdHeaderAlt">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Scheduler</td>
                <td>Patient</td>
                <td>State</td>
                <td>ScanPass</td>
                <td>Status</td>
                <td>Hours</td>
                </tr>
              <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_REPORTS_DUE,myAT_Where); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"load");
		long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myEO2.getNIM3_Encounter().getDateOfService().getTime())/(60*60*1000);
		int iWorkingHoursPassed = workingDays.getWorkingHours (myEO2.getNIM3_Encounter().getDateOfService(),PLCUtils.getNowDate(false));
		long iHoursPassedRpReview = (PLCUtils.getNowDate(false).getTime()-myEO2.getNIM3_Encounter().getTimeTrack_ReqDelivered().getTime())/(60*60*1000);
		String sHoursPassed = iHoursPassed + " hours since DOS<br><span class=tdBaseFade2>" + iWorkingHoursPassed + " working hours</span>";
		if (iHoursPassed<=0)
		{
			sHoursPassed = "Appt in "+-iHoursPassed+" hours";
		}
		boolean isShow = true;
		if ( (!myEO2.isScanPassAlertsComplete(2)||myEO2.getNIM3_Encounter().getSentTo_SP_IC().before(NIMUtils.getBeforeTime()))&& (myEO2.getNIM3_Encounter().getReportFileID().intValue()>0)&&myEO2.isScanPassAlertsComplete(3) )
		{
			isShow = false;
		}



        java.util.Calendar cal = java.util.Calendar.getInstance();
        //Clear all fields
        cal.clear();
        cal.set(java.util.Calendar.YEAR, 2012);
        cal.set(java.util.Calendar.MONTH, 2);
        cal.set(java.util.Calendar.DATE, 8);
        java.util.Date faxReminderStartDate = cal.getTime();


		String sStatus = "";
		if (iHoursPassed<0&&myEO2.getNIM3_Encounter().getReportFileID().intValue()==0)
		{
			sStatus = "ScanPass not sent";
			isShow = false;
		}
		else if (iHoursPassed<0&&myEO2.getNIM3_Encounter().getReportFileID().intValue()>0)
		{
			sStatus = "Warning: Report before appointment";
		}
		else if (myEO2.getNIM3_Encounter().getReportFollowUpCounter()>3&&myEO2.getNIM3_Encounter().getReportFileID().intValue()==0)
		{
			//4 have been sent so we show
			sStatus = "No Report on file; "+ myEO2.getNIM3_Encounter().getReportFollowUpCounter() +" fax reminders sent";
		}
		else if (iHoursPassed>0&&myEO2.getNIM3_Encounter().getReportFileID().intValue()==0&&myEO2.getNIM3_Encounter().getDateOfService().before(faxReminderStartDate))
		{
			sStatus = "No Report on file;*"; // for cases before the reminder start date
		}
		else if (iWorkingHoursPassed>7	&&myEO2.getNIM3_Encounter().getReportFollowUpCounter()<1&&myEO2.getNIM3_Encounter().getReportFileID().intValue()==0)
		{
			sStatus = "No Report on file;**";//this will indicate an error in the fax reminder program as it's been 7 working hours and no fax reminders have been sent out.  
		}
		else if (iWorkingHoursPassed<20&&myEO2.getNIM3_Encounter().getReportFollowUpCounter()<4&&myEO2.getNIM3_Encounter().getReportFileID().intValue()==0)
		{
			//No report on file, but less than 4 reminders have been sent, so we are hiding this from the worklist
			isShow=false;
		}
		else if (iWorkingHoursPassed>=20&&myEO2.getNIM3_Encounter().getReportFileID().intValue()==0)
		{
			sStatus = "No Report on file;***";//this will indicate an error in the fax reminder program as it's been  20 working hours all 4 fax reminders have not been sent out.  
		}
		else if (myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview().before(NIMUtils.getBeforeTime()))
		{
			sStatus = "Pending Report Review";
			sHoursPassed += "<br><strong>" + iHoursPassedRpReview + " since report u/l</strong>";
		}
		else if (false&&(myEO2.isScanPassAlertsComplete(2)&&!myEO2.getNIM3_Encounter().getSentTo_SP_IC().before(NIMUtils.getBeforeTime())&&myEO2.getNIM3_Referral().getReferralStatusID()==1))
		{
			sStatus = "*";
			isShow=false;
		}
		else  if (!myEO2.isScanPassAlertsComplete(3)&&myEO2.getNIM3_Encounter().getReportFileID().intValue()>0&&myEO2.getNIM3_Encounter().getEncounterStatusID()!=9)
		{
			sStatus = "Report on file, Confirmations <u>not</u> sent";
			sHoursPassed += "<br><strong>" + iHoursPassedRpReview + " since report u/l</strong>";
		}
		else  if (myEO2.getNIM3_Encounter().getEncounterStatusID()==9)
		{
			sStatus = "*";
			isShow=false;
		}
		else  if (myEO2.getNIM3_Referral().getReferralStatusID()!=1)
		{
			sStatus = "*";
		}
		else  
		{
			sStatus = "* [See Andy]";
		}
		
		if ( !myEO2.isScanPassAlertsComplete(2)&&myEO2.getNIM3_Encounter().getReportFileID().intValue()==0 )
		{
			isShow=true;
			sStatus += " | Warning: SP Not Sent" ;
		}
		
		if (myEO2.getNIM3_Encounter().getReportFileID().intValue()==0&&myEO2.getNIM3_Encounter().getSentTo_SP_IC().before(NIMUtils.getBeforeTime()))
		{
			isShow=true;
			sStatus += " | Warning: SP Not Sent to Imaging Center" ;
		}
		if (myEO2.getNIM3_Referral().getReferralStatusID()!=1)
		{
			sStatus += " | Warning: Referral is Pending Authorization";
		}
		

		if (isShow)
		{
			String theClass2 = "tdBase";
			if (iHoursPassed<0)
			{
				theClass2 = "tdBaseAlt_Action3";
			}
			else if (false&&iHoursPassed<4&&myEO2.getNIM3_Encounter().getReportFileID().intValue()==0)
			{
				theClass2 = "tdBaseFade1";
			}
			else if (iHoursPassed<8&&myEO2.getNIM3_Encounter().getReportFileID().intValue()==0)
			{
				theClass2 = "tdBase";
			}
			else if (iHoursPassed<12&&myEO2.getNIM3_Encounter().getReportFileID().intValue()==0)
			{
				theClass2 = "tdBaseAlt_Action1";
			}
			else if (iHoursPassed<24)
			{
				theClass2 = "tdBaseAlt_Action2";
			}
			else
			{
				theClass2 = "tdBaseAlt_Action3";
			}
			String NADNotes = "Due: " + PLCUtils.getDisplayDateWithTime(myEO2.getNIM3_Encounter().getNextActionDate());
			if (myEO2.getNIM3_Encounter().getNextActionDate().before(NIMUtils.getBeforeTime()))
			{
				NADNotes = "No Action Date Set";
			}
			else
			{
				NADNotes += "\\nAction:\\n" + DataControlUtils.Text2OneLine(myEO2.getNIM3_Encounter().getNextActionNotes());
			}
			
			String NADClass = "tdBase";
			String NADImg = "images/action.gif";
			if (myEO2.getNIM3_Encounter().getNextActionDate().before(PLCUtils.getToday()))
			{
				//NADClass = "tdBaseAlt_Action3";
				NADImg = "images/no-action.gif";
			}
				
				%>
				  <tr >
					<td align="center" class="<%=NADClass%>"><img src="<%=NADImg%>" onClick="alert('<%=NADNotes%>');"></td>
					<td align="center"><% if (myEO2.getNIM3_Encounter().getisSTAT()==1){out.println("<span class=\"tdBaseAltRed\">STAT</span>");}%></td>
					<td>
                    
                    
                    <a href="#" onClick ="modalWin('tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=quickview&EDITID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&SelectedEncounterID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','QuickView...','dialogWidth:1000px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1000,height=800');return false;"><img src="images/zoom_plus.png" width="25" height="25" border="0"></a>
                    
                    
                    </td>
		            <td><%=myEO2.getCase_AssignedTo().getContactFirstName()%></td>
					<td nowrap><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&SelectedEncounterID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p"><%=myEO2.getNIM3_CaseAccount().getPatientLastName()%>, <%=myEO2.getNIM3_CaseAccount().getPatientFirstName()%></a></td>
					<td><%=myEO2.getPatientAddressObject().getState()%></td>
					<td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&SelectedEncounterID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p"><%=myEO2.getNIM3_Encounter().getScanPass()%></a></td>
					<td class=<%=theClass2%>><%=sStatus%></td>
					<td class=<%=theClass2%>><%=sHoursPassed%></td>
					</tr>
				  <%
			}
		}
}
				%>
              </table></td>
            </tr>
          <tr>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr class="tdHeaderAlt">
                <td colspan="7" class="tdHeaderAltDark">Awaiting Rx Review (Not Processed By You)</td>
              </tr>
              <tr class="tdHeaderAlt">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Patient</td>
                <td>State</td>
                <td>ScanPass</td>
                <td>Status</td>
                <td>Hours Since Rec</td>
              </tr>
              <%
{				
//	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_RX_REVIEW," AND tNIM3_Encounter.TimeTrack_ReqProc_UserID<> " + theScheduler + " " ); 
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_RX_REVIEW,"" ); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"load");
		if (NIMUtils.isEncounterReadyToSchedule(myEO2,false, false))
		{
			long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myEO2.getNIM3_Referral().getReceiveDate().getTime())/(60*60*1000);
			String theClass2 = "tdBase";
			if (iHoursPassed<2)
			{
				theClass2 = "tdBaseFade1";
			}
			else if (iHoursPassed<4)
			{
				theClass2 = "tdBase";
			}
			else if (iHoursPassed<24)
			{
				theClass2 = "tdBaseAlt_Action1";
			}
			else if (iHoursPassed<48)
			{
				theClass2 = "tdBaseAlt_Action2";
			}
			else
			{
				theClass2 = "tdBaseAlt_Action3";
			}
			
			
			%>
			  <tr >
                <td>&nbsp;</td>
	            <td align="center"><% if (myEO2.getNIM3_Encounter().getisSTAT()==1){out.println("<span class=\"tdBaseAltRed\">STAT</span>");}%></td>
				<td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&SelectedEncounterID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p"><%=myEO2.getNIM3_CaseAccount().getPatientLastName()%>, <%=myEO2.getNIM3_CaseAccount().getPatientFirstName()%></a></td>
				<td><%=myEO2.getPatientAddressObject().getState()%></td>
				<td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&SelectedEncounterID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p"><%=myEO2.getNIM3_Encounter().getScanPass()%></a></td>
				<td >Ready for Review</td>
				<td class=<%=theClass2%>><%=iHoursPassed%> hours</td>
			  </tr>
			  <%
		}
		else
		{
			//skip only show those that are ready to review.
		}
	}
}
				%>
            </table></td>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr class="tdHeaderAlt">
                <td colspan=2 class="tdHeaderAltDark">Open CommTracks
                  <input name="button2" type="button" onClick="document.location='tNIM3_CommTrack_query_all.jsp';" class="inputButton_md_Action1" id="button2" value="View CommTrack" /></td>
              </tr>
              <tr class="tdHeaderAlt">
                <td nowrap>Name</td>
                <td nowrap >Hours Since Rec</td>
              </tr>
              <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_COMMTRACK,myAT_Where); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iCommTrackID = (Integer) myWorklist_Vector.elementAt(i);
		bltNIM3_CommTrack myCT = new bltNIM3_CommTrack(iCommTrackID);
    	int id = myCT.getIntUserID().intValue();
    	
		long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myCT.getCommStart().getTime())/(60*60*1000);
		String theClass2 = "tdBase";
		if (iHoursPassed<1)
		{
			theClass2 = "tdBaseFade1";
		}
		else if (iHoursPassed<3)
		{
			theClass2 = "tdBase";
		}
		else if (iHoursPassed<6)
		{
			theClass2 = "tdBaseAlt_Action1";
		}
		else if (iHoursPassed<12)
		{
			theClass2 = "tdBaseAlt_Action2";
		}
		else
		{
			theClass2 = "tdBaseAlt_Action3";
		}
		
		if (id == 0) %> 
		
			<!-- <font color="FF0AA0"> -->
			
		<% else %>
		  <tr >
			<td><%=myCT.getMessageName()%> @ <%=myCT.getMessageCompany()%></td>
			<td  class=<%=theClass2%>><%=iHoursPassed%> hours</td>
		  </tr>
		  <%
	}
}
				%>
            </table></td>
            </tr>
          </table></td>
      </tr>
      <%
	  if (isScheduler2)
	  {
	  %>
      <%
	  }
	  if (isScheduler3)
	  {
	  %>
      <%
	  }
	  else
	  {
		  %>
          <%
	  }
	  %>
    </table></td>
  </tr>
</table>
      <%
	  }
	  else if (isAdjuster||isAdjuster2||isAdjuster3||isProvider)
	  {
		  %>
<table width="960" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td>
    	<table  border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class=title>Welcome to NextImage Medical.</td>
          </tr>

          <%
		  if (isAdjuster3)
		  {
			  bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
			  %>
          <tr>
            <td colspan="2" align="left" class="tdHeader">You are signed in as a Admin for: <a href="Payer_Home_branch.jsp"><%=myPM.getPayerName()%></a></td>
          </tr>
          <%
		  }			  
		  %>


          <%if (isAdjuster||isProvider)
		  {
			  %>
        <tr>
          <td colspan="2" align="left" class="tdHeader"><input name="button7" type="button" onClick="document.location='Payer_submit1.jsp';" class="inputButton_lg_Default" id="button7" value="Submit New Referral" /></td>
        </tr>
          <%
		  }			  
		  %>

          <tr>
          <td colspan="2" align="left" class="tdHeader">10 Most Recent Cases</td>
          </tr>
          <td colspan="2" align="center" class="borderHighlightBlackSolid"><table width="100%" border="0" cellspacing="0" cellpadding="4">
              <tr class="tdHeaderAlt">
                <td>Action</td>
                <td>Claim #</td>
                <td>First</td>
                <td>Last</td>
                <td>Submitted</td>
                <td>Type</td>
              </tr>
              
              
  <!-- test -->
  <%
searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
	String mySQL = "null";
	String mySQL2 = "null";
	if (isAdjuster3)
	{
		mySQL = "select tNIM3_Encounter.encounterid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where tNIM3_CaseAccount.PayerID =" + iPayerID + " and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by ReceiveDate desc LIMIT 10";
	}
	else if (isAdjuster2)
	{
		mySQL = "select tNIM3_Encounter.encounterid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where ( (tNIM3_CaseAccount.adjusterid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.adjusterid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) ) OR (tNIM3_CaseAccount.nursecasemanagerid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.nursecasemanagerid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) )    OR (tNIM3_CaseAccount.caseadministratorid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministratorid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) ) OR (tNIM3_CaseAccount.caseadministrator2id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator2id in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) ) OR (tNIM3_CaseAccount.caseadministrator3id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator3id in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) )  OR (tNIM3_CaseAccount.caseadministrator4id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator4id in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) )																																																																																																																																																																																																																																															 ) and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by ReceiveDate desc LIMIT 10";

//		mySQL = "select tNIM3_Encounter.encounterid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where tNIM3_CaseAccount.PayerID =" + iPayerID + " and (tNIM3_CaseAccount.adjusterid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.adjusterid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) )  and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by ReceiveDate desc LIMIT 10";
	}
	else if (isAdjuster)
	{
		mySQL = "select tNIM3_Encounter.encounterid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where (tNIM3_CaseAccount.adjusterid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.nursecasemanagerid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministratorid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator2id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator3id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator4id = " + CurrentUserAccount.getUserID() + ") and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by ReceiveDate desc LIMIT 10";
	}
	else if (isProvider3)
	{
		mySQL = "select tNIM3_Encounter.encounterid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where tNIM3_Referral.referringphysicianid in (select userid from tUserAccount where payerid = " + iPayerID + ") and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by ReceiveDate desc LIMIT 10";
	}
	else if (isProvider2)
	{
		mySQL = "select tNIM3_Encounter.encounterid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where (tNIM3_Referral.referringphysicianID = " + CurrentUserAccount.getUserID() + " OR tNIM3_Referral.referringphysicianID in (select userid from tUserAccount where managerid=" + CurrentUserAccount.getUserID() + " and managerid>0 ) )  and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by ReceiveDate desc LIMIT 10";
	}
	else if (isProvider)
	{
		mySQL = "select tNIM3_Encounter.encounterid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where (tNIM3_Referral.referringphysicianid = " + CurrentUserAccount.getUserID() +") and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by ReceiveDate desc LIMIT 10";
	}
//out.println(mySQL);
	myRS = mySS.executeStatement(mySQL);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
	   
	   NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(new Integer(myRS.getString("EncounterID")), "loading");
	cnt++;
if (cnt<=10)
{
	cnt2++;
	
	String myClass = "tdBase";
	if (myEO2.getNIM3_CaseAccount().getCaseClaimNumber().equalsIgnoreCase(""))
	{
		myClass = "requiredFieldMain";
	}
	else if (cnt2%2!=0)
	{
		myClass = "tdBaseAlt";
	}
	
	out.print("<tr class="+myClass+">");

	if (isAdjuster||isAdjuster2||isAdjuster3||isProvider)
	{
	out.print("<td align=center  ><input class=\"inputButton_md_Default\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=viewpayer&EDITID=" + myEO2.getNIM3_CaseAccount().getCaseID() + "&KM=p'\" value=\"View Details\"></td>");
	}
	out.print("<td>");
	{
		out.print(myEO2.getNIM3_CaseAccount().getCaseClaimNumber() );
	}
	out.print("</td>");
	out.print("<td>");
	out.print(myEO2.getNIM3_CaseAccount().getPatientFirstName());
	out.print("</td>");
	out.print("<td>");
	out.print("<strong>" +myEO2.getNIM3_CaseAccount().getPatientLastName() + "</strong>");
	out.print("</td>");
	out.print("<td>");
	out.print(PLCUtils.getDisplayDate(myEO2.getNIM3_Referral().getReceiveDate(),false));
	out.print("</td>");
	out.print("<td>");
	out.print(myEO2.getEncounterType_Display());
	out.print("</td>");
	out.print("</tr>");
}
   }
mySS.closeAll();
endCount = cnt;

}
catch(Exception eee)
{
	out.print("item list error: " + eee);
}

%>
              
              
              
  <!-- test -->            
              
              
          </table></td>
          </tr>
	    </table>
    </td>
  </tr>
</table>
          
          <%
	  }
	  if (isAdjuster2&&!isAdjuster3)
	  {
		  %>
<table  border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td>
    	<table width="100%" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td class=title>You are signed in as a Supervisor of the following users:</td>
          </tr>
        <tr>
          <td valign="top" class="borderHighlightBlackSolid"><table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr class="tdHeaderAlt">
              <td>Login</td>
              <td>First Name</td>
              <td>Last Name</td>
              </tr>
            
            
            <!-- test -->
            <%
searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
	String mySQL = "null";
	String mySQL2 = "null";
	if (isAdjuster2)
	{
		mySQL = "select logonusername, contactfirstname, contactlastname from tUserAccount where payerid =" + iPayerID + " and managerid = " + CurrentUserAccount.getUserID() + " order by contactlastname";
	}
//out.println(mySQL);
	myRS = mySS.executeStatement(mySQL);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {

	cnt++;
	//if (cnt>=startID&&cnt<=startID+maxResults)
if (cnt<=10)
{
	cnt2++;
	
	String myClass = "tdBase";
	if (cnt2%2==0)
	{
		myClass = "tdBaseAlt";
	}
	
	out.print("<tr class="+myClass+">");

	out.print("<td>");
	out.print(myRS.getString("LogonUserName"));
	out.print("</td>");
	out.print("<td>");
	out.print(myRS.getString("ContactFirstName"));
	out.print("</td>");
	out.print("<td>");
	out.print(myRS.getString("ContactLastName"));
	out.print("</td>");
	out.print("</tr>");
}
   }
mySS.closeAll();
endCount = cnt;

}
catch(Exception eee)
{
	out.print("item list error: " + eee);
}

%>
            
            
            
            <!-- test -->            
            
            
          </table></td>
        </tr>
        </table>
    </td>
  </tr>
</table>

        
          
          <%
	  }



}
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
        
        
</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>