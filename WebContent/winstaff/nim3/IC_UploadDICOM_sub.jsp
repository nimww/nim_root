<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*,org.apache.commons.fileupload.*,org.apache.commons.fileupload.disk.*, org.apache.commons.fileupload.servlet.* " %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_icid.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
    <table cellpadding=0 cellspacing=0 border=0  >
    <tr><td width=10>&nbsp;</td><td>
  <%
	//initial declaration of list class and parentID
    boolean accessValid = false;
    Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
    String ENCID = null;
	NIM3_EncounterObject2 myEO2 = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isIC) 
    {
		accessValid = true;
    }
  //page security
  if (accessValid)
  {
	  try
	  {

		String myFileSS = "";
		boolean isPassed=false;

		// Create a factory for disk-based file items
		String myRPath = ConfigurationInformation.sDICOMFolderDirectory;
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new java.io.File(myRPath + "/temp/"));
		
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		
		// Set overall request size constraint
//		upload.setSizeThreshold(2000000);
//		upload.setSizeMax(2000000);
		// Parse the request
		java.util.List /* FileItem */ items = upload.parseRequest(request);
		java.util.Iterator iter = items.iterator();
		java.text.SimpleDateFormat fileDF = new java.text.SimpleDateFormat("yyyy-MM-dd" );
		if (iter.hasNext()) 
		{
			FileItem item = (FileItem) iter.next();
			FileItem item_edit = (FileItem) iter.next();
			ENCID = item_edit.getString();
			boolean isPassedAccess = false;
			if (NIMUtils.isICIDAuthorizedToUploadReport(CurrentUserAccount.getReferenceID(),new Integer(ENCID),"D"))
			{
				myEO2 = new NIM3_EncounterObject2(new Integer(ENCID),"Upload DICOM");
				isPassedAccess = true;
			}
			if (isPassedAccess)
			{
				String sFileDF = "DCM_" + myEO2.getNIM3_Encounter().getScanPass() + "_" + com.winstaff.password.RandomString.generateString(6).toLowerCase() + "_"+ fileDF.format(PLCUtils.getNowDate(false));
				String oFileName = item.getName();
				oFileName = oFileName.substring(oFileName.lastIndexOf("\\")+1,oFileName.length());
				oFileName = oFileName.replace(' ' , '_');				
				myFileSS= sFileDF + "_" + oFileName;
				String nFileName = myRPath + myFileSS;
				java.io.File uploadedFile = new java.io.File(nFileName);
				if (item.getName().indexOf(".zip")<=0)
				{
					isPassed = false;
					%>
					<script language="javascript">
					alert("Your file does not appear to be a ZIP file.  Files must be uploaded in ZIP format.");
					document.location = 'IC_Home.jsp';
					</script>	
					<%	
				}
				else if (item.getSize()==0)
				{
					isPassed = false;
					%>
					<script language="javascript">
					alert("Your file appears to have a size of ZERO or is locked/open by another user.\n\nYour document has NOT been uploaded successfully.");
					document.location = 'IC_Home.jsp';
					</script>	
					<%	
				}
				else
				{
					item.write(uploadedFile);
					isPassed = true;
				}
			}
			else
			{
				out.println("Error:[host:"+REMOTE_USER_host+"][ip:"+REMOTE_USER_ip+"]:WEB:IC_UploadReport_sub.jsp:Error[ICID="+CurrentUserAccount.getReferenceID()+"|EncID="+ENCID+"]");
			}
		}
		if (isPassed)
		{
			if (true)
			{
				String UserLogonDescription = "n/a";
				if (pageControllerHash.containsKey("UserLogonDescription"))
				{
					UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
				}
				//always send email since this is an IC
				if (true)
				{
					bltNIM3_Document myDoc = new bltNIM3_Document();
					myDoc.setFileName(myFileSS);
					myDoc.setComments("DICOM[IC]");
					myDoc.setReferralID(myEO2.getNIM3_Referral().getReferralID());
					myDoc.setEncounterID(myEO2.getNIM3_Encounter().getEncounterID());
					myDoc.setUploadUserID(CurrentUserAccount.getUserID());
					myDoc.setCaseID(myEO2.getNIM3_CaseAccount().getCaseID());
					myDoc.commitData();

					bltPracticeMaster myPM = new bltPracticeMaster(myEO2.getNIM3_Appointment().getProviderID());
					bltNIM3_CommTrack	working_bltNIM3_CommTrack = new bltNIM3_CommTrack();
					working_bltNIM3_CommTrack.setEncounterID(myEO2.getNIM3_Encounter().getEncounterID());
					working_bltNIM3_CommTrack.setReferralID(myEO2.getNIM3_Referral().getReferralID());
					working_bltNIM3_CommTrack.setCaseID(myEO2.getNIM3_CaseAccount().getCaseID());
					working_bltNIM3_CommTrack.setMessageName(CurrentUserAccount.getLogonUserName());
					working_bltNIM3_CommTrack.setMessageCompany(myPM.getPracticeName());
					working_bltNIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
					working_bltNIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
					working_bltNIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
					working_bltNIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
					working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
					
					//working_bltNIM3_CommTrack.setUniqueModifyComments(""+UserLogonDescription);
					String theMessage = "DICOM Uploaded by Imaging Center ["+CurrentUserAccount.getLogonUserName()+" @ " + myPM.getPracticeName() + "] to one of your cases.";
					working_bltNIM3_CommTrack.setMessageText(theMessage);
					working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
					working_bltNIM3_CommTrack.setUniqueModifyComments(UserLogonDescription);
					working_bltNIM3_CommTrack.commitData();
					emailType_V3 myEM = new emailType_V3();
					bltUserAccount myAT = new bltUserAccount(myEO2.getNIM3_CaseAccount().getAssignedToID());
//					myEM.setTo(myAT.getContactEmail());
					myEM.setFrom("server@nextimagemedical.com");
					myEM.setSubject("New DICOM Uploaded by IC for SP: " + myEO2.getNIM3_Encounter().getScanPass() + "[" + myEO2.getNIM3_CaseAccount().getPatientLastName() + "]");
					myEM.setBody(theMessage);
					myEM.setTo("emily.snyder@nextimagemedical.com");
					myEM.isSendMail();
					myEM.setTo("andy@nextimagemedical.com");
					myEM.isSendMail();
					myEM.setTo("scott@nextimagemedical.com");
					myEM.isSendMail();
					myEM.setTo("robert.bilow@nextimagemedical.com");
					myEM.isSendMail();
					//displayNotes = "Email sent to NetDev";
					%>
					
				</td></tr>
				<tr>
				  <td>&nbsp;</td>
				  <td class="title">Upload Successful</td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>Thank you for uploading the DICOM. We will process it and contact you if there are any issues.</td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
	  </tr>
				<tr>
				  <td>&nbsp;</td>
				  <td><a href="IC_Home.jsp">Return to Home Page</a></td>
	  </tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
	  </tr>
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
	  </tr>
				</table>
					
					
					
					<%
				}
			
			}
		
		}





	}
	catch (Exception eeee)
	{
		DebugLogger.println("Error:[host:"+REMOTE_USER_host+"][ip:"+REMOTE_USER_ip+"]:WEB:IC_UploadDICOM_sub.jsp:Error[ICID="+CurrentUserAccount.getReferenceID()+"|EncID="+ENCID+"]");
		%>
	<script language="javascript">
		alert("There was an error uploading your document\n\nThe file size likely exceeds the max size.\n\n<%=eeee%>");
//				document.location='tNIM3_Referral_main_NIM3_Referral_CaseID_4Payer.jsp';
		document.location = 'IC_Home.jsp';
		</script>		
	<%

	}








  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>