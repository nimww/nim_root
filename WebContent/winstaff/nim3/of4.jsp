<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>NextImage Secure Server</title>
<link href="www-images/style.css" rel="stylesheet" type="text/css" />
<link href="images/sign_in4/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#000000"  onLoad="document.getElementById('LogonUserName').focus();">
<center>
<div class="htmlBox">
    <a href="http://www.nextimagemedical.com">
        <div class="logoBox"> 
            <img src="images/sign_in4/logo1.png" width="238" height="62" border="0"><br>
        </div>
    </a>
	<div class="contentBox" style="text-align:center;">
		<span class="xbig" >Welcome to the NextImage Online Referral   </span>
		<br>
        Information submitted on this form is protected through SSL Security
	</div>
	<div class="contentBox">
    	If you already have a Username and Password to our Secure Portal, then <a href="signIn4.jsp">click here to sign in</a>.
        <br>
        <br>
        <span class="xbig" >Submit a Referral</span>
        
		<div class="contentBox_Form" >
          <p class=titleSub1>If your existing form already contains this information, you can simply fax this to us at   <strong>800-637-5164</strong>.</p>
          <p class=titleSub1>If you already have a secure online account, please <a href="http://nextimagemedical.com/login.php" target="_parent">sign in</a> here and your case will be automatically linked to your information on file. </p>
          <form name="at_intake" action="of_sub.jsp">
            <table border="0" cellpadding="4" cellspacing="0" >
              <tr>
                <td  class="tdBaseAlt2"><strong>Your Name</strong></td>
                <td ><input name="MessageName" type=text class="tdHeader" size="20" maxlength="80"></td>
                <td  class="tdBaseAlt2"><strong>Company</strong></td>
                <td ><input name="MessageCompany" type=text class="tdHeader" size="20" maxlength="80"></td>
                </tr>
              <tr>
                <td  class="tdBaseAlt2"><strong>Phone</strong></td>
                <td ><input name="MessagePhone" type=text class="tdHeader" size="20" maxlength="50"></td>
                <td  class="tdBaseAlt2"><strong>Fax</strong></td>
                <td ><input name="MessageFax" type=text class="tdHeader" size="20" maxlength="50"></td>
                </tr>
              <tr>
                <td  class="tdBaseAlt2"><strong>Email</strong></td>
                <td colspan="3" ><input name="MessageEmail" type=text class="tdHeader" size="20" maxlength="50"></td>
                </tr>
              </table>
            <table border="0" cellpadding="4" cellspacing="0" >
              <tr>
                <td  class="tdBaseAlt2"><strong> Adjuster Name<br>      
                  <input name="button" type="button" class="inputButtonSmall" id="button" onClick="document.forms.at_intake.PayerPhone.value = document.forms.at_intake.MessagePhone.value; document.forms.at_intake.AdjusterName.value = document.forms.at_intake.MessageName.value; document.forms.at_intake.PayerName.value = document.forms.at_intake.MessageCompany.value; document.forms.at_intake.PayerFax.value = document.forms.at_intake.MessageFax.value;" value="Same As Above">
                </strong></td>
                <td ><input name="AdjusterName" type=text class="tdHeader" size="20" maxlength="100"></td>
                <td  class="tdBaseAlt2"><strong>Insurance <br>
                  or Administrative<br>
                  Company</strong></td>
                <td ><input name="PayerName" type=text class="tdHeader" size="20" maxlength="100"></td>
                </tr>
              <tr>
                <td  class="tdBaseAlt2"><strong>Phone</strong></td>
                <td ><input name="PayerPhone" type=text class="tdHeader" size="20" maxlength="50"></td>
                <td  class="tdBaseAlt2"><strong>Fax</strong></td>
                <td ><input name="PayerFax" type=text class="tdHeader" size="20" maxlength="50"></td>
                </tr>
              <tr>
                <td  class="tdBaseAlt2"><strong>Claim #</strong></td>
                <td ><input name="CaseClaimNumber1" type=text class="tdHeader" size="20" maxlength="100"></td>
                <td  class="tdBaseAlt2"><strong>Preauthorization#</strong></td>
                <td ><input name="PayerPreAuthorizationConfirmation" type=text class="tdHeader" size="20" maxlength="100"></td>
                </tr>
              <tr>
                <td colspan="4"  class="xbig">Patient Information</td>
                </tr>
              <tr>
                <td  class="tdBaseAlt2"><strong>First</strong></td>
                <td ><input name="PatientFirstName" type="text" class="tdHeader" id="PatientFirstName" size="20" maxlength="100" /></td>
                <td  class="tdBaseAlt2"><strong>Last</strong></td>
        
                <td ><input name="PatientLastName" type="text" class="tdHeader" id="PatientLastName" size="20" maxlength="100" /></td>
                </tr>
              <tr>
                <td  class="tdBaseAlt2"><strong>SSN</strong></td>
                <td ><input name="PatientSSN" type="text" class="tdHeader" id="PatientSSN" size="20" maxlength="20" /></td>
                <td  class="tdBaseAlt2"><strong>DOB</strong></td>
                <td ><input name="PatientDOB"  type="text" class="tdHeader" id="PatientDOB" size="20" maxlength="10" /></td>
                </tr>
              <tr>
              <tr>
                <td  class="tdBaseAlt2"><strong>Address</strong></td>
                <td ><input name="PatientAddress1" type="text" class="tdHeader" id="PatientAddress1" size="20" maxlength="200" /></td>
                <td  class="tdBaseAlt2"><strong>City</strong></td>
                <td ><input name="PatientCity"  type="text" class="tdHeader" id="PatientCity" size="20" maxlength="100" /></td>
                </tr>
              <tr>
              <tr>
                <td  class="tdBaseAlt2"><strong>State</strong></td>
                <td ><select   name="PatientStateID" id="PatientStateID" ><jsp:include page="../generic/tStateLIShort.jsp" flush="true" ><jsp:param name="CurrentSelection" value="0" /></jsp:include></select></td>
                <td  class="tdBaseAlt2"><strong>ZIP</strong></td>
                <td ><input name="PatientZIP"  type="text" class="tdHeader" id="PatientZIP" size="20" maxlength="20" /></td>
                </tr>
              <tr>
                <td  class="tdBaseAlt2"><strong>Phone</strong></td>
                <td ><input name="PatientHomePhone" type="text" class="tdHeader" size="20" maxlength="50" /></td>
                <td  class="tdBaseAlt2"><strong>Mobile</strong></td>
                <td ><input name="PatientCellPhone" type="text" class="tdHeader" size="20" maxlength="50" /></td>
              </tr>
              <tr>
                <td  class="tdBaseAlt2"><strong>Employer</strong></td>
                <td ><input name="EmployerName" type="text" class="tdHeader" id="EmployerName" size="20" maxlength="50" /></td>
                <td  class="tdBaseAlt2"><strong>Employer Phone</strong></td>
                <td ><input name="EmployerPhone" type="text" class="tdHeader" id="EmployerPhone" size="20" maxlength="50" /></td>
              </tr>
              <tr>
                <td  class="tdBaseAlt2"><strong>Date of Injury</strong></td>
                <td ><input name="DateOfInjury" type="text" class="tdHeader" id="DateOfInjury" size="20" maxlength="50" /></td>
                <td  class="tdBaseAlt2">&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
              <tr>
                <td  class="tdBaseAlt2"><strong>Ordering Physician<br>
                  Full Name</strong></td>
                <td ><input name="ReferringPhysicianName" type=text class="tdHeader" size="20" maxlength="100"></td>
                <td  class="tdBaseAlt2"><strong>Phone</strong></td>
                <td ><input name="ReferringPhysicianPhone" type=text class="tdHeader" size="20" maxlength="100"></td>
              </tr>
              <tr>
                <td  class="tdBaseAlt2"><strong> Procedure &amp; Body Part:</strong></td>
                <td colspan="3" ><textarea name="CPTText" cols="40" rows="4" class="titleSub2" onKeyDown="textAreaStop(this,500)"></textarea></td>
                </tr>
              <tr>
                <td  class="tdBaseAlt2"><strong>Additional information:</strong></td>
                <td colspan="3" ><textarea name="MessageText" cols="40" rows="10" class="titleSub2" onKeyDown="textAreaStop(this,1900)"></textarea></td>
                </tr>
              <tr>
                <td colspan="4"  class="tdHeaderBright"><label>
                  <input name="button2" type="submit" class="inputButton" id="button2" value="Submit" />
                </label></td>
                </tr>
              </table>
            </form>

        </div>        
        
        
    </div>
	<div class="contentBox">
		<span class="xbig">What is the NextImage Secure Portal?</span><br>
		<br>
        The NextImage Portal allows you to submit referrals without having to re-enter your name and contact information.  Once you sign in, we know who you are. <br><br> You can also view the status of referral and view the diagnostic reports once available.   
      <br><br>If you already have a Username and Password to our Secure Portal, then <a href="signIn4.jsp">click here to sign in</a>. <br><br>		<span class="xbig">Tell me more about the security?</span><br><br>
        We use top SSL Security to protect the information you submit here.  Depending on your browser, you may notice the <em><strong>green</strong></em> highligh on the URL at the top or a <em><strong>green</strong></em> "NextImage Medical, Inc." to the side of the URL. You'll probably notice the similarity if you visit financial institutions and other high level security sites.<br><br>You won't see it everywhere, but we felt that it's important to provide you with the top security.  So you will see here. 
	</div>
</div>
</center>
</body>
</html>
