<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.* " %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

    <table cellpadding=0 cellspacing=0 border=0 width=100%  >
    <tr><td width=10>&nbsp;</td><td>
      <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM_Encounter")%>
      
      
      
  <%
//initial declaration of list class and parentID
    Integer        iReferralID        =    null;
    Integer        iEncounterID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);

   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
	    iEncounterID = (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
    else if (pageControllerHash.containsKey("iReferralID")) 
    {
	    iReferralID = (Integer)pageControllerHash.get("iReferralID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
	  String myReportType = request.getParameter("EDIT");
	  bltNIM3_Encounter myE = new bltNIM3_Encounter(iEncounterID);
	  NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,"load");
	  
//out.println(iAuthID);
%>
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td><h1>  Report Review</h1></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><table width="99%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2" class="tdHeader">Please Verify that the following is correct:
          	<p style="background:yellow;display:inline;"><br>
			DOS:<%=displayDateSDF1.format(myEO2.getNIM3_Appointment().getAppointmentTime())%></p>
		  <p style="background:#C7C7FF;display:inline;"><br>
			IC:<%=myEO2.getAppointment_PracticeMaster().getPracticeName()%><br>
			<%=myEO2.getAppointment_PracticeMaster().getOfficeAddress1()%><br>
			 <%=myEO2.getAppointment_PracticeMaster().getOfficeCity()%>, <%=new bltStateLI(myEO2.getAppointment_PracticeMaster().getOfficeStateID()).getShortState()%> <%=myEO2.getAppointment_PracticeMaster().getOfficeZIP() %></p>
          
          </td>
        </tr>
        <tr>
          <td colspan="2" class="borderHighlightGreen"><%=myE.getComments().replaceAll("(\r\n|\r|\n|\n\r)", "<br>")%></td>
        </tr>
        <tr>
          <td colspan="2" class="borderHighlightGreen"><table width="100%" border="1" cellpadding="3" cellspacing="0">
						    <%
	
				searchDB2 mySS_cpt = new searchDB2();
				
				java.sql.ResultSet myRS_cpt = null;;
				
				try
				{
					String mySQL_cpt = ("select * from tNIM3_Service where EncounterID = " + myE.getEncounterID()  + " AND (CPT>'') order by ServiceID");
					//out.println(mySQL);
					myRS_cpt = mySS_cpt.executeStatement(mySQL_cpt);
				}
				catch(Exception e)
				{
					out.println("ResultsSet:"+e);
				}
				int cnt55=0;
				while (myRS_cpt!=null&&myRS_cpt.next())
				{
					cnt55++;
					bltNIM3_Service working_bltNIM3_Service  =  new bltNIM3_Service(new Integer(myRS_cpt.getString("ServiceID")));
					if (working_bltNIM3_Service.getServiceStatusID().intValue()!=5){
					%>
						    <tr>
                            <td align="center" class="tdHeaderAlt"><%=cnt55%></td>
						      <td><span class="borderHighlightGreen"><strong>Lookup:</strong> [<%=NIMUtils.getCPTText(working_bltNIM3_Service.getCPT())%>]</span>						        <br />        
					          <span class="borderHighlightGreen"><strong>BP:</strong> [<%=working_bltNIM3_Service.getCPTBodyPart()%>]</span><br>
<span class="borderHighlightGreen"><strong>RuleOut:</strong> [<%=working_bltNIM3_Service.getCPTText()%>]</span></td>
						      <td>CPT: <strong><%=working_bltNIM3_Service.getCPT()%><br />
						        Mod: <strong><%=working_bltNIM3_Service.getCPTModifier()%></strong><br />
Qty Done: <strong><%=working_bltNIM3_Service.getCPTQty()%></strong>
                              </strong></td>
					        </tr>
						    <%
					}
				}
				mySS_cpt.closeAll();
			%>
					      </table></td>
        </tr>
        <tr class="tdHeader">
          <td width="50%" align="center">Rx</td>
          <td width="50%" align="center">Report</td>
        </tr>
        <tr>
          <td width="50%"><iframe width="100%" height="500" src="tNIM3_Referral_ShowFile.jsp?EDIT=rx"></iframe></td>
          <td width="50%"><iframe width="100%" height="500" src="tNIM3_Referral_ShowFile.jsp?EDIT=rp"></iframe></td>
        </tr>
      </table></td>
    </tr>
        <tr>
      <td>&nbsp;</td>
          <td colspan="2" class="tdHeader">Please select the appropriate option below:</td>
        </tr>
        <tr>
      <td>&nbsp;</td>
          <td align="center" ><input name="Agree" type="button" class="inputButton_md_Create" onclick="document.location='tNIM3_Encounter_RpReview_sub.jsp';" value="Agree" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="Disagree" type="button" class="inputButton_md_Action1" onclick="document.location='tNIM3_Encounter_RpReview_subNo.jsp';" value="Needs Further Review" /></td>
        </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>

    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    </table>
<br>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
