<%@page language="java" contentType="application/json; charset=UTF-8" import="java.io.*,com.winstaff.*, java.util.*, java.sql.*,com.google.gson.Gson;"%>
<%!class uaResult {
		String value;
		int id;
		int pid;
		String desc;
		public uaResult(String value, int id, int pid, String desc) {
			this.value = value;
			this.id = id;
			this.pid = pid;
			this.desc = desc;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
		public int getPid() {
			return pid;
		}

		public void setPid(int pid) {
			this.pid = pid;
		}
		
		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
	}%>
<%
	List<uaResult> uaResultList = new ArrayList<uaResult>();

	if (request.getParameter("term") != null && !request.getParameter("term").isEmpty()) {
		String search = request.getParameter("term");
		String query = "SELECT ua.userid, ua.contactfirstname || ' ' || ua.contactlastname , pm.payername, ua.payerid\n" +
				"from tuseraccount ua\n" +
				"join tnim3_payermaster pm on pm.payerid = ua.payerid\n" +
				"where LOWER (ua.contactfirstname || ' ' || ua.contactlastname\n" +
				") like('%" + search + "%')\n" +
				"ORDER BY\n" +
				"userid desc\n" +
				"limit 35";
		searchDB2 db = new searchDB2();

		ResultSet rs = db.executeStatement(query);

		try {
			while (rs.next()) {
				uaResultList.add(new uaResult(rs.getString(2), rs.getInt(1), rs.getInt(4), rs.getString(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closeAll();
		}
	}
	response.setHeader("Access-Control-Allow-Origin", "*");
	out.println(new Gson().toJson(uaResultList));
%>
