<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*    " %>
<%/*
    filename: tNIM3_Referral_main_NIM3_Referral_CaseID.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />


<title>NextImage - Secure</title><table cellpadding=0 cellspacing=0 border=0 width=900 >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM3_Referral")%>



<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
	  
	  //Check to see if payer is from City of San Diego
	  boolean isCityOfSanDiego  = false;
	  if (CurrentUserAccount.getPayerID()==649 || (new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID())).getParentPayerID()==649){
		  isCityOfSanDiego = true;
	  }
	  
	  
	  
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
      pageControllerHash.put("sParentReturnPage","tNIM3_Referral_main_NIM3_Referral_CaseID.jsp");
    pageControllerHash.remove("iReferralID");
    pageControllerHash.remove("iEncounterID");
    pageControllerHash.put("sINTNext","tNIM3_Referral_main_NIM3_Referral_CaseID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltNIM3_Referral_List        bltNIM3_Referral_List        =    new    bltNIM3_Referral_List(iCaseID,"","ReferralDate");
	bltNIM3_CaseAccount NCA = new bltNIM3_CaseAccount(iCaseID);
	bltUserAccount myUA = new bltUserAccount(new Integer(NCA.getAdjusterID()));
	bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(myUA.getPayerID());


			java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);


//declaration of Enumeration
    bltNIM3_Referral        working_bltNIM3_Referral;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Referral_List.elements();
    %>

         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0">
         <tr>
           <td><table border="0" cellpadding="5" cellspacing="0">
                 <tr>
                   <td colspan="5" align="center">If you have any questions, 
                   please contact a <br />
                   NextImage 
                   Support Representative at <strong>(888) 318-5111</strong></td>
                 </tr>
                 <tr>
                   <td nowrap="nowrap">Claim Number: </td>
                   <td><strong><%=NCA.getCaseClaimNumber()%></strong></td>
                   <td nowrap="nowrap">&nbsp;</td>
                   <td nowrap>Carrier: </td>
                   <td><strong><%=myPM.getPayerName()%></strong></td>
                 </tr>
                 <tr>
                   <td nowrap="nowrap">Patient: </td>
                   <td><strong><%=NCA.getPatientFirstName() + " "  + NCA.getPatientLastName()%></strong></td>
                   <td nowrap="nowrap">&nbsp;</td>
                   <td nowrap>Adjuster: </td>
                   <td><strong><%=myUA.getContactFirstName() + " " + myUA.getContactLastName()%></strong></td>
                 </tr>
                 <tr>
                   <td nowrap="nowrap">Date of Injury: </td>
                   <td><strong><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NCA.getDateOfInjury())%>" /></jsp:include></strong></td>
                   <td nowrap="nowrap">&nbsp;</td>
                   <td nowrap>&nbsp;</td>
                   <td>&nbsp;</td>
                 </tr>
        
           </table></td></tr>
    <%
    int altCnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
        String theClass = "tdBase";
        if (altCnt%2!=0)
        {
//            theClass = "tdBaseAlt";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_Referral  = (bltNIM3_Referral) leCurrentElement.getObject();
        working_bltNIM3_Referral.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltNIM3_Referral.isComplete())
        {
//            theClass = "incompleteItem";
        %>

        <%
        }
        else
        {
        %>
        <%
        }
		
		int myReferralEncounterCount = NIMUtils.getEncounterCount_ByReferralID(working_bltNIM3_Referral.getReferralID());
		if (myReferralEncounterCount>0)
		{
        %>

        <tr class=<%=theClass%> > 


<%String theClassF = "textBase";%>


<td valign="top" nowrap="nowrap">
<%
		   if (working_bltNIM3_Referral.getReferringPhysicianID().intValue()==0)
		   {%>
                
                   <center><p class="instructionsBig1">Note: This Case is still being reviewed by our support team.<br />
Some information that you submitted may not appear below<br />until we have reviewed and processed it.</p></center>
             <%
		   }
		   %>

            <table border="0" cellpadding="2" cellspacing="0">
              <tr>
                <td colspan="3" align="right" valign="middle" nowrap="nowrap">&nbsp;<%
if (working_bltNIM3_Referral.getReferringPhysicianID()>0)
{
%>Referring<br />
                Provider:<%}%></td>
                <td valign="middle" nowrap="nowrap"><%
if (working_bltNIM3_Referral.getReferringPhysicianID()>0)
{
%><div class="boxDr">
                  <jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" >
                    <jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getReferringPhysicianID()%>" />                
                    </jsp:include>
  </div><%}%></td>
               
                <td rowspan="2" align="center" valign="middle" nowrap="nowrap" class="instructionsBig1">
                <% 
				if (working_bltNIM3_Referral.getRxFileID()==0)
				{
				%>
                Supporting Documents can be submitted by clicking<br />
                  on the 'Upload' buttons to the left<br />
                  or faxed to us at:<br />
                  888-596-8680
                <% 
				}
				%>
                  
                  
                  </td>
              </tr>
              <tr>
                <td colspan="3" align="right" valign="middle" nowrap="nowrap">Referral<br />
                Date</td>
                <td valign="middle" nowrap="nowrap" class="borderHighlightGreen"> <jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Referral.getReferralDate())%>" /></jsp:include>&nbsp;</td>
                <td align="center" valign="middle" nowrap="nowrap">
                <%
if (isScheduler&&working_bltNIM3_Referral.getOrderFileID()==0)
{
%>
                  <input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=upor&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Upload Authorization">
                  <%
}
else if (working_bltNIM3_Referral.getOrderFileID()==0 && isCityOfSanDiego)
{
%>
                  <input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=upor&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Upload RFA">
                  <%
}
else if (working_bltNIM3_Referral.getOrderFileID()==0)
{
%>
                  <input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=upor&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Upload Authorization">
                  <%
}
else if (working_bltNIM3_Referral.getOrderFileID()>0 && isCityOfSanDiego)
{
%>
                  <input class="inputButton_md_Create"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=upor&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="View RFA">
                  <%
}
else
{
%>
                  <input  class="inputButton_md_Create"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=working_bltNIM3_Referral.getOrderFileID()%>&KM=p';" value="View Order" />
                <%
}
%>
                
                <%
if (working_bltNIM3_Referral.getRxFileID()==0&&isScheduler)
{
%>
                  <input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=uprx&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Upload Rx">
                  <%
}
else if (working_bltNIM3_Referral.getRxFileID()==0)
{
%>
                  <input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=uprx&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Upload Rx">
                  <%
}
else
{
%><input  class="inputButton_md_Create"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=working_bltNIM3_Referral.getRxFileID()%>&KM=p';" value="View Rx" />
                <%
}
%>&nbsp;</td>
              </tr>
            </table>
       </td>

</tr><tr>

  <td width="75%" align="left" valign="top" ><table width="100%">
  <tr><td>
    <%

			int cnt=0;
			
			try
			{
				searchDB2 mySS = new searchDB2();
				
				java.sql.ResultSet myRS = null;;
				String mySQL = ("select * from tNIM3_Encounter where ReferralID = " + working_bltNIM3_Referral.getReferralID()  + " AND tNIM3_Encounter.encounterstatusid in (1,6, 4, 8, 9) order by dateofservice");
				//out.println(mySQL);
				myRS = mySS.executeStatement(mySQL);
				
				String myMainTable= " ";
				int endCount = 0;
				int cnt2=0;
				while (myRS!=null&&myRS.next())
				{
					cnt++;
					bltNIM3_Encounter working_bltNIM3_Encounter  =  new bltNIM3_Encounter(new Integer(myRS.getString("EncounterID")));
					bltNIM3_Document working_bltNIM3_Document  =  new bltNIM3_Document(working_bltNIM3_Encounter.getReportFileID());
					
		bltNIM3_Referral NIMR = working_bltNIM3_Referral;

//		bltNIM3_CaseAccount NCA = new bltNIM3_CaseAccount(NIMR.getCaseID());
//		bltUserAccount myUAdj = new bltUserAccount(new Integer(NCA.getAdjusterID()));
//		bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(myUA.getPayerID());
//		bltUserAccount myRefDr = new bltUserAccount (NIMR.getReferringPhysicianID());



		boolean isReadyToSchedule = NIMUtils.isEncounterReadyToSchedule(working_bltNIM3_Encounter.getEncounterID())	;	
					
					%>
    <div class=caseBox>
      <table border="1" cellpadding="3" cellspacing="0">
        <tr class="tdHeaderAlt">
          <td align="center" valign="middle" class="tdHeaderAlt">     <b>ScanPass:&nbsp;</b><%=working_bltNIM3_Encounter.getScanPass()%>&nbsp;<% if (false){%><br /><input  class="inputButton_md_Create" disabled="disabled"  type="button"  value="Inquire About Case" /><% }%></td>
          <td align="center" valign="middle"><%if (working_bltNIM3_Encounter.getReportFileID()>0){%><table width="50%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="1" align="center"><%=working_bltNIM3_Document.getDocName()%>&nbsp;</td>
              </tr>
            <tr>
              <td align="center"> <input  class="inputButton_md_Create"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=working_bltNIM3_Encounter.getReportFileID()%>&KM=p';" value="Report" />&nbsp;</td>
            </tr>
            </table><%}else if (isScheduler){%>     <input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=upreport&EDITID=<%=working_bltNIM3_Encounter.getEncounterID()%>&KM=p'" value="Upload Report">
            <%}else {%>     <input class="inputButton_md_Default"   disabled="disabled" type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=upreport&EDITID=<%=working_bltNIM3_Encounter.getEncounterID()%>&KM=p'" value="Report Not Yet Available">
            <%}%></td>
          </tr>
  <tr><td colspan=2><table width="100%" border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td>
        <img src=images/payer_stage<%=NIMUtils.aEncounterStatus_HTML_Payer(working_bltNIM3_Encounter.getEncounterID())%>.jpg />
  </td>
      </tr>
  </table>
  </td></tr>					<tr>
    <td width="75%" valign="top">
      
      <table width="100%" border="1" cellpadding="3" cellspacing="0">
        <%
	
				searchDB2 mySS_cpt = new searchDB2();
				
				java.sql.ResultSet myRS_cpt = null;;
				
				try
				{
					String mySQL_cpt = ("select * from tNIM3_Service where EncounterID = " + working_bltNIM3_Encounter.getEncounterID()  + " AND (CPT>'') order by ServiceID");
					//out.println(mySQL);
					myRS_cpt = mySS_cpt.executeStatement(mySQL_cpt);
				}
				catch(Exception e)
				{
					out.println("ResultsSet:"+e);
				}
				int cnt55=0;
				while (myRS_cpt!=null&&myRS_cpt.next())
				{
					cnt55++;
					bltNIM3_Service working_bltNIM3_Service  =  new bltNIM3_Service(new Integer(myRS_cpt.getString("ServiceID")));
					%>
        <tr>
          <td align="center" class="tdHeaderAlt"><%=cnt55%></td>
          <td><span class="borderHighlightGreen"><%=NIMUtils.getCPTText(working_bltNIM3_Service.getCPT())%></span>						        <br />        
            </td>
          <td><span class="borderHighlightGreen">[BP: <%=working_bltNIM3_Service.getCPTBodyPart()%>]</span></td>
          <td>CPT: <strong><%=working_bltNIM3_Service.getCPT()%></strong></td>
          <td><%if (working_bltNIM3_Service.getDICOM_ShowImage().intValue()==1){ if (isUserAndroid){%><a href="tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=dicom_pls&hideCase=y&EDITID=<%=working_bltNIM3_Encounter.getEncounterID()%>&SEDITID=<%=working_bltNIM3_Service.getServiceID()%>&KM=p" target="_blank">View Image</a><%}else{%><input class="inputButton_md_Test"   type=button onClick = "modalPost('ViewDicom', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp&EDIT=dicom_pls&hideCase=y&EDITID=<%=working_bltNIM3_Encounter.getEncounterID()%>&SEDITID=<%=working_bltNIM3_Service.getServiceID()%>&KM=p','AddCommtrack','dialogWidth:1000px;dialogHeight:600px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));this.disabled=true;document.location =document.location;" value="View Image"><%} }%>&nbsp;
&nbsp;</td>
          </tr>
        <%
				}
				mySS_cpt.closeAll();
			%>
        </table>
      
      
      
  <!-- start enc detail-->
      
  <table width="100%" border="0" cellspacing="0" cellpadding="2">
    <tr>
      <td width="75%">
        
        
        
        
        <% 
	   if (isScheduler && NIMR.getReferralTypeID().intValue()==3&&working_bltNIM3_Encounter.getAppointmentID()==0&&isReadyToSchedule )
	   {
		   %><input class="inputButton_md_Action1"   type=button onClick = "this.disabled=true;document.location ='tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp?EDIT=sched&EDITID=<%=working_bltNIM3_Encounter.getEncounterID()%>&KM=p'" value="Schedule">
        
        <%
	   }
	   if (working_bltNIM3_Encounter.getAppointmentID()>0)
	   {
		   %>       <table width="100%" border="0" cellpadding="3" cellspacing="0" class="borderHighlight1" >
          <tr class="tdHeader">
            <td  class="tdHeader">Appointment:&nbsp;</td>
            <td align="center"  class="borderHighlightGreen"><%
bltNIM3_Appointment myNALU = new bltNIM3_Appointment(working_bltNIM3_Encounter.getAppointmentID());
bltPracticeMaster myPracM = new bltPracticeMaster(myNALU.getProviderID());
//bltICMaster myICM = new bltICMaster ();
//      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
            java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
            java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
            java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
            java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
            java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);
			java.text.SimpleDateFormat displayDateHourMin = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHourMin);
			java.util.Date dbAppointment = myNALU.getAppointmentTime();
			String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
			String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
			String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
			String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
			String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
			String sAppointmentHour = displayDateHour.format((dbAppointment));
			String sAppointmentHourMin = displayDateHourMin.format((dbAppointment));

%><%=sAppointmentTimeDayWeek%>&nbsp;<%=sAppointmentTimeMonth%>&nbsp;<%=sAppointmentTimeDay%>&nbsp;<%=sAppointmentTimeYear%> at <%=sAppointmentHourMin%>&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          <tr class="borderHighlightGreen">
            <td colspan="3" class="borderHighlightGreen"></td>
            </tr>
          <tr>
            <td><strong><%=myPracM.getPracticeName()%></strong> <br /></td>
            <td>[ID:&nbsp;<%=myPracM.getPracticeID()%>]</td>
            <td align="center" valign="middle">&nbsp;</td>
          </tr>
          </table>
        
        
        
        
        
        
        
        <%




	}
	else
	{
		%><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="borderHighlightGreen">	Schedule: Pending&nbsp;</td>
            </tr>
          </table>
        
        
        <%
	}
	   %></td>
      </tr>
    </table>
      
      
  <!-- end enc detail-->
      
      
      
      
      
      </td>
    <td valign="top" align="center">
  
 

      <p>
        <%
if (false)//working_bltNIM3_Encounter.getBillingHCFA_ToPayer_FileID()!=0&&working_bltNIM3_Encounter.getisRetro()==1)
{
%><input  class="inputButton_md_Create"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=working_bltNIM3_Encounter.getBillingHCFA_ToPayer_FileID()%>&KM=p';" value="Invoice to Client" />
        <%
}
if (false)//working_bltNIM3_Encounter.getBillingHCFA_FromProvider_FileID()!=0&&working_bltNIM3_Encounter.getisRetro()==1)
{
%><br>
        <br><input  class="inputButton_md_Create"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=working_bltNIM3_Encounter.getBillingHCFA_FromProvider_FileID()%>&KM=p';" value="Bill from Provider" />

<%
}
%>&nbsp;
        
        
      </p>
 



</td>
    </tr>
        
  <tr><td colspan="2">
    
    
    
    
  </td></tr>
        <%
				}
				mySS.closeAll();
			}
			catch(Exception e)
			{
				out.println("ResultsSet:"+e);
			}
		%>
        </table>
      <%
			if (false&&cnt==0&&working_bltNIM3_Referral.getRxFileID()>0)
			{
				%>
      <input class="inputButton_md_Action1" type=button onClick = "this.disabled=true;document.location ='tNIM3_Referral_main_NIM3_Referral_CaseID_form_authorize.jsp?EDIT=openflow&EDITID=<%=working_bltNIM3_Referral.getReferralID()%>&KM=p'" value="Add Encounter">
      <%
			}
		%>
      </div> 
  </td></tr></table>
    
    
</td>
  </tr>
        <%
		}//end isvalid

	}//end while
       }//end of if
       else 
       {
           %>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>