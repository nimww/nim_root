<%@page language="java" import="com.winstaff.*"%>
<%/*

    filename: out\jsp\tNIM3_PayerMaster_form.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_maint.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table width=98% height="90%" cellpadding=30 cellspacing=0 border=0 >
    <tr><td width=50 background="gsn_nim3/images/nim_v3_bg.jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign="top">
<%
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
  //page security
  if (isScheduler2)
  {

%>
<table border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p class=title>Payer Search</p>
      <table border="0" cellspacing="0" cellpadding="2">
        <tr>
          <td><input name="button" type="submit" class="inputButton_md_Default" id="button" onclick="document.location='Maint_Home.jsp';" value="Return to Maintenance" /></td>
          <td><input name="button2" type="submit" class="inputButton_md_Create" id="button2" onclick="document.PayerEdit.location='tNIM3_PayerMaster_form.jsp?EDIT=new';" value="Create New Payer" /></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
<%
try
{

String myPayerID = "";
String myPayerTypeID = "";
String myPayerName = "";
String myOfficeCity = "";
String myOfficeStateID = "0";
String myRequiresOnlineImage = "0";
String mySalesDivision = "";
String myParentPayerID = "";
String myParentPayerName = "";
String orderBy = "firstname";
int startID = 0;
int maxResults = 15;

boolean firstDisplay = false;

String ssTestCPT = "";
String ssTestMOD = "";
String ssTestZIP = "";
String ssTestDATE = "today";
try
{
	ssTestCPT = request.getParameter("ssTestCPT");
	ssTestMOD = request.getParameter("ssTestMOD");
	ssTestZIP = request.getParameter("ssTestZIP");
	ssTestDATE = request.getParameter("ssTestDATE");
}
catch (Exception e)
{
}
finally
{
	if (ssTestCPT==null)
	{
		ssTestCPT = "";
		ssTestMOD = "";
		ssTestZIP = "";
		ssTestDATE = "today";
	}
}

try
{
maxResults = Integer.parseInt(request.getParameter("maxResults"));
startID = Integer.parseInt(request.getParameter("startID"));
myPayerID = request.getParameter("PayerID").toLowerCase();
myPayerTypeID = request.getParameter("PayerTypeID").toLowerCase();
myPayerName = request.getParameter("PayerName").toLowerCase();

myOfficeCity = request.getParameter("OfficeCity").toLowerCase();
myOfficeStateID = request.getParameter("OfficeStateID").toLowerCase();
myRequiresOnlineImage = request.getParameter("RequiresOnlineImage").toLowerCase();
mySalesDivision = request.getParameter("SalesDivision").toLowerCase();
myParentPayerID = request.getParameter("ParentPayerID").toLowerCase();
myParentPayerName = request.getParameter("ParentPayerName").toLowerCase();
orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
maxResults = 15;
startID = 0;
firstDisplay = true;
myPayerID = "";
myPayerTypeID = "";
myPayerName = "";
myOfficeCity = "";
myOfficeStateID = "0";
myRequiresOnlineImage = "0";
mySalesDivision = "";
myParentPayerID = "";
myParentPayerName = "";
orderBy = "PayerID";
}
if (orderBy == null)
{
maxResults = 15;
startID = 0;
firstDisplay = true;
myPayerID = "";
myPayerTypeID = "";
myPayerName = "";
myOfficeCity = "";
myOfficeStateID = "0";
myRequiresOnlineImage = "0";
mySalesDivision = "";
myParentPayerID = "";
myParentPayerName = "";
orderBy = "PayerID";
}


if (firstDisplay)
{
%>
<table cellpadding=2 cellspacing=2>
  <tr>
<td width=10>&nbsp;</td>
<td class=tdBase><p>&nbsp;</p>
</td>
</tr>
<tr>
<td width=10>&nbsp;</td>

<td>
<form name="form1" method="POST" action="tNIM3_PayerMaster_query.jsp">
  <table width="400" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#003333">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
        <tr>
         <td class=tdHeaderAlt>
         PayerID
         </td>
         <td>
         <input type=text name="PayerID">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         PayerTypeID
         </td>
         <td>
			<select name="PayerTypeID">
                <jsp:include page="../generic/tPayerTypeLIShort.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="-1" />
                </jsp:include>
              </select>
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Name
         </td>
         <td>
         <input type=text name="PayerName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         City
         </td>
         <td>
         <input type=text name="OfficeCity">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         State
         </td>
         <td>
              <select name="OfficeStateID">
                <jsp:include page="../generic/tStateLIShort.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="0" />
                </jsp:include>
              </select>
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Archiving
         </td>
         <td>
              <select name="RequiresOnlineImage">
                <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="0" />
                </jsp:include>
              </select>
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Sales Division
         </td>
         <td>
         <input type=text name="SalesDivision">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Parent ID
         </td>
         <td>
         <input type=text name="ParentPayerID">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Parent Name
         </td>
         <td>
         <input type=text name="ParentPayerName">
         </td>
        </tr>
            <tr>
            <td class=tdHeaderAlt><p>Sort by:</p></td>
            <td> 
              <select name=orderBy>
                <option value="PayerID">PayerID</option>
                <option value="PayerTypeID">PayerTypeID</option>
                <option value="PayerName">Name</option>
                <option value="OfficeCity">City</option>
                <option value="RequiresOnlineImage">Archiving</option>
                <option value="SalesDivision">Sales Division</option>
                <option value="ParentPayer.PayerID">Parent ID</option>
                <option value="ParentPayer.PayerName">Parent Name</option>
              </select>
            </td>
          </tr>
          <tr bgcolor="#CCCCCC"> 
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td> 
              <input type=hidden name="startID" value=0>
              <input type=hidden name="maxResults" value=10>
              <input type="submit" name="Submit2" value="Submit">
            </td>
            <td>&nbsp;</td>
          </tr>
        </table>
  </form>
  <%
}
else
{


db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(null);String myWhere = "where (";
boolean theFirst = true;

try
{
if (!myPayerID.equalsIgnoreCase(""))
{
    if (!myPayerID.equalsIgnoreCase(""))
    {
        if (!theFirst) { myWhere+=" and ";}
        myWhere += "tNIM3_PayerMaster.PayerID = ?";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myPayerID));
        theFirst = false;
    }
}
if (!myPayerTypeID.equalsIgnoreCase("-1"))
{
    if (!myPayerTypeID.equalsIgnoreCase(""))
    {
        if (!theFirst) { myWhere+=" and ";}
        myWhere += "tNIM3_PayerMaster.PayerTypeID = ?";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myPayerTypeID));
        theFirst = false;
    }
}
if (!myPayerName.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myPayerName.indexOf("%")>=0)
    {
        myWhere += "lower(tNIM3_PayerMaster.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPayerName));
    }
    else if (myPayerName.length()<=3)
    {
        myWhere += "lower(tNIM3_PayerMaster.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPayerName + "%"));
    }
    else
    {
        myWhere += "lower(tNIM3_PayerMaster.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myPayerName + "%"));
    }
    theFirst = false;
}
if (!myOfficeCity.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myOfficeCity.indexOf("%")>=0)
    {
        myWhere += "lower(tNIM3_PayerMaster.OfficeCity) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myOfficeCity));
    }
    else if (myOfficeCity.length()<=3)
    {
        myWhere += "lower(tNIM3_PayerMaster.OfficeCity) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myOfficeCity + "%"));
    }
    else
    {
        myWhere += "lower(tNIM3_PayerMaster.OfficeCity) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myOfficeCity + "%"));
    }
    theFirst = false;
}
if (!myOfficeStateID.equalsIgnoreCase(""))
{
    if (!myOfficeStateID.equalsIgnoreCase("0"))
    {
        if (!theFirst) { myWhere+=" and ";}
        myWhere += "tNIM3_PayerMaster.OfficeStateID = ?";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myOfficeStateID));
        theFirst = false;
    }
}
if (!myRequiresOnlineImage.equalsIgnoreCase(""))
{
    if (!myRequiresOnlineImage.equalsIgnoreCase("0"))
    {
        if (!theFirst) { myWhere+=" and ";}
        myWhere += "tNIM3_PayerMaster.RequiresOnlineImage = ?";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myRequiresOnlineImage));
        theFirst = false;
    }
}
if (!mySalesDivision.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (mySalesDivision.indexOf("%")>=0)
    {
        myWhere += "lower(tNIM3_PayerMaster.SalesDivision) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,mySalesDivision));
    }
    else if (mySalesDivision.length()<=3)
    {
        myWhere += "lower(tNIM3_PayerMaster.SalesDivision) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,mySalesDivision + "%"));
    }
    else
    {
        myWhere += "lower(tNIM3_PayerMaster.SalesDivision) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + mySalesDivision + "%"));
    }
    theFirst = false;
}
if (!myParentPayerID.equalsIgnoreCase(""))
{
    if (!myParentPayerID.equalsIgnoreCase(""))
    {
        if (!theFirst) { myWhere+=" and ";}
        myWhere += "ParentPayer.PayerID = ?";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myParentPayerID));
        theFirst = false;
    }
}
if (!myParentPayerName.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myParentPayerName.indexOf("%")>=0)
    {
        myWhere += "lower(ParentPayer.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myParentPayerName));
    }
    else if (myParentPayerName.length()<=3)
    {
        myWhere += "lower(ParentPayer.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myParentPayerName + "%"));
    }
    else
    {
        myWhere += "lower(ParentPayer.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myParentPayerName + "%"));
    }
    theFirst = false;
}
myWhere += ")";

//System.out.println(myWhere);
if (theFirst||myWhere.equalsIgnoreCase(")"))
{
    myWhere = "";
}


}
catch(Exception e)
{
    out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;
String mySQL = "select tNIM3_PayerMaster.*,ParentPayer.PayerID as ParentPayerID, ParentPayer.PayerName as ParentPayerName from tNIM3_PayerMaster LEFT JOIN tNIM3_PayerMaster as ParentPayer on ParentPayer.payerid = tNIM3_PayerMaster.parentpayerid " + myWhere + " order by " + orderBy + " LIMIT " + (maxResults+1) + " OFFSET " + startID + "";

try
{
   myDPSO.setSQL(mySQL);
   //out.print(mySQL);
   myRS = mySS.executePreparedStatement(myDPSO);
}
catch(Exception e)
{
    out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

    int endCount = 0;

    int cnt=0;
    int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
        cnt++;
        if (cnt<=maxResults)
        {
            cnt2++;
            String myClass = "tdBase";
            if (cnt2%2==0)
            {
                myClass = "tdBaseAlt";
            }
			

			
			
            myMainTable +="<tr class="+myClass+">";
            myMainTable +="<td nowrap>";
            myMainTable +="<input type=button name=Edit class=inputButton_md_Default value=Edit onClick=\"document.PayerEdit.location='tNIM3_PayerMaster_form.jsp?EDIT=edit&EDITID=" + myRS.getString("PayerID")+"'\"><br>";
            myMainTable +="<input type=button name=Edit class=inputButton_sm_Test value=\"FSID Calc\" onClick=\"modalPost('TESTCPTFS', modalWin('LI_ModalPass.jsp?ssPF=testZIPFS.jsp?&nullParam=null&hideCase=y&&iPayerID=" + myRS.getString("PayerID") +"&testCPT=' + document.getElementById('testCPT').value + '&testMOD=' + document.getElementById('testMOD').value + '&testZIP=' + document.getElementById('testZIP').value + '&testDATE=' + document.getElementById('testDATE').value,'TESTCPTFS','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));\">";
            myMainTable +="</td>";
            myMainTable +="<td>"+cnt+"</td>";
            myMainTable +="<td>";
            myMainTable +="<a target=\"PayerEdit\" href=\"tNIM3_PayerMaster_form.jsp?EDIT=edit&EDITID=" + myRS.getString("PayerID")+"\">";
            myMainTable +=myRS.getString("PayerID")+"";
            myMainTable +="</a>";
            myMainTable +="</td>";
            myMainTable +="<td>";
			String sPayerTypeID = myRS.getString("PayerTypeID");
			String sPayerType = "0=Invalid";
			if (sPayerTypeID.equalsIgnoreCase("1")) { sPayerType = "1=Provider";}
			else if (sPayerTypeID.equalsIgnoreCase("2")) { sPayerType = "2=Payer-Branch";}
			else if (sPayerTypeID.equalsIgnoreCase("3")) { sPayerType = "3=Parent";}
            myMainTable +=sPayerType +"";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("PayerName")+"";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("OfficeCity")+"";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=(new bltStateLI (new Integer(myRS.getString("OfficeStateID")) ).getShortState()+"");
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=(new bltYesNoLI (new Integer(myRS.getString("RequiresOnlineImage")) ).getYNShort()+"");
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("SalesDivision")+"";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("ParentPayerID")+"";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("ParentPayerName")+"";
            myMainTable +="</td>";
            myMainTable +="</tr>";
        }
   }
    mySS.closeAll();
    endCount = cnt;

    if (maxResults<=0)
    {
        maxResults=10;
    }

    if (startID<=0)
    {
        startID=0;
    }




%>
  <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</p>
<table width=100% border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
<tr>
<td width=10>&nbsp;</td>
<td>
<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
<tr>
<td>

<form name="selfForm" method="POST" action="tNIM3_PayerMaster_query.jsp">
<table border=0 cellspacing=1 width='100%' cellpadding=2>
  <tr >
    <td colspan=5 class=tdHeader><table  border="0" class="borderHighlightBlackDotted" cellspacing="0" cellpadding="2">
      <tr >
        <td colspan="8" nowrap="nowrap" align="center" class="tdHeaderAltDark">CPT/Fee Schedule Tester</td>
      </tr>
      <tr>
        <td nowrap="nowrap"><label> <strong>CPT</strong>: </label></td>
        <td nowrap="nowrap"><input name="ssTestCPT" type="text" value="<%=ssTestCPT%>" class="inputButton_md_Test" id="testCPT" size="7" maxlength="10" /></td>
        <td nowrap="nowrap">Mod:</td>
        <td nowrap="nowrap"><input name="ssTestMOD" type="text" value="<%=ssTestMOD%>" class="inputButton_md_Test" id="testMOD" size="4" maxlength="2" /></td>
        <td nowrap="nowrap"><strong>ZIP</strong>:</td>
        <td nowrap="nowrap"><input name="ssTestZIP" type="text" value="<%=ssTestZIP%>"  class="inputButton_md_Test" id="testZIP" size="7" maxlength="5" /></td>
        <td nowrap="nowrap"><strong>Date</strong>:</td>
        <td nowrap="nowrap"><input name="ssTestDATE" type="text" value="<%=ssTestDATE%>"  class="inputButton_md_Test" id="testDATE" size="7" maxlength="10" /></td>
      </tr>
    </table></td>
    <td colspan=1 class=tdBase nowrap="nowrap">&nbsp;</td>
    <td colspan=1 class=tdHeader nowrap><input type=hidden name=maxResults value=<%=maxResults%> />
      <input type=hidden name=startID value=0 />
      <p>Sort:
        <select name=orderBy id="orderBy">
          <option value=PayerID <%if (orderBy.equalsIgnoreCase("PayerID")){out.println(" selected");}%> >PayerID</option>
          <option value=PayerTypeID <%if (orderBy.equalsIgnoreCase("PayerTypeID")){out.println(" selected");}%> >PayerTypeID</option>
          <option value=PayerName <%if (orderBy.equalsIgnoreCase("PayerName")){out.println(" selected");}%> >Name</option>
          <option value=OfficeCity <%if (orderBy.equalsIgnoreCase("OfficeCity")){out.println(" selected");}%> >City</option>
          <option value=RequiresOnlineImage <%if (orderBy.equalsIgnoreCase("RequiresOnlineImage")){out.println(" selected");}%> >Archiving</option>
          <option value=SalesDivision <%if (orderBy.equalsIgnoreCase("SalesDivision")){out.println(" selected");}%> >Sales Division</option>
          <option value=ParentPayer.PayerID <%if (orderBy.equalsIgnoreCase("ParentPayer.PayerID")){out.println(" selected");}%> >Parent ID</option>
          <option value=ParentPayer.PayerName <%if (orderBy.equalsIgnoreCase("ParentPayer.PayerName ")){out.println(" selected");}%> >Parent Name</option>
        </select>
      </p></td>
    <td class=tdHeader colspan=1><p  align="center">
      <input type="Submit" name="Submit" value="Submit" align="middle" />
    </p></td>
    
<td colspan="2" align="right"><%if (startID>0)
{%>
      <a href="tNIM3_PayerMaster_query.jsp?startID=<%=(startID-maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&PayerID=<%=myPayerID%>&PayerTypeID=<%=myPayerTypeID%>&PayerName=<%=myPayerName%>&OfficeCity=<%=myOfficeCity%>&OfficeStateID=<%=myOfficeStateID%>&RequiresOnlineImage=<%=myRequiresOnlineImage%>&SalesDivision=<%=mySalesDivision%>&ParentPayerID=<%=myParentPayerID%>&ParentPayerName=<%=myParentPayerName%>"><< previous <%=maxResults%></a>&nbsp;&nbsp;
      <%
}
else
{
%><p>&nbsp;</p><%
}
%></td>    
    
    <td> <%
if (cnt>cnt2)
{
%>
<a href="tNIM3_PayerMaster_query.jsp?startID=<%=(startID+maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&PayerID=<%=myPayerID%>&PayerTypeID=<%=myPayerTypeID%>&PayerName=<%=myPayerName%>&OfficeCity=<%=myOfficeCity%>&OfficeStateID=<%=myOfficeStateID%>&RequiresOnlineImage=<%=myRequiresOnlineImage%>&SalesDivision=<%=mySalesDivision%>&ParentPayerID=<%=myParentPayerID%>&ParentPayerName=<%=myParentPayerName%>"> next >> <%=maxResults%></a>         
        <%
}
else
{
%><p>&nbsp;</p><%
}


}
catch(Exception e)
{
out.println("PrevNext:"+e);
}




try{

%>
     </td>
  </tr>
  <tr class=tdHeader>
    <td  colspan=1>&nbsp;</td>
    <td  colspan=1 width=50>&nbsp;</td>
    <td colspan=1><input type="text" name="PayerID" value="<%=myPayerID%>" size="5" /></td>
    <td colspan=1><select name="PayerTypeID">
      <jsp:include page="../generic/tPayerTypeLIShort.jsp" flush="true" >
        <jsp:param name="CurrentSelection" value="<%=myPayerTypeID%>" />
        </jsp:include>
    </select></td>
    <td colspan=1><input type="text" name="PayerName" value="<%=myPayerName%>" size="15" /></td>
    <td colspan=1><input type="text" name="OfficeCity" value="<%=myOfficeCity%>" size="8" /></td>
    <td colspan=1><select name="OfficeStateID">
      <jsp:include page="../generic/tStateLIShort.jsp" flush="true" >
        <jsp:param name="CurrentSelection" value="<%=myOfficeStateID%>" />
        </jsp:include>
    </select></td>
    <td colspan=1><select name="RequiresOnlineImage" id="RequiresOnlineImage">
      <jsp:include page="../generic/tYesNoLILong.jsp" flush="true" >
        <jsp:param name="CurrentSelection" value="<%=myRequiresOnlineImage%>" />
        </jsp:include>
    </select></td>
    <td colspan=1><input name="SalesDivision" type="text" id="SalesDivision" value="<%=mySalesDivision%>" size="5" /></td>
    <td colspan=1><input type="text" name="ParentPayerID" value="<%=myParentPayerID%>" size="5" /></td>
    <td colspan=1><input name="ParentPayerName" type="text" id="ParentPayerName" value="<%=myParentPayerName%>" size="15" /></td>
  </tr>
  <tr class=tdHeaderAlt>
    <td nowrap="nowrap">Action</td>
    <td nowrap="nowrap">#</td>
    <td colspan=1 nowrap="nowrap"> PayerID </td>
    <td colspan=1 nowrap="nowrap"> Type</td>
    <td colspan=1 nowrap="nowrap"> Name </td>
    <td colspan=1 nowrap="nowrap"> City </td>
    <td colspan=1 nowrap="nowrap"> State </td>
    <td colspan=1 nowrap="nowrap"> Archiving </td>
    <td colspan=1 nowrap="nowrap"> Sales Division </td>
    <td colspan=1 nowrap="nowrap"> Parent ID </td>
    <td colspan=1 nowrap="nowrap"> Parent Name </td>
  </tr>
  <%=myMainTable%>
</table>
<%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>



  </table>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>    

   

        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

 </td>
    <td valign="top" height="90%" width="70%"><iframe width="500px" height="90%" name="PayerEdit" src="blank.jsp" /></td>
  </tr>
</table>


    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>