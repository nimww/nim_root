<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tNIM3_CaseAccount_form.jsp
    Created on Jan/05/2009
    Updated on Jan/12/2011
    Created by: Scott Ellis
*/%>


<%

if (true)
{
String thePLCID = "200";
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_ClearNoJS.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<link href="ui_200/style_PayerID.css" rel="stylesheet" type="text/css">



    <style type="text/css">
<!--
body {
	background-color: #FFF;
}
-->
</style><table cellpadding=0 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td>

<%
//initial declaration of list class and parentID
    boolean accessValid = true;
    // required for Type2
    String sKeyMasterReference = null;
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
	  Integer UserSecurityGroupID = new Integer(2);
	  boolean isShowAudit = false;
//initial declaration of list class and parentID
//always new
String ip = request.getRemoteAddr();
String host = request.getRemoteHost();

    bltNIM3_CommTrack        NIM3_CommTrack        =       new    bltNIM3_CommTrack();
	NIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setExtUserID(new Integer(19));
	NIM3_CommTrack.setComments("Start (never close)\nIP: " + ip + "\nHost: " + host);
	NIM3_CommTrack.commitData();

//fields
        %>
<table width="100%">
<tr>
<td>
<p class=title>Call Service Phone Intake Form</p>
<p class="instructionsBig1">Hello, NextImage Medical-WorkWell. My  name is ______ at the central message center.</p>
<p class="instructionsBig1">Would like to submit a new Diagnostic imaging or Physical therapy referral or leave a message for a service representative?</p>
<p class=instructionsBig1><span class="titleSub2">Note: if just leaving a message, fill out the 1st form only.</span> <span class="titleSub2">If they are submitting a new referral, fill out both forms.
  </span></p>
<p class=instructionsBig1><span class="titleSub2">For all calls, we need to collect the "Caller" the "Company" and their "Phone" Additional fields are provided as needed.
  </span></p>
<p class=instructionsBig1><span class="titleSub2">Any additional information collected, please record in the Notes field. </span></p>
<form name="at_intake" action="ext3_submit_sub.jsp"><input type="hidden" name="commID" value="<%=NIM3_CommTrack.getUniqueID()%>" />
  <table border="1" cellpadding="3" cellspacing="2" class="tdBaseAlt4Blue">
    <tr>
      <td bgcolor="#006699" class="tdHeaderBright">Caller*</td>
      <td><input name="MessageName" type=text class="tdHeader"  size="30" maxlength="80"></td>
      <td bgcolor="#006699" class="tdHeaderBright">Company*</td>
      <td><input name="MessageCompany" type=text class="tdHeader"  size="30" maxlength="80"></td>
      </tr>
    <tr>
      <td bgcolor="#006699" class="tdHeaderBright">Phone*</td>
      <td><input name="MessagePhone" type=text class="tdHeader"  size="30" maxlength="50"></td>
      <td class="tdHeaderBright">Fax</td>
      <td><input name="MessageFax" type=text class="tdHeader"  size="30" maxlength="50"></td>
      </tr>
    <tr>
      <td class="tdHeaderBright">Email</td>
      <td colspan="3"><input name="MessageEmail" type=text class="tdHeader"  size="30" maxlength="50"></td>
      </tr>
    <tr>
      <td bgcolor="#006699" class="tdHeaderBright">Notes</td>
      <td colspan="3"><textarea name="MessageText" cols="70" rows="10" class="titleSub2" onKeyDown="textAreaStop(this,1900)"><%=NIM3_CommTrack.getMessageText()%></textarea></td>
      </tr>
  </table>
  <p class=title>If Submitting a New Referral:</p>
  <p class=instructionsBig1>I'd be glad to help you with that. I just need to collect some information.</p>
  <p class=title><span class="titleSub2">Note: Please record as much information as possible. If the caller does not have or refuses to give certain information, just continue on and collect what you can.</span></p>
  <ul class="instructionsBig1">
    <li>What  is the Patient&rsquo;s name</li>
    <li>Social Security #</li>
    <li>Do you have the patients Home phone?  Cell Phone?</li>
  </ul>
  <table border="1" cellpadding="3" cellspacing="2" class="tdBaseAlt4Green">
    <tr>
      <td class="tdHeaderBright">Patient First</td>
      <td><input name="PatientFirstName" type=text class="tdHeader"  size="30" maxlength="100"></td>
      <td class="tdHeaderBright">Last</td>
      <td><input name="PatientLastName" type=text class="tdHeader"  size="30" maxlength="100"></td>
      </tr>
    <tr>
      <td class="tdHeaderBright">SSN</td>
      <td><input name="PatientSSN" type=text class="tdHeader"  size="30" maxlength="20"></td>
      <td class="tdHeaderBright">DOB</td>
      <td><input name="PatientDOB"  type=text class="tdHeader" size="30" maxlength=10 ></td>
      </tr>
    <tr>
      <td class="tdHeaderBright">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="tdHeaderBright">&nbsp;</td>
      <td>&nbsp;</td>
      </tr>
    <tr>
      <td class="tdHeaderBright">Phone</td>
      <td><input name="PatientHomePhone" type=text class="tdHeader"  size="30" maxlength="50"></td>
      <td class="tdHeaderBright">Mobile</td>
      <td><input name="PatientCellPhone" type=text class="tdHeader"  size="30" maxlength="50"></td>
      </tr>
    <tr >
      <td class="tdHeaderBright"><strong>Employer</strong></td>
      <td><input name="EmployerName" type="text" class="tdHeader" id="EmployerName" size="20" maxlength="50" /></td>
      <td class="tdHeaderBright" ><strong>Employer Phone</strong></td>
      <td><input name="EmployerPhone" type="text" class="tdHeader" id="EmployerPhone" size="20" maxlength="50" /></td>
      </tr>
    <tr >
      <td class="tdHeaderBright"><strong>Date of Injury</strong></td>
      <td><input name="DateOfInjury" type="text" class="tdHeader" id="DateOfInjury" size="20" maxlength="50" /></td>
      <td >&nbsp;</td>
      <td>&nbsp;</td>
      </tr>
    <tr>
      <td class="tdHeaderBright">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="tdHeaderBright">&nbsp;</td>
      <td>&nbsp;</td>
      </tr>
    <tr class="tdHeaderBright">
      <td colspan="4" bgcolor="#FFFFFF" class="tdHeader"><ul>
        <li class="instructionsBig1">Who  is the  adjuster in charge of this case?
          <ul>
            <li class="titleSub2">Note: the adjuster may be the person you are talking to</li>
            </ul>
          </li>
        <li class="instructionsBig1">Phone and fax  number?</li>
        <li class="instructionsBig1">Case# and/or Authorization# (if applicable)?
          
  </li>
        </ul></td>
      </tr>
    <tr>
      <td class="tdHeaderBright"> Adjuster Name<br>      <input name="button" type="button" class="tdHeader" id="button" onClick="document.forms.at_intake.PayerPhone.value = document.forms.at_intake.MessagePhone.value; document.forms.at_intake.AdjusterName.value = document.forms.at_intake.MessageName.value; document.forms.at_intake.PayerName.value = document.forms.at_intake.MessageCompany.value; document.forms.at_intake.PayerFax.value = document.forms.at_intake.MessageFax.value;" value="Same As Person Caller"></td>
      <td><input name="AdjusterName" type=text class="tdHeader"  size="30" maxlength="100"></td>
      <td class="tdHeaderBright">Insurance <br>
        or Administrative<br>
        Company</td>
      <td><input name="PayerName" type=text class="tdHeader"  size="30" maxlength="100"></td>
      </tr>
    <tr>
      <td class="tdHeaderBright">Phone</td>
      <td><input name="PayerPhone" type=text class="tdHeader"  size="30" maxlength="50"></td>
      <td class="tdHeaderBright">Fax</td>
      <td><input name="PayerFax" type=text class="tdHeader"  size="30" maxlength="50"></td>
      </tr>
    <tr>
      <td class="tdHeaderBright">Claim #</td>
      <td><input name="CaseClaimNumber1" type=text class="tdHeader"  size="30" maxlength="100"></td>
      <td class="tdHeaderBright">Preauthorization#</td>
      <td><input name="PayerPreAuthorizationConfirmation" type=text class="tdHeader"  size="30" maxlength="100"></td>
      </tr>
    <tr class="tdHeaderBright">
      <td colspan="4" bgcolor="#FFFFFF" class="tdHeader"><ul>
        <li  class="instructionsBig1">Who  is the Ordering Physician? 
          
          </span>
          <ul>
            <li class="titleSub2">Note: the person you are talking to may be from the Physicians Office</li>
            </ul>
          </li>
        <li class="instructionsBig1">Their Phone and Fax#? </li>
        </ul></td>
      </tr>
    <tr>
      <td class="tdHeaderBright">Ordering Physician<br>
        Full Name</td>
      <td><input name="ReferringPhysicianName" type=text class="tdHeader"  size="30" maxlength="100"></td>
      <td class="tdHeaderBright">Phone</td>
      <td><input name="ReferringPhysicianPhone" type=text class="tdHeader"  size="30" maxlength="100"></td>
      </tr>
    <tr>
      <td class="tdHeaderBright">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="tdHeaderBright">Fax</td>
      <td><input name="ReferringPhysicianFax" type="text" class="tdHeader" id="ReferringPhysicianFax"  size="30" maxlength="100" /></td>
      </tr>
    <tr class="tdHeaderBright">
      <td colspan="4" bgcolor="#FFFFFF" class="tdHeader"><ul>
        <li  class="instructionsBig1">What procedure are you ordering?
          <ul>
            <li class="titleSub2">Note: Please add any description or Procedural CPT Code that is given. </li>
            </ul>
          </li>
        </ul></td>
      </tr>
    <tr>
      <td class="tdHeaderBright"> Procedure:</td>
      <td colspan="3"><textarea name="CPTText" cols="60" rows="4" class="titleSub2" onKeyDown="textAreaStop(this,500)"></textarea></td>
      </tr>
    <tr>
      <td colspan="4" bgcolor="#FFFFFF" class="tdHeaderBright"><ul>
        <li  class="instructionsBig1">How  would you like to be notified of the progress of this case, fax or email?<br />
          <br />
          </li>
        <li  class="instructionsBig1">Is there any additional information about this procedure?
          <ul>
            <li class="titleSub2">Note: Record additional information in the blue Notes field above.</li>
            </ul>
          </li>
        </ul></td>
      </tr>
    <tr>
      <td class="tdHeaderBright">Extra Notes</td>
      <td colspan="3"><textarea name="ExtraNotes" cols="60" rows="6" class="titleSub2" id="ExtraNotes" onkeydown="textAreaStop(this,500)"></textarea></td>
      </tr>
    <tr class="tdHeaderBright">
      <td colspan="4" bgcolor="#FFFFFF" class="tdHeader"><ul>
        <li  class="instructionsBig1"></li>
        <li  class="instructionsBig1"> </li>
        </ul>
        <ul>
          <li  class="instructionsBig1">Thank you for calling.  <strong>A  representative will contact you with the status of the case. Can I help you  with anything else today</strong>.</li>
          </ul></td>
      </tr>
    <tr>
      <td colspan="4" bgcolor="#FFFFFF" class="tdHeaderBright"><label>
        <input type="submit" name="button2" id="button2" value="Submit" />
        </label></td>
      </tr>
  </table>
</form>
</td>
<td valign="top" bgcolor="#333333">&nbsp;</td>
<td valign="top"><table border="1" cellpadding="5" cellspacing="0" class="certborder">
  <tr class="tdHeaderAlt">
    <td colspan="2" bgcolor="#FFFFFF" class="tdHeaderAlt">NextImage Information</td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" class="title">Phone</td>
    <td nowrap="nowrap" bgcolor="#FFFFFF" class="titleSub1">888-318-5111</td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" class="title">Fax</td>
    <td nowrap="nowrap" bgcolor="#FFFFFF" class="titleSub1">800-637-5164</td>
  </tr>
</table>
<br />
<br />

  <table border="1" cellpadding="5" cellspacing="0" class="certborder">
    <tr>
      <td colspan="2" bgcolor="#FFFFFF" class="tdHeaderAlt">Common Questions:</td>
    </tr>
    <tr class="tdHeaderAlt" >
      <td bgcolor="#FFFFFF" class="tdHeaderAlt">Question</td>
      <td bgcolor="#FFFFFF" class="tdHeaderAlt">Answer</td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" class="title">Can you give me the status of a Patient?</td>
      <td bgcolor="#FFFFFF" class="titleSub1">I can have one of our service reps call you back with that information. </td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" class="title">What imaging centers do you have near _____ zip?</td>
      <td bgcolor="#FFFFFF" class="titleSub1">I can have one of our service reps call you back with that information. </td>
    </tr>
  </table>
  <p>&nbsp;</p></td></tr></table>


<%
} else
{
	%>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" height="100%">
  <tr> 
    <td width="100%" height="100%"> 
      <div align="center"> 
        <table width="400" border="1" cellspacing="0" cellpadding="5" bordercolor="#333333" class="tableColor">
          <tr> 
    <td>
      <p><b><font face="Arial, Helvetica, sans-serif" size="4">We are currently running 
        routine maintenance on our system to ensure your data integrity. </font></b></p>
      <p><font face="Arial, Helvetica, sans-serif" size="3">We apologize for any 
        inconvenience. This site will be back up soon.</font></p>
              <p><font face="Arial, Helvetica, sans-serif" size="3">If you have any questions, 
                please feel free to call us at 888-318-5111</font></p>
<p align=center><img src="images/logo1.jpg" width="250" height="70"></p>
      </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>

    
    <%
}
%>