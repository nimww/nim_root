<%@page language="java" contentType="application/json; charset=UTF-8" import="java.io.*,com.winstaff.*, java.util.*, java.sql.*,com.google.gson.Gson;"%>
<%!class N4_latLon {
		double lat;
		double lon;
		public N4_latLon( double lat,double lon) {
			this.lat = lat;
			this.lon = lon;
		}
		public double getLat() {
			return lat;
		}
		public void setLat(double lat) {
			this.lat = lat;
		}
		public double getLon() {
			return lon;
		}
		public void setLon(double lon) {
			this.lon = lon;
		}
	}
%>
<%
/* '" + search + "%' */
	List<N4_latLon> ll = new ArrayList<N4_latLon>();
	if (request.getParameter("term") != null && !request.getParameter("term").isEmpty()) {
		String search = request.getParameter("term");
		String query = "select lat, lon\n" +
				"from zip_code\n" +
				"where zip_code = '" + search + "'";
		searchDB2 db = new searchDB2();
		ResultSet rs = db.executeStatement(query);
		try {
			while (rs.next()) {
				ll.add(new N4_latLon(rs.getDouble(1),rs.getDouble(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closeAll();
		}
	}
	response.setHeader("Access-Control-Allow-Origin", "*");
	out.println(new Gson().toJson(ll));
%>
