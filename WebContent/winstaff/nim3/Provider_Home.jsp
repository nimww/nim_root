<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
</head>
<SCRIPT language="JavaScript1.2">
function openStatus()
{
	testwindow= window.open ("Home_Status.jsp", "HomeStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(0,0);
}

function openAuto(myAuto)
{
	testwindow= window.open ("report_auto2.jsp?rid=" + myAuto + "&MREF=300", "AutoStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(100,100);
}

</SCRIPT>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler) 
    {
        accessValid = true;
    }
    else if (pageControllerHash.containsKey("iAdjusterID")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID        =    (Integer)pageControllerHash.get("iAdjusterID");
		isAdjuster = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID2")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster2 = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID3")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster3 = true;
        accessValid = true;
	}
	
	
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	
		if (isScheduler)
	  {
		  String theScheduler = CurrentUserAccount.getUserID().toString();
		  String myAT_Where = "";
		  if ( request.getParameter("theScheduler")!=null&&!request.getParameter("theScheduler").equalsIgnoreCase("") )
		  {
			  theScheduler = request.getParameter("theScheduler");
		  }
		  if (!theScheduler.equalsIgnoreCase("0"))
		  {
			  myAT_Where =  " AND tNIM3_CaseAccount.assignedtoid = " + theScheduler + " ";
		  }
		  
	  %>
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td ><p class="title">Welcome to NextImage Grid Version 3.6</p></td>
      </tr>
      <tr>
        <td ><table width="100%" border="2" cellspacing="0" cellpadding="10">
         <%
		 if (isScheduler2)
		 {
			 %>
          <tr class="tdHeaderAlt">
            <td colspan="2" valign="top"><form action="#" method="POST" name="SchedulerSelect" class="tdHeader">
              Welcome Scheduler Admin (L2+). You are viewing the worklist for:
                  <select name="theScheduler" class="titleSub1" onChange="document.forms.SchedulerSelect.submit()">
                <option value="0">Show All</option>
                <%
{
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("select Userid,LogonUserName, contactfirstname, contactlastname from tUserAccount where accounttype ='SchedulerID' or accounttype='SchedulerID2' or accounttype='SchedulerID3' order by LogonUserName");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
				if (myRS.getString("UserID").equalsIgnoreCase(theScheduler))
				{
					%>
                <option selected value="<%=myRS.getString("UserID")%>"><%=myRS.getString("LogonUserName")%> [<%=myRS.getString("contactfirstname")%>, <%=myRS.getString("contactlastname")%>]</option>
                <%
				}
				else
				{
					%>
                <option  value="<%=myRS.getString("UserID")%>"><%=myRS.getString("LogonUserName")%> [<%=myRS.getString("contactfirstname")%>, <%=myRS.getString("contactlastname")%>]</option>
                <%
				}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
%>
              </select>
            </form><input name="Button" type="button" class="inputButton_md_Test" id="Button" onClick="openStatus()" value="Open Status Window">&nbsp;&nbsp;&nbsp;<input name="Button2" type="button" class="inputButton_md_Test" id="Button2" onClick="openAuto('volume1')" value="Open Daily Chart"></td>
            </tr>
           <%}%> 
          <tr>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr class="tdHeaderAlt">
                <td class="tdHeaderAltDark">Updates</td>
                </tr>
              <tr >
                <td class="tdBase"><ul>
                  <li>Server: <%=ConfigurationInformation.serverName%> [<%=ConfigurationInformation.applicationVersion%>]</li>
                  <li>New in 3.6
                    <ul>
                      <li>New Home Page Worklist Launched</li>
                      <li>Next Action Date Added</li>
                      <li>Problem Workflow</li>
                      <li>New UI</li>
                      </ul>
                  </li>
                  </ul>
                  <span class="requiredField">For any questions or tech support, please call us at 888-318-5111 ext. 101.</span></td>
                </tr>
              </table></td>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr class="tdHeaderAlt">
                <td colspan="3" class="tdHeaderAltDark">Next Action Alerts - Due</td>
                </tr>
              <tr class="tdHeaderAlt">
                <td>Name</td>
                <td>ScanPass</td>
                <td>Action Due</td>
                </tr>
              <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_NAD,myAT_Where); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);

		String theClass2 = "tdBase";
		if (myEO.NIM3_Encounter.getNextActionDate().before(PLCUtils.getYesterday()))
		{
			theClass2 = "tdBaseAlt_Action3";
		}
		else if (myEO.NIM3_Encounter.getNextActionDate().before(PLCUtils.getToday()))
		{
			theClass2 = "tdBaseAlt_Action2";
		}
			
			%>
              <tr >
                <td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_CaseAccount.getPatientLastName()%>, <%=myEO.NIM3_CaseAccount.getPatientFirstName()%></a></td>
                <td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_Encounter.getScanPass()%></a></td>
                <td  class=<%=theClass2%>><%=PLCUtils.getDisplayDateWithTime(myEO.NIM3_Encounter.getNextActionDate())%></td>
                </tr>
              <%
	}
}
				%>
              </table></td>
            </tr>
          <tr>
            <td valign="top"><table width="100%" border="1" cellspacing="0" cellpadding="1">
              <tr class="tdHeaderAlt">
                <td colspan="8" class="tdHeaderAltDark">Needs to be Scheduled</td>
                </tr>
              <tr class="tdHeaderAlt">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Payer</td>
                <td>Scheduler</td>
                <td>Patient</td>
                <td>ScanPass</td>
                <td>Status</td>
                <td>Hours Since Rec</td>
                </tr>
              <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_NOT_SCHEDULED,myAT_Where); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);
		long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myEO.NIM3_Referral.getReceiveDate().getTime())/(60*60*1000);
		String theClass2 = "tdBase";
		if (iHoursPassed<2)
		{
			theClass2 = "tdBaseFade1";
		}
		else if (iHoursPassed<4)
		{
			theClass2 = "tdBase";
		}
		else if (iHoursPassed<24)
		{
			theClass2 = "tdBaseAlt_Action1";
		}
		else if (iHoursPassed<48)
		{
			theClass2 = "tdBaseAlt_Action2";
		}
		else
		{
			theClass2 = "tdBaseAlt_Action3";
		}
		String NADClass = "tdBase";
		String NADImg = "images/action.gif";
		if (myEO.NIM3_Encounter.getNextActionDate().before(PLCUtils.getToday()))
		{
			//NADClass = "tdBaseAlt_Action3";
			NADImg = "images/no-action.gif";
		}
		String NADNotes = "Due: " + PLCUtils.getDisplayDateWithTime(myEO.NIM3_Encounter.getNextActionDate());
		if (myEO.NIM3_Encounter.getNextActionDate().before(NIMUtils.getBeforeTime()))
		{
			NADNotes = "No Action Date Set";
		}
		else
		{
			NADNotes += "\\nAction:\\n" + DataControlUtils.Text2OneLine(myEO.NIM3_Encounter.getNextActionNotes());
		}
		
		String tempStatus = "";
		if (NIMUtils.isEncounterReadyToSchedule(iEncounterID,true, true))
		{
			tempStatus = "Ready to Schedule";
		}
		else
		{
			tempStatus = "Incomplete Data";
			if (myEO.NIM3_CaseAccount.getPatientCellPhone().equalsIgnoreCase("")&&myEO.NIM3_CaseAccount.getPatientHomePhone().equalsIgnoreCase(""))
			{
				tempStatus += " | Missing Patient Phone #";
			}
			if (myEO.NIM3_Referral.getRxFileID()==0)
			{
				tempStatus += " | No RX";
			}
			else if (myEO.NIM3_Referral.getRxFileID()==NIMUtils.EncounterBypassRXID)
			{
				tempStatus += " | Using Bypass RX";
			}
			if (!myEO.isPreScreenComplete())
			{
				tempStatus += " | PreScreen Incomplete";
			}
			if (!myEO.isRxReviewed())
			{
				tempStatus += " | No Rx Review";
			}
			
		}		
		
		
		%>
		  <tr >
                <td align="center" class="<%=NADClass%>"><img src="<%=NADImg%>" onClick="alert('<%=NADNotes%>');"></td>
            <td align="center"><% if (myEO.NIM3_Encounter.getisSTAT()==1){out.println("<span class=\"tdBaseAltRed\">STAT</span>");}else{%>&nbsp;<%}%></td>
            <td><%=myEO.NIM3_PayerMaster.getPayerName()%></td>
            <td><%=myEO.Case_AssignedTo.getContactFirstName()%></td>
			<td nowrap><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_CaseAccount.getPatientLastName()%>, <%=myEO.NIM3_CaseAccount.getPatientFirstName()%></a></td>
			<td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_Encounter.getScanPass()%></a></td>
			<td ><%=tempStatus%></td>
			<td class=<%=theClass2%>><% if (iHoursPassed>10000){out.print("Invalid Receive Date");}else{out.print(iHoursPassed + " hours");}%></td>

			</tr>
		  <%
	}

}
				%>
              </table></td>
            <td valign="top"><table width="100%" border="1" cellspacing="0" cellpadding="2">
              <tr class="tdHeaderAlt">
                <td colspan="7" class="tdHeaderAltDark">Appointment Follow-up &amp; Report Delivery</td>
                </tr>
              <tr class="tdHeaderAlt">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Scheduler</td>
                <td>Name</td>
                <td>ScanPass</td>
                <td>Status</td>
                <td>Hours</td>
                </tr>
              <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_REPORTS_DUE,myAT_Where); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);
		long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myEO.NIM3_Encounter.getDateOfService().getTime())/(60*60*1000);
		long iHoursPassedRpReview = (PLCUtils.getNowDate(false).getTime()-myEO.NIM3_Encounter.getTimeTrack_ReqDelivered().getTime())/(60*60*1000);
		String sHoursPassed = iHoursPassed + " hours since DOS";
		if (iHoursPassed<=0)
		{
			sHoursPassed = "Appt in "+-iHoursPassed+" hours";
		}
		boolean isShow = true;
		if ( (!myEO.isScanPassAlertsComplete(2)||myEO.NIM3_Encounter.getSentTo_SP_IC().before(NIMUtils.getBeforeTime()))&& (myEO.NIM3_Encounter.getReportFileID().intValue()>0)&&myEO.isScanPassAlertsComplete(3) )
		{
			isShow = false;
		}

		String sStatus = "";

		if (iHoursPassed<0&&myEO.NIM3_Encounter.getReportFileID().intValue()==0)
		{
			sStatus = "ScanPass not sent";
		}
		else if (iHoursPassed<0&&myEO.NIM3_Encounter.getReportFileID().intValue()>0)
		{
			sStatus = "Warning: Report before appointment";
		}
		else if (myEO.NIM3_Encounter.getReportFileID().intValue()==0)
		{
			sStatus = "No Report on file";
		}
		else if (myEO.NIM3_Encounter.getTimeTrack_ReqRpReview().before(NIMUtils.getBeforeTime()))
		{
			sStatus = "Pending Report Review";
			sHoursPassed += "<br><strong>" + iHoursPassedRpReview + " since report u/l</strong>";
		}
		else if (false&&(myEO.isScanPassAlertsComplete(2)&&!myEO.NIM3_Encounter.getSentTo_SP_IC().before(NIMUtils.getBeforeTime())&&myEO.NIM3_Referral.getReferralStatusID()==1))
		{
			sStatus = "*";
			isShow=false;
		}
		else  if (!myEO.isScanPassAlertsComplete(3)&&myEO.NIM3_Encounter.getReportFileID().intValue()==0)
		{
			sStatus = "Report on file, Confirmations <u>not</u> sent";
			sHoursPassed += "<br><strong>" + iHoursPassedRpReview + " since report u/l</strong>";
		}
		else  if (myEO.NIM3_Referral.getReferralStatusID()!=1)
		{
			sStatus = "*";
		}
		else  
		{
			sStatus = "* [See Andy]";
		}
		
		if (myEO.NIM3_Encounter.getReportFileID().intValue()==0&&myEO.NIM3_Encounter.getSentTo_SP_IC().before(NIMUtils.getBeforeTime()))
		{
			sStatus += " | Warning: SP Not Sent to Imaging Center" ;
		}
		if (myEO.NIM3_Referral.getReferralStatusID()!=1)
		{
			sStatus += " | Warning: Referral is Pending Authorization";
		}
		



		if (isShow)
		{
			String theClass2 = "tdBase";
			if (iHoursPassed<0)
			{
				theClass2 = "tdBaseAlt_Action3";
			}
			else if (iHoursPassed<4&&myEO.NIM3_Encounter.getReportFileID().intValue()==0)
			{
				theClass2 = "tdBaseFade1";
			}
			else if (iHoursPassed<8&&myEO.NIM3_Encounter.getReportFileID().intValue()==0)
			{
				theClass2 = "tdBase";
			}
			else if (iHoursPassed<12&&myEO.NIM3_Encounter.getReportFileID().intValue()==0)
			{
				theClass2 = "tdBaseAlt_Action1";
			}
			else if (iHoursPassed<24)
			{
				theClass2 = "tdBaseAlt_Action2";
			}
			else
			{
				theClass2 = "tdBaseAlt_Action3";
			}
			String NADNotes = "Due: " + PLCUtils.getDisplayDateWithTime(myEO.NIM3_Encounter.getNextActionDate());
			if (myEO.NIM3_Encounter.getNextActionDate().before(NIMUtils.getBeforeTime()))
			{
				NADNotes = "No Action Date Set";
			}
			else
			{
				NADNotes += "\\nAction:\\n" + DataControlUtils.Text2OneLine(myEO.NIM3_Encounter.getNextActionNotes());
			}
			
			String NADClass = "tdBase";
			String NADImg = "images/action.gif";
			if (myEO.NIM3_Encounter.getNextActionDate().before(PLCUtils.getToday()))
			{
				//NADClass = "tdBaseAlt_Action3";
				NADImg = "images/no-action.gif";
			}
				
				%>
				  <tr >
					<td align="center" class="<%=NADClass%>"><img src="<%=NADImg%>" onClick="alert('<%=NADNotes%>');"></td>
					<td align="center"><% if (myEO.NIM3_Encounter.getisSTAT()==1){out.println("<span class=\"tdBaseAltRed\">STAT</span>");}%></td>
		            <td><%=myEO.Case_AssignedTo.getContactFirstName()%></td>
					<td nowrap><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_CaseAccount.getPatientLastName()%>, <%=myEO.NIM3_CaseAccount.getPatientFirstName()%></a></td>
					<td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_Encounter.getScanPass()%></a></td>
					<td class=<%=theClass2%>><%=sStatus%></td>
					<td class=<%=theClass2%>><%=sHoursPassed%></td>
					</tr>
				  <%
			}
		}
}
				%>
              </table></td>
            </tr>
          <tr>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr class="tdHeaderAlt">
                <td colspan="6" class="tdHeaderAltDark">Awaiting Rx Review (Not Processed By You)</td>
              </tr>
              <tr class="tdHeaderAlt">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Name</td>
                <td>ScanPass</td>
                <td>Status</td>
                <td>Hours Since Rec</td>
              </tr>
              <%
{				
//	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_RX_REVIEW," AND tNIM3_Encounter.TimeTrack_ReqProc_UserID<> " + theScheduler + " " ); 
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_RX_REVIEW,"" ); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		if (NIMUtils.isEncounterReadyToSchedule(iEncounterID,false, false))
		{
			NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);
			long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myEO.NIM3_Referral.getReceiveDate().getTime())/(60*60*1000);
			String theClass2 = "tdBase";
			if (iHoursPassed<2)
			{
				theClass2 = "tdBaseFade1";
			}
			else if (iHoursPassed<4)
			{
				theClass2 = "tdBase";
			}
			else if (iHoursPassed<24)
			{
				theClass2 = "tdBaseAlt_Action1";
			}
			else if (iHoursPassed<48)
			{
				theClass2 = "tdBaseAlt_Action2";
			}
			else
			{
				theClass2 = "tdBaseAlt_Action3";
			}
			
			
			%>
			  <tr >
                <td>&nbsp;</td>
	            <td align="center"><% if (myEO.NIM3_Encounter.getisSTAT()==1){out.println("<span class=\"tdBaseAltRed\">STAT</span>");}%></td>
				<td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_CaseAccount.getPatientLastName()%>, <%=myEO.NIM3_CaseAccount.getPatientFirstName()%></a></td>
				<td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_Encounter.getScanPass()%></a></td>
				<td >Ready for Review</td>
				<td class=<%=theClass2%>><%=iHoursPassed%> hours</td>
			  </tr>
			  <%
		}
		else
		{
			//skip only show those that are ready to review.
		}
	}
}
				%>
            </table></td>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr class="tdHeaderAlt">
                <td colspan=2 class="tdHeaderAltDark">Open CommTracks
                  <input name="button2" type="button" onClick="document.location='tNIM3_CommTrack_query_all.jsp';" class="inputButton_md_Action1" id="button2" value="View CommTrack" /></td>
              </tr>
              <tr class="tdHeaderAlt">
                <td nowrap>Name</td>
                <td nowrap >Hours Since Rec</td>
              </tr>
              <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_COMMTRACK,myAT_Where); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iCommTrackID = (Integer) myWorklist_Vector.elementAt(i);
		bltNIM3_CommTrack myCT = new bltNIM3_CommTrack(iCommTrackID);
		long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myCT.getCommStart().getTime())/(60*60*1000);
		String theClass2 = "tdBase";
		if (iHoursPassed<1)
		{
			theClass2 = "tdBaseFade1";
		}
		else if (iHoursPassed<3)
		{
			theClass2 = "tdBase";
		}
		else if (iHoursPassed<6)
		{
			theClass2 = "tdBaseAlt_Action1";
		}
		else if (iHoursPassed<12)
		{
			theClass2 = "tdBaseAlt_Action2";
		}
		else
		{
			theClass2 = "tdBaseAlt_Action3";
		}
		
		
		%>
		  <tr >
			<td><%=myCT.getMessageName()%> @ <%=myCT.getMessageCompany()%></td>
			<td  class=<%=theClass2%>><%=iHoursPassed%> hours</td>
		  </tr>
		  <%
	}
}
				%>
            </table></td>
            </tr>
          </table></td>
      </tr>
      <tr class="tdHeaderAlt" >
        <td ><span class="tdHeader"><hr />
          </span>
          <p class="title">          Administrative Options</p></td>
      </tr>
      <tr class="tdHeaderAlt" >
        <td ><span class="tdHeader">Reports:</span></td>
      </tr>
      <tr>
        <td bgcolor="#DAE0E4"><input name="button3" type="submit" class="inputButton_lg_Default" id="button3" onClick="this.disabled = true; document.location='Reports_Home.jsp';" value="Reports..." /></td>
        </tr>
      <%
	  if (isScheduler2)
	  {
	  %>
      <tr class="tdHeaderAlt" >
        <td ><span class="tdHeader">
          <hr />        
          Maintenance Options:</span></td>
      </tr>
      <tr  bgcolor="#DAE0E4">
        <td ><hr />
          &nbsp;
          <input name="button" type="submit" class="inputButton_lg_Default" id="button" onClick="this.disabled = true; document.location='Maint_Home.jsp';" value="Maintenance/Settings..." /></td>
      </tr>
      <%
	  }
	  if (isScheduler3)
	  {
	  %>
      <tr  bgcolor="#DAE0E4">
        <td ><hr />
          &nbsp;
          <input name="button" type="submit" class="inputButton_lg_Default" id="button" onClick="this.disabled = true; document.location='Billing_Home.jsp';" value="Billing Home..." /></td>
      </tr>
      <%
	  }
	  else
	  {
		  %>
          <%
	  }
	  %>
      <tr>
        <td >&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
      <%
	  }
	  else if (isAdjuster||isAdjuster2||isAdjuster3)
	  {
		  %>
<table width="<%=MasterTableWidth%>" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td>
    	<table width="100%" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class=title>Welcome to NextImage Medical.</td>
          </tr>
        <tr>
          <td colspan="2" align="left" class="tdHeader"><input name="button7" type="button" onClick="document.location='Payer_submit1.jsp';" class="inputButton_lg_Default" id="button7" value="Submit New Referral" /></td>
        </tr>
        <tr>
          <td colspan="2" align="left" class="tdHeader">10 Most Recent Cases</td>
          </tr>
          <td colspan="2" align="center"  class="borderHighlightBlackSolid"><table width="100%" border="0" cellspacing="0" cellpadding="4">
              <tr class="tdHeaderAlt">
                <td>Action</td>
                <td>Claim #</td>
                <td>First Name</td>
                <td>Last Name</td>
                <td>Submitted</td>
              </tr>
              
              
  <!-- test -->
  <%
searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
	String mySQL = "null";
	String mySQL2 = "null";
	if (isAdjuster)
	{
		mySQL = "select tNIM3_CaseAccount.caseID, tNIM3_Referral.receivedate from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where (tNIM3_CaseAccount.adjusterid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.nursecasemanagerid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministratorid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator2id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator3id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator4id = " + CurrentUserAccount.getUserID() + ") and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by referraldate desc LIMIT 10";
	}
	else if (isAdjuster2)
	{
		mySQL = "select tNIM3_CaseAccount.caseID, tNIM3_Referral.receivedate from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where tNIM3_CaseAccount.PayerID =" + iPayerID + " and (tNIM3_CaseAccount.adjusterid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.adjusterid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) )  and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by referraldate desc LIMIT 10";
	}
	else if (isAdjuster3)
	{
		mySQL = "select tNIM3_CaseAccount.caseID, tNIM3_Referral.receivedate from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where tNIM3_CaseAccount.PayerID =" + iPayerID + " and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by referraldate desc LIMIT 10";
	}
//out.println(mySQL);
	myRS = mySS.executeStatement(mySQL);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {

	bltNIM3_CaseAccount working_bltNIM3_CaseAccount  =  new bltNIM3_CaseAccount(new Integer(myRS.getString("CaseID")));
	//bltUserAccount myAdj = new bltUserAccount(working_bltNIM3_CaseAccount.getAdjusterID());
	bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(working_bltNIM3_CaseAccount.getPayerID());
	//bltNIM3_PatientAccount working_bltNIM3_PatientAccount  =  new bltNIM3_PatientAccount(working_bltNIM3_CaseAccount.getPatientAccountID());
	//bltNIM3_EmployerAccount working_bltNIM3_EmployerAccount  =  new bltNIM3_EmployerAccount(working_bltNIM3_PatientAccount.getEmployerID());
	cnt++;
	//if (cnt>=startID&&cnt<=startID+maxResults)
if (cnt<=10)
{
	cnt2++;
	
	String myClass = "tdBase";
	if (working_bltNIM3_CaseAccount.getCaseClaimNumber().equalsIgnoreCase(""))
	{
		myClass = "requiredFieldMain";
	}
	else if (cnt2%2!=0)
	{
		myClass = "tdBaseAlt";
	}
	
	out.print("<tr class="+myClass+">");

/*	else if (false)
	{
		out.print("<td><input class=\"inputButton_md_Default\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=edit&EDITID=" + working_bltNIM3_CaseAccount.getCaseID() + "&KM=p'\" value=\"Edit\">");
//	out.print("&nbsp;<input class=\"inputButton_md_Default\" value=\"Msg\"  type=button onClick = \"this.disabled=false;modalPost('PatientAccountID', modalWin('tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=viewcomm&EDITID=" + working_bltNIM3_CaseAccount.getCaseID() + "&KM=p','MsgWindow','dialogWidth:900px;dialogHeight:400px','status=yes,scrollbars=yes,resizable=yes,width=800,height=400'))\">");
		out.print("&nbsp;<input class=\"inputButton_md_Default\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=viewreports&EDITID=" + working_bltNIM3_CaseAccount.getCaseID() + "&KM=p'\" value=\"Reports\"></td>");
	}
*/

	if (isAdjuster||isAdjuster2||isAdjuster3)
	{
	out.print("<td align=center  ><input class=\"inputButton_md_Default\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=viewpayer&EDITID=" + working_bltNIM3_CaseAccount.getCaseID() + "&KM=p'\" value=\"View Details\"></td>");
	}
	out.print("<td>");
	{
		out.print(working_bltNIM3_CaseAccount.getCaseClaimNumber() );
	}
	out.print("</td>");
	out.print("<td>");
	out.print(working_bltNIM3_CaseAccount.getPatientFirstName());
	out.print("</td>");
	out.print("<td>");
	out.print(working_bltNIM3_CaseAccount.getPatientLastName());
	out.print("</td>");
	out.print("<td>");
	out.print(displayDateSDF1.format(dbdf.parse(myRS.getString("ReceiveDate"))));
	out.print("</td>");
	out.print("</tr>");
}
   }
mySS.closeAll();
endCount = cnt;

}
catch(Exception eee)
{
	out.print("item list error: " + eee);
}

%>
              
              
              
  <!-- test -->            
              
              
          </table></td>
          </tr>
	    </table>
    </td>
  </tr>
</table>
          
          <%
	  }
	  if (isAdjuster2)
	  {
		  %>
<table  border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td>
    	<table width="100%" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td class=title>You are signed in as a Supervisor of the following users:</td>
          </tr>
        <tr>
          <td valign="top" class="borderHighlightBlackSolid"><table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr class="tdHeaderAlt">
              <td>Login</td>
              <td>First Name</td>
              <td>Last Name</td>
              </tr>
            
            
            <!-- test -->
            <%
searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
	String mySQL = "null";
	String mySQL2 = "null";
	if (isAdjuster2)
	{
		mySQL = "select logonusername, contactfirstname, contactlastname from tUserAccount where payerid =" + iPayerID + " and managerid = " + CurrentUserAccount.getUserID() + " order by contactlastname";
	}
//out.println(mySQL);
	myRS = mySS.executeStatement(mySQL);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {

	cnt++;
	//if (cnt>=startID&&cnt<=startID+maxResults)
if (cnt<=10)
{
	cnt2++;
	
	String myClass = "tdBase";
	if (cnt2%2==0)
	{
		myClass = "tdBaseAlt";
	}
	
	out.print("<tr class="+myClass+">");

	out.print("<td>");
	out.print(myRS.getString("LogonUserName"));
	out.print("</td>");
	out.print("<td>");
	out.print(myRS.getString("ContactFirstName"));
	out.print("</td>");
	out.print("<td>");
	out.print(myRS.getString("ContactLastName"));
	out.print("</td>");
	out.print("</tr>");
}
   }
mySS.closeAll();
endCount = cnt;

}
catch(Exception eee)
{
	out.print("item list error: " + eee);
}

%>
            
            
            
            <!-- test -->            
            
            
          </table></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
          
          <%
	  }
	  if (isAdjuster3)
	  {
		  %>
<table  border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td>
    	<table width="100%" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td class=title>You are signed in as a Manager of the following users:</td>
          </tr>
        <tr>
          <td valign="top" class="borderHighlightBlackSolid"><table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr class="tdHeaderAlt">
              <td>Login</td>
              <td>First Name</td>
              <td>Last Name</td>
              <td>Referrals</td>
              </tr>
            
            
            <!-- test -->
            <%
searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
	String mySQL = "null";
	String mySQL2 = "null";
	if (isAdjuster3)
	{
		mySQL = "SELECT tuseraccount.logonusername, tuseraccount.contactfirstname, tuseraccount.contactlastname, \"public\".\"vCaseCount_AdjusterID\".\"iCount\" FROM tuseraccount Left Join \"public\".\"vCaseCount_AdjusterID\" ON tuseraccount.userid = \"public\".\"vCaseCount_AdjusterID\".adjusterid where tUserAccount.payerid=" + iPayerID + " order by tuseraccount.contactlastname";
	}
//out.println(mySQL);
	myRS = mySS.executeStatement(mySQL);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {

	cnt++;
	//if (cnt>=startID&&cnt<=startID+maxResults)
if (cnt<=50)
{
	cnt2++;
	
	String myClass = "tdBase";
	if (cnt2%2==0)
	{
		myClass = "tdBaseAlt";
	}
	String myCount = myRS.getString("iCount");
	if (myCount==null||myCount.equalsIgnoreCase("null"))
	{
		myCount = "0";
	}
	
	out.print("<tr class="+myClass+">");

	out.print("<td>");
	out.print(myRS.getString("logonusername"));
	out.print("</td>");
	out.print("<td>");
	out.print(myRS.getString("contactfirstname"));
	out.print("</td>");
	out.print("<td>");
	out.print(myRS.getString("contactlastname"));
	out.print("</td>");
	out.print("<td>");
	out.print(myCount);
	out.print("</td>");
	out.print("</tr>");
}
   }
mySS.closeAll();
endCount = cnt;

}
catch(Exception eee)
{
	out.print("item list error: " + eee);
}

%>
            
            
            
            <!-- test -->            
            
            
          </table></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
          
          <%
	  }
	  %>

    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>