<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN"> 
<link rel="stylesheet" type="text/css" href="../app/js/anytime/anytime.css" />
    <script type="text/javascript" src="../app/js/prototype/prototype-1.6.0.3.js" ></script>
    <script type="text/javascript" src="../app/js/anytime/anytime.js" ></script>

<%@page contentType="text/html" language="java" import="    com.winstaff.*" %>
<%/*

    filename: out\jsp\tNIM3_Appointment_form.jsp
    Created on Jan/05/2009
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>


<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 >
    <tr><td width=10>&nbsp;</td><td>

<p class="title">Create an Appointment</p>

<%
//initial declaration of list class and parentID
    Integer        iAppointmentID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iAppointmentID")) 
    {
        iAppointmentID        =    (Integer)pageControllerHash.get("iAppointmentID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");



//    pageControllerHash.put("sLocalChildReturnPage","tImaging_schedule.jsp?nullParam=null&KM=p&EDIT=sched");

    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID

    bltNIM3_Appointment        NIM3_Appointment        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        NIM3_Appointment        =    new    bltNIM3_Appointment(iAppointmentID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_Appointment        =    new    bltNIM3_Appointment(UserSecurityGroupID, true);
    }

//fields
        %>
        <form action="tNIM3_Appointment_form_sub_PayerID.jsp" name="tNIM3_Appointment_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
        <table  border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 >
            <%
            if ( (NIM3_Appointment.isRequired("iStatus",UserSecurityGroupID))&&(!NIM3_Appointment.isComplete("iStatus")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Appointment.isExpired("iStatus",expiredDays))&&(NIM3_Appointment.isExpiredCheck("iStatus",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_Appointment.isWrite("iStatus",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>iStatus&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="iStatus" value="<%=NIM3_Appointment.getiStatus()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=iStatus&amp;sTableName=tNIM3_Appointment&amp;sRefID=<%=NIM3_Appointment.getAppointmentID()%>&amp;sFieldNameDisp=<%=NIM3_Appointment.getEnglish("iStatus")%>&amp;sTableNameDisp=tNIM3_Appointment','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_Appointment.isRead("iStatus",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>iStatus&nbsp;</b></p></td><td valign=top><p><%=NIM3_Appointment.getiStatus()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=iStatus&amp;sTableName=tNIM3_Appointment&amp;sRefID=<%=NIM3_Appointment.getAppointmentID()%>&amp;sFieldNameDisp=<%=NIM3_Appointment.getEnglish("iStatus")%>&amp;sTableNameDisp=tNIM3_Appointment','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_Appointment.isRequired("AppointmentTime",UserSecurityGroupID))&&(!NIM3_Appointment.isComplete("AppointmentTime")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Appointment.isExpired("AppointmentTime",expiredDays))&&(NIM3_Appointment.isExpiredCheck("AppointmentTime",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM3_Appointment.isWrite("AppointmentTime",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Appointment Time:&nbsp;</b></p></td><td valign=top><p>
<input maxlength=20 class="inputButton_lg_Action1"  type=text size="40" name="AppointmentTime"  id="AppointmentTime" value='Click Here...' id="f_date_b"  >     <script type="text/javascript">
    new ATWidget( "AppointmentTime", { format: "%m/%d/%Y %h:%i %p"} );
                           </script>





</p>
                              	
						
						</td></tr>
                        <%
            }
            else if ((NIM3_Appointment.isRead("AppointmentTime",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>AppointmentTime&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM3_Appointment.getAppointmentTime())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AppointmentTime&amp;sTableName=tNIM3_Appointment&amp;sRefID=<%=NIM3_Appointment.getAppointmentID()%>&amp;sFieldNameDisp=<%=NIM3_Appointment.getEnglish("AppointmentTime")%>&amp;sTableNameDisp=tNIM3_Appointment','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_Appointment.isRequired("AppointmentConfirmation",UserSecurityGroupID))&&(!NIM3_Appointment.isComplete("AppointmentConfirmation")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Appointment.isExpired("AppointmentConfirmation",expiredDays))&&(NIM3_Appointment.isExpiredCheck("AppointmentConfirmation",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_Appointment.isWrite("AppointmentConfirmation",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>AppointmentConfirmation&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="AppointmentConfirmation" value="<%=NIM3_Appointment.getAppointmentConfirmation()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AppointmentConfirmation&amp;sTableName=tNIM3_Appointment&amp;sRefID=<%=NIM3_Appointment.getAppointmentID()%>&amp;sFieldNameDisp=<%=NIM3_Appointment.getEnglish("AppointmentConfirmation")%>&amp;sTableNameDisp=tNIM3_Appointment','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_Appointment.isRead("AppointmentConfirmation",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>AppointmentConfirmation&nbsp;</b></p></td><td valign=top><p><%=NIM3_Appointment.getAppointmentConfirmation()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AppointmentConfirmation&amp;sTableName=tNIM3_Appointment&amp;sRefID=<%=NIM3_Appointment.getAppointmentID()%>&amp;sFieldNameDisp=<%=NIM3_Appointment.getEnglish("AppointmentConfirmation")%>&amp;sTableNameDisp=tNIM3_Appointment','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_Appointment.isRequired("Scheduler",UserSecurityGroupID))&&(!NIM3_Appointment.isComplete("Scheduler")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Appointment.isExpired("Scheduler",expiredDays))&&(NIM3_Appointment.isExpiredCheck("Scheduler",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Appointment.isWrite("Scheduler",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Scheduler&nbsp;</b></p></td><td valign=top><p><input name="Scheduler" type=text class="inputButton_md_Default" value="<%=CurrentUserAccount.getLogonUserName()%>" size="20" maxlength="100" readonly="readonly">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Scheduler&amp;sTableName=tNIM3_Appointment&amp;sRefID=<%=NIM3_Appointment.getAppointmentID()%>&amp;sFieldNameDisp=<%=NIM3_Appointment.getEnglish("Scheduler")%>&amp;sTableNameDisp=tNIM3_Appointment','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Appointment.isRead("Scheduler",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Scheduler&nbsp;</b></p></td><td valign=top><p><%=NIM3_Appointment.getScheduler()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Scheduler&amp;sTableName=tNIM3_Appointment&amp;sRefID=<%=NIM3_Appointment.getAppointmentID()%>&amp;sFieldNameDisp=<%=NIM3_Appointment.getEnglish("Scheduler")%>&amp;sTableNameDisp=tNIM3_Appointment','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_Appointment.isRequired("Comments",UserSecurityGroupID))&&(!NIM3_Appointment.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_Appointment.isExpired("Comments",expiredDays))&&(NIM3_Appointment.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_Appointment.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Comments&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="Comments" value="<%=NIM3_Appointment.getComments()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Appointment&amp;sRefID=<%=NIM3_Appointment.getAppointmentID()%>&amp;sFieldNameDisp=<%=NIM3_Appointment.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Appointment','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_Appointment.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Comments&nbsp;</b></p></td><td valign=top><p><%=NIM3_Appointment.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_Appointment&amp;sRefID=<%=NIM3_Appointment.getAppointmentID()%>&amp;sFieldNameDisp=<%=NIM3_Appointment.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_Appointment','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
             <%
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
              {
              %>
                  <table width=75% border=1 bordercolor=333333 align=left cellspacing=0 cellpadding=0 class=wizardTable>
                  <tr class=requiredField><td>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="next" name="INTNext" checked>&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoMore","tNIM3_Appointment")%>
                  <br>
                  <input  <%=HTMLFormStyleButton%> type="radio" value="yes" name="INTNext">&nbsp;&nbsp;<%=ConfigurationMessages.getInterviewMessage("INTERVIEWAddMore","tNIM3_Appointment")%>
                  </td></tr></table><br><br><br>
              <%
              }
              %>
            <p><input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


