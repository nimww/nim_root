<%@page language="java" contentType="application/json; charset=UTF-8" import="java.io.*,com.winstaff.*, java.util.*, java.sql.*,com.google.gson.Gson;"%>
<%!class N4_DupSearch {
		String en_id;
		String ca_id;
		String scanpass;
		String caseclaimnumber;
		String patientfirstname;
		String patientlastname;
		String patientdob;
		public N4_DupSearch(String en_id, String ca_id, String scanpass,String caseclaimnumber, String patientfirstname,String patientlastname, String patientdob) {
			this.en_id =  en_id;
			this.ca_id = ca_id;
			this.scanpass = scanpass;
			this.caseclaimnumber = caseclaimnumber;
			this.patientfirstname = patientfirstname;
			this.patientlastname = patientlastname;
			this.patientdob = patientdob;
		}
		public String getEn_id() {
			return en_id;
		}
		public void setEn_id(String en_id) {
			this.en_id = en_id;
		}
		public String getCa_id() {
			return ca_id;
		}
		public void setCa_id(String ca_id) {
			this.ca_id = ca_id;
		}
		public String getScanpass() {
			return scanpass;
		}
		public void setScanpass(String scanpass) {
			this.scanpass = scanpass;
		}
		public String getCaseclaimnumber() {
			return caseclaimnumber;
		}
		public void setCaseclaimnumber(String caseclaimnumber) {
			this.caseclaimnumber = caseclaimnumber;
		}
		public String getPatientfirstname() {
			return patientfirstname;
		}
		public void setPatientfirstname(String patientfirstname) {
			this.patientfirstname = patientfirstname;
		}
		public String getPatientlastname() {
			return patientlastname;
		}
		public void setPatientlastname(String patientlastname) {
			this.patientlastname = patientlastname;
		}
		public String getPatientdob() {
			return patientdob;
		}
		public void setPatientdob(String patientdob) {
			this.patientdob = patientdob;
		}
	}
%>
<%
/* '" + search + "%' */
	List<N4_DupSearch> ds = new ArrayList<N4_DupSearch>();
	if (request.getParameter("term") != null && !request.getParameter("term").isEmpty()) {
		String search = request.getParameter("term");
		String query = "SELECT\n" +
				"	en.scanpass,\n" +
				"	ca.caseclaimnumber,\n" +
				"	ca.patientfirstname,\n" +
				"	ca.patientlastname,\n" +
				"	ca.patientdob,\n" +
				"	ca.caseid,\n" +
				"	en.encounterid\n" +
				"FROM\n" +
				"	tnim3_encounter en\n" +
				"INNER JOIN tnim3_referral rf ON rf.referralid = en.referralid\n" +
				"INNER JOIN tnim3_caseaccount ca ON ca.caseid = rf.caseid\n" +
				"WHERE\n" +
				"	(\n" +
				"		TRIM (ca.caseclaimnumber) != ''\n" +
				"		OR ca.caseclaimnumber IS NOT NULL\n" +
				"		OR TRIM (en.scanpass) != ''\n" +
				"		OR en.scanpass IS NOT NULL\n" +
				"		OR TRIM (ca.patientfirstname) != ''\n" +
				"		OR ca.patientfirstname IS NOT NULL\n" +
				"		OR TRIM (ca.patientlastname) != ''\n" +
				"		OR ca.patientlastname IS NOT NULL\n" +
				"		OR ca.patientdob != '1800-01-01'\n" +
				"		OR ca.patientdob IS NOT NULL\n" +
				"	)\n" +
				"AND LOWER (\n" +
				"	en.scanpass || ' ' || ca.patientfirstname || ' ' || ca.patientlastname || ' ' || ca.patientdob :: TEXT || ' ' || ca.caseclaimnumber\n" +
				") ~ LOWER ('alejan')\n" +
				"LIMIT 35";
		searchDB2 db = new searchDB2();
		ResultSet rs = db.executeStatement(query);
		try {
			while (rs.next()) {
				ds.add(new N4_DupSearch(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closeAll();
		}
	}
	response.setHeader("Access-Control-Allow-Origin", "*");
	out.println(new Gson().toJson(ds));
%>
