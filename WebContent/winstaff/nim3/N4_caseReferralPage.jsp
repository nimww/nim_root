<%@page import="java.io.*,com.winstaff.*, java.util.*, java.text.SimpleDateFormat,com.google.gson.Gson;"%>
<%!class incomingService {
		public String cpt = "";
		public String bp = "";
		public String qty = "";
		public String cptModifier = "";
		public String serviceStatus = "";
		public String sid = "";
		public incomingService(String cpt, String bp, String qty,String cptModifier, String serviceStatus, String sid) {
			this.cpt = cpt;
			this.bp = bp;
			this.qty = qty;
			this.cptModifier = cptModifier;
			this.serviceStatus = serviceStatus;
			this.sid = sid;
		}
		public String getCpt() {
			return cpt;
		}
		public void setCpt(String cpt) {
			this.cpt = cpt;
		}
		public String getBp() {
			return bp;
		}
		public void setBp(String bp) {
			this.bp = bp;
		}
		public String getQty() {
			return qty;
		}
		public void setQty(String qty) {
			this.qty = qty;
		}
		public String getCptModifier() {
			return cptModifier;
		}
		public void setCptModifier(String cptModifier) {
			this.cptModifier = cptModifier;
		}
		public String getServiceStatus() {
			return serviceStatus;
		}
		public void setServiceStatus(String serviceStatus) {
			this.serviceStatus = serviceStatus;
		}
		public String getSid() {
			return sid;
		}
		public void setSid(String sid) {
			this.sid = sid;
		}
	}
	%>
	<%
	String scheduler = "";
	String ca_id = "";
	String rf_id = "";
	String en_id = "";
	String NIM3_referral_source = "";
	String REFERRAL_STATUS = "";
	String REFERRAL_METHOD = "";
	String ENCOUNTER_TYPE = "";
	String REFERRAL_SOURCE = "";
	String REFERRAL_NOTES = "";
	String ENCOUNTER_NOTES = "";
	String NIM3_mdID = "";
	String patientavailability = "";
	
	String attorneyfirstname = "";
	String attorneylastname = "";
	String attorneyphone = "";
	String attorneyfax = "";
	String INJURY_DESCRIPTION = "";
	
	
	
	String metalinbody = "";
	String metalinbodydescreption = "";
	String previousimage = "";
	String previousimagedescreption = "";
	String pregnant = "";
	String pregnantdescreption = "";
	String requirehandcarrycd = "";
	String requiresfilms = "";
	String requiresageinjury = "";
	
	String iscourtesy = "";
	String isstat = "";
	String isretro = "";
	String isvip = "";
	String onlineimage = "";
	String sched_id = "";
	List<incomingService> incomingServiceList = new ArrayList<incomingService>();
	try{
	scheduler = request.getParameter("scheduler");
	NIM3_referral_source = request.getParameter("NIM3_referral_source");
	NIM3_mdID = request.getParameter("NIM3_mdID");
	REFERRAL_STATUS = request.getParameter("REFERRAL_STATUS");
	REFERRAL_METHOD = request.getParameter("REFERRAL_METHOD");
	ENCOUNTER_TYPE = request.getParameter("ENCOUNTER_TYPE");
	REFERRAL_SOURCE = request.getParameter("REFERRAL_SOURCE");
	REFERRAL_NOTES = request.getParameter("REFERRAL_NOTES");
	ENCOUNTER_NOTES = request.getParameter("ENCOUNTER_NOTES");
	ca_id = request.getParameter("ca_id");
	rf_id = request.getParameter("rf_id");
	en_id = request.getParameter("en_id");
	patientavailability = request.getParameter("patientavailability");
	for(int x = 0; x < 10; x++){
		 	String cpt = request.getParameter("services[" + x + "].cpt");
			String bp = request.getParameter("services[" + x + "].bp");
			String qty = request.getParameter("services[" + x + "].qty");
			String cptModifier = request.getParameter("services[" + x + "].cptModifier");
			String serviceStatus = request.getParameter("services[" + x + "].serviceStatus");
			String sid = request.getParameter("services[" + x + "].sid");
			 if(cpt != null && !cpt.trim().isEmpty() ){
				 incomingServiceList.add(new incomingService(cpt, bp, qty,cptModifier,serviceStatus,sid));
			 }
	}
	requirehandcarrycd = request.getParameter("requirehandcarrycd");
	requiresfilms = request.getParameter("requiresfilms");
	requiresageinjury = request.getParameter("requiresageinjury");
	isstat = request.getParameter("isstat");
	isretro = request.getParameter("isretro");
	isvip = request.getParameter("isvip");
	iscourtesy = request.getParameter("iscourtesy");
	onlineimage = request.getParameter("onlineimage");
	sched_id = request.getParameter("sched_id");
	bltNIM3_Referral rf = new bltNIM3_Referral(new Integer(rf_id));
	bltNIM3_Encounter en = new bltNIM3_Encounter(new Integer(en_id));
	bltNIM3_CommTrack ct = new bltNIM3_CommTrack();
	java.util.Date now = new java.util.Date();
	SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy");
	
	try{
		rf.setReferralMethod(REFERRAL_SOURCE);
	}catch(Exception e){
		e.printStackTrace();
	}
	try{
		 rf.setReferralDate(PLCUtils.getNowDate(true));
		
	}catch(Exception e){
		e.printStackTrace();
	}
	try{
		if(REFERRAL_STATUS != null && !REFERRAL_STATUS.trim().isEmpty()){
		rf.setReferralStatusID(new Integer(REFERRAL_STATUS));
			}
	}catch(Exception e){
		rf.setReferralStatusID(0);
	}
	try{
		if(!REFERRAL_METHOD.trim().isEmpty() && REFERRAL_METHOD.trim() != null){
				rf.setReferralMethod(REFERRAL_METHOD);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
				rf.setReferralTypeID(3);
	
	try{
		rf.setAttendingPhysicianID(52);
		}catch(Exception e){
			e.printStackTrace();
		}
	try{
		if(!NIM3_mdID.trim().isEmpty() && NIM3_mdID != null){
				rf.setReferringPhysicianID(new Integer(NIM3_mdID));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	try{
		if(!NIM3_referral_source.trim().isEmpty() && NIM3_referral_source.trim() != null){
				rf.setReferredByContactID(new Integer(NIM3_referral_source));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	try{
		if(!REFERRAL_NOTES.trim().isEmpty() && REFERRAL_NOTES.trim() != null){
				rf.setComments(REFERRAL_NOTES);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	rf.commitData();
	try{
		if(!patientavailability.trim().isEmpty() && patientavailability.trim() != null){
				en.setPatientAvailability(patientavailability);
			}
		}catch (Exception e){
		    	e.printStackTrace();
		}
	try{
		en.setComments(ENCOUNTER_NOTES);
	}catch(Exception e){
		e.printStackTrace();	
		}
	try{
			en.setUniqueModifyComments("Created by N4");
		}catch (Exception e){
		    	e.printStackTrace();
		}
	
	try{
		if(!requirehandcarrycd.trim().isEmpty() && requirehandcarrycd.trim() != null){
				en.setRequiresHandCarryCD(new Integer(requirehandcarrycd));
			}
		}catch (Exception e){
		    	e.printStackTrace();
		}
	try{
		if(!requiresfilms.trim().isEmpty() && requiresfilms.trim() != null){
				en.setRequiresFilms(new Integer(requiresfilms));
			}
		}catch (Exception e){
		    	e.printStackTrace();
	}
	try{
		if(!requiresageinjury.trim().isEmpty() && requiresageinjury.trim() != null){
				en.setRequiresAgeInjury(new Integer(requiresageinjury));
		}
	}catch (Exception e){
		    	e.printStackTrace();
	}
	try{
		if(!isstat.trim().isEmpty() && isstat.trim() != null){
			en.setisSTAT(new Integer(isstat));
		}
	}catch (Exception e){
	    	e.printStackTrace();
	}
	try{
		if(!isretro.trim().isEmpty() && isretro.trim() != null){
		en.setisRetro(new Integer(isretro));
		} 
	}catch (Exception e){
	    	e.printStackTrace();
		}
	try{
		if(!ENCOUNTER_TYPE.trim().isEmpty() && ENCOUNTER_TYPE.trim() != null){
				en.setEncounterTypeID(new Integer(ENCOUNTER_TYPE));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	en.setUniqueCreateDate(PLCUtils.getNowDate(false));
	en.setUniqueModifyComments("n4");
	en.setUniqueModifyDate(PLCUtils.getNowDate(false));
	en.commitData();
	
	for(incomingService incoming : incomingServiceList){
			bltNIM3_Service sr = new bltNIM3_Service(new Integer(incoming.getSid()));
		if(!incoming.getCpt().trim().isEmpty() && incoming.getCpt().trim() != null){
			sr.setCPT(incoming.getCpt());
		}
		sr.setUniqueModifyComments("Created by N4");
		if(!incoming.getQty().trim().isEmpty() && incoming.getQty().trim() != null){
			sr.setCPTQty(new Integer(incoming.getQty()));
			}
		if(incoming.getServiceStatus() != null && !incoming.getServiceStatus().isEmpty()){
			sr.setServiceStatusID(new Integer(incoming.getServiceStatus()));
		}
		if(!incoming.getBp().trim().isEmpty() && incoming.getBp().trim() != null){
			sr.setCPTBodyPart(incoming.getBp());
			}
		if(incoming.getCptModifier() !=null && !incoming.getCptModifier().trim().isEmpty()){
			sr.setCPTModifier(incoming.getCptModifier());
		}
		sr.setEncounterID(new Integer(en_id));
		out.println("EN:\t"+en_id);
		out.println("SR"+incoming.getSid());
		sr.setUniqueCreateDate(PLCUtils.getNowDate(false));
		sr.setUniqueModifyDate(PLCUtils.getNowDate(false));
		sr.setUniqueModifyComments("N4");
		sr.commitData();
		
		}
		ct.setUniqueCreateDate(now);
		ct.setCaseID(new Integer(ca_id));//FINISH SETTING COMMTRACKS
		ct.setReferralID(new Integer(rf_id));
		ct.setEncounterID(new Integer(en_id));
		ct.setIntUserID(new Integer(sched_id));
		ct.setMessageCompany("Update ");
		ct.setMessageSubject("Encounter Modification");
		ct.setMessageName(scheduler + " Modified " + en.getScanPass() + " N4 Generated.");
		ct.setMessageText("Generated via N4 " + now);
		ct.setCommStart(new Date());
		ct.setCommEnd(new Date());
		ct.commitData();
		out.println("Please wait saving case data loading page.."+ en.getScanPass());
		%>
	 	<script>
			document.location.href = "http://localhost:8080/n4/ca/<%=ca_id%>/<%=en_id%>";
		</script>
	 	<% 
	}catch(Exception e){// END OF IS GOOD
		e.printStackTrace();
	}
	
	
	
	
	
	
	
%>