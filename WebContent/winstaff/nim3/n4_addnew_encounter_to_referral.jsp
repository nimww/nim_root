<%@page import="java.io.*,com.winstaff.*, java.util.*, java.text.SimpleDateFormat,com.google.gson.Gson;"%>
<%!class incomingService {
	
	public String cpt = "";
	public String bp = "";
	public String qty = "";
	public String cptModifier = "";
	public String ro = "";
	public String diag1 = "";
	public String diag2 = "";
	public String diag3 = "";
	public String diag4 = "";
	public incomingService(String cpt, String bp, String qty,String cptModifier,String ro , String diag1, String diag2, String diag3, String diag4) {
		this.cpt = cpt;
		this.bp = bp;
		this.qty = qty;
		this.cptModifier = cptModifier;
		this.ro = ro;
		this.diag1 = diag1;
		this.diag2 = diag2;
		this.diag3 = diag3;
		this.diag4 = diag4;
	}
	public String getCpt() {
		return cpt;
	}
	public void setCpt(String cpt) {
		this.cpt = cpt;
	}
	public String getBp() {
		return bp;
	}
	public void setBp(String bp) {
		this.bp = bp;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getCptModifier() {
		return cptModifier;
	}
	public void setCptModifiert(String cptModifier) {
		this.cptModifier = cptModifier;
	}
	public String getRo() {
		return ro;
	}
	public void setRo(String ro) {
		this.ro = ro;
	}
	public void setDiag1(String diag1) {
		this.diag1 = diag1;
	}
	public String getDiag1() {
		return diag1;
	}
	public String getDiag2() {
		return diag2;
	}
	public void setDiag2(String diag2) {
		this.diag2 = diag2;
	}
	public String getDiag3() {
		return diag3;
	}
	public void setDiag3(String diag3) {
		this.diag3 = diag3;
	}
	public String getDiag4() {
		return diag4;
	}
	public void setDiag4(String diag4) {
		this.diag4 = diag4;
	}
}
	%>

<%
	String schedulerfullname ="";
	String cid ="";
	String rid = "";
	String requirehandcarrycd = "";
	String requiresfilms = "";
	String requiresageinjury = "";
	String isstat = "";
	String isretro = "";
	String isvip = "";
	String resched ="";
	String Encounter_STATUS = "";
	String ENCOUNTER_TYPE = "";
	String ENCOUNTER_NOTES = "";
	List<incomingService> incomingServiceList = new ArrayList<incomingService>();
	boolean isGood = true;
try{
	schedulerfullname = request.getParameter("schedulerfullname");
	cid = request.getParameter("cid");
	rid = request.getParameter("rid");
	requirehandcarrycd = request.getParameter("requirehandcarrycd");
	requiresfilms = request.getParameter("requiresfilms");
	requiresageinjury = request.getParameter("requiresageinjury");
	isstat = request.getParameter("isstat");
	isvip = request.getParameter("isvip");
	resched = request.getParameter("resched");
	Encounter_STATUS = request.getParameter("Encounter_STATUS");
	ENCOUNTER_TYPE = request.getParameter("ENCOUNTER_TYPE");
	ENCOUNTER_NOTES = request.getParameter("ENCOUNTER_NOTES");
	 for(int x = 1; x < 6; x++){
		 	String cpt = request.getParameter("services[" + x + "].cpt");
			String bp = request.getParameter("services[" + x + "].bp");
			String qty = request.getParameter("services[" + x + "].qty");
			String cptModifier = request.getParameter("services[" + x + "].cptModifier");
			String ro = request.getParameter("services[" + x + "].ro");
					String diag1 = request.getParameter("services[" + x + "].diag1");
					String diag2 = request.getParameter("services[" + x + "].diag2");
					String diag3 = request.getParameter("services[" + x + "].diag3");
					String diag4 = request.getParameter("services[" + x + "].diag4");
			if(cpt != null && !cpt.trim().isEmpty() ){
				 incomingServiceList.add(new incomingService(cpt, bp, qty, cptModifier, ro, diag1, diag2, diag3, diag4));
			 }
	 }
}catch(Exception e){
	isGood = false;
	e.printStackTrace();
}
if(isGood){
	bltNIM3_CaseAccount ca = new bltNIM3_CaseAccount();
	bltNIM3_Referral rf = new bltNIM3_Referral();
	bltNIM3_Encounter en = new bltNIM3_Encounter();
	bltNIM3_CommTrack ct = new bltNIM3_CommTrack();			
	java.util.Date now = new java.util.Date();
		
		try{
		en.setReferralID(new Integer(rid));
		
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			if(!ENCOUNTER_NOTES.trim().isEmpty() ){
				 en.setComments(ENCOUNTER_NOTES);
			}
		}catch (Exception e){
		    	e.printStackTrace();
		}
		try{
			en.setUniqueModifyComments("Created by N4");
		}catch (Exception e){
		    	e.printStackTrace();
		}
		try{
			if(!requirehandcarrycd.trim().isEmpty() && requirehandcarrycd.trim() != null){
				en.setRequiresHandCarryCD(new Integer(requirehandcarrycd));
			}
		}catch (Exception e){
		    	e.printStackTrace();
		}
		try{
			if(!requiresfilms.trim().isEmpty() && requiresfilms.trim() != null){
				en.setRequiresFilms(new Integer(requiresfilms));
			}
		}catch (Exception e){
		    	e.printStackTrace();
		}
		try{
			if(!requiresageinjury.trim().isEmpty() && requiresageinjury.trim() != null){
				en.setRequiresAgeInjury(new Integer(requiresageinjury));
			}
		}catch (Exception e){
		    	e.printStackTrace();
		}
		try{
			if(!isstat.trim().isEmpty() && isstat.trim() != null){
				en.setisSTAT(new Integer(isstat));
			}
		}catch (Exception e){
		    	e.printStackTrace();
		}
		try{
			if(!isretro.trim().isEmpty() && isretro.trim() != null){
				en.setisRetro(new Integer(isretro));
			} 
		}catch (Exception e){
		    	e.printStackTrace();
		}
		en.setEncounterStatusID(new Integer(Encounter_STATUS));
		en.setEncounterTypeID(new Integer(ENCOUNTER_TYPE));
		en.setUniqueCreateDate(PLCUtils.getNowDate(false));
 		en.setUniqueModifyComments("n4");
 		en.setUniqueModifyDate(PLCUtils.getNowDate(false));
 		en.commitData();
 		en.setScanPass("SP" + en.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random() * 100.0D) + com.winstaff.password.RandomString.generateString(2).toUpperCase());
 		en.commitData();
 		out.println(en.getScanPass());
 		
 		for(incomingService incoming : incomingServiceList){
 			bltNIM3_Service sr = new bltNIM3_Service();
 			if(!incoming.getCpt().trim().isEmpty() && incoming.getCpt().trim() != null){
 				sr.setCPT(incoming.getCpt());
 			}
				
			if(!incoming.getQty().trim().isEmpty() && incoming.getQty().trim() != null){
				sr.setCPTQty(new Integer(incoming.getQty()));
 			}
			
			if(!incoming.getBp().trim().isEmpty() && incoming.getBp().trim() != null){
				sr.setCPTBodyPart(incoming.getBp());
 			}
			
			if(!incoming.getCptModifier().trim().isEmpty() && incoming.getCptModifier().trim() != null){
				sr.setCPTModifier(incoming.getCptModifier());
			}
			
			if(!incoming.getRo().trim().isEmpty() && incoming.getRo().trim() != null){
				sr.setCPTText(incoming.getRo());
			}
			
			if(!incoming.getDiag1().trim().isEmpty() && incoming.getDiag1() != null){
				sr.setdCPT1(incoming.getDiag1());
			}
			if(!incoming.getDiag2().trim().isEmpty() && incoming.getDiag2() != null){
				sr.setdCPT2(incoming.getDiag2());						
			}
			if(!incoming.getDiag3().trim().isEmpty() && incoming.getDiag3() != null){
				sr.setdCPT3(incoming.getDiag3());
			}
			if(!incoming.getDiag4().trim().isEmpty() && incoming.getDiag4() != null){
				sr.setdCPT4(incoming.getDiag4());
			}
			
			sr.setServiceTypeID(3);
			sr.setServiceStatusID(1);
			sr.setEncounterID(en.getUniqueID());
			sr.setUniqueModifyComments("Created by N4");
			sr.setUniqueCreateDate(PLCUtils.getNowDate(false));
			sr.setUniqueModifyDate(PLCUtils.getNowDate(false));
			
			sr.commitData();
 		} 
 		try{
 			ct.setCaseID(ca.getUniqueID());//FINISH SETTING COMMTRACKS
 		}catch(Exception e){
 			e.printStackTrace();
 		}
		ct.setIntUserID(ca.getAssignedToID());
		ct.setMessageName(schedulerfullname);
		ct.setCaseID(new Integer(cid));
		ct.setReferralID(new Integer(rid));
		ct.setEncounterID(en.getUniqueID());
		ct.setUniqueCreateDate(now);
		ct.setMessageSubject("[Generated via N4]");
		ct.setMessageText("Generated via N4" + now);
		ct.setMessageCompany("N4 Generated");
		ct.setMessageSubject(" SM..");
		ct.setCommStart(new Date());
		ct.setCommEnd(new Date());
		ct.commitData();
		%>
	 	<script>
	 	document.location.href = "http://localhost:8080/n4/ca/<%=cid%>/<%=en.getEncounterID()%>";
		</script>
	 	<% 
}
%>
