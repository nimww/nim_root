<%@page contentType="text/html" language="java" import="com.winstaff.*,org.apache.http.*,java.io.*,java.util.List,com.winstaff.SalesPortal.*, java.util.LinkedHashMap"%>
<!doctype html>
<html>
<head>
<%@ include file="../generic/CheckLogin.jsp"%>
<meta charset="UTF-8">
<title>Client Tracker</title>
<link href="https://netdna.bootstrapcdn.com/bootswatch/2.3.1/flatly/bootstrap.min.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-responsive.min.css" rel="stylesheet">
<%
	int userID = CurrentUserAccount.getUserID();
List<BarChartData> lBCD =  SalesPortal.getOpenReferrals(userID);
List<BarChartData2> lBCD2 =  SalesPortal.getVolume(userID);
%>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<style>
.canvas-bar {
	width: 100% !important;
	background: white;
	margin-top: 10px;
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: 10px 0;
}
</style>
<script src="js/Chart.min.js"></script>
</head>

<body>
	<div class="container-fluid">
		<div class="navbar  ">
			<div class="navbar-inner">
				<a class="brand" href="#">Client Tracker</a>
				<div class="container">
					<div class="nav-collapse collapse" id="main-menu">
						<ul class="nav" id="main-menu-left">
							<li><a href="#"><i class="icon-th icon-white"></i>&nbsp;Stats</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">

		<div class="row-fluid">
			<div class="span6">
				<div class="well well-small">
					<h4>Referral Volume</h4>
					<div class="row-fluid">
						<canvas id="ref_volume" class="canvas-bar" height="300" width="800"></canvas>
					</div>
				</div>
			</div>
			<div class="span6">
				<div class="well well-small">
					<h4>Open Referrals</h4>
					<div class="row-fluid">
						<canvas id="open_ref" class="canvas-bar" height="300" width="800"></canvas>
					</div>

				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="well well-small">
					<h4>Open Cases</h4>
					<table class="table table-striped table-bordered table-hover" style="background: white">
						<thead>
							<tr>
								<th>Patient Name</th>
								<th>Track Name</th>
								<th>Payer Name</th>
								<th>Receive Date</th>
								<th>Scheduler</th>
								<th>Scanpass</th>
							</tr>
						</thead>
						<tbody>
							<%
								for(OpenCases o : SalesPortal.getListCases(userID,SalesPortal.OpenCases)){
							%>
							<tr>
								<td><%=o.getPatientName()%></td>
								<td><%=o.getTrackName()%></td>
								<td><%=o.getCarrier()%></td>
								<td><%=o.getReceiveDate()%></td>
								<td><%=o.getScheduler()%></td>
								<td><%=o.getScanpass()%></td>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<div class="well well-small">
					<h4>History Track</h4>
					<table class="table table-striped table-bordered table-hover" style="background: white">
						<thead>
							<tr>
								<th>Patient Name</th>
								<th>Track Name</th>
								<th>Payer Name</th>
								<th>Receive Date</th>
								<th>Date of Service</th>
								<th>Scheduler</th>
								<th>Scanpass</th>
							</tr>
						</thead>
						<tbody>
							<%
								for(OpenCases o : SalesPortal.getListCases(userID,SalesPortal.HistoryTrack)){
							%>
							<tr>
								<td><%=o.getPatientName()%></td>
								<td><%=o.getTrackName()%></td>
								<td><%=o.getCarrier()%></td>
								<td><%=o.getReceiveDate()%></td>
								<td><%=o.getDateofservice()%></td>
								<td><%=o.getScheduler()%></td>
								<td><%=o.getScanpass()%></td>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<script>
		var barChartData = {
			labels : [
	<%=SalesPortal.parseBarChartData(lBCD,SalesPortal.BarChartDataLabel)%>
		],
			datasets : [

			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,1)",
				data : [
	<%=SalesPortal.parseBarChartData(lBCD,SalesPortal.BarChartDataData)%>
		]
			} ]

		};
		var barChartData2 = {
			labels : [
	<%=SalesPortal.parseBarChartData2(lBCD2,SalesPortal.BarChartDataLabel)%>
		],
			datasets : [
					{
						fillColor : "rgba(220,220,220,0.5)",
						strokeColor : "rgba(220,220,220,1)",
						data : [
	<%=SalesPortal.parseBarChartData2(lBCD2,SalesPortal.BarChartDataData3)%>
		]
					},
					{
						fillColor : "rgba(151,187,205,0.5)",
						strokeColor : "rgba(151,187,205,1)",
						data : [
	<%=SalesPortal.parseBarChartData2(lBCD2,SalesPortal.BarChartDataData2)%>
		]
					},
					{
						fillColor : "rgba(90, 158, 192, 0.5)",
						strokeColor : "rgba(90, 158, 192, 1)",
						data : [
	<%=SalesPortal.parseBarChartData2(lBCD2,SalesPortal.BarChartDataData)%>
		]
					} ]

		};

		var BarChartData = new Chart(document.getElementById("open_ref")
				.getContext("2d")).Bar(barChartData);
		var BarChartData2 = new Chart(document.getElementById("ref_volume")
				.getContext("2d")).Bar(barChartData2);
	</script>
</body>
</html>
