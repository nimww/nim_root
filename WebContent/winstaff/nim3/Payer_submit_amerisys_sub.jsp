<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
</head>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler) 
    {
        accessValid = true;
    }
    else if (isAdjuster) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
	}
    else if (isProvider) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
	}
	
	
	
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	
	if (isAdjuster||isProvider)
	  {

try
{
//initial declaration of list class and parentID
String ip = request.getRemoteAddr();
String host = request.getRemoteHost();


//initial declaration of list class and parentID
    bltNIM3_CommTrack        NIM3_CommTrack        =       new    bltNIM3_CommTrack();
	NIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setExtUserID(CurrentUserAccount.getUserID());
	NIM3_CommTrack.setComments("On-Line Submit -> Start (never close)\nIP: " + ip + "\nHost: " + host);
	NIM3_CommTrack.commitData();


	bltNIM3_CaseAccount        NIM3_CaseAccount        =       new    bltNIM3_CaseAccount();
    bltNIM3_Referral        NIM3_Referral        =       new    bltNIM3_Referral();
    bltNIM3_Encounter        NIM3_Encounter        =       new    bltNIM3_Encounter();

String testChangeID = "0";
String testChangeIDMSG = "0";
//for CaseAccount
try
{
    String testObj = new String(request.getParameter("clientClaimNum")) ;
    if ( !NIM3_CaseAccount.getCaseClaimNumber().equals(testObj)  )
    {
         NIM3_CaseAccount.setCaseClaimNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount CaseClaimNumber1 not set. this is ok-not an error");
}

String sComments = "\n====\nAdd'l Info:\n";
String aComments = "";
String rComments = "";

try
{
    String testObj = new String(request.getParameter("patientFirstName")) ;
    if ( !NIM3_CaseAccount.getPatientFirstName().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientFirstName( testObj,UserSecurityGroupID   );
		 aComments += "\nPatientFirstName: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientFirstName not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    aComments += "\nPatientDOB: " + request.getParameter("patientDOI");
    testObj = (PLCUtils.getSubDate(request.getParameter("patientDOI")));
    if ( !NIM3_CaseAccount.getDateOfInjury().equals(testObj)  )
    {
         NIM3_CaseAccount.setDateOfInjury( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientDOB not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("EmployerPhone")) ;
    if ( !NIM3_CaseAccount.getEmployerPhone().equals(testObj)  )
    {
         NIM3_CaseAccount.setEmployerPhone( testObj,UserSecurityGroupID   );
		 aComments += "\nEmployerPhone: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("employer")) ;
    if ( !NIM3_CaseAccount.getEmployerName().equals(testObj)  )
    {
         NIM3_CaseAccount.setEmployerName( testObj,UserSecurityGroupID   );
		 aComments += "\nEmployerName: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("patientLastName")) ;
    if ( !NIM3_CaseAccount.getPatientLastName().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientLastName( testObj,UserSecurityGroupID   );
		 aComments += "\nPatientLastName: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientLastName not set. this is ok-not an error");
}


try
{
    java.util.Date testObj = null ;
    aComments += "\nPatientDOB: " + request.getParameter("patientDOB");
    testObj = (PLCUtils.getSubDate(request.getParameter("patientDOB"))) ;
    if ( !NIM3_CaseAccount.getPatientDOB().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientDOB( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientDOB not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("PatientSSN")) ;
    if ( !NIM3_CaseAccount.getPatientSSN().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientSSN( testObj,UserSecurityGroupID   );
		 aComments += "\nPatientSSN: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientSSN not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("patientGender")) ;
    if ( !NIM3_CaseAccount.getPatientGender().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientGender( testObj,UserSecurityGroupID   );
		 aComments += "\nPatientGender: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientSSN not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("patientPhone")) ;
    if ( !NIM3_CaseAccount.getPatientHomePhone().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientHomePhone( testObj,UserSecurityGroupID   );
		 aComments += "\nPatientHomePhone: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientHomePhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientCellPhone")) ;
    if ( !NIM3_CaseAccount.getPatientCellPhone().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientCellPhone( testObj,UserSecurityGroupID   );
		 aComments += "\nPatientCellPhone: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientCellPhone not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("patientAddress1")) ;
    if ( !NIM3_CaseAccount.getPatientAddress1().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientAddress1( testObj,UserSecurityGroupID   );
		 aComments += "\nPatientAddress1: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("patientAddress2")) ;
    if ( !NIM3_CaseAccount.getPatientAddress2().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientAddress2( testObj,UserSecurityGroupID   );
		 aComments += "\nPatientAddress2: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("patientCity")) ;
    if ( !NIM3_CaseAccount.getPatientCity().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientCity( testObj,UserSecurityGroupID   );
		 aComments += "\nPatientCity: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PatientStateID")) ;
    if ( !NIM3_CaseAccount.getPatientStateID().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientStateID( testObj,UserSecurityGroupID   );
		 aComments += "\nPatientStateID: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("patientZip")) ;
    if ( !NIM3_CaseAccount.getPatientZIP().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientZIP( testObj,UserSecurityGroupID   );
		 aComments += "\nPatientZIP: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientZIP not set. this is ok-not an error");
}






try
{
    String testObj = new String(request.getParameter("ReferringPhysicianName")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nRef. Dr Name: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReferringPhysicianPhone")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nRef. Dr Phone: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReferringPhysicianEmail")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nRef. Dr Email: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReferringPhysicianFax")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nRef. Dr Fax: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}

rComments+="\n============\nPlease verify that the adjuster is not already linked to the case";

try
{
    String testObj = new String(request.getParameter("adjusterFirstName")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nAdjuster First: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("adjusterLastName")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nAdjuster Last: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("casemanagerFirstName")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nCaseAdmin First: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("casemanagerLastName")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nCaseAdmin Last: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("AdjusterCompany")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nAdjusterCompany: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AdjusterCity")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nAdjusterCity: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AdjusterPhone")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nAdjusterPhone: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AdjusterFax")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nAdjusterFax: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("AdjusterEmail")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nAdjusterEmail: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}







try
{
    String testObj = new String(request.getParameter("CPTText")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         NIM3_Encounter.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter CPTText not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PayerPreAuthorizationConfirmation")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         NIM3_Referral.setPreAuthorizationConfirmation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM2_Authorization PayerPreAuthorizationConfirmation not set. this is ok-not an error");
}




//for CommTrack

try
{
    Integer testObj = new Integer(request.getParameter("IntUserID")) ;
    if ( !NIM3_CommTrack.getIntUserID().equals(testObj)  )
    {
         NIM3_CommTrack.setIntUserID( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack IntUserID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ExtUserID")) ;
    if ( !NIM3_CommTrack.getExtUserID().equals(testObj)  )
    {
         NIM3_CommTrack.setExtUserID( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack ExtUserID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CommTypeID")) ;
    if ( !NIM3_CommTrack.getCommTypeID().equals(testObj)  )
    {
         NIM3_CommTrack.setCommTypeID( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack CommTypeID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("CommStart"))) ;
    if ( !NIM3_CommTrack.getCommStart().equals(testObj)  )
    {
         NIM3_CommTrack.setCommStart( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack CommStart not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("CommEnd"))) ;
    if ( !NIM3_CommTrack.getCommEnd().equals(testObj)  )
    {
         NIM3_CommTrack.setCommEnd( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack CommEnd not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageText")) ;
    if ( !NIM3_CommTrack.getMessageText().equals(testObj)  )
    {
			NIM3_CommTrack.setMessageText( testObj,UserSecurityGroupID   );
			sComments += testObj;
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageText not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageName")) ;
    if ( !NIM3_CommTrack.getMessageName().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageName( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageCompany")) ;
    if ( !NIM3_CommTrack.getMessageCompany().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageCompany( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageCompany not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageEmail")) ;
    if ( !NIM3_CommTrack.getMessageEmail().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageEmail( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessagePhone")) ;
    if ( !NIM3_CommTrack.getMessagePhone().equals(testObj)  )
    {
         NIM3_CommTrack.setMessagePhone( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessagePhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageFax")) ;
    if ( !NIM3_CommTrack.getMessageFax().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageFax( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageFax not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AlertStatusCode")) ;
    if ( !NIM3_CommTrack.getAlertStatusCode().equals(testObj)  )
    {
         NIM3_CommTrack.setAlertStatusCode( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack AlertStatusCode not set. this is ok-not an error");
}




if (testChangeID.equalsIgnoreCase("1"))
{
//	out.println("Success Code: 102");
	//autosets Case Account
	NIM3_CaseAccount.setPayerID(new Integer(307));
	NIM3_CaseAccount.setCaseStatusID(new Integer(1));
	NIM3_CaseAccount.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_CaseAccount.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_CaseAccount.setUniqueModifyComments("Secure Online Form: UserID["+CurrentUserAccount.getUserID()+"]");

	//add primary user
	NIM3_CaseAccount.setReferredByContactID(CurrentUserAccount.getUserID());
	String sLinkType = request.getParameter("LinkType");
	if (sLinkType.equalsIgnoreCase("Adjuster"))
	{
		NIM3_CaseAccount.setAdjusterID(CurrentUserAccount.getUserID());
		NIM3_CaseAccount.setPayerID(CurrentUserAccount.getPayerID());
	}
	else if (sLinkType.equalsIgnoreCase("NCM"))
	{
		NIM3_CaseAccount.setNurseCaseManagerID(CurrentUserAccount.getUserID());
	}
	else if (sLinkType.equalsIgnoreCase("Admin"))
	{
		NIM3_CaseAccount.setCaseAdministratorID(CurrentUserAccount.getUserID());
	}
	else if (sLinkType.equalsIgnoreCase("Dr"))
	{
		NIM3_Referral.setReferringPhysicianID(CurrentUserAccount.getUserID());
	}

	if (isAdjuster3||isProvider3)
	{
		//add 2nd user
		Integer sLink2 = new Integer(request.getParameter("Link2"));
		String sLinkType2 = request.getParameter("LinkType2");
		if (NIMUtils.isValidUserByPayer(sLink2,CurrentUserAccount.getPayerID()))
		{
			if (sLinkType2.equalsIgnoreCase("Adjuster")&&NIM3_CaseAccount.getAdjusterID()==0)  //must check that it has not already been taken
			{
				NIM3_CaseAccount.setAdjusterID(sLink2);  //still the same payer
				NIM3_CaseAccount.setPayerID(CurrentUserAccount.getPayerID());  //still the same payer
			}
			else if (sLinkType2.equalsIgnoreCase("Adjuster")&&NIM3_CaseAccount.getAdjusterID()!=0)
			{
				if (NIM3_CaseAccount.getCaseAdministratorID()==0)  //try admin 1
				{
					NIM3_CaseAccount.setCaseAdministratorID(sLink2);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator2ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator2ID(sLink2);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator3ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator3ID(sLink2);
				}
			}
			else if (sLinkType2.equalsIgnoreCase("NCM")&&NIM3_CaseAccount.getNurseCaseManagerID()==0)
			{
				NIM3_CaseAccount.setNurseCaseManagerID(sLink2);
			}
			else if (sLinkType2.equalsIgnoreCase("NCM")&&NIM3_CaseAccount.getNurseCaseManagerID()!=0)
			{
				if (NIM3_CaseAccount.getCaseAdministratorID()==0)  //try admin 1
				{
					NIM3_CaseAccount.setCaseAdministratorID(sLink2);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator2ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator2ID(sLink2);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator3ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator3ID(sLink2);
				}
			}
			else if (sLinkType2.equalsIgnoreCase("Dr")&&NIM3_Referral.getReferringPhysicianID()==0)
			{
				NIM3_Referral.setReferringPhysicianID(sLink2);
			}
			else if (sLinkType2.equalsIgnoreCase("Dr")&&NIM3_Referral.getReferringPhysicianID()!=0)
			{
				if (NIM3_CaseAccount.getCaseAdministratorID()==0)  //try admin 1
				{
					NIM3_CaseAccount.setCaseAdministratorID(sLink2);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator2ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator2ID(sLink2);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator3ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator3ID(sLink2);
				}
			}
			else if (sLinkType2.equalsIgnoreCase("Admin"))
			{
				if (NIM3_CaseAccount.getCaseAdministratorID()==0)  //try admin 1
				{
					NIM3_CaseAccount.setCaseAdministratorID(sLink2);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator2ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator2ID(sLink2);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator3ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator3ID(sLink2);
				}
			}
		}
		else
		{

		}
		//add 3rd user
		Integer sLink3 = new Integer(request.getParameter("Link3"));
		String sLinkType3 = request.getParameter("LinkType3");
		if (NIMUtils.isValidUserByPayer(sLink3,CurrentUserAccount.getPayerID()))
		{
			if (sLinkType3.equalsIgnoreCase("Adjuster")&&NIM3_CaseAccount.getAdjusterID()==0)  //must check that it has not already been taken
			{
				NIM3_CaseAccount.setAdjusterID(sLink3);  //still the same payer
				NIM3_CaseAccount.setPayerID(CurrentUserAccount.getPayerID());  //still the same payer
			}
			else if (sLinkType3.equalsIgnoreCase("Adjuster")&&NIM3_CaseAccount.getAdjusterID()!=0)
			{
				if (NIM3_CaseAccount.getCaseAdministratorID()==0)  //try admin 1
				{
					NIM3_CaseAccount.setCaseAdministratorID(sLink3);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator2ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator2ID(sLink3);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator3ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator3ID(sLink3);
				}
			}
			else if (sLinkType3.equalsIgnoreCase("NCM")&&NIM3_CaseAccount.getNurseCaseManagerID()==0)
			{
				NIM3_CaseAccount.setNurseCaseManagerID(sLink3);
			}
			else if (sLinkType3.equalsIgnoreCase("NCM")&&NIM3_CaseAccount.getNurseCaseManagerID()!=0)
			{
				if (NIM3_CaseAccount.getCaseAdministratorID()==0)  //try admin 1
				{
					NIM3_CaseAccount.setCaseAdministratorID(sLink3);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator2ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator2ID(sLink3);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator3ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator3ID(sLink3);
				}
			}
			else if (sLinkType3.equalsIgnoreCase("Dr")&&NIM3_Referral.getReferringPhysicianID()==0)
			{
				NIM3_Referral.setReferringPhysicianID(sLink2);
			}
			else if (sLinkType3.equalsIgnoreCase("Dr")&&NIM3_Referral.getReferringPhysicianID()!=0)
			{
				if (NIM3_CaseAccount.getCaseAdministratorID()==0)  //try admin 1
				{
					NIM3_CaseAccount.setCaseAdministratorID(sLink3);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator2ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator2ID(sLink3);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator3ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator3ID(sLink3);
				}
			}
			else if (sLinkType3.equalsIgnoreCase("Admin"))
			{
				if (NIM3_CaseAccount.getCaseAdministratorID()==0)  //try admin 1
				{
					NIM3_CaseAccount.setCaseAdministratorID(sLink3);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator2ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator2ID(sLink3);
				}
				else if (NIM3_CaseAccount.getCaseAdministrator3ID()==0)  //try admin 2
				{
					NIM3_CaseAccount.setCaseAdministrator3ID(sLink3);
				}
			}
		}
	}


	NIM3_CaseAccount.setComments("This case was submitted via online secure form. Before proceeding, please verify that patient/claim is not already in the system.");
	NIM3_CaseAccount.commitData();

	NIM3_CaseAccount.setCaseCode("CP" +  NIM3_CaseAccount.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
	NIM3_CaseAccount.commitData();
	
	//autosets referral
	NIM3_Referral.setCaseID(NIM3_CaseAccount.getUniqueID());
	NIM3_Referral.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_Referral.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_Referral.setUniqueModifyComments("Secure Online Form: UserID["+CurrentUserAccount.getUserID()+"]");
	NIM3_Referral.setComments(aComments + "\n" + rComments + "\n" + sComments);
	NIM3_Referral.setReferralTypeID(new Integer(3));
	NIM3_Referral.setReferralDate(PLCUtils.getNowDate(true));
	NIM3_Referral.setReceiveDate(PLCUtils.getNowDate(true));
	
	NIM3_Referral.setReferralStatusID(new Integer(1));
	
	NIM3_Referral.setAttendingPhysicianID(new Integer(52));
	NIM3_Referral.commitData();
	
	//autosets Encounter
	NIM3_Encounter.setReferralID(NIM3_Referral.getUniqueID());
	NIM3_Encounter.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_Encounter.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_Encounter.setAttendingPhysicianID(new Integer(52));
	NIM3_Encounter.setUniqueModifyComments("Secure Online Form: UserID["+CurrentUserAccount.getUserID()+"]");
	NIM3_Encounter.setEncounterStatusID(new Integer(1));
	NIM3_Encounter.commitData();
	NIM3_Encounter.setScanPass("SP" +  NIM3_Encounter.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
	NIM3_Encounter.commitData();
	
	//autosets Commtrack
	NIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setUniqueModifyComments("Secure Online Form: UserID["+CurrentUserAccount.getUserID()+"]");
	NIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setCaseID(NIM3_CaseAccount.getUniqueID());
	NIM3_CommTrack.setEncounterID(NIM3_Encounter.getUniqueID());
	NIM3_CommTrack.setReferralID(NIM3_Referral.getUniqueID());
	NIM3_CommTrack.setExtUserID(CurrentUserAccount.getUserID());
	NIM3_CommTrack.setMessageName(CurrentUserAccount.getLogonUserName());
	bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
	NIM3_CommTrack.setMessageCompany(myPM.getPayerName());
	NIM3_CommTrack.setMessageText(aComments + "\n" + rComments + "\n" + sComments);
	NIM3_CommTrack.setCommTypeID(new Integer(2));//1=message 2=sched     0 = bogus
	NIM3_CommTrack.setComments("[OFs] Schedule Submit [CL:" + NIM3_CaseAccount.getCaseClaimNumber() + "]\n[IP: " + ip + "]\n[Host: " + host + "]");
	NIM3_CommTrack.commitData();
	out.println("<script language=\"javascript\">alert ('Successful Referral\\n\\nClick OK to review submitted Referral and upload supporting documents.');document.location='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=viewpayer&EDITID=" + NIM3_CaseAccount.getUniqueID() + "&KM=p';</script>");

}
else if (testChangeIDMSG.equalsIgnoreCase("1"))
{
	out.println("Success Code: 101");
	//autosets Commtrack
	NIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setUniqueModifyComments("Created by Online Form");
	NIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setExtUserID(new Integer(19));
	NIM3_CommTrack.setCommTypeID(new Integer(1));//1=message 2=sched     0 = bogus
	NIM3_CommTrack.setComments("[SOFs] Message Only Submit\nIP: " + ip + "\nHost: " + host);
	NIM3_CommTrack.commitData();
}
else 
{
	out.println("Success Code: 100");
	NIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
//	NIM3_CommTrack.setExtUserID(new Integer(19));
	NIM3_CommTrack.setCommTypeID(new Integer(0));//1=message 2=sched     0 = bogus
	NIM3_CommTrack.setComments("[SOFs] Invalid/Empty Submit\nIP: " + ip + "\nHost: " + host);
	NIM3_CommTrack.commitData();
}

bltNIM3_PayerMaster myPM2 = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
		emailType_V3 myETSend = new emailType_V3();
		String theBody = "NextImageGrid CommTrack Notification:\n\n";
		theBody += "From: " + CurrentUserAccount.getContactFirstName() + " " + CurrentUserAccount.getContactLastName() + " @ " + myPM2.getPayerName() ;
		theBody += "\nSubmitted by: Secure Online Form: UserID["+CurrentUserAccount.getUserID()+"]";
		theBody += "\nTrackingID: " + NIM3_CommTrack.getUniqueID();
		theBody += "\nDate: " + PLCUtils.getNowDate(false);
		
		myETSend.setBody(theBody);
		myETSend.setBodyType(myETSend.PLAIN_TYPE);
	
		myETSend.setFrom("support@nextimagemedical.com");
		myETSend.setSubject("NextImageGrid - CommTrack Submitted by Online Form [Sec] ");
		myETSend.setTo("orders@nextimagemedical.com");
		myETSend.isSendMail();

}
catch(Exception ee)
{
    out.println("<hr>" + ee + "<hr>");
}
	  }
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>