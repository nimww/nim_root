<%@page import="java.io.*,com.winstaff.*, java.util.*, java.sql.*, java.text.SimpleDateFormat;"%>
<%
	String scanpass = "";
	String fileName = "";
	String docType = "";
	boolean isGood = true;
	
	try{
		scanpass = request.getParameter("scanpass");
		fileName = request.getParameter("fileName");
		docType = request.getParameter("docType");
	} catch(Exception e){
		isGood = false;
	}
	
	String query = "";
	int encounterid = 0;
	
	NIM3_EncounterObject2 eo2 = null;
	
	SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy");
	SimpleDateFormat timestamp = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	
	if (isGood) {
		scanpass = request.getParameter("scanpass");

		query = "SELECT encounterid from tnim3_encounter where scanpass = '" + scanpass + "'";

		searchDB2 db = new searchDB2();

		ResultSet rs = db.executeStatement(query);

		try {
			while (rs.next()) {
				eo2 = new NIM3_EncounterObject2(rs.getInt(1), "");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closeAll();
		}
		String statText = " STAT CASE!!!!";
		java.util.Date now = new java.util.Date();
		
		bltNIM3_CommTrack ct = new bltNIM3_CommTrack();
		ct.setEncounterID(eo2.getNIM3_Encounter().getEncounterID());
		ct.setReferralID(eo2.getNIM3_Referral().getReferralID());
		ct.setCaseID(eo2.getNIM3_CaseAccount().getCaseID());
		ct.setMessageName("N4 FaxAPI");
		ct.setMessageCompany("NextImage Medical");
		ct.setUniqueCreateDate(now);
		ct.setUniqueModifyDate(now);
		ct.setCommStart(now);
		ct.setCommEnd(now);
		ct.setUniqueModifyComments("N4 FaxAPI");
		ct.setMessageText(docType+" has been upload to this case.");
		ct.setCommTypeID(0);
		ct.setAlertStatusCode(1);
		ct.commitData();
		
		bltEmailTransaction email = new bltEmailTransaction();
		email.setEmailTo(eo2.getCase_AssignedTo().getContactEmail());
		email.setEmailFrom("support@nextimagemedical.com");
		email.setEmailSubject(docType+" has been upload to claim #"+eo2.getNIM3_CaseAccount().getCaseClaimNumber()+"."+(eo2.getNIM3_Encounter().getisSTAT()==1 ? statText:""));
		email.setEmailBody(docType+" has been upload to "+eo2.getNIM3_Encounter().getScanPass()+" via N4 faxAPI.");
		email.commitData();
		
		bltNIM3_Document doc = new bltNIM3_Document();
		doc.setCaseID(eo2.getNIM3_CaseAccount().getCaseID());
        doc.setFileName(fileName);
        doc.setReferralID(eo2.getNIM3_Referral().getReferralID());
        doc.setEncounterID(eo2.getNIM3_Encounter().getEncounterID());
        doc.setUploadUserID(1);
        doc.setComments(docType);
        doc.setDocName(docType);
        doc.setUniqueModifyComments("Uploaded via N4 FaxAPI");
        doc.commitData();
		
        if(docType.equals("Rx")){
        	eo2.getNIM3_Referral().setRxFileID(doc.getUniqueID());
        	eo2.getNIM3_Referral().commitData();
        } else if(docType.equals("Auth")){
        	eo2.getNIM3_Referral().setOrderFileID(doc.getUniqueID());
        	eo2.getNIM3_Referral().commitData();
        } else if(docType.equals("Rp")){
        	eo2.getNIM3_Encounter().setReportFileID(doc.getUniqueID());
        	eo2.getNIM3_Encounter().commitData();
        } else if(docType.equals("HCFA")){
        	eo2.getNIM3_Encounter().setBillingHCFA_FromProvider_FileID(doc.getUniqueID());
        	eo2.getNIM3_Encounter().commitData();
        }
        
        
	}
	if (isGood) {
%>
Action submitted successfully.
<%
	} else{
%>
Error. Action did not complete. 
<%
	}
%>
