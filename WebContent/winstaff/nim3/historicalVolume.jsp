<%@page contentType="text/html" language="java" import="java.util.ArrayList,java.sql.*,com.winstaff.*,org.apache.http.*,java.io.*,java.util.List,com.winstaff.HistoricalVolume.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<meta charset="utf-8">

<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">


<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script src="jquery.maskedinput.min.js"></script>
<script src="jquery.maskedinput.js"></script>

<link href="https://rawgithub.com/jharding/typeahead.js-bootstrap.css/master/typeahead.js-bootstrap.css" rel="stylesheet" media="screen">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>historical view</title>
<script >

(function(e){function t(){var e=document.createElement("input"),t="onpaste";return e.setAttribute(t,""),"function"==typeof e[t]?"paste":"input"}var n,a=t()+".mask",r=navigator.userAgent,i=/iphone/i.test(r),o=/android/i.test(r);e.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawMaskFn",placeholder:"_"},e.fn.extend({caret:function(e,t){var n;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof e?(t="number"==typeof t?t:e,this.each(function(){this.setSelectionRange?this.setSelectionRange(e,t):this.createTextRange&&(n=this.createTextRange(),n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",e),n.select())})):(this[0].setSelectionRange?(e=this[0].selectionStart,t=this[0].selectionEnd):document.selection&&document.selection.createRange&&(n=document.selection.createRange(),e=0-n.duplicate().moveStart("character",-1e5),t=e+n.text.length),{begin:e,end:t})},unmask:function(){return this.trigger("unmask")},mask:function(t,r){var c,l,s,u,f,h;return!t&&this.length>0?(c=e(this[0]),c.data(e.mask.dataName)()):(r=e.extend({placeholder:e.mask.placeholder,completed:null},r),l=e.mask.definitions,s=[],u=h=t.length,f=null,e.each(t.split(""),function(e,t){"?"==t?(h--,u=e):l[t]?(s.push(RegExp(l[t])),null===f&&(f=s.length-1)):s.push(null)}),this.trigger("unmask").each(function(){function c(e){for(;h>++e&&!s[e];);return e}function d(e){for(;--e>=0&&!s[e];);return e}function m(e,t){var n,a;if(!(0>e)){for(n=e,a=c(t);h>n;n++)if(s[n]){if(!(h>a&&s[n].test(R[a])))break;R[n]=R[a],R[a]=r.placeholder,a=c(a)}b(),x.caret(Math.max(f,e))}}function p(e){var t,n,a,i;for(t=e,n=r.placeholder;h>t;t++)if(s[t]){if(a=c(t),i=R[t],R[t]=n,!(h>a&&s[a].test(i)))break;n=i}}function g(e){var t,n,a,r=e.which;8===r||46===r||i&&127===r?(t=x.caret(),n=t.begin,a=t.end,0===a-n&&(n=46!==r?d(n):a=c(n-1),a=46===r?c(a):a),k(n,a),m(n,a-1),e.preventDefault()):27==r&&(x.val(S),x.caret(0,y()),e.preventDefault())}function v(t){var n,a,i,l=t.which,u=x.caret();t.ctrlKey||t.altKey||t.metaKey||32>l||l&&(0!==u.end-u.begin&&(k(u.begin,u.end),m(u.begin,u.end-1)),n=c(u.begin-1),h>n&&(a=String.fromCharCode(l),s[n].test(a)&&(p(n),R[n]=a,b(),i=c(n),o?setTimeout(e.proxy(e.fn.caret,x,i),0):x.caret(i),r.completed&&i>=h&&r.completed.call(x))),t.preventDefault())}function k(e,t){var n;for(n=e;t>n&&h>n;n++)s[n]&&(R[n]=r.placeholder)}function b(){x.val(R.join(""))}function y(e){var t,n,a=x.val(),i=-1;for(t=0,pos=0;h>t;t++)if(s[t]){for(R[t]=r.placeholder;pos++<a.length;)if(n=a.charAt(pos-1),s[t].test(n)){R[t]=n,i=t;break}if(pos>a.length)break}else R[t]===a.charAt(pos)&&t!==u&&(pos++,i=t);return e?b():u>i+1?(x.val(""),k(0,h)):(b(),x.val(x.val().substring(0,i+1))),u?t:f}var x=e(this),R=e.map(t.split(""),function(e){return"?"!=e?l[e]?r.placeholder:e:void 0}),S=x.val();x.data(e.mask.dataName,function(){return e.map(R,function(e,t){return s[t]&&e!=r.placeholder?e:null}).join("")}),x.attr("readonly")||x.one("unmask",function(){x.unbind(".mask").removeData(e.mask.dataName)}).bind("focus.mask",function(){clearTimeout(n);var e;S=x.val(),e=y(),n=setTimeout(function(){b(),e==t.length?x.caret(0,e):x.caret(e)},10)}).bind("blur.mask",function(){y(),x.val()!=S&&x.change()}).bind("keydown.mask",g).bind("keypress.mask",v).bind(a,function(){setTimeout(function(){var e=y(!0);x.caret(e),r.completed&&e==x.val().length&&r.completed.call(x)},0)}),y()}))}})})(jQuery);
	$(document).ready(function(){
		$('#commit').click(function(e) {
	        var isValid = true;
	        $('input[type="text"].required').each(function() {
	            if ($.trim($(this).val()) == '') {
	                isValid = false;
	                $(this).css({
	                    "border": "1px solid red",
	                    "background": "#FFCECE"
	                });
	            }
	            else {
	                $(this).css({
	                    "border": "",
	                    "background": ""
	                });
	            }
	        });
	        if (isValid == false) 
	            e.preventDefault();
	        else 
	            return;
	    });
	      $('.date').mask('99/99/9999');
	      /* $('.phone').mask('(999) 999-9999');
	      $('.phoneext').mask("(999) 999-9999? x99999");
	      $(".tin").mask("99-9999999");
	      $(".ssn").mask("999-99-9999");
	      $(".product").mask("a*-999-a999",{placeholder:" ",completed:function(){alert("You typed the following: "+this.val());}});
	      $(".eyescript").mask("~9.99 ~9.99 999"); */
	      
	      $('.showMyTbl').click(function(){
				$('#myTable').toggle('slow');
			});
	});
	
	      
	  
</script>
</head>
<%
		if(request.getParameter("datevar1") != null && request.getParameter("datevar2") !=null){
			HistoricalVolume hv = new HistoricalVolume(request.getParameter("datevar1"), request.getParameter("datevar2"));
			List<myHistoricalListModel> mylist = hv.gethistoricalModel();
			

			HistoricalVolume lhv = new HistoricalVolume(request.getParameter("datevar1"), request.getParameter("datevar2"));
			List<myHistoricalListModel> mylist2 = lhv.getLimitedhistoricalModel();
		 
			
%>
<%
final String variable1 = request.getParameter("datevar1"); 
final String variable2 = request.getParameter("datevar2"); 
%>
<body>
	<div class="container-fluid">
		<div class="row alert alert-warning" role="alert" style="">
			<form method="post" class="form-search">
				<fieldset>
				  <legend>This report gives you a result set for the date range queried.</legend>
				  <label>Date Interval:</label> <input class="required date" name="datevar1" type="text" value="<%=variable1 %>"> 
				  <label>To:</label> <input class="required date" name="datevar2" type="text" value="<%=variable2 %>" ><button style="margin-left:5px" class="btn btn-info" id="commit" type="submit">submit</button>
				  <br><label>Total Count of Referrals:</label> <input type="text" value="<%=HistoricalVolume.casecount() %>" readonly>
				  <br><label>Referrals held back:</label> <input style="margin-left: 26px;" type="text" value="<%=HistoricalVolume.limitedcasecount() %>" readonly><br>
				 </fieldset>
			</form>
		</div>
	</div>
	
	<div class="" style="">		
				<div class="row-fluid" id="myTable" style="margin-top:10px;" >
				<h3>Referrals held back</h3>
					<table class="table table-condensed table-hover" >
							 	<thead>
							 		<tr>
							 			<th class="col-md-1">Payer Name</th>
							 			<th class="col-md-1">Encounter Status</th>
							 			<th class="col-md-1">Patient Name</th>
							 			<th class="col-md-1">State</th>
							 			<th class="col-md-1">Receive Date</th>
							 			<th class="col-md-1">Date of Service</th>
							 			<th class="col-md-1">Rp Delivery Date</th>
							 			<th class="col-md-1">Rp FileId</th>
							 			<th class="col-md-1">Request Created</th>
							 			<th class="col-md-1">Request Delivered</th>
							 			<th class="col-md-1">Request Approved</th>
							 			<th class="col-md-1">Request Initial Appointment</th>
							 			<th class="col-md-1">Request Rx Reviewed</th>
							 			<th class="col-md-1">Request RP Review</th>
							 			<th class="col-md-1">Appointment StatusId</th>
							 			<th class="col-md-1">CPT</th>
							 			<th class="col-md-1">Modality</th>
							 			
							 		</tr>
							 	</thead>
								<%
									for(int x = 0; x < mylist2.size(); x++){
								%> 
								<tbody>
								
								 	<tr>
								 		<td><%=mylist.get(x).getPayername() %></td>
								 		<td><%=mylist.get(x).getEidstatus() %></td>
								 		<td><%=mylist.get(x).getPtfirstname() + " " + mylist.get(x).getPtlastname()%></td>
								 		<td><%=mylist.get(x).getState() %></td>
								 		<td><%=mylist.get(x).getRecdate() %></td>
								 		<td><%=mylist.get(x).getDos() %></td>
								 		<td><%=mylist.get(x).getRpdeliverydate() %></td>
								 		<td><%=mylist.get(x).getRpfileid() %></td>
								 		<td><%=mylist.get(x).getReqcreated() %></td>
								 		<td><%=mylist.get(x).getReqdelivered() %></td>
								 		<td><%=mylist.get(x).getReqapproved() %></td>
								 		<td><%=mylist.get(x).getReqinitialappt() %></td>
								 		<td><%=mylist.get(x).getReqrxrev() %></td>
								 		<td><%=mylist.get(x).getReqrprev() %></td>
								 		<td><%=mylist.get(x).getApptstatusid() %></td>
								 		<td><%=mylist.get(x).getCpt() %></td>
								 		<td><%=mylist.get(x).getModality() %></td>
								 	</tr>
								 </tbody>
								
								<%
									}
								%> 
								
					</table>
				</div>
			</div>
	<%
	}else{
		HistoricalVolume hv = new HistoricalVolume(request.getParameter("datevar1"), request.getParameter("datevar2"));
		List<myHistoricalListModel> mylist = hv.gethistoricalModel();
	%> 
		<div class="container-fluid">
		<div class="row alert alert-warning" role="alert" style="">
			<form method="post" class="form-search">
				<fieldset>
				  <legend>This report gives you a result set for the date range queried.</legend>
				  <label>Date Interval:</label> <input class="required date" name="datevar1" type="text"> <label>To:</label> <input class="required date" name="datevar2" type="text" ><button style="margin-left:5px" class="btn btn-info" id="commit" type="submit">submit</button>
				  <br><label>Total Count of Referrals:</label> <input type="text" value="<%=HistoricalVolume.casecount() %>" readonly>
				  <br><label>Referrals held back:</label> <input style="margin-left: 26px;" type="text" value="<%=HistoricalVolume.limitedcasecount() %>" readonly><br>
				 
				 </fieldset>
			</form>
		</div>
	</div>
		
	<%
		}
	%>
</body>

</html>