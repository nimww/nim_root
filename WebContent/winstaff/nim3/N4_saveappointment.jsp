<%@page import="java.io.*,com.winstaff.*, java.util.*, java.text.SimpleDateFormat,com.google.gson.Gson;"%>
<%
	class incomingAppointment{
		String scheduledate = "";
		String pid = "";
		String schedulerid;
		String en_id = "";
		String ca_id = "";
		public incomingAppointment(String scheduledate, String pid, String schedulerid, String en_id, String ca_id) {
			this.scheduledate = scheduledate;
			this.pid = pid;
			this.schedulerid = schedulerid;
			this.ca_id = ca_id;
			this.en_id = en_id;
		}
		
		public String getScheduledate() {
			return scheduledate;
		}
		public void setScheduledate(String scheduledate) {
			this.scheduledate = scheduledate;
		}
		
		public String getPid() {
			return pid;
		}
		public void setPid(String pid) {
			this.pid = pid;
		}
		
		public String getCa_id() {
			return ca_id;
		}
		public void setCa_id(String ca_id) {
			this.ca_id = ca_id;
		}
		
		public String getSchedulerid() {
			return schedulerid;
		}
		public void setSchedulerid(String schedulerid) {
			this.schedulerid = schedulerid;
		}
		
		public String getEn_id() {
			return en_id;
		}
		public void setEn_id(String en_id) {
			this.en_id = en_id;
		}
}
%>
<%
	try{
		String scheduledate = "";
		String pid = "";
		String schedulerid;
		String en_id = "";
		String ca_id = "";
	
		out.println(scheduledate + " " + en_id + " " + pid);
		scheduledate = request.getParameter("scheduledate");
		pid = request.getParameter("pid");
		schedulerid = request.getParameter("schedulerid");
		en_id = request.getParameter("en_id");
		ca_id = request.getParameter("ca_id");
		bltNIM3_Encounter en = new bltNIM3_Encounter(new Integer(en_id));
		bltNIM3_CommTrack ct = new bltNIM3_CommTrack();
		bltNIM3_Appointment ap = new bltNIM3_Appointment();
		SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		
		ap.setAppointmentTime(new Date(scheduledate));
		ap.setUniqueCreateDate(new Date());
		ap.setUniqueModifyDate(new Date());
		ap.setUniqueModifyComments("N4 generated appointment");
		ap.setScheduler_UserID(new Integer(schedulerid));
		ap.setICID(new Integer(pid));
		ap.setProviderID(new Integer(pid));
		ap.setInitialEncounterID(new Integer(en_id));
		ap.setComments("N4 Created");
		ap.setiStatus(1);
		ap.commitData();
		
		
		en.setAppointmentID(ap.getUniqueID());
		en.setDateOfService(new Date(scheduledate));
		en.commitData();
		
		
		out.println("ApppointmentId "+ ap.getUniqueID()+ " enid " +en_id + " " + schedulerid + " " + scheduledate);
			ct.setComments("Appointment set by " + schedulerid + " on " + (new Date()));
			ct.setIntUserID(new Integer(schedulerid));
			ct.setCaseID(new Integer(ca_id));
			ct.setMessageName(schedulerid.toString());
			ct.setEncounterID(new Integer(en_id));
			ct.setUniqueCreateDate(new Date());
			ct.setMessageSubject("[ N4] ");
			ct.setMessageText(" N4 " + new Date());
			ct.setMessageCompany("N4 "+"Appointment set by " + schedulerid + " on " + (new Date()));
			ct.setMessageSubject("Appointment set for " + scheduledate);
			ct.setCommStart(new Date());
			ct.setCommEnd(new Date());
			ct.commitData();
			
			out.println("Please Wait redirecting you now...");
			%>
			<script>
			 	document.location.href = "http://localhost:8080/n4/ca/<%=ca_id%>/<%=en_id%>";
			</script>	 			
			<% 
	}catch(Exception e){
		e.printStackTrace();
	}
%>