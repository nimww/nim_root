<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*, org.apache.commons.fileupload.*" %>
<%/*
    filename: tNIM3_Encounter_main_NIM3_Encounter_CaseID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0  >
    <tr><td width=10>&nbsp;</td><td>
      <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM3_Encounter")%>
      
      
      
  <%
//initial declaration of list class and parentID
    Integer        iDocumentID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
	Integer iAuthID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iDocumentID")&&pageControllerHash.containsKey("iEncounterID")) 
    {
	    iDocumentID = (Integer)pageControllerHash.get("iDocumentID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
	
//	bltNIM3_Encounter myEncounter = new bltNIM3_Encounter(iEncounterID);
//	bltNIM3_Appointment myAppointment = new bltNIM3_Appointment(myEncounter.getAppointmentID());
//	bltNIM3_Referral myReferral = new bltNIM3_Referral(myEncounter.getReferralID());
//	bltNIM3_CaseAccount myCaseAccount = new bltNIM3_CaseAccount(myReferral.getCaseID());
//	bltNIM3_PayerMaster myPayerMaster = new bltNIM3_PayerMaster(myCaseAccount.getPayerID());
//	bltPracticeMaster myPracticeMaster = new bltPracticeMaster(myAppointment.getProviderID());
	
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
		UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
	bltNIM3_Document myDoc = new bltNIM3_Document(iDocumentID);
	myDoc.setDocName(request.getParameter("DocType"));
	myDoc.setUniqueModifyComments(UserLogonDescription);
	myDoc.commitData();
	
	//start calc Billing...
//	out.print(NIMUtils.ReCalcServiceBilling(iEncounterID));
	//end calc billing
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
      
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td><h1> Document Uploaded</h1></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    </table>
<br>
<script language="javascript">
alert("Document Uploaded");
//document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp';
window.close();
</script>

