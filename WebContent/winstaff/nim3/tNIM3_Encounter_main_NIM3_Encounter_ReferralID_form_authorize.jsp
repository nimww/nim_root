<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.* " %>
<%/*
    filename: out\jsp\tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
<%@ include file="../generic/generalDisplay.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iReferralID        =    null;
    Integer        iCaseID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
	Integer requestID = null;
	if (request.getParameter("EDITID")!=null)
	{
		requestID = new Integer(request.getParameter("EDITID"));
		out.println(requestID);
	}
	bltNIM3_Encounter        working_bltNIM3_Encounter = null;
	
	// Flag to indicate a custom email
	boolean customEmail = false;
	if (request.getParameter("FLAG")==null) {
		
	} else {
		customEmail = true;
	}
	
	
	if (isGenAdmin)
	{
        if (request.getParameter("EDIT").equalsIgnoreCase("sched")&&request.getParameter("PreSchedule")!=null&&request.getParameter("PreSchedule").equalsIgnoreCase("nd"))
        {
//            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize")   ;
			pageControllerHash.put("iEncounterID",requestID);
			pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
			session.setAttribute("pageControllerHash",pageControllerHash);
			String parameterPassString ="";
			java.util.Enumeration myParameterPassList = request.getParameterNames();
			while (myParameterPassList.hasMoreElements())
			{
				String myName = (String)myParameterPassList.nextElement();
				String myS = (String) request.getParameter(myName);
				parameterPassString+="&"+myName + "=" + myS;
			}
            String targetRedirect = "tImaging_schedule.jsp?nullParam=null"+parameterPassString    ;
	        response.sendRedirect(targetRedirect);
        }
	}

    if (pageControllerHash.containsKey("iReferralID")) 
    {
        iReferralID        =    (Integer)pageControllerHash.get("iReferralID");
		bltNIM3_Encounter_List        bltNIM3_Encounter_List        =    new    bltNIM3_Encounter_List(iReferralID,"EncounterID="+requestID,"");
	
	//declaration of Enumeration
		ListElement         leCurrentElement;
		java.util.Enumeration eList = bltNIM3_Encounter_List.elements();
		%>
		<%
		if (eList.hasMoreElements())
		{
			leCurrentElement    = (ListElement) eList.nextElement();
			working_bltNIM3_Encounter  = (bltNIM3_Encounter) leCurrentElement.getObject();
		}
		
        accessValid = true;
    }
	else if (pageControllerHash.containsKey("iCaseID")) 
	{
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
		searchDB2 mySS = new searchDB2();
		
		java.sql.ResultSet myRS = null;
		String mySQL = "select tNIM3_Encounter.encounterid, tNIM3_Referral.referralid from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralID INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid where tNIM3_Encounter.encounterid = " + requestID  + " AND tNIM3_CaseAccount.caseid = " + iCaseID;
		//out.print(mySQL);
		myRS = mySS.executeStatement(mySQL);
		try
		{
		   if (myRS!=null&&myRS.next())
		   {
			   working_bltNIM3_Encounter = new bltNIM3_Encounter(new Integer(myRS.getString("encounterid")));
	           pageControllerHash.put("iReferralID",new Integer(myRS.getString("referralid")) );
		       accessValid = true;
		   }
		}
		catch (Exception eAuth)
		{
			
		}
		mySS.closeAll();
		
	}
	else
	{
	}
  //page security
  if (accessValid)
  {
	  {
        pageControllerHash.put("iEncounterID",working_bltNIM3_Encounter.getEncounterID());
        pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
		String parameterPassString ="";
		java.util.Enumeration myParameterPassList = request.getParameterNames();
		while (myParameterPassList.hasMoreElements())
		{
			String myName = (String)myParameterPassList.nextElement();
			String myS = (String) request.getParameter(myName);
			parameterPassString+="&"+myName + "=" + myS;
		}
	boolean noResponse = false;
        String targetRedirect = "tNIM3_EncounterService_form.jsp?nullParam=null"+parameterPassString    ;
        if (request.getParameter("EDIT").equalsIgnoreCase("del"))
        {
            targetRedirect = "tNIM3_Encounter_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("addct"))
        {
            targetRedirect = "tNIM3_CommTrack_main_NIM3_CommTrack_CaseID_form_create.jsp?nullParam=null"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("nim3exp"))
        {
            targetRedirect = "tNIM3_CaseAccount_express1_form.jsp?nullParam=null"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("nim3exp_svc"))
        {
            targetRedirect = "tNIM3_CaseAccount_express1_svc_form.jsp?nullParam=null"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("nim3expbill"))
        {
            targetRedirect = "tNIM3_Encounter_billing.jsp?nullParam=null"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("nim3track"))
        {
            targetRedirect = "tNIM3_Encounter_track.jsp?nullParam=null"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("nim3expbill2"))
        {
            targetRedirect = "tNIM3_ServiceBilling.jsp?nullParam=null"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("addctold"))
        {
//            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize")   ;
//            targetRedirect = "tNIM3_Encounter_upReport.jsp";  ;
            targetRedirect = "tNIM3_CommTrack_main_NIM3_CommTrack_CaseID_form_create.jsp?nullParam=null"+parameterPassString    ;
			%>
            <script language="javascript">
			modalPost('AddCommtrack', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_CommTrack_main_NIM3_CommTrack_CaseID_form_create.jsp&nullParam=null<%=parameterPassString%>','AddCommtrack','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));
			document.location = '<%=targetRedirect%>';
			</script>
            <%
			noResponse = true;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("toggle_flag"))
        {
			if (working_bltNIM3_Encounter.getEncounterStatusID().intValue()==1)
			{
				working_bltNIM3_Encounter.setEncounterStatusID(new Integer(4));
				working_bltNIM3_Encounter.commitData();
			}
			else if (working_bltNIM3_Encounter.getEncounterStatusID().intValue()==4)
			{
				working_bltNIM3_Encounter.setEncounterStatusID(new Integer(1));
				working_bltNIM3_Encounter.commitData();
			}
			else 
			{
				%>
				<script language="javascript">
				alert("You can only Flag an Encounter that is Active.  If a case is Voided, Closed, in Billing, etc. then you can''t change the Flag status by clicking on this button.");
				</script>
                <%
			}
			targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp";
//			noResponse = true;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("dicom_pl"))
        {
//            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize")   ;
//            targetRedirect = "tNIM3_Encounter_upReport.jsp";  ;
//            targetRedirect = "tNIM3_CommTrack_main_NIM3_CommTrack_CaseID_form_create.jsp?nullParam=null"+parameterPassString    ;
			targetRedirect = "route_pl.jsp";
			%>
            <script language="javascript">
//			window.open('LI_ModalPass.jsp?ssPF=route_pl.jsp&nullParam=null<%=parameterPassString%>','DICOMPL','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));
			document.location = '<%=targetRedirect%>';
			</script>
            <%
//			noResponse = true;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("dicom_pls"))
        {
//            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize")   ;
//            targetRedirect = "tNIM3_Encounter_upReport.jsp";  ;
//            targetRedirect = "tNIM3_CommTrack_main_NIM3_CommTrack_CaseID_form_create.jsp?nullParam=null"+parameterPassString    ;
			try
			{
				if (request.getParameter("SEDITID")!=null&&!request.getParameter("SEDITID").equalsIgnoreCase(""))
				{
					Integer tempServiceID = new Integer(request.getParameter("SEDITID"));
					bltNIM3_Service myTempService = new bltNIM3_Service(tempServiceID);
					if (myTempService.getEncounterID().intValue()==working_bltNIM3_Encounter.getEncounterID().intValue())
					{
																							 
						pageControllerHash.put("iServiceID",tempServiceID);
						session.setAttribute("pageControllerHash",pageControllerHash);
						
						targetRedirect = "route_pl_service.jsp";
						%>
						<script language="javascript">
			//			window.open('LI_ModalPass.jsp?ssPF=route_pl.jsp&nullParam=null<%=parameterPassString%>','DICOMPL','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));
						document.location = '<%=targetRedirect%>';
						</script>
						<%
					}
					else
					{
						%>
						<script language="javascript">
						alert("Error SVC=ENC MAthc");
						</script>
						<%
					}
				}
				else
				{
						%>
						<script language="javascript">
						alert("invalid SEDIT");
						</script>
						<%
				}
			}
			catch (Exception eeeee)
			{
						%>
						<script language="javascript">
						alert("Error <%=eeeee%>");
						</script>
						
						<%
			}
//			noResponse = true;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("updoc")||request.getParameter("EDIT").equalsIgnoreCase("upreport")||request.getParameter("EDIT").equalsIgnoreCase("uphcfapay")||request.getParameter("EDIT").equalsIgnoreCase("uphcfapro"))
        {
//            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize")   ;
//            targetRedirect = "tNIM3_Encounter_upReport.jsp";  ;
			targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp?SelectedEncounterID="+requestID;
			%>
            <script language="javascript">
			modalPost('UploadDoc', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_upDoc.jsp&nullParam=null<%=parameterPassString%>','UploadDoc','dialogWidth:1000px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1000,height=800'));
			document.location = '<%=targetRedirect%>';
			</script>
            <%
			noResponse = true;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("nad"))
        {
//            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize")   ;
//            targetRedirect = "tNIM3_Encounter_upReport.jsp";  ;
			targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp?SelectedEncounterID="+requestID;
			%>
            <script language="javascript">
			modalPost('NAD', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_nad.jsp&nullParam=null<%=parameterPassString%>','NAD','dialogWidth:1000px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1000,height=800'));
			document.location = '<%=targetRedirect%>';
			</script>
            <%
			noResponse = true;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("sched"))
        {
//            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize")   ;
            targetRedirect = "tImaging_schedule.jsp?nullParam=null"+parameterPassString    ;
        }
        else if (customEmail && (request.getParameter("EDIT").equalsIgnoreCase("send_sp_adj")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_ncm")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_adm")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_adm2")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_adm3")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_adm4")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_ic")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_refdr")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_ncm")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_adj")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_adm")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_adm2")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_adm3")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_adm4")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_refdr")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_adj")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_ncm")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_adm")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_adm2")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_adm3")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_adm4")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_refdr")   ||request.getParameter("EDIT").equalsIgnoreCase("emaila_rc_all")||request.getParameter("EDIT").equalsIgnoreCase("emaila_sp_all")||request.getParameter("EDIT").equalsIgnoreCase("emaila_rp_all")))
        {
//            targetRedirect = "tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp"   ;
            targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp?SelectedEncounterID="+requestID;
			%>
            <script language="javascript">
			modalPost('PRINTSP', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_sendAlert.jsp&nullParam=null<%=parameterPassString%>&customEmail=1','PRINTSP','dialogWidth:1200px;dialogHeight:1000px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=1000'));
			document.location = '<%=targetRedirect%>';
			</script>
            <%
			noResponse = true;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("send_sp_adj")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_ncm")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_adm")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_adm2")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_adm3")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_adm4")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_ic")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_refdr")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_ncm")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_adj")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_adm")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_adm2")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_adm3")||request.getParameter("EDIT").equalsIgnoreCase("send_rp_adm4")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_refdr")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_adj")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_ncm")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_adm")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_adm2")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_adm3")||request.getParameter("EDIT").equalsIgnoreCase("send_rc_adm4")||request.getParameter("EDIT").equalsIgnoreCase("send_sp_refdr")   ||request.getParameter("EDIT").equalsIgnoreCase("emaila_rc_all")||request.getParameter("EDIT").equalsIgnoreCase("emaila_sp_all")||request.getParameter("EDIT").equalsIgnoreCase("emaila_rp_all")||request.getParameter("EDIT").equalsIgnoreCase("email_rc_pt"))
        {
//            targetRedirect = "tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp"   ;
            targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp?SelectedEncounterID="+requestID;
			%>
            <script language="javascript">
			modalPost('PRINTSP', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_sendAlert.jsp&nullParam=null<%=parameterPassString%>&customEmail=0','PRINTSP','dialogWidth:1200px;dialogHeight:1000px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=1000'));
			document.location = '<%=targetRedirect%>';
			</script>
            <%
			noResponse = true;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("enc_problem"))
        {
//            targetRedirect = "tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp"   ;
            targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp?SelectedEncounterID="+requestID;
			%>
            <script language="javascript">
			modalPost('PRINTSP', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_Problem.jsp&nullParam=null<%=parameterPassString%>','PRINTSP','dialogWidth:1200px;dialogHeight:1000px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=1000'));
			document.location = '<%=targetRedirect%>';
			</script>
            <%
			noResponse = true;
        }
		else if (request.getParameter("EDIT").equalsIgnoreCase("print_rem_pro"))
        {
//            targetRedirect = "tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp"   ;
            targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp?SelectedEncounterID="+requestID;

			if (working_bltNIM3_Encounter.getTimeTrack_ReqPaidOut().before( new java.util.Date(1)))
			{
//				working_bltNIM3_Encounter.setTimeTrack_ReqPaidOut(PLCUtils.getNowDate(false));
//				working_bltNIM3_Encounter.commitData();
			}
			%>
            <script language="javascript">
			modalPost('PRINTBILL', modalWin('tNIM3_Encounter_bill-rem_print.jsp?nullParam=null<%=parameterPassString%><%=parameterPassString%>','PRINTBILL','dialogWidth:1200px;dialogHeight:1000px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=1000'));
			document.location = '<%=targetRedirect%>';
			</script>
            <%
			noResponse = true;
        }
		else if (request.getParameter("EDIT").equalsIgnoreCase("print_bill_pay"))
        {
//            targetRedirect = "tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp"   ;
            targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp?SelectedEncounterID="+requestID;

			if (working_bltNIM3_Encounter.getSentTo_Bill_Pay().before(NIMUtils.getBeforeTime()))
			{
				working_bltNIM3_Encounter.setSentTo_Bill_Pay(PLCUtils.getNowDate(true));
				working_bltNIM3_Encounter.commitData();
			}
			%>
            <script language="javascript">
			modalPost('PRINTBILL', modalWin('tNIM3_Encounter_bill-pay_print.jsp?nullParam=null<%=parameterPassString%><%=parameterPassString%>','PRINTBILL','dialogWidth:1200px;dialogHeight:1000px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=1000'));
			document.location = '<%=targetRedirect%>';
			</script>
            <%
			noResponse = true;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("rxreview"))
        {
//            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize")   ;
//            targetRedirect = "tNIM3_Encounter_upReport.jsp";  ;
			targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp?SelectedEncounterID="+requestID;
			%>
            <script language="javascript">
			modalPost('ReviewD', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_RxReview.jsp&nullParam=null<%=parameterPassString%>','ReviewD','dialogWidth:1700px;dialogHeight:1000px','status=yes,scrollbars=yes,resizable=yes,width=1700,height=900'));
			document.location = '<%=targetRedirect%>';
			</script>
            <%
			noResponse = true;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("rpreview"))
        {
//            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize")   ;
//            targetRedirect = "tNIM3_Encounter_upReport.jsp";  ;
			targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp?SelectedEncounterID="+requestID;
			%>
            <script language="javascript">
			modalPost('ReviewD', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_RpReview.jsp&nullParam=null<%=parameterPassString%>','ReviewD','dialogWidth:1700px;dialogHeight:1000px','status=yes,scrollbars=yes,resizable=yes,width=1700,height=900'));
			document.location = '<%=targetRedirect%>';
			</script>
            <%
			noResponse = true;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("openflow"))
        {
//            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize")   ;
            targetRedirect = "tNIM3_Service_main_NIM3_Service_EncounterID_Express.jsp";  ;
        }
		if (!noResponse)
		{
	        response.sendRedirect(targetRedirect);
		}
    }

  }
  else
  {
   out.println("illegal");
  }
}
else
{
out.println("Your Security Level does not permit you to View this.");
}
%>