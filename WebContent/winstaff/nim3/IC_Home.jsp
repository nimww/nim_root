<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
</head>
<SCRIPT language="JavaScript1.2">
function openStatus()
{
	testwindow= window.open ("Home_Status.jsp", "HomeStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(0,0);
}

function openAuto(myAuto)
{
	testwindow= window.open ("report_auto2.jsp?rid=" + myAuto + "&MREF=300", "AutoStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(100,100);
}

</SCRIPT>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_icid.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isIC) 
    {
        accessValid = true;
    }
	
	
  //page security
  if (accessValid)
  {


String myScanPass = "";
String myPatientFirstName = "";
String myPatientLastName = "";



try
{
	myScanPass = request.getParameter("ScanPass").toLowerCase().trim();
	myPatientFirstName = request.getParameter("PatientFirstName").toLowerCase().trim();
	myPatientLastName = request.getParameter("PatientLastName").toLowerCase().trim();
}
catch (Exception e)
{
	myScanPass = "";
	myPatientFirstName = "";
	myPatientLastName = "";
}






	java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	
	  if (isIC)
	  {
		  %>
<table width="<%=MasterTableWidth%>" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td>
    	<table width="100%" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class=title>Welcome to NextImage Medical.</td>
          </tr>
        <tr>
          <td colspan="2" align="left" class="tdHeader"><table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td class="tdHeader">Last 10 Appointments Matchg your Search Criteria</td>
              </tr>
            <tr>
              <td class="tdHeader"><form name="form1" method="post" action="#"><table border="0" cellspacing="0" cellpadding="3">
                <tr>
                  <td>Patient First</td>
                  <td>
                    <input name="PatientFirstName" type="text" id="PatientFirstName" value="<%=myPatientFirstName%>" size="12">
                    </td>
                  <td>Patient Last</td>
                  <td><input name="PatientLastName" type="text" id="PatientLastName"  value="<%=myPatientLastName%>" size="12"></td>
                </tr>
                <tr>
                  <td>ScanPass</td>
                  <td><input name="ScanPass" type="text" id="ScanPass"  value="<%=myScanPass%>" size="12"></td>
                  <td colspan="2" align="left"><input name="input" type="submit" value="Submit">
                    <input name="input2" type="button"  onClick="document.getElementById('ScanPass').value='';document.getElementById('PatientFirstName').value='';document.getElementById('PatientLastName').value='';" value="Clear"></td>
                </tr>
                </table></form></td>
            </tr>
            <tr>
              <td><table border="0" cellspacing="0" cellpadding="4">
              
              
  <!-- test -->
  <%
searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(null);
String myWhere = " (1=1) ";
boolean theFirst = false;

	if (!myScanPass.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myScanPass.indexOf("%")>=0)
		{
			myWhere += "lower(\"Encounter_ScanPass\") LIKE lower(?)";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myScanPass));
		}
		else if (myScanPass.indexOf("=")==0)
		{
			myWhere += "lower(\"Encounter_ScanPass\") = lower(?)";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myScanPass.substring(1, myScanPass.length())));
		}
		else if (myScanPass.length()<=3)
		{
			myWhere += "lower(\"Encounter_ScanPass\") LIKE lower(?)";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myScanPass + "%"));
		}
		else 
		{
			myWhere += "lower(\"Encounter_ScanPass\") LIKE lower(?)";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myScanPass + "%"));
		}
		theFirst = false;
	}
	if (!myPatientFirstName.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myPatientFirstName.indexOf("%")>=0)
		{
			myWhere += "lower(\"PatientFirstName\") LIKE lower(?)";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPatientFirstName));
		}
		else if (myPatientFirstName.indexOf("=")==0)
		{
			myWhere += "lower(\"PatientFirstName\") = lower(?)";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPatientFirstName.substring(1, myPatientFirstName.length())));
		}
		else if (myPatientFirstName.length()<=3)
		{
			myWhere += "lower(\"PatientFirstName\") LIKE lower(?)";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPatientFirstName + "%"));
		}
		else 
		{
			myWhere += "lower(\"PatientFirstName\") LIKE lower(?)";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myPatientFirstName + "%"));
		}
		theFirst = false;
	}
	if (!myPatientLastName.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myPatientLastName.indexOf("%")>=0)
		{
			myWhere += "lower(\"PatientLastName\") LIKE lower(?)";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPatientLastName));
		}
		else if (myPatientLastName.indexOf("=")==0)
		{
			myWhere += "lower(\"PatientLastName\") = lower(?)";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPatientLastName.substring(1, myPatientLastName.length())));
		}
		else if (myPatientLastName.length()<=3)
		{
			myWhere += "lower(\"PatientLastName\") LIKE lower(?)";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPatientLastName + "%"));
		}
		else 
		{
			myWhere += "lower(\"PatientLastName\") LIKE lower(?)";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myPatientLastName + "%"));
		}
		theFirst = false;
	}



String mySQL = "SELECT \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"EncounterID\", \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PracticeID\" FROM \"public\".\"vMQ4_Encounter_NoVoid_BP\" where " + myWhere + " AND \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"PracticeID\" = ? Order By \"public\".\"vMQ4_Encounter_NoVoid_BP\".\"Appointment_AppointmentTime\" DESC Limit 10";
//out.print(mySQL);
try
{
	myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,CurrentUserAccount.getReferenceID().toString()));
	myDPSO.setSQL(mySQL);
	myRS = mySS.executePreparedStatement(myDPSO);
	//myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,"0"));
	//myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myUserName));
	myRS = mySS.executePreparedStatement(myDPSO);
}
catch(Exception e)
{
			DebugLogger.println("WEB:IC_Home.jsp:[MYSQL="+mySQL+"]:"+e);
}

try
{

	int endCount = 0;
	
	int cnt=0;
	int cnt2=0;
	while (myRS!=null&&myRS.next())
	{
		cnt++;
		if (true)
		{
			NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(new Integer(myRS.getString("EncounterID")),"IC Home Load");
			cnt2++;
			String myClass = "tdBase";
			Integer iDays = PLCUtils.getDaysTill(myEO2.getNIM3_Appointment().getAppointmentTime());
			if (cnt2%2!=0)
			{
				myClass = "tdBaseAlt";
			}
			out.print("<tr class="+myClass+">");
			%>
              <tr class="tdHeaderAlt">
                <td>Patient Info</td>
                <td>Procedure</td>
                <td>Special Instructions</td>
                <td>Appointment Time</td>
              </tr>
              <tr class="<%=myClass%>">
                <td nowrap><%="SP: <strong>" + myEO2.getNIM3_Encounter().getScanPass()%></strong><br/><br/><strong><span class="title"><%=myEO2.getNIM3_CaseAccount().getPatientLastName()%>, <%=myEO2.getNIM3_CaseAccount().getPatientFirstName()%></span></strong>
               <br>
            <br>
            <%="Injury Date: <strong>" + PLCUtils.getDisplayDate(myEO2.getNIM3_CaseAccount().getDateOfInjury(),false)%></strong>
            <br>
			<%="Phone: <strong>" + myEO2.getNIM3_CaseAccount().getPatientHomePhone() + "</strong>&nbsp;&nbsp;Cell: <strong>" + myEO2.getNIM3_CaseAccount().getPatientCellPhone()%></strong>
            <br>
			<%="DOB: <strong>" + PLCUtils.getDisplayDate(myEO2.getNIM3_CaseAccount().getPatientDOB(),false)%></strong>
			<br>
			<%="Height: " +myEO2.getNIM3_CaseAccount().getPatientHeight() + "&nbsp;&nbsp;&nbsp;" + "Weight: " +myEO2.getNIM3_CaseAccount().getPatientWeight()%>
               <br>
            <%=myEO2.getNIM3_CaseAccount().getPatientAddress1() + " "  + myEO2.getNIM3_CaseAccount().getPatientAddress2()%>
            <br>
            <%=myEO2.getNIM3_CaseAccount().getPatientCity() + ", "  + new bltStateLI(myEO2.getNIM3_CaseAccount().getPatientStateID()).getShortState() + " " +  myEO2.getNIM3_CaseAccount().getPatientZIP()%>
                </td>
                <td><%
					bltNIM3_Service        working_bltNIM3_Service;
					ListElement         leCurrentElement_Service;
					java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
					int cnt55=0;
					while (eList_Service.hasMoreElements())
					{
						cnt55++;
						leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
						working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
						if ( working_bltNIM3_Service.getServiceStatusID()==1)
						{
							if (cnt55>1)
							{
								out.println("<br/>");
							}
							if ( !working_bltNIM3_Service.getCPT().equalsIgnoreCase(""))
							{
			//                    theBody += "\t" + NIMUtils.getCPTText(working_bltNIM3_Service.getCPT()) + "";
							}
							if ( !working_bltNIM3_Service.getCPTBodyPart().equalsIgnoreCase(""))
							{
								out.println(working_bltNIM3_Service.getCPTBodyPart());
							}
							if ( !working_bltNIM3_Service.getCPTText().equalsIgnoreCase(""))
							{
								out.println("&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;Diag/Rule-Out:&nbsp;&nbsp;" + working_bltNIM3_Service.getCPTText());
							}
						}
		
					}
					out.println("<br/>Procedure Notes: " + myEO2.getNIM3_Encounter().getComments());
					out.println("<br/>* Please verify all Procedure details with attached prescription");
						
				
				%>
                

                
                </td>
                <td nowrap><%
            if (myEO2.getNIM3_Encounter().getisSTAT()==1)
            {
                %><br>STAT Order: Yes<%
            }
            if (myEO2.getNIM3_Encounter().getRequiresFilms()==1)
            {
                %><br>Hand Carry Films: Yes<%
            }
            if (myEO2.getNIM3_Encounter().getRequiresAgeInjury()==1)
            {
                %><br>Age Injury: Yes<%
            }				
				%></td>
                
                 <td align="center"  class="borderHighlightGreen"><%
java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);
java.text.SimpleDateFormat displayDateHourMin = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHourMin);
java.util.Date dbAppointment = myEO2.getNIM3_Appointment().getAppointmentTime();

String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
String sAppointmentHour = displayDateHour.format((dbAppointment));
String sAppointmentHourMin = displayDateHourMin.format((dbAppointment));

%>
                   <span class="tdHeader"><%=PLCUtils.getDaysTill_Display(myEO2.getNIM3_Appointment().getAppointmentTime(), true)%></span><br>
<%=sAppointmentTimeDayWeek%>&nbsp;<%=sAppointmentTimeMonth%>&nbsp;<%=sAppointmentTimeDay%>&nbsp;<%=sAppointmentTimeYear%><br/>at <%=sAppointmentHourMin%>

<br>
					<%
					if (myEO2.getNIM3_Referral().getRxFileID()==0)
					{
					%>
                   <input  class="inputButton_md_Stop"  type="button" onClick="alert('Rx is not currently available.\n\nPlease call NextImage if you have any questions: 888-318-5111');" value="View Rx" />
                   <%
					}
					else
					{
					%>
                   <input  class="inputButton_md_Default"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_ICID_form_authorize.jsp?EDIT=view&EDITID=<%=myEO2.getNIM3_Referral().getRxFileID()%>&KM=p';" value="View Rx" />
                   <%
					}
                   %>

<br>



					<%
					if (myEO2.getNIM3_Encounter().getReportFileID()!=0)
					{
					%>
                   <input  class="inputButton_md_Stop"  type="button" onClick="alert('Report has already been uploaded.\n\nIf you need to make a correction, please please call NextImage at 888-318-5111');" value="Upload Report" />
                   <%
					}
					else
					{
					%>
                   <input  class="inputButton_md_Default"  type="button" onClick="alert('*** Please upload report in PDF Format ***');this.disabled=false;document.location='IC_UploadReport.jsp?EDIT=view&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Upload Report" />
                   <%
					}
                   %>

<br>
                   <input  class="inputButton_md_Default"  type="button" onClick="this.disabled=false;document.location='IC_UploadDICOM.jsp?EDIT=view&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p';" value="Upload DICOM" />
                   
                   

</td>
                
              </tr>
            <%
		}
	}
	  if (cnt==0)
	  {
		  %>
          <h2>No Matching Results</h2>
          <%
	  }
	mySS.closeAll();
	endCount = cnt;
}
catch(Exception eee)
{
	out.print("item list error: " + eee);
}

%>
              
              
              
          </table></td>
              </tr>
          </table></td>
        </tr>
	    </table>
    </td>
  </tr>
</table>

    <%
	  }
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>