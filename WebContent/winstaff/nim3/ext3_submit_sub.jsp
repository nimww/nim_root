<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import=" com.winstaff.*" %>
<%/*
    filename: out\jsp\tNIM3_CaseAccount_form_sub.jsp
    Created on Jan/05/2009
    Created by: Scott Ellis
*/%>



<%

try
{
//initial declaration of list class and parentID
  java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
  java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID
Integer commTrackID = null;
try
{
    String testObj = new String(request.getParameter("commID")) ;
	commTrackID = new Integer(testObj);
}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount CaseClaimNumber1 not set. this is ok-not an error");
}




	bltNIM3_CaseAccount        NIM3_CaseAccount        =       new    bltNIM3_CaseAccount();
    bltNIM3_Referral        NIM3_Referral        =       new    bltNIM3_Referral();
    bltNIM3_Encounter        NIM3_Encounter        =       new    bltNIM3_Encounter();
    bltNIM3_CommTrack        NIM3_CommTrack        =       new    bltNIM3_CommTrack(commTrackID);

Integer UserSecurityGroupID = new Integer (2);
String testChangeID = "0";
String testChangeIDMSG = "0";
//for CaseAccount
try
{
    String testObj = new String(request.getParameter("CaseClaimNumber1")) ;
    if ( !NIM3_CaseAccount.getCaseClaimNumber().equals(testObj)  )
    {
         NIM3_CaseAccount.setCaseClaimNumber( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount CaseClaimNumber1 not set. this is ok-not an error");
}

String sComments = "";
String rComments = "";

try
{
    String testObj = new String(request.getParameter("PayerName")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         sComments+="\nPayer Name: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PayerName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AdjusterName")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         sComments+="\nAdjuster Name: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount AdjusterName not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("PayerPhone")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         sComments+="\nAdj. Phone: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PayerPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PayerFax")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         sComments+="\nAdj. Fax: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PayerFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PayerEmail")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         sComments+="\nAdj Email: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PayerEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ExtraNotes")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         sComments+="\nExtra Notes: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PayerEmail not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("PatientFirstName")) ;
    if ( !NIM3_CaseAccount.getPatientFirstName().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientFirstName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientLastName")) ;
    if ( !NIM3_CaseAccount.getPatientLastName().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientLastName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientLastName not set. this is ok-not an error");
}


try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("PatientDOB"))) ;
  //  String testObj = new String(request.getParameter("PatientDOB")) ;
    if ( !NIM3_CaseAccount.getPatientDOB().equals(testObj)  )
    {
         sComments+="\nPatient DOB: " + testObj;
         NIM3_CaseAccount.setPatientDOB( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }
	else
	{
         sComments+="\nPatient DOB: " + request.getParameter("PatientDOB") ;
	}

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientDOB not set. this is ok-not an error");
         sComments+="\nPatient DOB: " + request.getParameter("PatientDOB") ;
         testChangeID = "1";
}

try
{
    String testObj = new String(request.getParameter("PatientSSN")) ;
    if ( !NIM3_CaseAccount.getPatientSSN().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientSSN( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientSSN not set. this is ok-not an error");
}


try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateOfInjury"))) ;
//    String testObj = new String(request.getParameter("DateOfInjury")) ;
    if ( !NIM3_CaseAccount.getDateOfInjury().equals(testObj)  )
    {
         sComments+="\nPatient DOI: " + testObj;
         NIM3_CaseAccount.setDateOfInjury( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }
	else
	{
         sComments+="\nPatient DOI: " + request.getParameter("DateOfInjury") ;
	}

}
catch(Exception e)
{
	 sComments+="\nPatient DOI: " + request.getParameter("DateOfInjury");
	 testChangeID = "1";
     //out.println("NIM3_CaseAccount PatientDOB not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("EmployerPhone")) ;
    if ( !NIM3_CaseAccount.getEmployerPhone().equals(testObj)  )
    {
         NIM3_CaseAccount.setEmployerPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("EmployerName")) ;
    if ( !NIM3_CaseAccount.getEmployerName().equals(testObj)  )
    {
         NIM3_CaseAccount.setEmployerName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientFirstName not set. this is ok-not an error");
}




try
{
    String testObj = new String(request.getParameter("PatientHomePhone")) ;
    if ( !NIM3_CaseAccount.getPatientHomePhone().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientHomePhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientHomePhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientCellPhone")) ;
    if ( !NIM3_CaseAccount.getPatientCellPhone().equals(testObj)  )
    {
         NIM3_CaseAccount.setPatientCellPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientCellPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReferringPhysicianName")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nRef. Dr Name: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReferringPhysicianPhone")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nRef. Dr Phone: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ReferringPhysicianFax")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         rComments+="\nRef. Dr Fax: " + testObj;
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter ReferringPhysicianPhone not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("CPTText")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         NIM3_Encounter.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter CPTText not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PayerPreAuthorizationConfirmation")) ;
    if ( !testObj.equalsIgnoreCase("")  )
    {
         NIM3_Referral.setPreAuthorizationConfirmation( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM2_Authorization PayerPreAuthorizationConfirmation not set. this is ok-not an error");
}




//for CommTrack

try
{
    Integer testObj = new Integer(request.getParameter("IntUserID")) ;
    if ( !NIM3_CommTrack.getIntUserID().equals(testObj)  )
    {
         NIM3_CommTrack.setIntUserID( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack IntUserID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ExtUserID")) ;
    if ( !NIM3_CommTrack.getExtUserID().equals(testObj)  )
    {
         NIM3_CommTrack.setExtUserID( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack ExtUserID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CommTypeID")) ;
    if ( !NIM3_CommTrack.getCommTypeID().equals(testObj)  )
    {
         NIM3_CommTrack.setCommTypeID( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack CommTypeID not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("CommStart"))) ;
    if ( !NIM3_CommTrack.getCommStart().equals(testObj)  )
    {
         NIM3_CommTrack.setCommStart( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack CommStart not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("CommEnd"))) ;
    if ( !NIM3_CommTrack.getCommEnd().equals(testObj)  )
    {
         NIM3_CommTrack.setCommEnd( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack CommEnd not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageText")) ;
    if ( !NIM3_CommTrack.getMessageText().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageText( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageText not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageName")) ;
    if ( !NIM3_CommTrack.getMessageName().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageName( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageCompany")) ;
    if ( !NIM3_CommTrack.getMessageCompany().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageCompany( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageCompany not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageEmail")) ;
    if ( !NIM3_CommTrack.getMessageEmail().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageEmail( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessagePhone")) ;
    if ( !NIM3_CommTrack.getMessagePhone().equals(testObj)  )
    {
         NIM3_CommTrack.setMessagePhone( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessagePhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MessageFax")) ;
    if ( !NIM3_CommTrack.getMessageFax().equals(testObj)  )
    {
         NIM3_CommTrack.setMessageFax( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack MessageFax not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AlertStatusCode")) ;
    if ( !NIM3_CommTrack.getAlertStatusCode().equals(testObj)  )
    {
         NIM3_CommTrack.setAlertStatusCode( testObj,UserSecurityGroupID   );
         testChangeIDMSG = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CommTrack AlertStatusCode not set. this is ok-not an error");
}


String ip = request.getRemoteAddr();
String host = request.getRemoteHost();


if (testChangeID.equalsIgnoreCase("1"))
{
	out.println("Success Code: 102");
	//autosets Case Account
	NIM3_CaseAccount.setPayerID(new Integer(50));
	NIM3_CaseAccount.setCaseStatusID(new Integer(1));
	NIM3_CaseAccount.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_CaseAccount.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_CaseAccount.setUniqueModifyComments("Created by Backup Call Service");
	NIM3_CaseAccount.commitData();
	NIM3_CaseAccount.setCaseCode("CP" +  NIM3_CaseAccount.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
	NIM3_CaseAccount.setComments(sComments);
	NIM3_CaseAccount.commitData();
	

	//autosets Encounter
	NIM3_Referral.setCaseID(NIM3_CaseAccount.getUniqueID());
	NIM3_Referral.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_Referral.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_Referral.setUniqueModifyComments("Created by Backup Call Service");
	NIM3_Referral.setComments(rComments);
	NIM3_Referral.setReferralTypeID(new Integer(3));
	NIM3_Referral.setReferralStatusID(new Integer(1));
	NIM3_Referral.setAttendingPhysicianID(new Integer(52));
	NIM3_Referral.setReferralDate(PLCUtils.getNowDate(true));
	NIM3_Referral.setReceiveDate(PLCUtils.getNowDate(true));
	NIM3_Referral.commitData();
	
	//autosets Encounter
	NIM3_Encounter.setReferralID(NIM3_Referral.getUniqueID());
	NIM3_Encounter.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_Encounter.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_Encounter.setAttendingPhysicianID(new Integer(52));
	NIM3_Encounter.setUniqueModifyComments("Created by Backup Call Service");
	NIM3_Encounter.setEncounterStatusID(new Integer(1));
	NIM3_Encounter.commitData();
	NIM3_Encounter.setScanPass("SP" +  NIM3_Encounter.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
	NIM3_Encounter.commitData();
	
	//autosets Commtrack
	NIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setUniqueModifyComments("Created by Backup Call Service");
	NIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setCaseID(NIM3_CaseAccount.getUniqueID());
	NIM3_CommTrack.setEncounterID(NIM3_Encounter.getUniqueID());
	NIM3_CommTrack.setReferralID(NIM3_Referral.getUniqueID());
	NIM3_CommTrack.setExtUserID(new Integer(19));
	NIM3_CommTrack.setCommTypeID(new Integer(2));//1=message 2=sched     0 = bogus
	NIM3_CommTrack.setComments("[CS] Schedule Submit [CL:" + NIM3_CaseAccount.getCaseClaimNumber() + "]\n[IP: " + ip + "]\n[Host: " + host + "]");
	NIM3_CommTrack.commitData();
}
else if (testChangeIDMSG.equalsIgnoreCase("1"))
{
	out.println("Success Code: 101");
	//autosets Commtrack
	NIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setUniqueModifyComments("Created by Backup Call Service");
	NIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
	NIM3_CommTrack.setExtUserID(new Integer(19));
	NIM3_CommTrack.setCommTypeID(new Integer(1));//1=message 2=sched     0 = bogus
	NIM3_CommTrack.setComments("[CS] Message Only Submit\nIP: " + ip + "\nHost: " + host);
	NIM3_CommTrack.commitData();
}
else 
{
	out.println("Success Code: 100");
	NIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
//	NIM3_CommTrack.setExtUserID(new Integer(19));
	NIM3_CommTrack.setCommTypeID(new Integer(0));//1=message 2=sched     0 = bogus
	NIM3_CommTrack.setComments("[CS] Invalid/Empty Submit\nIP: " + ip + "\nHost: " + host);
	NIM3_CommTrack.commitData();
}


		emailType_V3 myETSend = new emailType_V3();
		String theBody = "NextImageGrid CommTrack Notification:\n\n";
		theBody += "From: " + NIM3_CommTrack.getMessageName() + " @ " +NIM3_CommTrack.getMessageCompany() ;
		theBody += "\nPhone: " + NIM3_CommTrack.getMessagePhone() + " Fax: " +NIM3_CommTrack.getMessageFax() ;
		theBody += "\nEmail: " + NIM3_CommTrack.getMessageEmail() ;
		theBody += "\n-----------------------\nMessage: ";
		theBody += "\n" + NIM3_CommTrack.getMessageText() ;
		theBody += "\n-----------------------";
		theBody += "\nSubmitted by: Backup Call Service";
		theBody += "\nTrackingID: " + NIM3_CommTrack.getUniqueID();
		theBody += "\nDate: " + PLCUtils.getNowDate(false);
		
		myETSend.setBody(theBody);
		myETSend.setBodyType(myETSend.PLAIN_TYPE);
	
		myETSend.setFrom("support@nextimagemedical.com");
		myETSend.setSubject("NextImageGrid - CommTrack Submitted by Back Up Call Service ");
		
		myETSend.setTo("scott@nextimagemedical.com");
		myETSend.isSendMail();
		myETSend.setTo("order@nextimagemedical.com");
		myETSend.isSendMail();



}
catch(Exception ee)
{
    out.println("<hr>" + ee + "<hr>");
}
%>
<link href="ui_200/style_PayerID.css" rel="stylesheet" type="text/css">
<p class="title"><a href="ext3_submit_sub2.jsp" class="title">Continue </a></p>
<script language="javascript">
alert ("Success");
document.location='ext3_submit_sub2.jsp';
</script>
