<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
</head>
<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?iDelay=-1&plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler3) 
    {
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	  %>
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td>
          <table border="1" cellspacing="0" cellpadding="1">
            <tr class="tdHeaderAlt">
              <td colspan="9" class="tdHeaderAltDark">Case Load</td>
              </tr>
            <tr class="tdHeaderAlt">
              <td>Scheduler</td>
              <td>Total Active</td>
              <td>Not Scheduled</td>
              <td>Not Scheduled >2days</td>
              <td>Past Appt</td>
              </tr>
            <%
if (isScheduler3)
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getCaseLoad(); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		NIM_CaseLoadObject myCaseLoadObject = (NIM_CaseLoadObject) myWorklist_Vector.elementAt(i);
		%>
            <tr >
              <td align="center"><%=myCaseLoadObject.getName()%></td>
              <td align="center"><%=myCaseLoadObject.getTotalActive()%></td>
              <td align="center"><%=myCaseLoadObject.getTotalNotScheduled()%></td>
              <td align="center"><%=myCaseLoadObject.getTotalPastDue()%></td>
              <td align="center"><%=myCaseLoadObject.getTotalAfterAppt()%></td>
              </tr>
            <%
	}

}
				%>
            </table>
          
    
      </td>
  </tr>
      </table>
      <%
	  


}
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</body>
</html>
