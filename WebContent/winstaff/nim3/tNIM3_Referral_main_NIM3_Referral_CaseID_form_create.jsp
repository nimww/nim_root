<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tNIM3_Referral_main_NIM3_Referral_CaseID_form_create.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
        }

//declaration of Enumeration
    bltNIM3_Referral        working_bltNIM3_Referral = new bltNIM3_Referral();
    working_bltNIM3_Referral.setCaseID(iCaseID);
    working_bltNIM3_Referral.setUniqueCreateDate(PLCUtils.getNowDate(false));
    working_bltNIM3_Referral.setUniqueModifyDate(PLCUtils.getNowDate(false));
	String newExpress = iCaseID.toString();		
	
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltNIM3_Referral.setUniqueModifyComments(""+UserLogonDescription);
    }
			String tempAN = working_bltNIM3_Referral.getAuditNotes() + "\n[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nCreated";
			working_bltNIM3_Referral.setAuditNotes(tempAN);

				working_bltNIM3_Referral.setReferralTypeID(new Integer(3));
//				working_bltNIM3_Referral.setReferralStatusID(new Integer(1));
				working_bltNIM3_Referral.setAttendingPhysicianID(new Integer(52));
				tempAN = "[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nCreated (express)";
				working_bltNIM3_Referral.setAuditNotes(tempAN);
				working_bltNIM3_Referral.commitData();
				pageControllerHash.put("iReferralID",working_bltNIM3_Referral.getUniqueID());

				bltNIM3_Encounter  NIME = new bltNIM3_Encounter();

				NIME.setReferralID(working_bltNIM3_Referral.getUniqueID());
//				NIME.setReferringPhysicianID(CurrentUserAccount.getUniqueID());
				tempAN = "[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nCreated (express)";
				NIME.setAuditNotes(tempAN);
				NIME.setEncounterTypeID(new Integer(9));
				NIME.setEncounterStatusID(new Integer(1));
				NIME.commitData();
				newExpress = NIME.getUniqueID().toString();
				NIME.setScanPass("SP" +  NIME.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());								
				NIME.commitData();
				pageControllerHash.put("iEncounterID",NIME.getUniqueID());

        {
            {
                pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
                session.setAttribute("pageControllerHash",pageControllerHash);
                //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
                String targetRedirect = "tNIM3_Referral_form.jsp?nullParam=null"+parameterPassString    ;

                {
//                    targetRedirect = "tNIM3_Referral_form.jsp?EDITID="+working_bltNIM3_Referral.getUniqueID()+"&routePageReference=sParentReturnPage&EDIT=edit"+parameterPassString    ;
                }
				targetRedirect = "tNIM3_CaseAccount_express1_form.jsp?QUICKVIEW=yes&EDITID="+newExpress+"&routePageReference=sParentReturnPage&EDIT=edit"+parameterPassString    ;
				response.sendRedirect(targetRedirect);
            }
        }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>





