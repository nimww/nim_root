

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="    com.winstaff.bltNIM2_CaseAccount" %>
<%/*

    filename: out\jsp\tNIM2_CaseAccount_form.jsp
    Created on Jan/05/2009
    Created by: Scott Ellis
*/%>


<%
String thePLCID = "200";
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_PayerID.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>

<%
//initial declaration of list class and parentID
    boolean accessValid = true;
    // required for Type2
    String sKeyMasterReference = null;

  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
	  Integer UserSecurityGroupID = new Integer(2);
	  boolean isShowAudit = false;
//initial declaration of list class and parentID
//always new
    bltNIM2_CaseAccount        NIM2_CaseAccount        =       new    bltNIM2_CaseAccount(UserSecurityGroupID, true);

//fields
        %>
<p class=title>Injury Case Information</p>
        <form action="tNIM2_CaseAccount_form_sub.jsp" name="tNIM2_CaseAccount_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr><td class=tableColor>


            <table cellpadding=0 cellspacing=0 width=100%>
            <%
            if ( (NIM2_CaseAccount.isRequired("CasePass",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("CasePass")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("CasePass",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("CasePass",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((false&&NIM2_CaseAccount.isWrite("CasePass",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>CaseCode&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="CasePass" value="<%=NIM2_CaseAccount.getCasePass()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CasePass&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("CasePass")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM2_CaseAccount.isRead("CasePass",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>CaseCode&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getCasePass()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CasePass&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("CasePass")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>
                     <tr class="tdHeaderAlt">
                       <td class="tdHeaderAlt" colspan="2" valign=top>Payer Information </td>
                     </tr>



            <%
            if ( (NIM2_CaseAccount.isRequired("CaseClaimNumber1",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("CaseClaimNumber1")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("CaseClaimNumber1",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("CaseClaimNumber1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("CaseClaimNumber1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b> Claim Number<br>
                     </b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="CaseClaimNumber1" value="<%=NIM2_CaseAccount.getCaseClaimNumber1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CaseClaimNumber1&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("CaseClaimNumber1")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("CaseClaimNumber1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Case Claim # 1&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getCaseClaimNumber1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CaseClaimNumber1&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("CaseClaimNumber1")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("CaseClaimNumber2",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("CaseClaimNumber2")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("CaseClaimNumber2",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("CaseClaimNumber2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM2_CaseAccount.isWrite("CaseClaimNumber2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Case Claim # 2&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="CaseClaimNumber2" value="<%=NIM2_CaseAccount.getCaseClaimNumber2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CaseClaimNumber2&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("CaseClaimNumber2")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM2_CaseAccount.isRead("CaseClaimNumber2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>CaseClaimNumber2&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getCaseClaimNumber2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CaseClaimNumber2&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("CaseClaimNumber2")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PayerName",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PayerName")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PayerName",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PayerName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PayerName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Insurance Carrier/Company Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="PayerName" value="<%=NIM2_CaseAccount.getPayerName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PayerName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PayerName&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPayerName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("AdjusterName",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("AdjusterName")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("AdjusterName",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("AdjusterName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("AdjusterName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Adjuster Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="AdjusterName" value="<%=NIM2_CaseAccount.getAdjusterName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AdjusterName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("AdjusterName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("AdjusterName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>AdjusterName&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getAdjusterName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AdjusterName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("AdjusterName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("AdjusterID",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("AdjusterID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("AdjusterID",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("AdjusterID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM2_CaseAccount.isWrite("AdjusterID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>AdjusterID&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="AdjusterID" value="<%=NIM2_CaseAccount.getAdjusterID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AdjusterID&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("AdjusterID")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM2_CaseAccount.isRead("AdjusterID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>AdjusterID&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getAdjusterID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AdjusterID&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("AdjusterID")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PayerAddress1",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PayerAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PayerAddress1",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PayerAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM2_CaseAccount.isWrite("PayerAddress1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="PayerAddress1" value="<%=NIM2_CaseAccount.getPayerAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerAddress1&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerAddress1")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM2_CaseAccount.isRead("PayerAddress1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PayerAddress1&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPayerAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerAddress1&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerAddress1")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PayerAddress2",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PayerAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PayerAddress2",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PayerAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM2_CaseAccount.isWrite("PayerAddress2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address 2&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="PayerAddress2" value="<%=NIM2_CaseAccount.getPayerAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerAddress2&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerAddress2")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM2_CaseAccount.isRead("PayerAddress2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PayerAddress2&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPayerAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerAddress2&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerAddress2")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PayerCity",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PayerCity")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PayerCity",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PayerCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM2_CaseAccount.isWrite("PayerCity",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="PayerCity" value="<%=NIM2_CaseAccount.getPayerCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerCity&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerCity")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM2_CaseAccount.isRead("PayerCity",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PayerCity&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPayerCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerCity&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerCity")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PayerStateID",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PayerStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PayerStateID",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PayerStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM2_CaseAccount.isWrite("PayerStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State</b></p></td><td valign=top><p><select   name="PayerStateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM2_CaseAccount.getPayerStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerStateID&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerStateID")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM2_CaseAccount.isRead("PayerStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>PayerStateID&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM2_CaseAccount.getPayerStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerStateID&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerStateID")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM2_CaseAccount.isRequired("PayerZIP",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PayerZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PayerZIP",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PayerZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM2_CaseAccount.isWrite("PayerZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="PayerZIP" value="<%=NIM2_CaseAccount.getPayerZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerZIP&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerZIP")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM2_CaseAccount.isRead("PayerZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PayerZIP&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPayerZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerZIP&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerZIP")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PayerPhone",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PayerPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PayerPhone",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PayerPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PayerPhone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Phone&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="PayerPhone" value="<%=NIM2_CaseAccount.getPayerPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerPhone&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerPhone")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PayerPhone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PayerPhone&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPayerPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerPhone&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerPhone")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PayerFax",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PayerFax")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PayerFax",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PayerFax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PayerFax",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Fax&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="PayerFax" value="<%=NIM2_CaseAccount.getPayerFax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerFax&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerFax")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PayerFax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PayerFax&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPayerFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerFax&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerFax")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PayerEmail",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PayerEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PayerEmail",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PayerEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PayerEmail",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Email&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="PayerEmail" value="<%=NIM2_CaseAccount.getPayerEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerEmail&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerEmail")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PayerEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PayerEmail&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPayerEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerEmail&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PayerEmail")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

<%
if (false)
{
%>
                     <tr class="tdHeaderAlt">
                       <td class="tdHeaderAlt" colspan="2" valign=top>Nurse Case Manager (if Applicable) </td>
                     </tr>

            <%
            if ( (NIM2_CaseAccount.isRequired("NurseCaseManagerName",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("NurseCaseManagerName")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("NurseCaseManagerName",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("NurseCaseManagerName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("NurseCaseManagerName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="NurseCaseManagerName" value="<%=NIM2_CaseAccount.getNurseCaseManagerName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NurseCaseManagerName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("NurseCaseManagerName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("NurseCaseManagerName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>NurseCaseManagerName&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getNurseCaseManagerName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NurseCaseManagerName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("NurseCaseManagerName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("NurseCaseManagerID",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("NurseCaseManagerID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("NurseCaseManagerID",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("NurseCaseManagerID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("NurseCaseManagerID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ID&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="NurseCaseManagerID" value="<%=NIM2_CaseAccount.getNurseCaseManagerID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NurseCaseManagerID&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("NurseCaseManagerID")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("NurseCaseManagerID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>NurseCaseManagerID&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getNurseCaseManagerID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NurseCaseManagerID&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("NurseCaseManagerID")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("NurseCaseManagerPreparedBy",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("NurseCaseManagerPreparedBy")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("NurseCaseManagerPreparedBy",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("NurseCaseManagerPreparedBy",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((false&&NIM2_CaseAccount.isWrite("NurseCaseManagerPreparedBy",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>NurseCaseManagerPreparedBy&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="NurseCaseManagerPreparedBy" value="<%=NIM2_CaseAccount.getNurseCaseManagerPreparedBy()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NurseCaseManagerPreparedBy&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("NurseCaseManagerPreparedBy")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((false&&NIM2_CaseAccount.isRead("NurseCaseManagerPreparedBy",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>NurseCaseManagerPreparedBy&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getNurseCaseManagerPreparedBy()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NurseCaseManagerPreparedBy&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("NurseCaseManagerPreparedBy")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("NurseCaseManagerPhone",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("NurseCaseManagerPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("NurseCaseManagerPhone",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("NurseCaseManagerPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("NurseCaseManagerPhone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Phone&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="NurseCaseManagerPhone" value="<%=NIM2_CaseAccount.getNurseCaseManagerPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NurseCaseManagerPhone&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("NurseCaseManagerPhone")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("NurseCaseManagerPhone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>NurseCaseManagerPhone&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getNurseCaseManagerPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NurseCaseManagerPhone&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("NurseCaseManagerPhone")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("NurseCaseManagerFax",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("NurseCaseManagerFax")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("NurseCaseManagerFax",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("NurseCaseManagerFax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("NurseCaseManagerFax",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Fax&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="NurseCaseManagerFax" value="<%=NIM2_CaseAccount.getNurseCaseManagerFax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NurseCaseManagerFax&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("NurseCaseManagerFax")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("NurseCaseManagerFax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>NurseCaseManagerFax&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getNurseCaseManagerFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NurseCaseManagerFax&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("NurseCaseManagerFax")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("NurseCaseManagerEmail",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("NurseCaseManagerEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("NurseCaseManagerEmail",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("NurseCaseManagerEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("NurseCaseManagerEmail",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Email&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="NurseCaseManagerEmail" value="<%=NIM2_CaseAccount.getNurseCaseManagerEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NurseCaseManagerEmail&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("NurseCaseManagerEmail")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("NurseCaseManagerEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>NurseCaseManagerEmail&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getNurseCaseManagerEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=NurseCaseManagerEmail&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("NurseCaseManagerEmail")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

<%
}%>


                     <tr class="tdHeaderAlt">
                       <td class="tdHeaderAlt" colspan="2" valign=top>Patient & Injury Information </td>
                     </tr>




            <%
            if ( (NIM2_CaseAccount.isRequired("PatientFirstName",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientFirstName")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientFirstName",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientFirstName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientFirstName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>First Name</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="PatientFirstName" value="<%=NIM2_CaseAccount.getPatientFirstName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientFirstName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientFirstName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientFirstName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientFirstName&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientFirstName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientFirstName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientFirstName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientLastName",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientLastName")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientLastName",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientLastName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientLastName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Last Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="PatientLastName" value="<%=NIM2_CaseAccount.getPatientLastName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientLastName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientLastName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientLastName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientLastName&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientLastName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientLastName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientLastName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientAccountNumber",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientAccountNumber")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientAccountNumber",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientAccountNumber",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM2_CaseAccount.isWrite("PatientAccountNumber",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Account Number&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="PatientAccountNumber" value="<%=NIM2_CaseAccount.getPatientAccountNumber()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientAccountNumber&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientAccountNumber")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM2_CaseAccount.isRead("PatientAccountNumber",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientAccountNumber&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientAccountNumber()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientAccountNumber&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientAccountNumber")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientExpirationDate",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientExpirationDate")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientExpirationDate",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientExpirationDate",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (false&&(NIM2_CaseAccount.isWrite("PatientExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Eligibility Expiration&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="PatientExpirationDate" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM2_CaseAccount.getPatientExpirationDate())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientExpirationDate&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientExpirationDate")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM2_CaseAccount.isRead("PatientExpirationDate",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>PatientExpirationDate&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM2_CaseAccount.getPatientExpirationDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientExpirationDate&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientExpirationDate")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientIsActive",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientIsActive")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientIsActive",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientIsActive",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM2_CaseAccount.isWrite("PatientIsActive",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Is Active&nbsp;</b></p></td><td valign=top><p><select   name="PatientIsActive" ><jsp:include page="../generic/tYesNoLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM2_CaseAccount.getPatientIsActive()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientIsActive&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientIsActive")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM2_CaseAccount.isRead("PatientIsActive",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>PatientIsActive&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM2_CaseAccount.getPatientIsActive()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientIsActive&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientIsActive")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM2_CaseAccount.isRequired("PatientDOB",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientDOB")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientDOB",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientDOB",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM2_CaseAccount.isWrite("PatientDOB",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>DOB&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="PatientDOB" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM2_CaseAccount.getPatientDOB())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientDOB&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientDOB")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientDOB",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>PatientDOB&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM2_CaseAccount.getPatientDOB())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientDOB&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientDOB")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientSSN",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientSSN")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientSSN",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientSSN",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientSSN",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>SSN&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="PatientSSN" value="<%=NIM2_CaseAccount.getPatientSSN()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientSSN&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientSSN")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientSSN",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientSSN&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientSSN()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientSSN&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientSSN")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientGender",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientGender")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientGender",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientGender",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientGender",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Gender&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="PatientGender" value="<%=NIM2_CaseAccount.getPatientGender()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientGender&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientGender")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientGender",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientGender&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientGender()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientGender&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientGender")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientAddress1",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientAddress1",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientAddress1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="PatientAddress1" value="<%=NIM2_CaseAccount.getPatientAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientAddress1&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientAddress1")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientAddress1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientAddress1&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientAddress1&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientAddress1")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientAddress2",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientAddress2",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientAddress2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Address 2 &nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="PatientAddress2" value="<%=NIM2_CaseAccount.getPatientAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientAddress2&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientAddress2")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientAddress2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientAddress2&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientAddress2&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientAddress2")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientCity",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientCity")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientCity",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientCity",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>City&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="PatientCity" value="<%=NIM2_CaseAccount.getPatientCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCity&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCity")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientCity",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientCity&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCity&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCity")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientStateID",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientStateID",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>State&nbsp;</b></p></td><td valign=top><p><select   name="PatientStateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM2_CaseAccount.getPatientStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientStateID&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientStateID")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>PatientStateID&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM2_CaseAccount.getPatientStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientStateID&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientStateID")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM2_CaseAccount.isRequired("PatientZIP",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientZIP",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="PatientZIP" value="<%=NIM2_CaseAccount.getPatientZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientZIP&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientZIP")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientZIP&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientZIP&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientZIP")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientHomePhone",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientHomePhone")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientHomePhone",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientHomePhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientHomePhone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Home Phone&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="PatientHomePhone" value="<%=NIM2_CaseAccount.getPatientHomePhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHomePhone&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientHomePhone")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientHomePhone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientHomePhone&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientHomePhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientHomePhone&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientHomePhone")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientWorkPhone",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientWorkPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientWorkPhone",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientWorkPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientWorkPhone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Work Phone&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="PatientWorkPhone" value="<%=NIM2_CaseAccount.getPatientWorkPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientWorkPhone&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientWorkPhone")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientWorkPhone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientWorkPhone&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientWorkPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientWorkPhone&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientWorkPhone")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientCellPhone",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientCellPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientCellPhone",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientCellPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientCellPhone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Cell Phone&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="PatientCellPhone" value="<%=NIM2_CaseAccount.getPatientCellPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCellPhone&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCellPhone")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientCellPhone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientCellPhone&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientCellPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCellPhone&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCellPhone")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("EmployerName",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("EmployerName")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("EmployerName",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("EmployerName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("EmployerName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Employer Name&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="EmployerName" value="<%=NIM2_CaseAccount.getEmployerName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EmployerName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("EmployerName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("EmployerName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>EmployerName&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getEmployerName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EmployerName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("EmployerName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("EmployerPhone",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("EmployerPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("EmployerPhone",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("EmployerPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("EmployerPhone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Employer Phone&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="EmployerPhone" value="<%=NIM2_CaseAccount.getEmployerPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EmployerPhone&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("EmployerPhone")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("EmployerPhone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>EmployerPhone&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getEmployerPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=EmployerPhone&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("EmployerPhone")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("DateOfInjury",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("DateOfInjury")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("DateOfInjury",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("DateOfInjury",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((NIM2_CaseAccount.isWrite("DateOfInjury",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Of Injury&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><input maxlength=10  type=text size="80" name="DateOfInjury" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM2_CaseAccount.getDateOfInjury())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfInjury&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("DateOfInjury")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("DateOfInjury",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>DateOfInjury&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NIM2_CaseAccount.getDateOfInjury())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfInjury&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("DateOfInjury")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("InjuryDescription",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("InjuryDescription")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("InjuryDescription",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("InjuryDescription",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("InjuryDescription",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=NIM2_CaseAccount.getEnglish("InjuryDescription")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="InjuryDescription" cols="40" maxlength=200><%=NIM2_CaseAccount.getInjuryDescription()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InjuryDescription&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("InjuryDescription")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("InjuryDescription",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=NIM2_CaseAccount.getEnglish("InjuryDescription")%>&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getInjuryDescription()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=InjuryDescription&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("InjuryDescription")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>






<% if (false)
{
%>


            <%
            if ( (NIM2_CaseAccount.isRequired("PatientCCAccountFullName",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientCCAccountFullName")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientCCAccountFullName",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientCCAccountFullName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientCCAccountFullName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>PatientCCAccountFullName&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="PatientCCAccountFullName" value="<%=NIM2_CaseAccount.getPatientCCAccountFullName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCCAccountFullName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCCAccountFullName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientCCAccountFullName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientCCAccountFullName&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientCCAccountFullName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCCAccountFullName&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCCAccountFullName")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientCCAccountNumber1",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientCCAccountNumber1")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientCCAccountNumber1",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientCCAccountNumber1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientCCAccountNumber1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>PatientCCAccountNumber1&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="PatientCCAccountNumber1" value="<%=NIM2_CaseAccount.getPatientCCAccountNumber1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCCAccountNumber1&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCCAccountNumber1")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientCCAccountNumber1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientCCAccountNumber1&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientCCAccountNumber1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCCAccountNumber1&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCCAccountNumber1")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientCCAccountNumber2",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientCCAccountNumber2")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientCCAccountNumber2",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientCCAccountNumber2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientCCAccountNumber2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>PatientCCAccountNumber2&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="PatientCCAccountNumber2" value="<%=NIM2_CaseAccount.getPatientCCAccountNumber2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCCAccountNumber2&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCCAccountNumber2")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientCCAccountNumber2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientCCAccountNumber2&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientCCAccountNumber2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCCAccountNumber2&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCCAccountNumber2")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientCCAccountType",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientCCAccountType")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientCCAccountType",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientCCAccountType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientCCAccountType",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>PatientCCAccountType&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="PatientCCAccountType" value="<%=NIM2_CaseAccount.getPatientCCAccountType()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCCAccountType&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCCAccountType")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientCCAccountType",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientCCAccountType&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientCCAccountType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCCAccountType&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCCAccountType")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientCCAccountExpMonth",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientCCAccountExpMonth")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientCCAccountExpMonth",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientCCAccountExpMonth",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientCCAccountExpMonth",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>PatientCCAccountExpMonth&nbsp;</b></p></td><td valign=top><p><input maxlength="4" type=text size="80" name="PatientCCAccountExpMonth" value="<%=NIM2_CaseAccount.getPatientCCAccountExpMonth()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCCAccountExpMonth&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCCAccountExpMonth")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientCCAccountExpMonth",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientCCAccountExpMonth&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientCCAccountExpMonth()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCCAccountExpMonth&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCCAccountExpMonth")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM2_CaseAccount.isRequired("PatientCCAccountExpYear",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("PatientCCAccountExpYear")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("PatientCCAccountExpYear",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("PatientCCAccountExpYear",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("PatientCCAccountExpYear",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>PatientCCAccountExpYear&nbsp;</b></p></td><td valign=top><p><input maxlength="6" type=text size="80" name="PatientCCAccountExpYear" value="<%=NIM2_CaseAccount.getPatientCCAccountExpYear()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCCAccountExpYear&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCCAccountExpYear")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("PatientCCAccountExpYear",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>PatientCCAccountExpYear&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getPatientCCAccountExpYear()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PatientCCAccountExpYear&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("PatientCCAccountExpYear")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

<% 
}
%>



            <%
            if ( (NIM2_CaseAccount.isRequired("Comments",UserSecurityGroupID))&&(!NIM2_CaseAccount.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM2_CaseAccount.isExpired("Comments",expiredDays))&&(NIM2_CaseAccount.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM2_CaseAccount.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=NIM2_CaseAccount.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=NIM2_CaseAccount.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM2_CaseAccount.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=NIM2_CaseAccount.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=NIM2_CaseAccount.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM2_CaseAccount&amp;sRefID=<%=NIM2_CaseAccount.getCaseID()%>&amp;sFieldNameDisp=<%=NIM2_CaseAccount.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM2_CaseAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
            <p><input <%=HTMLFormStyleButton%> type=Submit value="Continue" name=Submit></p>
        </td></tr></table>
        </form>
        <%
  }
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>