<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*  " %>
<%/*
    filename: out\jsp\tNIM3_Appointment_main_NIM3_Appointment_ProviderID_form_create.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPayerID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
        }
    Integer        iProviderID        =    requestID;

//declaration of Enumeration
    bltNIM3_Appointment        working_bltNIM3_Appointment = new bltNIM3_Appointment();
    working_bltNIM3_Appointment.setProviderID(iProviderID);
    working_bltNIM3_Appointment.setUniqueCreateDate(PLCUtils.getNowDate(false));
    working_bltNIM3_Appointment.setUniqueModifyDate(PLCUtils.getNowDate(false));
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltNIM3_Appointment.setUniqueModifyComments(""+UserLogonDescription);
    }

            working_bltNIM3_Appointment.commitData();
        {
            {
                pageControllerHash.put("iAppointmentID",working_bltNIM3_Appointment.getUniqueID());
                pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
                session.setAttribute("pageControllerHash",pageControllerHash);
                //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
                String targetRedirect = "tNIM3_Appointment_form_PayerID.jsp?nullParam=null"+parameterPassString    ;

                {
                    targetRedirect = "tNIM3_Appointment_form_PayerID.jsp?EDITID="+working_bltNIM3_Appointment.getUniqueID()+"&routePageReference=sParentReturnPage&EDIT=edit"+parameterPassString    ;
                }
                response.sendRedirect(targetRedirect);
            }
        }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>





