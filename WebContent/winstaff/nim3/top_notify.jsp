<html xmlns="http://www.w3.org/1999/xhtml">
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<%@ include file="../generic/CheckLogin.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
<style>
body {
margin:0px;
padding:0px;
font-size: 1px;
}
table {
margin:0px;
padding:0px;
}
</style>
</head>
<body><%-- <%
	String line1 = null; 
	String line2 = null;
	
	line1 = "";
	line2 = "";
	
	//line1 = "NIM will be down for maintenance";
	//line2 = "June 9th @ 12 noon for 15 minutes.";
	
   	boolean accessValid = false;
   	Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   	Integer iPayerID = null;
   	Integer iAdjusterID = null;
   	if (iSecurityCheck.intValue()!=0)
   	{
	    if (isScheduler) 
	    {
        	accessValid = true;
    	}
    	else if (pageControllerHash.containsKey("iAdjusterID")&&pageControllerHash.containsKey("iPayerID")) 
    	{
	        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
			isAdjuster = true;
        	accessValid = true;
		}
    	else if (pageControllerHash.containsKey("iAdjusterID2")&&pageControllerHash.containsKey("iPayerID")) 
    	{
	        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
			isAdjuster2 = true;
        	accessValid = true;
		}
    	else if (pageControllerHash.containsKey("iAdjusterID3")&&pageControllerHash.containsKey("iPayerID")) 
    	{
	        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
			isAdjuster3 = true;
        	accessValid = true;
		}
		
		//this delay prevents the heavy stats from loading until a preset time.  This will speed up the site
        Integer iDelay = null;
        if (request.getParameter("iDelay")!=null&&!request.getParameter("iDelay").equalsIgnoreCase(""))
        {
                iDelay = new Integer(request.getParameter("iDelay"));
        }
		if (iDelay!=null&&iDelay==-1)
		{
		%>
		<br><h2 style="text-align:center"><em>Quickmode</em> Enabled</h2><h2 style="font-size:14px; text-align:center;">Stats are not shown on this page</h2>
		<%
		}
		else if (iDelay!=null)
		{
		%>
		<meta http-equiv="refresh" content="<%=iDelay%>;URL=top_notify.jsp" />
		<br><h2 style="text-align:center"><em>Quickmode</em> Enabled</h2><h2 style="font-size:14px; text-align:center;">Stats loading in background... <br><img id="progress" src="../js/load8.gif" alt=""/></h2>
		<%
  		//page security
		} else if (accessValid){
	      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      	  java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      	  pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
	  	  if (isScheduler)
	  		{
				searchDB2 db = new searchDB2();
				try{
					java.sql.ResultSet result = null;;
					String query ="select  (select count(distinct referral.referralid) from tnim3_encounter as \"encounter\" inner join tnim3_referral as \"referral\" on referral.referralid = encounter.referralid inner join tnim3_caseaccount as \"caseaccount\" on caseaccount.caseid = referral.caseid inner join tuseraccount as \"assignedto\" on assignedto.userid = caseaccount.assignedtoid where (encounter.encounterstatusid ='1' or encounter.encounterstatusid = '12') and encounter.uniquecreatedate > (now() - '3 month'::interval) and appointmentid = 0) as \"Active\",  (select count(DISTINCT tnim3_encounter.encounterid) from tnim3_encounter inner join tnim3_referral on tnim3_referral.referralid = tnim3_encounter.referralid  where tNIM3_Encounter.encounterstatusid in ('1','12') and tnim3_encounter.uniquecreatedate > (now() - '3 month'::interval)  and rxfileid=0) as \"NeedRx\", (select count(DISTINCT tnim3_caseaccount.caseid)  from tnim3_caseaccount inner join tnim3_referral on tnim3_referral.caseid = tnim3_caseaccount.caseid  RIGHT JOIN tnim3_encounter on tnim3_encounter.referralid = tnim3_referral.referralid left join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid where cast(patientdob as VARCHAR) not like '1800%' and cast(dateofinjury as VARCHAR) not like '1800%'  and adjusterid != 0  and  patientfirstname !='' and patientzip != '' and patientstateid !=0 and (patienthomephone !='' or patientcellphone !='') and caseclaimnumber !='' and  cast(appointmenttime as VARCHAR)  is null and encounterstatusid = '1' and tnim3_encounter.uniquecreatedate > (now() - '3 month'::interval) )as \"ReadyToSched\", (select count(DISTINCT encounterid) from tnim3_encounter inner join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid where encounterstatusid ='1' and tnim3_encounter.uniquecreatedate > (now() - '3 month'::interval) and appointmenttime < now() and reportfileid=0 and tnim3_encounter.appointmentid >0) as \"NeedRp\", (select  count(DISTINCT encounterid)  from tnim3_encounter inner join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid where encounterstatusid in ('1', '12') and tnim3_encounter.uniquecreatedate > (now() - '3 month'::interval) and appointmenttime < now() and reportfileid!=0 and timetrack_reqrpreview_userid=0)  as \"RpNeedsRw\", (select count(DISTINCT tnim3_commtrack.encounterid)  from tnim3_commtrack INNER JOIN tnim3_encounter on tnim3_encounter.encounterid = tnim3_commtrack.encounterid where encounterstatusid ='9' and (messagetext LIKE '%Billing: In Progress%' or messagetext LIKE '%Moved to Billing%') and tnim3_commtrack.uniquecreatedate > (now() - '1 day'::interval)) as \"BillingInLastDay\" , (select  count(DISTINCT tnim3_commtrack.encounterid)  from tnim3_commtrack INNER JOIN tnim3_encounter on tnim3_encounter.encounterid = tnim3_commtrack.encounterid where encounterstatusid ='9' and (messagetext LIKE '%Billing: In Progress%' or messagetext LIKE '%Moved to Billing%') and tnim3_commtrack.uniquecreatedate > (now() - '7 day'::interval)) as \"BillingInLastWeek\", (select  count(DISTINCT tnim3_commtrack.encounterid)  from tnim3_commtrack INNER JOIN tnim3_encounter on tnim3_encounter.encounterid = tnim3_commtrack.encounterid where encounterstatusid ='9' and (messagetext LIKE '%Billing: In Progress%' or messagetext LIKE '%Moved to Billing%') and tnim3_commtrack.uniquecreatedate > (now() - '1 month'::interval)) as \"BillingInLastMonth\", (select count(encounterid) from tnim3_encounter where encounterstatusid='12') AS \"Escalated\", (select count(encounterid) from tnim3_encounter where seenetdev_waiting ='1') AS \"In NetDev\", ( select count(tNIM3_Encounter.encounterid) from tNIM3_Encounter  INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN (select distinct tnim3_service.encounterid from tnim3_service)as service on service.encounterid = tnim3_encounter.encounterid  INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid  where tNIM3_Encounter.AppointmentID=0 AND tnim3_referral.rxfileid>0 AND tnim3_referral.orderfileid>0 AND tnim3_referral.referringphysicianid>0 AND tnim3_caseaccount.adjusterid>0 AND tnim3_caseaccount.patientstateid>0  AND tnim3_caseaccount.patientaddress1 != '' AND tNIM3_encounter.timetrack_reqrxreview_userid=0 AND tnim3_referral.referralstatusid = '1' AND tNIM3_Encounter.encounterstatusid not in (0,2,3,5,6,9,10))   as \"Rx Review\"  ";
					result = db.executeStatement(query);
					
					if (result!=null&&result.next())
					{%><table border="0" cellspacing="0" cellpadding="2" width="100%">
							<tr>
								<td>Active</td><td><%=result.getString("Active")%></td>
								<td style="padding-left:15px">Billing In Last Day</td><td><%=result.getString("BillingInLastDay")%></td>
							</tr>
							<tr>
								<td>
								<form action="Worklist.jsp" id="showWL_missingrx" method="post" style="margin:0">
									<input type='hidden' name='showWL_missingrx' value="1"/>
									<input type='hidden' name='theScheduler1' value="0">
									<a href="#" onclick="document.getElementById('showWL_missingrx').submit(); return false;">Need Rx</a>
								</form>
								</td><td><%=result.getString("NeedRx")%></td>
								<td style="padding-left:15px">Billing In Last Week</td><td><%=result.getString("BillingInLastWeek")%></td>
							</tr>
							<tr>
								<td>
								<form action="Worklist.jsp" id="showWL_rxreview" method="post" style="margin:0">
									<input type='hidden' name='showWL_rxreview' value="1"/>
									<input type='hidden' name='theScheduler1' value="0">
									<a href="#" onclick="document.getElementById('showWL_rxreview').submit(); return false;">Need Rx Rw</a>
								</form>
								</td><td><%=result.getString("Rx Review")%></td>
								<td style="padding-left:15px">Billing In Last Month</td><td><%=result.getString("BillingInLastMonth")%></td>
							</tr>
							<tr>
								<td>
								<form action="Worklisk.jsp" id="showWL_readytoschedule" method="post" style="margin:0">
									<input type='hidden' name='showWL_readytoschedule' value="1"/>
									<input type='hidden' name='theScheduler1' value="0">
									<a href="#" onclick="document.getElementById('showWL_readytoschedule').submit(); return false;">Ready To Sched</a>
									Ready To Sched
								</form>
								</td><td><%=result.getString("ReadyToSched")%></td>
								<td style="padding-left:15px">
								<form action="Worklist.jsp" id="showWL_escalatedcases" method="post" style="margin:0">
									<input type='hidden' name='showWL_escalatedcases' value="1"/>
									<input type='hidden' name='theScheduler1' value="0">
									<a href="#" onclick="document.getElementById('showWL_escalatedcases').submit(); return false;">Escalated Cases</a>
								</form>
								</td><td><%=result.getString("Escalated")%></td>
							</tr>
							<tr>
								<td>
								<form action="Worklist.jsp" id="showWL_missingreport" method="post" style="margin:0">
									<input type='hidden' name='showWL_missingreport' value="1"/>
									<input type='hidden' name='theScheduler1' value="0">
									<a href="#" onclick="document.getElementById('showWL_missingreport').submit(); return false;">Need Rp</a>
								</form>
								</td><td><%=result.getString("NeedRp")%></td>
								<td style="padding-left:15px">
								<form action="Worklist.jsp" id="showWL_innetdev" method="post" style="margin:0">
									<input type='hidden' name='showWL_innetdev' value="1"/>
									<input type='hidden' name='theScheduler1' value="0">
									<a href="#" onclick="document.getElementById('showWL_innetdev').submit(); return false;">In NetDev</a>
								</form>
								</td><td><%=result.getString("In NetDev")%></td>
							</tr>
							<tr>
								<td>
								<form action="Worklist.jsp" id="showWL_needsrpreview" method="post" style="margin:0">
									<input type='hidden' name='showWL_needsrpreview' value="1"/>
									<input type='hidden' name='theScheduler1' value="0">
									<a href="#" onclick="document.getElementById('showWL_needsrpreview').submit(); return false;">Rp Needs Rw</a>
								</form>
								</td><td><%=result.getString("RpNeedsRw")%></td>
								<td style="padding-left:15px"></td>&nbsp;<td>&nbsp;</td>
							</tr>
							
						</table>

					<%}
					db.closeAll();
				}
				catch (Exception e){
					out.print("topnav "+e);
					db.closeAll();
				}
				finally{
					db.closeAll();
				}














	  		}
  		}
  		else
  		{
   		out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
 		}		
	}
	else
		{
			out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
		}
	%> --%>
	<h1></h1>
</body>
</html>
