<%@page language="java" contentType="application/json; charset=UTF-8" import="java.io.*,com.winstaff.*, java.util.*, java.sql.*,com.google.gson.Gson;"%>
<%!class PG_ServiceController {
	 String cptwizardid;
	 String mod;
	 String special;
	 String contrast;
	 String bodypart;
	 String orientation;
	 String cpt1;
	 String bp1;
	 String cpt2;
	 String bp2;
	 String cpt3;
	 String bp3;
	 String cpt4;
	 String bp4;
	 String cpt5;
	 String bp5;
		public PG_ServiceController(String cptwizardid, String mod, String special, String contrast, String bodypart, String orientation, String cpt1, String bp1, String cpt2, String bp2, String cpt3, String bp3, String cpt4, String bp4, String cpt5, String bp5) {
			this.cptwizardid = cptwizardid;
			this.mod = mod;
			this.special = special;
			this.contrast = contrast;
			this.bodypart = bodypart;
			this.orientation = orientation;
			this.cpt1 = cpt1;
			this.bp1 = bp1;
			this.cpt2 = cpt2;
			this.bp2 = bp2;
			this.cpt3 = cpt3;
			this.bp3 = bp3;
			this.cpt4 = cpt4;
			this.bp4 = bp4;
			this.cpt5 = cpt5;
			this.bp5 = bp5;
		}
		public String getCptwizardid() {
			return cptwizardid;
		}
		public void setCptwizardid(String cptwizardid) {
			this.cptwizardid = cptwizardid;
		}
		public String getMod() {
			return mod;
		}
		public void setMod(String mod) {
			this.mod = mod;
		}
		public String getSpecial() {
			return special;
		}
		public void setSpecial(String special) {
			this.special = special;
		}
		public String getContrast() {
			return contrast;
		}
		public void setContrast(String contrast) {
			this.contrast = contrast;
		}
		public String getBodypart() {
			return bodypart;
		}
		public void setBodypart(String bodypart) {
			this.bodypart = bodypart;
		}
		public String getOrientation() {
			return orientation;
		}
		public void setOrientation(String orientation) {
			this.orientation = orientation;
		}
		public String getCpt1() {
			return cpt1;
		}
		public void setCpt1(String cpt1) {
			this.cpt1 = cpt1;
		}
		public String getBp1() {
			return bp1;
		}
		public void setBp1(String bp1) {
			this.bp1 = bp1;
		}
		public String getCpt2() {
			return cpt2;
		}
		public void setCpt2(String cpt2) {
			this.cpt2 = cpt2;
		}
		public String getBp2() {
			return bp2;
		}
		public void setBp2(String bp2) {
			this.bp2 = bp2;
		}
		public String getCpt3() {
			return cpt3;
		}
		public void setCpt3(String cpt3) {
			this.cpt3 = cpt3;
		}
		public String getBp3() {
			return bp3;
		}
		public void setBp3(String bp3) {
			this.bp3 = bp3;
		}
		public String getCpt4() {
			return cpt4;
		}
		public void setCpt4(String cpt4) {
			this.cpt4 = cpt4;
		}
		public String getBp4() {
			return bp4;
		}
		public void setBp4(String bp4) {
			this.bp4 = bp4;
		}
		public String getCpt5() {
			return cpt5;
		}
		public void setCpt5(String cpt5) {
			this.cpt5 = cpt5;
		}
		public String getBp5() {
			return bp5;
		}
		public void setBp5(String bp5) {
			this.bp5 = bp5;
		}
	}
%>
<%
/* '" + search + "%' */
	List<PG_ServiceController> sl = new ArrayList<PG_ServiceController>();
	if (request.getParameter("term") != null && !request.getParameter("term").isEmpty()) {
		String term = request.getParameter("term");
		String[] fullterm = term.trim().split(" ");
		int size = fullterm.length;
		System.out.println("Size:\t"+size);
		if(fullterm.length == 1 && fullterm[0].trim().length() > 1 && fullterm[0].trim() != null){
			String split1 = fullterm[0];
			String query = "select cptwizardid, modality, special, contrast, bodypart, orientation, cpt1, bp1, cpt2, bp2, cpt3, bp3, cpt4, bp4, cpt5 , bp5\n" +
					"from tcptwizard\n" +
					"where bodypart ilike '%"+ split1 + "%'\n" +
					"or bp1 ilike '%"+ split1 + "%'\n" +
					"order by cpt1 asc limit 15";
			searchDB2 db = new searchDB2();
			ResultSet rs = db.executeStatement(query);
			try {
				while (rs.next()) {
					sl.add(new PG_ServiceController(rs.getString("cptwizardid"),rs.getString("modality"),rs.getString("special"),rs.getString("contrast"),rs.getString("bodypart"),rs.getString("orientation"),rs.getString("cpt1"),rs.getString("bp1"),rs.getString("cpt2"),rs.getString("bp2"),rs.getString("cpt3"),rs.getString("bp3"),rs.getString("cpt4"),rs.getString("bp4"),rs.getString("cpt5"),rs.getString("bp5")));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				db.closeAll();
			}
		}else if(fullterm.length == 2 && fullterm[0].trim().length() > 1 && fullterm[0].trim() != null && fullterm[1].trim().length() > 1 && fullterm[1].trim() != null){
			String split1 = fullterm[0];
			String split2 = fullterm[1];
			String query = "select cptwizardid, modality, special, contrast, bodypart, orientation, cpt1, bp1, cpt2, bp2, cpt3, bp3, cpt4, bp4, cpt5, bp5\n" +
					"from tcptwizard\n" +
					"where bp1 ilike '%"+ split1 + "%'\n" +
					"and bodypart ilike '%"+ split2 + "%'\n" +
					"order by cpt1 asc limit 15";
			searchDB2 db = new searchDB2();
			ResultSet rs = db.executeStatement(query);
			try {
				while (rs.next()) {
					sl.add(new PG_ServiceController(rs.getString("cptwizardid"),rs.getString("modality"),rs.getString("special"),rs.getString("contrast"),rs.getString("bodypart"),rs.getString("orientation"),rs.getString("cpt1"),rs.getString("bp1"),rs.getString("cpt2"),rs.getString("bp2"),rs.getString("cpt3"),rs.getString("bp3"),rs.getString("cpt4"),rs.getString("bp4"),rs.getString("cpt5"),rs.getString("bp5")));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				db.closeAll();
			}
		}else if(fullterm.length == 3 && fullterm[0].trim().length() > 1 && fullterm[0].trim() != null && fullterm[1].trim().length() > 1 && fullterm[1].trim() != null && fullterm[2].trim().length() > 1 && fullterm[2].trim() != null){
			String split1 = fullterm[0];
			String split2 = fullterm[1];
			String split3 = fullterm[2];
			String query = "select cptwizardid, modality, special, contrast, bodypart, orientation, cpt1, bp1, cpt2, bp2, cpt3, bp3, cpt4, bp4, cpt5, bp5\n" +
					"from tcptwizard\n" +
					"where bp1 ilike '%"+ split1 + "%'\n" +
					"and bodypart ilike '%"+ split2 + "%'\n" +
					"and orientation ilike '%"+ split3 + "%'\n" +
					"order by cpt1 asc limit 15";
			searchDB2 db = new searchDB2();
			ResultSet rs = db.executeStatement(query);
			try {
				while (rs.next()) {
					sl.add(new PG_ServiceController(rs.getString("cptwizardid"),rs.getString("modality"),rs.getString("special"),rs.getString("contrast"),rs.getString("bodypart"),rs.getString("orientation"),rs.getString("cpt1"),rs.getString("bp1"),rs.getString("cpt2"),rs.getString("bp2"),rs.getString("cpt3"),rs.getString("bp3"),rs.getString("cpt4"),rs.getString("bp4"),rs.getString("cpt5"),rs.getString("bp5")));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				db.closeAll();
			}
		}
	}
	response.setHeader("Access-Control-Allow-Origin", "*");
	out.println(new Gson().toJson(sl));
%>
