<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.* " %>
<%@ include file="../generic/CheckLogin.jsp" %>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />
<%
//initial declaration of list class and parentID
boolean accessValid = true;    
//page security
if (accessValid&&isScheduler2)
{
	try
	{
        java.text.DecimalFormat DFCurrency = new java.text.DecimalFormat(PLCUtils.String_defDecFormatCurrency1);
    	Integer iPayerID = new Integer(request.getParameter("iPayerID"));
    	String testZIP = request.getParameter("testZIP");
    	String testCPT = request.getParameter("testCPT");
    	String testMOD = request.getParameter("testMOD");
		if (testMOD==null){ testMOD="";}
    	String testDATE = request.getParameter("testDATE");
		java.util.Date EffDate = PLCUtils.getSubDate(testDATE);
		Double theFee = NIMUtils.getPayerAllowAmount(iPayerID,testZIP, testCPT, testMOD,EffDate );
		Double theFeeBill = NIMUtils.getPayerBillAmount(iPayerID,testZIP, testCPT, testMOD,EffDate );
        %>

<table border="1" cellspacing="0" cellpadding="5" align="center">
  <tr>
    <td colspan="2"><h1 class="big3">Fee Schedule Lookup</h1></td>
  </tr>
<tr><td><h2>Allow/Contract Amount:</h2>
        Testing <strong><%=testCPT%></strong> [<strong><%=testMOD%></strong>] in <strong><%=testZIP%></strong> on <strong><%=testDATE%></strong><br />
        The price should be: <%=DFCurrency.format(theFee)%>

</td>
  <td><h2>Bill Amount:</h2>
        Testing <strong><%=testCPT%></strong> in <strong><%=testZIP%></strong> on <strong><%=testDATE%></strong><br />
    The price should be: <%=DFCurrency.format(theFeeBill)%></td>
</tr>
<tr><td valign="top"><h2>Radiology Table[Allow]:</h2>
<%="mr_wo = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(iPayerID,testZIP,"mr_wo","",EffDate))%><br />
<%="mr_w = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(iPayerID,testZIP,"mr_w","",EffDate))%><br />
<%="mr_wwo = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(iPayerID,testZIP,"mr_wwo","",EffDate))%><br />
<%="ct_wo = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(iPayerID,testZIP,"ct_wo","",EffDate))%><br />
<%="ct_w = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(iPayerID,testZIP,"ct_w","",EffDate))%><br />
<%="ct_wwo = " + DFCurrency.format(NIMUtils.getPayerAllowAmount(iPayerID,testZIP,"ct_wwo","",EffDate))%><br />
        

</td>
  <td valign="top"><h2>Radiology Table[Bill]:</h2>
<%="mr_wo = " + DFCurrency.format(NIMUtils.getPayerBillAmount(iPayerID,testZIP,"mr_wo","",EffDate))%><br />
<%="mr_w = " + DFCurrency.format(NIMUtils.getPayerBillAmount(iPayerID,testZIP,"mr_w","",EffDate))%><br />
<%="mr_wwo = " + DFCurrency.format(NIMUtils.getPayerBillAmount(iPayerID,testZIP,"mr_wwo","",EffDate))%><br />
<%="ct_wo = " + DFCurrency.format(NIMUtils.getPayerBillAmount(iPayerID,testZIP,"ct_wo","",EffDate))%><br />
<%="ct_w = " + DFCurrency.format(NIMUtils.getPayerBillAmount(iPayerID,testZIP,"ct_w","",EffDate))%><br />
<%="ct_wwo = " + DFCurrency.format(NIMUtils.getPayerBillAmount(iPayerID,testZIP,"ct_wwo","",EffDate))%><br />
    </td>
  </tr>
</table>        <%
	}
	catch(Exception e)
	{
	    out.println("<hr>" + e + "<hr />");
	}
	
}
else
{
	out.println("illegal");
}
%>
