<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*     " %>
<%/*
    filename: tNIM_Authorization_main_NIM_Authorization_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td><hr></td></tr>
    <tr><td width=10>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM_Authorization")%>



<%
//initial declaration of list class and parentID
    Integer        iAuthorizationID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat("MM/dd/yyyy");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iAuthorizationID")) 
    {
        iAuthorizationID        =    (Integer)pageControllerHash.get("iAuthorizationID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
	  bltNIM2_Authorization NIM2_Authorization = new bltNIM2_Authorization(iAuthorizationID);
	  bltNIM2_CaseAccount NCA = new bltNIM2_CaseAccount(NIM2_Authorization.getCaseID());
	  bltUserAccount myUA = new bltUserAccount(new Integer(NCA.getAdjusterID()));

	com.winstaff.password.Encrypt myEnc = new com.winstaff.password.Encrypt();
	String P1 = ""+Math.round(Math.random()*100000000);
	if (P1.length()<8)
	{
		P1 = P1 + "5";
	}
	String myNewPassword = P1;
myUA.setLogonUserPassword( myEnc.getMD5Base64(P1) );

	    try
	    {
				if (!myUA.getLogonUserName().equalsIgnoreCase("Declined")&&!myUA.getLogonUserName().equalsIgnoreCase(""))
				{
							//send email user
//					com.winstaff.password.Encrypt myEnc = new com.winstaff.password.Encrypt();
//					String myNewPassword = myEnc.getRandomPassword();
//					UserAccount.setLogonUserPassword(myEnc.getMD5Base64(myNewPassword));
					myUA.commitData();
					emailType_V3 myETSend = new emailType_V3();
//					myETSend.setTo(UserAccount.getLogonUserName());
					myETSend.setFrom("support@nextimagemedical.com");
					myETSend.setSubject("NextImage: Account Update");
					String theBody = myUA.getContactFirstName() + ",\n\nWelcome to the NextImage Medical system. Due to HIPAA Compliance, some information that we send you will be sent using a secure connection. Your username will be your eMail address: " + myUA.getLogonUserName() + " and your password will be: " + myNewPassword + "\n\nIf you have any questions, please call us at 888-318-5111.\n\nThank You";
					myETSend.setBody(theBody);
					myETSend.setBodyType(myETSend.PLAIN_TYPE);
					myETSend.setTo("eayala@nextimagemedical.com");
					myETSend.isSendMail();
					myETSend.setTo("scott@nextimagemedical.com");
					myETSend.isSendMail();
					myETSend.setTo("andy@nextimagemedical.com");
					myETSend.isSendMail();
				//send email to Andy & Scott
					//create update Item
				}

	    }
	    catch (Exception e55)
	    {
			out.print("Commit Error" + e55);
//	        errorRouteMe = true;
	    }






%>
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td><h1> Email Sent</h1></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>For Logon: <%=myUA.getLogonUserName()%></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>New Password is: <%=myNewPassword%></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="button" onclick="this.disabled=true;document.location='tNIM2_Authorization_main_NIM2_Authorization_CaseID.jsp?nullParam=null&amp;EDIT=open&amp;EDITID=1&amp;KM=p';" value="Continue" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr> 
    </table>
<br>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>