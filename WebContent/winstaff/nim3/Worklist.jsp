<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">


</head>
<SCRIPT type="text/javascript">
function openStatus()
{
	testwindow= window.open ("Home_Status.jsp", "HomeStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(0,0);
}

function openAuto(myAuto)
{
	testwindow= window.open ("report_auto2.jsp?rid=" + myAuto + "&MREF=300", "AutoStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700")
	testwindow.moveTo(100,100);
}

function commTrackForm(IW,iEncounterID){
		$("#commTrackForm").html("Add CommTrack for: <br><strong>"+IW+"</strong><br><textarea id=\"messageText_"+iEncounterID+"\" style=\"resize:none; margin: 2px;width: 292px;height: 244px;\"></textarea><input type=submit onclick=\"ajaxSubmit("+iEncounterID+")\">");
		$("#commTrackForm, #faded-bg").show();
		windowwidth = ($(window).width()-300)/2;
		windowheight =($(window).height()-300)/2;

		$("#commTrackForm").css('left',windowwidth);
		$("#commTrackForm").css('top',windowheight);
		
		$("#faded-bg").css({'width':$(window).width(),'height':$(window).height()});
}
function ajaxSubmit(iEncounterID){
	var messageText = $('textarea#messageText_'+iEncounterID).val();
	var dataString = "messageText="+messageText+"&iEncounterID="+iEncounterID;
	$.ajax({
		type: "GET",
		url: "worklist_action.jsp",
		data: dataString,
		success: function() {
			alert("Success");
		}
	});
	$("#commTrackForm, #faded-bg").hide().html("");
}
</SCRIPT>


<body>
<%String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?iDelay=-1&plcID="+thePLCID;%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<script>
$(document).ready(function() {
	$("#faded-bg").click(function(){
		$("#commTrackForm, #faded-bg").hide().html("");
	});
	
});
</script>
	<%boolean accessValid = false;
	Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
	Integer iPayerID = null;
	Integer iAdjusterID = null;
	if (iSecurityCheck.intValue()!=0)
	{
		if (isScheduler) 
		{
			accessValid = true;
		}
		else if (pageControllerHash.containsKey("iAdjusterID")&&pageControllerHash.containsKey("iPayerID")) 
		{
			iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
	//        iAdjusterID        =    (Integer)pageControllerHash.get("iAdjusterID");
			isAdjuster = true;
			accessValid = true;
		}
		else if (pageControllerHash.containsKey("iAdjusterID2")&&pageControllerHash.containsKey("iPayerID")) 
		{
			iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
	//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
			isAdjuster2 = true;
			accessValid = true;
		}
		else if (pageControllerHash.containsKey("iAdjusterID3")&&pageControllerHash.containsKey("iPayerID")) 
		{
			iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
	//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
			isAdjuster3 = true;
			accessValid = true;
		}
		else if (isProvider&&pageControllerHash.containsKey("iPayerID")) 
		{
			iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
			accessValid = true;
		}
		
		boolean isShowAll = false;	
		//page security
		if (accessValid)
		{
		 	java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
			java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
			pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
			//pageControllerHash.remove("iCaseID");
			int wlCount = 0;
			int wlArray[];
			int maxWL = 22;
			wlArray = new int[maxWL];
			java.util.HashMap<String,Boolean> chosenWL = new java.util.HashMap<String,Boolean>();
 			chosenWL.put ("showWL_receiptsent",false);
			chosenWL.put ("showWL_prescreen",false);
			chosenWL.put ("showWL_missingrx",false);
			chosenWL.put ("showWL_rxreview",false);
			chosenWL.put ("showWL_readytoschedule",false);
			chosenWL.put ("showWL_missingreport",false);
			chosenWL.put ("showWL_needsrpreview",false);
			chosenWL.put ("showWL_nad",false);
			chosenWL.put ("showWL_ct",false);
			chosenWL.put ("showWL_unassigned",false);
			chosenWL.put ("showWL_stillactive",false);
			chosenWL.put ("showWL_holdbill",false);
			chosenWL.put ("showWL_stat",false);
			chosenWL.put ("showWL_76140",false);
			chosenWL.put ("showWL_pending",false);
			chosenWL.put ("showWL_escalatedcases",false);
			chosenWL.put ("showWL_innetdev",false);
			chosenWL.put ("showWL_stalecases",false);
			chosenWL.put("showWL_appointmentpending", false);
			chosenWL.put("showWL_otanetdev", false);
			chosenWL.put("showWL_allcasestouched", false);
			chosenWL.put("showWL_ftprc_pc", false);
			chosenWL.put("showWL_ftprp_pc", false);
			boolean updateWL = false;
		  
			if (request.getParameter("showWL_receiptsent")!=null){
			  chosenWL.put ("showWL_receiptsent",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_prescreen")!=null){
			  chosenWL.put ("showWL_prescreen",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_missingrx")!=null){
			  chosenWL.put ("showWL_missingrx",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_rxreview")!=null){
			  chosenWL.put ("showWL_rxreview",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_readytoschedule")!=null){
			  chosenWL.put ("showWL_readytoschedule",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_needsrpreview")!=null){
			  chosenWL.put ("showWL_needsrpreview",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_stillactive")!=null){
			  chosenWL.put ("showWL_stillactive",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_holdbill")!=null){
			  chosenWL.put ("showWL_holdbill",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_nad")!=null){
			  chosenWL.put ("showWL_nad",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_unassigned")!=null){
			  chosenWL.put ("showWL_unassigned",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_stat")!=null){
			  chosenWL.put ("showWL_stat",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_76140")!=null){
			  chosenWL.put ("showWL_76140",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_pending")!=null){
			  chosenWL.put ("showWL_pending",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_missingreport")!=null){
			  chosenWL.put ("showWL_missingreport",true);
			  updateWL = true;
			}	
			if (request.getParameter("showWL_ct")!=null){
			  chosenWL.put ("showWL_ct",true);
			  updateWL = true;
			}
			if (request.getParameter("showWL_escalatedcases")!=null){
			  chosenWL.put ("showWL_escalatedcases",true);
		 	  updateWL = true;
			}
			if (request.getParameter("showWL_innetdev")!=null){
				  chosenWL.put ("showWL_innetdev",true);
			 	  updateWL = true;
			}
			if (request.getParameter("showWL_stalecases")!=null){
				  chosenWL.put ("showWL_stalecases",true);
			 	  updateWL = true;
			}
			if (request.getParameter("showWL_appointmentpending")!=null){
				  chosenWL.put ("showWL_appointmentpending",true);
				  updateWL = true;
			}
			if (request.getParameter("showWL_otanetdev")!=null){
				  chosenWL.put ("showWL_otanetdev",true);
				  updateWL = true;
			}
			if (request.getParameter("showWL_allcasestouched")!=null){
				  chosenWL.put ("showWL_allcasestouched",true);
				  updateWL = true;
			}
			if (request.getParameter("showWL_ftprc_pc")!=null){
				  chosenWL.put ("showWL_ftprc_pc",true);
				  updateWL = true;
			}
			if (request.getParameter("showWL_ftprp_pc")!=null){
				  chosenWL.put ("showWL_ftprp_pc",true);
				  updateWL = true;
			}
			
			if (updateWL){
			  pageControllerHash.put("chosenWL",chosenWL);
			  session.setAttribute("pageControllerHash",pageControllerHash);
			} 
			else {
			  if (pageControllerHash.containsKey("chosenWL")){
				  chosenWL = (java.util.HashMap) pageControllerHash.get("chosenWL");
			  }
			}
			if (chosenWL.get("showWL_receiptsent")){
				wlArray[wlCount] = 1;
				wlCount++;
			}
			if (chosenWL.get("showWL_prescreen")){
				wlArray[wlCount] = 2;
				wlCount++;
			}
			if (chosenWL.get("showWL_missingrx")){
				wlArray[wlCount] = 3;
				wlCount++;
			}
			if (chosenWL.get("showWL_rxreview")){
				wlArray[wlCount] = 4;
				wlCount++;
			}
			if (chosenWL.get("showWL_readytoschedule")){
				wlArray[wlCount] = 5;
				wlCount++;
			}
			if (chosenWL.get("showWL_needsrpreview")){
				wlArray[wlCount] = 6;
				wlCount++;
			}
			if (chosenWL.get("showWL_stillactive")){
				wlArray[wlCount] = 7;
				wlCount++;
			}
			if (chosenWL.get("showWL_holdbill")){
				wlArray[wlCount] = 8;
				wlCount++;
			}
			if (chosenWL.get("showWL_nad")){
				wlArray[wlCount] = 9;
				wlCount++;
			}
			if (chosenWL.get("showWL_unassigned")){
				wlArray[wlCount] = 10;
				wlCount++;
			}
			if (chosenWL.get("showWL_stat")){
				wlArray[wlCount] = 11;
				wlCount++;
			}
			if (chosenWL.get("showWL_76140")){
				wlArray[wlCount] = 12;
				wlCount++;
			}
			if (chosenWL.get("showWL_pending")){
				wlArray[wlCount] = 13;
				wlCount++;
			}
			if (chosenWL.get("showWL_ct")){
				wlArray[wlCount] = 14;
				wlCount++;
			}
			if (chosenWL.get("showWL_missingreport")){
				wlArray[wlCount] = 15;
				wlCount++;
			}
			if (chosenWL.get("showWL_escalatedcases")){
				wlArray[wlCount] = 16;
				wlCount++;
			}
			if (chosenWL.get("showWL_innetdev")){
				wlArray[wlCount] = 17;
				wlCount++;
			}
			if (chosenWL.get("showWL_stalecases")){
				wlArray[wlCount] = 18;
				wlCount++;
			}
			if (chosenWL.get("showWL_appointmentpending")){
				wlArray[wlCount] = 19;
				wlCount++;
			}
			if (chosenWL.get("showWL_otanetdev")){
				wlArray[wlCount] = 20;
				wlCount++;
			}
			if (chosenWL.get("showWL_allcasestouched")){
				wlArray[wlCount] = 21;
				wlCount++;
			}
			if (chosenWL.get("showWL_ftprc_pc")){
				wlArray[wlCount] = 22;
				wlCount++;
			}
			if (chosenWL.get("showWL_ftprp_pc")){
				wlArray[wlCount] = 23;
				wlCount++;
			}
			
			if (isScheduler)
			{
			  String theScheduler1 = CurrentUserAccount.getUserID().toString();
			  String theScheduler2 = "";
			  String theScheduler3 = "";
			  if (pageControllerHash.containsKey("theScheduler1")){
					theScheduler1 = (String) pageControllerHash.get("theScheduler1");
			  }
			  if (pageControllerHash.containsKey("theScheduler2")){
					theScheduler2 = (String) pageControllerHash.get("theScheduler2");
			  }
			  if (pageControllerHash.containsKey("theScheduler3")){
					theScheduler3 = (String) pageControllerHash.get("theScheduler3");
			  }
			  String myAT_Where = "";
			  if ( request.getParameter("theScheduler1")!=null&&!request.getParameter("theScheduler1").equalsIgnoreCase("") )
			  {
				  theScheduler1 = request.getParameter("theScheduler1");
			  }
			  if ( request.getParameter("theScheduler2")!=null )
			  {
				  theScheduler2 = request.getParameter("theScheduler2");
			  }
			  if ( request.getParameter("theScheduler3")!=null )
			  {
				  theScheduler3 = request.getParameter("theScheduler3");
			  }
			  pageControllerHash.put("theScheduler1",theScheduler1);
			  pageControllerHash.put("theScheduler2",theScheduler2);
			  pageControllerHash.put("theScheduler3",theScheduler3);
			  
			  if (!theScheduler1.equalsIgnoreCase("0"))
			  {
				  myAT_Where =  " AND tNIM3_CaseAccount.assignedtoid in ('" + theScheduler1;
			  } 
			  if (!theScheduler2.equalsIgnoreCase("-98"))
			  {
				  myAT_Where +=  "' , '" + theScheduler2;
			  } 
			  if (!theScheduler3.equalsIgnoreCase("-98"))
			  {
				  myAT_Where +=  "' , '" + theScheduler3;
			  } 
			  if (!theScheduler1.equalsIgnoreCase("0")){
				  myAT_Where += "')";
			  }
			  else {
				  isShowAll = true;
			  }
			  
			//Extract user name using userid to get results from commtrack table -AJ 05/07
				String myAT_Where2 = "";
				
				if(chosenWL.get("showWL_allcasestouched") == true){
					searchDB2 mySSNameHolder = new searchDB2();
					java.sql.ResultSet myRSNameHolder1 = null;;
					java.sql.ResultSet myRSNameHolder2 = null;;
					java.sql.ResultSet myRSNameHolder3 = null;;
					
					try{			
						myRSNameHolder1 = mySSNameHolder.executeStatement("SELECT contactfirstname\n," +
											 						"contactlastname\n" +
												 	                "FROM tuseraccount ua\n" +
															  		"WHERE ua.userid = " + theScheduler1);
						
						myRSNameHolder2 = mySSNameHolder.executeStatement("SELECT contactfirstname\n," +
																	"contactlastname\n" +
													                "FROM tuseraccount ua\n" +
															  		"WHERE ua.userid = " + theScheduler2);
						
						myRSNameHolder3 = mySSNameHolder.executeStatement("SELECT contactfirstname\n," +
																   	"contactlastname\n" +
												                   	"FROM tuseraccount ua\n" +
														  		   	"WHERE ua.userid = " + theScheduler3);
					  }
					  catch(Exception e){
						  out.println("ResultsSet:"+e);
					  }
					  
					 try{
						//Scheduler 1
						while (myRSNameHolder1!=null && myRSNameHolder1.next()){	
							if (theScheduler1.equalsIgnoreCase("0")){
							 myAT_Where2 =  "";
							}
							else if (!theScheduler1.equalsIgnoreCase("0")){		
								  myAT_Where2 = " AND ct.messagename ILIKE " + "'" + myRSNameHolder1.getString("contactfirstname") + "%" + " " + myRSNameHolder1.getString("contactlastname") + "%'";
							}
						}
						
						//Scheduler 2
						if (!theScheduler2.equalsIgnoreCase("-98")){
							while (myRSNameHolder2!=null && myRSNameHolder2.next()){
							  myAT_Where2 += " OR to_char(ct.uniquecreatedate, 'yyyy-mm-dd') = to_char(now(), 'yyyy-mm-dd') AND ct.messagename ILIKE " + "'" + myRSNameHolder2.getString("contactfirstname") + "%" + " " + myRSNameHolder2.getString("contactlastname") + "%'";
							}
						 }
						
						//Scheduler 3
						if (!theScheduler3.equalsIgnoreCase("-98")){
							while (myRSNameHolder3!=null && myRSNameHolder3.next()){
								myAT_Where2 += " OR to_char(ct.uniquecreatedate, 'yyyy-mm-dd') = to_char(now(), 'yyyy-mm-dd') AND ct.messagename ILIKE " + "'" + myRSNameHolder3.getString("contactfirstname") + "%" + " " + myRSNameHolder3.getString("contactlastname") + "%'";
							}
						}
					
						mySSNameHolder.closeAll();	  
						//out.println(theScheduler1 + "&" + theScheduler2 + "&" + theScheduler3);
					 }
					 catch(Exception e){
					 	out.println("While:"+e);
					 }
					 
					 //out.print(myAT_Where2);
				}
			%>
            <%
			String[][] schedulerList;
            schedulerList = new String[4][100];
			int count = 0;
			
			String delims = "[_]";
			String tempString;
			%>
            
            <%!
			
			
            String mySQL_worklist = "";
            String tempStatus = "";
			searchDB2 mySS_worklist = new searchDB2();
			java.sql.ResultSet myRS_worklist = null;
			java.sql.ResultSet myRS_worklist2 = null;
					
            public String wlSQL(int chosenWL, String myAT_Where){
            String mySQL_worklist = "";
			
            //Receipt Confirmations
			if (chosenWL == 1){
            	//mySQL_worklist = "select tnim3_encounter.scanpass, tNIM3_encounter.encounterid, tnim3_referral.receivedate,tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname,patient_state_link.shortstate as patient_state, patient_state_link.stateid as stateid, tnim3_caseaccount.caseid, ua_assignedto.contactfirstname as assignedto_firstname, tnim3_encounter.isstat, tnim3_encounter.seenetdev_waiting, tNIM3_Referral.receivedate, tNIM3_Encounter.nextactiondate, tnim3_encounter.NextActionNotes,tNIM3_PayerMaster.PayerName from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerID INNER JOIN tuseraccount as ua_assignedto on ua_assignedto.userid = tnim3_caseaccount.assignedtoid INNER JOIN tstateli as patient_state_link on patient_state_link.stateid = tnim3_caseaccount.patientstateid where tNIM3_Encounter.AppointmentID=0 AND tNIM3_CaseAccount.AdjusterID>0  " + myAT_Where + " AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, true) + ")  order by tNIM3_Referral.ReceiveDate";	
            	mySQL_worklist = "select tNIM3_encounter.encounterid from tNIM3_Encounter  where tNIM3_Encounter.AppointmentID=0  AND (cast(sentto_reqrec_adj as varchar) like '1800%' or cast(sentto_reqrec_refdr as varchar) like '1800%') "+ myAT_Where +" AND tNIM3_Encounter.encounterstatusid in ('1','12') order by encounterid";
			}
			
			//Needs PreScreen
			if (chosenWL == 2){
				mySQL_worklist = "select tnim3_encounter.scanpass, tNIM3_encounter.encounterid, tnim3_referral.receivedate,tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname,patient_state_link.shortstate as patient_state, patient_state_link.stateid as stateid, tnim3_caseaccount.caseid, ua_assignedto.contactfirstname as assignedto_firstname, tnim3_encounter.isstat, tnim3_encounter.seenetdev_waiting, tNIM3_Referral.receivedate, tNIM3_Encounter.nextactiondate, tnim3_encounter.NextActionNotes,tNIM3_PayerMaster.PayerName from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerID INNER JOIN tuseraccount as ua_assignedto on ua_assignedto.userid = tnim3_caseaccount.assignedtoid INNER JOIN tstateli as patient_state_link on patient_state_link.stateid = tnim3_caseaccount.patientstateid where tNIM3_Encounter.AppointmentID=0 AND (patientisclaus = 0 OR patientishwratio = 0 OR patienthasimplants = 0 OR patienthasmetalinbody = 0 OR patienthasallergies = 0 OR patienthasrecentsurgery = 0 OR patienthaspreviousmris = 0 OR patienthaskidneyliverhypertensiondiabeticconditions = 0 OR patientispregnant = 0) " + myAT_Where + " AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, true) + ")  order by tNIM3_Referral.ReceiveDate";
            }
			
			//Missing Rx
			if (chosenWL == 3){
				mySQL_worklist = "SELECT DISTINCT en.encounterid \n" +
								"FROM tnim3_encounter en\n" +
								"INNER JOIN tnim3_referral ref on ref.referralid = en.referralid\n" +
								"INNER JOIN tNIM3_CaseAccount ON tNIM3_CaseAccount.caseid = ref.caseid\n" +
								"WHERE en.encounterstatusid IN ('1','12')\n" +
								"AND en.uniquecreatedate > (now() - '3 month'::interval)\n" +
								myAT_Where +
								"AND ref.rxfileid = 0\n" +
								"ORDER BY en.encounterid";
			}
			//Needs Rx Review
			if (chosenWL == 4){
				mySQL_worklist = "select tnim3_encounter.scanpass, service.encounterid, tnim3_referral.receivedate,tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname,patient_state_link.shortstate as patient_state, patient_state_link.stateid as stateid,tnim3_caseaccount.caseid, ua_assignedto.contactfirstname as assignedto_firstname, tnim3_encounter.isstat, tnim3_encounter.seenetdev_waiting, tNIM3_Referral.receivedate, tNIM3_Encounter.nextactiondate, tnim3_encounter.NextActionNotes,tNIM3_PayerMaster.PayerName from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerID INNER JOIN tuseraccount as ua_assignedto on ua_assignedto.userid = tnim3_caseaccount.assignedtoid INNER JOIN (select distinct tnim3_service.encounterid from tnim3_service)as service on service.encounterid = tnim3_encounter.encounterid INNER JOIN tstateli as patient_state_link on patient_state_link.stateid = tnim3_caseaccount.patientstateid where tNIM3_Encounter.AppointmentID=0 AND tnim3_referral.rxfileid>0 AND tnim3_referral.orderfileid>0 AND tnim3_referral.referringphysicianid>0 AND tnim3_caseaccount.adjusterid>0 AND tnim3_caseaccount.patientstateid>0 AND tnim3_caseaccount.patientaddress1 != '' AND tNIM3_encounter.timetrack_reqrxreview_userid=0 AND tnim3_referral.referralstatusid = '1'" + myAT_Where + " AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, true) + ")  order by tNIM3_Referral.ReceiveDate";
			}
			//Ready To Schedule
			if (chosenWL == 5){
				mySQL_worklist = "select distinct tnim3_encounter.scanpass, tNIM3_encounter.encounterid, tnim3_referral.receivedate,tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname,patient_state_link.shortstate as patient_state, patient_state_link.stateid as stateid,tnim3_caseaccount.caseid, ua_assignedto.contactfirstname as assignedto_firstname, tnim3_encounter.isstat, tnim3_encounter.seenetdev_waiting, tNIM3_Referral.receivedate, tNIM3_Encounter.nextactiondate, tnim3_encounter.NextActionNotes,tNIM3_PayerMaster.PayerName from tNIM3_Encounter INNER JOIN tnim3_service on tnim3_service.encounterid = tnim3_encounter.encounterid INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerID INNER JOIN tuseraccount as ua_assignedto on ua_assignedto.userid = tnim3_caseaccount.assignedtoid INNER JOIN tstateli as patient_state_link on patient_state_link.stateid = tnim3_caseaccount.patientstateid where tNIM3_Encounter.AppointmentID=0 AND tnim3_referral.rxfileid>0 AND (patientisclaus >0 AND patientishwratio >0 AND patienthasimplants >0 AND patienthasmetalinbody >0 AND patienthasallergies >0 AND patienthasrecentsurgery >0 AND patienthaspreviousmris >0 AND patienthaskidneyliverhypertensiondiabeticconditions >0 AND patientispregnant >0) and tNIM3_encounter.timetrack_reqrxreview_userid>0  " + myAT_Where + " AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, true) + ") and cpt !='76140' order by tNIM3_Referral.ReceiveDate";
			}
			
			//Needs Report Review
			if (chosenWL == 6){
				mySQL_worklist = "select DISTINCT encounterid from tnim3_encounter inner join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid where encounterstatusid in ('1','12') and appointmenttime < now() " + myAT_Where + " and reportfileid!=0 and timetrack_reqrpreview_userid=0 order by encounterid";
			}
			
			//Still Active: Why?
			if (chosenWL == 7){
				mySQL_worklist = "select tnim3_document.uniquecreatedate as docdate, tnim3_encounter.scanpass, tNIM3_encounter.encounterid, tnim3_referral.receivedate,tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname,patient_state_link.shortstate as patient_state, patient_state_link.stateid as stateid,tnim3_caseaccount.caseid, ua_assignedto.contactfirstname as assignedto_firstname, tnim3_encounter.isstat, tnim3_encounter.seenetdev_waiting, tNIM3_Referral.receivedate, tNIM3_Encounter.nextactiondate, tnim3_encounter.NextActionNotes,tNIM3_PayerMaster.PayerName from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerID INNER JOIN tuseraccount as ua_assignedto on ua_assignedto.userid = tnim3_caseaccount.assignedtoid INNER JOIN tstateli as patient_state_link on patient_state_link.stateid = tnim3_caseaccount.patientstateid INNER JOIN tnim3_document on tnim3_document.documentid = tnim3_encounter.reportfileid where tNIM3_Encounter.AppointmentID>0 AND tNIM3_Encounter.reportfileid>0 AND tNIM3_encounter.timetrack_reqrpreview_userid>0 and tnim3_referral.receivedate> '2011-01-01' " + myAT_Where + " AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, false) + ")  order by tNIM3_Referral.ReceiveDate";
			}
			
			//Hold Bill
			if (chosenWL == 8){
				mySQL_worklist = "select tnim3_document.uniquecreatedate as docdate, tnim3_encounter.scanpass, tNIM3_encounter.encounterid, tnim3_referral.receivedate,tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname,patient_state_link.shortstate as patient_state, patient_state_link.stateid as stateid,tnim3_caseaccount.caseid, ua_assignedto.contactfirstname as assignedto_firstname, tnim3_encounter.isstat, tnim3_encounter.seenetdev_waiting, tNIM3_Referral.receivedate, tNIM3_Encounter.nextactiondate, tnim3_encounter.NextActionNotes,tNIM3_PayerMaster.PayerName from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerID INNER JOIN tuseraccount as ua_assignedto on ua_assignedto.userid = tnim3_caseaccount.assignedtoid INNER JOIN tstateli as patient_state_link on patient_state_link.stateid = tnim3_caseaccount.patientstateid INNER JOIN tnim3_document on tnim3_document.documentid = tnim3_encounter.reportfileid where tNIM3_Encounter.encounterstatusid in (7) " + myAT_Where + "  order by tNIM3_Referral.ReceiveDate";
			}
			
			//Next Action Date Due
			if (chosenWL == 9){
				mySQL_worklist = "select tnim3_encounter.scanpass, tNIM3_encounter.encounterid, tnim3_referral.receivedate,tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname,patient_state_link.shortstate as patient_state, patient_state_link.stateid as stateid,tnim3_caseaccount.caseid, ua_assignedto.contactfirstname as assignedto_firstname, tnim3_encounter.isstat, tnim3_encounter.seenetdev_waiting, tNIM3_Referral.receivedate, tNIM3_Encounter.nextactiondate, tnim3_encounter.NextActionNotes,tNIM3_PayerMaster.PayerName from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerID INNER JOIN tuseraccount as ua_assignedto on ua_assignedto.userid = tnim3_caseaccount.assignedtoid INNER JOIN tstateli as patient_state_link on patient_state_link.stateid = tnim3_caseaccount.patientstateid where Date(tNIM3_Encounter.NextActionDate)<=Date(Now()) and tNIM3_Encounter.NextActionDate>'2000-01-01'  " + myAT_Where + " AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, true) + ")  order by tNIM3_Referral.ReceiveDate";
			}
			//Unassigned Cases
			if (chosenWL == 10){
				mySQL_worklist = "select tnim3_encounter.scanpass, tNIM3_encounter.encounterid, tnim3_referral.receivedate,tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname,patient_state_link.shortstate as patient_state, patient_state_link.stateid as stateid,tnim3_caseaccount.caseid, ua_assignedto.contactfirstname as assignedto_firstname, tnim3_encounter.isstat, tnim3_encounter.seenetdev_waiting, tNIM3_Referral.receivedate, tNIM3_Encounter.nextactiondate, tnim3_encounter.NextActionNotes,tNIM3_PayerMaster.PayerName from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerID INNER JOIN tuseraccount as ua_assignedto on ua_assignedto.userid = tnim3_caseaccount.assignedtoid INNER JOIN tstateli as patient_state_link on patient_state_link.stateid = tnim3_caseaccount.patientstateid where tnim3_caseaccount.assignedtoid=0  AND tNIM3_Encounter.encounterstatusid in (1)  order by tNIM3_Referral.ReceiveDate";
			}
			
			//Stat Cases
			if (chosenWL == 11){
				mySQL_worklist = "select tnim3_encounter.scanpass, tNIM3_encounter.encounterid, tnim3_referral.receivedate,tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname,patient_state_link.shortstate as patient_state, patient_state_link.stateid as stateid,tnim3_caseaccount.caseid, ua_assignedto.contactfirstname as assignedto_firstname, tnim3_encounter.isstat, tnim3_encounter.seenetdev_waiting, tNIM3_Referral.receivedate, tNIM3_Encounter.nextactiondate, tnim3_encounter.NextActionNotes,tNIM3_PayerMaster.PayerName from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerID INNER JOIN tuseraccount as ua_assignedto on ua_assignedto.userid = tnim3_caseaccount.assignedtoid LEFT JOIN tnim3_appointment on tnim3_appointment.initialencounterid  = tnim3_encounter.encounterid INNER JOIN tstateli as patient_state_link on patient_state_link.stateid = tnim3_caseaccount.patientstateid where tnim3_encounter.isstat=1 AND tnim3_encounter.appointmentid = 0 AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, false) + ")  order by tNIM3_Referral.ReceiveDate";
			}
			
			//Open CPT 76140
			if (chosenWL == 12){
				mySQL_worklist = "select tnim3_encounter.scanpass, tNIM3_encounter.encounterid, tnim3_referral.receivedate,tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname,patient_state_link.shortstate as patient_state, patient_state_link.stateid as stateid,tnim3_caseaccount.caseid, ua_assignedto.contactfirstname as assignedto_firstname, tnim3_encounter.isstat, tnim3_encounter.seenetdev_waiting, tNIM3_Referral.receivedate, tNIM3_Encounter.nextactiondate, tnim3_encounter.NextActionNotes,tNIM3_PayerMaster.PayerName from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerID INNER JOIN tuseraccount as ua_assignedto on ua_assignedto.userid = tnim3_caseaccount.assignedtoid INNER JOIN tstateli as patient_state_link on patient_state_link.stateid = tnim3_caseaccount.patientstateid INNER JOIN tnim3_service on tnim3_service.encounterid = tnim3_encounter.encounterid LEFT JOIN tnim3_appointment on tnim3_appointment.initialencounterid  = tnim3_encounter.encounterid where tnim3_service.cpt = '76140' AND servicestatusid = '1' AND tnim3_appointment.appointmentid is null AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, false) + ")  order by tNIM3_Referral.ReceiveDate";
			}
			
			//Referral Status Pending or N/A
			if (chosenWL == 13){
				mySQL_worklist = "select tnim3_encounter.scanpass, tNIM3_encounter.encounterid, tnim3_referral.receivedate,tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname,patient_state_link.shortstate as patient_state, patient_state_link.stateid as stateid,tnim3_caseaccount.caseid, ua_assignedto.contactfirstname as assignedto_firstname, tnim3_encounter.isstat, tnim3_encounter.seenetdev_waiting, tNIM3_Referral.receivedate, tNIM3_Encounter.nextactiondate, tnim3_encounter.NextActionNotes,tNIM3_PayerMaster.PayerName from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerID INNER JOIN tuseraccount as ua_assignedto on ua_assignedto.userid = tnim3_caseaccount.assignedtoid INNER JOIN tstateli as patient_state_link on patient_state_link.stateid = tnim3_caseaccount.patientstateid inner join tnim3_service on tnim3_service.encounterid = tnim3_encounter.encounterid where (referralstatusid = '6' or referralstatusid = '0') AND servicestatusid = 1 AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, false) + ")  order by tNIM3_Referral.ReceiveDate";
			}
			//Missing reports
			if (chosenWL == 15){
				/* mySQL_worklist = "select tNIM3_encounter.encounterid, tnim3_encounter.scanpass, tnim3_referral.receivedate,tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname,patient_state_link.shortstate as patient_state, patient_state_link.stateid as stateid,tnim3_caseaccount.caseid, ua_assignedto.contactfirstname as assignedto_firstname, tnim3_encounter.isstat, tnim3_encounter.seenetdev_waiting, tNIM3_Referral.receivedate, tNIM3_Encounter.nextactiondate, tnim3_encounter.NextActionNotes,tNIM3_PayerMaster.PayerName from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerID INNER JOIN tuseraccount as ua_assignedto on ua_assignedto.userid = tnim3_caseaccount.assignedtoid INNER JOIN tstateli as patient_state_link on patient_state_link.stateid = tnim3_caseaccount.patientstateid INNER JOIN tNIM3_Appointment on tnim3_appointment.appointmentid = tnim3_encounter.appointmentid INNER JOIN tnim3_service on tnim3_service.encounterid = tnim3_encounter.encounterid " + 
		"where tNIM3_Encounter.AppointmentID>0  AND  tNIM3_Encounter.ReportFileID=0 AND encounterstatusid=1 AND servicestatusid=1 AND now() > appointmenttime" +
		myAT_Where + " AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, true) + ")  order by tnim3_appointment.AppointmentTime"; */
				mySQL_worklist = "select DISTINCT encounterid , appointmenttime from tnim3_encounter inner join tnim3_referral on tnim3_referral.referralid = tnim3_encounter.referralid inner join tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid inner join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid where encounterstatusid ='1' and reportfileid=0 and tnim3_encounter.appointmentid >0 and appointmenttime <  now() -'1 day':: INTERVAL and appointmenttime >  now() -'416 day':: INTERVAL  and cast(appointmenttime as VARCHAR) not like '1800%' and istatus != 4" + myAT_Where + " ORDER BY appointmenttime DESC";
		
			}
			//Escalated Cases
			if (chosenWL == 16){
				mySQL_worklist = "SELECT DISTINCT en.encounterid\n" +
						"FROM tnim3_encounter en\n" +
						"INNER JOIN tNIM3_CaseAccount ON tNIM3_CaseAccount.caseid = en.referralid\n" +
						"WHERE encounterstatusid ='12'\n" +
						myAT_Where +
						" ORDER BY en.encounterid";
			}
			//See Netdev waiting
			if (chosenWL == 17){
				mySQL_worklist = "select encounterid from tnim3_encounter where seenetdev_waiting ='1' order by encounterid";
			}
			//Stale Cases
			if (chosenWL == 18){
				mySQL_worklist = "select encounterid, caseaccount.patientfirstname||' '||caseaccount.patientlastname, encounter.scanpass, referral.receivedate, now() - INTERVAL '84 hours', assignedto.contactfirstname from tnim3_referral as referral    inner join tnim3_encounter as encounter on encounter.referralid = referral.referralid  inner join tnim3_caseaccount as caseaccount on caseaccount.caseid = referral.caseid  inner join tuseraccount as assignedto on assignedto.userid = caseaccount.assignedtoid    where referral.receivedate < now() - INTERVAL '84 hours'   and encounter.encounterstatusid in (1,12,11,8)   and cast(referral.receivedate as varchar) != '1800-01-01 00:00:00'   and encounter.appointmentid = 0   and lower(caseaccount.patientlastname) not similar to '%(test|bauer)%'  and lower(caseaccount.patientfirstname) not similar to '%(test|bauer)%'";
			}
			if (chosenWL == 19){
				/* mySQL_worklist = "select tNIM3_encounter.encounterid, tnim3_encounter.scanpass, tnim3_referral.receivedate,tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname,patient_state_link.shortstate as patient_state, patient_state_link.stateid as stateid,tnim3_caseaccount.caseid, ua_assignedto.contactfirstname as assignedto_firstname, tnim3_encounter.isstat, tnim3_encounter.seenetdev_waiting, tNIM3_Referral.receivedate, tNIM3_Encounter.nextactiondate, tnim3_encounter.NextActionNotes,tNIM3_PayerMaster.PayerName from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerID INNER JOIN tuseraccount as ua_assignedto on ua_assignedto.userid = tnim3_caseaccount.assignedtoid INNER JOIN tstateli as patient_state_link on patient_state_link.stateid = tnim3_caseaccount.patientstateid INNER JOIN tNIM3_Appointment on tnim3_appointment.appointmentid = tnim3_encounter.appointmentid INNER JOIN tnim3_service on tnim3_service.encounterid = tnim3_encounter.encounterid " + 
		"where tNIM3_Encounter.AppointmentID>0  AND  tNIM3_Encounter.ReportFileID=0 AND encounterstatusid=1 AND servicestatusid=1 AND now() > appointmenttime" +
		myAT_Where + " AND tNIM3_Encounter.encounterstatusid in (" + NIMUtils.getSQL_Encounters_Active(false, true) + ")  order by tnim3_appointment.AppointmentTime"; */
				mySQL_worklist = "select DISTINCT encounterid , appointmenttime from tnim3_encounter inner join tnim3_referral on tnim3_referral.referralid = tnim3_encounter.referralid inner join tnim3_caseaccount on tnim3_caseaccount.caseid = tnim3_referral.caseid inner join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid where encounterstatusid ='1' and reportfileid=0 and tnim3_encounter.appointmentid >0 and appointmenttime >=  now() " + myAT_Where + " ORDER BY appointmenttime DESC";
		
			}
			if (chosenWL == 20){
				mySQL_worklist = "SELECT\n" +
						"	en.encounterid\n" +
						"FROM\n" +
						"	tnim3_encounter en\n" +
						"INNER JOIN tnim3_service sr ON sr.encounterid = en.encounterid\n" +
						"WHERE\n" +
						"	seenetdev_selectedpracticeid != 0\n" +
						"AND en.seenetdev_reqsenttonetdev > now() - INTERVAL '120 days'\n" +
						"AND sr.paidoutamount = 0.00\n" +
						"AND en.iscourtesy = 2\n" +
						"AND sr.servicestatusid != 5\n" +
						"AND en.encounterstatusid NOT IN (5, 6)\n" +
						"ORDER BY\n" +
						"	en.seenetdev_reqsenttonetdev DESC";
			}
			if (chosenWL == 22){
				mySQL_worklist = "SELECT DISTINCT en.encounterid\n" +
						"FROM tnim3_encounter en\n" +
						"INNER JOIN tnim3_referral re ON en.referralid = re.referralid\n" +
						"INNER JOIN tnim3_caseaccount ca ON  ca.caseid = re.caseid\n" +
						"INNER JOIN tnim3_payermaster pm ON ca.payerid = pm.payerid\n" +
						"WHERE pm.payername ILIKE 'packard%'\n" +
						"AND to_char(en.uniquecreatedate, 'yyyy-mm-dd') = to_char(now(), 'yyyy-mm-dd')\n" +
						"AND en.encounterstatusid IN ('1', '4', '5', '6', '9', '11', '12')\n" +
						"ORDER BY en.encounterid";
			}
			if (chosenWL == 23){
				mySQL_worklist = "SELECT DISTINCT en.encounterid\n" +
						"FROM tnim3_encounter en\n" +
						"INNER JOIN tnim3_appointment ap ON en.encounterid = ap.initialencounterid\n" +
						"INNER JOIN tnim3_referral re ON en.referralid = re.referralid\n" +
						"INNER JOIN tnim3_caseaccount ca ON  ca.caseid = re.caseid\n" +
						"INNER JOIN tnim3_payermaster pm ON ca.payerid = pm.payerid\n" +
						"WHERE pm.payername ILIKE 'packard%'\n" +
						"AND to_char(ap.appointmenttime, 'yyyy-mm-dd') <= to_char(now(), 'yyyy-mm-dd')\n" +
						"AND to_char(en.timetrack_reqrpreview, 'yyyy-mm-dd') = to_char(now(), 'yyyy-mm-dd')\n" +
						"AND en.encounterstatusid IN ('1', '4', '5', '6', '9', '12')\n" +
						"AND en.reportfileid != 0\n" +
						"AND en.timetrack_reqrpreview_userid != 0\n" +
						"ORDER BY en.encounterid";
			}
			return mySQL_worklist;
			
            }
            
            public String wlSQL2(int chosenWL, String myAT_Where2){
                String mySQL_worklist = "";
                
              	//All cases touched or viewed
				if (chosenWL == 21){
					mySQL_worklist = "SELECT DISTINCT ON (en.encounterid) en.encounterid,\n" +
							"ct.uniquecreatedate\n" +
							"FROM tnim3_encounter en\n" +
							"INNER JOIN tnim3_commtrack ct ON ct.encounterid = en.encounterid\n" +
							"INNER JOIN tnim3_caseaccount ca ON ca.caseid = ct.caseid\n" +
							"INNER JOIN tuseraccount ua ON ua.userid = ca.assignedtoid\n" +
							"WHERE to_char(ct.uniquecreatedate, 'yyyy-mm-dd') = to_char(now(), 'yyyy-mm-dd')\n" +
							myAT_Where2 +
							" ORDER BY en.encounterid, ct.uniquecreatedate DESC";
				}
				
				return mySQL_worklist;
            }
			
			public String wlTitle(int chosenWL){
            String wlTitle = "* Error *";
			
			if (chosenWL == 1){
            	wlTitle = "Receipt Confirmations";
			}
			
			if (chosenWL == 2){
				wlTitle = "Needs PreScreen";
            }
			
			if (chosenWL == 3){
				wlTitle = "Missing Rx";
			}
			
			if (chosenWL == 4){
				wlTitle = "Needs Rx Review";
			}
			
			if (chosenWL == 5){
				wlTitle = "Ready To Schedule";
			}
			
			if (chosenWL == 6){
				wlTitle = "Needs Report Review";
			}
			
			if (chosenWL == 7){
				wlTitle = "Still Active: Why?";
			}
			
			if (chosenWL == 8){
				wlTitle = "Hold Bill";
			}
			
			if (chosenWL == 9){
				wlTitle = "Next Action Date Due";
			}
			
			if (chosenWL == 10){
				wlTitle = "Unassigned Cases";
			}
			
			if (chosenWL == 11){
				wlTitle = "Stat Cases";
			}
			
			if (chosenWL == 12){
				wlTitle = "Open CPT 76140";
			}
			
			if (chosenWL == 13){
				wlTitle = "Referral Status Pending or N/A";
			}
			if (chosenWL == 14){
				wlTitle = "Open CommTracks";
			}
			if (chosenWL == 15){
				wlTitle = "Missing Report";
			}
			if (chosenWL == 16){
				wlTitle = "Escalated Cases";
			}
			if (chosenWL == 17){
				wlTitle = "In NetDev";
			}
			if (chosenWL == 18){
				wlTitle = "80+ hrs N/S";
			}
			if (chosenWL == 19){
				wlTitle = "Appointment Pending";
			}
			if (chosenWL == 20){
				wlTitle = "OTA NetDev Pay-out";
			}
			if (chosenWL == 21){
				wlTitle = "Viewed Cases";
			}
			if (chosenWL == 22){
				wlTitle = "Receipt Confirmation (Packard Claims)";
			}
			if (chosenWL == 23){
				wlTitle = "Report Review (Packard Claims)";
			}
			return wlTitle;
			
            }
            
            public String getTimeZone (int stateShort){
				String timeZone;
						if (stateShort > 0 && stateShort < 7 && stateShort != 4 ){
                        	timeZone="PST";
						}
						else if (stateShort == 4 || stateShort > 6 && stateShort < 15 && stateShort != 14 && stateShort != 12){
							timeZone="MST";
						}
						else if (stateShort == 12 || stateShort == 14 || stateShort > 14 && stateShort < 30 && stateShort != 28 ) {
							timeZone="CST";
						}
						else if (stateShort == 28 || stateShort == 51 || stateShort > 30 && stateShort < 49 ) {
							timeZone="EST";
						}
						else if ( stateShort == 49 || stateShort == 50) {
							timeZone="HST";
						}
						else {
							timeZone="--";
						}
						return timeZone;
			}
					
            
            %>
			<!-- <Script type="text/javascript">
			$(function(){
					$.tablesorter.addParser({ 
					    // set a unique id 
					    id: 'data', 
					    is: function(s) { 
					      // return false so this parser is not auto detected 
					      return false; 
					    }, 
					    format: function(s, table, cell, cellIndex) { 
					    
					        return $(cell).attr('data-hour'); 
					      
					    }, 
					    // set type, either numeric or text 
					    type: 'numeric' 
					  }); 
					 
					$('.sortable').tablesorter({ 
					    headers : { 
					      12 : { sorter: 'data' } 
					    }, 
					  }); 
				}); 
			</script> -->
			<Script type="text/javascript">
			$(document).ready(function(){
				$('.sortable').tablesorter();
			});
			
			</script>
			<table width="100%" border="0" cellspacing="0" cellpadding="10">
			  <tr>
				<td>
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td><p class="title">Worklists 2.0</p></td>
					</tr>
					<tr>
						<td>
							<table width="100%" border="2" cellspacing="0" cellpadding="10">
							<%
		 if (isScheduler2)
		 {
			 %>
          <tr class="tdHeaderAlt">
            <td valign="top"><form action="#" method="POST" name="SchedulerSelect" class="tdHeader">
              Viewing the worklist for:
                   <select name="theScheduler1" class="titleSub2">
                <option value="0">Show All</option>
                <%
{
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("SELECT\n" +
					"	ua.Userid,\n" +
					"	ua.LogonUserName,\n" +
					"	ua.contactfirstname,\n" +
					"	ua.contactlastname\n" +
					"FROM\n" +
					"	tUserAccount ua\n" +
					"WHERE\n" +
					"	(\n" +
					"		accounttype = 'SchedulerID'\n" +
					"		OR accounttype = 'SchedulerID2'\n" +
					"		OR accounttype = 'SchedulerID3'\n" +
					"		OR accounttype = 'SchedulerID4'\n" +
					"		OR accounttype = 'GenAdminID'\n" +
					"	)\n" +
					"AND ua.status = 2\n" +
					"ORDER BY\n" +
					"	LogonUserName");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{

			while (myRS!=null&&myRS.next())
			{
				if (myRS.getString("UserID").equalsIgnoreCase(theScheduler1)){%>
              		<option selected value="<%=myRS.getString("UserID")%>"><%=myRS.getString("contactfirstname")%> <%=myRS.getString("contactlastname")%>&nbsp;
	                <%if (myRS.getString("LogonUserName").length()==2 || myRS.getString("LogonUserName").length()==3 || myRS.getString("LogonUserName").length()==4){
	                	out.print("["+myRS.getString("LogonUserName")+"]");
	                }
	                else if (myRS.getString("LogonUserName").length()>4){
	                	out.print("["+myRS.getString("LogonUserName").substring(0,5)+"..]");
	                }
	                else{
							out.print("["+myRS.getString("LogonUserName")+"]");
					}%>
	                </option>	               
				<%}
				else {%>
	                 <option value="<%=myRS.getString("UserID")%>"><%=myRS.getString("contactfirstname")%> <%=myRS.getString("contactlastname")%>&nbsp;
	                <%if (myRS.getString("LogonUserName").length()==2 || myRS.getString("LogonUserName").length()==3 || myRS.getString("LogonUserName").length()==4){
	                	out.print("["+myRS.getString("LogonUserName")+"]");
	                }
	                else if (myRS.getString("LogonUserName").length()>4){
	                	out.print("["+myRS.getString("LogonUserName").substring(0,5)+"..]");
					}
					else{
						out.print("["+myRS.getString("LogonUserName")+"]");
					}%>
	                </option>
                <%}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
%>
              </select>
            
            &amp;
            <select name="theScheduler2" class="titleSub2">
               <option value="-98">Not used</option>
                <%
{
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("SELECT\n" +
					"	ua.Userid,\n" +
					"	ua.LogonUserName,\n" +
					"	ua.contactfirstname,\n" +
					"	ua.contactlastname\n" +
					"FROM\n" +
					"	tUserAccount ua\n" +
					"WHERE\n" +
					"	(\n" +
					"		accounttype = 'SchedulerID'\n" +
					"		OR accounttype = 'SchedulerID2'\n" +
					"		OR accounttype = 'SchedulerID3'\n" +
					"		OR accounttype = 'SchedulerID4'\n" +
					"	)\n" +
					"AND ua.status = 2\n" +
					"ORDER BY\n" +
					"	LogonUserName");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
			{
			while (myRS!=null&&myRS.next())
			{
				if (myRS.getString("UserID").equalsIgnoreCase(theScheduler2)){%>
              		<option selected value="<%=myRS.getString("UserID")%>"><%=myRS.getString("contactfirstname")%> <%=myRS.getString("contactlastname")%>&nbsp;
					                <%if (myRS.getString("LogonUserName").length()==2 || myRS.getString("LogonUserName").length()==3 || myRS.getString("LogonUserName").length()==4){
	                	out.print("["+myRS.getString("LogonUserName")+"]");
	                }
	                else if (myRS.getString("LogonUserName").length()>4){
	                	out.print("["+myRS.getString("LogonUserName").substring(0,5)+"..]");
	                }
	                else{
							out.print("["+myRS.getString("LogonUserName")+"]");
					}%>
	                </option>	               
				<%}
				else {%>
	                 <option value="<%=myRS.getString("UserID")%>"><%=myRS.getString("contactfirstname")%> <%=myRS.getString("contactlastname")%>&nbsp;
	                <%if (myRS.getString("LogonUserName").length()==2 || myRS.getString("LogonUserName").length()==3 || myRS.getString("LogonUserName").length()==4){
	                	out.print("["+myRS.getString("LogonUserName")+"]");
	                }
	                else if (myRS.getString("LogonUserName").length()>4){
	                	out.print("["+myRS.getString("LogonUserName").substring(0,5)+"..]");
					}
					else{
						out.print("["+myRS.getString("LogonUserName")+"]");
					}%>
	                </option>
                <%}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
%>
              </select>
              
              &amp;
              
              <select name="theScheduler3" class="titleSub2">
                <option value="-98">Not used</option>
                <%
{
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("SELECT\n" +
					"	ua.Userid,\n" +
					"	ua.LogonUserName,\n" +
					"	ua.contactfirstname,\n" +
					"	ua.contactlastname\n" +
					"FROM\n" +
					"	tUserAccount ua\n" +
					"WHERE\n" +
					"	(\n" +
					"		accounttype = 'SchedulerID'\n" +
					"		OR accounttype = 'SchedulerID2'\n" +
					"		OR accounttype = 'SchedulerID3'\n" +
					"		OR accounttype = 'SchedulerID4'\n" +
					"	)\n" +
					"AND ua.status = 2\n" +
					"ORDER BY\n" +
					"	LogonUserName");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
				if (myRS.getString("UserID").equalsIgnoreCase(theScheduler3)){%>
              		<option selected value="<%=myRS.getString("UserID")%>"><%=myRS.getString("contactfirstname")%> <%=myRS.getString("contactlastname")%>&nbsp;
	                <%if (myRS.getString("LogonUserName").length()==2 || myRS.getString("LogonUserName").length()==3 || myRS.getString("LogonUserName").length()==4){
	                	out.print("["+myRS.getString("LogonUserName")+"]");
	                }
	                else if (myRS.getString("LogonUserName").length()>4){
	                	out.print("["+myRS.getString("LogonUserName").substring(0,5)+"..]");
	                }
	                else{
							out.print("["+myRS.getString("LogonUserName")+"]");
					}%>
	                </option>	               
				<%}
				else {%>
	                 <option value="<%=myRS.getString("UserID")%>"><%=myRS.getString("contactfirstname")%> <%=myRS.getString("contactlastname")%>&nbsp;
	                <%if (myRS.getString("LogonUserName").length()==2 || myRS.getString("LogonUserName").length()==3 || myRS.getString("LogonUserName").length()==4){
	                	out.print("["+myRS.getString("LogonUserName")+"]");
	                }
	                else if (myRS.getString("LogonUserName").length()>4){
	                	out.print("["+myRS.getString("LogonUserName").substring(0,5)+"..]");
					}
					else{
						out.print("["+myRS.getString("LogonUserName")+"]");
					}%>
	                </option>
                <%}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
%>
              </select>
									
									
									<hr>
									<table border="1" cellpadding="10" cellspacing="0">
										<tr>
											<td class="tdHeaderAltDark">Data &amp; Scheduling</td>
											<td align="left" class="tdHeaderAltDark">Post Schedule</td>
											<td align="left" class="tdHeaderAltDark">Other</td>
											<td rowspan="2" align="center"><input name="Update" type="submit" class="inputButton_lg_Create" value="Update"></td>
											
										</tr>
										<tr class="tdBaseAlt_Default">
											<td align="left" valign="top">
												<input name="showWL_receiptsent" type="checkbox" <%if (chosenWL.get("showWL_receiptsent")){%> checked <%}%> value="1">&nbsp;Receipt Confirmations<br/>
												<input name="showWL_prescreen" type="checkbox" <%if (chosenWL.get("showWL_prescreen")){%> checked <%}%>  value="1">&nbsp;Needs Pre-screen<br/>
												<input name="showWL_missingrx" type="checkbox" <%if (chosenWL.get("showWL_missingrx")){%> checked <%}%>  value="1">&nbsp;Missing Rx<br/>
												<input name="showWL_rxreview" type="checkbox" <%if (chosenWL.get("showWL_rxreview")){%> checked <%}%>  value="1">&nbsp;Needs Rx Review<br/>
												<input name="showWL_readytoschedule" type="checkbox" <%if (chosenWL.get("showWL_readytoschedule")){%> checked <%}%>  value="1">&nbsp;Ready to Schedule<br/>    
												<input name="showWL_escalatedcases" type="checkbox" <%if (chosenWL.get("showWL_escalatedcases")){%> checked <%}%>  value="1">&nbsp;Escalated Cases<br/>                
												<input name="showWL_innetdev" type="checkbox" <%if (chosenWL.get("showWL_innetdev")){%> checked <%}%>  value="1">&nbsp;In NetDev<br/>
												<input name="showWL_ftprc_pc" type="checkbox" <%if (chosenWL.get("showWL_ftprc_pc")){%> checked <%}%>  value="1">&nbsp;Receipt Confirmations (Packard Claims Only)<br/> 
											</td>
											<td align="left" valign="top">
												<input name="showWL_missingreport" type="checkbox" <%if (chosenWL.get("showWL_missingreport")){%> checked <%}%>  value="1">&nbsp;Missing Report<br/>
												<input name="showWL_appointmentpending" type="checkbox" <%if (chosenWL.get("showWL_appointmentpending")){%> checked <%}%>  value="1">&nbsp;Appointment Pending<br/>
												<input name="showWL_needsrpreview" type="checkbox" <%if (chosenWL.get("showWL_needsrpreview")){%> checked <%}%>  value="1">&nbsp;Report Review <br/>
												
												<input name="showWL_stillactive" type="checkbox" <%if (chosenWL.get("showWL_stillactive")){%> checked <%}%>  value="1">&nbsp;Still Active: Why?<br/>
												<input name="showWL_holdbill" type="checkbox" <%if (chosenWL.get("showWL_holdbill")){%> checked <%}%>  value="1">&nbsp;Hold Bill<br/>
												<input name="showWL_otanetdev" type="checkbox" <%if (chosenWL.get("showWL_otanetdev")){%> checked <%}%>  value="1">&nbsp;OTA NetDev Pay-out<br/>
												<input name="showWL_ftprp_pc" type="checkbox" <%if (chosenWL.get("showWL_ftprp_pc")){%> checked <%}%>  value="1">&nbsp;Report Review (Packard Claims Only)<br/>
											</td>
											<td align="left" valign="top"><input name="showWL_nad" type="checkbox" <%if (chosenWL.get("showWL_nad")){%> checked <%}%>  value="1">&nbsp;Next Action Due<br/>
												<input name="showWL_ct" type="checkbox" <%if (chosenWL.get("showWL_ct")){%> checked <%}%>  value="1">&nbsp;Open CommTracks<br/>
												<input name="showWL_unassigned" type="checkbox" <%if (chosenWL.get("showWL_unassigned")){%> checked <%}%>  value="1">&nbsp;Unassigned Cases<br>    
												<input name="showWL_stat" type="checkbox" <%if (chosenWL.get("showWL_stat")){%> checked <%}%>  value="1">&nbsp;STAT Cases<br>
												<input name="showWL_76140" type="checkbox" <%if (chosenWL.get("showWL_76140")){%> checked <%}%>  value="1">&nbsp;CPT 76140<br>
												<input name="showWL_pending" type="checkbox" <%if (chosenWL.get("showWL_pending")){%> checked <%}%>  value="1">&nbsp;Pending or N/A<br>
                                                <input name="showWL_stalecases" type="checkbox" <%if (chosenWL.get("showWL_stalecases")){%> checked <%}%>  value="1">&nbsp;80+ hrs N/S<br>                                          
                                                <input name="showWL_allcasestouched" type="checkbox" <%if (chosenWL.get("showWL_allcasestouched")){%> checked <%}%>  value="1">&nbsp;Viewed Cases
											</td>
											
										</tr>
									</table>
									<hr>
								</form>
								</td>
							  </tr>
                              <tr><td style="padding:0">
                    	<form id="form1" name="form1" method="post" action="tNIM3_CaseAccount_Create1.jsp">
                          <p style="margin:0">
                            To create a new case, please enter the Claim Number: 
                            <input name="CN" type="text" class="inputButton_md_Create" id="CN" size="20" maxlength="20" />
                            <input name="button" type="submit" class="inputButton_md_Create" id="button" onClick="MM_validateForm('CN','','R');return document.MM_returnValue" value="Create" />
                            </p>
                      </form>
                   </td> </tr>
							<% }%> 
                            
							</table>
						</td>
					</tr>
                    
                    <!-- start generate data-->
                 	
                    <%
					
					for (int x = 0; x < wlCount; x++){
						if (wlArray[x] != 14){
							myRS_worklist = mySS_worklist.executeStatement(wlSQL(wlArray[x],myAT_Where));
							//out.println(wlSQL(wlArray[x],myAT_Where));
							
							if(wlArray[x] == 21){
								myRS_worklist = mySS_worklist.executeStatement(wlSQL2(wlArray[x],myAT_Where2));
								//out.print(wlSQL2(wlArray[x],myAT_Where2));
							}
						%>
						<tr class="tdHeaderAlt">
							<td colspan="10" class="tdHeaderAltDark"><%=wlTitle(wlArray[x])%> - <span id="wL<%=wlArray[x]%>"></span></td>
							<td align="right" class="tdHeaderAltDark">[Hide<input type="checkbox" id="hide-<%=wlArray[x]%>" disabled>] [Show Courtesy<input type="checkbox" id="courtesy-<%=wlArray[x]%>" checked disabled>]</td>
						</tr>
						<tr id="row-<%=wlArray[x]%>">
							<td colspan="11">
							<table width="100%" border="1" cellspacing="0" cellpadding="1" class="sortable">
								<thead>
								<tr class="tdHeaderAlt">
								<th align="center">#</td>
                                <th></th>
								<th></th>
								<th></th>
								<th></th>
								<th>Payer</th>
								<th>Adjuster</th>
								<th>Scheduler</th>
								<th>Patient</th>
								
								
								
								<%if (wlArray[x] == 15){%>
								<th>Image Center</th>
								<th>Study</th>
								<%}else if(wlArray[x] == 19){%>
								<th>Image Center</th>
								<th>Study</th>
								<%} %>
								<%if (wlArray[x] == 16){%>
								<th>Status</th>
								<%}%>
								<th>State</th>
								<th>TZ</th>
								<th>ScanPass</th>
								<%if (wlArray[x] == 15){%>
								<th>Hours Since DOS</th>
								<%}
								else if (wlArray[x] == 6){%>
								<th>Hours Since Upload</th>
								<%}
								else if (wlArray[x] == 17 || wlArray[x] == 20){%>
								<th>Hours Since Sent to NetDev</th>
								<%}
								else if(wlArray[x] == 19){%>
								<th>Hours Since DOS</th>
								<%}
								else{%>
								<th>Hours Since Rec</th>
								<%} %>
								</tr>
								</thead>
								<tbody id="wL<%=wlArray[x]%>-body">
								<%
								count = 0;
								while (myRS_worklist!=null&&myRS_worklist.next())
								{
								NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(myRS_worklist.getInt("encounterid"),"load");
								bltStateLI mySt = new bltStateLI(myEO2.getNIM3_CaseAccount().getPatientStateID());
								
								java.util.Date dNextActionDate = myEO2.getNIM3_Encounter().getNextActionDate();
								int iHoursPassed=0;
								count++;
								java.util.Date dReceiveDate = null;
								if (wlArray[x] == 15 || wlArray[x] == 19){
									dReceiveDate = myEO2.getNIM3_Appointment().getAppointmentTime();
									iHoursPassed = (int)Math.round(((PLCUtils.getNowDate(false).getTime()-dReceiveDate.getTime())*.00000027777777777777776) * 1) / 1;
									if (iHoursPassed<0){
										iHoursPassed = 0;
									}
								}
								
								else if (wlArray[x] == 6){
									bltNIM3_Document mydoc = new bltNIM3_Document(myEO2.getNIM3_Encounter().getReportFileID());
									dReceiveDate = mydoc.getUniqueCreateDate();
									iHoursPassed = (int)Math.round(((PLCUtils.getNowDate(false).getTime()-dReceiveDate.getTime())*.00000027777777777777776) * 1) / 1;
									if (iHoursPassed<0){
										iHoursPassed = 0;
									}
								}
								else if (wlArray[x] == 17 || wlArray[x] == 20){
									dReceiveDate = myEO2.getNIM3_Encounter().getSeeNetDev_ReqSentToNetDev();
									iHoursPassed = (int)Math.round(((PLCUtils.getNowDate(false).getTime()-dReceiveDate.getTime())*.00000027777777777777776) * 1) / 1;
									if (iHoursPassed<0){
										iHoursPassed = 0;
									}
								}
								else{
									dReceiveDate = myEO2.getNIM3_Referral().getReceiveDate();
									iHoursPassed = (int)Math.round(((PLCUtils.getNowDate(false).getTime()-dReceiveDate.getTime())*.00000027777777777777776) * 1) / 1;
								}
								
								String theClass2 = "tdBase";
								if (iHoursPassed<2)
								{
								theClass2 = "tdBaseFade1";
								}
								else if (iHoursPassed<4)
								{
								theClass2 = "tdBase";
								}
								else if (iHoursPassed<24)
								{
								theClass2 = "tdBaseAlt_Action1";
								}
								else if (iHoursPassed<48)
								{
								theClass2 = "tdBaseAlt_Action2";
								}
								else
								{
								theClass2 = "tdBaseAlt_Action3";
								}
								String NADClass = "tdBase";
								String NADImg = "images/action.gif";
								if (dNextActionDate.before(PLCUtils.getToday()))
								{
								//NADClass = "tdBaseAlt_Action3";
								NADImg = "images/no-action.gif";
								}
								String NADNotes = "Due: " + PLCUtils.getDisplayDateWithTime(dNextActionDate);
								if (dNextActionDate.before(NIMUtils.getBeforeTime()))
								{
								NADNotes = "No Action Date Set";
								NADImg = "images/noAction.jpg";
								}
								else
								{
								NADNotes += "\\nAction:\\n" + DataControlUtils.Text2OneLine(myEO2.getNIM3_Encounter().getNextActionNotes());
								}

								%>
								<tr <%if (myEO2.getNIM3_Encounter().getisCourtesy()==1){out.print("class=\"courtesy-case-"+wlArray[x]+"\"");}%> <%if (myEO2.getNIM3_Encounter().getEncounterStatusID()==12&&wlArray[x] != 16){%>style="background:#FFB13E"<%}%>>
                                	<td align="center" class="numbering-<%=wlArray[x]%>"><%=count%></td>
									<td align="center" class="<%=NADClass%>"><img src="<%=NADImg%>" onClick="alert('<%=NADNotes%>');"></td>
									<td align="center"><% if (myEO2.getNIM3_Encounter().getisSTAT()==1){out.println("<span class=\"tdBaseAltRed\">STAT</span>");}else{}%><%if (myEO2.getNIM3_Encounter().getisSTAT()==1&&myEO2.getNIM3_Encounter().getisCourtesy()==1){out.println("<br>");}%><%if (myEO2.getNIM3_Encounter().getisCourtesy()==1){out.println("<span style=\"background:#f2abf3\">CTSY</span>");}%></td>
									<td align="center"><% if (myEO2.getNIM3_Encounter().getSeeNetDev_Waiting()==1){out.println("<span class=\"tdBaseAltRed\">NetDev</span>");}else{%>&nbsp;<%}%></td>
									<td align="center" width="25"><img value="<%=myEO2.getNIM3_Encounter().getEncounterID()%>" src="images/zoom_plus.png" width="25" height="25" border="0" 
											<%if (wlArray[x] == 15){%>
												onclick="commTrackForm('<%=myEO2.getNIM3_CaseAccount().getPatientLastName()%>, <%=myEO2.getNIM3_CaseAccount().getPatientFirstName()%>',this.getAttribute('value'))"<%}
											else if(wlArray[x] == 19){ %>
												onclick="commTrackForm('<%=myEO2.getNIM3_CaseAccount().getPatientLastName()%>, <%=myEO2.getNIM3_CaseAccount().getPatientFirstName()%>',this.getAttribute('value'))"
									<%} %>>		
									</td>
									
									
									
									<td><%=myEO2.getPayerFullName()%>
                                    <%if (wlArray[x] == 15){%>
									<%-- rec date --%>
										<br><span style="background:#00a2ff">Rec'd: <%=displayDateSDF1.format(myEO2.getNIM3_Referral().getReceiveDate())%></span>
									<%}else if(wlArray[x] == 19){%>
										<br><span style="background:#00a2ff">Rec'd: <%=displayDateSDF1.format(myEO2.getNIM3_Referral().getReceiveDate())%></span>
									<%}
									%>
                                    </td>
                                    
                                    
									<td>
									<%if (myEO2.getCase_Adjuster().getContactFirstName().equals("TBD")) { %>
									TBD
									<%} else if  (myEO2.getCase_Adjuster().getContactFirstName().equalsIgnoreCase("EMPTY")){%>
									Empty
									<%} else{%>
									<%=myEO2.getCase_Adjuster().getContactFirstName()%> <%=myEO2.getCase_Adjuster().getContactLastName()%>
									<%}%>
									</td>
									<td><%=myEO2.getCase_AssignedTo().getContactFirstName()%></td>
									
									
									<td nowrap><%-- iw name --%><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&SelectedEncounterID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p"><%=myEO2.getNIM3_CaseAccount().getPatientLastName()%>, <%=myEO2.getNIM3_CaseAccount().getPatientFirstName()%></a>
									<%if (wlArray[x] == 15){%>
									<%-- iw dob --%>
									<br>DOB: <%=displayDateSDF1.format(myEO2.getNIM3_CaseAccount().getPatientDOB())%>
                                    
									<%}else if(wlArray[x] == 19){ %>
									<br>DOB: <%=displayDateSDF1.format(myEO2.getNIM3_CaseAccount().getPatientDOB())%>
									<%} %>
									</td>
									
									
									
									<%if (wlArray[x] == 15){%>
									<td><%-- ic --%><strong><%=myEO2.getAppointment_PracticeMaster().getPracticeName() %></strong><br> [<span style="background:#f2abf3"><%=myEO2.getAppointment_PracticeMaster().getOfficePhone() %></span>]<br>
                                    <span style="background:#06ff00">DOS: <%=displayDateSDF1.format(myEO2.getNIM3_Encounter().getDateOfService())%></span><br>
                                    Auto Fax sent: <%=myEO2.getNIM3_Encounter().getReportFollowUpCounter()%>
                                    </td>
									<td><%-- study --%>
									<%bltNIM3_Service  service;
									ListElement         current_element;
									java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
									int cnt55=1;
									while (eList_Service.hasMoreElements()){
										current_element = (ListElement) eList_Service.nextElement();
										service  = (bltNIM3_Service) current_element.getObject();%>
										<%if (service.getServiceStatusID().intValue()!=5){out.print(cnt55+") "+service.getCPTBodyPart()+"<br>");cnt55++;}%>	
									<%}%></td>
									<%}
									
									
									
									
									else if(wlArray[x] == 19){%>
									<td>	<%-- ic --%><strong><%=myEO2.getAppointment_PracticeMaster().getPracticeName() %></strong><br> [<span style="background:#f2abf3"><%=myEO2.getAppointment_PracticeMaster().getOfficePhone() %></span>]<br>
                                    <span style="background:#06ff00">DOS: <%=displayDateSDF1.format(myEO2.getNIM3_Encounter().getDateOfService())%></span><br>
                                    Auto Fax sent: <%=myEO2.getNIM3_Encounter().getReportFollowUpCounter()%>
                                    </td>
									<td><%-- study --%>
									<%bltNIM3_Service  service;
									ListElement         current_element;
									java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
									int cnt55=1;
									while (eList_Service.hasMoreElements()){
										current_element = (ListElement) eList_Service.nextElement();
										service  = (bltNIM3_Service) current_element.getObject();%>
										<%if (service.getServiceStatusID().intValue()!=5){out.print(cnt55+") "+service.getCPTBodyPart()+"<br>");cnt55++;}%>	
									<%}%>									
									<%} %>
									</td>
									
									
									<%if (wlArray[x] == 16){%>
									<td><%=myEO2.getNIM3_Encounter().getProblemDescription()%></td>
									<%}%>
									<td><%=mySt.getShortState()%></td>
									<td><%=getTimeZone(myEO2.getNIM3_CaseAccount().getPatientStateID())%></td>
									<td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO2.getNIM3_CaseAccount().getCaseID()%>&SelectedEncounterID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p"><%=myEO2.getNIM3_Encounter().getScanPass()%></a></td>
									<td class=<%=theClass2%> data-hour="<%=iHoursPassed%>"><% if (iHoursPassed>10000){out.print("Invalid Receive Date");}else{out.print(iHoursPassed);}%></td> 
								</tr>
								<% }
								mySS_worklist.closeAll();
								%>
                                <script>
                                document.getElementById("wL<%=wlArray[x]%>").innerHTML=<%=count%>;
                                $("#courtesy-<%=wlArray[x]%>, #hide-<%=wlArray[x]%>").attr("disabled", false);
                                $('#courtesy-<%=wlArray[x]%>').change(function(){
                                	if (this.checked){
                                		$('.courtesy-case-<%=wlArray[x]%>').children(".no-numbering-<%=wlArray[x]%>").removeClass("no-numbering-<%=wlArray[x]%>").addClass("numbering-<%=wlArray[x]%>");
                                	}
                                	else{
                                		$('.courtesy-case-<%=wlArray[x]%>').children(".numbering-<%=wlArray[x]%>").removeClass("numbering-<%=wlArray[x]%>").addClass("no-numbering-<%=wlArray[x]%>");
                                	}
                                	$('.courtesy-case-<%=wlArray[x]%>').toggle();
                                	var count = 1;
                                	$(".numbering-<%=wlArray[x]%>").each(function(){
                                		$(this).text(count);
                                		count++;
                                	});
                                	$("#wL<%=wlArray[x]%>").text(count-1);
                                });
                                $('#hide-<%=wlArray[x]%>').change(function(){
                                	$('#row-<%=wlArray[x]%>').toggle();
                                	$("#courtesy-<%=wlArray[x]%>").attr("disabled", $('#hide-<%=wlArray[x]%>').is(':checked'));
                                });
                                
                                </script>
                                
								</tbody> 
							</table>             
							</td>
						</tr>
					<% }
						else { %>
                    	<tr>
                            <td valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                  <tr class="tdHeaderAlt">
                                  	<td colspan=2 class="tdHeaderAltDark">Open CommTracks
                                      <input name="button2" type="button" onClick="document.location='tNIM3_CommTrack_query_all.jsp';" class="inputButton_md_Action1" id="button2" value="View CommTrack" /></td>
                                  </tr>
                                  <tr class="tdHeaderAlt">
                                    <td nowrap>Name</td>
                                    <td nowrap >Hours Since Rec</td>
                                  </tr>
                                  	<%
									{				
									java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_COMMTRACK,myAT_Where); 
									for (int i=0;i<myWorklist_Vector.size();i++)
									{
									Integer iCommTrackID = (Integer) myWorklist_Vector.elementAt(i);
									bltNIM3_CommTrack myCT = new bltNIM3_CommTrack(iCommTrackID);
									int caseId = myCT.getCaseID().intValue();
									long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myCT.getCommStart().getTime())/(60*60*1000);
									String theClass2 = "tdBase";
									if (iHoursPassed<1)
									{
										theClass2 = "tdBaseFade1";
									}
									else if (iHoursPassed<3)
									{
										theClass2 = "tdBase";
									}
									else if (iHoursPassed<6)
									{
										theClass2 = "tdBaseAlt_Action1";
									}
									else if (iHoursPassed<12)
									{
										theClass2 = "tdBaseAlt_Action2";
									}
									else
									{
										theClass2 = "tdBaseAlt_Action3";
									}
									
									if (caseId !=0) { %> 
									  <tr > 
										<td><font color="red"><%=myCT.getMessageName()%> @ <%=myCT.getMessageCompany()%></font></td>
										<td  class=<%=theClass2%>><%=iHoursPassed%> hours</td>
									  </tr>				
									<% } else { %>
									  <tr > 
										<td><%=myCT.getMessageName()%> @ <%=myCT.getMessageCompany()%></td>
										<td  class=<%=theClass2%>><%=iHoursPassed%> hours</td>
									  </tr>
									<% }
									
									}
									}
                                    %>
                            </table>
                          
                            </td>
						</tr>
                    <% } 
					}%> 
					  
                    
                    <!-- end generate data-->
				</table>
				</td>
			  </tr>
			</table>
			
			<div id="faded-bg" style="background:rgba(0,0,0,.4);display:none;width:2000px;height:2000px"></div>
			<div id="commTrackForm" style="width:300px;height:300px;background:#fff;position:fixed;display:none;z-index:10"></div>
			<%}//if (isScheduler)
		}//if (accessValid)
		else
		{
			out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");//if (accessvaild)
		}
	}//if (iSecurityCheck.intValue()!=0)
	else
	{
	out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");// if (iSecurityCheck.intValue()!=0)
	}%>

</body>
</html>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>