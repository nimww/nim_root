<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
</head>
<SCRIPT language="JavaScript1.2">
function openStatus()
{
	testwindow= window.open ("Home_Status.jsp", "HomeStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(0,0);
}

function openAuto(myAuto)
{
	testwindow= window.open ("report_auto2.jsp?rid=" + myAuto + "&MREF=300", "AutoStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(100,100);
}

</SCRIPT>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?iDelay=-1&plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler) 
    {
        accessValid = true;
    }
    else if (pageControllerHash.containsKey("iAdjusterID")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID        =    (Integer)pageControllerHash.get("iAdjusterID");
		isAdjuster = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID2")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster2 = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID3")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster3 = true;
        accessValid = true;
	}
    else if (isProvider&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
	}
	
	
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	
	  if (isAdjuster||isAdjuster2||isAdjuster3||isProvider)
	  {
		  %>
<table width="960" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td>
    	<table  border="0" cellpadding="5" cellspacing="0">
        <%
		  if (isAdjuster3)
		  {
			  bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
			  %>
          <tr>
            <td align="left" class="tdHeader">You are signed in as a Admin for: <%=myPM.getPayerName()%> </td>
          </tr>
          <%
		  }			  
		  %>


          <%if (isAdjuster||isAdjuster2||isAdjuster3)
		  {
			  %>
        <%
		  }			  
		  %>
	    </table>
    </td>
  </tr>
</table>
          
          <%
	  }
	  if (isAdjuster2&&!isAdjuster3)
	  {
		  %>
<table  border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td>
    	<table width="100%" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td class=title>You are signed in as a Supervisor of the following users:</td>
          </tr>
        <tr>
          <td valign="top" class="borderHighlightBlackSolid"><table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr class="tdHeaderAlt">
              <td>Login</td>
              <td>First Name</td>
              <td>Last Name</td>
              </tr>
            
            
            <!-- test -->
            <%
searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
	String mySQL = "null";
	String mySQL2 = "null";
	if (isAdjuster2)
	{
		mySQL = "select logonusername, contactfirstname, contactlastname from tUserAccount where payerid =" + iPayerID + " and managerid = " + CurrentUserAccount.getUserID() + " order by contactlastname";
	}
//out.println(mySQL);
	myRS = mySS.executeStatement(mySQL);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {

	cnt++;
	//if (cnt>=startID&&cnt<=startID+maxResults)
if (cnt<=10)
{
	cnt2++;
	
	String myClass = "tdBase";
	if (cnt2%2==0)
	{
		myClass = "tdBaseAlt";
	}
	
	out.print("<tr class="+myClass+">");

	out.print("<td>");
	out.print(myRS.getString("LogonUserName"));
	out.print("</td>");
	out.print("<td>");
	out.print(myRS.getString("ContactFirstName"));
	out.print("</td>");
	out.print("<td>");
	out.print(myRS.getString("ContactLastName"));
	out.print("</td>");
	out.print("</tr>");
}
   }
mySS.closeAll();
endCount = cnt;

}
catch(Exception eee)
{
	out.print("item list error: " + eee);
}

%>
            
            
            
            <!-- test -->            
            
            
          </table></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
          
          <%
	  }
	  if (isAdjuster3)
	  {
		  %>
<table  border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td>
    	<table width="100%" border="0" cellpadding="5" cellspacing="0">
        <tr>
          <td class=title>The following users are associated with your location:</td>
          </tr>
        <tr>
          <td valign="top" class="borderHighlightBlackSolid"><table width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr class="tdHeaderAlt">
              <td rowspan=2 class="tdHeaderAlt">First Name</td>
              <td rowspan=2 class="tdHeaderAlt">Last Name</td>
              <td colspan=3 align="center" class="tdHeaderAlt">Referrals</td>
            </tr>
            <tr class="tdBaseAlt2">
              <td align="center"><strong>As Adjuster</strong></td>
              <td align="center"><strong>As Nurse Case Manager</strong></td>
              <td align="center"><strong>As Case Admin</strong></td>
            </tr>
            
            
            <!-- test -->
            <%
searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
	String mySQL = "null";
	String mySQL2 = "null";
	if (isAdjuster3)
	{
		mySQL = "SELECT tuseraccount.contactfirstname, tuseraccount.contactlastname, coalesce(\"vLinkedUsersCount_CaseAccount\".count_as_adjuster,0) as count0_as_adjuster, coalesce(\"vLinkedUsersCount_CaseAccount\".count_as_ncm,0) as count0_as_ncm, (coalesce(\"vLinkedUsersCount_CaseAccount\".count_as_admin1,0) + coalesce(\"vLinkedUsersCount_CaseAccount\".count_as_admin2,0) + coalesce(\"vLinkedUsersCount_CaseAccount\".count_as_admin3,0) + coalesce(\"vLinkedUsersCount_CaseAccount\".count_as_admin4,0)) as total_count0_as_admin FROM \"vLinkedUsersCount_CaseAccount\" INNER JOIN tuseraccount ON \"vLinkedUsersCount_CaseAccount\".userid = tuseraccount.userid where tUserAccount.payerid=" + iPayerID + " order by tuseraccount.contactlastname";
	}
//out.println(mySQL);
	myRS = mySS.executeStatement(mySQL);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {

	cnt++;
	//if (cnt>=startID&&cnt<=startID+maxResults)
if (cnt<=50)
{
	cnt2++;
	
	String myClass = "tdBase";
	if (cnt2%2==0)
	{
		myClass = "tdBaseAlt";
	}
	
	String scount0_as_adjuster = myRS.getString("count0_as_adjuster");
	String scount0_as_ncm =  myRS.getString("count0_as_ncm");
	String total_count0_as_admin = myRS.getString("total_count0_as_admin"); 

	String scount0_as_adjuster_class = "tdBaseFade1";
	if (!scount0_as_adjuster.equalsIgnoreCase("0"))
	{
		scount0_as_adjuster_class = "tdBase";
	}
	String scount0_as_ncm_class = "tdBaseFade1";
	if (!scount0_as_ncm.equalsIgnoreCase("0"))
	{
		scount0_as_ncm_class = "tdBase";
	}
	String total_count0_as_admin_class = "tdBaseFade1";
	if (!total_count0_as_admin.equalsIgnoreCase("0"))
	{
		total_count0_as_admin_class = "tdBase";
	}
	
	

	out.print("<tr class="+myClass+">");
	out.print("<td >" + myRS.getString("contactfirstname") + "</td>");
	out.print("<td>" + myRS.getString("contactlastname") + "</td>");
	out.print("<td align=center class="+scount0_as_adjuster_class+">" + scount0_as_adjuster + "</td>");
	out.print("<td align=center class="+scount0_as_ncm_class+">" + scount0_as_ncm + "</td>");
	out.print("<td align=center class="+total_count0_as_admin_class+">" + total_count0_as_admin + "</td>");
	out.print("</tr>");
}
   }
mySS.closeAll();
endCount = cnt;

}
catch(Exception eee)
{
	out.print("item list error: " + eee);
}

%>
            
            
            
            <!-- test -->            
            
            
          </table></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
          
          <%
	  }
	  %>

    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>