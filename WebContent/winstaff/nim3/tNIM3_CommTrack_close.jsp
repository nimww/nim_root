<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    filename: tnim2_Authorization_main_NIM_Authorization_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td><hr></td></tr>
    <tr><td width=10>&nbsp;</td>
    <td class=tdHeader>Welcome to the NextImage Medical WebPortal.</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<% if (isScheduler)
{
%>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<%
}
%>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tnim2_Authorization")%>



<%
//initial declaration of list class and parentID
    Integer        iPayerID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if (pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
    }
  //page security
  if (accessValid||isScheduler)
  {

Integer requestID = new Integer(request.getParameter("EDITID"));
bltNIM3_CommTrack myCT = new bltNIM3_CommTrack(requestID);
myCT.setUniqueModifyComments(UserLogonDescription);
myCT.setAlertStatusCode(new Integer(1)); //1=closed 0=open
myCT.commitData();
%>
<script language="javascript">
document.location="tNIM3_CommTrack_query_all.jsp";
</script>
<%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>
    <br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>