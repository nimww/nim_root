<%@page contentType="text/html" language="java" import="java.util.ArrayList,java.sql.*,com.winstaff.*,org.apache.http.*,java.io.*,java.util.List,com.winstaff.tUserAccount_Activity.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link href="https://rawgithub.com/jharding/typeahead.js-bootstrap.css/master/typeahead.js-bootstrap.css" rel="stylesheet" media="screen">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>User Account Activity</title>
</head>


<body>
	<%
	List<UserAccountActivity> tuaa = tUserAccount_Activity.getUserAccountActivityList();
	%>
	<div class="container-fluid">
	<h3>User Account Activity Table.</h3>
		<div class="row-fluid">
			<div class="">
				<table class="table table-condensed table-hover">
					<thead>
						<tr>
							<th>UserId</th>
							<th>UserId Create Date</th>
							<!-- <th>Referral ContactId</th> -->
							<th>Scanpass</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Phone</th>
							<th>Fax</th>
							<th>Email</th>
							<th> Adj Activity</th>
							<th>Activity Date</th>
							<th>NCM Activity</th>
							<th>Activity Date</th>
							<th>Adm Activity</th>
							<th>Activity Date</th>
							<!-- <th>MD Activity</th>
							<th>Activity Date</th> -->
						</tr>
					</thead>
					<tbody>
					<%
						for(int i = 0; i < tuaa.size(); i++){
					%>
						<tr>
							<td><small><%=tuaa.get(i).getUid() %></small></td>
							<td><small><%=tuaa.get(i).getUniCreateDate() %></small></td>
							<%-- <small><td><%=tuaa.get(i).getRefbyconId() %></small></td> --%>
							<td><small><%=tuaa.get(i).getSp() %></small></td>
							<td><small><%=tuaa.get(i).getFirst() %></small></td>
							<td><small><%=tuaa.get(i).getLast() %></small></td>
							<td><small><%=tuaa.get(i).getPhone() %></small></td>
							<td><small><%=tuaa.get(i).getFax() %></small></td>
							<td><small><%=tuaa.get(i).getEmail() %></small></td>
							<td><small><%=tuaa.get(i).getSentto_reqrec_adj_yesNo() %></small></td>
							<td><small><%=tuaa.get(i).getSentto_reqrec_adj() %></small></td>
							<td><small><%=tuaa.get(i).getSentto_reqrec_ncm_yesNo() %></small></td>
							<td><small><%=tuaa.get(i).getSentto_reqrec_ncm() %></small></td>
							<td><small><%=tuaa.get(i).getSentto_reqrec_adm_yesNo() %></small></td>
							<td><small><%=tuaa.get(i).getSentto_reqrec_adm() %></small></td>
							<%-- <td><small><%=tuaa.get(i).getSentto_reqrec_refdr_yesNo() %></small></td>
							<td><small><%=tuaa.get(i).getSentto_reqrec_refdr() %></small></td> --%>
						</tr>
					<%
						}
					%>					
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>