<%@page contentType="text/html" language="java" import="com.winstaff.*,java.util.Arrays"%>
<%	PayerExceptionObject payerExceptionObject = new PayerExceptionObject(); %>
<%@ include file="../generic/CheckLogin.jsp" 
%>
<!DOCTYPE html>
<html data-ng-app="">
<head>
<title>NextImage Application</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<style type="text/css">
.max-cell {
	max-width: 200px !important;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
</style>
</head>

<body data-ng-controller="nimController">
	<div class="row"><h4>NextImage Application</h4></div>
	<div class="container">
		<%if (isScheduler){ %>
		<h3>NextImage Application</h3>
		<div>
			<input type="text" id="search" placeholder="Search table">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Edit</th>
						<th>Payer Id</th>
						<th>Practice Id</th>
						<th>CPTs</th>
						<th>FS</th>
						<th>Percentage</th>
						<th>Fee Type</th>
					</tr>
				</thead>
				<tbody>
					<tr data-ng-repeat="(key, value) in payerExceptionList">
						<td>
							<button class="btn" data-ng-click="editPayerException(key)" data-ng-disabled="canEdit">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Edit
							</button>
						</td>
						<td>{{ value.payerId }}</td>
						<td>{{ value.practiceId }}</td>
						<td class="max-cell">{{ value.cpts }}</td>
						<td>{{ value.fs }}</td>
						<td>{{ value.percentage }}</td>
						<td>{{ value.feeType }}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div>
			<button class="btn btn-success" data-ng-click="createPayerException()" data-ng-disabled="canEdit">
				<span class="glyphicon glyphicon-user"></span> Create New Exception
			</button>
			<hr>

			<h3 data-ng-show="edit">Create New Exception:</h3>
			<h3 data-ng-hide="edit">Edit Exception:</h3>

			<form class="form-horizontal" name="editForm">
				<div class="form-group">
					<div class="col-sm-6">
						<label class="col-md-6 col-lg-2 ccontrol-label">PayerId</label>
						<div class="col-md-6 col-lg-4">
							<input type="text" name="payerId" data-ng-model="payerId" placeholder="PayerId" required>
						</div>
						<label class="col-md-6 col-lg-2 ccontrol-label">PracticeID</label>
						<div class="col-md-6 col-lg-4">
							<input type="text" name="practiceId" data-ng-model="practiceId" placeholder="PracticeId" required>
						</div>
					</div>
					<div class="col-sm-6">
						<label class="col-md-6 col-lg-2 ccontrol-label">FS</label>
						<div class="col-md-6 col-lg-4">
							<input type="text" name="fs" data-ng-model="fs" placeholder="FS" required>
						</div>
						<label class="col-md-6 col-lg-2 ccontrol-label">Percentage</label>
						<div class="col-md-6 col-lg-4">
							<input type="text" name="percentage" data-ng-model="percentage" placeholder="Percentage" required>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label">CPTs</label>
					<div class="col-sm-8">
						<textarea name="cpts" data-ng-model="cpts" placeholder="CPTs" class="form-control" placeholder="CPTs seperated by commas" required></textarea>
					</div>
					<label class="col-sm-1 control-label">Fee Type</label>
					<div class="col-sm-2">
						<select class="form-control" name="feeType" data-ng-model="feeType">
							<option data-ng-repeat="feeType in feeTypes" value="{{feeType}}">{{feeType}}</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label">Insert Modals</label>
					<div class="col-sm-8">
						<button class="btn btn-danger" data-ng-click="insertModalObject('clear')" data-ng-disabled="canEdit">Clear</button>
						<button class="btn btn-primary" data-ng-click="insertModalObject('mr_wo')" data-ng-disabled="canEdit">mr_wo</button>
						<button class="btn btn-primary" data-ng-click="insertModalObject('mr_w')" data-ng-disabled="canEdit">mr_w</button>
						<button class="btn btn-primary" data-ng-click="insertModalObject('mr_wwo')" data-ng-disabled="canEdit">mr_wwo</button>
						<button class="btn btn-warning" data-ng-click="insertModalObject('ct_wo')" data-ng-disabled="canEdit">ct_wo</button>
						<button class="btn btn-warning" data-ng-click="insertModalObject('ct_w')" data-ng-disabled="canEdit">ct_w</button>
						<button class="btn btn-warning" data-ng-click="insertModalObject('ct_wwo')" data-ng-disabled="canEdit">ct_wwo</button>
					</div>

				</div>
			</form>
			<hr>
			<button class="btn btn-success" data-ng-disabled="editForm.$dirty && editForm.$invalid" data-ng-click="savePayerException()" data-ng-disabled="canEdit">
				<span class="glyphicon glyphicon-save"></span> Save Changes
			</button>
			<hr>
		</div>
		
		<%} else{%>
			<h3>Are you forgetting something?</h3>
		<% }%>
	</div>
	<script>
	
		function nimController($scope,$http) {
			$scope.payerId = '';
			$scope.practiceId = '';
			$scope.fs = '';
			$scope.percentage = '';
			$scope.cpts = '';
			$scope.feeType = 'direct';
			$scope.editId = '';
			$scope.canEdit = <%=PoUtils.arrayContainsForInteter(iCurrentUserID, Arrays.asList(28050,28049,23610)) ? false:true %>;
			
			$http.get('https://staging.nextimagedirect.com/winstaff/nim3/PayerExceptionRest.jsp?exceptionList').success(function(data) {
				$scope.payerExceptionList = data;
            });
			$http.get('https://staging.nextimagedirect.com/winstaff/nim3/PayerExceptionRest.jsp?feeTypes').success(function(data) {
				$scope.feeTypes = data;
            });
			$http.get('https://staging.nextimagedirect.com/winstaff/nim3/PayerExceptionRest.jsp?modalObject').success(function(data) {
				$scope.modalObject = data;
            });
			
			$scope.edit = true;
			$scope.error = false;
			$scope.incomplete = false;
			
			$scope.createPayerException = function() {
				if(!$scope.canEdit){
					$scope.payerId = '';
					$scope.practiceId = '';
					$scope.fs = '';
					$scope.percentage = '';
					$scope.cpts = '';
					$scope.feeType = 'direct';
					$scope.editId = '';
					$scope.edit = true;
				}
			};

			$scope.savePayerException = function() {
				if(!$scope.canEdit){
					var dataString = JSON.stringify($('form').serializeObject());
					$http.get('https://staging.nextimagedirect.com/winstaff/nim3/PayerExceptionRest.jsp?'+($scope.edit ? 'submitNewObject':'submitEditObject')+'='+dataString).success(function(data) {
						//location.href = "https://staging.nextimagedirect.com/winstaff/nim3/PayerExceptionApp.jsp";
						$http.get('https://staging.nextimagedirect.com/winstaff/nim3/PayerExceptionRest.jsp?exceptionList').success(function(data) {
							$scope.payerExceptionList = '';
							$scope.createPayerException();
							$scope.payerExceptionList = data;
			            });
		            });
				}
			};
			$scope.editPayerException = function(id) {
				if(!$scope.canEdit){
					$scope.payerId = $scope.payerExceptionList[id].payerId;
					$scope.practiceId = $scope.payerExceptionList[id].practiceId;
					$scope.fs = $scope.payerExceptionList[id].fs;
					$scope.percentage = $scope.payerExceptionList[id].percentage;
					$scope.cpts = $scope.payerExceptionList[id].cpts;
					$scope.feeType = $scope.payerExceptionList[id].feeType;
					$scope.editId = id;
					$scope.edit = false;
				}
			};
			
			$scope.insertModalObject = function(id) {
				if(id=='clear'){
					$scope.cpts = '';
				} else{
					$scope.cpts += ($scope.cpts.trim().length>0 ? ',':'')+$scope.modalObject[id].toString();
				}
			};
			
			$.fn.serializeObject = function()
			{
			    var o = {};
			    var a = this.serializeArray();
			    $.each(a, function() {
			        if (o[this.name] !== undefined) {
			            if (!o[this.name].push) {
			                o[this.name] = [o[this.name]];
			            }
			            o[this.name].push(this.value || '');
			        } else {
			            o[this.name] = this.value || '';
			        }
			    });
			    if($scope.editId!=''){
			    	o["payerExceptionId"] = $scope.editId;
			    }
			    
			    return o;
			};
		}
		
		$('#search').keyup(function() {
			var $rows = $('table tbody tr');
		    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
		    
		    $rows.show().filter(function() {
		        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
		        return !~text.indexOf(val);
		    }).hide();
		});
	</script>

</body>
</html>