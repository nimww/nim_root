<%@page language="java" contentType="application/json; charset=UTF-8" import="java.io.*,com.winstaff.*, java.util.*, java.sql.*,com.google.gson.Gson;"%>
<%!class N4_zSearch {
		int id;
		String zip;
		String city;
		public N4_zSearch( int id,String zip, String city) {
			this.id = id;
			this.zip = zip;
			this.city = city;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getZip() {
			return zip;
		}
		public void setZip(String zip) {
			this.zip = zip;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
	}
%>
<%
/* '" + search + "%' */
	List<N4_zSearch> zrl = new ArrayList<N4_zSearch>();
	if (request.getParameter("term") != null && !request.getParameter("term").isEmpty()) {
		String search = request.getParameter("term");
		String query = "SELECT s.stateid,zip.zip_code,zip.city\n" +
				"FROM zip_code AS zip\n" +
				"INNER JOIN tstateli AS s\n" +
				"ON s.shortstate = zip.state_prefix\n" +
				"WHERE zip.zip_code like '" + search + "%'\n" +
				"ORDER BY s.shortstate DESC\n" +
				"LIMIT 15";
		searchDB2 db = new searchDB2();
		ResultSet rs = db.executeStatement(query);
		try {
			while (rs.next()) {
				zrl.add(new N4_zSearch(rs.getInt(1),rs.getString(2),rs.getString(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closeAll();
		}
	}
	response.setHeader("Access-Control-Allow-Origin", "*");
	out.println(new Gson().toJson(zrl));
%>
