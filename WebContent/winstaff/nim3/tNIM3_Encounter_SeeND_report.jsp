<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">

<%@page contentType="text/html" language="java" import="com.winstaff.*,java.util.*" %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
<%@ include file="../generic/generalDisplay.jsp" %>



<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

    <table cellpadding=5 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM_Encounter")%>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat("MM/dd/yyyy");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);
		String theMessage = "\n\n---\nScheduling Request for a \"See NetDev\" Provider";
		if (myEO.NIM3_Encounter.getisSTAT()==1)
		{
			theMessage += "\n\nThis is a STAT order\n";
		}
		theMessage += "\nRequesting Scheduler: " + CurrentUserAccount.getLogonUserName();
		theMessage += "\nCase Assigned To: " + myEO.getCase_AssignedTo().getLogonUserName();
		theMessage += "\nProcedure Codes: " + myEO.getServicePrintOut(false);
		theMessage += "\n\nPatient: " + myEO.NIM3_CaseAccount.getPatientLastName() + ", " + myEO.NIM3_CaseAccount.getPatientFirstName();
		theMessage += "\n   Address: " + myEO.NIM3_CaseAccount.getPatientAddress1() + " " + myEO.NIM3_CaseAccount.getPatientAddress2();
		theMessage += "\n            " + myEO.NIM3_CaseAccount.getPatientCity() + ", " + new bltStateLI(myEO.NIM3_CaseAccount.getPatientStateID()).getShortState() + " " + myEO.NIM3_CaseAccount.getPatientZIP() ;
		theMessage += "\nScanPass: " + myEO.NIM3_Encounter.getScanPass();
		theMessage += "\nScanPass Comments: " + myEO.NIM3_Encounter.getComments();
		theMessage += "\n---";
		theMessage += "\nPrescreen Notes:\n" + myEO.getPatientPreScreen();
		theMessage += "\n---";
		theMessage += "\nProvider Information:";
		bltPracticeMaster myPM = null;
		
		if (request.getParameter("EDIT")!=null&&request.getParameter("EDIT").equalsIgnoreCase("0"))
		{
			theMessage += "\nNo Provider Selected/Available";
		}
		else
		{
			String sIC = request.getParameter("EDIT");
			Integer iIC = new Integer(sIC);
			myPM = new bltPracticeMaster (iIC);
			theMessage += "\nSelected Provider/Practice:" + myPM.getPracticeName();
			theMessage += "\nPractice ID:" + myPM.getUniqueID();
		}
		



%>
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td><h1>Contact NetDev: Scheduling a Patient</h1></td>
    </tr>
<%
if (myPM==null)
{
%>
    <tr>
      <td>&nbsp;</td>
      <td class="tdHeader">No Provider Selected</td>
    </tr>
<%
}
else
{
%>
    <tr>
      <td>&nbsp;</td>
      <td class="tdHeader">Selected Provider: <%=myPM.getPracticeName()%></td>
    </tr>
<%
}
%>
	<tr>
	<% 
	boolean addressCheck = true;
	boolean cptCheck = true;
		
		//False if zipcode is empty
		if(myEO.NIM3_CaseAccount.getPatientZIP().isEmpty()){  
			addressCheck = false;
		}
		
		//Get all active services, add to list
		ListElement listelement;
		bltNIM3_Service ser;
		java.util.Enumeration s = myEO.NIM3_Service_List.elements();
		List<String> cpt = new ArrayList<String>();
		while(s.hasMoreElements()){
			listelement = (ListElement) s.nextElement();
			ser = (bltNIM3_Service)listelement.getObject();
			if(ser.getServiceStatusID()==1){
				cpt.add(ser.getCPT());
			}
		}
		String o[] = {"77002","77003"};
		
		//False if no service, False if in CA with "77002","77003" codes
		if (cpt.size() == 0){
			cptCheck = false;
		} 
		
		
		
		%>
		<td>&nbsp;</td>
		<td>
			<h3 style="<%=addressCheck ? "color:rgb(106, 194, 25);":"background:red;"%>display:inline;"><%=addressCheck ? "Patient Address Good": "Need to collect Patient address before sending a NetDev Requet" %></h3><br>
			<h3 style="<%=cptCheck ? "color:rgb(106, 194, 25);":"background:red;"%>display:inline;"><%=cptCheck ? "CPT Good": "CPT missing or wrong code, please fix before proceeding" %></h3><br>
		
		</td>
	</tr>
    <tr>
      <td>&nbsp;</td>
      <td><p class="instructionsBig1">Please Make sure to include details appropriate as follows:</p>
      <p>When no provider  (within 50 miles) is available in NetDev:</p>
      <ol>
        <li>Always check with referring MD to see if there is a facility or MD they  normally use. If so, provide name and Phone #.</li>
        <li>See  if referring MD does the procedure. If so, get details for how to negotiate and provide Phone #.</li>
        <li>In  addition to the vital information on the referral, if there are physical  limitations (size, claustrophobia, etc.) indicate so.</li>
        <li>If  patient cannot travel 50 miles to provider, indicate so.</li>
        <li>If  scheduler has identified network providers in NetDev who cannot handle the  referral (time, unavailable) indicate so. </li>
      </ol>
      <p>Various Items to Consider &ndash; For Other OTA Requests </p>
      <ol start="1" type="1">
        <li>Must always include the source of a request.</li>
        <li>Must understand and apply the &ldquo;in network&rdquo; and &ldquo;out of       network&rdquo; thought process (concept) when communicating with Client. (Help       the client understand what out of network means to them)</li>
        <li>Make major deciding factors clear i.e Ref physician       requesting facility with the main cause being that they are holding the       RX.</li>
        <li>Ensure critical timelines or requests are clearly       detailed. Utilize the follow up procedure and notify when things are       outstanding (or beginning to look problematic)</li>
        <li>Include details on any previously eliminated options.       If you have already determined facilities that will not work with us, or       have denied in previously, we should know.</li>
        <li>Continue to immediately update NetDev on any additional       client requests or concerns made throughout the OTA process.</li>
      </ol></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><form id="form1" name="form1" method="post" action="tNIM3_Encounter_SeeND_report_sub.jsp">
        <label>
          <textarea name="problemReport" id="problemReport" cols="140" rows="10" ><%=theMessage%></textarea>
        </label>
        <br />
        <input name="" type="submit" class="inputButton_md_Create" value="Submit" <%=addressCheck && cptCheck ? "":"disabled" %>/>
      </form></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="right"><input type="button" class="inputButton_md_Stop" onclick="this.disabled=true;window.close();" value="Cancel" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr> 
    </table>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

