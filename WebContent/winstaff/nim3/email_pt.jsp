<%@page contentType="text/html" language="java" import="java.text.DecimalFormat ,com.winstaff.*,java.util.Calendar,java.text.SimpleDateFormat,java.util.Calendar,java.sql.ResultSet" %>
<!DOCTYPE HTML>
<%@ include file="../generic/CheckLogin.jsp" %>
<html>
<head>
<meta charset="UTF-8">
<title>email_pt</title>
</head>

<body>

<%
NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(new Integer(request.getParameter("eid")),"load");
java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
java.text.SimpleDateFormat dos = new java.text.SimpleDateFormat("EEEE, MMMM d, yyyy 'at' hh:mm a");
emailType_V3 myETSend = new emailType_V3();
String spType = request.getParameter("spType");
String theBody = "";	
bltNIM3_CommTrack myNCT = new bltNIM3_CommTrack();
	
	if(spType.equals("email_rc_pt")){
            //Send Notification to Patient
		String result_temp = NIMUtils.sendAlertEmail(myEO2, "email_rc_pt", "", false, "Payment Received - Alerts Sent");
 		myNCT.setMessageText("Email Patient Confirmation");
	}
	else if(spType.equals("email_sp_pt")){
		String result_temp = NIMUtils.sendAlertEmail(myEO2, "email_sp_pt", "", false, "SP Sent - Alerts Sent");
		myNCT.setMessageText("Email Patient SP");
		/* theBody = "<div style=\"padding:30px;font-size:14px;border:2px solid;\">";
		theBody += "<img src=\"http://www.nextimagedirect.com/logo.png\"><br>";
		theBody += displayDateSDF1.format(PLCUtils.getNowDate(false))+"<br><br>";
		theBody += "NextImage Direct greatly appreciates your business. Below you will find a receipt for your radiology service/s and your appointment details. If you have any questions or concerns, please do not hesitate to contact us at NextImage Direct (888) 693-8867. We look forward to meeting all your Radiology needs.<br><br>";
		theBody += "<strong style=\"text-decoration:underline;\">Your Appointment Details:</strong><br>";
		theBody += "Patient Name: "+myEO2.getNIM3_CaseAccount().getPatientFirstName()+" "+myEO2.getNIM3_CaseAccount().getPatientLastName()+"<br>";
		theBody += "Imaging Service(s):<br>";
		
		bltNIM3_Service  service;
		ListElement	current_element;
		java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
		int cnt55=1;
		while (eList_Service.hasMoreElements()){
			current_element = (ListElement) eList_Service.nextElement();
			service  = (bltNIM3_Service) current_element.getObject();
			if (service.getServiceStatusID().intValue()!=5){
				theBody += "<span style=\"margin-left:10px\">"+cnt55+") "+service.getCPTBodyPart()+"<br></span><br>";
				theBody += "Diagnosis: "+service.getdCPT1()+" "+service.getdCPT2()+" "+service.getdCPT3()+" "+service.getdCPT4()+"<br>";
				cnt55++;
			}
		}
		theBody += "<br>";
		theBody += dos.format(myEO2.getNIM3_Encounter().getDateOfService())+"<br><br>";
	
		theBody += myEO2.getAppointment_PracticeMaster().getPracticeName()+"<br>";
		theBody += myEO2.getAppointment_PracticeMaster().getOfficeAddress1()+" "+myEO2.getAppointment_PracticeMaster().getOfficeAddress2()+"<br>";
		theBody += myEO2.getAppointment_PracticeMaster().getOfficeCity()+", "+new bltStateLI(myEO2.getAppointment_PracticeMaster().getOfficeStateID()).getShortState()+" "+myEO2.getAppointment_PracticeMaster().getOfficeZIP()+"<br>";
		theBody += "P "+myEO2.getAppointment_PracticeMaster().getOfficePhone()+" F "+myEO2.getAppointment_PracticeMaster().getOfficeFaxNo()+"<br><br>";
		theBody += "Thank you for your payment in the amount of ";
		
		eList_Service = myEO2.getNIM3_Service_List().elements();
	
		while (eList_Service.hasMoreElements()){
			current_element = (ListElement) eList_Service.nextElement();
			service  = (bltNIM3_Service) current_element.getObject();
			if (service.getServiceStatusID().intValue()!=5){
				theBody += " $"+(Math.round(service.getReceivedCheck1Amount()+service.getReceivedCheck2Amount()+service.getReceivedCheck3Amount()));
			}
		}
		theBody += " paid directly to Next Image of CA.<br><br>";
		theBody += "<strong style=\"text-decoration:underline;\">Payment Confirmation:</strong><br>";
		
		bltCCardTransaction ccardtrans = new bltCCardTransaction(myEO2.getNIM3_Encounter().getCCTransactionID());
		//bltCCardTransaction ccardtrans = new bltCCardTransaction(2);
		
		theBody += "Bill To:"+"<br>";
		theBody += "<span style=\"text-transform:capitalize\">"+ccardtrans.getCName().toLowerCase()+"</span><br>";
		theBody += "<span style=\"text-transform:capitalize\">"+ccardtrans.getCAddress().toLowerCase()+"</span><br>";
		theBody += "<span style=\"text-transform:capitalize\">"+ccardtrans.getCCity().toLowerCase()+", "+ccardtrans.getCState()+" "+ccardtrans.getCZip()+"</span><br>";
		theBody += "<br>";
		theBody += "Account: *"+ccardtrans.getCCNumber()+"<br>";
		theBody += "Transaction Type: Sale"+"<br>";
		theBody += "Order: "+ccardtrans.getOrderNumber()+"<br>";
		theBody += "Auth: "+ccardtrans.getAuthNumber()+"<br><br>";
		theBody += "Amount: $"+ccardtrans.getAmountCharged()+".00<br>";
		theBody += "Tax: $0.00<br>";
		theBody += "Total: $"+ccardtrans.getAmountCharged()+".00<br><br>";
		theBody += "Cardmember acknowledges receipt of goods and/or services in the amount of the total shown hereon and agrees to perform the obligations set forth by the cardmember's agreement with the issuer.<br><br>";
		
		theBody += "<div style=\"text-align: center;\">www.nextimagedirect.com<br><span style=\"font-size:12px;\">Phone: 888-693-8867 Fax: 888-371-3302<br>Email: orders@nextimagedirect.com</span></div>";
		theBody += "</div>";
		
		myETSend.setBody(theBody);
		myETSend.setBodyType(myETSend.HTML_TYPE);
	
		myETSend.setFrom("support@nextimagemedical.com");
		myETSend.setSubject("NextImage Direct receipt for your radiology service/s and your appointment details");
		//myETSend.setSubject("Thank you for your Diagnostic Imaging Reservation!  ["+myEO2.getNIM3_Encounter().getScanPass()+"] ");
		myEO2.getNIM3_Encounter().setSentTo_SP_Pt(PLCUtils.getNowDate(true));
		myEO2.getNIM3_Encounter().commitData();
		myNCT.setMessageText("Email Patient Scanpass / Receipt"); */
		
	}
	
/*	
	myETSend.setTo(myEO2.getNIM3_CaseAccount().getPatientEmail());
	//myETSend.setTo("po.le@nextimagemedical.com");
	
	
	myNCT.setUniqueModifyComments("NIMUtils:sendAlertEmail:System Generated");
	myNCT.setCaseID(myEO2.getNIM3_CaseAccount().getCaseID());
	myNCT.setReferralID(myEO2.getNIM3_Referral().getReferralID());
	myNCT.setEncounterID(myEO2.getNIM3_Encounter().getEncounterID());
	myNCT.setCommStart(PLCUtils.getNowDate(false));
	myNCT.setCommEnd(PLCUtils.getNowDate(false));
	myNCT.setCommTypeID(new Integer(3));
	myNCT.setAlertStatusCode(new Integer(1));
	myNCT.setMessageName("System Generated");
	
	bltNIM3_CommReference myNCR = new bltNIM3_CommReference();
	myNCR.setUniqueModifyComments("NIMUtils:sendAlertEmail:System Generated");
	myNCR.setCRTypeID(new Integer(1));  //1 = email   2 = fax email  3 = fax manual
	myNCR.setRefID(myETSend.isSendMail_Track());
	myNCR.commitData();
	myNCT.setCommReferenceID(myNCR.getUniqueID());
	myNCT.commitData();
*/
%>


</body>
</html>