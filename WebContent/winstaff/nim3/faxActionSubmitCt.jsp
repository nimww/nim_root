<%@page import="java.io.*,com.winstaff.*, java.util.*, java.sql.*, java.text.SimpleDateFormat;"%>
<%
	String scanpass = "";
	String ctNotes = "";
	boolean isGood = true;
	
	try{
		scanpass = request.getParameter("scanpass");
		ctNotes = request.getParameter("ct");
	} catch(Exception e){
		isGood = false;
	}
	
	String query = "";
	NIM3_EncounterObject2 eo2 = null;
	
	SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy");
	SimpleDateFormat timestamp = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	
	if (isGood) {
		scanpass = request.getParameter("scanpass");

		query = "SELECT encounterid from tnim3_encounter where scanpass = '" + scanpass + "'";

		searchDB2 db = new searchDB2();

		ResultSet rs = db.executeStatement(query);

		try {
			while (rs.next()) {
				eo2 = new NIM3_EncounterObject2(rs.getInt(1), "");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closeAll();
		}
		
		java.util.Date now = new java.util.Date();
		
		bltNIM3_CommTrack ct = new bltNIM3_CommTrack();
		ct.setEncounterID(eo2.getNIM3_Encounter().getEncounterID());
		ct.setReferralID(eo2.getNIM3_Referral().getReferralID());
		ct.setCaseID(eo2.getNIM3_CaseAccount().getCaseID());
		ct.setMessageName("N4 FaxAPI");
		ct.setMessageCompany("NextImage Medical");
		ct.setUniqueCreateDate(now);
		ct.setUniqueModifyDate(now);
		ct.setCommStart(now);
		ct.setCommEnd(now);
		ct.setUniqueModifyComments("N4 FaxAPI");
		ct.setMessageText(ctNotes);
		ct.setCommTypeID(0);
		ct.setAlertStatusCode(1);
		ct.commitData();
        
        
	}
	if (isGood) {
%>
Action submitted successfully.
<%
	} else{
%>
Error. Action did not complete. 
<%
	}
%>
