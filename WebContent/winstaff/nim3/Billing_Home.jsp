<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
</head>
<SCRIPT language="JavaScript1.2">
function openStatus()
{
	testwindow= window.open ("Home_Status.jsp", "HomeStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(0,0);
}

function openAuto(myAuto)
{
	testwindow= window.open ("report_auto2.jsp?rid=" + myAuto + "&MREF=300", "AutoStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(100,100);
}

</SCRIPT>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler2) 
    {
        accessValid = true;
    }
	
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
	  %>
<table  border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td ><p class="title">Welcome to NextImage Grid Version <%=ConfigurationInformation.applicationVersion%><br>
          Server: <%=ConfigurationInformation.serverName%> [<%=ConfigurationInformation.applicationVersion%>] </p></td>
      </tr>
      <%
	  if (isScheduler2)
	  {
	  %>
      <tr class="tdHeaderAlt" >
        <td ><span class="tdHeader"><hr />
        Billing Home:</span></td>
      </tr>
<%if (isScheduler3)
{
	%>
      <tr>
        <td bgcolor="#DAE0E4"><input name="button2" type="button" class="inputButton_md_Create" id="button2" onClick="this.disabled = true; document.location='../app/AdminPracticeAll_home-seend.jsp';" value="See NetDev Worklist" /></td>
      </tr>
    <%
}
%>
      <tr>
        <td bgcolor="#DAE0E4">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#9AE0E4" ><p class="tdHeader">Download Billing</p>
          <form name="form1" method="post" action="billing_dl.jsp">
            <p>
              <label>Start Date: 
                <input name="StartDate" type="text" id="StartDate" value="t-7" size="10" maxlength="20">
              </label>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <label>
                End Date: 
                  <input name="EndDate" type="text" id="EndDate" value="t+1" size="10" maxlength="20">
              </label>
            </p>
            <p>
              <label>Bill Type
                <select name="BillType" id="BillType">
                  <option value="AR">AR</option>
                  <option value="AP">AP</option>
                  </select>
              </label>
            </p>
            <p>
              <label>
                <input type="submit" name="Download" id="Download" value="Download">
              </label>
            </p>
          </form>
</td>
      </tr>
    </table></td>
  </tr>
</table>
          <%
	  }
	  %>

    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>