<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    filename: tnim2_Authorization_main_NIM_Authorization_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
    }
	if (isScheduler)
	{
        accessValid = true;
	}
	int iOpenID = 0;
	String sOpenField = "";
	try
	{
		iOpenID= Integer.parseInt(request.getParameter("ssOpenID"));
		sOpenField= (request.getParameter("ssOpenField"));
	}
	catch(Exception e)
	{
		accessValid = false;
	}

	int iMaint_PayerID = 0;
	boolean isMaintCreate = false;
	//for creating via Payer Maint.
	try
	{
		iMaint_PayerID= Integer.parseInt(request.getParameter("ssMaint_PayerID"));
		isMaintCreate = true;
		accessValid = true;
	}
	catch(Exception e)
	{
	}


//page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
	  boolean firstDisplay = false;
String sOpenType = "Invalid";
int i2OpenID = iOpenID;
boolean searchAll = false;
if (iOpenID==1)
{
	sOpenType = "Provider";
}
else if (iOpenID==2)
{
	sOpenType = "Adjuster/Case Manager";
}
else if (iOpenID==3)
{
	sOpenType = "Adjuster/Case Manager";
	i2OpenID = 2;
}
else if (iOpenID==4)
{
	sOpenType = "Adjuster/Case Manager";
	i2OpenID = 2;
	searchAll = true;
}

%>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<form id="form1" target="_self" name="form1" method="post" action="tNIM3_UserAccount_create_sub.jsp">
<input type="hidden" value="new" name="EDIT">
<input type="hidden" name="ssOpenID" value ="<%=i2OpenID%>">
<input type="hidden" name="ssOpenField" value ="<%=sOpenField%>">
<table border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td colspan="3" class="title">New Record</td>
    </tr>
  <tr>
    <td>Account Type</td>
    <td><% if (isMaintCreate&&isScheduler4)
	{
	%>
		<select   name="AccountType" >
          <jsp:include page="../generic/tUserAccountTypeLI_admin4.jsp" flush="true" >
            <jsp:param name="CurrentSelection" value="0" />  
          </jsp:include>
	    </select>    <%
	} else if (isMaintCreate&&isScheduler) {
	%>
		<select   name="AccountType" >
          <jsp:include page="../generic/tUserAccountTypeLI.jsp" flush="true" >
            <jsp:param name="CurrentSelection" value="0" />  
          </jsp:include>
	    </select>    <%
	} else {
	%>
		<input name="ssOpenID" type="text" id="ssOpenID" readonly="readonly" value="<%=sOpenType%>" />
    <%
	}
	%>
	
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Affiliated With</td>
    <td><select id='ReferenceID' name="ReferenceID"/>
        <option value="0">Please Select...</option>
    <%
if (iOpenID==1)
{
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("select PayerID, PayerName from tNIM3_PayerMaster where PayerTypeID = " + iOpenID + "and payerid=1 order by payername");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
				%>
                <option value="<%=myRS.getString("PayerID")%>"><%=myRS.getString("PayerName")%></option>
        <%
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
else if (iOpenID==3||iOpenID==4)
{
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("select PayerID, PayerName from tNIM3_PayerMaster where PayerTypeID = 2 and payerid>0 order by payername");  //matches iOpen Type = Paye r Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
/*
				if (myRS.getString("PayerID").equalsIgnoreCase("50"))
				{
					%>
					<option selected value="<%=myRS.getString("PayerID")%>"><%=myRS.getString("PayerName")%></option>
					<%
				}
				else
*/				
				{
					%>
					<option  value="<%=myRS.getString("PayerID")%>"><%=myRS.getString("PayerName")%></option>
					<%
				}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
else if (isMaintCreate)
{
	bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(iMaint_PayerID);
	
	%>
	<option selected="selected" value = "<%=myPM.getPayerID()%>"><%=myPM.getPayerName()%></option>
<%
}
else
{
	  bltNIM3_CaseAccount NCA = new bltNIM3_CaseAccount(iCaseID);
	bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(NCA.getPayerID());
	
	%>
	<option value = "<%=myPM.getPayerID()%>"><%=myPM.getPayerName()%></option>
<%
}
%>
    </select></td>
    <td class="instructionsBig1">Note: If the payer does not exist, then you must leave this screen and create the payer before creating this account.</td>
  </tr>
  <tr>
    <td>First Name</td>
    <td><input name="ContactFirstName" type="text" class="inputButton_md_Action1" id="ContactFirstName" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Last Name</td>
    <td><input name="ContactLastName" type="text" class="inputButton_md_Action1" id="ContactLastName" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Phone</td>
    <td><input name="ContactPhone" type="text" class="inputButton_md_Action1" id="ContactPhone" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Mobile</td>
    <td><input type="text" name="ContactMobile" id="ContactMobile" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Fax</td>
    <td><input name="ContactFax" type="text" class="inputButton_md_Action1" id="ContactFax" /></td>
    <td>&nbsp;</td>
  </tr>
<!--
  <tr>
    <td>Address 1</td>
    <td><input type="text" name="ContactAddress1" id="ContactAddress1" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Address 2</td>
    <td><input type="text" name="ContactAddress2" id="ContactAddress2" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>City</td>
    <td><input type="text" name="ContactCity" id="ContactCity" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>State</td>
    <td><select name="ContactStateID">
                <jsp:include page="../generic/tStateLIShort.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="0" />
                </jsp:include>
              </select></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>ZIP</td>
    <td><input type="text" name="ContactZIP" id="ContactZIP" /></td>
    <td>&nbsp;</td>
  </tr>
  -->
  <tr>
    <td>Email</td>
    <td><input name="ContactEmail" type="text" class="inputButton_md_Action1" id="ContactEmail" /></td>
    <td><input name="Declined" type="button" class="inputButton_sm_Stop" id="Declined" onclick="if (confirm('Are you sure the user does not have an email address?')){javascript:document.forms.form1.ContactEmail.value='Declined';}" value="Declined" />&nbsp;</td>
  </tr>
  <tr>
    <td>Alt Email</td>
    <td><input name="ContactEmail2" type="text" class="inputButton_md_Action1" id="ContactEmail2" /></td>
    <td><input name="Declined" type="button" class="inputButton_sm_Stop" id="Declined" onclick="if (confirm('Are you sure the user does not have an alt email address?')){javascript:document.forms.form1.ContactEmail2.value='Declined';}" value="Declined" />&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><table border="1" cellspacing="0" cellpadding="3">
      <tr class="tdHeaderAlt">
        <td>&nbsp;</td>
        <td>Reports</td>
        <td>Schedule Notification</td>
        <td>Other Notifications</td>
      </tr>
      <tr class="tdHeaderAlt">
        <td>Notify me when...</td>
        <td><select name="Comm_Report_LevelUser" id="AlertDays">
          <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
            <jsp:param name="CurrentSelection" value="" />    
            </jsp:include>
        </select></td>
        <td><select name="Comm_Alerts_LevelUser" id="AlertDays">
          <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
            <jsp:param name="CurrentSelection" value="" />    
            </jsp:include>
        </select></td>
        <td><select name="Comm_Alerts2_LevelUser" id="AlertDays">
          <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
            <jsp:param name="CurrentSelection" value="" />    
            </jsp:include>
        </select></td>
      </tr>
      <% if (false){%>
      <tr class="tdHeaderAlt">
        <td>If I supervise anyone, alert me of their cases when...</td>
        <td><select name="Comm_Report_LevelManager" id="AlertDays">
          <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
            <jsp:param name="CurrentSelection" value="" />    
            </jsp:include>
        </select></td>
        <td><select name="Comm_Alerts_LevelManager" id="AlertDays">
          <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
            <jsp:param name="CurrentSelection" value="" />    
            </jsp:include>
        </select></td>
        <td><select name="Comm_Alerts2_LevelManager" id="AlertDays">
          <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
            <jsp:param name="CurrentSelection" value="" />    
            </jsp:include>
        </select></td>
      </tr>
      <tr class="tdHeaderAlt">
        <td>If I manage an office, alert me of all cases when...</td>
        <td><select name="Comm_Report_LevelBranch" id="AlertDays">
          <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
            <jsp:param name="CurrentSelection" value="" />    
            </jsp:include>
        </select></td>
        <td><select name="Comm_Alerts_LevelBranch" id="AlertDays">
          <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
            <jsp:param name="CurrentSelection" value="" />    
            </jsp:include>
        </select></td>
        <td><select name="Comm_Alerts2_LevelBranch" id="AlertDays">
          <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
            <jsp:param name="CurrentSelection" value="" />    
            </jsp:include>
        </select></td>
      </tr>
      <% }%>
    </table></td>
    </tr>
  <tr>
    <td colspan="3" class="tdHeaderAlt"><strong>Optional Information</strong></td>
    </tr>
  <tr>
    <td class="tdBaseAlt2">User Notes</td>
    <td class="tdBaseAlt2"><textarea onKeyDown="textAreaStop(this,200)" rows="3" name="ImportantNotes" cols="40"></textarea></td>
    <td>&nbsp;</td>
  </tr>
<!--
  <tr>
    <td class="tdBaseAlt2">Pop-Up User Notes</td>
    <td class="tdBaseAlt2"><textarea onKeyDown="textAreaStop(this,200)" rows="3" name="ImportantNotes_Alert" cols="40"></textarea></td>
    <td>&nbsp;</td>
  </tr>
-->
  <tr>
    <td class="tdBaseAlt2">User Notes<br />
for Notifications</td>
    <td class="tdBaseAlt2"><textarea onKeyDown="textAreaStop(this,200)" rows="3" name="EmailAlertNotes_Alert" cols="40"></textarea></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="tdBaseAlt2">Office/Business Name</td>
    <td class="tdBaseAlt2"><input type="text" name="CompanyName" id="CompanyName" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="tdBaseAlt2">Address</td>
    <td class="tdBaseAlt2"><input type="text" name="ContactAddress1" id="ContactAddress1" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="tdBaseAlt2">Address 2</td>
    <td class="tdBaseAlt2"><input type="text" name="ContactAddress2" id="ContactAddress2" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="tdBaseAlt2">City</td>
    <td class="tdBaseAlt2"><input type="text" name="ContactCity" id="ContactCity" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="tdBaseAlt2">State</td>
    <td class="tdBaseAlt2"><select   name="ContactStateID" >
      <jsp:include page="../generic/tStateLILong.jsp" flush="true" >
        <jsp:param name="CurrentSelection" value="0" />  
        </jsp:include>
    </select></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="tdBaseAlt2">ZIP</td>
    <td class="tdBaseAlt2"><input type="text" name="ContactZIP" id="ContactZIP" /></td>
    <td>&nbsp;</td>
  </tr>

 <tr>
    <td class="tdBaseAlt2">NPI</td>
<td valign=top><p><input maxlength="50" type=text size="40" name="UserNPI" value=""></td>
<td>&nbsp;</td>
</tr>
 <tr>
    <td class="tdBaseAlt2">State License</td>
<td valign=top><p><input maxlength="40" type=text size="40" name="UserStateLicense" value=""></td>
<td>&nbsp;</td>
</tr>
 <tr>
    <td class="tdBaseAlt2">UserStateLicenseDesc</td>
<td valign=top><p><input maxlength="10" type=text size="15" name="UserStateLicenseDesc" value=""></td>
<td>&nbsp;</td>
</tr>


  <tr>
    <td>
      <label>
        <input name="button" type="submit" onclick="javascript:if(document.getElementById('contactEmail').value>''&&document.getElementById('ContactFirstName').value>''&&document.getElementById('ContactLastName').value>''&&document.getElementById('ContactPhone').value>''){if (document.getElementById('ReferenceID').value==0){alert('You must select a Payer');return false;} else if (document.getElementById('ReferenceID').value==50){if (confirm('Are you sure you want to create this user linked to NO PAYER?')){return true;}else{return false;}}}else{alert('Name, Phone, and Email must be filled out.  Please Correct.');return false;}" class="inputButton_lg_Create" id="button" value="Create" />
      </label>
</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
    </form>
<%



  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_ClearID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>