<%@page language="java" contentType="application/json; charset=UTF-8" import="java.io.*,com.winstaff.*, java.util.*, java.sql.*,com.google.gson.Gson;"%>
<%!class PMResults {
		String value;
		int id;

		public PMResults(String value, int id) {
			this.value = value;
			this.id = id;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
	}%>
<%
	List<PMResults> pmrl = new ArrayList<PMResults>();

	if (request.getParameter("term") != null && !request.getParameter("term").isEmpty()) {
		String search = request.getParameter("term");

		String query = "SELECT payerid, payername\n" +
				"from tnim3_payermaster\n" +
				"where LOWER (\n" +
				"				payername\n" +
				"			) ~ lower('" + search + "')\n" +
				"ORDER BY payerid desc\n" +
				"limit 50";
		searchDB2 db = new searchDB2();

		ResultSet rs = db.executeStatement(query);

		try {
			while (rs.next()) {
				pmrl.add(new PMResults(rs.getString(2), rs.getInt(1)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closeAll();
		}
	}
	response.setHeader("Access-Control-Allow-Origin", "*");
	out.println(new Gson().toJson(pmrl));
%>
