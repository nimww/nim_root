<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*    " %>
<%/*
    filename: out\jsp\tNIM3_Encounter_form_sub.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
        if (iEncounterID.intValue() != iEDITID.intValue())
        {
        	accessValid = false;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltNIM3_Encounter        NIM3_Encounter        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("nad") )
    {
        NIM3_Encounter        =    new    bltNIM3_Encounter(iEncounterID,UserSecurityGroupID);
    }

String Enc_testChangeID = "0";
      String tempAN =	NIM3_Encounter.getAuditNotes();
	  String tempAN_Status = "";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("NextActionDate"))) ;
    if ( !NIM3_Encounter.getNextActionDate().equals(testObj)  )
    {
         NIM3_Encounter.setNextActionDate( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NextActionNotes")) ;
    if ( !NIM3_Encounter.getNextActionNotes().equals(testObj)  )
    {
         NIM3_Encounter.setNextActionNotes( testObj,UserSecurityGroupID   );
         Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter UniqueModifyComments not set. this is ok-not an error");
}



boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tNIM3_Service","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (Enc_testChangeID.equalsIgnoreCase("1"))
{

	NIM3_Encounter.setUniqueModifyDate(PLCUtils.getNowDate(false));
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    NIM3_Encounter.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
			tempAN +="\n[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nNAD Altered";
			NIM3_Encounter.setAuditNotes(tempAN);
			boolean isClear = false;
			String theNotes = NIM3_Encounter.getNextActionNotes();
			if (NIM3_Encounter.getNextActionDate().before(NIMUtils.getBeforeTime()))
			{
				NIM3_Encounter.setNextActionNotes ("");
				isClear = true;
			}
	        NIM3_Encounter.commitData();
			
//create commTrack
			NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);

			
			bltNIM3_CommTrack        working_bltNIM3_CommTrack = new bltNIM3_CommTrack();
			working_bltNIM3_CommTrack.setEncounterID(iEncounterID);
			working_bltNIM3_CommTrack.setReferralID(myEO.NIM3_Encounter.getReferralID());
			working_bltNIM3_CommTrack.setCaseID(myEO.NIM3_Referral.getCaseID());
			working_bltNIM3_CommTrack.setMessageName(CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName());
			bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
			working_bltNIM3_CommTrack.setMessageCompany(myPM.getPayerName());
			working_bltNIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
			working_bltNIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
			working_bltNIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
			working_bltNIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
			working_bltNIM3_CommTrack.setUniqueModifyComments(""+UserLogonDescription);
			working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
			if (isClear)
			{
				working_bltNIM3_CommTrack.setMessageText("NextAction Date Cleared:\n Notes:" + theNotes);
			}
			else
			{
				working_bltNIM3_CommTrack.setMessageText("NextAction Date Set\nDate:" + NIM3_Encounter.getNextActionDate() + "\nNotes:" + theNotes);
			}
			working_bltNIM3_CommTrack.commitData();																															
	        NIM3_Encounter.commitData();
			

	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }
}








if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else
{
	    %><script language=Javascript>
window.close();
	      </script><hr><input value="Close" name="closeme" onClick="window.close();" type="button" /><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
