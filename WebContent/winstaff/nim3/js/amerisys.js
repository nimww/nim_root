function validateForm(){
	var mdName=document.forms["at_intake"]["mdName"].value;
	var mdPhone=document.forms["at_intake"]["ReferringPhysicianPhone"].value;
	var claimsNumber=document.forms["at_intake"]["clientClaimNum"].value;
	var cptText=document.forms["at_intake"]["CPTText"].value;
	
	if ((claimsNumber==null || claimsNumber=="") )
	{
		alert("Please select patient from the search.");
		return false;
	}
	if ((mdName==null || mdName=="") )
	{
		alert("Please enter Ordering/Treating Physician name.");
		return false;
	}	  
	if ((mdPhone==null || mdPhone=="") )
	{
		alert("Please enter Ordering/Treating Physician phone.");
		return false;
	}
	if ((cptText==null || cptText=="") )
	{
		alert("Please enter Procedure Information & Body Part.");
		return false;
	}
}
function search_main(){
	var query ="";
	var query2="";
	var temp = document.getElementById("claimNumberSearch").value;
	if (temp.indexOf(',') !== -1){
		var temparr = temp.replace(/\s/g, '').split(",");
		query = temparr[0].toLowerCase();
		query2 = temparr[1].toLowerCase();
	}
	else{
		query = temp.toLowerCase();
		query2 = 'undefined';
	}
	$("#ameri-query").html("<p>Search Results:</p><iframe src=\"amerisys_query.jsp?submit=true&query="+query+"&query2="+query2+"\" width=\"630\" height=\"480px\" id=\"ameriDbResults\"></iframe>");
	//$("#ameri-query").html("<p>Search Results:</p><img id=\"close-search-box\" src=\"images/close.png\" onclick=\"esc();\"><iframe src=\"amerisys_query.jsp?query="+query+"&query2="+query2+"\" width=\"630\" height=\"480px\" id=\"ameriDbResults\"></iframe>");
}

function selectClaimant (clientClaimNum, patientLastName, patientFirstName, patientAddress1, patientAddress2, patientCity, patientState, patientZip, patientPhone,        
patientDOB, adjusterFirstName, adjusterLastName, casemanagerFirstName, casemanagerLastName, employer, patientDOI, patientGender, PatientSSN, stateid){
	$("input#clientClaimNum").val(clientClaimNum);
	$("span#clientClaimNum").text(clientClaimNum);
	
	$("input#patientLastName").val(patientLastName);
	$("span#patientLastName").text(patientLastName+",");
	
	$("input#patientFirstName").val(patientFirstName);
	$("span#patientFirstName").text(patientFirstName);
	
	$("input#patientAddress1").val(patientAddress1);
	$("span#patientAddress1").text(patientAddress1);
	
	$("input#patientAddress2").val(patientAddress2);
	$("span#patientAddress2").text(patientAddress2);
	
	$("input#patientCity").val(patientCity);
	$("span#patientCity").text(patientCity+",");
	
	$("input#patientState").val(patientState);
	$("span#patientState").text(patientState);
	
	$("input#patientZip").val(patientZip);
	$("span#patientZip").text(patientZip);
	
	$("input#patientPhone").val(patientPhone);
	$("span#patientPhone").text(patientPhone);
	
	$("input#patientDOB").val(patientDOB);
	$("span#patientDOB").text(patientDOB);
	
	$("input#adjusterFirstName").val(adjusterFirstName);
	$("span#adjusterFirstName").text(adjusterFirstName);
	
	$("input#adjusterLastName").val(adjusterLastName);
	$("span#adjusterLastName").text(adjusterLastName);
	
	$("input#casemanagerFirstName").val(casemanagerFirstName);
	$("span#casemanagerFirstName").text(casemanagerFirstName);
	
	$("input#casemanagerLastName").val(casemanagerLastName);
	$("span#casemanagerLastName").text(casemanagerLastName);
	
	$("input#employer").val(employer);
	$("span#employer").text(employer);
	
	$("input#patientDOI").val(patientDOI);
	$("span#patientDOI").text(patientDOI);
	
	$("input#PatientStateID").val(stateid);
	$("input#PatientSSN").val(PatientSSN);
	$("input#patientGender").val(patientGender);
	
	patientGender
	$("#ameri-query, #faded-bg").fadeOut('fast');
	$("#ameri-query").html("");
}

function esc(){
	$("#ameri-query, #faded-bg").fadeOut('fast');
}

$(document).ready(function(e) {
	windowwidth = ($(window).width()-640)/2;
	windowheight =($(window).height()-480)/2;
	if ($(window).width() < 640){
		$("#ameri-query").css({left:0});
	}
	else $("#ameri-query").css({'left':windowwidth});
	if ($(window).height() < 480){
		$("#ameri-query").css({top:0});
	}
	else $("#ameri-query").css({'top':windowheight});
	$("#faded-bg").css({'width':$(window).width(),'height':$(window).height()});
	
	$("#claimant-search").click(function(e) {
		$("#ameri-query, #faded-bg").fadeIn('fast');
	});
	$("#faded-bg").click(function(e) {
		$("#ameri-query, #faded-bg").fadeOut('fast');
		$("#ameri-query").html("");
	});
});

$(document).keyup(function(e) {
	if (e.keyCode == 27) { $("#ameri-query, #faded-bg").fadeOut('fast');$("#ameri-query").html(""); }
	if ($("#claimNumberSearch").is(":focus") && e.keyCode == 13) { search_main() }
});

$(window).resize(function(){
	windowwidth = ($(window).width()-640)/2;
	windowheight =($(window).height()-480)/2;
	if ($(window).width() < 640){
		$("#ameri-query").css({left:0});
	}
	else $("#ameri-query").css({'left':windowwidth});
	if ($(window).height() < 480){
		$("#ameri-query").css({top:0});
	}
	else $("#ameri-query").css({'top':windowheight});
	$("#faded-bg").css({'width':$(window).width(),'height':$(window).height()});
});
