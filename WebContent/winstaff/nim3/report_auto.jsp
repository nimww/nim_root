<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<style type="text/css">
<!--
th 
{
	font-family: Verdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #FFF;
	background-color: #036;
}

  .initial { background-color: #DDDDDD; color:#000000 }
  .normal { background-color: #CCCCCC }
  .highlight { background-color: #8888FF }
</style>


td 
{
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
}

.tdAlt 
{
	background:#EEE;
}

-->
</style>
</head>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler) 
    {
        accessValid = true;
    }
	if (accessValid)
	{
		String myRID = request.getParameter("rid");
		
		String sTitle = "";
		String sSQL = "";
		if (myRID.equalsIgnoreCase("1"))
		{
			sSQL = "select * FROM \"public\".\"vSummary of Referrals6\"";
			sTitle = "Summary Of Referrals V6";
		}
		else if (myRID.equalsIgnoreCase("2"))
		{
			sSQL = "select * FROM \"public\".\"vActivity View All Payers March 2010\"";
			sTitle = "vActivity View All Payers March 2010";
		}
		else if (myRID.equalsIgnoreCase("6"))
		{
			sSQL = "SELECT tnim3_caseaccount.caseclaimnumber AS \"Claim #\", tnim3_service.cptbodypart AS \"Service\", (((tnim3_caseaccount.patientfirstname)::text || ' '::text) || (tnim3_caseaccount.patientlastname)::text) AS \"Patient Name\", (((tuseraccount.contactfirstname)::text || ' '::text) || (tuseraccount.contactlastname)::text) AS \"Adjuster\", tnim3_payermaster.payername AS \"Payer Name\", tnim3_referral.referraldate AS \"Referral Date\", tnim3_encounter.sentto_sp_adj AS \"Appointment Set\", (tnim3_encounter.sentto_sp_adj - tnim3_referral.referraldate) AS \"Turnaround Time\", tnim3_encounter.senttoadj AS \"Report Completed\" FROM (((((tnim3_caseaccount JOIN tuseraccount ON ((tnim3_caseaccount.adjusterid = tuseraccount.userid))) JOIN tnim3_payermaster ON ((tnim3_caseaccount.payerid = tnim3_payermaster.payerid))) JOIN tnim3_referral ON ((tnim3_caseaccount.caseid = tnim3_referral.caseid))) JOIN tnim3_encounter ON ((tnim3_referral.referralid = tnim3_encounter.referralid))) JOIN tnim3_service ON ((tnim3_encounter.encounterid = tnim3_service.encounterid))) WHERE ((((((tnim3_payermaster.payerid = 65) AND ((tnim3_caseaccount.patientlastname)::text <> ''::text)) AND ((tnim3_service.cptbodypart)::text <> ''::text)) AND (tnim3_encounter.encounterstatusid <> 5)) AND (tnim3_encounter.encounterstatusid <> 4)) AND (tnim3_encounter.encounterstatusid <> 3));";
			sTitle = "vActivity for PMOA";
		}
		else if (myRID.equalsIgnoreCase("4"))
		{
			sSQL = "SELECT tnim3_caseaccount.caseclaimnumber AS \"Claim #\", tnim3_service.cptbodypart AS \"Service\", (((tnim3_caseaccount.patientfirstname)::text || ' '::text) || (tnim3_caseaccount.patientlastname)::text) AS \"Patient Name\", (((tuseraccount.contactfirstname)::text || ' '::text) || (tuseraccount.contactlastname)::text) AS \"Adjuster\", tnim3_payermaster.payername AS \"Payer Name\", tnim3_referral.referraldate AS \"Referral Date\", tnim3_encounter.sentto_sp_adj AS \"Appointment Set\", (tnim3_encounter.sentto_sp_adj - tnim3_referral.referraldate) AS \"Turnaround Time\", tnim3_encounter.senttoadj AS \"Report Completed\" FROM (((((tnim3_caseaccount JOIN tuseraccount ON ((tnim3_caseaccount.adjusterid = tuseraccount.userid))) JOIN tnim3_payermaster ON ((tnim3_caseaccount.payerid = tnim3_payermaster.payerid))) JOIN tnim3_referral ON ((tnim3_caseaccount.caseid = tnim3_referral.caseid))) JOIN tnim3_encounter ON ((tnim3_referral.referralid = tnim3_encounter.referralid))) JOIN tnim3_service ON ((tnim3_encounter.encounterid = tnim3_service.encounterid))) WHERE ((((((tnim3_payermaster.payerid = 51) AND ((tnim3_caseaccount.patientlastname)::text <> ''::text)) AND ((tnim3_service.cptbodypart)::text <> ''::text)) AND (tnim3_encounter.encounterstatusid <> 5)) AND (tnim3_encounter.encounterstatusid <> 4)) AND (tnim3_encounter.encounterstatusid <> 3));";
			sTitle = "vActivity for Farmers Insurance";
		}
		else if (myRID.equalsIgnoreCase("7"))
		{
			sSQL = "SELECT tnim3_payermaster.payername, tnim3_payermaster.billprice_mod_mri_wo AS \"Bill Amt MRI wo\", tnim3_payermaster.price_mod_mri_wo AS \"Allow Amt MRI wo\", tnim3_payermaster.billprice_mod_mri_w AS \"Bill Amt MRI w\", tnim3_payermaster.price_mod_mri_w AS \"Allow Amt MRI w\", tnim3_payermaster.billprice_mod_mri_wwo AS \"Bill Amt MRI wwo\", tnim3_payermaster.price_mod_mri_wwo AS \"Allow Amt MRI wwo\", tnim3_payermaster.billprice_mod_ct_wo AS \"Bill Amt CT wo\", tnim3_payermaster.price_mod_ct_wo AS \"Allow Amt CT wo\", tnim3_payermaster.billprice_mod_ct_w AS \"Bill Amt CT w\", tnim3_payermaster.price_mod_ct_w AS \"Allow Amt CT w\", tnim3_payermaster.billprice_mod_ct_wwo AS \"Bill Amt CT wwo\", tnim3_payermaster.price_mod_ct_wwo AS \"Allow Amt CT wwo\", tnim3_payermaster.feepercentage AS \"FS1 %\", tnim3_payermaster.feepercentage2 AS \"FS2 %\" FROM tnim3_payermaster WHERE (tnim3_payermaster.payertypeid <> 0) ORDER BY tnim3_payermaster.payername;";
			sTitle = "vPayer Table Bill Allow Amounts";
		}
		else if (myRID.equalsIgnoreCase("3"))
		{
			sSQL = "select * from \"public\".vND_Overview";
			sTitle = "NetDev Overview";
		}
		
		else if (myRID.equalsIgnoreCase("8"))
		{
			sSQL = "select * from \"public\".\"vActivity All Payers April 2010\"";
			sTitle = "vActivity All Payers April 2010";
		}
		else if (myRID.equalsIgnoreCase("9"))
		{
			sSQL = "select * from \"public\".\"vSummary of Referrals7 by Create Date\"";
			sTitle = "vSummary of Referrals7 by Create Date";
		}
		else if (myRID.equalsIgnoreCase("10"))
		{
			sSQL = "select * from \"public\".\"vSummary of Referrals7 by Patient Name\"";
			sTitle = "vSummary of Referrals7 by Patient Name";
		}
		else if (myRID.equalsIgnoreCase("11"))
		{
			sSQL = "select * FROM \"public\".\"NIM3_CaseLoad\"";
			sTitle = "Scheduler Case Load";
		}
		
		
		%>
		<form action="report_auto.jsp" method="POST" id="autoform">
		<select name="rid" onChange="document.getElementById('autoform').submit();">
		  <option value="1">Please Select</option>
		  <option value="11">Scheduler Case Load</option>
		  <option value="1">Summary Of Referrals V6</option>
		  <option value="4">vActivity for Farmers Insurance</option>
		  <option value="8">vActivity All Payers April 2010</option>
		  <option value="6">vActivity for PMOA</option>
		  <option value="7">vPayer Table Bill Allow Amounts</option>
		  <option value="9">vSummary of Referrals7 by Create Date</option>
		  <option value="10">vSummary of Referrals7 by Patient Name</option>
		  <option value="2">vActivity View All Payers March 2010</option>
		  <option value="3">ND_State Report</option>
		</select>
		</form>
		
		<h1><%=sTitle%></h1>
		<%
			searchDB2 mySS = new searchDB2();
		//	String mySQL = ("SELECT LogonUserName, UniqueModifyDate from tUserAccount");
			java.sql.ResultSet result = null;
			java.sql.ResultSetMetaData rsmd = null;    
			result = mySS.executeStatement(sSQL);
			int columns=0;
			try 
			{
			   rsmd = result.getMetaData();
			   columns = rsmd.getColumnCount();
			}
			catch (SQLException e) 
			{
				out.println("Error occurred " + e);
			}
		%>
		<table cellspacing="3" border="1">
		  <tr>
		  <% // write out the header cells containing the column labels
			 try 
			 {
				for (int i=1; i<=columns; i++) 
				{
					 out.write("<th>" + rsmd.getColumnLabel(i) + "</th>");
				}
		  %>
		  </tr>
		  <% // now write out one row for each entry in the database table
		  int i5=0;
				while (result.next()) 
				{
					i5++;
					if (i5%2==0)
					{
					   out.write("<tr class=tdAlt   onMouseOver=\"this.className='highlight'\" onMouseOut=\"this.className='tdAlt'\" >");
					}
					else
					{
					   out.write("<tr onMouseOver=\"this.className='highlight'\" onMouseOut=\"this.className='normal'\" >");
					}
				   for (int i=1; i<=columns; i++) 
				   {
					 out.write("<td>&nbsp;" + result.getString(i) + "</td>");
				   }
				   out.write("</tr>");
				}
		
				// close the connection and the statement
				mySS.closeAll();
			 } // end of the try block
			 catch (SQLException e) 
			 {
				System.out.println("Error " + e);
			 }
			 // ensure everything is closed
		   finally 
		   {
		   }
	}
	else
	{
		out.print ("no access");
	}
   }
   else
   {
		out.print ("no sec");
   }

   %>
</table>
</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>