<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    filename: tnim2_Authorization_main_NIM_Authorization_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>
<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<base target=_self>
<%
//initial declaration of list class and parentID
    Integer        iPayerID        =    null;
    Integer        iCaseID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPayerID")&&pageControllerHash.containsKey("iCaseID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }
    else if (isScheduler&&pageControllerHash.containsKey("iCaseID")) 
    {
//        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }
    if (isScheduler) 
    {
//        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
%>


<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p class=title>Search Accounts<br>
<%
try
{

String myUserID = "";
String myLogonUserName = "";
String myContactFirstName = "";
String myContactLastName = "";
String myContactPhone = "";
String myContactEmail = "";
String myContactCity = "";
String myContactStateID = "0";
String myContactZIP = "";
String myAccountType = "";
String myPayerName = "";
String orderBy = "payername";
int startID = 0;
int maxResults = 10;
int iOpenID = 0;
String sOpenField = "";

boolean firstDisplay = false;

try
{
sOpenField= (request.getParameter("ssOpenField"));
//iOpenID= Integer.parseInt(request.getParameter("ssOpenID"));
maxResults = Integer.parseInt(request.getParameter("maxResults"));
startID = Integer.parseInt(request.getParameter("startID"));
//myUserID = request.getParameter("UserID").toLowerCase();
myLogonUserName = request.getParameter("LogonUserName").toLowerCase();
myContactFirstName = request.getParameter("ContactFirstName").toLowerCase();
myContactLastName = request.getParameter("ContactLastName").toLowerCase();
myContactPhone = request.getParameter("ContactPhone").toLowerCase();
myContactEmail = request.getParameter("ContactEmail").toLowerCase();
myContactCity = request.getParameter("ContactCity").toLowerCase();
myContactStateID = request.getParameter("ContactStateID").toLowerCase();
myContactZIP = request.getParameter("ContactZIP").toLowerCase();
myAccountType = request.getParameter("AccountType").toLowerCase();
myPayerName = request.getParameter("PayerName").toLowerCase();
orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
maxResults = 10;
startID = 0;
firstDisplay = true;
myUserID = "";
myLogonUserName = "";
myContactFirstName = "";
myContactLastName = "";
myContactPhone = "";
myContactEmail = "";
myContactCity = "";
myContactStateID = "0";
myContactZIP = "";
myAccountType = "";
myPayerName = "";
orderBy = "UserID";
}
if (orderBy == null)
{
maxResults = 10;
startID = 0;
firstDisplay = true;
myUserID = "";
myLogonUserName = "";
myContactFirstName = "";
myContactLastName = "";
myContactPhone = "";
myContactEmail = "";
myContactCity = "";
myContactStateID = "0";
myContactZIP = "";
myAccountType = "";
myPayerName = "";
orderBy = "UserID";
}

if (sOpenField.indexOf("PhysicianID")>0)
{
	iOpenID = 1;
}
else if (sOpenField.indexOf("Adjuster")>=0)
{
	iOpenID = 2;
}
else if (sOpenField.indexOf("Nurse")>=0)
{
	iOpenID = 4;
}
else if (sOpenField.indexOf("AID")>=0)
{
	iOpenID = 3;
}
else
{
//	out.println("Unrecognized Opening Field");
	iOpenID = 4;
}

//if (iOpenID!=3)
{
	%>
      <a href="tNIM3_UserAccount_create.jsp?ssOpenID=<%=iOpenID%>&ssOpenField=<%=sOpenField%>" target="_self">Create Record</a>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="#" onClick="javascript:window.returnValue=0; window.close()" >Remove Link</a>      
      </p>
<%
}
firstDisplay= false;
if (iOpenID>0)
{

%>


<table width=700 border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
<tr>
<td width=10>&nbsp;</td>
<td>
<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
<tr>
<td>

<form name="selfForm" method="POST" action="LI_tUserAccount_Search2.jsp">

  <table border=0 cellspacing=1 width='100%' cellpadding=2>
    <tr > 
      <td colspan=3 class=tdHeader><p>Search Here:</p></td>
      <td colspan=2 class=tdBase align=right><div id="dPrev"></div>
</td>
      <td colspan=1 class=tdBase> <div id="dNext"></div>
      </td>
      <td colspan=3 class=tdHeader nowrap> 
<input type=hidden name=ssOpenID value ="<%=iOpenID%>">
<input type=hidden name=ssOpenField value ="<%=sOpenField%>">
<input type=hidden name=maxResults value=<%=maxResults%> >
<input type=hidden name=startID value=0 ><p>Sort:
<select name=orderBy>
      <option value=ContactFirstName <%if (orderBy.equalsIgnoreCase("ContactFirstName")){out.println(" selected");}%> >First Name</option>
      <option value=ContactLastName <%if (orderBy.equalsIgnoreCase("ContactLastName")){out.println(" selected");}%> >Last Name</option>
      <option value=ContactStateID <%if (orderBy.equalsIgnoreCase("ContactStateID")){out.println(" selected");}%> >State</option>
      <option value=PayerName <%if (orderBy.equalsIgnoreCase("PayerName")){out.println(" selected");}%> >Company/Payer</option>
</select>
      </td> 
      <td class=tdHeader colspan=1> 
        <p  align="center"> 
          <input class="inputButton_md_Create" type="Submit" name="Submit" value="Submit" align="middle">
        </p>
      </td>
    </tr>
    <tr class=tdHeader> 
      <td  colspan=1 width=50>&nbsp; 

      </td>
      <td colspan=1> 
        <input type="text" name="PayerName" value="<%=myPayerName%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactFirstName" value="<%=myContactFirstName%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactLastName" value="<%=myContactLastName%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactPhone" value="<%=myContactPhone%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactEmail" value="<%=myContactEmail%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactCity" value="<%=myContactCity%>" size="10">
      </td>
      <td colspan=1> 
              <select name="ContactStateID">
                <jsp:include page="../generic/tStateLIShort.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myContactStateID%>" />
                </jsp:include>
              </select>
      </td>
      <td colspan=1> 
        <input type="text" name="ContactZIP" value="<%=myContactZIP%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="LogonUserName" value="<%=myLogonUserName%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="AccountType" value="<%=myAccountType%>" size="5">
      </td>
    </tr>
    <tr class=tdHeader> 
    <td>Action</td>
      <td colspan=1> 
        Linked to Payer
      </td>
      <td colspan=1>First</td>
      <td colspan=1> 
        Last</td>
      <td colspan=1> 
        Phone
      </td>
      <td colspan=1> 
        Email
      </td>
      <td colspan=1>City</td>
      <td colspan=1> 
        State
      </td>
      <td colspan=1> 
        ZIP</td>
      <td colspan=1> 
        Logon Name
      </td>
      <td colspan=1> 
        Type
      </td>
    </tr>



<%
db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(null);
String myWhere = "where Status in (1, 2, 3)  ";
if (iOpenID==1)
{
	myWhere += "AND  (AccountType in ('PhysicianID','PhysicianID3')  ";
}
else if (false&&iOpenID==2 && isScheduler)
{
	myWhere += "AND (AccountType in ('AdjusterID') ";
}
else if (iOpenID==2)
{
	bltNIM3_CaseAccount NCA = new bltNIM3_CaseAccount(iCaseID);
	myWhere += "AND (AccountType in ('AdjusterID','AdjusterID2','AdjusterID3') AND tNIM3_PayerMaster.PayerID =  " + NCA.getPayerID() + " ";
}
else if (iOpenID==3 && isScheduler)
{
	myWhere += "AND (AccountType in ('AdjusterID','AdjusterID2','AdjusterID3') ";
}
else if (iOpenID==4 && isScheduler)
{
	myWhere += "AND (ContactLastname>''  ";
}
boolean theFirst = true;

try
{
	if (!myLogonUserName.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myLogonUserName.indexOf("%")>=0)
		{
			myWhere += "lower(LogonUserName) LIKE ?";
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myLogonUserName));
		}
		else if (myLogonUserName.indexOf("=")==0)
		{
			myWhere += "lower(LogonUserName) = ?";
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myLogonUserName.substring(1, myLogonUserName.length())));
		}
		else if (myLogonUserName.length()<=3)
		{
			myWhere += "lower(LogonUserName) LIKE ?";
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myLogonUserName + "%"));
		}
		else 
		{
			myWhere += "lower(LogonUserName) LIKE ?";
            myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myLogonUserName + "%"));
		}
		theFirst = false;
	}
	if (!myContactFirstName.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myContactFirstName.indexOf("%")>=0)
		{
			myWhere += "lower(ContactFirstName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactFirstName));
		}
		else if (myContactFirstName.indexOf("=")==0)
		{
			myWhere += "lower(ContactFirstName) = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactFirstName.substring(1, myContactFirstName.length())));
		}
		else if (myContactFirstName.length()<=3)
		{
			myWhere += "lower(ContactFirstName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactFirstName + "%"));
		}
		else 
		{
			myWhere += "lower(ContactFirstName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactFirstName + "%"));
		}
		theFirst = false;
	}
	if (!myContactLastName.equalsIgnoreCase(""))
	{
	if (true||!theFirst) { myWhere+=" and ";}
		if (myContactLastName.indexOf("%")>=0)
		{
			myWhere += "lower(ContactLastName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactLastName));
		}
		else if (myContactLastName.indexOf("=")==0)
		{
			myWhere += "lower(ContactLastName) = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactLastName.substring(1, myContactLastName.length())));
		}
		else if (myContactLastName.length()<=3)
		{
			myWhere += "lower(ContactLastName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactLastName + "%"));
		}
		else 
		{
			myWhere += "lower(ContactLastName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactLastName + "%"));
		}
		theFirst = false;
	}
	if (!myContactPhone.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myContactPhone.indexOf("%")>=0)
		{
			myWhere += "lower(ContactPhone) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactPhone));
		}
		else if (myContactPhone.indexOf("=")==0)
		{
			myWhere += "lower(ContactPhone) = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactPhone.substring(1, myContactPhone.length())));
		}
		else if (myContactPhone.length()<=3)
		{
			myWhere += "lower(ContactPhone) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactPhone + "%"));
		}
		else 
		{
			myWhere += "lower(ContactPhone) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactPhone + "%"));
		}
		theFirst = false;
	}
	if (!myContactEmail.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myContactEmail.indexOf("%")>=0)
		{
			myWhere += "lower(ContactEmail) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactEmail));
		}
		else if (myContactEmail.indexOf("=")==0)
		{
			myWhere += "lower(ContactEmail) = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactEmail.substring(1, myContactEmail.length())));
		}
		else if (myContactEmail.length()<=3)
		{
			myWhere += "lower(ContactEmail) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactEmail + "%"));
		}
		else 
		{
			myWhere += "lower(ContactEmail) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactEmail + "%"));
		}
		theFirst = false;
	}
	if (!myContactCity.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myContactCity.indexOf("%")>=0)
		{
			myWhere += "lower(ContactCity) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactCity));
		}
		else if (myContactCity.indexOf("=")==0)
		{
			myWhere += "lower(ContactCity) = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactCity.substring(1, myContactCity.length())));
		}
		else if (myContactCity.length()<=3)
		{
			myWhere += "lower(ContactCity) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactCity + "%"));
		}
		else 
		{
			myWhere += "lower(ContactCity) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactCity + "%"));
		}
		theFirst = false;
	}
	if (!myContactStateID.equalsIgnoreCase("0"))
	{
		if (!myContactStateID.equalsIgnoreCase(""))
		{
			if (true||!theFirst) { myWhere+=" and ";}
			myWhere += "ContactStateID = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myContactStateID));
			theFirst = false;
		}
	}
	if (!myContactZIP.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myContactZIP.indexOf("%")>=0)
		{
			myWhere += "lower(ContactZIP) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactZIP));
		}
		else if (myContactZIP.indexOf("=")==0)
		{
			myWhere += "lower(ContactZIP) = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactZIP.substring(1, myContactZIP.length())));
		}
		else if (myContactZIP.length()<=3)
		{
			myWhere += "lower(ContactZIP) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactZIP + "%"));
		}
		else 
		{
			myWhere += "lower(ContactZIP) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactZIP + "%"));
		}
		theFirst = false;
	}
	if (!myAccountType.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myAccountType.indexOf("%")>=0)
		{
			myWhere += "lower(AccountType) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myAccountType));
		}
		else if (myAccountType.indexOf("=")==0)
		{
			myWhere += "lower(AccountType) = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myAccountType.substring(1, myAccountType.length())));
		}
		else if (myAccountType.length()<=3)
		{
			myWhere += "lower(AccountType) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myAccountType + "%"));
		}
		else 
		{
			myWhere += "lower(AccountType) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myAccountType + "%"));
		}
		theFirst = false;
	}
	if (!myPayerName.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myPayerName.indexOf("%")>=0)
		{
			myWhere += "lower(PayerName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPayerName));
		}
		else if (myPayerName.indexOf("=")==0)
		{
			myWhere += "lower(PayerName) = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPayerName.substring(1, myPayerName.length())));
		}
		else if (myPayerName.length()<=3)
		{
			myWhere += "lower(PayerName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPayerName + "%"));
		}
		else 
		{
			myWhere += "lower(PayerName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myPayerName + "%"));
		}
		theFirst = false;
	}
	myWhere += ") AND (tUserAccount.PayerID>0)";
	
	//System.out.println(myWhere);
	if (theFirst||myWhere.equalsIgnoreCase(")"))
	{
	//	myWhere = "where (AccountType in ('AdjusterID','PhysicianID'))";
	//	myWhere += ")";
	}
	

}
catch(Exception e)
{
out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;
try
{
String mysql=("select tUserAccount.*, PayerName from tUserAccount INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.PayerID = tUserAccount.PayerID " + myWhere + " order by " + orderBy + " LIMIT " + (maxResults) + " OFFSET " + startID + "");
//out.print(mysql);
//	out.print("<hr>"+myDPSO.printOutSQLParameters() );
myDPSO.setSQL(mysql);
myRS = mySS.executePreparedStatement(myDPSO);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
cnt++;
//if (cnt>=startID&&cnt<=startID+maxResults)
if (cnt<=maxResults)
{
cnt2++;

String myClass = "tdBase";
if (cnt2%2==0)
{
myClass = "tdBaseAlt";
}
int userStatus = new Integer(myRS.getString("Status"));
if (userStatus==3)
{
	out.print("<tr class=\"tdBaseAltVoid\">");
} else {
	out.print("<tr class="+myClass+">");
}
if (iOpenID>0)
{
	if (userStatus==3){
		out.print("<td><input class=\"inputButton_md_Create\" onClick = \"javascript:if (confirm('This user has been archived - which means they do not work at this branch any more.\\n\\n Are you sure that you want to select this person?')){window.returnValue = '" + myRS.getString("UserID") + "';window.close();}\" type=\"Button\" name=\"Select\" value=\"Select\" align=\"middle\"></td>");
	} else {
		out.print("<td><input class=\"inputButton_md_Create\" onClick = \"javascript:window.returnValue = '" + myRS.getString("UserID") + "';window.close();\" type=\"Button\" name=\"Select\" value=\"Select\" align=\"middle\"></td>");
	}
}
else
{
	out.print("<td><a href=\"#\">Select Error</a></td>");
}
out.print("<td nowrap>");
out.print(myRS.getString("PayerName")+"");
out.print("</td>");
out.print("<td>");
out.print(myRS.getString("ContactFirstName")+"");
out.print("</td>");
out.print("<td>");
out.print(myRS.getString("ContactLastName")+"");
if (userStatus==3){
	out.print(" (Archived)");
}
out.print("</td>");
out.print("<td>");
out.print(myRS.getString("ContactPhone")+"");
out.print("</td>");
out.print("<td>");
out.print(myRS.getString("ContactEmail")+"");
out.print("</td>");
out.print("<td nowrap>");
out.print("Office: <strong>" + myRS.getString("CompanyName")+"</strong><br>");
out.print(myRS.getString("ContactCity")+"");
out.print("</td>");
out.print("<td>");
out.print((new bltStateLI (new Integer(myRS.getString("ContactStateID")) ).getShortState()+""));
out.print("</td>");
out.print("<td>");
out.print(myRS.getString("ContactZIP")+"");
out.print("</td>");
out.print("<td>");
out.print(myRS.getString("LogonUserName")+"");
out.print("</td>");
out.print("<td>");
out.print(myRS.getString("AccountType")+"");
out.print("</td>");
out.print("</tr>");
}
   }
mySS.closeAll();
endCount = cnt;



if (startID>=endCount)
{
//startID = endCount-maxResults;

}

if (maxResults<=0)
{
maxResults=10;
}


if (startID<=0)
{
startID=0;
}

//if ((startID+maxResults)<endCount)
if (endCount>=maxResults)
{
%>
    	<script language="javascript">
//		alert("next");
        document.getElementById('dNext').innerHTML='<a href="LI_tUserAccount_Search2.jsp?startID=<%=(startID+maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&UserID=<%=myUserID%>&LogonUserName=<%=myLogonUserName%>&ContactFirstName=<%=myContactFirstName%>&ContactLastName=<%=myContactLastName%>&ContactPhone=<%=myContactPhone%>&ContactEmail=<%=myContactEmail%>&ContactCity=<%=myContactCity%>&ContactStateID=<%=myContactStateID%>&ContactZIP=<%=myContactZIP%>&AccountType=<%=myAccountType%>&PayerName=<%=myPayerName%>&ssOpenField=<%=sOpenField%>"> next >> <%=maxResults%></a>         ';
        </script>
        <%
}

if (startID>0)
{
	%>
    	<script language="javascript">
        document.getElementById('dPrev').innerHTML='<a href="LI_tUserAccount_Search2.jsp?startID=<%=(startID-maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&UserID=<%=myUserID%>&LogonUserName=<%=myLogonUserName%>&ContactFirstName=<%=myContactFirstName%>&ContactLastName=<%=myContactLastName%>&ContactPhone=<%=myContactPhone%>&ContactEmail=<%=myContactEmail%>&ContactCity=<%=myContactCity%>&ContactStateID=<%=myContactStateID%>&ContactZIP=<%=myContactZIP%>&AccountType=<%=myAccountType%>&PayerName=<%=myPayerName%>&ssOpenField=<%=sOpenField%>"><< previous <%=maxResults%></a> ';
        </script>
          <%
}
%>

</table> 


<%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>



  </table>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </td>
  </tr>
</table>

<%



  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_ClearID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
