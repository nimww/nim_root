<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    filename: tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_200/style_PayerID.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td><p class=title>Live Status</p>

    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM3_CaseAccount")%>



<%
//initial declaration of list class and parentID
    Integer        iPayerID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
    }
	  boolean isProvider = false;
	  boolean isAdjuster = false;
	  boolean isPayer = false;
	
  //page security
  if (accessValid)
  {
		if (CurrentUserAccount.getAccountType().equalsIgnoreCase("PhysicianID")) 
		{
			isProvider=true;
		}
		else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("AdjusterID")) 
		{
			isAdjuster=true;
		}
		else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("PayerAdminID")) 
		{
			isPayer=true;
		}
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
		pageControllerHash.remove("iCaseID");
		pageControllerHash.put("sINTNext","tNIM3_CaseAccount_PayerID_query.jsp");
		session.setAttribute("pageControllerHash",pageControllerHash);



searchDB2 mySS = new searchDB2();
java.sql.ResultSet myRS = null;;

try
{
	String mySQL = "null";
	String mySQL2 = "null";
	if (isAdjuster&&isScheduler)
	{	
		mySQL = "SELECT tnim3_payermaster.payername, tnim3_caseaccount.caseclaimnumber, tnim3_caseaccount.patientfirstname, tnim3_caseaccount.patientlastname, tnim3_caseaccount.patienthomephone, tnim3_caseaccount.patientworkphone, tnim3_caseaccount.patientcellphone, tnim3_referral.receivedate, tnim3_referral.preauthorizationconfirmation, tnim3_encounter.reportfileid, tnim3_referral.rxfileid, tnim3_encounter.scanpass, tnim3_appointment.providerid, tnim3_appointment.appointmenttime, tpracticemaster.practicename, tpracticemaster.officeaddress1, tpracticemaster.officecity, tstateli.shortstate, tpracticemaster.officezip, tpracticemaster.officephone, tpracticemaster.officeemail, tnim3_referral.uniquecreatedate, tcaseaccountstatusli.casestatusshort, tnim3_encounter.dateofservice, tnim3_encounter.rec_bill_pro, tnim3_encounter.sentto_bill_pro, tnim3_encounter.rec_bill_pay, tnim3_encounter.sentto_bill_pay, tnim3_encounter.sentto_sp_ic, tnim3_encounter.sentto_sp_adj, tnim3_encounter.sentto_sp_refdr, tnim3_encounter.senttoic, tnim3_encounter.senttoadj, tnim3_encounter.senttorefdr FROM (((((((tnim3_caseaccount JOIN tnim3_referral ON ((tnim3_referral.caseid = tnim3_caseaccount.caseid))) JOIN tnim3_encounter ON ((tnim3_encounter.referralid = tnim3_referral.referralid))) JOIN tnim3_payermaster ON ((tnim3_caseaccount.payerid = tnim3_payermaster.payerid))) JOIN tcaseaccountstatusli ON ((tcaseaccountstatusli.casestatusid = tnim3_caseaccount.casestatusid))) LEFT JOIN tnim3_appointment ON ((tnim3_encounter.appointmentid = tnim3_appointment.appointmentid))) LEFT JOIN tpracticemaster ON ((tnim3_appointment.providerid = tpracticemaster.practiceid))) LEFT JOIN tstateli ON ((tstateli.stateid = tpracticemaster.officestateid))) WHERE ((tnim3_caseaccount.casestatusid = ANY (ARRAY[0, 1, 3, 5])) AND (((((((tnim3_appointment.appointmenttime IS NULL) OR (tnim3_appointment.appointmenttime < '2000-01-01 00:00:00'::timestamp without time zone)) OR ((tnim3_appointment.appointmenttime > '2000-01-01 00:00:00'::timestamp without time zone) AND (((tnim3_encounter.sentto_sp_adj < '2000-01-01 00:00:00'::timestamp without time zone) OR (tnim3_encounter.sentto_sp_ic < '2000-01-01 00:00:00'::timestamp without time zone)) OR (tnim3_encounter.sentto_sp_refdr < '2000-01-01 00:00:00'::timestamp without time zone)))) OR ((tnim3_encounter.reportfileid > 0) AND ((tnim3_encounter.senttoadj < '2000-01-01 00:00:00'::timestamp without time zone) OR (tnim3_encounter.senttorefdr < '2000-01-01 00:00:00'::timestamp without time zone)))) OR (tnim3_referral.rxfileid = 0)) OR ((tnim3_appointment.appointmenttime <= '2009-11-06 15:38:36.50'::timestamp without time zone) AND (tnim3_encounter.dateofservice <= '1900-01-01 00:00:00'::timestamp without time zone))) OR (((tnim3_encounter.reportfileid = 0) AND (tnim3_encounter.dateofservice > '2000-01-01 00:00:00'::timestamp without time zone)) AND ((tnim3_encounter.dateofservice + '24:00:00'::interval) <= now())))) ORDER BY tnim3_caseaccount.patientlastname;";
	}
//out.println(mySQL);
	myRS = mySS.executeStatement(mySQL);
}
catch(Exception e)
{
	out.println("ResultsSet:"+e);
}

try
{
%>
	<table width="100%" border="1" cellspacing="1" cellpadding="5" class="tableBorderDot" >
<%	
	int cnt=0;
	while (myRS!=null&&myRS.next())
	{
		String pm_payername = myRS.getString ("payername");
		String ca_caseclaimnumber = myRS.getString ("caseclaimnumber");
		String ca_patientfirstname = myRS.getString ("patientfirstname");
		String ca_patientlastname = myRS.getString ("patientlastname");
		String ca_patienthomephone = myRS.getString ("patienthomephone");
		String ca_patientworkphone = myRS.getString ("patientworkphone");
		String ca_patientcellphone = myRS.getString ("patientcellphone");
		String r_receivedate = myRS.getString ("receivedate");
		String r_preauthorizationconfirmation = myRS.getString ("preauthorizationconfirmation");
		String e_reportfileid = myRS.getString ("reportfileid");
		String r_rxfileid = myRS.getString ("rxfileid");
		String e_scanpass = myRS.getString ("scanpass");
		String a_providerid = myRS.getString ("providerid");
		String a_appointmenttime = myRS.getString ("appointmenttime");
		String pracm_practicename = myRS.getString ("practicename");
		String pracm_officeaddress1 = myRS.getString ("officeaddress1");
		String pracm_officecity = myRS.getString ("officecity");
		String tsli_shortstate = myRS.getString ("shortstate");
		String pracm_officezip = myRS.getString ("officezip");
		String pracm_officephone = myRS.getString ("officephone");
		String pracm_officeemail = myRS.getString ("officeemail");
		String r_uniquecreatedate = myRS.getString ("uniquecreatedate");
		String cas_licasestatusshort = myRS.getString ("casestatusshort");
		String e_dateofservice = myRS.getString ("dateofservice");
		String e_rec_bill_pro = myRS.getString ("rec_bill_pro");
		String e_sentto_bill_pro = myRS.getString ("sentto_bill_pro");
		String e_rec_bill_pay = myRS.getString ("rec_bill_pay");
		String e_sentto_bill_pay = myRS.getString ("sentto_bill_pay");
		String e_sentto_sp_ic = myRS.getString ("sentto_sp_ic");
		String e_sentto_sp_adj = myRS.getString ("sentto_sp_adj");
		String e_sentto_sp_refdr = myRS.getString ("sentto_sp_refdr");
		String e_senttoic = myRS.getString ("senttoic");
		String e_senttoadj = myRS.getString ("senttoadj");
		String e_senttorefdr = myRS.getString ("senttorefdr");
		String eMessage = "";
//		out.print("<hr>" + a_appointmenttime  + "<hr>");
		java.util.Calendar c1 = java.util.Calendar.getInstance(); 
//		out.println("Date + 1 days is : " + (c1.getTime()));
		String sClass = "tdBaseAlt";

		if (a_appointmenttime==null||a_appointmenttime.equalsIgnoreCase("")||a_appointmenttime.equalsIgnoreCase("null")||dbdf.parse(a_appointmenttime).before (new java.util.Date(2000,1,1)) )
		{
				eMessage += "<hr>Need to Schedule";
				sClass = "tdBaseAltYellow";
		}
		else if (dbdf.parse(a_appointmenttime).after (new java.util.Date(2000,1,1)) && (dbdf.parse(e_sentto_sp_ic).before (new java.util.Date(2000,1,1)) || dbdf.parse(e_sentto_sp_adj).before (new java.util.Date(2000,1,1)) || dbdf.parse(e_sentto_sp_refdr).before (new java.util.Date(2000,1,1))      ) )
		{
				eMessage += "<hr>Need to Send SP Alerts";
				sClass = "tdBaseAltYellow";
		}
		else if ( dbdf.parse(a_appointmenttime).before (c1.getTime()) &&  dbdf.parse(e_dateofservice).before (new java.util.Date(1900,1,1))     )
		{
				eMessage += "<hr>Confirm Patient showed up at appointment";
				sClass = "tdBaseAltOrange";
		}
		if ( (new Integer(e_reportfileid).intValue() > 0) && (dbdf.parse(e_senttoadj).before (new java.util.Date(2000,1,1)) || dbdf.parse(e_senttorefdr).before (new java.util.Date(2000,1,1))      ) )
		{
				eMessage += "<hr>Need to Send Report";
				sClass = "tdBaseAltYellow";
		}
		if (r_rxfileid.equalsIgnoreCase("0"))
		{
				eMessage += "<hr>Missing RX";
				sClass = "tdBaseAltRed";
		}
		c1.add(java.util.Calendar.DATE,1);
		if (  new Integer(e_reportfileid).intValue() == 0 &&  dbdf.parse(e_dateofservice).after (new java.util.Date(2000,1,1))  &&  dbdf.parse(e_dateofservice).after( c1.getTime() )     )
		{
				eMessage += "<hr>Late Report (24 hours past appointment)";
				sClass = "tdBaseAltOrange";
		}
		String d_ca_patientlastname = "";
		if (ca_patientlastname.length()<1)
		{
			d_ca_patientlastname = "[Missing]";
		}
		else
		{
			d_ca_patientlastname = ca_patientlastname.substring(0,1);
		}
		%>
          <tr class="<%=sClass%>">
            <td class="tableBorderDot"><%=pm_payername%>&nbsp;</td>
            <td class="tableBorderDot"><a href="tNIM3_CaseAccount_Encounter_PayerID_query.jsp?maxResults=5&startID=0&orderBy=PatientLastName&CaseStatusID=0&open=-All+Act&CaseClaimNumber=<%=ca_caseclaimnumber%>&ScanPass=&PayerID=-1&PatientFirstName=&PatientLastName=&EmployerName="><%=ca_caseclaimnumber%>&nbsp;</a></td>
            <td class="tableBorderDot"><%=ca_patientfirstname%>&nbsp;<%=d_ca_patientlastname%></td>
            <td class="tableBorderDot"><%=PLCUtils.getDisplayDateWithTime(dbdf.parse(r_receivedate))%>&nbsp;</td>
            <td class="tableBorderDot"><%=eMessage%>&nbsp;</td>
          </tr>
        <%	
	}
%>
	</table>
<%	
}
catch(Exception e)
{
	out.println("Display:"+e);
}

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>