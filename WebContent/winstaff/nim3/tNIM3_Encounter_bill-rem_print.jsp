<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="java.text.DecimalFormat ,com.winstaff.*" %>
<%/*

    filename: out\jsp\tmyEO.NIM3_Encounter_form.jsp
    Created on May/28/2008
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<%@ include file="../generic/generalDisplay.jsp" %>


<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   //Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (isScheduler3)
   {

        iEncounterID        =   new Integer(request.getParameter("EDITID"));
        accessValid = true;    
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_Encounter_form.jsp");
//initial declaration of list class and parentID








  pageControllerHash.put("sLocalChildReturnPage","tNIM3_Encounter_form.jsp");
//initial declaration of list class and parentID
//    bltmyEO.NIM3_Encounter        myEO.NIM3_Encounter        =    null;
		NIM3_EncounterObject2 myEO2 = null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit")||request.getParameter( "EDIT" ).substring(0,5).equalsIgnoreCase("print")||request.getParameter( "EDIT" ).equalsIgnoreCase("sched") )
    {
        myEO2        =    new    NIM3_EncounterObject2(iEncounterID,"Bill Remit");
    }



            java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
            java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
            java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
            java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
            java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);
			java.util.Date dbAppointment = myEO2.getNIM3_Appointment().getAppointmentTime();
			String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
			String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
			String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
			String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
			String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
			String sAppointmentHour = displayDateHour.format((dbAppointment));




//fields
        %>
          <%  String theClass ="tdBase";%>
    <table cellpadding=0 cellspacing=0 border=0 width=100% >
    <tr class=title>
      <td width="10">&nbsp;</td>
      <td><table width="100%" border="0" class="certborderPrint" cellspacing="0" cellpadding="10">
        <tr>
          <td><table width="100%" border="2" cellpadding="5" cellspacing="0" class = "certborderPrintInt">
            <tr >
              <td colspan="2" valign="top" class="big3" style="font-size: 25px;"><p style="position: relative;top: -20px;height:70px"><img src="images/logo1.jpg" width="250" height="70" />
                 <span style="position: relative;top: -20px;left: 20px;">Remittance</span></p></td>
              </tr>
            <tr>
              <td colspan="2" valign="top"><font size="4">ScanPass: <strong><%=myEO2.getNIM3_Encounter().getScanPass()%></strong></font><br>
              
              <%if(!myEO2.getNIM3_Encounter().getHCFAinvoice().equals("")){ %>
              HCFA Invoice #: <%=myEO2.getNIM3_Encounter().getHCFAinvoice()%>
              <%} %>
              
              </td>
              </tr>
            <tr>
              <td colspan="2" valign="top">Patient Information</td>
            </tr>
            <tr>
              <td colspan="2" valign="top"><p>Patient: <strong><%=myEO2.getNIM3_CaseAccount().getPatientFirstName()%> <%=myEO2.getNIM3_CaseAccount().getPatientLastName()%></strong><br />
                  <strong><%=myEO2.getNIM3_CaseAccount().getPatientAddress1()%> <%=myEO2.getNIM3_CaseAccount().getPatientAddress2()%><br />
  <%=myEO2.getNIM3_CaseAccount().getPatientCity()%>, <%=new bltStateLI(myEO2.getNIM3_CaseAccount().getPatientStateID()).getShortState()%> <%=myEO2.getNIM3_CaseAccount().getPatientZIP()%></strong><p>Gender: <strong><%=(myEO2.getNIM3_CaseAccount().getPatientGender())%></strong><br />
                DOB: <strong><%=displayDateSDF.format(myEO2.getNIM3_CaseAccount().getPatientDOB())%></strong><br />
                <br />         
                Date of Injury: <strong><%if (myEO2.getNIM3_CaseAccount().getDateOfInjury().after(NIMUtils.getBeforeTime())){out.print(displayDateSDF.format(myEO2.getNIM3_CaseAccount().getDateOfInjury()));}else{out.print("Unknown");}%></strong><br />
                Employer: <strong><%=myEO2.getNIM3_CaseAccount().getEmployerName()%></strong></p></td>
            </tr>
            <tr>
              <td colspan="2" valign="top">
              
<table border="1" cellpadding="3" cellspacing="0">
        <tr class="tdHeaderAlt">
          <td>Date Of Service</td>
        <td>CPT</td>
        <td>Mod</td>
          <td>Amount</td>
          <td>Desc</td>
        <td>Diagnosis/ICD</td>
        </tr>
        <%

			searchDB2 mySS = new searchDB2();
			
			java.sql.ResultSet myRS = null;;
			
			try
			{
				String mySQL = ("select * from tNIM3_Service where EncounterID = " + myEO2.getNIM3_Encounter().getEncounterID()  + " order by ServiceID");
				//out.println(mySQL);
				myRS = mySS.executeStatement(mySQL);
			}
			catch(Exception e)
			{
				out.println("ResultsSet:"+e);
			}
			
			String myMainTable= " ";
			int endCount = 0;
			int cnt=0;
			int cnt2=0;
			while (myRS!=null&&myRS.next())
			{
				cnt++;
				bltNIM3_Service working_bltNIM3_Service  =  new bltNIM3_Service(new Integer(myRS.getString("ServiceID")));
				%>
                <tr>
                  <td><%=displayDateSDF.format(myEO2.getNIM3_Encounter().getDateOfService())%></td>
                	<td><%=working_bltNIM3_Service.getCPT()%></td>
                	<td><%=working_bltNIM3_Service.getCPTModifier()%></td>
                    <td>$<%=working_bltNIM3_Service.getPaidOutAmount()%></td>
                    <td><%=NIMUtils.getCPTText(working_bltNIM3_Service.getCPT())%><br />
                      BP: <%=NIMUtils.getCPTText(working_bltNIM3_Service.getCPTBodyPart())%></td>
                    <td><strong><%=working_bltNIM3_Service.getdCPT1()%></strong> <%=working_bltNIM3_Service.getdCPT2()%> <strong><%=working_bltNIM3_Service.getdCPT3()%></strong> <%=working_bltNIM3_Service.getdCPT4()%>&nbsp;</td>
                  </tr>
                <%
			}
		%>
        </table>
        
        <%
		if (myEO2.getNIM3_Encounter().getEncounterTypeID()==NIMUtils.ENCOUNTER_TYPE_EMG)
		{
			%>
				<br />        
		<strong><h4>*Paid according to AANEM guidelines</h4> </strong>
			<%
		}
		%>
              
  </td>
            </tr>
            <tr>
              <td colspan="2" valign="top"><p>Place of Service:</p>
  <strong><%=myEO2.getAppointment_PracticeMaster().getPracticeName()%></strong><br />
                <%=myEO2.getAppointment_PracticeMaster().getOfficeAddress1()%><br />
                <%=myEO2.getAppointment_PracticeMaster().getOfficeCity()%>, <%=new bltStateLI(myEO2.getAppointment_PracticeMaster().getOfficeStateID()).getShortState()%> <%=myEO2.getAppointment_PracticeMaster().getOfficeZIP()%><br />                Phone: <%=myEO2.getAppointment_PracticeMaster().getOfficePhone()%><br />
                
  <%=myEO2.getAppointment_PracticeMaster().getPracticeID()%>
</td>
              </tr>
            <tr>
              <td valign="top" >
<%if (myEO2.getNIM3_Encounter().getisCourtesy()!=1)
{
	%>
              
              <p><strong>NextImage Medical, Inc.</strong><br />
3390 Carmel Mountain Rd. Suite 150<br />
San Diego, CA 92121</p>
<strong>Phone:</strong> 888-318-5111 | <strong>Fax:</strong> 858-430-45-72<br />
<strong>NPI  No.:</strong> 1881861359<br />
<strong>Tax ID&nbsp;:</strong> 205673072
<%
}
else
{
	%>
<p>Bill To Insurance Information:</p>
<% 
NIM3_AddressObject N3AO = myEO2.getWhoShouldIBillTo();
%>
                <p><strong><%=N3AO.getAddressName()%></strong><br />
                  <%=N3AO.getAddress1()%><br />
                  <%=N3AO.getAddress2()%><br />
                  <%=N3AO.getCity()%>, <%=N3AO.getState()%> <%=N3AO.getZIP()%>

                  
                  
                  <br />
                  WC Claim #: <%=myEO2.getNIM3_CaseAccount().getCaseClaimNumber()%><br />                  
                Authorization #: <%=myEO2.getNIM3_Referral().getPreAuthorizationConfirmation()%><br />
Phone:<%=N3AO.getPhone()%><br />
Fax: <%=N3AO.getFax()%></p>
                <p>&nbsp;</p>    
    <%
}
%>

</td>
              <td valign="top" >Referring Physician: <strong><%=myEO2.getReferral_ReferringDoctor().getContactFirstName()%> <%=myEO2.getReferral_ReferringDoctor().getContactLastName()%><br />Phone: <strong><%=myEO2.getReferral_ReferringDoctor().getContactPhone()%></strong><br />
                Fax: <strong><%=myEO2.getReferral_ReferringDoctor().getContactFax()%></strong></strong>&nbsp;</td>
            </tr>
            <tr>
              <td valign="top">

</td>
              <td valign="top"></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</table>
<script language="javascript">window.print();</script>


