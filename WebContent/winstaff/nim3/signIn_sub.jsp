<%@page language="java" import="com.winstaff.*, com.winstaff.password.Encrypt" %>
<%
String ip = request.getRemoteAddr();
String host = request.getRemoteHost();
session.setAttribute("pageControllerHash",null);
int userID = 0;
if (true)
{
	String myUserName="";
	String myPassword="";
	String myRoute="";
Integer UID = null;
try
{
	myUserName = DataControlUtils.fixApostrophe(request.getParameter("LogonUserName"));
	myPassword = request.getParameter("LogonUserPassword");
	myRoute = request.getParameter("sendMe");
}
catch(Exception e)
{
}

boolean isPassed = true;
try
{

searchDB2 mySS = new searchDB2();
db_PreparedStatementObject myDPSO = new db_PreparedStatementObject("select * from tUserAccount where UPPER(LogonUserName)= UPPER(?)");
myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myUserName));
java.sql.ResultSet myRS = mySS.executePreparedStatement(myDPSO);
//out.println("=================================="+ConfigurationInformation.sURL+"==================================");
com.winstaff.password.Encrypt myEnc = new com.winstaff.password.Encrypt();
System.out.println("test:"+myEnc.getMD5Base64(myPassword));
if (myRS!=null&&myRS.next())
{
	String thePassword = myRS.getString("LogonUserPassword");
//	if ( (theStatus!=-1)&&(Encrypt.checkPasswords(thePassword, myEnc.getMD5Base64(myPassword))) )
	Integer UserStatus = new Integer(myRS.getString("Status"));
	if ( (UserStatus.intValue()>0)&&(Encrypt.checkPasswords(thePassword, myEnc.getMD5Base64(myPassword))) )
	{
		UID = new Integer(myRS.getString("UserID"));
		//out.println("UID = " + UID);
		out.println("Redirecting...");
	}
	else
	{
		isPassed = false;
	}
}
else
{
		isPassed = false;
}



mySS.closeAll();

}
catch(Exception e)
{
out.println(e);
}


if (!isPassed)
{
bltUATransaction myUTA = new bltUATransaction();
myUTA.setTransactionDate(PLCUtils.getNowDate(false));
myUTA.setActionID(new Integer(2));
myUTA.setComments("Failed Login [" + myUserName + "] - host: " + "[" + host + "]");
myUTA.setRemoteIP(ip);
myUTA.setUniqueCreateDate(PLCUtils.getNowDate(false));
myUTA.setUniqueModifyComments("System Generated");
myUTA.commitData();

response.sendRedirect("signIn_Error.jsp");
}
else
{


bltUATransaction myUTA = new bltUATransaction();
myUTA.setTransactionDate(PLCUtils.getNowDate(false));
myUTA.setActionID(new Integer(1));
myUTA.setComments("Successful Login - host: " + "[" + host + "]");
myUTA.setRemoteIP(ip);
myUTA.setUserID(UID);
myUTA.setUniqueCreateDate(PLCUtils.getNowDate(false));
myUTA.setUniqueModifyComments("System Generated");
myUTA.commitData();
try
{
	java.util.Hashtable pageControllerHash = new java.util.Hashtable();

	bltUserAccount UA = new bltUserAccount(UID);
	pageControllerHash.put("i"+UA.getAccountType(),UA.getReferenceID());
	//Payer Add On
	if (UA.getPayerID()>0)
	{
		pageControllerHash.put("iPayerID",UA.getPayerID());
	}
	userID = UA.getUserID();
	pageControllerHash.put("iCurrentUserID",UA.getUserID());
	pageControllerHash.put("plcID",String.valueOf(UA.getPLCID()));
	//out.println("<Br>PLCID" + UA.getPLCID());
	UA.setPLCID(new Integer(200));
	//out.println("<Br>PLCID_after" + UA.getPLCID());
	java.text.SimpleDateFormat umcD = new java.text.SimpleDateFormat("MM/dd/yyyy h:mm a");
	pageControllerHash.put("UserLogonDescription",UA.getLogonUserName()+"["+UA.getUserID()+"]["+umcD.format(PLCUtils.getNowDate(false))+"]");
//test/temp code for first NY implementation.  This should be moved to DB
	pageControllerHash.put("signOffPageRoute","ui_"+UA.getPLCID()+"/signOff.jsp");
	

        String sStart = UA.getStartPage();
        if (myRoute ==null||myRoute.equalsIgnoreCase("null")||myRoute.equalsIgnoreCase(""))
        {
        }
        else
        {
	        sStart = myRoute;
        }
	pageControllerHash.put("sStartPage",sStart);
	pageControllerHash.put("UserSecurityGroupID",UA.getSecurityGroupID());
	pageControllerHash.put("iGenericSecurityGroupID",UA.getGenericSecurityGroupID());
//	pageControllerHash.put("iCompanyID",new Integer("1"));


	//out.println("<Br>SecGID" + UA.getSecurityGroupID());
	bltSecurityGroupMaster mySG = new bltSecurityGroupMaster(UA.getSecurityGroupID());
        pageControllerHash.put("isShowAudit",mySG.getShowAuditID());

Boolean isBasic = new Boolean(false);

        if (UA.getCreditCardFullName().equalsIgnoreCase("Basic"))
	{
		isBasic = new Boolean(true);			
	}

//if Physician
if (UA.getAccountType().equalsIgnoreCase("PhysicianID"))
{
	if (DataControlUtils.isAdminUpgraded(UA.getReferenceID()))
	{
		isBasic = new Boolean(true);			
	}
    
}
		pageControllerHash.put("isBasic",isBasic);

	session.setAttribute("pageControllerHash",pageControllerHash);
%>

<%if (UA.getAccountType().contains("AdjusterID") && !(new bltNIM3_PayerMaster(UA.getPayerID()).getParentPayerID()==1026 || new bltNIM3_PayerMaster(UA.getPayerID()).getParentPayerID()==394)){%>

<form id="frm1" action="/NIM_Adj_Portal/home" method="post">
<input type="hidden" name="user_id" value="<%=UA.getUserID()%>"/>
</form>
<script language=javascript>
document.getElementById("frm1").submit();
</script>
<%} else{%>
<script language=javascript>
features = 'resizable=yes,width=350,height=300';
theURL = 'signOn_message.jsp';
winName = 'signON';
//window.open(theURL,winName,features);
<%
if (UA.getAccountType().equalsIgnoreCase("GenAdminID"))
{%>
	parent.document.location = '/winstaff/app/<%="ui_"+UA.getPLCID() + "/ui_init.jsp"%>';
<%}
else{%>
	parent.document.location = '<%="ui_"+UA.getPLCID() + "/ui_init.jsp"%>';
<%}%>
</script>

<%}%>
<%
	//response.sendRedirect("ui_"+UA.getPLCID() + "/ui_init.jsp");
	
}
catch(Exception e)
{
out.println(e);
}



}
	

}

else
{
%>
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="2" height="100%">
  <tr> 
    <td width="100%" height="100%"> 
      <div align="center"> 
        <table width="400" border="1" cellspacing="0" cellpadding="5" bordercolor="#333333" class="tableColor">
          <tr> 
    <td>
      <p><b><font face="Arial, Helvetica, sans-serif" size="4">We are currently running 
        routine maintenance on our system to ensure your data integrity. </font></b></p>
      <p><font face="Arial, Helvetica, sans-serif" size="3">We apologize for any 
        inconvenience. This site will be back up soon.</font></p>
              <p><font face="Arial, Helvetica, sans-serif" size="3">If you have any questions, 
                please feel free to call us at 888-318-5111</font></p>
<p align=center><img src="images/logo1.jpg" width="250" height="70"></p>
      </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>

<%
}
%>

