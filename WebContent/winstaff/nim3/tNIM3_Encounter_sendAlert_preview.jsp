<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<head>
<script type="text/javascript">

function escapeVal(textarea,replaceWith) {	//textarea is reference to that object, replaceWith is string that will replace the encoded return
	
	textarea = escape(textarea); //encode textarea string's carriage returns

		for(i=0; i<textarea.length; i++) { //loop through string, replacing carriage return encoding with HTML break tag
		
		 	if(textarea.indexOf("%0D%0A") > -1){ //Windows encodes returns as \r\n hex
				textarea=textarea.replace("%0D%0A",replaceWith);
			}
			else if(textarea.indexOf("%0A") > -1){  //Unix encodes returns as \n hex
				textarea=textarea.replace("%0A",replaceWith);
			}
			else if(textarea.indexOf("%0D") > -1){ // Macintosh encodes returns as \r hex
				textarea=textarea.replace("%0D",replaceWith);
			}
			
		}
		
	textarea.value=unescape(textarea.value); //unescape all other encoded characters
	return textarea;
}

function processButton(arg1,arg2,arg3) {

	confirmButton.disabled=true;
	
	if (confirm('Are you sure you want to send this alert?')){		
		var formatted = escapeVal(arg2,'<br>');
		var str1 = "tNIM3_Encounter_sendAlert_confirm.jsp?EDIT=";
		var result = str1.concat(arg1,"&APPEND='",arg2,"'","&customEmail='",arg3,"'");		
		document.location=result;		
	}    
}
</script>

</head>

<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
<%@ include file="../generic/generalDisplay.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
    <style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->

</style>
<!-- <script type="text/javascript" src="js/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script> -->

<table cellpadding=0 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td><hr></td></tr>
    <tr><td width=10>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM_Encounter")%>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat("MM/dd/yyyy");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
		String sFlag = request.getParameter("EDIT");
		emailType_V3 myETSend = NIMUtils.getAlertEmail(iEncounterID, sFlag);		
		String customEmail = request.getParameter("customEmail");	
		
		if (customEmail.compareToIgnoreCase("true")==0) {
		%>
   		</td></tr>
   		<tr>
    	 <td>&nbsp;</td>
   		 <td><h1>Create email</h1></td>
   		</tr>
   		<tr>
     	 <td>&nbsp;</td>
     	 <td>
      		<text rows="1" cols="80" readonly>
      			<b>Type your email below:</b>
			</text> 
	  	 </td> 
		</tr>       
    	<tr>
      	<td>&nbsp;</td>
      	<td>
      		<textarea id=appendedText rows="20" cols="80"></textarea> 
		</td>      
    	</tr>		
		<%
		} else { %>

	    </td></tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><h1> Notification Review</h1></td>
	    </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td><%=(NIMUtils.returnEmailAsHTML(myETSend,true))%>
	    </td>
	    </tr>
	    <tr>
	      <td>&nbsp;</td>
	      <td>
	      	<text rows="1" cols="80" readonly>
	      		<b>Enter any comments in the text box below. These comments will be appended to the text above.</b>
		  	</text> 
		  </td> 
	    </tr>   
	    <tr>
	      <td>&nbsp;</td>
	      <td>
	      <textarea id=appendedText rows="10" cols="80"></textarea> 
	      </td>      
	    </tr>		
		<% } %>
	
      
    <tr>
      <td>&nbsp;</td>
      <td><input type="button" id="confirmButton" onclick="processButton('<%=sFlag%>',escape(document.getElementById('appendedText').value),'<%=customEmail%>')" value="Confirm" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="button" class="inputButton_md_AlertLevel1" onclick="this.disabled=true;if (confirm('Are you sure you want to send this alert?')){document.location='alert_test.jsp?EDIT=<%=sFlag%>';}" value="Direct Send - DO NOT CLICK" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr> 
    </table>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

