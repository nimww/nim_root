<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
<%@ include file="../generic/generalDisplay.jsp" %>



<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

    <table cellpadding=5 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM_Encounter")%>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat("MM/dd/yyyy");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
	NIM3_EncounterObject myEO2 = new NIM3_EncounterObject(iEncounterID);
  //page security
  if (accessValid)
  {
%>
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td><h1> Problem</h1></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><table width="100%%" border="1" cellspacing="0" cellpadding="5">


<%if (myEO2.getNIM3_ParentPayerMaster().getPayerID()==138){%>
        <tr>
          <td align="center"><input type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_Problem_report.jsp?EDIT=problem_bypassrx';" value="Bypass RX &amp; Rx Review" /></td>
          <td><p>For Tyson Cases where the Rx is not yet Available.</p></td>
        </tr>
<%}%>

        <tr>
          <td align="center"><input type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_Problem_report.jsp?EDIT=problem_resched';" value="ReSchedule/Cancel" /></td>
          <td><p>If Patient needs to reschedule an appointment prior to the appointment date/time. </p>
          <p>If Imaging Center calls and requests a reschedule due to maintenance of equipment</p>
          <p>If you need to cancel/remove the appointment.</p></td>
        </tr>
        <tr>
          <td align="center"><input type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_Problem_report.jsp?EDIT=problem_escalate';" value="Escalated Problem" /></td>
          <td><p>If there is a significant problem in communication with the patient, the adjuster, the referring physician, etc. and you need a Manager to be alerted.</p></td>
        </tr>
<%
if (isScheduler3)
{
	%>
        <tr>
          <td align="center"><input type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_Problem_report.jsp?EDIT=problem_void';" value="Void" /></td>
          <td><p>If you have an extra encounter that is no longer needed. For example, you created two encounters and need to combine all services into one encounter. Once you move all the appropriate information to the first encounter, you would then use this button to void the second encounter.</p></td>
        </tr>
<%
}
if (isScheduler3)
{
	%>
        <tr>
          <td align="center"><input type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_Problem_report.jsp?EDIT=problem_reactive';" value="Change to Active" /></td>
          <td><p>Manager Only Feature.</p></td>
        </tr>
        <tr>
          <td align="center"><input type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_Problem_report.jsp?EDIT=problem_close';" value="Change to Closed" /></td>
          <td><p>Manager Only Feature.</p></td>
        </tr>
        <tr>
          <td align="center"><input type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_Problem_report.jsp?EDIT=problem_courtesy';" value="Change to Courtesy Schedule" /></td>
          <td><p>Manager Only Feature.</p></td>
        </tr>
        <tr>
          <td align="center"><input type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_Problem_report.jsp?EDIT=problem_holdbill';" value="Change to Flag:Hold Bill" /></td>
          <td><p>Manager Only Feature.</p></td>
        </tr>
        <tr>
          <td align="center"><input type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_Problem_report.jsp?EDIT=problem_billinginprogress';" value="Change to Billing:In Progress" /></td>
          <td><p>Manager Only Feature.</p></td>
        </tr>
        <tr>
          <td align="center"><input type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_Problem_report.jsp?EDIT=problem_tier2';" value="Change to Tier 2 Pricing" /></td>
          <td><p>Manager Only Feature.</p></td>
        </tr>
        <tr>
          <td align="center"><input type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_Problem_report.jsp?EDIT=problem_tier3';" value="Change to Tier 3 Pricing" /></td>
          <td><p>Manager Only Feature.</p></td>
        </tr>
<%
}

if (isScheduler3)
{
	%>
        <tr>
          <td align="center"><input type="button" class="inputButton_md_Default" onclick="this.disabled=true;document.location='tNIM3_Encounter_Problem_report.jsp?EDIT=problem_removecourtesy';" value="Remove Courtesy Status" /></td>
          <td><p>NETDEV Only Feature.</p></td>
        </tr>
<%
}
%>
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="right"><input type="button" class="inputButton_md_Stop" onclick="this.disabled=true;window.close();" value="Cancel" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr> 
    </table>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

