<html>
<head>

<title>NextImage Medical - Secure Login</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">td img {display: block;}</style>
<!--Fireworks CS3 Dreamweaver CS3 target.  Created Sun Jun 08 15:05:38 GMT-0700 (Pacific Daylight Time) 2008-->
<script language="JavaScript1.2" type="text/javascript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_nbGroup(event, grpName) { //v6.0
var i,img,nbArr,args=MM_nbGroup.arguments;
  if (event == "init" && args.length > 2) {
    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
      img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;
      if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();
      nbArr[nbArr.length] = img;
      for (i=4; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
        if (!img.MM_up) img.MM_up = img.src;
        img.src = img.MM_dn = args[i+1];
        nbArr[nbArr.length] = img;
    } }
  } else if (event == "over") {
    document.MM_nbOver = nbArr = new Array();
    for (i=1; i < args.length-1; i+=3) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = (img.MM_dn && args[i+2]) ? args[i+2] : ((args[i+1])?args[i+1] : img.MM_up);
      nbArr[nbArr.length] = img;
    }
  } else if (event == "out" ) {
    for (i=0; i < document.MM_nbOver.length; i++) { img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }
  } else if (event == "down") {
    nbArr = document[grpName];
    if (nbArr) for (i=0; i < nbArr.length; i++) { img=nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }
    document[grpName] = nbArr = new Array();
    for (i=2; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = img.MM_dn = (args[i+1])? args[i+1] : img.MM_up;
      nbArr[nbArr.length] = img;
  } }
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

//-->
</script>
<link href="www-images/style.css" rel="stylesheet" type="text/css" />
</head>
<body bgcolor="#001f3e" onLoad="MM_preloadImages('www-images/images/index_link_home_f2.gif','www-images/images/index_link_home_f3.gif','www-images/images/index_link_network_f2.gif','www-images/images/index_link_network_f3.gif','www-images/images/index_link_service_f2.gif','www-images/images/index_link_service_f3.gif','www-images/images/index_link_technology_f2.gif','www-images/images/index_link_technology_f3.gif','www-images/images/index_link_about_f2.gif','www-images/images/index_link_about_f3.gif','www-images/images/index_link_contact_f2.gif','www-images/images/index_link_contact_f3.gif','www-images/images/index_link_login_f2.gif','www-images/images/index_link_login_f3.gif')">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" height="100%">
<!-- fwtable fwsrc="index1.png" fwpage="Page 1" fwbase="index-nav.jpg" fwstyle="Dreamweaver" fwdocid = "689853260" fwnested="1" -->
  <tr valign="top">
   <td id="left">&nbsp;</td>
   <td width="804" valign="top"><table width="804" height="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="images/index_bg_repeat.gif" bgcolor="#FFFFFF" >
	  <tr>
	   <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="804">
		  <tr>
		   <td><a href="http://www.nextimagemedical.com/home.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_home','www-images/images/index_link_home_f2.gif','www-images/images/index_link_home_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_home','www-images/images/index_link_home_f3.gif',1)"><img name="index_link_home" src="www-images/images/index_link_home.gif" width="88" height="23" border="0" id="index_link_home" alt="" /></a></td>
		   <td><a href="http://www.nextimagemedical.com/network.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_network','www-images/images/index_link_network_f2.gif','www-images/images/index_link_network_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_network','www-images/images/index_link_network_f3.gif',1)"><img name="index_link_network" src="www-images/images/index_link_network.gif" width="191" height="23" border="0" id="index_link_network" alt="" /></a></td>
		   <td><a href="http://www.nextimagemedical.com/service.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_service','www-images/images/index_link_service_f2.gif','www-images/images/index_link_service_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_service','www-images/images/index_link_service_f3.gif',1)"><img name="index_link_service" src="www-images/images/index_link_service.gif" width="105" height="23" border="0" id="index_link_service" alt="" /></a></td>
		   <td><a href="http://www.nextimagemedical.com/technology.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_technology','www-images/images/index_link_technology_f2.gif','www-images/images/index_link_technology_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_technology','www-images/images/index_link_technology_f3.gif',1)"><img name="index_link_technology" src="www-images/images/index_link_technology.gif" width="136" height="23" border="0" id="index_link_technology" alt="" /></a></td>
		   <td><a href="http://www.nextimagemedical.com/about.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_about','www-images/images/index_link_about_f2.gif','www-images/images/index_link_about_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_about','www-images/images/index_link_about_f3.gif',1)"><img name="index_link_about" src="www-images/images/index_link_about.gif" width="90" height="23" border="0" id="index_link_about" alt="" /></a></td>
		   <td><a href="http://www.nextimagemedical.com/contact.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_contact','www-images/images/index_link_contact_f2.gif','www-images/images/index_link_contact_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_contact','www-images/images/index_link_contact_f3.gif',1)"><img name="index_link_contact" src="www-images/images/index_link_contact.gif" width="106" height="23" border="0" id="index_link_contact" alt="" /></a></td>
		   <td><a href="http://www.nextimagemedical.com/login.php" onMouseOut="MM_nbGroup('out');" onMouseOver="MM_nbGroup('over','index_link_login','www-images/images/index_link_login_f2.gif','www-images/images/index_link_login_f3.gif',1)" onClick="MM_nbGroup('down','navbar1','index_link_login','www-images/images/index_link_login_f3.gif',1)"><img name="index_link_login" src="www-images/images/index_link_login.gif" width="88" height="23" border="0" id="index_link_login" alt="" /></a></td>
		  </tr>
		</table></td>
	  </tr>
	  <tr>
	   <td><img name="index_top" src="www-images/images/index_top.jpg" width="804" height="240" border="0" id="index_top" alt="" /></td>
	  </tr>
	  <tr>
	   <td background="images/index_bg_repeat.gif"><table width="100%" border="0" cellspacing="0" cellpadding="15">
         <tr>
           <td bgcolor="#FFFFFF"><%
java.util.Hashtable pageControllerHash = null;
session.setAttribute("pageControllerHash",pageControllerHash);
%>             <table border="0" align="center" cellpadding="0" cellspacing="0">
               <tr>
                 <td align="center">&nbsp;</td>
      <td >&nbsp;</td>
    </tr>
               <tr>
                 <td align="center" ><span class="style2">NextImage<br>
                   Secure Server</span> </td>
      <td ><table border="1" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td><form name="form1" method="post" action="Outside_View_report_sub.jsp" target="_top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="tdHeader">
                <td colspan="2" >Please enter your Sign In information </td>
                </tr>
              <tr class="tdBase">
                <td align="right">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              <tr class="tdBase">
                <td align="right">User Name:&nbsp;</td>
                  <td><input type="text" name="LogonUserName" value="<% if (request.getParameter("ssun")!=null){out.print(request.getParameter("ssun"));}%>">                </td>
                </tr>
              <tr class="tdBase">
                <td align="right">Password:&nbsp;</td>
                <td><input type="password" name="LogonUserPassword"></td>
              </tr class="tdBase">
              <tr class="tdBase">
                <td align="right">ScanPass:&nbsp;</td>
                  <td><input name="ScanPass" type="text" value ="<% if (request.getParameter("sssp")!=null){out.print(request.getParameter("sssp"));}%>" id="ScanPass">                </td>
                </tr class="tdBase">
              <tr>
                <td>&nbsp;</td>
                  <td><input type="submit" name="Submit" value="Submit" style="font-family: Arial; font-size: 10px; background-color: #C0C0C0">                </td>
                </tr>
              <tr>
                <td colspan="2"><hr>                </td>
                </tr>
              </table>
          </form></td>
        </tr>
        
        </table></td>
    </tr>
               <tr>
                 <td >&nbsp;</td>
      <td >&nbsp;</td>
    </tr>
               <tr>
                 <td align="center">copyright &copy; 2007-2009 NextImage Medical, Inc.</td>
      <td bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
             </table></td>
         </tr>
       </table></td>
	  </tr>
	  <tr>
	   <td bgcolor="#FFFFFF">&nbsp;</td>
	  </tr>
	  <tr>
	    <td height="100%" valign="bottom"><table width="100%" border="0" cellspacing="0" cellpadding="15">
          <tr>
            <td bgcolor="#FFFFFF">copyright &copy; 2009 NextImage Medical, Inc. </td>
          </tr>
        </table></td>
	    </tr>
	</table></td>
   <td id="right">&nbsp;</td>
  </tr>
</table>
</body>
</html>
