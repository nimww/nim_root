<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*, com.winstaff.*" errorPage=""%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
</head>

<body>
	<%@ include file="../generic/generalDisplay.jsp"%>

	<%
	String line1 = null; 
	String line2 = null;
	
	line1 = "";
	line2 = "";
	
	//line1 = "NIM will be down for maintenance";
	//line2 = "June 9th @ 12 noon for 15 minutes.";
	
   	boolean accessValid = false;
   	Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   	Integer iPayerID = null;
   	Integer iAdjusterID = null;
   	if (iSecurityCheck.intValue()!=0)
   	{
	    if (isScheduler) 
	    {
        	accessValid = true;
    	}
    	else if (pageControllerHash.containsKey("iAdjusterID")&&pageControllerHash.containsKey("iPayerID")) 
    	{
	        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
			isAdjuster = true;
        	accessValid = true;
		}
    	else if (pageControllerHash.containsKey("iAdjusterID2")&&pageControllerHash.containsKey("iPayerID")) 
    	{
	        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
			isAdjuster2 = true;
        	accessValid = true;
		}
    	else if (pageControllerHash.containsKey("iAdjusterID3")&&pageControllerHash.containsKey("iPayerID")) 
    	{
	        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
			isAdjuster3 = true;
        	accessValid = true;
		}
		
  		//page security

	  	if (accessValid)
  		{
	      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      	  java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      	  pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");

	  	  if (isScheduler)
	  		{
	  		%>
			<meta http-equiv="refresh" content="600">
			<table width="100%" border="0" cellspacing="0" cellpadding="1">
				<tr class="tdBaseAltOrange">
					<td width="50%"><b><b></td>
				</tr>
				<tr class="tdBaseAltOrange">
					<td width="50%"></td>
				</tr>
				<tr class="tdBaseAlt">
					<td width="100%"><%=line1%></td>
				</tr>
				<tr class="tdBaseAlt">
					<td width="100%"><%=line2%></td>
				</tr>
				<tr class="tdBaseAlt">
					<td width="100%"></td>
				</tr>
			</table>
			<%
	  		}
  		}
  		else
  		{
   		out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
 		}		
	}
	else
		{
			out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
		}
	%>
	<h1></h1>
</body>
</html>
