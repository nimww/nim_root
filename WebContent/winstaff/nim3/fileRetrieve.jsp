<%@ page trimDirectiveWhitespaces="true" %>
<%@ page contentType="application/pdf" language="java" import="com.winstaff.*"%>
<%@ include file="../generic/CheckLogin.jsp"%>
<%

//initial declaration of list class and parentID
String sFileName = "";
String sDownloadName = "";
Boolean bDownload = null;
boolean accessValid = false;

if (pageControllerHash.containsKey("sFileName")&&pageControllerHash.containsKey("sDownloadName")&&pageControllerHash.containsKey("bDownload")) 
    {
//			String myRPath = "/var/lib/tomcat5/webapps/nimdox/";
			String myRPath = ConfigurationInformation.sUploadFolderDirectory;
        sFileName        =    myRPath + (String)pageControllerHash.get("sFileName");
        sDownloadName        =    (String)pageControllerHash.get("sDownloadName");
        bDownload        =    (Boolean)pageControllerHash.get("bDownload");
        accessValid = true;    
   }
  //page security
	if (accessValid) {
	//out.println(sFileName);
	//out.println(sDownloadName);
	//out.println(bDownload);

		try {
			if (bDownload.booleanValue()) {
				response.setHeader("Content-Disposition", "attachment; filename="+sDownloadName  ); 
			} 
			else {
			response.setHeader("Content-Disposition", "inline; filename="+sDownloadName  ); 
			}
			
			if (ConfigurationInformation.storeDocumentsInDatabase) {
				
				// Get BLOB data from database for this specific filename.
				DocumentDatabase db = new DocumentDatabase();
				byte [] data = db.readFile((String)pageControllerHash.get("sFileName"));
				response.getOutputStream().write(data);
			} else {
				
				// -- Original call.  Get from file system.
				response.getOutputStream().write(DataControlUtils.FileToBytes(sFileName));
			}
					
			}
		catch(Exception e) {
		}

  	}
  	else{
   	out.println("illegal");
  	}
%>