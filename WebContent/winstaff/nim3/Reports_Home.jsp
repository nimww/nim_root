<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
</head>
<SCRIPT language="JavaScript1.2">
function openStatus()
{
	testwindow= window.open ("Home_Status.jsp", "HomeStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(0,0);
}

function openAuto(myAuto)
{
	testwindow= window.open ("report_auto2.jsp?rid=" + myAuto + "&MREF=300", "AutoStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(100,100);
}

</SCRIPT>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_report.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isReporter) 
    {
        accessValid = true;
    }

	
	
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	
		if (isReporter)
	  {
		  String theScheduler = CurrentUserAccount.getUserID().toString();
		  String myAT_Where = "";
		  if ( request.getParameter("theScheduler")!=null&&!request.getParameter("theScheduler").equalsIgnoreCase("") )
		  {
			  theScheduler = request.getParameter("theScheduler");
		  }
		  if (!theScheduler.equalsIgnoreCase("0"))
		  {
			  myAT_Where =  " AND tNIM3_CaseAccount.assignedtoid = " + theScheduler + " ";
		  }
		  
	  %>
      
 
      
      
      
<table width="<%=MasterTableWidth%>" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td ><p class="title" style="margin:0">Welcome to NextImage Grid Version <%=ConfigurationInformation.applicationVersion%><br>
          Server: <%=ConfigurationInformation.serverName%> [<%=ConfigurationInformation.applicationVersion%>] </p></td>
      </tr>
	<tr><% pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");


				searchDB2 db = new searchDB2();

				try{

					java.sql.ResultSet result = null;;

					String query ="select  (select count(distinct referral.referralid) from tnim3_encounter as \"encounter\" inner join tnim3_referral as \"referral\" on referral.referralid = encounter.referralid inner join tnim3_caseaccount as \"caseaccount\" on caseaccount.caseid = referral.caseid inner join tuseraccount as \"assignedto\" on assignedto.userid = caseaccount.assignedtoid where (encounter.encounterstatusid ='1' or encounter.encounterstatusid = '12') and encounter.uniquecreatedate > (now() - '3 month'::interval) and appointmentid = 0) as \"Active\",  (select count(DISTINCT tnim3_encounter.encounterid) from tnim3_encounter inner join tnim3_referral on tnim3_referral.referralid = tnim3_encounter.referralid  where tNIM3_Encounter.encounterstatusid in ('1','12') and tnim3_encounter.uniquecreatedate > (now() - '3 month'::interval)  and rxfileid=0) as \"NeedRx\", (select count(DISTINCT tnim3_caseaccount.caseid)  from tnim3_caseaccount inner join tnim3_referral on tnim3_referral.caseid = tnim3_caseaccount.caseid  RIGHT JOIN tnim3_encounter on tnim3_encounter.referralid = tnim3_referral.referralid left join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid where cast(patientdob as VARCHAR) not like '1800%' and cast(dateofinjury as VARCHAR) not like '1800%'  and adjusterid != 0  and  patientfirstname !='' and patientzip != '' and patientstateid !=0 and (patienthomephone !='' or patientcellphone !='') and caseclaimnumber !='' and  cast(appointmenttime as VARCHAR)  is null and encounterstatusid = '1' and tnim3_encounter.uniquecreatedate > (now() - '3 month'::interval) )as \"ReadyToSched\", (select count(DISTINCT encounterid) from tnim3_encounter inner join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid where encounterstatusid ='1' and tnim3_encounter.uniquecreatedate > (now() - '3 month'::interval) and appointmenttime < now() and reportfileid=0 and tnim3_encounter.appointmentid >0) as \"NeedRp\", (select  count(DISTINCT encounterid)  from tnim3_encounter inner join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid where encounterstatusid in ('1', '12') and tnim3_encounter.uniquecreatedate > (now() - '3 month'::interval) and appointmenttime < now() and reportfileid!=0 and timetrack_reqrpreview_userid=0)  as \"RpNeedsRw\", (select count(DISTINCT tnim3_commtrack.encounterid)  from tnim3_commtrack INNER JOIN tnim3_encounter on tnim3_encounter.encounterid = tnim3_commtrack.encounterid where encounterstatusid ='9' and (messagetext LIKE '%Billing: In Progress%' or messagetext LIKE '%Moved to Billing%') and tnim3_commtrack.uniquecreatedate > (now() - '1 day'::interval)) as \"BillingInLastDay\" , (select  count(DISTINCT tnim3_commtrack.encounterid)  from tnim3_commtrack INNER JOIN tnim3_encounter on tnim3_encounter.encounterid = tnim3_commtrack.encounterid where encounterstatusid ='9' and (messagetext LIKE '%Billing: In Progress%' or messagetext LIKE '%Moved to Billing%') and tnim3_commtrack.uniquecreatedate > (now() - '7 day'::interval)) as \"BillingInLastWeek\", (select  count(DISTINCT tnim3_commtrack.encounterid)  from tnim3_commtrack INNER JOIN tnim3_encounter on tnim3_encounter.encounterid = tnim3_commtrack.encounterid where encounterstatusid ='9' and (messagetext LIKE '%Billing: In Progress%' or messagetext LIKE '%Moved to Billing%') and tnim3_commtrack.uniquecreatedate > (now() - '1 month'::interval)) as \"BillingInLastMonth\", (select count(encounterid) from tnim3_encounter where encounterstatusid='12') AS \"Escalated\", (select count(encounterid) from tnim3_encounter where seenetdev_waiting ='1') AS \"In NetDev\", ( select count(tNIM3_Encounter.encounterid) from tNIM3_Encounter  INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN (select distinct tnim3_service.encounterid from tnim3_service)as service on service.encounterid = tnim3_encounter.encounterid  INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid  where tNIM3_Encounter.AppointmentID=0 AND tnim3_referral.rxfileid>0 AND tnim3_referral.orderfileid>0 AND tnim3_referral.referringphysicianid>0 AND tnim3_caseaccount.adjusterid>0 AND tnim3_caseaccount.patientstateid>0  AND tnim3_caseaccount.patientaddress1 != '' AND tNIM3_encounter.timetrack_reqrxreview_userid=0 AND tnim3_referral.referralstatusid = '1' AND tNIM3_Encounter.encounterstatusid not in (0,2,3,5,6,9,10))   as \"Rx Review\"  ";

					result = db.executeStatement(query);%>
				<%if (result!=null&&result.next()){%>
				<table border="0" cellspacing="0" cellpadding="2" width="<%=MasterTableWidth%>">

<tr>

								<td colspan=4><h3>Quick Stats</h3></td>

							</tr>							
<tr>

								<td>Active</td><td><%=result.getString("Active")%></td>

								<td style="padding-left:15px">Billing In Last Day</td><td><%=result.getString("BillingInLastDay")%></td>

							</tr>

							<tr>

								<td>

								<form action="Worklist.jsp" id="showWL_missingrx" method="post" style="margin:0">

									<input type='hidden' name='showWL_missingrx' value="1"/>

									<input type='hidden' name='theScheduler1' value="0">

									<a href="#" onclick="document.getElementById('showWL_missingrx').submit(); return false;">Need Rx</a>

								</form>

								</td><td><%=result.getString("NeedRx")%></td>

								<td style="padding-left:15px">Billing In Last Week</td><td><%=result.getString("BillingInLastWeek")%></td>

							</tr>

							<tr>

								<td>

								<form action="Worklist.jsp" id="showWL_rxreview" method="post" style="margin:0">

									<input type='hidden' name='showWL_rxreview' value="1"/>

									<input type='hidden' name='theScheduler1' value="0">

									<a href="#" onclick="document.getElementById('showWL_rxreview').submit(); return false;">Need Rx Rw</a>

								</form>

								</td><td><%=result.getString("Rx Review")%></td>

								<td style="padding-left:15px">Billing In Last Month</td><td><%=result.getString("BillingInLastMonth")%></td>

							</tr>

							<tr>

								<td>

								<form action="Worklisk.jsp" id="showWL_readytoschedule" method="post" style="margin:0">

									<input type='hidden' name='showWL_readytoschedule' value="1"/>

									<input type='hidden' name='theScheduler1' value="0">

									<%--<a href="#" onclick="document.getElementById('showWL_readytoschedule').submit(); return false;">Ready To Sched</a> --%>

									Ready To Sched

								</form>

								</td><td><%=result.getString("ReadyToSched")%></td>

								<td style="padding-left:15px">

								<form action="Worklist.jsp" id="showWL_escalatedcases" method="post" style="margin:0">

									<input type='hidden' name='showWL_escalatedcases' value="1"/>

									<input type='hidden' name='theScheduler1' value="0">

									<a href="#" onclick="document.getElementById('showWL_escalatedcases').submit(); return false;">Escalated Cases</a>

								</form>

								</td><td><%=result.getString("Escalated")%></td>

							</tr>

							<tr>

								<td>

								<form action="Worklist.jsp" id="showWL_missingreport" method="post" style="margin:0">

									<input type='hidden' name='showWL_missingreport' value="1"/>

									<input type='hidden' name='theScheduler1' value="0">

									<a href="#" onclick="document.getElementById('showWL_missingreport').submit(); return false;">Need Rp</a>

								</form>

								</td><td><%=result.getString("NeedRp")%></td>

								<td style="padding-left:15px">

								<form action="Worklist.jsp" id="showWL_innetdev" method="post" style="margin:0">

									<input type='hidden' name='showWL_innetdev' value="1"/>

									<input type='hidden' name='theScheduler1' value="0">

									<a href="#" onclick="document.getElementById('showWL_innetdev').submit(); return false;">In NetDev</a>

								</form>

								</td><td><%=result.getString("In NetDev")%></td>

							</tr>

							<tr>

								<td>

								<form action="Worklist.jsp" id="showWL_needsrpreview" method="post" style="margin:0">

									<input type='hidden' name='showWL_needsrpreview' value="1"/>

									<input type='hidden' name='theScheduler1' value="0">

									<a href="#" onclick="document.getElementById('showWL_needsrpreview').submit(); return false;">Rp Needs Rw</a>

								</form>

								</td><td><%=result.getString("RpNeedsRw")%></td>

								<td style="padding-left:15px"></td>&nbsp;<td>&nbsp;</td>

							</tr>

							

						</table>
				<%}

					db.closeAll();

				}

				catch (Exception e){

					out.print("topnav "+e);

					db.closeAll();

				}

				finally{

					db.closeAll();

				}%>     </tr>
      
      <%
	  if (isReporter)
	  {
	  %>
      <tr class="tdHeaderAlt" >
        <td ><span class="tdHeader"><hr />
        Reporting Home:</span></td>
      </tr>
      <tr>
        <td bgcolor="#DAE0E4"><table width="100%%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="33%" align="center" class="tdHeaderAltDark">Sales</td>
            <td align="center">&nbsp;</td>
            <td width="33%" align="center" class="tdHeaderAltDark">AutoReports: Network Development</td>
            <td align="center">&nbsp;</td>
            <td width="33%" align="center" class="tdHeaderAltDark">AutoReports: Scheduling</td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center"><input name="button4" type="submit" class="inputButton_md_Default" id="button4" onClick="this.disabled = true; document.location='report_auto_int.jsp?showall=m';" value="Interactive Payer Report" /></td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button" type="submit" class="inputButton_md_Default" id="button" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=ndovr';" value="NetDev Overview" /></td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button8" type="submit" class="inputButton_md_Default" id="button8" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=volume1';" value="Volume: By Day" /></td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center"><input name="button4" type="submit" class="inputButton_md_Default" id="button4" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=actuals_to_plan';" value="Actuals to Plan, by Region" /></td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button17" type="submit" class="inputButton_md_Default" id="button17" onClick="this.disabled = true; alert('This report may take ~30 seconds or so to run.'); document.location='report_auto2.jsp?rid=nd_pending';" value="NetDev Pending" /></td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button18" type="submit" class="inputButton_md_Default" id="button18" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=volume1_reg';" value="Volume: By Day By Region" /></td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center"><input name="button15" type="submit" class="inputButton_md_Default" id="button15" onClick="this.disabled = true; document.location='tUserAccount_Sales_query.jsp';" value="User Search" /></td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button2" type="submit" class="inputButton_md_Default" id="button2" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=ndrefpract';" value="Referral by Practice" /></td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button9" type="submit" class="inputButton_md_Default" id="button9" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=mVolume2010';" value="Volume: By Month 2010" /></td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button13" type="submit" class="inputButton_md_Default" id="button13" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=mVolume2011';" value="Volume: By Month 2011" /></td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center"><input name="button16" type="submit" class="inputButton_md_Default" id="button16" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=nimcrm_activity_alltime';" value="NIMCRM: Activity (All-time)" /></td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button5" type="submit" class="inputButton_md_Default" id="button5" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=ndrefadmin';" value="Referral by Vendor" /></td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button6" type="submit" class="inputButton_md_Default" id="button6" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=sppaystate';" value="Payer: Breakdown by State" /></td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button7" type="submit" class="inputButton_md_Default" id="button7" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=ndrefscc';" value="Referral by State, County, City" /></td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center"><input name="button19" type="submit" class="inputButton_md_Default" id="button19" onClick="this.disabled = true; document.location='sales_report.jsp';" value="Sales Report" /></td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button14" type="submit" class="inputButton_md_Default" id="button14" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=ndrefscc2';" value="Referral by State, County, City By Year/Month" /></td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button12" type="submit" class="inputButton_md_Default" id="button12" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=mq4_activeaging';" value="Active &quot;Aging&quot; Cases" /></td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button10" type="submit" class="inputButton_md_Default" id="button10" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=mq4_chartisca';" value="Chartis: CA Cases for Pilot" /></td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center"><input name="button11" type="submit" class="inputButton_md_Default" id="button11" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=mq4_je1';" value="Johns Eastern: Cases" /></td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td colspan="3" align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td colspan="3" align="center"><input name="button3" type="submit" class="inputButton_md_Default" id="button3" onClick="this.disabled = true; document.location='report_auto2.jsp?rid=volume1';" value="Add'l Auto Reports" /></td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
        </table> 
        
        
        
        
        </td>
      </tr>
      <%
	  }
	  else
	  {
		  %>
          <%
	  }
	  %>
      <tr>
        <td >&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>


        
        
          <%
	  }
	  %>

    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>