<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<head>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />
<%@ include file="../generic/CheckLogin.jsp" %>

<%@ include file="../generic/generalDisplay.jsp" %>
<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function confirmReset()
{
	if (confirm("Are you sure you want to Reset this Password?"))
	{
		modalPost('RESETPSWD', modalWin('UserAccount_ChangePassword_reset.jsp','Reset...','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));		
//window.open('UserAccount_ChangePassword_reset.jsp','PReset','status=yes,scrollbars=yes,resizable=yes,width=500,height=500');
	}

}

function confirmReset2()
{
	if (confirm("Are you sure you want to Reset & Email this Password?\n\nThis will IMMEDIATELY send an email to the user."))
	{
		modalPost('RESETPSWD', modalWin('UserAccount_ChangePassword_reset_em.jsp','Reset...','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));		
	}

}

function confirmChange()
{
	if (confirm("You must know the current password in order to change it\n\nAlso, new passwords must be 8 digits/characters long?"))
	{
		modalPost('RESETPSWD', modalWin('UserAccount_ChangePassword.jsp','Reset...','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));		
	}

}

//-->

</script>
<script language="JavaScript">
<!--
function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}
//-->
</script>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_maint.jsp?hidenav=y&plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>


    <title>NIM v3 User Account Edit</title>
    </head>
    
<body >    
<table width="100%" height="100%" border=1 cellpadding=30 cellspacing=0 bordercolor="#666666">
    <tr>
    <td valign="top">
      <p><a href="tUserAccount_Sales_query.jsp" class="titleBlue">Return to Search</a> </p>
        
        
        <%
//initial declaration of list class and parentID
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      		java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
      java.text.SimpleDateFormat dbdfFull = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    bltUserAccount        UserAccount        =    null;

    Integer        iUserID        =    null;
//    Integer        iCompanyID =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;
    if (pageControllerHash.containsKey("iUserID_Reporter")) 
    {
        iUserID        =    (Integer)pageControllerHash.get("iUserID_Reporter");
//        iCompanyID        =    (Integer)pageControllerHash.get("iCompanyID");
        accessValid = true;    }
  //page security
  if (accessValid&& isReporter )
  {

    pageControllerHash.put("sLocalChildReturnPage","../tUserAccount_Sales_view.jsp");
    pageControllerHash.remove("iCommTrackID");
	
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID


    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") ||request.getParameter( "EDIT" ).equalsIgnoreCase("salesview") )
    {
        UserAccount        =    new    bltUserAccount(iUserID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        UserAccount        =    new    bltUserAccount();
    }

//fields
        %>
      <p><span class=title>Edit Contact: <%=UserAccount.getContactFirstName()%>&nbsp;<%=UserAccount.getContactLastName()%></span><br>
	<%
	if (UserAccount.getStatus()==3){
	%>
	<h2 class="tdBaseAlt_Action1">This user has been archived<br>They may no longer work at this branch.</h2>
     <%
	 }
	%></p>
      <form method=POST action="gsn_nim3b/tUserAccount_form3_sub.jsp" name="tUserAccount_form1">
                  <p><input name=Submit type=Submit class="titleBlue" value=Save></p>

<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
        <input type="hidden" name="EDITID" value = "<%=iUserID%>" >  


	<%  String theClass ="tdBase";%>
        <p class=<%=theClass%> > ContactID: <%=UserAccount.getUserID()%><br>
ContactType:&nbsp;<strong><%=UserAccount.getAccountType() %></strong> <br>
        Logon  Name:
          <strong><%=UserAccount.getLogonUserName()%></strong><br>
          <strong><%=UserAccount.getStatus() == 2 ? "<span style=\"color:rgb(37, 196, 37)\">Active</span>" : "<span style=\"color:red\">Inactive</span>"%></strong>
        </p>
            
    <% if (UserAccount.getAccountType().indexOf("Adjuster")>=0){ %>
        <p class=titleSub1 ><b>Payer:</b>&nbsp;&nbsp;<%= new bltNIM3_PayerMaster(UserAccount.getPayerID()).getPayerName()%>&nbsp; [<%=UserAccount.getPayerID()%>]   
        </p>
	<% } %>
            
    



<table cellpadding=2 cellspacing=0 width=100%>

          
          <tr ><td valign=top bgcolor="#E3EADF" class="titleSub1"  ><b>Name</b></td><td valign=top bgcolor="#E3EADF" class="titleSub1"><%=UserAccount.getContactFirstName()%>&nbsp;<%=UserAccount.getContactLastName()%></td></tr>
 
            <%
            if ( (UserAccount.isRequired("NIM_UserType",UserSecurityGroupID))&&(!UserAccount.isComplete("NIM_UserType")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("NIM_UserType",expiredDays))&&(UserAccount.isExpiredCheck("NIM_UserType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("NIM_UserType",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>User Type&nbsp;</b></p></td><td valign=top><p><select   name="NIM_UserType" ><jsp:include page="../generic/tNIMUserType_String.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getNIM_UserType()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=NIM_UserType&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("NIM_UserType")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("NIM_UserType",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>User Type&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getNIM_UserType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=NIM_UserType&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("NIM_UserType")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>





            <%
            if ( (UserAccount.isRequired("Internal_PrimaryContact",UserSecurityGroupID))&&(!UserAccount.isComplete("Internal_PrimaryContact")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("Internal_PrimaryContact",expiredDays))&&(UserAccount.isExpiredCheck("Internal_PrimaryContact",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("Internal_PrimaryContact",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Internal Primary Contact&nbsp;</b></p></td><td valign=top>
                     
                     
                  
<select name="Internal_PrimaryContact"/>
<option value="0">Unassigned/Empty</option>
    <%
{
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("select Userid,LogonUserName, contactfirstname, contactlastname from tUserAccount where AllowAsPrimaryContact =1 order by contactfirstname, contactlastname");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
				if (new Integer(myRS.getString("UserID")).intValue()==UserAccount.getInternal_PrimaryContact().intValue())
				{
					%>
					<option selected value="<%=myRS.getString("UserID")%>"><%=myRS.getString("contactfirstname")%> <%=myRS.getString("contactlastname")%></option>
					<%
				}
				else
				{
					%>
					<option  value="<%=myRS.getString("UserID")%>"><%=myRS.getString("contactfirstname")%> <%=myRS.getString("contactlastname")%></option>
					<%
				}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
%>
    </select>                    
               &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Internal_PrimaryContact&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("Internal_PrimaryContact")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("Internal_PrimaryContact",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Internal Primary Contact&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getInternal_PrimaryContact()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Internal_PrimaryContact&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("Internal_PrimaryContact")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("LastContacted",UserSecurityGroupID))&&(!UserAccount.isComplete("LastContacted")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("LastContacted",expiredDays))&&(UserAccount.isExpiredCheck("LastContacted",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if (false&&(UserAccount.isWrite("LastContacted",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Last Contacted&nbsp;</b></p></td><td valign=top><p><input maxlength=20  type=text size="20" name="LastContacted" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UserAccount.getLastContacted())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LastContacted&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("LastContacted")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("LastContacted",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Last Contacted</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UserAccount.getLastContacted())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LastContacted&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("LastContacted")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("DateOfBirth",UserSecurityGroupID))&&(!UserAccount.isComplete("DateOfBirth")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("DateOfBirth",expiredDays))&&(UserAccount.isExpiredCheck("DateOfBirth",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

            <%
            if ((UserAccount.isWrite("DateOfBirth",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Of Birth</b></p></td><td valign=top><p><input maxlength=20  type=text size="20" name="DateOfBirth" value='<jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UserAccount.getDateOfBirth())%>" /></jsp:include>' >&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfBirth&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("DateOfBirth")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%> 
                          <br>
                          *We care more about Month/Day than Year (so you can pick any year) </p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("DateOfBirth",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Of Birth&nbsp;(mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UserAccount.getDateOfBirth())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateOfBirth&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("DateOfBirth")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("PersonalInsights",UserSecurityGroupID))&&(!UserAccount.isComplete("PersonalInsights")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("PersonalInsights",expiredDays))&&(UserAccount.isExpiredCheck("PersonalInsights",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("PersonalInsights",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b>Personal Insights&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,5000)" rows="12" name="PersonalInsights" cols="60"><%=UserAccount.getPersonalInsights()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PersonalInsights&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("PersonalInsights")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("PersonalInsights",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=UserAccount.getEnglish("PersonalInsights")%>&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getPersonalInsights()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PersonalInsights&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("PersonalInsights")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>






            <%
            if ( (UserAccount.isRequired("ContactEmail",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactEmail",expiredDays))&&(UserAccount.isExpiredCheck("ContactEmail",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("ContactEmail",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input maxlength="75" type=text size="80" name="ContactEmail" value="<%=UserAccount.getContactEmail()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactEmail",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

            <%
            if ( (UserAccount.isRequired("CompanyName",UserSecurityGroupID ))&&(!UserAccount.isComplete("CompanyName")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("CompanyName",expiredDays))&&(UserAccount.isExpiredCheck("CompanyName",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("CompanyName",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Business/Office Name&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input name="CompanyName" type=text value="<%=UserAccount.getCompanyName()%>" size="50" maxlength="50">
                     &nbsp;
          <%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=CompanyName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("CompanyName",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Company Name&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getCompanyName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=CompanyName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("CompanyName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactAddress1",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactAddress1",expiredDays))&&(UserAccount.isExpiredCheck("ContactAddress1",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactAddress1",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> Address&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input maxlength="50" type=text size="80" name="ContactAddress1" value="<%=UserAccount.getContactAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactAddress1&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactAddress1")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactAddress1",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact Address&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactAddress1&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactAddress1")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactAddress2",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactAddress2",expiredDays))&&(UserAccount.isExpiredCheck("ContactAddress2",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactAddress2",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> Address 2&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input maxlength="20" type=text size="80" name="ContactAddress2" value="<%=UserAccount.getContactAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactAddress2&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactAddress2")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactAddress2",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact Address 2&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactAddress2&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactAddress2")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactCity",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactCity")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactCity",expiredDays))&&(UserAccount.isExpiredCheck("ContactCity",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactCity",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> City&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input maxlength="30" type=text size="80" name="ContactCity" value="<%=UserAccount.getContactCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactCity&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactCity")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactCity",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact City&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactCity&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactCity")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactStateID",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactStateID",expiredDays))&&(UserAccount.isExpiredCheck("ContactStateID",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactStateID",UserSecurityGroupID )))
            {
                        %>
                        <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> State&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><select   name="ContactStateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getContactStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactStateID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactStateID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactStateID",UserSecurityGroupID )))
            {
                        %>
                        <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact State&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getContactStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactStateID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactStateID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("ContactProvince",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactProvince")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactProvince",expiredDays))&&(UserAccount.isExpiredCheck("ContactProvince",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactProvince",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> Province, District, State&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input maxlength="100" type=text size="80" name="ContactProvince" value="<%=UserAccount.getContactProvince()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactProvince&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactProvince")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactProvince",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact Province, District, State&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactProvince&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactProvince")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactZIP",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactZIP",expiredDays))&&(UserAccount.isExpiredCheck("ContactZIP",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactZIP",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> ZIP&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input maxlength="50" type=text size="80" name="ContactZIP" value="<%=UserAccount.getContactZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactZIP&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactZIP")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactZIP",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact ZIP&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactZIP&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactZIP")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactCountryID",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactCountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactCountryID",expiredDays))&&(UserAccount.isExpiredCheck("ContactCountryID",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactCountryID",UserSecurityGroupID )))
            {
                        %>
                        <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> Country&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><select   name="ContactCountryID" ><jsp:include page="../generic/tCountryLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getContactCountryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactCountryID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactCountryID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactCountryID",UserSecurityGroupID )))
            {
                        %>
                        <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact Country&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getContactCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactCountryID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactCountryID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("ContactPhone",UserSecurityGroupID ))&&(!UserAccount.isComplete("ContactPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactPhone",expiredDays))&&(UserAccount.isExpiredCheck("ContactPhone",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("ContactPhone",UserSecurityGroupID )))
            {
                        %>
                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> Phone&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input name="ContactPhone" type=text class="titleSub1" value="<%=UserAccount.getContactPhone()%>" size="50" maxlength="50">
                     &nbsp;
                     <%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactPhone&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactPhone")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

                     <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b> Fax&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><input name="ContactFax" type=text class="titleSub1" value="<%=UserAccount.getContactFax()%>" size="50" maxlength="50">
                     &nbsp;
                     <%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactFax&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactFax")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

                        <%
            }
            else if ((UserAccount.isRead("ContactPhone",UserSecurityGroupID )))
            {
                        %>
                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact Phone: &nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactPhone&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactPhone")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

                         <tr><td valign=top bgcolor="#E3EADF"><p class=<%=theClass%> ><b>Contact Fax:&nbsp;</b></p></td><td valign=top bgcolor="#E3EADF"><p><%=UserAccount.getContactFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ContactPhone&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactFax")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("UserNPI",UserSecurityGroupID))&&(!UserAccount.isComplete("UserNPI")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("UserNPI",expiredDays))&&(UserAccount.isExpiredCheck("UserNPI",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("UserNPI",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>UserNPI&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="UserNPI" value="<%=UserAccount.getUserNPI()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=UserNPI&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("UserNPI")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("UserNPI",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>User NPI&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getUserNPI()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=UserNPI&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("UserNPI")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("UserStateLicense",UserSecurityGroupID))&&(!UserAccount.isComplete("UserStateLicense")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("UserStateLicense",expiredDays))&&(UserAccount.isExpiredCheck("UserStateLicense",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("UserStateLicense",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>User State License&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="UserStateLicense" value="<%=UserAccount.getUserStateLicense()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=UserStateLicense&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("UserStateLicense")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("UserStateLicense",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>UserStateLicense&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getUserStateLicense()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=UserStateLicense&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("UserStateLicense")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("UserStateLicenseDesc",UserSecurityGroupID))&&(!UserAccount.isComplete("UserStateLicenseDesc")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("UserStateLicenseDesc",expiredDays))&&(UserAccount.isExpiredCheck("UserStateLicenseDesc",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("UserStateLicenseDesc",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>User State License desc</b></p></td><td valign=top><p><input maxlength="10" type=text size="80" name="UserStateLicenseDesc" value="<%=UserAccount.getUserStateLicenseDesc()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=UserStateLicenseDesc&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("UserStateLicenseDesc")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
          <%
            }
            else if (false&&(UserAccount.isRead("UserStateLicenseDesc",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>UserStateLicenseDesc&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getUserStateLicenseDesc()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=UserStateLicenseDesc&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("UserStateLicenseDesc")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("Comments",UserSecurityGroupID ))&&(!UserAccount.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("Comments",expiredDays))&&(UserAccount.isExpiredCheck("Comments",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("Comments",UserSecurityGroupID )))
            {
                        %>
                       <tr><td valign=top bgcolor="#ECECEC"><p class=<%=theClass%> ><b><%=UserAccount.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top bgcolor="#ECECEC"><p><textarea onKeyDown="textAreaStop(this,200)" rows="3" name="Comments" cols="60"><%=UserAccount.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("Comments")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("Comments",UserSecurityGroupID )))
            {
                        %>
                       <tr><td valign=top bgcolor="#ECECEC"> <p class=<%=theClass%> ><b><%=UserAccount.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top bgcolor="#ECECEC"><p><%=UserAccount.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("Comments")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("ImportantNotes",UserSecurityGroupID))&&(!UserAccount.isComplete("ImportantNotes")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ImportantNotes",expiredDays))&&(UserAccount.isExpiredCheck("ImportantNotes",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("ImportantNotes",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#FFD595"><p class=<%=theClass%> ><b>User Notes&nbsp;</b></p></td><td valign=top bgcolor="#FFD595"><p><textarea onKeyDown="textAreaStop(this,200)" rows="6" name="ImportantNotes" cols="60"><%=UserAccount.getImportantNotes()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ImportantNotes&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ImportantNotes")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ImportantNotes",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#FFD595"> <p class=<%=theClass%> ><b><%=UserAccount.getEnglish("ImportantNotes")%>&nbsp;</b></p></td><td valign=top bgcolor="#FFD595"><p><%=UserAccount.getImportantNotes()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ImportantNotes&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ImportantNotes")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




          <%
            if ( (UserAccount.isRequired("ImportantNotes_Alert",UserSecurityGroupID))&&(!UserAccount.isComplete("ImportantNotes_Alert")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ImportantNotes_Alert",expiredDays))&&(UserAccount.isExpiredCheck("ImportantNotes_Alert",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
          <%
            if (false&&(UserAccount.isWrite("ImportantNotes_Alert",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#FFD595"><p class=<%=theClass%> ><b>Pop-Up User  Notes</b></p></td><td valign=top bgcolor="#FFD595"><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="ImportantNotes_Alert" cols="60"><%=UserAccount.getImportantNotes_Alert()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ImportantNotes_Alert&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ImportantNotes_Alert")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ImportantNotes_Alert",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#FFD595"> <p class=<%=theClass%> ><b><%=UserAccount.getEnglish("ImportantNotes_Alert")%>&nbsp;</b></p></td><td valign=top bgcolor="#FFD595"><p><%=UserAccount.getImportantNotes_Alert()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=ImportantNotes_Alert&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ImportantNotes_Alert")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("EmailAlertNotes_Alert",UserSecurityGroupID))&&(!UserAccount.isComplete("EmailAlertNotes_Alert")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("EmailAlertNotes_Alert",expiredDays))&&(UserAccount.isExpiredCheck("EmailAlertNotes_Alert",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("EmailAlertNotes_Alert",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#FFD595"><p class=<%=theClass%> ><b>Pop-Up User Notes - for Notifications</b></p></td><td valign=top bgcolor="#FFD595"><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="EmailAlertNotes_Alert" cols="60"><%=UserAccount.getEmailAlertNotes_Alert()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=EmailAlertNotes_Alert&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("EmailAlertNotes_Alert")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("EmailAlertNotes_Alert",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#FFD595"> <p class=<%=theClass%> ><b><%=UserAccount.getEnglish("EmailAlertNotes_Alert")%>&nbsp;</b></p></td><td valign=top bgcolor="#FFD595"><p><%=UserAccount.getEmailAlertNotes_Alert()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=EmailAlertNotes_Alert&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("EmailAlertNotes_Alert")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (UserAccount.isRequired("SelectMRI_ID",UserSecurityGroupID))&&(!UserAccount.isComplete("SelectMRI_ID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("SelectMRI_ID",expiredDays))&&(UserAccount.isExpiredCheck("SelectMRI_ID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("SelectMRI_ID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top bgcolor="#666666"><p class=<%=theClass%> ><b>SelectMRI_ID&nbsp;</b></p></td><td valign=top bgcolor="#666666"><p><input maxlength="20" type=text size="80" name="SelectMRI_ID" value="<%=UserAccount.getSelectMRI_ID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=SelectMRI_ID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SelectMRI_ID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("SelectMRI_ID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top bgcolor="#666666"><p class=<%=theClass%> ><b>SelectMRI_ID&nbsp;</b></p></td><td valign=top bgcolor="#666666"><p><%=UserAccount.getSelectMRI_ID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=SelectMRI_ID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SelectMRI_ID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("SelectMRI_Notes",UserSecurityGroupID))&&(!UserAccount.isComplete("SelectMRI_Notes")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("SelectMRI_Notes",expiredDays))&&(UserAccount.isExpiredCheck("SelectMRI_Notes",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("SelectMRI_Notes",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#666666"><p class=<%=theClass%> ><b><%=UserAccount.getEnglish("SelectMRI_Notes")%>&nbsp;</b></p></td><td valign=top bgcolor="#666666"><p><textarea onKeyDown="textAreaStop(this,5000)" rows="2" name="SelectMRI_Notes" cols="40" maxlength=5000><%=UserAccount.getSelectMRI_Notes()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=SelectMRI_Notes&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SelectMRI_Notes")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("SelectMRI_Notes",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top bgcolor="#666666"> <p class=<%=theClass%> ><b><%=UserAccount.getEnglish("SelectMRI_Notes")%>&nbsp;</b></p></td><td valign=top bgcolor="#666666"><p><%=UserAccount.getSelectMRI_Notes()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=SelectMRI_Notes&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SelectMRI_Notes")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (UserAccount.isRequired("SelectMRI_UserType",UserSecurityGroupID))&&(!UserAccount.isComplete("SelectMRI_UserType")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("SelectMRI_UserType",expiredDays))&&(UserAccount.isExpiredCheck("SelectMRI_UserType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("SelectMRI_UserType",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top bgcolor="#666666"><p class=<%=theClass%> ><b>SelectMRI_UserType&nbsp;</b></p></td><td valign=top bgcolor="#666666"><p><input maxlength="20" type=text size="80" name="SelectMRI_UserType" value="<%=UserAccount.getSelectMRI_UserType()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=SelectMRI_UserType&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SelectMRI_UserType")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("SelectMRI_UserType",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top bgcolor="#666666"><p class=<%=theClass%> ><b>SelectMRI_UserType&nbsp;</b></p></td><td valign=top bgcolor="#666666"><p><%=UserAccount.getSelectMRI_UserType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('../auditView_Field.jsp?sFieldName=SelectMRI_UserType&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("SelectMRI_UserType")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=gsn_nim3b/images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

         <tr>
           <td colspan="2" valign=top bgcolor="#EAEEF2" class="titleSub1" >Sales Information</td>
          </tr>



            <%
            if ( (UserAccount.isRequired("TotalMonthlyMRIs",UserSecurityGroupID))&&(!UserAccount.isComplete("TotalMonthlyMRIs")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("TotalMonthlyMRIs",expiredDays))&&(UserAccount.isExpiredCheck("TotalMonthlyMRIs",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("TotalMonthlyMRIs",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top bgcolor="#EAEEF2"><p class=<%=theClass%> ><b>Monthly Referrals (Capacity)&nbsp;</b></p></td><td valign=top bgcolor="#EAEEF2"><p><input maxlength="20" type=text size="10" name="TotalMonthlyMRIs" value="<%=UserAccount.getTotalMonthlyMRIs()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TotalMonthlyMRIs&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("TotalMonthlyMRIs")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("TotalMonthlyMRIs",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top bgcolor="#EAEEF2"><p class=<%=theClass%> ><b>TotalMonthlyMRIs&nbsp;</b></p></td><td valign=top bgcolor="#EAEEF2"><p><%=UserAccount.getTotalMonthlyMRIs()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TotalMonthlyMRIs&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("TotalMonthlyMRIs")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ExpectedMonthlyMRIs",UserSecurityGroupID))&&(!UserAccount.isComplete("ExpectedMonthlyMRIs")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ExpectedMonthlyMRIs",expiredDays))&&(UserAccount.isExpiredCheck("ExpectedMonthlyMRIs",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ExpectedMonthlyMRIs",UserSecurityGroupID)))
            {
                        %>
                     <tr>
          <td valign=top bgcolor="#EAEEF2"><p class=<%=theClass%> ><b>Monthly Referrals<strong>(</strong></b><strong>Expected)</strong></p></td><td valign=top bgcolor="#EAEEF2"><p><input maxlength="20" type=text size="10" name="ExpectedMonthlyMRIs" value="<%=UserAccount.getExpectedMonthlyMRIs()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpectedMonthlyMRIs&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ExpectedMonthlyMRIs")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ExpectedMonthlyMRIs",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top bgcolor="#EAEEF2"><p class=<%=theClass%> ><b>ExpectedMonthlyMRIs&nbsp;</b></p></td><td valign=top bgcolor="#EAEEF2"><p><%=UserAccount.getExpectedMonthlyMRIs()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ExpectedMonthlyMRIs&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ExpectedMonthlyMRIs")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>





         <tr>
           <td valign=top >&nbsp;</td>
           <td valign=top >&nbsp;</td>
         </tr>
         <tr>
           <td valign=top >&nbsp;</td>
           <td valign=top >&nbsp;</td>
         </tr>
         <tr>
           <td valign=top >&nbsp;</td>
           <td valign=top >&nbsp;</td>
         </tr>
         <tr>
           <td valign=top >&nbsp;</td>
           <td valign=top >&nbsp;</td>
         </tr>




            <%
            if ( (UserAccount.isRequired("PromoCode",UserSecurityGroupID ))&&(!UserAccount.isComplete("PromoCode")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("PromoCode",expiredDays))&&(UserAccount.isExpiredCheck("PromoCode",UserSecurityGroupID )))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("PromoCode",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else if ((UserAccount.isRead("PromoCode",UserSecurityGroupID )))
            {
                        %>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

        </table>







        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <p><select name=routePageReference class="buttonSleekLarge">
            <option value="sLocalChildReturnPage" selected="selected">Save & Stay</option>
            </select></p>
            <p><input name=Submit type=Submit class="titleBlue" value=Save>
            </p>
        <%}%>
      </form>
      <%
  }
  else
  {
   response.sendRedirect("/errorhandler.jsp");
  }

%>    </td>
    <td align="center" valign="top" bgcolor="#E4E7EE"><p class="title">Similar Accounts</p>
      <table width="100%" border="1" cellpadding="5" cellspacing="0" bordercolor="#666666" bgcolor="#C7CDDD">
      <tr>
        <td class="titleSub1">Contacts with the same Logon Name may cause errors. </td>
      </tr>
      <tr>
        <td><%
  {//localize brackets

%>
          <table width="100%" border="1" cellpadding="3" cellspacing="0" bgcolor="#F3F4F8">
            <tr>
              <td class="tdHeaderAlt">Status</td>
              <td class="tdHeaderAlt">ContactID</td>
              <td class="tdHeaderAlt">Logon Name</td>
              <td class="tdHeaderAlt">Payer (Branch)</td>
              <td class="tdHeaderAlt">First</td>
              <td class="tdHeaderAlt">Last</td>
              <td class="tdHeaderAlt">Phone</td>
             </tr>
            <%
	searchDB2 mySSAct = new searchDB2();
	db_PreparedStatementObject myDPSO = null;
	java.sql.ResultSet myRSAct = null;
	try
	{
		String mySQL55 = "None";
		mySQL55 = "SELECT tuseraccount.userid, tuseraccount.logonusername, tuseraccount.contactfirstname, tuseraccount.contactlastname, tuseraccount.contactphone, tnim3_payermaster.payername AS payername FROM tuseraccount LEFT JOIN tnim3_payermaster ON tuseraccount.payerid = tnim3_payermaster.payerid where (upper(logonusername) = upper(?) and logonusername>'' AND upper(logonusername)<>upper('Declined')) or (   upper(contactfirstname) % upper(?) AND upper(contactlastname) % upper(?) and upper(contactphone) % upper(?)  )";
		myDPSO = new db_PreparedStatementObject(mySQL55);
		myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,UserAccount.getLogonUserName()));
		myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,UserAccount.getContactFirstName()));
		myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,UserAccount.getContactLastName()));
		myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,UserAccount.getContactPhone()));
		//out.println(mySQL55);
		if (mySQL55.equalsIgnoreCase("None"))
		{
		}
		else
		{
		   myRSAct = mySSAct.executePreparedStatement(myDPSO);
		}
			
		
		
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
	try
	{
	   if (myRSAct!=null)
	   {
		   int tempcnt=0;
		   while (myRSAct!=null&&myRSAct.next())
		   {
			   tempcnt++;
			   String tempClass = " class = \"tdBaseAlt_Action1\" ";
			   int tempUserID = new Integer(myRSAct.getString("UserID"));
			   if (tempUserID==UserAccount.getUserID())
			   {
				   tempClass = " class = \"tdBaseAlt_Create\" ";
			   }
			   else if (myRSAct.getString("LogonUserName").equalsIgnoreCase(UserAccount.getLogonUserName()))
			   {
				   tempClass = " class = \"tdBaseAlt_Action3\" ";
			   }
			   String tempAltClass = " class = \"tdBase\" ";
			   if (tempcnt%2==0)
			   {
				   tempAltClass = " class = \"tdBaseAlt\" ";
			   }
																										
	%>
            <tr <%=tempAltClass%> >
              <td  <%=tempClass%> ><%if (tempUserID==UserAccount.getUserID()){out.print("Current");}else if (myRSAct.getString("LogonUserName").equalsIgnoreCase(UserAccount.getLogonUserName())){out.print("WARNING");}else{out.print("Possible Match");}%></td>
              <td ><%=myRSAct.getString("Userid")%></td>
              <td ><%=myRSAct.getString("LogonUserName")%></td>
              <td ><%=myRSAct.getString("payername")%></td>
              <td ><%=myRSAct.getString("contactfirstname")%></td>
              <td ><%=myRSAct.getString("contactlastname")%></td>
              <td ><%=myRSAct.getString("contactphone")%></td>
              </tr>
            <%		
		   }
	   }
	   else
	   {
 		   out.println("No Results");
	   }
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
	finally
	{
		mySSAct.closeAll();
	}
%>
          </table>
          <%
  }//localize
%></td>
      </tr>
    </table>
      <p class="title">Contact Interactions</p>
      <table width="100%" border="1" cellpadding="5" cellspacing="0" bordercolor="#666666" bgcolor="#C7CDDD">
        <tr>
          <td class="titleSub1"><input type="button" class="inputButton_md_Create"  onclick="this.disabled=true;modalPost('CASEID', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_CommTrack_Sales_form.jsp&EDIT=newsales&hideCase=y&Link_UserID=<%=UserAccount.getUserID()%>','Add CT...','dialogWidth:600px;dialogHeight:500px','status=yes,scrollbars=yes,resizable=yes,width=600,height=600'));document.location=document.location;" value="Add Interaction">&nbsp;</td>
        </tr>
        <tr>
          <td><%
  {//localize brackets

%>
            <table width="100%" border="1" cellpadding="3" cellspacing="0" bgcolor="#F3F4F8">
              <tr>
                <td class="tdHeaderAlt">Date</td>
                <td class="tdHeaderAlt">Type</td>
                <td class="tdHeaderAlt">Subject</td>
                <td class="tdHeaderAlt">Owner</td>
              </tr>
              <%
	searchDB2 mySSAct = new searchDB2();
	db_PreparedStatementObject myDPSO = null;
	java.sql.ResultSet myRSAct = null;
	try
	{
		String mySQL55 = "None";
		mySQL55 = "select commtrackid from tnim3_commtrack where link_userid = ? order by CommStart desc";
		myDPSO = new db_PreparedStatementObject(mySQL55);
		myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,UserAccount.getUserID()+""));
		//out.println(mySQL55);
		if (mySQL55.equalsIgnoreCase("None"))
		{
		}
		else
		{
		   myRSAct = mySSAct.executePreparedStatement(myDPSO);
		}
			
		
		
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
	try
	{
	   if (myRSAct!=null)
	   {
		   int tempcnt=0;
		   while (myRSAct!=null&&myRSAct.next())
		   {
			   tempcnt++;
			   int tempCTID = new Integer(myRSAct.getString("commtrackid"));
			   bltNIM3_CommTrack myNCT = new bltNIM3_CommTrack(tempCTID);
			   bltUserAccount tempUA = new bltUserAccount(myNCT.getIntUserID());
			   
			   String tempAltClass = " class = \"tdBase\" ";
			   if (tempcnt%2==0)
			   {
				   tempAltClass = " class = \"tdBaseAlt\" ";
			   }
																										
			%>
    		  <tr onClick="if (document.getElementById('commTrack_<%=tempcnt%>').style.display == 'block'){document.getElementById('commTrack_<%=tempcnt%>').style.display = 'none';}else{document.getElementById('commTrack_<%=tempcnt%>').style.display = 'block';}" class="tdBaseAlt">
                <td><strong><%=displayDateTimeSDF.format(myNCT.getCommStart())%></strong><br>Duration: <%=workingDays.getWorkingHours_displayEasy (myNCT.getCommStart(), myNCT.getCommEnd(), 0, 24, true)%> </td>
                <td><jsp:include page="../generic/tCommTrackTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myNCT.getCommTypeID()%>" /></jsp:include></td>
                <td><%=myNCT.getMessageSubject()%></td>
                <td><%=tempUA.getContactFirstName()%>&nbsp;<%=tempUA.getContactLastName()%> [ID:<%=tempUA.getUserID()%>]</td>
              </tr>
			  
    		  <tr>
                <td colspan="4"><div id="commTrack_<%=tempcnt%>" style="display:none;"><%=myNCT.getMessageText()%></div></td>
              </tr>
              
              <%		
		   }
	   }
	   else
	   {
 		   out.println("No Results");
	   }
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
	finally
	{
		mySSAct.closeAll();
	}
%>
            </table>
          <%
  }//localize
%></td>
        </tr>
      </table>
      <p><span class="title">Referral Activity - Summary</span><br />
      (Includes all cases where contact is linked as Adjuster or Referring Doctor)</p>
      <table width="100%" border="1" cellpadding="5" cellspacing="0" bordercolor="#666666" bgcolor="#C7CDDD">
        <tr>
          <td><%
  {//localize brackets

			java.util.Vector<NIM3_Reporting_UserActivity> myNIM3_Reporting_UserActivity = NIMUtils.getReporting_UserActivity(UserAccount, 2011);

%>

            <table border="1" align="center" cellpadding="3" cellspacing="0" bgcolor="#F3F4F8">
              <tr class="tdHeaderAltDark">
                <td align="center" class="tdHeaderAltDark">Year</td>
                <td align="center" class="tdHeaderAltDark">Month</td>
                <td align="center" class="tdHeaderAltDark">Total</td>
                <td align="center" class="tdHeaderAltDark">MRs</td>
                <td align="center" class="tdHeaderAltDark">CTs</td>
                <td align="center" class="tdHeaderAltDark">EMGs</td>
                <td align="center" class="tdHeaderAltDark">Other</td>
                <td align="center" class="tdHeaderAltDark">Courtesy</td>
                <td align="center" class="tdHeaderAltDark">Voids</td>
              </tr>
              <%
			  int countTotals = 0;
			  int countMRI = 0;
			  int countCT = 0;
			  int countEMG = 0;
			  int countCourt = 0;
			  int countOther = 0;
			  int countVoid = 0;
			  for (int i=0;i<myNIM3_Reporting_UserActivity.size();i++){
				  NIM3_Reporting_UserActivity tempNRUA = (NIM3_Reporting_UserActivity) myNIM3_Reporting_UserActivity.elementAt(i);
				   String tempAltClass = " class = \"tdBase\" ";
				   int cutGreenTotal = 5;
				   int cutGreen = 210;
				   if (UserAccount.getExpectedMonthlyMRIs()>0)
				   {
					   cutGreenTotal = UserAccount.getExpectedMonthlyMRIs();
				   }
				   countTotals +=tempNRUA.getCountTotal(false, true);
				   countMRI += tempNRUA.getCountMR();
				   countCT += tempNRUA.getCountCT();
				   countEMG += tempNRUA.getCountEMG();
				   countOther += tempNRUA.getCountOther();
				   countCourt += tempNRUA.getCountCourtesy();
				   countVoid += tempNRUA.getCountVoid();
				   
				   
				  %>
                      <tr onMouseOver="this.className='highlight'" onMouseOut="this.className='tdBase'" >
                        <td align="center" ><%=tempNRUA.getYear()%></td>
                        <td align="center" ><%=dateUtils.getMonthAsText(tempNRUA.getMonth())%></td>
                        <td align="center" <% if (tempNRUA.getCountTotal(false, false)>=cutGreenTotal){%>class="tdBaseAltGreen"<%}else if (tempNRUA.getCountTotal(false, false)>0){%>class="tdBase"<%}else{%>class="tdBaseFade1"<%}%> ><%=tempNRUA.getCountTotal(false, true)%></td>
                        <td align="center" <% if (tempNRUA.getCountMR()>=cutGreen){%>class="tdBaseAltGreen"<%}else if (tempNRUA.getCountMR()>0){%>class="tdBase"<%}else{%>class="tdBaseFade1"<%}%> ><%=tempNRUA.getCountMR()%></td>
                        <td align="center" <% if (tempNRUA.getCountCT()>=cutGreen){%>class="tdBaseAltGreen"<%}else if (tempNRUA.getCountCT()>0){%>class="tdBase"<%}else{%>class="tdBaseFade1"<%}%> ><%=tempNRUA.getCountCT()%></td>
                        <td align="center" <% if (tempNRUA.getCountEMG()>=cutGreen){%>class="tdBaseAltGreen"<%}else if (tempNRUA.getCountEMG()>0){%>class="tdBase"<%}else{%>class="tdBaseFade1"<%}%> ><%=tempNRUA.getCountEMG()%></td>
                        <td align="center" <% if (tempNRUA.getCountOther()>=cutGreen){%>class="tdBaseAltGreen"<%}else if (tempNRUA.getCountOther()>0){%>class="tdBase"<%}else{%>class="tdBaseFade1"<%}%> ><%=tempNRUA.getCountOther()%></td>
                        <td align="center" <% if (tempNRUA.getCountCourtesy()>=cutGreen){%>class="tdBaseAltRed"<%}else if (tempNRUA.getCountCourtesy()>0){%>class="tdBase"<%}else{%>class="tdBaseFade1"<%}%> ><%=tempNRUA.getCountCourtesy()%></td>
                        <td align="center" <% if (tempNRUA.getCountVoid()>=cutGreen){%>class="tdBaseAltRed"<%}else if (tempNRUA.getCountVoid()>0){%>class="tdBase"<%}else{%>class="tdBaseFade1"<%}%> ><%=tempNRUA.getCountVoid()%></td>
                      </tr>
              <%
				  if (i==(myNIM3_Reporting_UserActivity.size()-1)){
					  %>
                       <tr class="tdHeaderAlt" onMouseOver="this.className='highlight'" onMouseOut="this.className='tdBase'" >
                            <td align="center" class="tdHeaderAlt" colspan="2" >Totals</td>
                            <td align="center"  class="tdHeaderAlt"><%=countTotals%></td>
                            <td align="center"  class="tdHeaderAlt"><%=countMRI%></td>
                            <td align="center"  class="tdHeaderAlt"><%=countCT%></td>
                            <td align="center" class="tdHeaderAlt" ><%=countEMG%></td>
                            <td align="center" class="tdHeaderAlt" ><%=countOther%></td>
                            <td align="center"  class="tdHeaderAlt"><%=countCourt%></td>
                            <td align="center" class="tdHeaderAlt" ><%=countVoid%></td>
                          </tr>                      
                      <%
				  }
			  }
			  %>
            </table>
          <%
  }//localize
%></td>
        </tr>
      </table>
<p><span class="title">Referral Activity - Details</span><br />
  (Includes all cases that this contact is linked to)
</p>
      <table width="100%" border="1" cellpadding="5" cellspacing="0" bordercolor="#666666" bgcolor="#C7CDDD">
        <tr>
        <td class="titleSub1">Last ~20 Cases</td>
      </tr>
      <tr>
        <td>
          <%
  {//localize brackets

%>
          <table width="100%" border="1" cellpadding="3" cellspacing="0" bgcolor="#F3F4F8">
            <tr>
              <td class="tdHeaderAlt">Received</td>
              <td class="tdHeaderAlt">Source</td>
              <td class="tdHeaderAlt">Patient<br />
                Last</td>
              <td class="tdHeaderAlt">Claim #</td>
              <td class="tdHeaderAlt">ScanPass</td>
              <td class="tdHeaderAlt">Type</td>
              <td class="tdHeaderAlt">Status</td>
              <td class="tdHeaderAlt">Scheduled</td>
              <td class="tdHeaderAlt">Appointment</td>
              <td class="tdHeaderAlt">Case Payer</td>
              </tr>
            <%
	searchDB2 mySSAct = new searchDB2();
	java.sql.ResultSet myRSAct = null;;
	try
	{
		String mySQL55 = "None";
		if (UserAccount.getAccountType().equalsIgnoreCase("AdjusterID")||UserAccount.getAccountType().equalsIgnoreCase("AdjusterID2")||UserAccount.getAccountType().equalsIgnoreCase("AdjusterID3"))
		{
			mySQL55 = ("select RefSourceUA.UserID as RefUAID, RefSourceUA.ContactFirstName || ' ' || RefSourceUA.ContactLastName as \"ReferralSource_Full\", tNIM3_Encounter.timetrack_reqsched, tNIM3_PayerMaster.payername as PayerName, tNIM3_PayerMaster.payertypeid, tNIM3_CaseAccount.caseclaimnumber, tNIM3_CaseAccount.patientfirstname,tNIM3_CaseAccount.patientlastname , tNIM3_Referral.receivedate, tNIM3_Encounter.encounterstatusid, tNIM3_Encounter.encountertypeid,tNIM3_Encounter.scanpass, tNIM3_Appointment.appointmenttime from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid  INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid LEFT JOIN tNIM3_Appointment on tNIM3_Encounter.appointmentid = tNIM3_Appointment.appointmentid LEFT JOIN tUserAccount as RefSourceUA on RefSourceUA.userid = tNIM3_Referral.ReferredByContactID where (tNIM3_CaseAccount.adjusterid=" + UserAccount.getUserID() +" or tNIM3_CaseAccount.nursecasemanagerid=" + UserAccount.getUserID() +" or tNIM3_CaseAccount.caseadministratorid=" + UserAccount.getUserID() +" or tNIM3_CaseAccount.caseadministrator2id=" + UserAccount.getUserID() +" or tNIM3_CaseAccount.caseadministrator3id=" + UserAccount.getUserID() +" or tNIM3_CaseAccount.caseadministrator4id=" + UserAccount.getUserID() +") order by tNIM3_Referral.receivedate Desc LIMIT 20");
		}
		else if (UserAccount.getAccountType().equalsIgnoreCase("PhysicianID"))
		{
			mySQL55 = ("select RefSourceUA.UserID as RefUAID,RefSourceUA.ContactFirstName || ' ' || RefSourceUA.ContactLastName as \"ReferralSource_Full\", tNIM3_Encounter.timetrack_reqsched, tNIM3_PayerMaster.payername as PayerName, tNIM3_PayerMaster.payertypeid,tNIM3_CaseAccount.caseclaimnumber, tNIM3_CaseAccount.patientfirstname,tNIM3_CaseAccount.patientlastname , tNIM3_Referral.receivedate, tNIM3_Encounter.encounterstatusid, tNIM3_Encounter.encountertypeid,tNIM3_Encounter.scanpass, tNIM3_Appointment.appointmenttime from tNIM3_CaseAccount INNER JOIN tNIM3_PayerMaster on tNIM3_PayerMaster.payerid = tNIM3_CaseAccount.payerid INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid LEFT JOIN tNIM3_Appointment on tNIM3_Encounter.appointmentid = tNIM3_Appointment.appointmentid LEFT JOIN tUserAccount as RefSourceUA on RefSourceUA.userid = tNIM3_Referral.ReferredByContactID  where tNIM3_Referral.ReferringPhysicianID=" + UserAccount.getUserID() +" order by tNIM3_Referral.receivedate Desc LIMIT 20");
		}
//		out.println(mySQL55);
		if (mySQL55.equalsIgnoreCase("None"))
		{
		}
		else
		{
			myRSAct = mySSAct.executeStatement(mySQL55);
		}
			
		
		
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
	try
	{
	   if (myRSAct!=null)
	   {
		   while (myRSAct!=null&&myRSAct.next())
		   {
	%>
				<tr>
				  <td nowrap="nowrap"><%=PLCUtils.getDisplayDateWithTime(dbdfFull.parse(myRSAct.getString("receivedate")))%></td>
				  <td <%if (myRSAct.getString("RefUAID").equalsIgnoreCase(UserAccount.getUserID().toString())) {%> class="tdBaseAltGreen" <%} %>><%=myRSAct.getString("ReferralSource_Full")%></td>
				  <td><%=myRSAct.getString("PatientLastName")%></td>
				  <td><%=myRSAct.getString("CaseClaimNumber")%></td>
				  <td><%=myRSAct.getString("ScanPass")%></td>
				  <td><jsp:include page="../generic/tEncounterTypeLI_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myRSAct.getString(\"EncounterTypeID\")%>"  /></jsp:include></td>
				  <td><jsp:include page="../generic/tEncounterStatusLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myRSAct.getString(\"EncounterStatusID\")%>"  /></jsp:include></td>
				  <td nowrap="nowrap"><%=PLCUtils.getDisplayDateWithTime(dbdfFull.parse(myRSAct.getString("timetrack_reqsched")))%>&nbsp;</td>
				  <td nowrap="nowrap"><%=PLCUtils.getDisplayDateWithTime(dbdfFull.parse(myRSAct.getString("appointmenttime")))%>&nbsp;</td>
				  <td><strong><%=myRSAct.getString("PayerName")%></strong><br />Type: <%=myRSAct.getString("PayerTypeID")%></td>
			  </tr>
				<%		
		   }
	   }
	   else
	   {
 		   out.println("No Results");
	   }
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
%>
            </table>
          <%
  }//localize
%>		</td>
      </tr>
      </table></td>
    </tr></table>

</body>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>