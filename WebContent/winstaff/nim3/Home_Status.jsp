<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="refresh" content="300">

<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
</head>
<SCRIPT language="JavaScript1.2">
function openStatus()
{
	testwindow= window.open ("Home_Status.jsp", "HomeStatus","location=1,status=1,scrollbars=1,resizable=1,width=900,height=700");
	testwindow.moveTo(0,0);
}
</SCRIPT>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
	tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear_fade.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


<style type="text/css">
<!--
body {
	background-color: #666;
}
-->
</style>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler) 
    {
        accessValid = true;
    }
    else if (pageControllerHash.containsKey("iAdjusterID")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID        =    (Integer)pageControllerHash.get("iAdjusterID");
		isAdjuster = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID2")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster2 = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID3")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster3 = true;
        accessValid = true;
	}
	
	
  //page security
  if (accessValid&&isScheduler2)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	
		if (isScheduler)
	  {
		  String theScheduler = CurrentUserAccount.getUserID().toString();
		  String myAT_Where = "";
		  if (false&&request.getParameter("theScheduler")!=null&&!request.getParameter("theScheduler").equalsIgnoreCase("") )
		  {
			  theScheduler = request.getParameter("theScheduler");
		  }
		  if (false&&!theScheduler.equalsIgnoreCase("0"))
		  {
			  myAT_Where =  " AND tNIM3_CaseAccount.assignedtoid = " + theScheduler + " ";
		  }
		  
	  %>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td ><table width="100%" border="2" cellspacing="0" cellpadding="5">
          <tr>
            <td valign="top" bgcolor="#E3E3E3" class="title">NextImageGrid - ScanPass Worklist</td>
            <td align="right" valign="top" bgcolor="#E3E3E3" class="title">Last Updated: <%=PLCUtils.getDisplayDateWithTime(PLCUtils.getNowDate(false))%></td>
            </tr>
          <%
		 if (isScheduler2&&false)
		 {
			 %>
          <tr class="tdHeaderAlt">
            <td width="100%" valign="top" bgcolor="#E3E3E3"><form action="#" method="POST" name="SchedulerSelect" class="tdHeader">
              Welcome Scheduler Admin (L2+). You are viewing the worklist for:
              <select name="theScheduler" class="titleSub1" onChange="document.forms.SchedulerSelect.submit()">
                <option value="0">Show All</option>
                <%
{
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("select Userid,LogonUserName, contactfirstname, contactlastname from tUserAccount where accounttype ='SchedulerID' or accounttype='SchedulerID2' or accounttype='SchedulerID3' order by LogonUserName");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
				if (myRS.getString("UserID").equalsIgnoreCase(theScheduler))
				{
					%>
                <option selected value="<%=myRS.getString("UserID")%>"><%=myRS.getString("LogonUserName")%> [<%=myRS.getString("contactfirstname")%>, <%=myRS.getString("contactlastname")%>]</option>
                <%
				}
				else
				{
					%>
                <option  value="<%=myRS.getString("UserID")%>"><%=myRS.getString("LogonUserName")%> [<%=myRS.getString("contactfirstname")%>, <%=myRS.getString("contactlastname")%>]</option>
                <%
				}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
%>
                </select>
            </form></td>
            <td width="100%" valign="top" bgcolor="#E3E3E3">&nbsp;</td>
            </tr>
          <%}%> 
          <tr>
            <td valign="top" bgcolor="#E3E3E3"><table width="100%" border="0" cellspacing="0" cellpadding="3">
              <tr class="tdHeaderAlt">
                <td colspan="5" class="tdHeaderAltDark">Needs to be Scheduled</td>
              </tr>
              <tr class="tdHeaderAlt">
                <td>Assigned To</td>
                <td>Name</td>
                <td>ScanPass</td>
                <td>Status</td>
                <td>Hours Since Rec</td>
              </tr>
              <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_NOT_SCHEDULED,myAT_Where); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);
		long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myEO.NIM3_Referral.getReceiveDate().getTime())/(60*60*1000);
		String theClass2 = "tdBase";
		if (iHoursPassed<2)
		{
			theClass2 = "tdBaseFade1";
		}
		else if (iHoursPassed<4)
		{
			theClass2 = "tdBase";
		}
		else if (iHoursPassed<24)
		{
			theClass2 = "tdBaseAlt_Action1";
		}
		else if (iHoursPassed<48)
		{
			theClass2 = "tdBaseAlt_Action2";
		}
		else
		{
			theClass2 = "tdBaseAlt_Action3";
		}
		
		
		%>
		  <tr >
			<td><%=myEO.Case_AssignedTo.getLogonUserName()%></td>
			<td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_CaseAccount.getPatientLastName()%>, <%=myEO.NIM3_CaseAccount.getPatientFirstName()%></a></td>
			<td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_Encounter.getScanPass()%></a></td>
			<td ><%if (NIMUtils.isEncounterReadyToSchedule(iEncounterID,true)){out.print("Ready to Schedule");}else{out.print("Incomplete Data");}%></td>
			<td class=<%=theClass2%>><% if (iHoursPassed>10000){out.print("Invalid Receive Date");}else{out.print(iHoursPassed + " hours");}%></td>
		  </tr>
		  <%
	}
}
				%>
            </table></td>
            <td valign="top" bgcolor="#E3E3E3"><table width="100%" border="0" cellspacing="0" cellpadding="3">
              <tr class="tdHeaderAlt">
                <td colspan="5" class="tdHeaderAltDark">Report Delivery</td>
              </tr>
              <tr class="tdHeaderAlt">
                <td>Assigned To</td>
                <td>Name</td>
                <td>ScanPass</td>
                <td>Status</td>
                <td>Hours since DOS</td>
              </tr>
              <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_REPORTS_DUE,myAT_Where); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);
		long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myEO.NIM3_Encounter.getDateOfService().getTime())/(60*60*1000);
		String sStatus = "";
		if (myEO.NIM3_Encounter.getReportFileID().intValue()==0)
		{
			sStatus = "No Report on File";
		}
		else if (myEO.NIM3_Encounter.getTimeTrack_ReqRpReview().before(NIMUtils.getBeforeTime()))
		{
			sStatus = "Pending Report Review";
		}
		else 
		{
			sStatus = "Confirmations Not Sent";
		}
												
		String theClass2 = "tdBase";
		if (iHoursPassed<5)
		{
			theClass2 = "tdBaseFade1";
		}
		if (iHoursPassed<8)
		{
			theClass2 = "tdBase";
		}
		else if (iHoursPassed<12)
		{
			theClass2 = "tdBaseAlt_Action1";
		}
		else if (iHoursPassed<24)
		{
			theClass2 = "tdBaseAlt_Action2";
		}
		else
		{
			theClass2 = "tdBaseAlt_Action3";
		}
		
		
		%>
		  <tr >
			<td><%=myEO.Case_AssignedTo.getLogonUserName()%></td>
			<td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_CaseAccount.getPatientLastName()%>, <%=myEO.NIM3_CaseAccount.getPatientFirstName()%></a></td>
			<td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_Encounter.getScanPass()%></a></td>
			<td class=<%=theClass2%>><%=sStatus%></td>
			<td class=<%=theClass2%>><%=iHoursPassed%> hours</td>
		  </tr>
		  <%
	}
}
				%>
            </table></td>
            </tr>
          <tr>
            <td valign="top" bgcolor="#E3E3E3"><table width="100%" border="0" cellspacing="0" cellpadding="3">
              <tr class="tdHeaderAlt">
                <td colspan=2 class="tdHeaderAltDark">Open CommTracks </td>
              </tr>
              <tr class="tdHeaderAlt">
                <td nowrap>Name</td>
                <td nowrap >Hours Since Rec</td>
              </tr>
              <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_COMMTRACK,myAT_Where); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iCommTrackID = (Integer) myWorklist_Vector.elementAt(i);
		bltNIM3_CommTrack myCT = new bltNIM3_CommTrack(iCommTrackID);
		long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myCT.getCommStart().getTime())/(60*60*1000);
		String theClass2 = "tdBase";
		if (iHoursPassed<1)
		{
			theClass2 = "tdBaseFade1";
		}
		else if (iHoursPassed<3)
		{
			theClass2 = "tdBase";
		}
		else if (iHoursPassed<6)
		{
			theClass2 = "tdBaseAlt_Action1";
		}
		else if (iHoursPassed<12)
		{
			theClass2 = "tdBaseAlt_Action2";
		}
		else
		{
			theClass2 = "tdBaseAlt_Action3";
		}
		
		
		%>
		  <tr >
			<td><%=myCT.getMessageName()%> @ <%=myCT.getMessageCompany()%></td>
			<td  class=<%=theClass2%>><%=iHoursPassed%> hours</td>
		  </tr>
		  <%
	}
}
				%>
            </table></td>
            <td valign="top" bgcolor="#E3E3E3"><table width="100%" border="0" cellspacing="0" cellpadding="3">
              <tr class="tdHeaderAlt">
                <td colspan="4" class="tdHeaderAltDark">Next Action Alerts - Due</td>
              </tr>
              <tr class="tdHeaderAlt">
                <td>Assigned To</td>
                <td>Name</td>
                <td>ScanPass</td>
                <td>Action Due</td>
              </tr>
              <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_NAD,myAT_Where); 
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);

		String theClass2 = "tdBase";
		if (myEO.NIM3_Encounter.getNextActionDate().before(PLCUtils.getYesterday()))
		{
			theClass2 = "tdBaseAlt_Action3";
		}
		else if (myEO.NIM3_Encounter.getNextActionDate().before(PLCUtils.getToday()))
		{
			theClass2 = "tdBaseAlt_Action2";
		}
			
			%>
              <tr >
                <td><%=myEO.Case_AssignedTo.getLogonUserName()%></td>
                <td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_CaseAccount.getPatientLastName()%>, <%=myEO.NIM3_CaseAccount.getPatientFirstName()%></a></td>
                <td><a href="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=<%=myEO.NIM3_CaseAccount.getCaseID()%>&KM=p"><%=myEO.NIM3_Encounter.getScanPass()%></a></td>
                <td  class=<%=theClass2%>><%=PLCUtils.getDisplayDateWithTime(myEO.NIM3_Encounter.getNextActionDate())%></td>
              </tr>
              <%
	}
}
				%>
            </table></td>
          </tr>
          </table></td>
      </tr>
      <tr>
        <td align="center" class="instructions" >Copyright NextImage Medical 2007-2010 All Rights Reserved</td>
      </tr>
      <%
	  if (isScheduler2)
	  {
	  %>
      <%
	  }
	  else
	  {
		  %>
		  Test
          <%
	  }
	  %>
    </table></td>
  </tr>
</table>
         
            
            <!-- test -->            
            
            
          </table></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
          
          <%
	  }
	  %>

    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</body>
</html>

<script language="javascript">
hide_pws_Loading();
</script>
