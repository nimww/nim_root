<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<script type="text/javascript">
<!--
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
//-->
</script>
<body onLoad="document.getElementById('PatientLastName').focus();">
<table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM3_CaseAccount")%>



<%
//initial declaration of list class and parentID
    Integer        iPayerID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
    }
    else if (isScheduler) 
    {
        accessValid = true;
    }
	
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_Encounter_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");
	  pageControllerHash.put("sINTNext","tNIM3_CaseAccount_Encounter_PayerID_query.jsp");
	  session.setAttribute("pageControllerHash",pageControllerHash);

%>



<%
try
{

String myEncounterStatusID = "0";
String myEncounterTypeID = "0";
String myPayerID = "0";
String myCaseClaimNumber = "";
String myScanPass = "";
String myPatientFirstName = "";
String myPatientLastName = "";
String myPatientSSN = "";
String myEmployerName = "";
String myFilterBill = "0";
String myAssignedToID = CurrentUserAccount.getUserID().toString();
String orderBy = "firstname";
int startID = 0;
int defaultMax = 10;
int maxResults = defaultMax;

boolean firstDisplay = true;

try
{
	myFilterBill = request.getParameter("FilterBill").toLowerCase();
	firstDisplay=false;
}
catch (Exception e)
{
	myFilterBill = "0";
}

try
{
	myAssignedToID = request.getParameter("AssignedToID");
	firstDisplay=false;
}
catch (Exception e)
{
//myAssignedToID = CurrentUserAccount.getUserID().toString();
	myAssignedToID = "-1";
}


try
{
	maxResults = Integer.parseInt(request.getParameter("maxResults"));
	startID = Integer.parseInt(request.getParameter("startID"));
	myPayerID = request.getParameter("PayerID").toLowerCase();
	myEncounterStatusID = request.getParameter("CaseStatusID").toLowerCase();
	myEncounterTypeID = request.getParameter("EncounterTypeID").toLowerCase();
	myCaseClaimNumber = request.getParameter("CaseClaimNumber").toLowerCase().trim();
	myScanPass = request.getParameter("ScanPass").toLowerCase().trim();
	myPatientFirstName = request.getParameter("PatientFirstName").toLowerCase().trim();
	myPatientLastName = request.getParameter("PatientLastName").toLowerCase().trim();
	//myFilterBill = request.getParameter("FilterBill").toLowerCase();
	//myPatientSSN = request.getParameter("PatientSSN").toLowerCase();
	myEmployerName = request.getParameter("EmployerName").toLowerCase().trim();
	orderBy = request.getParameter("orderBy");
	firstDisplay=false;
}
catch (Exception e)
{
	maxResults = defaultMax;
	startID = 0;
	firstDisplay = true;
	myPayerID = "-1";
	myEncounterStatusID = "-1";
	myEncounterTypeID = "0";
	myCaseClaimNumber = "";
	myScanPass = "";
	myPatientFirstName = "";
	myPatientLastName = "";
	myPatientSSN = "";
	myEmployerName = "";
	//myFilterBill = "0";
	orderBy = "PatientLastName";
}
if (orderBy == null)
{
	maxResults = defaultMax;
	startID = 0;
	firstDisplay = true;
	myPayerID = "-1";
	myEncounterStatusID = "-1";
	myEncounterTypeID = "0";
	myCaseClaimNumber = "";
	myScanPass = "";
	myPatientFirstName = "";
	myPatientLastName = "";
	myPatientSSN = "";
	myEmployerName = "";
	myFilterBill = "0";
	orderBy = "ReceiveDate";
}
//firstDisplay = false;

if (myAssignedToID==null||myAssignedToID.equalsIgnoreCase(""))
{
	myAssignedToID = CurrentUserAccount.getUserID().toString();
}



Integer imyPayerID = new Integer(myPayerID);
{




%>


<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 



  <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</p>
<table border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><input class="inputButton_md_Default" type="button" onclick = "this.disabled=true;document.location ='tNIM3_CaseAccount_Encounter_PayerID_query.jsp?FilterBill=4'" value="Alert: Not Scheduled" /></td>
    <td><input class="inputButton_md_Default" type="button" onclick = "this.disabled=true;document.location ='tNIM3_CaseAccount_Encounter_PayerID_query.jsp?FilterBill=3'" value="Alert: Report Due" /></td>
    <td bgcolor="#000000">&nbsp;</td>
    <td><input class="inputButton_md_Default" type="button" onclick = "this.disabled=true;document.location ='tNIM3_CaseAccount_Encounter_PayerID_query.jsp?AssignedToID=-1'" value="All Active Cases" /></td>
    <td><input class="inputButton_md_Default" type="button" onclick = "this.disabled=true;document.location ='tNIM3_CaseAccount_Encounter_PayerID_query.jsp?AssignedToID=0'" value="Alert: Unassigned Cases" /></td>
    <td><input class="inputButton_md_Action2" type="button" onclick = "this.disabled=true;document.location ='tNIM3_CaseAccount_Encounter_PayerID_query.jsp?AssignedToID=-1&FilterBill=6'" value="Alert: Escalated Cases" /></td>
    <td>&nbsp;</td>
  </tr>
</table>

<table width=<%=MasterTableWidth%> border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
  <tr>
  <td width=10>&nbsp;</td>
  <td>
  <table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
    
  <%
if (isScheduler)
{
	%>
  <%
}
%>
  <tr>
  <td align="center">
    
  <form name="selfForm" id="selfForm"  method="POST" action="tNIM3_CaseAccount_Encounter_PayerID_query.jsp">
    <table border=0 cellspacing=2 width='100%' cellpadding=5>
      <%
  if (imyPayerID.intValue()>0)
  {
	  %>
      <tr ><td colspan=8 class="borderHighlightGreen">Note: You are only viewing cases from: <strong><%=new bltNIM3_PayerMaster(imyPayerID).getPayerName()%></strong></td></tr>
      <%
  }
  %>
      <tr > 
        <td colspan=3 nowrap="nowrap" class=tdHeader>
          Searching  Assigned To:
          <select name="AssignedToID"/>
          <option <%if (myAssignedToID.equalsIgnoreCase("-1")){ out.print ("selected");}%> value="-1">All Cases</option>
          <option <%if (myAssignedToID.equalsIgnoreCase("0")){ out.print ("selected");}%> value="0">Unassigned</option>
          <%
{
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("select Userid,LogonUserName, contactfirstname, contactlastname from tUserAccount where accounttype ='SchedulerID' or accounttype='SchedulerID2' or accounttype='SchedulerID3' order by LogonUserName");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
				if (myRS.getString("UserID").equalsIgnoreCase(myAssignedToID))
				{
					%>
					<option value="<%=myRS.getString("UserID")%>" selected="selected"><%=myRS.getString("LogonUserName")%> [<%=myRS.getString("contactfirstname")%>, <%=myRS.getString("contactlastname")%>]</option>
					<%
				}
				else
				{
					%>
					<option  value="<%=myRS.getString("UserID")%>"><%=myRS.getString("LogonUserName")%> [<%=myRS.getString("contactfirstname")%>, <%=myRS.getString("contactlastname")%>]</option>
					<%
				}
			}
			mySS.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
%>
          </select></td>
        <td colspan=1 align=right nowrap="nowrap" class=tdBaseAlt3>
  <div id="dPrev"></div>
  </td>
        <td colspan=1 nowrap="nowrap" class=tdBaseAlt3> 
  <div id="dNext"></div>
          
          </td>
        <td colspan=2 class=tdHeader nowrap> 
  <input type=hidden name=maxResults value=<%=maxResults%> >
  <input type=hidden name=startID value=0 ><p>Sort:
  <select name=orderBy>
    <option value=ReceiveDate <%if (orderBy.equalsIgnoreCase("ReceiveDate")){out.println(" selected");}%> >Receive Date</option>
    <option value="ReceiveDate Desc" <%if (orderBy.equalsIgnoreCase("ReceiveDate Desc")){out.println(" selected");}%> >Receive Date Desc</option>
    <option value=AppointmentTime <%if (orderBy.equalsIgnoreCase("AppointmentTime")){out.println(" selected");}%> >Appointment</option>
    <option value="AppointmentTime Desc" <%if (orderBy.equalsIgnoreCase("AppointmentTime Desc")){out.println(" selected");}%> >Appointment Desc</option
      ><option value=CaseClaimNumber <%if (orderBy.equalsIgnoreCase("CaseClaimNumber")){out.println(" selected");}%> >Claim Number</option>
    <option value=PatientFirstName <%if (orderBy.equalsIgnoreCase("PatientFirstName")){out.println(" selected");}%> >First Name</option>
    <option value=PatientLastName <%if (orderBy.equalsIgnoreCase("PatientLastName")){out.println(" selected");}%> >Last Name</option>
    <option value=PatientSSN <%if (orderBy.equalsIgnoreCase("PatientSSN")){out.println(" selected");}%> >SSN</option>
    <option value=EmployerName <%if (orderBy.equalsIgnoreCase("EmployerName")){out.println(" selected");}%> >Employer</option>
  </select><br />
Filter:          <select name="FilterBill">
            <option value="0">Show All</option>
            <option value="1" <% if (myFilterBill.equalsIgnoreCase("1")) {out.print(" selected ");}%>>Ready to Bill</option>
            <option value="2" <% if (myFilterBill.equalsIgnoreCase("2")) {out.print(" selected ");}%>>Payment Due</option>
            <option value="3" <% if (myFilterBill.equalsIgnoreCase("3")) {out.print(" selected ");}%>>Alert: Report Due</option>
            <option value="4" <% if (myFilterBill.equalsIgnoreCase("4")) {out.print(" selected ");}%>>Alert: Not Scheduled</option>
            <option value="5" <% if (myFilterBill.equalsIgnoreCase("5")) {out.print(" selected ");}%>>Alert: Needs Rx Review</option>
            <option value="6" <% if (myFilterBill.equalsIgnoreCase("6")) {out.print(" selected ");}%>>Alert: Escalated Cases</option>
            </select>

          </td> 
        <td colspan=1 align="right" class=tdHeader> 
            <input class="inputButton_lg_Create" type="Submit" name="Submit" value="Search" align="middle">
          </td>
        </tr>
      <tr class=tdHeader> 
  <%
if (isScheduler)
{
%>
  <td colspan="2">&nbsp;</td>
        <td colspan=1 nowrap="nowrap"> 
          Status: 
          <select name="CaseStatusID" id="CaseStatusID">
            <option value=-2 <%if (myEncounterStatusID.equalsIgnoreCase("-2") ) {out.println("selected");}%>>-All</option>
            <option value=-1 <%if (myEncounterStatusID.equalsIgnoreCase("-1") ) {out.println("selected");}%>>-All Active</option>
            <option <%if (myEncounterStatusID.equalsIgnoreCase("0") ) {out.println("selected");}%> value=0>NA</option>
<option <%if (myEncounterStatusID.equalsIgnoreCase("1") ) {out.println("selected");}%> value=1>Active</option>
<option <%if (myEncounterStatusID.equalsIgnoreCase("4") ) {out.println("selected");}%> value=4>Active: Flag</option>
<option <%if (myEncounterStatusID.equalsIgnoreCase("7") ) {out.println("selected");}%> value=7>Active: Flag: Hold-Bill</option>
<option <%if (myEncounterStatusID.equalsIgnoreCase("12") ) {out.println("selected");}%> value=12>Active: Flag: Mgr Escalate</option>
<option <%if (myEncounterStatusID.equalsIgnoreCase("11") ) {out.println("selected");}%> value=11>Active: Flag: Mgr Rp Review</option>
<option <%if (myEncounterStatusID.equalsIgnoreCase("8") ) {out.println("selected");}%> value=8>Active: Flag: Mgr Rx Review</option>
<option <%if (myEncounterStatusID.equalsIgnoreCase("9") ) {out.println("selected");}%> value=9>Billing: In Progress</option>
<option <%if (myEncounterStatusID.equalsIgnoreCase("6") ) {out.println("selected");}%> value=6>Closed</option>
<option <%if (myEncounterStatusID.equalsIgnoreCase("5") ) {out.println("selected");}%> value=5>Void</option>
<option <%if (myEncounterStatusID.equalsIgnoreCase("10") ) {out.println("selected");}%> value=10>Void: Leak</option>
<option <%if (myEncounterStatusID.equalsIgnoreCase("3") ) {out.println("selected");}%> value=3>Void: No-Show</option>
<option <%if (myEncounterStatusID.equalsIgnoreCase("2") ) {out.println("selected");}%> value=2>Void: Rescan</option>
            </select>






          <input name="open" type="submit" class="inputButton_sm_Default" id="open" onClick="document.getElementById('CaseStatusID').value='-1';document.getElementById('selfForm').submit();" value="-All Act" />
          <input name="open" type="submit" class="inputButton_sm_Default" id="open" onClick="document.getElementById('CaseStatusID').value='6';document.getElementById('selfForm').submit();" value="closed" />
  <input name="open" type="submit" class="inputButton_sm_Action1" id="open" onClick="document.getElementById('CaseStatusID').value='4';document.getElementById('selfForm').submit();" value="flag" /><br />
          
          Payer: 
          <select name="PayerID" id="PayerID" >
            </p>
            <option value="-1">--Show All</option>
            <%
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("select PayerID, PayerName from tNIM3_PayerMaster where PayerTypeID = 2 order by payername");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
				String temppid = myRS.getString("PayerID");
				%>
            <option <% if (myPayerID.equalsIgnoreCase(temppid)){out.print("  selected=\"selected\" ");}%> value="<%=temppid%>"><%=myRS.getString("PayerName")%></option>
            <%
			}
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
		mySS.closeAll();
	
	%>
            </select>
          <br />
Type:
<select name="EncounterTypeID">
  <option <%if (myEncounterTypeID.equalsIgnoreCase("0") ) {out.println("selected");}%> value="0">Show All</option>
  <option <%if (myEncounterTypeID.equalsIgnoreCase("1") ) {out.println("selected");}%> value="1">Other: Single-Visit</option>
  <option <%if (myEncounterTypeID.equalsIgnoreCase("2") ) {out.println("selected");}%> value="2">Other: Multiple-Visit</option>
  <option <%if (myEncounterTypeID.equalsIgnoreCase("3") ) {out.println("selected");}%> value="3">Other</option>
  <option <%if (myEncounterTypeID.equalsIgnoreCase("4") ) {out.println("selected");}%> value="4">Diag: MRI</option>
  <option <%if (myEncounterTypeID.equalsIgnoreCase("5") ) {out.println("selected");}%> value="5">Diag: CT</option>
  <option <%if (myEncounterTypeID.equalsIgnoreCase("6") ) {out.println("selected");}%> value="6">Diag: EMG</option>
  <option <%if (myEncounterTypeID.equalsIgnoreCase("7") ) {out.println("selected");}%> value="7">Diag: NCV</option>
  <option <%if (myEncounterTypeID.equalsIgnoreCase("8") ) {out.println("selected");}%> value="8">Diag: PET</option>
  <option <%if (myEncounterTypeID.equalsIgnoreCase("9") ) {out.println("selected");}%> value="9">Diag: Other</option>
  <option <%if (myEncounterTypeID.equalsIgnoreCase("10") ) {out.println("selected");}%> value="10">Lab</option>
</select></td>
  <%
}
%>
        <td colspan=1 nowrap="nowrap"> 
          Claim#:<input type="text" name="CaseClaimNumber" value="<%=myCaseClaimNumber%>" size="10"><br />
          SP: <input type="text" name="ScanPass" value="<%=myScanPass%>" size="10"></td>
        <td colspan=2 nowrap="nowrap"> 
          Patient Name:<br />
          First:
  <input type="text" name="PatientFirstName" value="<%=myPatientFirstName%>" size="10">
          <br />
          Last: 
          <input name="PatientLastName" id="PatientLastName" type="text" value="<%=myPatientLastName%>" size="10" /></td>
        <td colspan=1 nowrap="nowrap"> 
          Employer: 
          <input name="EmployerName" type="text" value="<%=myEmployerName%>" size="10" />
          <br />
          <br /></td>
        <td colspan=1>&nbsp;</td>
        </tr>
      <tr class=tdHeaderAlt> 
        <td colspan=2>Action</td>
        <td colspan=1> 
          Status
          </td>
        <td colspan=1 nowrap="nowrap"> 
          Claim Number
          </td>
        <td colspan=2 nowrap="nowrap"> 
          Patient/Employer</td>
        <td colspan=1 nowrap="nowrap"> 
          SSN
          /DOB</td>
        <td colspan=1 nowrap="nowrap"> 
          Appointment
          </td>
        </tr>
      
      
      
      
      
      
      
      
      
  <%



db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(null);
String myWhere = "where (tNIM3_CaseAccount.PayerID >0 ";
boolean theFirst = false;

try
{
	if (!myAssignedToID.equalsIgnoreCase("-1")&&myAssignedToID!=null)
	{
			if (!theFirst) { myWhere+=" and ";}
			myWhere += " ( AssignedToID =  " + myAssignedToID + ")";
			theFirst = false;
	}
	
	if (myFilterBill.equalsIgnoreCase("1"))
	{
			if (!theFirst) { myWhere+=" and ";}
			myWhere += " ( reportfileid>0 AND EncounterStatusID in (9) and SentTo_Bill_Pay<'2008-01-01')";
			theFirst = false;
	}
	else if (myFilterBill.equalsIgnoreCase("2"))
	{
			if (!theFirst) { myWhere+=" and ";}
			myWhere += " ( rec_bill_pro + interval '50 days' < NOW() AND SentTo_Bill_Pro<'2008-01-01' )";
			theFirst = false;
	}
	else if (myFilterBill.equalsIgnoreCase("3"))
	{
			if (!theFirst) { myWhere+=" and ";}
			myWhere += " ( tNIM3_Encounter.appointmentid>0 AND appointmenttime + interval '8 hours' < NOW() AND reportfileid=0)";
			theFirst = false;
	}
	else if (myFilterBill.equalsIgnoreCase("4"))
	{
			if (!theFirst) { myWhere+=" and ";}
			myWhere += " ( tNIM3_Encounter.appointmentid=0 AND timetrack_reqcreated + interval '1 hours' < NOW() )";
			theFirst = false;
	}
	else if (myFilterBill.equalsIgnoreCase("5"))
	{
			if (!theFirst) { myWhere+=" and ";}
			myWhere += " ( timetrack_reqrxreview < '1900-01-01' )";
			theFirst = false;
	}
	else if (myFilterBill.equalsIgnoreCase("6"))
	{
			if (!theFirst) { myWhere+=" and ";}
			myWhere += " (  EncounterStatusID in (8,11,12) )";
			theFirst = false;
	}
	if (myPayerID.equalsIgnoreCase("-1"))
	{
//		if (!theFirst) { myWhere+=" and ";}
//		myWhere += "PayerID > 0";
//		theFirst = false;
	}
	else
	{
		if (!myPayerID.equalsIgnoreCase(""))
		{
			if (!theFirst) { myWhere+=" and ";}
			myWhere += "PayerID = " + myPayerID +"";
			theFirst = false;
		}
	}

	if (myEncounterStatusID.equalsIgnoreCase("-1"))
	{
		if (!myEncounterStatusID.equalsIgnoreCase(""))
		{
			if (!theFirst) { myWhere+=" and ";}
			myWhere += "EncounterStatusID in (0,1,4,7,8,9,11,12)";
			theFirst = false;
		}
	}
	else if (myEncounterStatusID.equalsIgnoreCase("-2"))
	{
		if (!myEncounterStatusID.equalsIgnoreCase(""))
		{
			if (!theFirst) { myWhere+=" and ";}
			myWhere += "EncounterStatusID >=0";
			theFirst = false;
		}
	}
	else
	{
		{
			if (!theFirst) { myWhere+=" and ";}
			myWhere += "EncounterStatusID  = " + myEncounterStatusID;
			theFirst = false;
		}
	}

	if (myEncounterTypeID.equalsIgnoreCase("0"))
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "EncounterTypeID >=0";
		theFirst = false;
	}
	else
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "EncounterTypeID  = " + myEncounterTypeID;
		theFirst = false;
	}



	if (!myCaseClaimNumber.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myCaseClaimNumber.indexOf("%")>=0)
		{
			myWhere += "lower(CaseClaimNumber) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myCaseClaimNumber));
		}
		else if (myCaseClaimNumber.indexOf("=")==0)
		{
			myWhere += "lower(CaseClaimNumber) = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myCaseClaimNumber.substring(1, myCaseClaimNumber.length())));
		}
		else if (myCaseClaimNumber.length()<=3)
		{
			myWhere += "lower(CaseClaimNumber) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myCaseClaimNumber + "%"));
		}
		else 
		{
			myWhere += "lower(CaseClaimNumber) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myCaseClaimNumber + "%"));
		}
		theFirst = false;
	}
	if (!myScanPass.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myScanPass.indexOf("%")>=0)
		{
			myWhere += "lower(ScanPass) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myScanPass));
		}
		else if (myScanPass.indexOf("=")==0)
		{
			myWhere += "lower(ScanPass) = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myScanPass.substring(1, myScanPass.length())));
		}
		else if (myScanPass.length()<=3)
		{
			myWhere += "lower(ScanPass) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myScanPass + "%"));
		}
		else 
		{
			myWhere += "lower(ScanPass) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myScanPass + "%"));
		}
		theFirst = false;
	}
	if (!myPatientFirstName.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myPatientFirstName.indexOf("%")>=0)
		{
			myWhere += "lower(PatientFirstName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPatientFirstName));
		}
		else if (myPatientFirstName.indexOf("=")==0)
		{
			myWhere += "lower(PatientFirstName) = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPatientFirstName.substring(1, myPatientFirstName.length())));
		}
		else if (myPatientFirstName.length()<=3)
		{
			myWhere += "lower(PatientFirstName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPatientFirstName + "%"));
		}
		else 
		{
			myWhere += "lower(PatientFirstName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myPatientFirstName + "%"));
		}
		theFirst = false;
	}
	if (!myPatientLastName.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myPatientLastName.indexOf("%")>=0)
		{
			myWhere += "lower(PatientLastName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPatientLastName));
		}
		else if (myPatientLastName.indexOf("=")==0)
		{
			myWhere += "lower(PatientLastName) = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPatientLastName.substring(1, myPatientLastName.length())));
		}
		else if (myPatientLastName.length()<=3)
		{
			myWhere += "lower(PatientLastName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPatientLastName + "%"));
		}
		else 
		{
			myWhere += "lower(PatientLastName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myPatientLastName + "%"));
		}
		theFirst = false;
	}
	if (!myEmployerName.equalsIgnoreCase(""))
	{
		if (true||!theFirst) { myWhere+=" and ";}
		if (myEmployerName.indexOf("%")>=0)
		{
			myWhere += "lower(EmployerName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myEmployerName));
		}
		else if (myEmployerName.indexOf("=")==0)
		{
			myWhere += "lower(EmployerName) = ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myEmployerName.substring(1, myEmployerName.length())));
		}
		else if (myEmployerName.length()<=3)
		{
			myWhere += "lower(EmployerName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myEmployerName + "%"));
		}
		else 
		{
			myWhere += "lower(EmployerName) LIKE ?";
			myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myEmployerName + "%"));
		}
		theFirst = false;
	}
	myWhere += ")";
	
	theFirst=false;
	//System.out.println(myWhere);
	if (theFirst||myWhere.equalsIgnoreCase(")"))
	{
	myWhere = "";
	}
	

}
catch(Exception e)
{
out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

String orderByString = orderBy;
//if (orderBy.equalsIgnoreCase("ReferralDate"))
{
//	orderByString = "ReferralDate Desc";
}

try
{
	String mySQL = "null";
	String mySQL2 = "null";
	if (isScheduler)
	{	
		mySQL = "select tNIM3_CaseAccount.*, tNIM3_Appointment.appointmenttime,tNIM3_Referral.ReceiveDate, tNIM3_Encounter.encounterid, tNIM3_Encounter.encounterstatusid, tNIM3_Encounter.ScanPass from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid=tNIM3_CaseAccount.caseID LEFT JOIN tNIM3_Encounter on tNIM3_Encounter.referralid=tNIM3_Referral.referralid LEFT JOIN tNIM3_Appointment on tNIM3_Appointment.appointmentid = tNIM3_Encounter.appointmentid   " + myWhere + " order by case when tNIM3_CaseAccount.PayerID=50 then 1 when (tNIM3_CaseAccount.CaseClaimNumber='') then 2 when (tNIM3_Encounter.encounterstatusid=4) then 3 when (true) then 4 END, " + orderByString + " LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
	}


myDPSO.setSQL(mySQL);
myRS = mySS.executePreparedStatement(myDPSO);


}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
if (firstDisplay)
{
	%>
    <tr><td colspan=6><span class="borderHighlightBlackDottedNoFont titleSub1">Please enter search criteria and click Submit above to see results.</span></td></tr>
    <%
}
else
{
   while (myRS!=null&&myRS.next())
   {
	   Integer tempEncounterID = new Integer(myRS.getString("EncounterID"));
	   Integer tempEncounterStatusID = new Integer(myRS.getString("EncounterStatusID"));
	   Integer tempATUserID = new Integer(myRS.getString("AssignedToID"));
	   NIM3_EncounterObject myEO = new NIM3_EncounterObject(tempEncounterID);
	cnt++;
	//if (cnt>=startID&&cnt<=startID+maxResults)
if (cnt<=maxResults)
{
	cnt2++;
	
	String myClass = "tdBase";
	if (myEO.NIM3_CaseAccount.getCaseClaimNumber().equalsIgnoreCase(""))
	{
		myClass = "requiredFieldMain";
	}
	else if (cnt2%2!=0)
	{
		myClass = "tdBaseAlt";
	}
	
	out.print("<tr class="+myClass+">");

/*	else if (false)
	{
		out.print("<td><input class=\"inputButton_md_Default\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=edit&EDITID=" + myEO.NIM3_CaseAccount.getCaseID() + "&KM=p'\" value=\"Edit\">");
//	out.print("&nbsp;<input class=\"inputButton_md_Default\" value=\"Msg\"  type=button onClick = \"this.disabled=false;modalPost('PatientAccountID', modalWin('tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=viewcomm&EDITID=" + myEO.NIM3_CaseAccount.getCaseID() + "&KM=p','MsgWindow','dialogWidth:900px;dialogHeight:400px','status=yes,scrollbars=yes,resizable=yes,width=800,height=400'))\">");
		out.print("&nbsp;<input class=\"inputButton_md_Default\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=viewreports&EDITID=" + myEO.NIM3_CaseAccount.getCaseID() + "&KM=p'\" value=\"Reports\"></td>");
	}
*/

	if (myEO.NIM3_CaseAccount.getPayerID().intValue()==50&&isScheduler)
	{
		out.print("<td align=center colspan=2 rowspan=2><input class=\"inputButton_md_Action1\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=" + myEO.NIM3_CaseAccount.getCaseID() + "&KM=p'\" value=\"[NEW]Case\"></td>");
	}
	else if (!myEO.NIM3_CaseAccount.isComplete()&&isScheduler)
	{
		out.print("<td align=center  colspan=2 rowspan=2><input class=\"inputButton_md_Action1\"  type=button onClick = \"this.disabled=true;alert('This case is missing critical information\\n\\nClick OK to be taken to the Case Edit screen in order to complete this data.  The Referral Dashboard for this case may not be available until all of this information is complete.');document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=" + myEO.NIM3_CaseAccount.getCaseID() + "&KM=p'\" value=\"Open\">");
	}
	else if (isScheduler)
	{
	out.print("<td align=center  colspan=2 rowspan=2><input class=\"inputButton_md_Default\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=" + myEO.NIM3_CaseAccount.getCaseID() + "&KM=p'\" value=\"Open\"></td>");
	}
	else 
	{
	out.print("<td align=center  colspan=2><input class=\"inputButton_md_Default\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=viewpayer&EDITID=" + myEO.NIM3_CaseAccount.getCaseID() + "&KM=p'\" value=\"View Details\"></td>");
	}

	out.print("<td>");
	if (tempEncounterStatusID.intValue()==4||tempEncounterStatusID.intValue()==7)
	{
		out.print("Encounter Status: <span style=\"color:#FFFF99;background-color:#000;\"><strong>"+ (new bltEncounterStatusLI (tempEncounterStatusID).getDescriptionLong()+"</strong></span>"));
	}
	else
	{
		out.print("Encounter Status: <strong>"+ (new bltEncounterStatusLI (tempEncounterStatusID).getDescriptionLong()+"</strong>"));
	}
	out.print("<br>Assigned To: <strong>"+ (new bltUserAccount (tempATUserID).getLogonUserName()+"</strong>"));
	out.print("<br>Receive Date: <strong>" + PLCUtils.getDisplayDateWithTime(PLCUtils.getParseDBDFT(myRS.getString("ReceiveDate"))) + "</strong><br><span class=\"borderHighlightBlackDottedNoFont\">Carrier: <strong>" + myEO.NIM3_PayerMaster.getPayerName() + "</strong></span>");
	out.print("</td>");
	out.print("<td>");
	if (myEO.NIM3_CaseAccount.getCaseClaimNumber().equalsIgnoreCase(""))
	{
		out.print("<img src=\"images/blink_missing.gif\"onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=edit&EDITID=" + myEO.NIM3_CaseAccount.getCaseID() + "&KM=p'\">");
	}
	else 
	{
		out.print("Type: <strong>" + myEO.getEncounterType_Display()+"</strong>" );
		out.print("<br>Claim#: <strong>" + myEO.NIM3_CaseAccount.getCaseClaimNumber() +"</strong>" );
		out.print("<br>SP: <strong>" + myRS.getString("ScanPass")+"</strong>" );
	}
	out.print("</td>");
	out.print("<td nowrap colspan=2>");
	out.print("<strong>" + myEO.NIM3_CaseAccount.getPatientFirstName() + "&nbsp;&nbsp;" + myEO.NIM3_CaseAccount.getPatientLastName() + "</strong>");
	out.print("<br>" + myEO.NIM3_CaseAccount.getEmployerName());
	out.print("</td>");
	out.print("<td>");
	int mySSN_Length = myEO.NIM3_CaseAccount.getPatientSSN().length();
	String dispSSN = "[no-data]";
	if (mySSN_Length>5)
	{
	 	dispSSN = "xxx-xx-" + myEO.NIM3_CaseAccount.getPatientSSN().substring(mySSN_Length-4,mySSN_Length);
	}
	out.print(dispSSN+"");
	out.print("<br>" + PLCUtils.getDisplayDate(myEO.NIM3_CaseAccount.getPatientDOB(),false) );
	out.print("</td>");
	out.print("<td>");
	out.print("" + PLCUtils.getDisplayDateWithTime(PLCUtils.getParseDBDFT(myRS.getString("AppointmentTime"))) );
//	out.print("<br>" + myEO.NIM3_CaseAccount.getEmployerName());
	out.print("</td>");
	out.print("</tr>");
	if (tempEncounterID!=null)
	{
		out.print("</tr><tr class="+myClass+"><td colspan=6>" + NIMUtils.aEncounterStatus_HTML(tempEncounterID) + "</td></tr>");	
	}
	else
	{
		out.print("</tr><tr class="+myClass+"><td colspan=6>Encounter not Active</td></tr>");	
	}
}
   }
mySS.closeAll();
endCount = cnt;


if (startID>=endCount)
{
//startID = endCount-maxResults;

}

if (maxResults<=0)
{
maxResults=5;
}


if (startID<=0)
{
startID=0;
}


if (startID>0)
{
	%>
      <script language="javascript">
        document.getElementById('dPrev').innerHTML='<a href="tNIM3_CaseAccount_Encounter_PayerID_query.jsp?startID=<%=(startID-maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&PayerID=<%=myPayerID%>&CaseClaimNumber=<%=myCaseClaimNumber%>&ScanPass=<%=myScanPass%>&PatientFirstName=<%=myPatientFirstName%>&PatientLastName=<%=myPatientLastName%>&CaseStatusID=<%=myEncounterStatusID%>&PatientSSN=<%=myPatientSSN%>&EmployerName=<%=myEmployerName%>&FilterBill=<%=myFilterBill%>&AssignedToID=<%=myAssignedToID%>&EncounterTypeID=<%=myEncounterTypeID%>&EmployerName=<%=myEmployerName%>"><< previous <%=maxResults%></a> ';
        </script>
      <%
}
//if ((startID+maxResults)<endCount)
if (endCount>=maxResults)
{
//out.println("[" + startID + "]");
%>
      <script language="javascript">
        document.getElementById('dNext').innerHTML='<a href="tNIM3_CaseAccount_Encounter_PayerID_query.jsp?startID=<%=(startID+maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&PayerID=<%=myPayerID%>&CaseClaimNumber=<%=myCaseClaimNumber%>&ScanPass=<%=myScanPass%>&PatientFirstName=<%=myPatientFirstName%>&PatientLastName=<%=myPatientLastName%>&CaseStatusID=<%=myEncounterStatusID%>&PatientSSN=<%=myPatientSSN%>&EmployerName=<%=myEmployerName%>&FilterBill=<%=myFilterBill%>&AssignedToID=<%=myAssignedToID%>&EncounterTypeID=<%=myEncounterTypeID%>&EmployerName=<%=myEmployerName%>"> next >> <%=maxResults%></a>         ';
        </script> 
      <%
}


}
//end if first

%>
      
  </table> 
    
    
  <%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>
    
    
    
    </table>
  </form><p><input class="inputButton_md_Default" type=button onClick = "this.disabled=true;document.location ='tUserAccount_form_settings.jsp'" value="User Settings"></p><br />
  <br />
  <%
if (false&&CurrentUserAccount.getAccountType().equalsIgnoreCase("PhysicianID"))
{
%>
    
    To create a new case record, please select a Carrier: <select id="PayerIDSelect" name="PayerID" onChange="this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_create.jsp?CREATEID=' + document.getElementById('PayerIDSelect').value + '&EDIT=new&KM=p&INTNext=yes'"/>
  <option value="0">Please Select...</option>
    <%
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("select PayerID, PayerName from tNIM3_PayerMaster where PayerTypeID = 2 and PayerID = 41 order by payername");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
				%>
    <option value="<%=myRS.getString("PayerID")%>"><%=myRS.getString("PayerName")%></option>
    <%
			}
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
		mySS.closeAll();
	
	%>
    </select>
  <%
}
else if (false&&isScheduler)
{
%>
    
    To create a new case record, please select a Carrier: <select id="PayerIDSelect" name="PayerID" onChange="this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_create.jsp?CREATEID=' + document.getElementById('PayerIDSelect').value + '&EDIT=new&KM=p&INTNext=yes'"/>
  <option value="0">Please Select...</option>
    <%
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("select PayerID, PayerName from tNIM3_PayerMaster where PayerTypeID = 2 order by payername");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
				%>
    <option value="<%=myRS.getString("PayerID")%>"><%=myRS.getString("PayerName")%></option>
    <%
			}
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
		mySS.closeAll();
	
	%>
    </select>
  <%
}
else if (false)
{
%>
  <input name="CreateCA" type="button" id="CreateCA" value="Create Case..." onClick="this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_create.jsp?CREATEID=<%=CurrentUserAccount.getPayerID()%>&EDIT=new&KM=p&INTNext=yes'" />
  <%
}
%>
    
  <%
if (isScheduler&&false)
{
%>
    
    <p><img src="express/expressButton1.gif" width="150" height="36" /><br />To create a new case record, please select a Carrier: 
      <select name="PayerID" class="inputButton_md_Create" id="PayerIDSelectExpress" onChange="this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_create.jsp?CREATEID=' + document.getElementById('PayerIDSelectExpress').value + '&EDIT=newexpress&KM=p&INTNext=yes'"/>
      </p>
    <option value="0">Please Select...</option>
    <%
		searchDB2 mySS = new searchDB2();
		java.sql.ResultSet myRS = null;;
		try
		{
			String mysql=("select PayerID, PayerName from tNIM3_PayerMaster where PayerTypeID = 2 order by payername");  //matches iOpen Type = Payer Type
			myRS = mySS.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS!=null&&myRS.next())
			{
				%>
    <option value="<%=myRS.getString("PayerID")%>"><%=myRS.getString("PayerName")%></option>
    <%
			}
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
		mySS.closeAll();
	
	%>
    </select>
    
    
    
  <%	
}
else if (isScheduler)
{
	%>
    <hr />
    <form id="form1" name="form1" method="post" action="tNIM3_CaseAccount_Create1.jsp">
      <p>
        To create a new case, please enter the Claim Number: 
        <input name="CN" type="text" class="inputButton_md_Create" id="CN" size="20" maxlength="20" />
        <input name="button" type="submit" class="inputButton_md_Create" id="button" onClick="MM_validateForm('CN','','R');return document.MM_returnValue" value="Create" />
        </p>
  </form>
    <%
}

%>
    
    
    
  </td>
  </tr>
</table>
</td>
</tr>
</table>      
    </td>
  </tr>
</table>

    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>