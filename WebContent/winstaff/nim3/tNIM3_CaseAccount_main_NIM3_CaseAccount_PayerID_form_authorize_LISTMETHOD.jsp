<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*    " %>
<%/*
    filename: out\jsp\tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPayerID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }
    bltNIM3_CaseAccount_List        bltNIM3_CaseAccount_List        =    new    bltNIM3_CaseAccount_List(iPayerID,"CaseID="+requestID,"");

//declaration of Enumeration
    bltNIM3_CaseAccount        working_bltNIM3_CaseAccount;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_CaseAccount_List.elements();
    %>
    <%
    if (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_CaseAccount  = (bltNIM3_CaseAccount) leCurrentElement.getObject();
        pageControllerHash.put("iCaseID",working_bltNIM3_CaseAccount.getCaseID());
        pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tNIM3_CaseAccount_form.jsp?nullParam=null"+parameterPassString    ;
        if (request.getParameter("EDIT").equalsIgnoreCase("del"))
        {
            targetRedirect = "tNIM3_CaseAccount_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("openflow"))
        {
            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize")   ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("viewcomm"))
        {
            targetRedirect = "tNIM3_CommTrack_frame.jsp"    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("openflowexpress"))
        {
            targetRedirect = ("tNIM3_CaseAccount_Express.jsp?EDITID=" + working_bltNIM3_CaseAccount.getCaseID())   ;
        }
        response.sendRedirect(targetRedirect);
    }
    else
    {
   out.println("invalid where query");
    }

  }
  else
  {
   out.println("illegal");
  }
}
else
{
out.println("Your Security Level does not permit you to View this.");
}
%>




