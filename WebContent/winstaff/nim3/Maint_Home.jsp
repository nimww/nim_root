<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
</head>
<SCRIPT language="JavaScript1.2">
function openStatus()
{
	testwindow= window.open ("Home_Status.jsp", "HomeStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(0,0);
}

function openAuto(myAuto)
{
	testwindow= window.open ("report_auto2.jsp?rid=" + myAuto + "&MREF=300", "AutoStatus","location=1,status=1,scrollbars=1,resizable=1,width=1000,height=700");
	testwindow.moveTo(100,100);
}

</SCRIPT>

<body>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler) 
    {
        accessValid = true;
    }
    else if (pageControllerHash.containsKey("iAdjusterID")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID        =    (Integer)pageControllerHash.get("iAdjusterID");
		isAdjuster = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID2")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster2 = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID3")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster3 = true;
        accessValid = true;
	}
	
	
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	
		if (isScheduler)
	  {
		  String theScheduler = CurrentUserAccount.getUserID().toString();
		  String myAT_Where = "";
		  if ( request.getParameter("theScheduler")!=null&&!request.getParameter("theScheduler").equalsIgnoreCase("") )
		  {
			  theScheduler = request.getParameter("theScheduler");
		  }
		  if (!theScheduler.equalsIgnoreCase("0"))
		  {
			  myAT_Where =  " AND tNIM3_CaseAccount.assignedtoid = " + theScheduler + " ";
		  }
		  
	  %>
<table width="<%=MasterTableWidth%>" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td ><p class="title">Welcome to NextImage Grid Version <%=ConfigurationInformation.applicationVersion%><br>
          Server: <%=ConfigurationInformation.serverName%> [<%=ConfigurationInformation.applicationVersion%>] </p></td>
      </tr>
      <%
	  if (isScheduler2)
	  {
	  %>
      <tr class="tdHeaderAlt" >
        <td ><span class="tdHeader"><hr />
          Maintenance Options:</span></td>
      </tr>
      <tr>
        <td bgcolor="#DAE0E4"><p class="tdHeader">
          <input name="button3" type="submit" class="inputButton_md_Create" id="button3" onClick="this.disabled = true; document.location='tNIM3_PayerMaster_query.jsp';" value="Payer Maintenance" />
          </p></td>
      </tr>
      <tr  bgcolor="#DAE0E4">
        <td ><input name="button5" type="submit" class="inputButton_md_Create" id="button5" onClick="this.disabled = true; document.location='gsn_nim3b/tUserAccount_query.jsp';" value="User Account Maintenance V2" /> <input name="button" type="submit" class="inputButton_sm_Create" id="button" onClick="this.disabled = true; document.location='gsn_nim3/tUserAccount_query.jsp';" value="User Account Maintenance [OLD]" /></td>
      </tr>
<% 
if (isScheduler4)
{
%>
	<tr  bgcolor="#DAE0E4">
        <td ><hr />	<input name="Launch Revision Tracker" type="button" id="CreateCA" value="Launch Revision Tracker..." onClick="this.disabled=false;modalPost('RevisionTRackerWindow', modalWin('LI_RevisionTracker.jsp','Search Revision...','dialogWidth:1050px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));" />
&nbsp;</td>
      </tr>
      <%
  }
if (isScheduler3)
{
%>
	<tr  bgcolor="#DAE0E4">
        <td ><hr />	<input name="Submit Support Request" type="button" class="inputButton_lg_Test" id="CreateCA" onClick="this.disabled=false;document.location='../updateNIM/tUpdateItem_request.jsp?EDIT=request';" value="Submit Support Request" />
&nbsp; <input name="CreateCA" type="button" class="inputButton_lg_Test" id="CreateCA2" onClick="this.disabled=false;document.location='../updateNIM/report.jsp?rid=0';" value="View Requests" /></td>
      </tr>
      <%
  }

	  }
	  else
	  {
		  %>
          <%
	  }
	  %>
      <tr>
        <td ><input name="button2" type="submit" class="inputButton_md_Test" id="button2" onClick="this.disabled = true; document.location='intake/form1.jsp';" value="EasyIntake (alpha)" /></td>
      </tr>
    </table></td>
  </tr>
</table>
          <%
	  }
	  %>

    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>