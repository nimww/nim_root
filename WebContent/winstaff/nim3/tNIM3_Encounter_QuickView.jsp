<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*    " %>
<%/*
    filename: tNIM3_Referral_main_NIM3_Referral_CaseID.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />


<title>NextImage - Secure</title><table cellpadding=0 cellspacing=0 border=0 width=98% >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM3_Referral")%>



<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iCaseID")&&isScheduler) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }
	String SelectedEncounterID = "";
	if (request.getParameter("SelectedEncounterID")!=null&&!request.getParameter("SelectedEncounterID").equalsIgnoreCase(""))
	{
		SelectedEncounterID = request.getParameter("SelectedEncounterID");
	}
	
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
      pageControllerHash.put("sParentReturnPage","tNIM3_Referral_main_NIM3_Referral_CaseID.jsp");
    pageControllerHash.remove("iReferralID");
    pageControllerHash.remove("iEncounterID");
    pageControllerHash.put("sINTNext","tNIM3_Referral_main_NIM3_Referral_CaseID_form_create.jsp?EDIT=new&KM=p&INTNext=yes");
    session.setAttribute("pageControllerHash",pageControllerHash);

    bltNIM3_Referral_List        bltNIM3_Referral_List        =    new    bltNIM3_Referral_List(iCaseID,"","ReferralDate");

//declaration of Enumeration
    bltNIM3_Referral        working_bltNIM3_Referral;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Referral_List.elements();
    %>
     
<h1>     Only Showing Details for Selected ScanPass</h1>
     
<table border="0" cellpadding="1" cellspacing="0">
<%
//		bltNIM3_CaseAccount myCA = new bltNIM3_CaseAccount(iCaseID);
//		bltUserAccount myAdj = new bltUserAccount(myCA.getAdjusterID());

		bltNIM3_CaseAccount NCA = new bltNIM3_CaseAccount(iCaseID);
		bltUserAccount myUAdj = new bltUserAccount(new Integer(NCA.getAdjusterID()));
//		bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(myUAdj.getPayerID());
		bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(NCA.getPayerID());
		bltNIM3_PayerMaster myPPM = new bltNIM3_PayerMaster(myPM.getParentPayerID());
//		bltUserAccount myUAdm = new bltUserAccount(new Integer(NCA.getCaseAdministratorID()));
//		bltUserAccount myUNCM = new bltUserAccount(new Integer(NCA.getNurseCaseManagerID()));
//		bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(myUA.getPayerID());

		
		int tEcnt = 0;







%>
  <tr>
    <td align="right" class="tdBaseAlt">Claim #:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getCaseClaimNumber()%></strong></td>
    <td rowspan="2" align="right"><img src="images/child_line1.gif" /></td>
    <td align="right" class="tdBaseAlt">Patient:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getPatientFirstName()%>&nbsp;<%=NCA.getPatientLastName()%></strong></td>
    <td rowspan="2"><img src="images/child_line1.gif"></td>
    <td align="right" class="tdBaseAlt">Address:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getPatientAddress1()%><strong><br /><%=NCA.getPatientAddress2()%></strong></strong></td>
    <td rowspan="2"><img src="images/child_line1.gif"></td>
    <td align="right" class="tdBaseAlt">Assigned To:</td>
    <td class="tdBaseAlt"><strong><%=new bltUserAccount(NCA.getAssignedToID()).getLogonUserName()%></strong></td>
    <td rowspan="2"><img src="images/child_line1.gif"></td>
    <td align="right" class="tdBaseAlt">&nbsp;</td>
    <td class="tdBaseAlt">&nbsp;</td>
    </tr>
  <tr>
    <td align="right" class="tdBaseAlt">Carrier:</td>
    <td class="tdBaseAlt"><strong><%=myPM.getPayerName()%></strong></td>
    <td align="right" class="tdBaseAlt">DOB:</td>
    <td class="tdBaseAlt"><strong><jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(NCA.getPatientDOB())%>" /></jsp:include></strong></td>
    <td align="right" class="tdBaseAlt">City, State:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getPatientCity()%>, <%=new bltStateLI(NCA.getPatientStateID()).getShortState()%> <%=NCA.getPatientZIP()%></strong></td>
    <td colspan="2" rowspan="3" class="tdBaseAlt"><textarea name="textarea" cols="30" rows="5" readonly="readonly" class="borderHighlightBlackDottedNoFont" style="background-color:#EEE" ><%=NCA.getComments()%></textarea></td>
    <td colspan="2" rowspan="3" class="tdBaseAlt"><%if (!myPM.getImportantNotes().equalsIgnoreCase("")||!myPPM.getImportantNotes().equalsIgnoreCase("")){%><textarea name="textarea2" cols="35" rows="5" readonly="readonly" class="borderHighlightBlackDottedNoFont" style="background-color:#FFB13E" ><%=myPM.getImportantNotes()%>
<%=myPPM.getImportantNotes()%></textarea><%}%></td>
    </tr>
  <tr>
    <td align="right" class="tdBaseAlt">Adjuster:</td>
    <td class="tdBaseAlt"><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NCA.getAdjusterID()%>" /></jsp:include></div></td>
    <td align="right" >&nbsp;</td>
    <td align="right" class="tdBaseAlt">SSN:</td>
    <td class="tdBaseAlt"><strong><%=(NCA.getPatientSSN())%>
    </strong></td>
    <td>&nbsp;</td>
    <td align="right" class="tdBaseAlt">Home Phone:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getPatientHomePhone()%></strong><%if (myPM.getPayerName().indexOf("Tyson")>=0){%><br><font color="#FF0000">Note: This is a Tyson case. <br> <strong>Please do <u>not</u> call/email the Patient directly for any reason.</strong><br>If you have any questions, please see Andy Lock</font><%}%></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td align="right" class="tdBaseAlt">Adj. Phone:</td>
    <td class="tdBaseAlt"><strong><%=myUAdj.getContactPhone()%></strong></td>
    <td align="right" >&nbsp;</td>
    <td align="right" class="tdBaseAlt">DOI:</td>
    <td class="tdBaseAlt"><strong>
      <jsp:include page="../generic/DateTypeConvert_Main.jsp" flush="true" >
        <jsp:param name="CurrentSelection" value="<%=dbdf.format(NCA.getDateOfInjury())%>" />    
        </jsp:include>
    </strong></td>
    <td>&nbsp;</td>
    <td align="right" class="tdBaseAlt">Mobile Phone:</td>
    <td class="tdBaseAlt"><strong><%=NCA.getPatientCellPhone()%></strong></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
</table>

      
     
         <table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
    <%
    int altCnt = 0;
	int Rcnt = 0;
    if (eList.hasMoreElements())
    {
     while (eList.hasMoreElements())
     {

        altCnt++;
		Rcnt++;
        String theClass = "tdBase";
        //if (altCnt%2!=0)
        {
            theClass = "tdHeaderAltDark";
        }
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_Referral  = (bltNIM3_Referral) leCurrentElement.getObject();
        working_bltNIM3_Referral.GroupSecurityInit(UserSecurityGroupID);
        if (!working_bltNIM3_Referral.isComplete())
        {
//            theClass = "incompleteItem";
        %>

        <%
        }
        else
        {
        %>
        <%
        }
        %>

        <tr class=<%=theClass%> > 


<%String theClassF = "textBase";%>


<td valign="top" nowrap="nowrap">

            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td><img src="images/marker_referral_2.gif" align="absmiddle" width="115" height="31" />  <strong><jsp:include page="../generic/tReferralTypeLI_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getReferralTypeID()%>" /></jsp:include></strong><br />Rec'd: <span class="borderHighlightGreen"><jsp:include page="../generic/DateTypeConvertDT_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Referral.getReceiveDate())%>" /></jsp:include></span><br /><%theClassF = "textBase";%>
  <%if ((working_bltNIM3_Referral.isExpired("ReferralTypeID",expiredDays))&&(working_bltNIM3_Referral.isExpiredCheck("ReferralTypeID"))){theClassF = "expiredFieldMain";}%>
  <%if ( (working_bltNIM3_Referral.isRequired("ReferralTypeID"))&&(!working_bltNIM3_Referral.isComplete("ReferralTypeID")) ){theClassF = "requiredFieldMain";}%>
    
  <%theClassF = "textBase";%>
  <%if ((working_bltNIM3_Referral.isExpired("ReferralStatusID",expiredDays))&&(working_bltNIM3_Referral.isExpiredCheck("ReferralStatusID"))){theClassF = "expiredFieldMain";}%>
  <%if ( (working_bltNIM3_Referral.isRequired("ReferralStatusID"))&&(!working_bltNIM3_Referral.isComplete("ReferralStatusID")) ){theClassF = "requiredFieldMain";}%>
   Status:&nbsp;
   <strong>   <jsp:include page="../generic/tReferralStatusLI_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getReferralStatusID()%>" /></jsp:include> </strong></td>
                <td align="center" valign="middle">&nbsp;

</td>
                <td align="center" valign="middle">&nbsp;</td>
                <td align="center" valign="middle">&nbsp;</td>
                <td align="right" valign="middle">Referred To<br />
Provider:&nbsp;</td>
                <td valign="middle"> <div class=boxDr>     <jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getAttendingPhysicianID()%>" /></jsp:include></div> </td>

<td colspan="3" align="right" valign="middle">&nbsp;Referring<br />
Provider:&nbsp;</td>
                <td valign="middle"><div class="boxDr">
                  <jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" >
                    <jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getReferringPhysicianID()%>" />                
                    </jsp:include>
  </div></td>
              </tr>
            </table>
       </td>
</tr><tr>

  <td width="75%" align="left" valign="top" ><table width="100%">
<tr><td>    
        <%

			int Ecnt=0;
			
			try
			{
				searchDB2 mySS = new searchDB2();
				
				java.sql.ResultSet myRS = null;;
				String mySQL = ("select * from tNIM3_Encounter where ReferralID = " + working_bltNIM3_Referral.getReferralID()  + " and EncounterID = "+SelectedEncounterID +" order by EncounterID");
				//out.println(mySQL);
				myRS = mySS.executeStatement(mySQL);
				
				String myMainTable= " ";
				int endCount = 0;
				int cnt2=0;
				while (myRS!=null&&myRS.next())
				{
					Ecnt++;
					tEcnt++;
					NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(new Integer(myRS.getString("EncounterID")),"Load QuickView");
					bltNIM3_Document working_bltNIM3_Document  =  new bltNIM3_Document(myEO2.getNIM3_Encounter().getReportFileID());
					boolean isEActive = false;
					if (NIMUtils.getEncounterIsActive(myEO2.getNIM3_Encounter()))
					{
						isEActive=true;
					}
					if (tEcnt==1)
					{
//						out.print(myEO.getCaseDashboard_AlertCode_HTML());
					}




		String sMarkerGIF = "marker_encounter_none.gif";
		String sStatusClass = "tdBaseAltRed";
		if (myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==2||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==3||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==5||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==10)
		{
			sMarkerGIF = "marker_encounter_void.gif";
			sStatusClass = "tdBaseAltRed";
		}
		else if (myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==4||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==7||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==8||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==11||myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==12)
		{
			sMarkerGIF = "marker_encounter_flag.gif";
			sStatusClass = "tdBaseAltYellow";
		}
		else if (myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==1)
		{
			sMarkerGIF = "marker_encounter_active.gif";
			sStatusClass = "tdBaseAltGreen";
		}
		else if (myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==6)
		{
			sMarkerGIF = "marker_encounter_closed.gif";
			sStatusClass = "tdBase";
		}
		else if (myEO2.getNIM3_Encounter().getEncounterStatusID().intValue()==9)
		{
			sMarkerGIF = "marker_encounter_billing.gif";
			sStatusClass = "tdBase";
		}
		
		String sActionClass = "tdBase";

		if (myEO2.getNIM3_Encounter().getNextActionDate().before(new java.util.Date(1)))
		{
			sActionClass = "tdBase";
		}
		else if (myEO2.getNIM3_Encounter().getNextActionDate().before(PLCUtils.getYesterday()))
		{
			sActionClass = "tdBaseAlt_Action3";
		}
		else  if (myEO2.getNIM3_Encounter().getNextActionDate().before(PLCUtils.getToday()))
		{
			sActionClass = "tdBaseAlt_Action2";
		}
		else 
		{
			sActionClass = "tdBaseAlt_Action1";
		}
		
		boolean isReadyToSchedule = NIMUtils.isEncounterReadyToSchedule(myEO2,false, true)	;	
		boolean isEncounterProcessed = myEO2.isEncounterProcessed();
		boolean isReadyToScheduleRx = NIMUtils.isEncounterReadyToSchedule(myEO2, true, true)	;	
		boolean isCaseCriticalDone = NIMUtils.isCaseCreateCriticalDone(myEO2);
		
		NIM3_BillingObject myBO = myEO2.getBillingTotals();
		//Alert Checks
		boolean isReady4Alert_ReceiveConfirmation = false;
		boolean isReady4Alert_ScanPass = false;
		boolean isReady4Alert_Report = false;
		if (isCaseCriticalDone)
		{
			isReady4Alert_ReceiveConfirmation=true;
		}
		if (isCaseCriticalDone&&isReadyToScheduleRx&&myEO2.getNIM3_Encounter().getAppointmentID().intValue()>0)
		{
			isReady4Alert_ScanPass=true;
		}
		if (isCaseCriticalDone&&isReadyToScheduleRx&&myEO2.getNIM3_Encounter().getReportFileID().intValue()>0&&myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview().after(new java.util.Date(1))&&myEO2.getNIM3_Encounter().getTimeTrack_ReqRpReview_UserID().intValue()!=0)
		{
			isReady4Alert_Report=true;
		}
					
					%>
		<div class=caseBox>
        <table width="100%" border="1" cellpadding="3" cellspacing="0">
        <tr class="tdBaseAlt">
          <td nowrap><span class="tdHeader"><img src="images/<%=sMarkerGIF%>" align="absmiddle" width="114" height="31" /></span>&nbsp; </td>
        <td colspan="2" align="left" valign="top" nowrap><table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr class="<%=sStatusClass%>">
            <td width=50% nowrap><%=new bltEncounterStatusLI(myEO2.getNIM3_Encounter().getEncounterStatusID()).getDescriptionLong()%></td>
            <td width=25% nowrap align="right"><span class="tdBaseAlt_Action1" >  <%if (myEO2.getNIM3_Encounter().getSeeNetDev_Waiting().intValue()==1){out.print("<strong>|NetDev:Waiting|</strong>");}%></span>&nbsp;<span class="tdBaseAlt_Action2" >  <%if (myEO2.getNIM3_Encounter().getisCourtesy().intValue()==1){out.print("<strong>|Courtesy Schedule|</strong>");}else if (myEO2.getNIM3_Encounter().getisCourtesy().intValue()==4){out.print("<strong>|Courtesy Pending Approval|</strong>");}else if (myEO2.getNIM3_Encounter().getisCourtesy().intValue()==3){out.print("<strong>|Courtesy Declined|</strong>");}%></span>&nbsp;<span class="tdBaseAlt_Action2" > <%if (myEO2.getNIM3_Encounter().getisRetro().intValue()==1){out.print("<strong>|Retro Auth|</strong>");}%></span></td>
            <td width=25% nowrap align="right"><span class="tdBaseAlt_Action3"><%if (myEO2.getNIM3_Encounter().getisSTAT().intValue()==1){out.print("<strong>|STAT|</strong>");}%></span>&nbsp;</td>
            <td width=25% nowrap align="right"">&nbsp;</td>
            <td width=25% nowrap align="right""><%=new bltEncounterTypeLI(myEO2.getNIM3_Encounter().getEncounterTypeID()).getEncounterTypeLong()%></td>
            </tr>
          <tr class="<%=sActionClass%>">
            <td width=75% nowrap>Action: <%=PLCUtils.getDisplayDateWithTime(myEO2.getNIM3_Encounter().getNextActionDate())%></td>
            <td width=25% nowrap align="right"">&nbsp;</td>
            <td width=25% nowrap align="right"">&nbsp;</td>
            <td width=25% nowrap align="right"">&nbsp;</td>
            <td width=25% nowrap align="right"">&nbsp;</td>
            </tr>
          </table>
          
        </td>
        <td colspan="5" align="left" valign="middle" nowrap> ScanPass:&nbsp;<span class="borderHighlightGreen"><%=myEO2.getNIM3_Encounter().getScanPass()%></span>&nbsp;&nbsp;<br>
          &nbsp;DOS:&nbsp;<span class="borderHighlightGreen"><jsp:include page="../generic/DateTypeConvertDT_Main.jsp" flush="true" >
          <jsp:param name="CurrentSelection" value="<%=dbdf.format(myEO2.getNIM3_Encounter().getDateOfService())%>" />          
          </jsp:include></span></td>
        </tr>
<tr><td colspan=8><table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td>
      <%=NIMUtils.aEncounterStatus_HTML(myEO2.getNIM3_Encounter().getEncounterID())%>
</td>
  </tr>
</table>
</td></tr>				

  <tr style="display:block" id="ENC_<%=Rcnt%>_<%=Ecnt%>">

						<td width="75%" colspan="2" valign="top">
						  
						  <table width="100%" border="1" cellpadding="3" cellspacing="0">
						    <%
	

		bltNIM3_Service        working_bltNIM3_Service;
		ListElement         leCurrentElement_Service;
		java.util.Enumeration eList_Service = myEO2.getNIM3_Service_List().elements();
		int cnt55=0;
		while (eList_Service.hasMoreElements())
		{
			cnt55++;
			leCurrentElement_Service    = (ListElement) eList_Service.nextElement();
			working_bltNIM3_Service  = (bltNIM3_Service) leCurrentElement_Service.getObject();
					String serviceClass = "tdBase";
					boolean isVoid=false;
					if (working_bltNIM3_Service.getServiceStatusID().intValue()==5)
					{
						serviceClass = "tdBaseAltVoid";
						isVoid = true;
					}
					%>
						    <tr class="<%=serviceClass%>">
                            <td align="center" class="tdHeaderAlt"><%=cnt55%></td>
						      <td><span <%if (!isVoid){out.println(" class=\"borderHighlightGreen\" ");}%>  ><strong>Lookup:</strong> [<%=NIMUtils.getCPTText(working_bltNIM3_Service.getCPT())%>]</span>						        <br />        
					          <span <%if (!isVoid){out.println(" class=\"borderHighlightGreen\" ");}%>   ><strong>BP:</strong> [<%=working_bltNIM3_Service.getCPTBodyPart()%>]</span><br>
<span <%if (!isVoid){out.println(" class=\"borderHighlightGreen\" ");}%>  ><strong>RuleOut:</strong> [<%=working_bltNIM3_Service.getCPTText()%>]</span></td>
						      <td><%if (isVoid){out.println("<strike>");}%>CPT: <strong><%=working_bltNIM3_Service.getCPT()%></strong><br>
Mod: <strong><%=working_bltNIM3_Service.getCPTModifier()%></strong><br>
Qty Done: <strong><%=working_bltNIM3_Service.getCPTQty()%></strong><%if (isVoid){out.println("</strike>");}%>
                              
                              </td>
						      <td><%
if (working_bltNIM3_Service.getServiceStatusID().intValue()==1){out.println("<span class=\"borderHighlightGreen\">Bill / Pay</span><br>(Normal)");}
else if (working_bltNIM3_Service.getServiceStatusID().intValue()==2){out.println("<span class=\"borderHighlightGreen\">Don't Bill / Don't Pay</span>");}
else if (working_bltNIM3_Service.getServiceStatusID().intValue()==3){out.println("<span class=\"borderHighlightGreen\">Don't Bill / Do Pay</span>");}
else if (working_bltNIM3_Service.getServiceStatusID().intValue()==4){out.println("<span class=\"borderHighlightGreen\">Do Bill / Don't Pay</span>");}
else if (working_bltNIM3_Service.getServiceStatusID().intValue()==5){out.println("<span class=\"borderHighlightGreen\">VOID</span>");}

%>
</td>
					        </tr>
						    <%
				}
			%>
					      </table>
                          
                          

<!-- start enc detail-->

<table width="100%" border="0" cellspacing="0" cellpadding="2">
     <tr>
       <td valign="top" nowrap class="tdBaseAlt2"><strong>Patient Availability:&nbsp;&nbsp;</strong></td>
       <td width="95%" valign="top" class="tdBaseAlt2"><pre><%=myEO2.getNIM3_Encounter().getPatientAvailability()%></pre></td>
       </tr>
     <tr>
       <td width="75%" colspan="2"><%
	   
if (isScheduler &&myEO2.getNIM3_Encounter().getAppointmentID()>0)
	   {
		   %>       <table width="100%" border="0" cellpadding="3" cellspacing="0" class="borderHighlight1" >
           <tr class="tdHeader">
             <td  class="tdHeader">Appointment:&nbsp;</td>
             <td align="center"  class="borderHighlightGreen"><%
//bltICMaster myICM = new bltICMaster ();
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
            java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
            java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
            java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
            java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
            java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);
            java.text.SimpleDateFormat displayDateHourMin = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHourMin);
			java.util.Date dbAppointment = myEO2.getNIM3_Appointment().getAppointmentTime();
			String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
			String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
			String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
			String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
			String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
			String sAppointmentHour = displayDateHour.format((dbAppointment));
			String sAppointmentHourMin = displayDateHourMin.format((dbAppointment));

%><%=sAppointmentTimeDayWeek%>&nbsp;<%=sAppointmentTimeMonth%>&nbsp;<%=sAppointmentTimeDay%>&nbsp;<%=sAppointmentTimeYear%> at <%=sAppointmentHourMin%>&nbsp;</td>
             <td>
<%
if (  isEActive&&isReadyToScheduleRx&&false )
{
	%>
  <input type="button" class="inputButton_md_Stop"   onclick="this.disabled=true;modalPost('Schedule', modalWin('LI_ModalPass.jsp?ssPF=tNIM3_Encounter_main_NIM3_Encounter_ReferralID_form_authorize.jsp&EDIT=sched&EDITID=<%=myEO2.getNIM3_Encounter().getEncounterID()%>&KM=p','Schedule...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.location=document.location;" value="Re-Schedule">
  <%}
	   else if (  isEActive&&isReadyToScheduleRx )
	   {
		   %>
		<input type="button" class="inputButton_md_Stop"  onclick="alert('To reschedule this appointment, please use the Problem Wizard.');" value="Re-Schedule">                      
        <%
	   }
	   else
	   {
		   %>
		<input type="button" class="inputButton_md_Stop"  onclick="alert('Information appears to be missing that is critical to re-schedule.  Please review the above status bar or see a manager to resolve this.');" value="Re-Schedule">                      
        <%
	   }
  
  %>
  &nbsp;</td>
             </tr>
           <tr class="borderHighlightGreen">
             <td colspan="3" class="borderHighlightGreen"></td>
             </tr>
           <tr>
             <td><strong><%=myEO2.getAppointment_PracticeMaster().getPracticeName()%></strong> <br />
               [ID:&nbsp;<%=myEO2.getAppointment_PracticeMaster().getPracticeID()%>]</td>
              <td><% if (isScheduler){%><%=myEO2.getAppointment_PracticeMaster().getOfficeAddress1()%><br />
               <%=myEO2.getAppointment_PracticeMaster().getOfficeCity()%>, <%=new bltStateLI(myEO2.getAppointment_PracticeMaster().getOfficeStateID()).getShortState()%> <%=myEO2.getAppointment_PracticeMaster().getOfficeZIP()%><% }%></td>
             <td align="center" valign="middle"><% if (isScheduler){%><a href="http://maps.google.com/maps?q=<%=myEO2.getAppointment_PracticeMaster().getOfficeAddress1()%>,<%=myEO2.getAppointment_PracticeMaster().getOfficeZIP()%>" target="_blank">View Map</a>&nbsp;<% }%></td>
             </tr>
           <tr>
             <td class="tdHeader">Phone: <%=myEO2.getAppointment_PracticeMaster().getOfficePhone()%>&nbsp;</td>
             <td class="tdHeader">Fax: <%=myEO2.getAppointment_PracticeMaster().getOfficeFaxNo()%>&nbsp;</td>
             <td>&nbsp;</td>
             </tr>
           </table>
         
         
         
         
         
         
         
  <%




	}
	   %></td>
     </tr>
     <tr>
       <td colspan="2">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td><p class="tdHeader">CommTrack:<br>&nbsp;<input name="showCT" type="button" class="inputButton_sm_Default" onclick="document.getElementById('comTrack_<%=myEO2.getNIM3_Encounter().getEncounterID()%>').style.display = 'block';" value="Show" />&nbsp;<input name="showCT" type="button" class="inputButton_sm_Default" onclick="document.getElementById('comTrack_<%=myEO2.getNIM3_Encounter().getEncounterID()%>').style.display = 'none';" value="Hide" /> </p></td>
  </tr>
  <tr style="display:block" id="comTrack_<%=myEO2.getNIM3_Encounter().getEncounterID()%>">
    <td>
    
		    <%
	try
	{
		searchDB2 mySS_ct = new searchDB2();
		java.sql.ResultSet myRS_ct = null;
//		String mySQL_ct = ("SELECT commtrackid from tnim3_commtrack where (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=0) OR (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=" + myEO2.getNIM3_Encounter().getEncounterID() +") order by CommEnd desc");
		String mySQL_ct = ("SELECT commtrackid from tnim3_commtrack where (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=0) OR (encounterid=" + myEO2.getNIM3_Encounter().getEncounterID() +") order by CommStart desc");
		//out.println(mySQL);
  
		myRS_ct = mySS_ct.executeStatement(mySQL_ct);
		int i2cnt_ct=0;
		while(myRS_ct!=null&&myRS_ct.next())
		{
			i2cnt_ct++;
			bltNIM3_CommTrack myCT = new bltNIM3_CommTrack (new  Integer(myRS_ct.getString("commtrackid")));
			%>

		<table width="100%"  border="0" cellspacing="0" cellpadding="3" class="certborderPrintInt">
  <tr class=tdBaseAlt4Blue>
    <td class=tdBaseAlt4Blue>Type:</td>
    <td class=tdBaseAlt4Blue><%if (myCT.getCommTypeID()==3){%>Notification<%}else if (myCT.getCommTypeID()==4){%>See NetDev Request<%}if (myCT.getCommTypeID()==2){%>Referral/MSG<%}if (myCT.getCommTypeID()==1){%>MSG<%}if (myCT.getCommTypeID()==0){%>Note<%}%></td>
    <td>&nbsp;</td>
  </tr>
  <tr class=tdBaseAlt>
    <td>From:</td>
		<td width="75%" nowrap><strong><%=myCT.getMessageName()%></strong> @ <strong><%=myCT.getMessageCompany()%></strong></td>
		<td width="25%" align="right"><%if (myCT.getEncounterID().intValue()==myEO2.getNIM3_Encounter().getEncounterID().intValue())
		{
			out.println("E");
		}else
		{
			out.println("R");
		}%>&nbsp;</td>
  </tr>
  <%
	String myClass_CT_Duration = "";
	int hoursSince = Math.abs(PLCUtils.getHoursTill(myCT.getCommStart()));
	if (myCT.getCommEnd().after(NIMUtils.getBeforeTime()))
	{
			myClass_CT_Duration = "tdBaseAlt2";
	}
	else
	{
		if (hoursSince>48)
		{
			myClass_CT_Duration = "tdBaseAlt_Action3";
		}
		else if (hoursSince>24 || myEO2.isEscalatedSTAT())
		{
			myClass_CT_Duration = "tdBaseAlt_Action2";
		}
		else if (hoursSince>12 )
		{
			myClass_CT_Duration = "tdBaseAlt_Action1";
		}
		else
		{
			myClass_CT_Duration = "tdBaseAlt2";
		}
	}
  
  
  %>
  <tr class="<%=myClass_CT_Duration%>" class=tdBase>
    <td>Time:</td>
    <td>
<%				if (myCT.getCommEnd().after(NIMUtils.getBeforeTime()))
				{
					out.print(displayDateTimeSDF.format(myCT.getCommStart()) + "&nbsp;&nbsp;&nbsp;(Dur: <strong>" + Math.round((myCT.getCommEnd().getTime()-myCT.getCommStart().getTime())/1000/60) + "</strong> min)");
				}
				else 
				{
					out.print("Start: " + displayDateTimeSDF.format(myCT.getCommStart()) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Item Still Open&nbsp;&nbsp;&nbsp;Started: <strong>" + PLCUtils.getHoursTill_Display(myCT.getCommStart(), false)+ "  (" + PLCUtils.getDaysTill_Display(myCT.getCommStart(), true) + ")</strong>");
				}
%>				
</td>
    <td>Status:<jsp:include page="../generic/tCommTrackAlertStatusCodeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myCT.getAlertStatusCode()%>" /><jsp:param name="UseColor" value="y" /></jsp:include>
     </td>
  </tr>
  <%
  if (myCT.getCommTypeID()==4)
  {
  %>
  <tr bgcolor="#CCCCCC">
    <td colspan="3">Response:<br><%=myCT.getComments().replaceAll("(\r\n|\r|\n|\n\r)", "<br>")%></td>
  </tr>
  <%
  }
  %>
  <tr bgcolor="#FFFFFF">
    <td colspan="3"><%=myCT.getMessageText().replaceAll("(\r\n|\r|\n|\n\r)", "<br>")%></td>
  </tr>
  <tr bgcolor="#DDDDDD">
    <td>CommRef:</td>
    <td colspan="2"><%
		searchDB2 mySS_cr = new searchDB2();
		java.sql.ResultSet myRS_cr = null;
//		String mySQL_ct = ("SELECT commtrackid from tnim3_commtrack where (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=0) OR (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=" + myEO2.getNIM3_Encounter().getEncounterID() +") order by CommEnd desc");
		String mySQL_cr = ("SELECT commreferenceid from tnim3_commreference where commreferenceid = " + myCT.getCommReferenceID());
		//out.println(mySQL);
		myRS_cr = mySS_cr.executeStatement(mySQL_cr);
		int i2cnt_cr=0;
		if(myRS_cr!=null)
		{
			while(myRS_cr!=null&&myRS_cr.next())
			{
				i2cnt_cr++;
				bltNIM3_CommReference myCR = new bltNIM3_CommReference (new  Integer(myRS_cr.getString("commreferenceid")));
				if (myCR.getCRTypeID()==1)
				{
				%>	
				<a target="_blank" href="tNIM3_CommReference_view.jsp?EDITID=<%=myCR.getCommReferenceID()%>">View Notification</a>
				<%
				}
				else
				{
				%>	
				Other
				<%
				}
			}
		}
		else
		{
				%>	
				None
				<%
		}
	%></td>
  </tr>
</table>
			<%
		}
		mySS_ct.closeAll();
	}
	catch(Exception e)
	{
			out.println("ResultsSet:"+e);
	}
%>

    
    
    </td>
  </tr>
</table>

</td>
     </tr>
     <tr>
       <td colspan="2">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td><p class="tdHeader">All Documents:<br>&nbsp;<input name="showDox" type="button" class="inputButton_sm_Default" onclick="document.getElementById('AllDox_<%=myEO2.getNIM3_Encounter().getEncounterID()%>').style.display = 'block';" value="Show" />&nbsp;<input name="showDox" type="button" class="inputButton_sm_Default" onclick="document.getElementById('AllDox_<%=myEO2.getNIM3_Encounter().getEncounterID()%>').style.display = 'none';" value="Hide" /> </p></td>
  </tr>
  <tr style="display:none" id="AllDox_<%=myEO2.getNIM3_Encounter().getEncounterID()%>">
    <td>
		<table width="100%"  border="0" cellspacing="0" cellpadding="3" class="certborderPrintInt">
<tr class="tdBaseAlt2">
<td nowrap>Action</td>
<td nowrap>Description</td>
<td nowrap>Date Added</td>
<td nowrap>User ID</td>
<td nowrap>&nbsp;</td>
<td nowrap>Remove</td>
</tr>    
		    <%
	try
	{
		searchDB2 mySS_dox = new searchDB2();
		java.sql.ResultSet myRS_dox = null;
		String mySQL_dox = ("SELECT documentid from tnim3_document where (referralid = " + working_bltNIM3_Referral.getReferralID() + " and encounterid=0) OR (encounterid=" + myEO2.getNIM3_Encounter().getEncounterID() +")  order by uniquecreatedate desc");
		//out.println(mySQL);
  
		myRS_dox = mySS_dox.executeStatement(mySQL_dox);
		int i2cnt_dox=0;
		while(myRS_dox!=null&&myRS_dox.next())
		{
			i2cnt_dox++;
			bltNIM3_Document myDoc = new bltNIM3_Document (new  Integer(myRS_dox.getString("documentid")));
			String myDoxClass = "tdBase";
			boolean noRemove=false;
			if (myDoc.getDocType().intValue()==-1)
			{
				myDoxClass = "tdBaseAltStrikeRemove";
			}
			else if (myDoc.getDocumentID().intValue()==myEO2.getNIM3_Encounter().getReportFileID().intValue()||myDoc.getDocumentID().intValue()==working_bltNIM3_Referral.getRxFileID().intValue()||myDoc.getDocumentID().intValue()==working_bltNIM3_Referral.getOrderFileID().intValue()||myDoc.getDocumentID().intValue()==myEO2.getNIM3_Encounter().getBillingHCFA_ToPayer_FileID().intValue()||myDoc.getDocumentID().intValue()==myEO2.getNIM3_Encounter().getBillingHCFA_FromProvider_FileID().intValue())
			{
				myDoxClass = "tdBaseAltGreen";
				noRemove = true;
			}
			%>

  <tr class="<%=myDoxClass%>">
		<td nowrap><input  class="inputButton_sm_Default"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=myDoc.getDocumentID()%>&KM=p';" value="View" /></td>
		<td nowrap><strong><%=myDoc.getDocName()%> [<%=myDoc.getComments()%>]</strong></td>
		<td nowrap><strong><jsp:include page="../generic/DateTypeConvertDT_Main.jsp" flush="true" >
          <jsp:param name="CurrentSelection" value="<%=dbdf.format(myDoc.getUniqueCreateDate())%>" />          
          </jsp:include></strong></td>
		<td width="75%"  nowrap><strong><%=myDoc.getUploadUserID()%></strong></td>
		<td width="25%" align="right"><%if (myDoc.getEncounterID().intValue()==myEO2.getNIM3_Encounter().getEncounterID().intValue())
		{
			out.println("E");
		}
		else
		{
			out.println("R");
		}%>&nbsp;</td><td><%if (!noRemove){%><input class="inputButton_sm_Default"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=remdoc&EDITID=<%=myDoc.getDocumentID()%>&KM=p';" value="Remove" /><%}%></td>
  </tr>
			<%
		}
		mySS_dox.closeAll();
	}
	catch(Exception e)
	{
			out.println("ResultsSet:"+e);
	}
%>

</table>
    
    
    </td>
  </tr>
</table>

</td>
     </tr>
   </table>


<!-- end enc detail-->
                          
                          
                          
                          
                          
   					    </td>
						<td valign="top" align="center" colspan="5">

<table width="0%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td valign="top">
      <!-- Conf Actions Start -->
      
      <%
String but_class = "inputButton_md_Default";
%><table  border="2" align="center" cellpadding="2" cellspacing="0">
        <tr>
          <td nowrap bgcolor="#CCCCCC" class="tdHeader" >Users on this Case</td>
          </tr>
        <tr>
          <td bgcolor="#333333">&nbsp;</td>
          </tr>
        <tr>
          <td bgcolor="#CCCCCC">&nbsp;</td>
          </tr>
        <tr>
          <td bgcolor="#CCCCCC"><strong>Adjuster<br>      <div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getCase_Adjuster().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
          </strong></td>
          </tr>
        
  <%
if (myEO2.getNIM3_CaseAccount().getNurseCaseManagerID()>0)
{
%>
        <tr>
          <td bgcolor="#CCCCCC"><strong>Nurse Case Manager<br><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getCase_NurseCaseManager().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
          </strong></td>
          </tr>
  <%
}
if (myEO2.getNIM3_CaseAccount().getCaseAdministratorID()>0)
{
%>
        
        <tr>
          <td bgcolor="#CCCCCC"><strong>Case Admin<br><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getCase_Admin().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
          </strong></td>
          </tr>
        <%
}
if (myEO2.getNIM3_CaseAccount().getCaseAdministrator2ID()>0)
{
%>
        
        <tr>
          <td bgcolor="#CCCCCC"><strong>Case Admin 2<br><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getCase_Admin2().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
          </strong></td>
          </tr>
        <%
}
if (myEO2.getNIM3_CaseAccount().getCaseAdministrator3ID()>0)
{
%>
        
        <tr>
          <td bgcolor="#CCCCCC"><strong>Case Admin 3<br><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getCase_Admin3().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
          </strong></td>
          </tr>
        <%
}
if (myEO2.getNIM3_CaseAccount().getCaseAdministrator4ID()>0)
{
%>
        
        <tr>
          <td bgcolor="#CCCCCC"><strong>Case Admin 4<br><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getCase_Admin4().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
          </strong></td>
          </tr>  
  <%
}
%>
        <tr>
          <td bgcolor="#333333">&nbsp;</td>
          </tr>
        <tr>
          <td bgcolor="#CCCCCC"><strong>Referring Provider<br><div class=boxDr><jsp:include page="../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO2.getReferral_ReferringDoctor().getUserID()%>" /><jsp:param name="showExpand" value="y" /></jsp:include></div>
          </strong></td>
          </tr>
        <tr>
          <td bgcolor="#CCCCCC"><strong>IC/Provider</strong></td>
          </tr>
        </table>
</td>
    </tr>
</table>

<!-- Conf Actions End -->&nbsp;</td>
					  </tr>

<tr><td colspan="4">




</td></tr>
					<%
				}
				mySS.closeAll();
			}
			catch(Exception e)
			{
				out.println("ResultsSet:"+e);
			}
		%>
        </table>
	</div> 


</td>


 </tr>
        <%
    }//end while
	
       }//end of if
       else 
       {
           %>
           <tr><td colspan=3><b>Please click the "create" to add <%=ConfigurationMessages.getDataCategory("tNIM3_Referral")%> information or click 'Continue' to go to the next section.</b>
           <script language=javascript>
           if (confirm("<%=ConfigurationMessages.getInterviewMessage("INTERVIEWNoElements","tNIM3_Referral")%>"))
           {
               document.location="tNIM3_Referral_main_NIM3_Referral_CaseID_form_create.jsp?EDIT=new&KM=p&INTNext=yes"; 
           }
           else
           {

           }
           </script>
           </td></tr>
           <%
       }
    %>

    </table>
    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>


<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_Clear.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>