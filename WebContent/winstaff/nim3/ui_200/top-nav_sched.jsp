<%@page contentType="text/html" language="java" import="java.text.SimpleDateFormat, java.util.Date, com.winstaff.*" %>
<link rel="stylesheet" href="/winstaff/nim3/ui_200/style_sched.css" type="text/css">
<style>
html {
	overflow-y: scroll; 
}
</style>
<%@ include file="../../generic/CheckLogin.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    boolean accessValid = false;
    boolean isCase = false;
	
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
	if (pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
		isCase = true;
        accessValid = true;
    }
    else  
    {
        accessValid = true;
    }
	String iDelay = "5";
	if (request.getParameter("iDelay")!=null)
	{
		iDelay = request.getParameter("iDelay");
	}
	boolean showNav = true;
	if (request.getParameter("hidenav")!=null)
	{
		if (request.getParameter("hidenav").equalsIgnoreCase("y"))
		{
			showNav = false;
		};
	}
	
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
//	  bltNIM3_PatientAccount myPA = new bltNIM3_PatientAccount(myCA.getPatientAccountID());
//	  bltNIM3_EmployerAccount myEA = new bltNIM3_EmployerAccount(myPA.getEmployerID());
//	  bltNIM3_JobType myJT = new bltNIM3_JobType(myPA.getJobID());

%>
<jsp:include page="/winstaff/nim3/ui_200/top-nav_universal.jsp" flush="true" ></jsp:include>
<%
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
Date expireDate = sdf.parse("2014-01-01");
 
if(isScheduler&&expireDate.compareTo(new Date())>0){%>
	<div style="background: url('/winstaff/nim3/ui_200/images/xmas_lights.png') repeat-x;height: 68px;position: absolute;width: 100%;top: 22;">
    &nbsp;
    </div>
<%}
%>
<table width="1400px" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="/winstaff/nim3/images/title_v3-4.png" width="134" height="51">
    <%if (CurrentUserAccount.getUserID()==32110){%><br>
    	<img src="/winstaff/nim3/images/cosd.png">
    <%}%>
    
    </td>
  </tr>
<%
if (showNav)
{
	%>
  <tr>
    <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr style="vertical-align:top;">
        <td align="left">&nbsp;</td>
        <td align="center"><input name="button3" type="button" onclick="document.location='Payer_Home.jsp';" class="inputButton_md_Default" id="button3" value="Home" /></td>
        <td align="center"><% if (isScheduler){%><input name="button4" type="button" onclick="document.location='Worklist.jsp';" class="inputButton_md_Default" id="button4" value="Worklist" /><%}%></td>
        <td align="center" nowrap="nowrap"><input name="button2" type="button" onclick="document.location='tNIM3_CaseAccount_PayerID_query.jsp';" class="inputButton_md_Default" id="button2" value="Quick Search" /></td>
        <td align="center" nowrap="nowrap"><% if (isScheduler){%><input name="button" type="button" onclick="document.location='tNIM3_CaseAccount_Encounter_PayerID_query2.jsp';" class="inputButton_md_Default" id="button" value="Adv. Search" /><%}%></td>
        <td align="center" nowrap="nowrap"><% if (isScheduler){%><input name="button" type="button" onclick="document.location='tNIM3_CaseAccount_PayerID_query.jsp?ssSearchDICOM=Y';" class="inputButton_md_Default" id="button" value="DICOM Search" /><%}%></td>
        <td align="center"><input class="inputButton_md_Default" type=button onClick = "this.disabled=true;document.location ='tUserAccount_form_settings.jsp'" value="User Settings">&nbsp;</td>
        <td align="left" rowspan="2" ><%if (isScheduler){%><iframe width="300" height="110" src="top_notify.jsp?iDelay=<%=iDelay%>" marginheight="0" marginwidth="0" scrolling="no"></iframe><%}%></td>
        <td align="right" nowrap="nowrap">&nbsp;</td>
        <td width=500px rowspan="3">
		&nbsp;
        </td>
      </tr>
<%
}
%>
      
<%
if (isScheduler && showNav)
{
		
//		bltUserAccount myAdj = new bltUserAccount(myCA.getAdjusterID());
//		bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(myAdj.getPayerID());
%>
      <tr>
        <td align="left">&nbsp;</td>
        <td align="center" nowrap="nowrap"><% if (isScheduler4){%><input name="button3" type="submit" class="inputButton_md_Default" id="button3" onClick="alerty('This feature is in BETA and should only be used for testing');this.disabled = true; document.location='tNIM3_CaseAccount_Create_new1.jsp';" value="Create Case" /><%}%>&nbsp;</td>
        <td align="center" nowrap="nowrap"><% if (isScheduler){%><input name="button3" type="submit" class="inputButton_md_Default" id="button3" onClick="this.disabled = true; document.location='Reports_Home.jsp';" value="Reports" /><%}%>&nbsp;</td>
        <td align="center" nowrap="nowrap"><% if (isScheduler2){%><input name="button" type="submit" class="inputButton_md_Default" id="button" onClick="this.disabled = true; document.location='Maint_Home.jsp';" value="Maintenance" /><%}%>&nbsp;</td>
        <td align="center" nowrap="nowrap"><% if (isScheduler2){%><input name="button" type="submit" class="inputButton_md_Default" id="button" onClick="this.disabled = true; document.location='Billing_Home.jsp';" value="Billing Home" /><%}%>&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="right"><%if (isCase){bltNIM3_CaseAccount myCA = new bltNIM3_CaseAccount(iCaseID);%>Last Open Case: <%=myCA.getCaseClaimNumber()%> [<%=myCA.getPatientFirstName()%>&nbsp;<%=myCA.getPatientLastName()%>]<%}%></td>
      </tr>
<%
}
%>
    </table>
  <tr>
    <td><hr></td>
  </tr>
</table>
  <%}%>
<%}%>
