<%@page contentType="text/html" language="java" import="com.winstaff.*   com.winstaff.bltNIM3_CaseAccount" %>
<link rel="stylesheet" href="ui_200/style_sched.css" type="text/css">
<link rel="stylesheet" href="../ui_200/style_sched.css" type="text/css">
<%@ include file="../../generic/CheckLogin.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    Integer        iReferralID        =    null;
    Integer        iEncounterID        =    null;
    boolean accessValid = false;
    boolean isEnc = false;
    boolean isRef = false;
    boolean isCase = false;
	
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iCaseID")&&pageControllerHash.containsKey("iReferralID")&&pageControllerHash.containsKey("iEncounterID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        iReferralID        =    (Integer)pageControllerHash.get("iReferralID");
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
		isEnc = true;
		isRef = true;
		isCase = true;
    }
    else if (pageControllerHash.containsKey("iCaseID")&&pageControllerHash.containsKey("iReferralID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        iReferralID        =    (Integer)pageControllerHash.get("iReferralID");
        accessValid = true;
		isRef = true;
		isCase = true;
    }
    else if (pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
		isCase = true;
        accessValid = true;
    }
    else  
    {
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
//	  bltNIM3_PatientAccount myPA = new bltNIM3_PatientAccount(myCA.getPatientAccountID());
//	  bltNIM3_EmployerAccount myEA = new bltNIM3_EmployerAccount(myPA.getEmployerID());
//	  bltNIM3_JobType myJT = new bltNIM3_JobType(myPA.getJobID());

%>
<table width="1000" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="images/title_v3-1.jpg" width="1000" height="50"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr>
        <td align="left">&nbsp;</td>
        <td><a href="Payer_Home.jsp">Home</a></td>
        <td>&nbsp;</td>
        <td><a href="tNIM3_CaseAccount_PayerID_query.jsp">Search</a></td>
        <td>&nbsp;</td>
        <td width="75%" align="right"><a href="signOff.jsp">Logout</a></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table border="0" cellpadding="1" cellspacing="0">
<%
if (isCase)
{
		bltNIM3_CaseAccount myCA = new bltNIM3_CaseAccount(iCaseID);
		bltUserAccount myAdj = new bltUserAccount(myCA.getAdjusterID());
		bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(myAdj.getPayerID());
%>
  <tr>
    <td align="left"><span class="titleSub1">Case:</span></td>
    <td><input type="button" class="inputButton_sm_Default"  onclick="this.disabled=false;modalPost('CASEID', modalWin('LI_tCaseAccount_edit.jsp?QUICKVIEW=yes&EDITID=<%=iCaseID%>&EDIT=edit&KM=p','Search...','dialogWidth:800px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=800,height=800'));" value="Edit Case Data">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td align="right" class="tdBaseAlt">Claim Number:</td>
    <td class="tdBaseAlt"><strong><%=myCA.getCaseClaimNumber()%></strong></td>
    <td rowspan="2" align="right"><img src="images/child_line1.gif" /></td>
    <td align="right" class="tdBaseAlt">Patient:</td>
    <td class="tdBaseAlt"><strong><%=myCA.getPatientFirstName()%>&nbsp;<%=myCA.getPatientLastName()%></strong></td>
    <td rowspan="2"><img src="images/child_line1.gif"></td>
    <td align="right" class="tdBaseAlt">Address:</td>
    <td class="tdBaseAlt"><strong><%=myCA.getPatientAddress1()%><strong><br /><%=myCA.getPatientAddress2()%></strong></strong></td>
    </tr>
  <tr>
    <td align="right" class="tdBaseAlt">Carrier:</td>
    <td class="tdBaseAlt"><strong><%=myPM.getPayerName()%></strong></td>
    <td align="right" class="tdBaseAlt">DOB:</td>
    <td class="tdBaseAlt"><strong><jsp:include page="../../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(myCA.getPatientDOB())%>" /></jsp:include></strong></td>
    <td align="right" class="tdBaseAlt">City, State:</td>
    <td class="tdBaseAlt"><strong><%=myCA.getPatientCity()%>, <%=new bltStateLI(myCA.getPatientStateID()).getShortState()%> <%=myCA.getPatientZIP()%></strong></td>
    </tr>
  <tr>
    <td align="right" class="tdBaseAlt">Adjuster:</td>
    <td class="tdBaseAlt"><strong><%=myAdj.getContactFirstName()%>&nbsp;<%=myAdj.getContactLastName()%></strong></td>
    <td align="right" >&nbsp;</td>
    <td align="right" class="tdBaseAlt">SSN:</td>
    <td class="tdBaseAlt"><strong><%=(myCA.getPatientSSN())%>
    </strong></td>
    <td>&nbsp;</td>
    <td align="right" class="tdBaseAlt">Home Phone:</td>
    <td class="tdBaseAlt"><strong><%=myCA.getPatientHomePhone()%></strong></td>
    </tr>
  <tr>
    <td align="right" class="tdBaseAlt">Adj. Phone:</td>
    <td class="tdBaseAlt"><strong><%=myAdj.getContactPhone()%></strong></td>
    <td align="right" >&nbsp;</td>
    <td align="right" class="tdBaseAlt">DOI:</td>
    <td class="tdBaseAlt"><strong>
      <jsp:include page="../../generic/DateTypeConvert_Main.jsp" flush="true" >
        <jsp:param name="CurrentSelection" value="<%=dbdf.format(myCA.getDateOfInjury())%>" />    
        </jsp:include>
    </strong></td>
    <td>&nbsp;</td>
    <td align="right" class="tdBaseAlt">Mobile Phone:</td>
    <td class="tdBaseAlt"><strong><%=myCA.getPatientCellPhone()%></strong></td>
    </tr>
</table>
<%
}
if (isRef)
{
		bltNIM3_Referral working_bltNIM3_Referral  = new bltNIM3_Referral(iReferralID);
%>

      <br>
      <table border="0" cellpadding="1" cellspacing="0">
        <tr>
          <td colspan="3" rowspan="3"><img src="images/child_arrow1.gif"></td>
          <td align="left"><span class="titleSub1">Referral:</span></td>
          <td><%
if (working_bltNIM3_Referral.getRxFileID()>0)
{
%>
            <input  class="inputButton_sm_Create"  type="button" onClick="this.disabled=false;document.location='tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&EDITID=<%=working_bltNIM3_Referral.getRxFileID()%>&KM=p';" value="View Rx" />
          <%
}
%></td>
          <td>&nbsp;</td>
          <td colspan="2" align="left"><input type="button" value="" onclick="document.location='tNIM3_Referral_main_NIM3_Referral_CaseID.jsp';"></td>
        </tr>
        <tr>
          <td align="right" class="tdBaseAlt"> Type:&nbsp;</td>
          <td class="tdBaseAlt"><strong>
            <jsp:include page="../../generic/tReferralTypeLI_translate.jsp" flush="true" >
              <jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getReferralTypeID()%>" />      
            </jsp:include>
          </strong></td>
          <td rowspan="2"><img src="images/child_line1.gif"></td>
          <td align="right" class="tdBaseAlt">Referring Provider:</td>
          <td class="tdBaseAlt"><strong>
            <jsp:include page="../../generic/LI_tUserAccount_Search_translate.jsp" flush="true" >
              <jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getReferringPhysicianID()%>" />      
            </jsp:include>
          </strong></td>
        </tr>
        <tr>
          <td align="right" class="tdBaseAlt">StatusID:&nbsp;</td>
          <td class="tdBaseAlt"><strong>
            <jsp:include page="../../generic/tReferralStatusLI_translate.jsp" flush="true" >
              <jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getReferralStatusID()%>" />      
            </jsp:include>
          </strong></td>
          <td align="right" class="tdBaseAlt">Referred To  Provider:</td>
          <td class="tdBaseAlt"><strong>
            <jsp:include page="../../generic/LI_tUserAccount_Search_translate.jsp" flush="true" >
              <jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Referral.getAttendingPhysicianID()%>" />      
            </jsp:include>
          </strong></td>
        </tr>
      </table>
<%
}
if (isEnc)
{
		bltNIM3_Encounter working_bltNIM3_Encounter  = new bltNIM3_Encounter(iEncounterID);
%>
      <br>
<table border="0" cellpadding="1" cellspacing="0">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="3" rowspan="3">&nbsp;<img src="images/child_arrow1.gif"></td>
    <td align="left"><span class="titleSub1">Encounter:</span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2" align="left"><a href="tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp">View All Encounters</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right" class="tdBaseAlt"> Type:&nbsp;</td>
<td class="tdBaseAlt"><strong><jsp:include page="../../generic/tEncounterTypeLI_translate.jsp" flush="true" >
<jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Encounter.getEncounterTypeID()%>" />
</jsp:include></strong></td>
<td rowspan="2"><img src="images/child_line1.gif"></td>
<td align="right" class="tdBaseAlt">&nbsp;</td>
<td class="tdBaseAlt">
</td>
</tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right" class="tdBaseAlt">Service Date:</td>
    <td class="tdBaseAlt"><strong><jsp:include page="../../generic/DateTypeConvert_Main.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(working_bltNIM3_Encounter.getDateOfService())%>" /></jsp:include></strong></td>
    <td align="right" class="tdBaseAlt">Attending Provider:</td>
    <td class="tdBaseAlt"><strong><jsp:include page="../../generic/LI_tUserAccount_Search_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Encounter.getAttendingPhysicianID()%>" /></jsp:include></strong></td>
  </tr>
</table>
  </tr>
<%
}
%>


  <tr>
    <td><hr></td>
  </tr>
</table>
      <%}%>
<%}%>