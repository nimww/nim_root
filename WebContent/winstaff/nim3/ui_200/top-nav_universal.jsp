<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%@ include file="/winstaff/generic/CheckLogin.jsp" %>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

#topbar {
	background-color:#000;
	color:#FFF;
	padding:5px;
	height:25px;
	font-family: Arial, Helvetica, sans-serif;
	text-align: justify;
	
}

#topbar .final {
	float:right;
	
	
}

#topbar * {
  display: inline;
}

#topbar li {
	display:inline;
	font-size:12px;
}

#topbar a:hover {
	background-color:#666;
}

#topbar a {
	color:#FFF;
	font-style:normal;
	text-decoration:none;
	padding:15px;
}

#topbar span {
  display: inline-block;
  position: relative;
  width: 100%;
  height: 5px;
}

#topbar .first {
}

-->
</style>
<link type="text/css" href="css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="Stylesheet" />	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="js/jquery.tablesorter.js"></script>

	<script type="text/javascript">
		function searchTop(){
			var temp = $.trim(document.topSearchForm.temp.value);
			if (temp.indexOf(',') !== -1){
				var temparr = temp.replace(/\s/g, '').split(",");
				document.topSearchForm.ssSearchMe.value = temparr[0];
				document.topSearchForm.ssSearchMe2.value = temparr[1];
			}
			else if(temp.indexOf(':') !== -1){
				var temparr = temp.split(":");
				document.topSearchForm.ssSearchMe.value = temparr[0];
				document.topSearchForm.ssSearchMe2.value = temparr[1];
			}
			else if(temp.indexOf(' ') !== -1){
				var temparr = temp.split(" ");
				document.topSearchForm.ssSearchMe2.value = temparr[0];
				document.topSearchForm.ssSearchMe.value = temparr[1];
			}
			else{
				document.topSearchForm.ssSearchMe.value = temp;
				document.topSearchForm.ssSearchMe2.value = 'undefined';
			}
		}
	
	</script>
    <div id="topbar">
    <li class="first"><strong>NextImage&nbsp;Grid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></li>
    <% if (isScheduler){ %>
    <li><a target="_top" href="/winstaff/nim3/Payer_Home.jsp">Scheduling</a></li>
    <% } %>
    <% if (isGenAdmin){ %>
    <li><a target="_top" href="/winstaff/app/ui_200/ui_init.jsp">NetDev</a></li>
    <% } %>
    <% if (isReporter){ %>
    <li><a target="_top" href="/winstaff/nim3/Reports_Home.jsp">Reports</a></li>
    <% } %>
    <% if (isGenAdmin){ %><!--  isScheduler3-->
    <li><a target="_top" href="/winstaff/nim3/tNIM3_PayerMaster_query.jsp">Payer&nbsp;Maintenance</a></li>
    <% } %>
    <% if (isScheduler3){ %><!--  isScheduler2-->
    <li><a target="_top" href="/winstaff/nim3/gsn_nim3b/tUserAccount_query.jsp">User&nbsp;Maintenance</a></li>
    <% } %>
    <% if (isScheduler3){ %>
    <li><a target="_top" href="/winstaff/nim3/Billing_Home.jsp">Billing</a></li>
    <% } %>
    <li>&nbsp;</li>
    <li>&nbsp;</li>
	
		<li>
			<form method="post" action="/winstaff/nim3/tNIM3_CaseAccount_PayerID_query.jsp" name="topSearchForm" id="topSearchForm" style="height:20px;overflow:hidden;" target="_top">
			  <input name="temp" type="text" autocomplete="off" onchange="searchTop()" value="Search for patients..." onclick="this.value=='Search for patients...'?this.value='':this.value" onblur="this.value==''?this.value='Search for patients...':this.value"/>
			  <input name="ssSearchMe" type="text" style="display:none;"/>
			  <input name="ssSearchMe2" type="text" style="display:none;"/>
			<input name="input" type="submit" class="inputButton_md_Default" value="Search" style="position:absolute;top:-100px"/>
			<input type="hidden" name="maxResults" value=25 />
			<input type="hidden" name="startID" value=0 />
			</form>
	</li>


    <li>&nbsp;</li>
    <li>&nbsp;</li>
    <li>&nbsp;</li>
    <li>&nbsp;</li>
    <li>&nbsp;</li>
    <li ><%=CurrentUserAccount.getContactFirstName()%>&nbsp;<%=CurrentUserAccount.getContactLastName()%> <a href="/winstaff/nim3/signOff.jsp" title="Sign out of NextImageGrid">X</a></li>
	
	
	
	</div>
