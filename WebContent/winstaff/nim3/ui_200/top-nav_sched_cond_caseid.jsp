<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<link rel="stylesheet" href="ui_200/style_sched.css" type="text/css">
<link rel="stylesheet" href="../ui_200/style_sched.css" type="text/css">
<%@ include file="../../generic/CheckLogin.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iCaseID       =    null;
    boolean accessValid = false;
    boolean isCase = false;
	
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
	if (pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
		isCase = true;
        accessValid = true;
    }
    else  
    {
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
	  bltNIM3_CaseAccount myCA = new bltNIM3_CaseAccount(iCaseID);
%>
<style type="text/css">
<!--
body,td,th {
	color: #333;
}
body {
	background-color: #333;
}
-->
</style><table bgcolor="#EEEEEE" width="100%" border="1" cellspacing="0" cellpadding="2">
  <tr class="tdHeaderAlt">
    <td class="tdHeader">Patient <input name="R"  value="Reset" type="button" onclick="document.location=document.location;" /></td>
    <td class="tdHeader">Pre-Screen</td>
  </tr>
  <tr>
    <td valign="top"><strong><%=myCA.getPatientLastName()%></strong>, <strong><%=myCA.getPatientFirstName()%></strong></td>
    <td rowspan="4" valign="top">
    <%{
	java.util.Vector myV = NIM3_EncounterObject.getPatientPreScreen(myCA);
	for (int iTemp=0;iTemp<myV.size();iTemp++)
	{
		if (iTemp!=0)
		{
			out.print("<br>");
		}
		out.print(myV.elementAt(iTemp));
	}
	}%>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">SSN: <strong><%=myCA.getPatientSSN()%></strong>&nbsp;&nbsp;&nbsp;DOB: <strong><%=PLCUtils.getDisplayDate(myCA.getPatientDOB(),false)%></strong></td>
  </tr>
  <tr>
    <td valign="top">Home: <strong><%=myCA.getPatientHomePhone()%></strong>&nbsp;&nbsp;&nbsp;Mobile: <strong><%=myCA.getPatientCellPhone()%></strong></td>
  </tr>
  <tr>
    <td valign="top">Height: <strong><%=myCA.getPatientHeight()%></strong>&nbsp;&nbsp;&nbsp;Weight: <strong><%=myCA.getPatientWeight()%> lbs</strong></td>
  </tr>
</table>



  <%}else{out.print("no caseid");}%>
<%}else{out.print("no security");}%>
