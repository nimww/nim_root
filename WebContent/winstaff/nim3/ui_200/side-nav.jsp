




<%@page contentType="text/html" language="java" import="com.winstaff.stepVerify_PhysicianID,com.winstaff.bltStateLI, com.winstaff.bltPhysicianMaster" %>

<%@ include file="../../generic/CheckLogin.jsp" %>


<html>
<head>
<title>side-nav</title>
<meta http-equiv="Content-Type" content="text/html;">
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Oct 07 16:23:07 GMT-0700 (Pacific Daylight Time) 2002-->
<link rel="stylesheet" href="style_SideNav.css" type="text/css">
<script language="JavaScript">
<!--
function MM_popupMsg(msg) { //v1.0
  alert(msg);
}
//-->
</script>
</head>
<base target="wsMain">
<%
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("HCOID")&&!CurrentUserAccount.getStartPage().equalsIgnoreCase("../phdb/tHCOPhysicianStanding_query.jsp"))
{
%>
<body bgcolor="#A49E86" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" text="#000000" link="#000000" vlink="#000000" alink="#000000">
<%
}
else if (CurrentUserAccount.getStartPage().equalsIgnoreCase("../phdb/tHCOPhysicianStanding_query.jsp"))
{
%>
<body bgcolor="#A49E86" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" text="#000000" link="#000000" vlink="#000000" alink="#000000">
<%
}
else
{
%>
<body bgcolor="#999999" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" text="#000000" link="#000000" vlink="#000000" alink="#000000">
<%
}
%>
<table border="0" width="150" cellspacing="0" cellpadding="2" bordercolor="#666666" align="center" >
  
<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center">
<%
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("PayerID"))
{
%><a href="../tPayer_start.jsp"><img src="sid-nav_Images/practitioners_icon2-search.gif" border="0" width="48" height="41"><br>Home</a>
<%
}
else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("PatientID"))
{
%><a href="../tPatient_start.jsp"><img src="sid-nav_Images/practitioners_icon2-search.gif" border="0" width="48" height="41"><br>Home</a>
<%
}
else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("ICID"))
{
%><a href="../tIC_start.jsp"><img src="sid-nav_Images/practitioners_icon2-search.gif" border="0" width="48" height="41"><br>Home</a>
<%
}
%></td>
    <td align="center"><%
if (CurrentUserAccount.getAccountType().equalsIgnoreCase("PayerID"))
{
%><a href="../tPayer_home.jsp?CaseID=&CasePass=&PatientFirstName=&PatientLastName=&CaseClaimNumber1=&orderBy=CaseID&startID=0&maxResults=50&Submit2=Submit"><img src="sid-nav_Images/resources_icon1.gif" border="0" width="64" height="47"><br>
      Search</a>
      <%
}
%></td>
  </tr>
  <tr>
    <td align="center"><a href="../help.jsp"  target="_blank"><img src="sid-nav_Images/help_icon1.gif" width="45" height="49" border="0"><br> 
      Help
</a></td>
    <td align="center"><a href="signOff.jsp" target="_top"><img src="sid-nav_Images/signOff1.gif" width="38" height="43" border="0"><br>
              Sign Off</a></td>
  </tr>
</table></td><td bgcolor="#000000">
</td>
</tr>
<td>

<table width="100%" border="0" align="center" cellpadding="2">
  <tr>
    <td colspan="2" class="tdHeaderAlt">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="tdHeaderAlt">You are signed in as:</td>
  </tr>
  <tr>
    <td class="tdBaseAlt">&nbsp;</td>
    <td class="tdBaseAlt"><%=CurrentUserAccount.getLogonUserName()%></td>
  </tr>
  <tr>
    <td colspan="2" class="tdHeaderAlt">Contact Information</td>
  </tr>
  <tr>
    <td class="tdBaseAlt">&nbsp;</td>
    <td class="tdBaseAlt"><%=CurrentUserAccount.getContactFirstName()%>&nbsp;<%=CurrentUserAccount.getContactLastName()%><br>
<%=CurrentUserAccount.getContactPhone()%></td>
  </tr>
</table></td>
<td width="5" bgcolor="#000000">&nbsp;</td>
</tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td bgcolor="#000000">&nbsp;</td>
  </tr>
</table>
<blockquote>&nbsp;</blockquote>
</body>
</html>
