<script src="/winstaff/js/jQuery.js"></script>
<script language="javascript">
	function show_pws_Loading()
	{
		document.all.pws_Loading.style.pixelTop = (document.body.scrollTop + 50);
		document.all.pws_Loading.style.visibility="visible";
	}	
	function hide_pws_Loading()
	{
		document.all.pws_Loading.style.visibility="hidden";
	}	
	function show_pws_Saving()
	{
		document.all.pws_Saving.style.pixelTop = (document.body.scrollTop + 50);
		document.all.pws_Saving.style.visibility="visible";
	}	
	function hide_pws_Saving()
	{
		document.all.pws_Saving.style.visibility="hidden";
	}	
	function show_pws_Calc()
	{
		document.all.pws_Calc.style.pixelTop = (document.body.scrollTop + 50);
		document.all.pws_Calc.style.visibility="visible";
	}	
	function hide_pws_Calc()
	{
		document.all.pws_Calc.style.visibility="hidden";
	}	
</script>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<link rel="stylesheet" href="ui_200/style_sched.css" type="text/css">
<DIV ID="pws_Loading" STYLE="position:absolute;z-index:5;top:30%;left:42%;visibility:hidden">
<center>				<FONT FACE="Arial" SIZE="5" COLOR="blue"><B>Loading<br>
						Please Wait</B><br><img src="../js/load1.gif"></FONT></center>
</DIV>
<DIV ID="pws_Saving" STYLE="position:absolute;z-index:5;top:30%;left:42%;visibility:hidden">
<center>				<FONT FACE="Arial" SIZE="5" COLOR="blue"><B>Saving<br>
						Please Wait</B><br><img src="../js/load1.gif"></FONT></center>
</DIV>
<DIV ID="pws_Calc" STYLE="position:absolute;z-index:5;top:30%;left:42%;visibility:hidden">
<center>				<FONT FACE="Arial" SIZE="5" COLOR="blue"><B>Calculating<br>
						Please Wait</B><br><img src="../js/load1.gif"></FONT></center>
</DIV>
