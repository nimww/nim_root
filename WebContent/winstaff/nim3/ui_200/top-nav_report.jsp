<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<link rel="stylesheet" href="/winstaff/nim3/ui_200/style_sched.css" type="text/css">
<%@ include file="../../generic/CheckLogin.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    boolean accessValid = false;
    boolean isCase = false;
	
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
	if (pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
		isCase = true;
        accessValid = true;
    }
    else  
    {
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
//	  bltNIM3_PatientAccount myPA = new bltNIM3_PatientAccount(myCA.getPatientAccountID());
//	  bltNIM3_EmployerAccount myEA = new bltNIM3_EmployerAccount(myPA.getEmployerID());
//	  bltNIM3_JobType myJT = new bltNIM3_JobType(myPA.getJobID());

%>
<jsp:include page="/winstaff/nim3/ui_200/top-nav_universal.jsp" flush="true" ></jsp:include>
<table width="1000" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="left"><img src="/winstaff/nim3/images/title_v3-3.png" width="960" height="75" /></td>
        <td width="38%" align="right" nowrap="nowrap">Signed in as: <%=CurrentUserAccount.getContactFirstName()%>&nbsp;<%=CurrentUserAccount.getContactLastName()%><br />
          <a href="signOff.jsp">Logout</a></td>
      </tr>
<%
if (isScheduler||isReporter)
{
%>
<%
}
%>
    </table>
  <tr>
    <td><hr></td>
  </tr>
</table>
  <%}%>
<%}%>
<DIV ID="pleasewaitScreen" STYLE="position:absolute;z-index:5;top:30%;left:42%;visibility:visible">
	<TABLE BGCOLOR="#000000" BORDER="1" BORDERCOLOR="#000000" CELLPADDING="0" CELLSPACING="0" HEIGHT="100" WIDTH="150" ID="Table1">
		<TR>
			<TD WIDTH="100%" HEIGHT="100%" BGCOLOR="#DDDDDD" ALIGN="CENTER" VALIGN="MIDDLE">
				<FONT FACE="Arial" SIZE="4" COLOR="blue"><B>Loading...<br>
						Please Wait</B><br><img src="../js/load1.gif"></FONT>
			</TD>
		</TR>
	</TABLE>
</DIV>