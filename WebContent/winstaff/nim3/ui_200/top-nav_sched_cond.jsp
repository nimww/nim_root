<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<link rel="stylesheet" href="ui_200/style_sched.css" type="text/css">
<link rel="stylesheet" href="../ui_200/style_sched.css" type="text/css">
<%@ include file="../../generic/CheckLogin.jsp" %>
<%
//initial declaration of list class and parentID
    Integer        iEncounterID       =    null;
    Integer        iCaseID       =    null;
    boolean accessValid = false;
    boolean isEncounter = false;
    boolean isCase = false;
	
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0&&isScheduler)
   {
	if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
		isEncounter = true;
        accessValid = true;
    }
	else if (pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
		isCase = true;
        accessValid = true;
    }
    else  
    {
        accessValid = true;
    }
	Integer iDelay = null;
	if (request.getParameter("iDelay")!=null&&!request.getParameter("iDelay").equalsIgnoreCase(""))
	{
		iDelay = new Integer(request.getParameter("iDelay"));
	}
  //page security
  if (iDelay!=null)
  {
	  %>
      <meta http-equiv="refresh" content="1;URL=top-nav_sched_cond.jsp" />
		Pausing
      <%
  }
  else if (accessValid&&isEncounter)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
	  NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);
%>
<style type="text/css">
<!--
body,td,th {
	color: #333;
}
body {
	background-color: #333;
}
-->
</style><table bgcolor="#EEEEEE" width="98%" border="1" cellspacing="0" cellpadding="1">
  <tr class="tdHeaderAlt">
    <td class="tdHeader">Patient <input name="R" type="button" class="inputButton_sm_Test" onclick="document.location=document.location;"  value="Refresh" /></td>
    <td class="tdHeader">Pre-Screen</td>
    <td nowrap="nowrap" class="tdHeader">Case Notes</td>
    <td nowrap="nowrap" class="tdHeader">Important Notes</td>
    <td class="tdHeader">Procedure</td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap"><strong><%=myEO.NIM3_CaseAccount.getPatientLastName()%></strong>, <strong><%=myEO.NIM3_CaseAccount.getPatientFirstName()%></strong></td>
    <td rowspan="8" valign="top">
	<%{
	java.util.Vector myV = myEO.getPatientPreScreen();
	for (int iTemp=0;iTemp<myV.size();iTemp++)
	{
		if (iTemp!=0)
		{
			out.print("<br>");
		}
		out.print(myV.elementAt(iTemp));
	}
	}%>&nbsp;</td>
    <td rowspan="8" valign="middle"  <% if(!myEO.getCaseDashboard_CombinedNotes("<br>").equalsIgnoreCase("")){out.println(" class=\"tdBaseAlt_Action2\" ");}%>><span class="tdBaseAlt">
    <form action="../tNIM3_Encounter_notes_sub.jsp" method="post"><input type="hidden" name="EDIT" value="top_navEDIT" /><input type="hidden" name="EDITID" value="<%=myEO.NIM3_Encounter.getEncounterID()%>" /><textarea name="Comments" cols="30" rows="6" class="borderHighlightBlackDottedNoFont" style="background-color:#EEE" ><%=myEO.NIM3_CaseAccount.getComments()%></textarea><br />
<input name="" type="submit" value="Update" /></form>
    </span></td>
    <td rowspan="8" valign="middle"  <% if(!myEO.getCaseDashboard_CombinedNotes("<br>").equalsIgnoreCase("")){out.println(" class=\"tdBaseAlt_Action2\" ");}%>><%=myEO.getCaseDashboard_CombinedNotes("<br>")%></td>
    <td rowspan="8" valign="top">
	  <%
if (myEO.NIM3_Referral.getRxFileID()!=0)
{
%>
	  <a target="_blank" href="../tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp?EDIT=view&amp;EDITID=<%=myEO.NIM3_Referral.getRxFileID()%>&amp;KM=p';" class="tdHeader" >View Rx</a>
      <%
}
%>
      &nbsp;<br />
	Is STAT: <strong><jsp:include page="../../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getisSTAT()%>" /></jsp:include></strong>
<br />	Requires Aging: <strong><jsp:include page="../../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myEO.NIM3_Encounter.getRequiresAgeInjury()%>" /></jsp:include></strong>
	<br /><%{
		
	//declaration of Enumeration
		bltNIM3_Service        NIM3_Service;
		ListElement         leCurrentElement;
		java.util.Enumeration eList = myEO.NIM3_Service_List.elements();
		while (eList.hasMoreElements())
		{
			leCurrentElement    = (ListElement) eList.nextElement();
			NIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
			%>
            <strong><%=NIM3_Service.getCPT()%></strong>&nbsp;&nbsp;BP: <strong><%=NIM3_Service.getCPTBodyPart()%></strong>
    <%
		}
		
		
		
		}%></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap">SSN: <strong><%=myEO.NIM3_CaseAccount.getPatientSSN()%></strong>&nbsp;&nbsp;&nbsp;DOB: <strong><%=PLCUtils.getDisplayDate(myEO.NIM3_CaseAccount.getPatientDOB(),false)%></strong></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap">Home: <strong><%=myEO.NIM3_CaseAccount.getPatientHomePhone()%></strong>&nbsp;&nbsp;&nbsp;Mobile: <strong><%=myEO.NIM3_CaseAccount.getPatientCellPhone()%></strong></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap">Height: <strong><%=myEO.NIM3_CaseAccount.getPatientHeight()%></strong>&nbsp;&nbsp;&nbsp;Weight: <strong><%=myEO.NIM3_CaseAccount.getPatientWeight()%> lbs</strong></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap" class="tdHeaderAlt">Referring Dr.</td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap"><strong><%=myEO.Referral_ReferringDoctor.getContactLastName()%></strong>, <strong><%=myEO.Referral_ReferringDoctor.getContactFirstName()%></strong></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap">Phone: <strong><%=myEO.Referral_ReferringDoctor.getContactPhone()%></strong>&nbsp;&nbsp;&nbsp;Fax: <strong><%=myEO.Referral_ReferringDoctor.getContactFax()%></strong></td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap"><strong><%=myEO.Referral_ReferringDoctor.getCompanyName()%></strong> - <strong><%=myEO.Referral_ReferringDoctor.getContactCity()%></strong></td>
  </tr>
</table>



  <%}
  else if (accessValid&&isCase)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
	  bltNIM3_CaseAccount myCA = new bltNIM3_CaseAccount(iCaseID);
%>
<style type="text/css">
<!--
body,td,th {
	color: #333;
}
body {
	background-color: #333;
}
-->
</style><table bgcolor="#EEEEEE" width="98%" border="1" cellspacing="0" cellpadding="1">
  <tr class="tdHeaderAlt">
    <td class="tdHeader">Patient <input name="R" type="button" class="inputButton_sm_Test" onclick="document.location=document.location;"  value="Refresh" /></td>
    <td class="tdHeader">Pre-Screen</td>
  </tr>
  <tr>
    <td valign="middle" nowrap="nowrap"><strong><%=myCA.getPatientLastName()%></strong>, <strong><%=myCA.getPatientFirstName()%></strong></td>
    <td rowspan="5" valign="top">
    <%{
	java.util.Vector myV = NIM3_EncounterObject.getPatientPreScreen(myCA);
	for (int iTemp=0;iTemp<myV.size();iTemp++)
	{
		if (iTemp!=0)
		{
			out.print("<br>");
		}
		out.print(myV.elementAt(iTemp));
	}
	}%>&nbsp;</td>
  </tr>
  <tr valign="middle">
    <td nowrap="nowrap">SSN: <strong><%=myCA.getPatientSSN()%></strong>&nbsp;&nbsp;&nbsp;DOB: <strong><%=PLCUtils.getDisplayDate(myCA.getPatientDOB(),false)%></strong></td>
  </tr>
  <tr valign="middle">
    <td nowrap="nowrap">Home: <strong><%=myCA.getPatientHomePhone()%></strong>&nbsp;&nbsp;&nbsp;Mobile: <strong><%=myCA.getPatientCellPhone()%></strong></td>
  </tr>
  <tr valign="middle">
    <td nowrap="nowrap">Height: <strong><%=myCA.getPatientHeight()%></strong>&nbsp;&nbsp;&nbsp;Weight: <strong><%=myCA.getPatientWeight()%> lbs</strong></td>
  </tr>
  <tr valign="middle">
    <td nowrap="nowrap">&nbsp;</td>
  </tr>
</table>



  <%}else{out.print("no caseid");}%>
  
<%}%>
