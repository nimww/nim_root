<%@page contentType="text/html" language="java" import="com.winstaff.stepVerify_PhysicianID,com.winstaff.bltStateLI, com.winstaff.bltPhysicianMaster" %>

<%@ include file="../../generic/CheckLogin.jsp" %>

<%
    Integer        iSecurityGroupID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iSecurityGroupID")) 
    {
        iSecurityGroupID        =    (Integer)pageControllerHash.get("iSecurityGroupID");
        accessValid = true;
    }

%>
<html>
<head>
<title>top-nav_SecurityGroupID.jpg</title>
<meta http-equiv="Content-Type" content="text/html;">
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Thu Sep 26 16:11:52 GMT-0700 (Pacific Daylight Time) 2002-->
<script language="JavaScript">function fwLoadMenus() {
  if (window.fw_menu_0) return;
  window.fw_menu_0 = new Menu("root",152,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_0.addMenuItem("Security Group Info","location='tSecurityGroupMaster_form.jsp?nullParam=null&EDIT=edit'");
  fw_menu_0.addMenuItem("Manage Item Security","location='tGroupSecurity_main_GroupSecurity_SecurityGroupID.jsp'");
  fw_menu_0.addMenuItem("Manage Field Security","location='tFieldSecurity_query.jsp'");
  fw_menu_0.addMenuItem("Manage Look-Up Tables","location='maint_lookupTables.jsp'");
  fw_menu_0.addMenuItem("---","location='#'");
  fw_menu_0.addMenuItem("Return to Maintenance Manager...","location='pro-file-company_Status.jsp'");
   fw_menu_0.fontWeight="bold";
   fw_menu_0.hideOnMouseOut=true;
  window.fw_menu_1 = new Menu("root",161,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_1.addMenuItem("Frequently Asked Questions","location='#'");
  fw_menu_1.addMenuItem("Contact Us","location='#'");
  fw_menu_1.addMenuItem("About","location='#'");
   fw_menu_1.fontWeight="bold";
   fw_menu_1.hideOnMouseOut=true;

  fw_menu_1.writeMenus();
} 
// fwLoadMenus()

//-->
</script>
<script language="JavaScript1.2" src="ui_1/fw_menu.js"></script>
<link rel="stylesheet" href="ui_1/style_SecurityGroupID.css" type="text/css">
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff">
<script language="JavaScript1.2">fwLoadMenus();</script>
<table border="0" cellpadding="0" cellspacing="0" width="700">
<!-- fwtable fwsrc="top-nav2_SecurityGroupID.png" fwbase="top-nav_SecurityGroupID.jpg" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->

  <tr>
   <td colspan="7"><img name="topnav_SecurityGroupID_r1_c1" src="ui_1/images/top-nav_SecurityGroupID_r1_c1.jpg" width="700" height="66" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="1" height="66" border="0"></td>
  </tr>
  <tr>
   <td rowspan="2"><img name="topnav_SecurityGroupID_r2_c1" src="ui_1/images/top-nav_SecurityGroupID_r2_c1.jpg" width="135" height="34" border="0"></td>
   <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_0,136,91);" ><img name="topnav_SecurityGroupID_r2_c2" src="ui_1/images/top-nav_SecurityGroupID_r2_c2.jpg" width="92" height="25" border="0"></a></td>
   <td rowspan="2"><img name="topnav_SecurityGroupID_r2_c3" src="ui_1/images/top-nav_SecurityGroupID_r2_c3.jpg" width="222" height="34" border="0"></td>
   <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_1,450,91);" ><img name="topnav_SecurityGroupID_r2_c4" src="ui_1/images/top-nav_SecurityGroupID_r2_c4.jpg" width="59" height="25" border="0"></a></td>
   <td rowspan="2"><img name="topnav_SecurityGroupID_r2_c5" src="ui_1/images/top-nav_SecurityGroupID_r2_c5.jpg" width="7" height="34" border="0"></td>
    <td><a href="signOff.jsp"><img name="topnav_SecurityGroupID_r2_c6" src="ui_1/images/top-nav_SecurityGroupID_r2_c6.jpg" width="77" height="25" border="0"></a></td>
    <td rowspan="2"><img name="topnav_SecurityGroupID_r2_c7" src="ui_1/images/top-nav_SecurityGroupID_r2_c7.jpg" width="108" height="34" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="1" height="25" border="0"></td>
  </tr>
  <tr>
   <td><img name="topnav_SecurityGroupID_r3_c2" src="ui_1/images/top-nav_SecurityGroupID_r3_c2.jpg" width="92" height="9" border="0"></td>
   <td><img name="topnav_SecurityGroupID_r3_c4" src="ui_1/images/top-nav_SecurityGroupID_r3_c4.jpg" width="59" height="9" border="0"></td>
   <td><img name="topnav_SecurityGroupID_r3_c6" src="ui_1/images/top-nav_SecurityGroupID_r3_c6.jpg" width="77" height="9" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="1" height="9" border="0"></td>
  </tr>
  <tr>
   <td><img src="ui_1/images/spacer.gif" width="135" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="92" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="222" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="59" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="7" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="77" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="108" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="1" height="1" border="0"></td>
  </tr>
</table>
</body>
</html>
