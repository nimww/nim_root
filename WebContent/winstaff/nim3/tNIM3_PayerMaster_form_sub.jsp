<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tNIM3_PayerMaster_form_sub.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iEDITID        =    null;
	boolean isNew = false;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
		if (iEDITID==0)
		{
	    	isNew= true;
		}
    }
    else
    {
    	isNew= true;
		iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

  //page security
  if (isScheduler3)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID

    bltNIM3_PayerMaster        NIM3_PayerMaster        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        NIM3_PayerMaster        =    new    bltNIM3_PayerMaster(iEDITID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("new") )
    {
        NIM3_PayerMaster        =    new    bltNIM3_PayerMaster(UserSecurityGroupID,true);
    }

String testChangeID = "0";

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate"))) ;
    if ( !NIM3_PayerMaster.getUniqueCreateDate().equals(testObj)  )
    {
         NIM3_PayerMaster.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate"))) ;
    if ( !NIM3_PayerMaster.getUniqueModifyDate().equals(testObj)  )
    {
         NIM3_PayerMaster.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !NIM3_PayerMaster.getUniqueModifyComments().equals(testObj)  )
    {
         NIM3_PayerMaster.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster UniqueModifyComments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PayerName")) ;
    if ( !NIM3_PayerMaster.getPayerName().equals(testObj)  )
    {
         NIM3_PayerMaster.setPayerName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster PayerName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AutoAuth")) ;
    if ( !NIM3_PayerMaster.getAutoAuth().equals(testObj)  )
    {
         NIM3_PayerMaster.setAutoAuth( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster AutoAuth not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactName")) ;
    if ( !NIM3_PayerMaster.getContactName().equals(testObj)  )
    {
         NIM3_PayerMaster.setContactName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster ContactName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingAddress1")) ;
    if ( !NIM3_PayerMaster.getBillingAddress1().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillingAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillingAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingAddress2")) ;
    if ( !NIM3_PayerMaster.getBillingAddress2().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillingAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillingAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingCity")) ;
    if ( !NIM3_PayerMaster.getBillingCity().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillingCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillingCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("BillingStateID")) ;
    if ( !NIM3_PayerMaster.getBillingStateID().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillingStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillingStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingZIP")) ;
    if ( !NIM3_PayerMaster.getBillingZIP().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillingZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillingZIP not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingPhone")) ;
    if ( !NIM3_PayerMaster.getBillingPhone().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillingPhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillingPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingFax")) ;
    if ( !NIM3_PayerMaster.getBillingFax().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillingFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillingFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("BillingEmail")) ;
    if ( !NIM3_PayerMaster.getBillingEmail().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillingEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillingEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !NIM3_PayerMaster.getComments().equals(testObj)  )
    {
         NIM3_PayerMaster.setComments( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Comments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("BillingEntityID")) ;
    if ( !NIM3_PayerMaster.getBillingEntityID().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillingEntityID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillingEntityID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PayerTypeID")) ;
    if ( !NIM3_PayerMaster.getPayerTypeID().equals(testObj)  )
    {
         NIM3_PayerMaster.setPayerTypeID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster PayerTypeID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PayableTo")) ;
    if ( !NIM3_PayerMaster.getPayableTo().equals(testObj)  )
    {
         NIM3_PayerMaster.setPayableTo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster PayableTo not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_MRI_WO"))) ;
    if ( !NIM3_PayerMaster.getPrice_Mod_MRI_WO().equals(testObj)  )
    {
         NIM3_PayerMaster.setPrice_Mod_MRI_WO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Price_Mod_MRI_WO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_MRI_W"))) ;
    if ( !NIM3_PayerMaster.getPrice_Mod_MRI_W().equals(testObj)  )
    {
         NIM3_PayerMaster.setPrice_Mod_MRI_W( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Price_Mod_MRI_W not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_MRI_WWO"))) ;
    if ( !NIM3_PayerMaster.getPrice_Mod_MRI_WWO().equals(testObj)  )
    {
         NIM3_PayerMaster.setPrice_Mod_MRI_WWO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Price_Mod_MRI_WWO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_CT_WO"))) ;
    if ( !NIM3_PayerMaster.getPrice_Mod_CT_WO().equals(testObj)  )
    {
         NIM3_PayerMaster.setPrice_Mod_CT_WO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Price_Mod_CT_WO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_CT_W"))) ;
    if ( !NIM3_PayerMaster.getPrice_Mod_CT_W().equals(testObj)  )
    {
         NIM3_PayerMaster.setPrice_Mod_CT_W( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Price_Mod_CT_W not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Price_Mod_CT_WWO"))) ;
    if ( !NIM3_PayerMaster.getPrice_Mod_CT_WWO().equals(testObj)  )
    {
         NIM3_PayerMaster.setPrice_Mod_CT_WWO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Price_Mod_CT_WWO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("BillPrice_Mod_MRI_WO"))) ;
    if ( !NIM3_PayerMaster.getBillPrice_Mod_MRI_WO().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillPrice_Mod_MRI_WO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillPrice_Mod_MRI_WO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("BillPrice_Mod_MRI_W"))) ;
    if ( !NIM3_PayerMaster.getBillPrice_Mod_MRI_W().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillPrice_Mod_MRI_W( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillPrice_Mod_MRI_W not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("BillPrice_Mod_MRI_WWO"))) ;
    if ( !NIM3_PayerMaster.getBillPrice_Mod_MRI_WWO().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillPrice_Mod_MRI_WWO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillPrice_Mod_MRI_WWO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("BillPrice_Mod_CT_WO"))) ;
    if ( !NIM3_PayerMaster.getBillPrice_Mod_CT_WO().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillPrice_Mod_CT_WO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillPrice_Mod_CT_WO not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("BillPrice_Mod_CT_W"))) ;
    if ( !NIM3_PayerMaster.getBillPrice_Mod_CT_W().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillPrice_Mod_CT_W( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillPrice_Mod_CT_W not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("BillPrice_Mod_CT_WWO"))) ;
    if ( !NIM3_PayerMaster.getBillPrice_Mod_CT_WWO().equals(testObj)  )
    {
         NIM3_PayerMaster.setBillPrice_Mod_CT_WWO( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster BillPrice_Mod_CT_WWO not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("FeeScheduleRefID")) ;
    if ( !NIM3_PayerMaster.getFeeScheduleRefID().equals(testObj)  )
    {
         NIM3_PayerMaster.setFeeScheduleRefID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster FeeScheduleRefID not set. this is ok-not an error");
}


try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("FeePercentageBill"))) ;
    if ( !NIM3_PayerMaster.getFeePercentageBill().equals(testObj)  )
    {
         NIM3_PayerMaster.setFeePercentageBill( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster FeePercentageBill not set. this is ok-not an error");
}


try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("FeePercentage2Bill"))) ;
    if ( !NIM3_PayerMaster.getFeePercentage2Bill().equals(testObj)  )
    {
         NIM3_PayerMaster.setFeePercentage2Bill( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster FeePercentage2Bill not set. this is ok-not an error");
}



try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("FeePercentage"))) ;
    if ( !NIM3_PayerMaster.getFeePercentage().equals(testObj)  )
    {
         NIM3_PayerMaster.setFeePercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster FeePercentage not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("FeeSchedule2RefID")) ;
    if ( !NIM3_PayerMaster.getFeeSchedule2RefID().equals(testObj)  )
    {
         NIM3_PayerMaster.setFeeSchedule2RefID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster FeeSchedule2RefID not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("FeePercentage2"))) ;
    if ( !NIM3_PayerMaster.getFeePercentage2().equals(testObj)  )
    {
         NIM3_PayerMaster.setFeePercentage2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster FeePercentage2 not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("AcceptsEMGCaseRate")) ;
    if ( !NIM3_PayerMaster.getAcceptsEMGCaseRate().equals(testObj)  )
    {
         NIM3_PayerMaster.setAcceptsEMGCaseRate( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster AcceptsEMGCaseRate not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("OfficeAddress1")) ;
    if ( !NIM3_PayerMaster.getOfficeAddress1().equals(testObj)  )
    {
         NIM3_PayerMaster.setOfficeAddress1( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster OfficeAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeAddress2")) ;
    if ( !NIM3_PayerMaster.getOfficeAddress2().equals(testObj)  )
    {
         NIM3_PayerMaster.setOfficeAddress2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster OfficeAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeCity")) ;
    if ( !NIM3_PayerMaster.getOfficeCity().equals(testObj)  )
    {
         NIM3_PayerMaster.setOfficeCity( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster OfficeCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("OfficeStateID")) ;
    if ( !NIM3_PayerMaster.getOfficeStateID().equals(testObj)  )
    {
         NIM3_PayerMaster.setOfficeStateID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster OfficeStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeZIP")) ;
    if ( !NIM3_PayerMaster.getOfficeZIP().equals(testObj)  )
    {
         NIM3_PayerMaster.setOfficeZIP( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster OfficeZIP not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficePhone")) ;
    if ( !NIM3_PayerMaster.getOfficePhone().equals(testObj)  )
    {
         NIM3_PayerMaster.setOfficePhone( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster OfficePhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeFax")) ;
    if ( !NIM3_PayerMaster.getOfficeFax().equals(testObj)  )
    {
         NIM3_PayerMaster.setOfficeFax( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster OfficeFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("OfficeEmail")) ;
    if ( !NIM3_PayerMaster.getOfficeEmail().equals(testObj)  )
    {
         NIM3_PayerMaster.setOfficeEmail( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster OfficeEmail not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ParentPayerID")) ;
    if ( !NIM3_PayerMaster.getParentPayerID().equals(testObj)  )
    {
         NIM3_PayerMaster.setParentPayerID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster ParentPayerID not set. this is ok-not an error");
}
try
{
    String testObj = new String(request.getParameter("SalesDivision")) ;
    if ( !NIM3_PayerMaster.getSalesDivision().equals(testObj)  )
    {
         NIM3_PayerMaster.setSalesDivision( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster SalesDivision not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AcquisitionDivision")) ;
    if ( !NIM3_PayerMaster.getAcquisitionDivision().equals(testObj)  )
    {
         NIM3_PayerMaster.setAcquisitionDivision( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster AcquisitionDivision not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("ImportantNotes")) ;
    if ( !NIM3_PayerMaster.getImportantNotes().equals(testObj)  )
    {
         NIM3_PayerMaster.setImportantNotes( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster ImportantNotes not set. this is ok-not an error");
}
try
{
    Integer testObj = new Integer(request.getParameter("Contract1_FeeScheduleID")) ;
    if ( !NIM3_PayerMaster.getContract1_FeeScheduleID().equals(testObj)  )
    {
         NIM3_PayerMaster.setContract1_FeeScheduleID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Contract1_FeeScheduleID not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Contract1_FeePercentage"))) ;
    if ( !NIM3_PayerMaster.getContract1_FeePercentage().equals(testObj)  )
    {
         NIM3_PayerMaster.setContract1_FeePercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Contract1_FeePercentage not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Contract2_FeeScheduleID")) ;
    if ( !NIM3_PayerMaster.getContract2_FeeScheduleID().equals(testObj)  )
    {
         NIM3_PayerMaster.setContract2_FeeScheduleID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Contract2_FeeScheduleID not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Contract2_FeePercentage"))) ;
    if ( !NIM3_PayerMaster.getContract2_FeePercentage().equals(testObj)  )
    {
         NIM3_PayerMaster.setContract2_FeePercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Contract2_FeePercentage not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Contract3_FeeScheduleID")) ;
    if ( !NIM3_PayerMaster.getContract3_FeeScheduleID().equals(testObj)  )
    {
         NIM3_PayerMaster.setContract3_FeeScheduleID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Contract3_FeeScheduleID not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Contract3_FeePercentage"))) ;
    if ( !NIM3_PayerMaster.getContract3_FeePercentage().equals(testObj)  )
    {
         NIM3_PayerMaster.setContract3_FeePercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Contract3_FeePercentage not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Bill1_FeeScheduleID")) ;
    if ( !NIM3_PayerMaster.getBill1_FeeScheduleID().equals(testObj)  )
    {
         NIM3_PayerMaster.setBill1_FeeScheduleID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Bill1_FeeScheduleID not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Bill1_FeePercentage"))) ;
    if ( !NIM3_PayerMaster.getBill1_FeePercentage().equals(testObj)  )
    {
         NIM3_PayerMaster.setBill1_FeePercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Bill1_FeePercentage not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Bill2_FeeScheduleID")) ;
    if ( !NIM3_PayerMaster.getBill2_FeeScheduleID().equals(testObj)  )
    {
         NIM3_PayerMaster.setBill2_FeeScheduleID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Bill2_FeeScheduleID not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Bill2_FeePercentage"))) ;
    if ( !NIM3_PayerMaster.getBill2_FeePercentage().equals(testObj)  )
    {
         NIM3_PayerMaster.setBill2_FeePercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Bill2_FeePercentage not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Bill3_FeeScheduleID")) ;
    if ( !NIM3_PayerMaster.getBill3_FeeScheduleID().equals(testObj)  )
    {
         NIM3_PayerMaster.setBill3_FeeScheduleID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Bill3_FeeScheduleID not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Bill3_FeePercentage"))) ;
    if ( !NIM3_PayerMaster.getBill3_FeePercentage().equals(testObj)  )
    {
         NIM3_PayerMaster.setBill3_FeePercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Bill3_FeePercentage not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ImportantNotes_Alert")) ;
    if ( !NIM3_PayerMaster.getImportantNotes_Alert().equals(testObj)  )
    {
         NIM3_PayerMaster.setImportantNotes_Alert( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster ImportantNotes_Alert not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("EmailAlertNotes_Alert")) ;
    if ( !NIM3_PayerMaster.getEmailAlertNotes_Alert().equals(testObj)  )
    {
         NIM3_PayerMaster.setEmailAlertNotes_Alert( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster EmailAlertNotes_Alert not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("RequiresOnlineImage")) ;
    if ( !NIM3_PayerMaster.getRequiresOnlineImage().equals(testObj)  )
    {
         NIM3_PayerMaster.setRequiresOnlineImage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster RequiresOnlineImage not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("RequiresAging")) ;
    if ( !NIM3_PayerMaster.getRequiresAging().equals(testObj)  )
    {
         NIM3_PayerMaster.setRequiresAging( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster RequiresAging not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Sales_Monthly_Target")) ;
    if ( !NIM3_PayerMaster.getSales_Monthly_Target().equals(testObj)  )
    {
         NIM3_PayerMaster.setSales_Monthly_Target( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Sales_Monthly_Target not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("AcceptsTier2")) ;
    if ( !NIM3_PayerMaster.getAcceptsTier2().equals(testObj)  )
    {
         NIM3_PayerMaster.setAcceptsTier2( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster AcceptsTier2 not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AcceptsTier3")) ;
    if ( !NIM3_PayerMaster.getAcceptsTier3().equals(testObj)  )
    {
         NIM3_PayerMaster.setAcceptsTier3( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster AcceptsTier3 not set. this is ok-not an error");
}


try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Tier2_MinSavings"))) ;
    if ( !NIM3_PayerMaster.getTier2_MinSavings().equals(testObj)  )
    {
         NIM3_PayerMaster.setTier2_MinSavings( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Tier2_MinSavings not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Tier2_TargetSavings"))) ;
    if ( !NIM3_PayerMaster.getTier2_TargetSavings().equals(testObj)  )
    {
         NIM3_PayerMaster.setTier2_TargetSavings( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Tier2_TargetSavings not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("Tier3_Fee"))) ;
    if ( !NIM3_PayerMaster.getTier3_Fee().equals(testObj)  )
    {
         NIM3_PayerMaster.setTier3_Fee( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster Tier3_Fee not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("IsNID")) ;
    if ( !NIM3_PayerMaster.getIsNID().equals(testObj)  )
    {
         NIM3_PayerMaster.setIsNID( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster IsNID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NID_WebCode")) ;
    if ( !NIM3_PayerMaster.getNID_WebCode().equals(testObj)  )
    {
         NIM3_PayerMaster.setNID_WebCode( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster NID_WebCode not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("NID_ShowLogo")) ;
    if ( !NIM3_PayerMaster.getNID_ShowLogo().equals(testObj)  )
    {
         NIM3_PayerMaster.setNID_ShowLogo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster NID_ShowLogo not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NID_WebLogo")) ;
    if ( !NIM3_PayerMaster.getNID_WebLogo().equals(testObj)  )
    {
         NIM3_PayerMaster.setNID_WebLogo( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster NID_WebLogo not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("NID_ReferralSource")) ;
    if ( !NIM3_PayerMaster.getNID_ReferralSource().equals(testObj)  )
    {
         NIM3_PayerMaster.setNID_ReferralSource( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster NID_ReferralSource not set. this is ok-not an error");
}


try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("CommissionHighPercentageSelf"))) ;
    if ( !NIM3_PayerMaster.getCommissionHighPercentageSelf().equals(testObj)  )
    {
         NIM3_PayerMaster.setCommissionHighPercentageSelf( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster CommissionHighPercentageSelf not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("CommissionLowPercentageSelf"))) ;
    if ( !NIM3_PayerMaster.getCommissionLowPercentageSelf().equals(testObj)  )
    {
         NIM3_PayerMaster.setCommissionLowPercentageSelf( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster CommissionLowPercentageSelf not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("CommissionHighPercentageParent"))) ;
    if ( !NIM3_PayerMaster.getCommissionHighPercentageParent().equals(testObj)  )
    {
         NIM3_PayerMaster.setCommissionHighPercentageParent( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster CommissionHighPercentageParent not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("CommissionLowPercentageParent"))) ;
    if ( !NIM3_PayerMaster.getCommissionLowPercentageParent().equals(testObj)  )
    {
         NIM3_PayerMaster.setCommissionLowPercentageParent( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster CommissionLowPercentageParent not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CommissionNotes")) ;
    if ( !NIM3_PayerMaster.getCommissionNotes().equals(testObj)  )
    {
         NIM3_PayerMaster.setCommissionNotes( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster CommissionNotes not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("DiscountHighAmount"))) ;
    if ( !NIM3_PayerMaster.getDiscountHighAmount().equals(testObj)  )
    {
         NIM3_PayerMaster.setDiscountHighAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster DiscountHighAmount not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("DiscountHighPercentage"))) ;
    if ( !NIM3_PayerMaster.getDiscountHighPercentage().equals(testObj)  )
    {
         NIM3_PayerMaster.setDiscountHighPercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster DiscountHighPercentage not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("DiscountLowAmount"))) ;
    if ( !NIM3_PayerMaster.getDiscountLowAmount().equals(testObj)  )
    {
         NIM3_PayerMaster.setDiscountLowAmount( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster DiscountLowAmount not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("DiscountLowPercentage"))) ;
    if ( !NIM3_PayerMaster.getDiscountLowPercentage().equals(testObj)  )
    {
         NIM3_PayerMaster.setDiscountLowPercentage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster DiscountLowPercentage not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CustomerDisplayName")) ;
    if ( !NIM3_PayerMaster.getCustomerDisplayName().equals(testObj)  )
    {
         NIM3_PayerMaster.setCustomerDisplayName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster CustomerDisplayName not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("NID_ShowCode")) ;
    if ( !NIM3_PayerMaster.getNID_ShowCode().equals(testObj)  )
    {
         NIM3_PayerMaster.setNID_ShowCode( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster NID_ShowCode not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NID_CodeDisplayName")) ;
    if ( !NIM3_PayerMaster.getNID_CodeDisplayName().equals(testObj)  )
    {
         NIM3_PayerMaster.setNID_CodeDisplayName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster NID_CodeDisplayName not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("NID_ShowSource")) ;
    if ( !NIM3_PayerMaster.getNID_ShowSource().equals(testObj)  )
    {
         NIM3_PayerMaster.setNID_ShowSource( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster NID_ShowSource not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NID_SourceDisplayName")) ;
    if ( !NIM3_PayerMaster.getNID_SourceDisplayName().equals(testObj)  )
    {
         NIM3_PayerMaster.setNID_SourceDisplayName( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster NID_SourceDisplayName not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("NID_ShowBanner")) ;
    if ( !NIM3_PayerMaster.getNID_ShowBanner().equals(testObj)  )
    {
         NIM3_PayerMaster.setNID_ShowBanner( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster NID_ShowBanner not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NID_WelcomeMessage")) ;
    if ( !NIM3_PayerMaster.getNID_WelcomeMessage().equals(testObj)  )
    {
         NIM3_PayerMaster.setNID_WelcomeMessage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster NID_WelcomeMessage not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("NID_DiscountPercentageMessage")) ;
    if ( !NIM3_PayerMaster.getNID_DiscountPercentageMessage().equals(testObj)  )
    {
         NIM3_PayerMaster.setNID_DiscountPercentageMessage( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster NID_DiscountPercentageMessage not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("InsideSalesDivision")) ;
    if ( !NIM3_PayerMaster.getInsideSalesDivision().equals(testObj)  )
    {
         NIM3_PayerMaster.setInsideSalesDivision( testObj,UserSecurityGroupID   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_PayerMaster InsideSalesDivision not set. this is ok-not an error");
}



boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (testChangeID.equalsIgnoreCase("1"))
{

	NIM3_PayerMaster.setUniqueModifyDate(PLCUtils.getNowDate(false));
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    NIM3_PayerMaster.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
	        NIM3_PayerMaster.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }
}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (false&&pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
if (isNew)
{
	nextPage = "tNIM3_PayerMaster_form.jsp?EDIT=edit&EDITID=" + NIM3_PayerMaster.getUniqueID();	
}
if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
	      alert("Your information has been saved.");
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
  %>
