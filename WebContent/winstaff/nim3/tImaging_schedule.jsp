<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="java.text.DecimalFormat,java.util.* ,com.winstaff.*" %>
<%/*

    filename: out\jsp\NIM3_Encounter_form.jsp
    Created on May/28/2008
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
	tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear_fade.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>

<%@ include file="../generic/generalDisplay.jsp" %>

<script language="javascript">
show_pws_Loading();
</script>




<%
//initial declaration of list class and parentID
	String searchNotes = "";
    Integer        iEncounterID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
	  java.text.DecimalFormat myDF = new java.text.DecimalFormat("0.0");
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
      java.text.SimpleDateFormat displayDateTimeOnlySDF = new java.text.SimpleDateFormat("hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","NIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp");
    pageControllerHash.put("sParentReturnPage","NIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp");

//initial declaration of list class and parentID

	NIM3_EncounterObject2 myEO2 = null;

    if ( request.getParameter( "EDIT" )==null||request.getParameter( "EDIT" ).equalsIgnoreCase("edit")||request.getParameter( "EDIT" ).equalsIgnoreCase("sched") )
    {
        myEO2        =    new    NIM3_EncounterObject2(iEncounterID,"Load Scheduling");
    }
	boolean useNetDevCenter = true;
	if (myEO2.getNIM3_Encounter().getSeeNetDev_SelectedPracticeID()==null||myEO2.getNIM3_Encounter().getSeeNetDev_SelectedPracticeID()==0)
	{
		useNetDevCenter = false;
	}

	boolean isPreSchedule = false;
	boolean isNetDev = false;
        String sPreSchedule        =      "n";
    if ( request.getParameter( "PreSchedule" )!=null)
    {
        sPreSchedule        =      request.getParameter("PreSchedule");
		if (sPreSchedule.equalsIgnoreCase("y"))
		{
			isPreSchedule = true;
		}
		else if (sPreSchedule.equalsIgnoreCase("nd"))
		{
			isPreSchedule = true;
			isNetDev = true;
		}
    }


String phoneTemp = myEO2.getNIM3_CaseAccount().getPatientCellPhone().replaceAll("\\p{Punct}|\\s","");
if (phoneTemp.equalsIgnoreCase(""))
{
	phoneTemp = myEO2.getNIM3_CaseAccount().getPatientHomePhone().replaceAll("\\p{Punct}|\\s","");
}
if (!phoneTemp.equalsIgnoreCase("") && phoneTemp.length()>6)
{
	try
	{
	//	phoneTemp = phoneTemp.substring(0,3) + "-" + phoneTemp.substring(3,3) + "-" + phoneTemp.substring(5,4);
		phoneTemp = phoneTemp.substring(0,3) + "-" + phoneTemp.substring(3,6) + "-" + phoneTemp.substring(6,10);
	}
	catch (Exception eeeeee) 
	{
		out.println("phone: [" + eeeeee + "]");
	}
}
int iRange = 30;
if (request.getParameter("ssiRange")!=null)
{
	iRange = new Integer(request.getParameter("ssiRange")).intValue();
}
String ssZIP = myEO2.getNIM3_CaseAccount().getPatientZIP();
if (request.getParameter("ssZIP")!=null)
{
	ssZIP = request.getParameter("ssZIP");
}

String ssAddress1 = myEO2.getNIM3_CaseAccount().getPatientAddress1();
if (request.getParameter("ssAddress1")!=null)
{
	ssAddress1 = request.getParameter("ssAddress1");
}

String ssCity = myEO2.getNIM3_CaseAccount().getPatientCity();
if (request.getParameter("ssCity")!=null)
{
	ssCity = request.getParameter("ssCity");
}

String ssState = new bltStateLI(myEO2.getNIM3_CaseAccount().getPatientStateID()).getShortState();
if (request.getParameter("ssState")!=null)
{
	ssState = request.getParameter("ssState");
}



//fields
        %>
          <%  String theClass ="tdBase";%>
          <link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />
          
          <script type="text/javascript">
<!--
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    	
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);	
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    //document.MM_returnValue = (errors == '');
} }
//-->
          </script>
<%
{
	com.winstaff.CPTGroupObject2 myCPTO2_full =  NIMUtils.getCPTGroupObject2(iEncounterID);
	com.winstaff.CPTGroupObject2 myCPTO2_dom = NIMUtils.getDominantCode(myCPTO2_full);
	//com.winstaff.CPTGroupObject2 myCPTO2_dom = myCPTO2_full;
	String temp_has_mr = "tdBaseFade1";
	String temp_has_ct = "tdBaseFade1";
	String temp_has_emg = "tdBaseFade1";
	String temp_has_fl = "tdBaseFade1";
	String temp_has_us = "tdBaseFade1";
	String temp_has_nm = "tdBaseFade1";
	String temp_has_xr = "tdBaseFade1";
	String temp_has_pet = "tdBaseFade1";

	String temp_needs_mr = "tdBaseFade1";
	String temp_needs_ct = "tdBaseFade1";
	String temp_needs_emg = "tdBaseFade1";
	String temp_needs_fl = "tdBaseFade1";
	String temp_needs_us = "tdBaseFade1";
	String temp_needs_nm = "tdBaseFade1";
	String temp_needs_xr = "tdBaseFade1";
	String temp_needs_pet = "tdBaseFade1";


	if (myCPTO2_dom.isHasMRI())
	{
		temp_has_mr = "tdBaseAlt_Create";
	}
	if (myCPTO2_dom.isHasCT())
	{
		temp_has_ct = "tdBaseAlt_Create";
	}
	if (myCPTO2_dom.isHasEMG())
	{
		temp_has_emg = "tdBaseAlt_Create";
	}
	if (myCPTO2_dom.isHasFL())
	{
		temp_has_fl = "tdBaseAlt_Create";
	}
	if (myCPTO2_dom.isHasUS())
	{
		temp_has_us = "tdBaseAlt_Create";
	}
	if (myCPTO2_dom.isHasNM())
	{
		temp_has_nm = "tdBaseAlt_Create";
	}
	if (myCPTO2_dom.isHasXR())
	{
		temp_has_xr = "tdBaseAlt_Create";
	}
	if (myCPTO2_dom.isHasPET())
	{
		temp_has_pet = "tdBaseAlt_Create";
	}




	if (myCPTO2_full.isHasMRI())
	{
		temp_needs_mr = "tdBaseAlt_Create";
	}
	if (myCPTO2_full.isHasCT())
	{
		temp_needs_ct = "tdBaseAlt_Create";
	}
	if (myCPTO2_full.isHasEMG())
	{
		temp_needs_emg = "tdBaseAlt_Create";
	}
	if (myCPTO2_full.isHasFL())
	{
		temp_needs_fl = "tdBaseAlt_Create";
	}
	if (myCPTO2_full.isHasUS())
	{
		temp_needs_us = "tdBaseAlt_Create";
	}
	if (myCPTO2_full.isHasNM())
	{
		temp_needs_nm = "tdBaseAlt_Create";
	}
	if (myCPTO2_full.isHasXR())
	{
		temp_needs_xr = "tdBaseAlt_Create";
	}
	if (myCPTO2_full.isHasPET())
	{
		temp_needs_pet = "tdBaseAlt_Create";
	}

	Double myPayer_Cost = null;
	Double defaultMaxPrice = 9000.00;
	Double myMaxMACPrice = 800.00;
	if (myEO2.isNextImageDirectCase()){
		myPayer_Cost = defaultMaxPrice;
	} else {
		myPayer_Cost = NIMUtils.getCPTGroupPayPriceforPayer(myEO2.getNIM3_PayerMaster(),ssZIP,myCPTO2_full,PLCUtils.getToday());
		out.println("<!-- payer master id: "+myEO2.getNIM3_PayerMaster().getPayerID()+" zip: "+ssZIP+"  eff: "+PLCUtils.getToday()+" -->");
		myMaxMACPrice = myPayer_Cost;
	}
	
	String d_MyPayer_Cost ="";
	if (myPayer_Cost==null)
	{
		d_MyPayer_Cost = "Invalid Payer Cost";
		//myPayer_Cost = 0.0;
	}
	else 
	{
		if (myEO2.isNextImageDirectCase()){
			d_MyPayer_Cost = "NextImage Direct Case";
		} else {
			d_MyPayer_Cost = "$"+PLCUtils.getDisplayDefaultDecimalFormatCurrency(myPayer_Cost);
		}
	}

%>
          


          <table cellpadding=0 cellspacing=0 border=0 width="99%" >
    <tr class=title><td width=10>&nbsp;</td><td class="title">Schedule </td></tr>
    <tr><td width=10>&nbsp;</td><td>
        <table width=100% border=0 bordercolor=#333333 cellpadding=5 cellspacing=0 >
         <tr>
           <td colspan="4" class="title" ><% if (!isNetDev){%><input type="button" class="inputButton_md_Stop" onclick="this.disabled=true;window.close();" value="Cancel Schedule" />&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" class="inputButton_md_Default" onclick="window.print();" value="Print" /> &nbsp;&nbsp;&nbsp;&nbsp;<input name="" class="inputButton_md_Action1" onclick="this.disable;document.location='tNIM3_Encounter_SeeND_report.jsp?EDIT=0';" value="See NetDev" type="button" /><% }%>
</td>
         </tr>
         <tr>
           <td colspan="3" class=titleSub1 >
<%
if (!useNetDevCenter)
{
%>
           Available centers near Patient Address:<%if (isPreSchedule){%>&nbsp;&nbsp;&nbsp;[Note: This is a Pre-Schedule Search]<%}%><br />
<span class="borderHighlightGreen"><%=myEO2.getNIM3_CaseAccount().getPatientAddress1()%></span> &nbsp;&nbsp; <span class="borderHighlightGreen"><%=myEO2.getNIM3_CaseAccount().getPatientCity()%>, <%=new bltStateLI(myEO2.getNIM3_CaseAccount().getPatientStateID()).getShortState()%> <%=myEO2.getNIM3_CaseAccount().getPatientZIP()%></span>

<%
} else {
%>
             Using Assigned Center from NetDev

<%
}
%>


</td>
           <td rowspan="2" valign="top" class=title ><table width="100%" border="1" cellspacing="0" cellpadding="2">
             <tr class="inputButton_md_Default">
               <td class="tdHeaderAlt">Additional Resources</td>
              </tr>
             <tr>
               <td><a href="http://www.zipmap.net/" target="_blank">ZIP Code Maps</a></td>
              </tr>
           </table></td>
         </tr>
         <tr>
             <td>
			 	<%java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("MM-dd-yyy");%>
             	<span style="background:#00ff2a;"><strong>ScanPass:</strong> <%=myEO2.getNIM3_Encounter().getScanPass()%></span> <br>
				<strong>DOI:</strong> 
				<%
				try{
					String temp = df.format(myEO2.getNIM3_CaseAccount().getDateOfInjury()).toString(); 
					if(!temp.equals("01-01-00")){
						out.print( df.format(myEO2.getNIM3_CaseAccount().getDateOfInjury()) );
					}
					
				}catch(Exception e){
					out.print("Blank");
				}
				%>
             </td>
         </tr>
         <tr>
           <td ><form id="form1" name="form1" method="post" action="tImaging_schedule.jsp">
             Alternate address for distance calculation:<br />
                 <input name="ssAddress1" value="<%=ssAddress1%>" type="text" class="tdBase" id="address1" />&nbsp;<input name="ssCity" value="<%=ssCity%>" type="text" class="tdBase" id="city" size="10" /> &nbsp;<input name="ssState" type="text" class="tdBase" value="<%=ssState%>" id="address1" size="2" maxlength="2" />
                 <br />
                 <br />

Search Area: <input name="ssZIP" type="text"  class="inputHi" id="ssZIP" value="<%=ssZIP%>" size="5" maxlength="5"/>		
<label>
  <select name="ssiRange" class="inputHi" id="select"  >
    <option <% if (iRange==1){out.print(" selected ");}%> value="1">1 mile</option>
    <option  <% if (iRange==5){out.print(" selected ");}%> value="5">5 miles</option>
    <option  <% if (iRange==10){out.print(" selected ");}%> value="10">10 miles</option>
    <option  <% if (iRange==15){out.print(" selected ");}%> value="15">15 miles</option>
    <option  <% if (iRange==20){out.print(" selected ");}%> value="20">20 miles</option>
    <option  <% if (iRange==25){out.print(" selected ");}%> value="25">25 miles</option>
    <option  <% if (iRange==30){out.print(" selected ");}%> value="30">30 miles</option>
    <option  <% if (iRange==45){out.print(" selected ");}%> value="45">45 miles</option>
    <option  <% if (iRange==60){out.print(" selected ");}%> value="60">60 miles</option>
    <option  <% if (iRange==80){out.print(" selected ");}%> value="80">80 miles</option>
    <option  <% if (iRange==100){out.print(" selected ");}%> value="100">100 miles</option>
  </select>
</label>
&nbsp;               
<input name="Search" type="submit" class="inputButton_md_Create" id="Search" onclick="show_pws_Loading();MM_validateForm('ssZIP','','NisNum');return document.MM_returnValue" value="Search" />
               <input type="hidden" name="EDIT" value="sched" />
               <input type="hidden" name="ssiRange" value="<%=iRange%>" />
               <input type="hidden" name="PreSchedule" value="<%=sPreSchedule%>" />
             </p>
           </form>
           
           </td>
           <td align="center" valign="middle" >
           <span class="tdHeader">Looking for Centers that Match: </span>
             <table border="0" cellspacing="5" cellpadding="5">
             <tr>
               <td class="<%=temp_has_mr%>">MR</td>
               <td class="<%=temp_has_ct%>">CT</td>
               <td class="<%=temp_has_emg%>">EMG</td>
               <td class="<%=temp_has_fl%>">FL</td>
               <td class="<%=temp_has_us%>">US</td>
               <td class="<%=temp_has_nm%>">NM</td>
               <td class="<%=temp_has_xr%>">XR</td>
               <td class="<%=temp_has_pet%>">PET</td>
             </tr>
           </table>
           <span class="tdHeader">Patient Encounter Needs: </span>
             <table border="0" cellspacing="5" cellpadding="5">
             <tr>
               <td class="<%=temp_needs_mr%>">MR</td>
               <td class="<%=temp_needs_ct%>">CT</td>
               <td class="<%=temp_needs_emg%>">EMG</td>
               <td class="<%=temp_needs_fl%>">FL</td>
               <td class="<%=temp_needs_us%>">US</td>
               <td class="<%=temp_needs_nm%>">NM</td>
               <td class="<%=temp_needs_xr%>">XR</td>
               <td class="<%=temp_needs_pet%>">PET</td>
             </tr>
           </table>
           </td>
           <%
			Double gCostFull = 0d;
	    	String query = "SELECT\n" +
	    			"	sr.cpt, sr.cptmodifier, sr.cptqty\n" +
	    			"FROM\n" +
	    			"	tnim3_encounter en\n" +
	    			"INNER JOIN tnim3_referral rf ON rf.referralid = en.referralid\n" +
	    			"INNER JOIN tnim3_caseaccount myca ON myca.caseid = rf.caseid\n" +
	    			"INNER JOIN tnim3_service sr ON sr.encounterid = en.encounterid\n" +
	    			"WHERE\n" +
	    			"	en.scanpass = '"+ myEO2.getScanPass() +"'\n" +
	    			"AND sr.servicestatusid = 1";
	    	searchDB2 conn = new searchDB2();
	    	db_PreparedStatementObject pso = new db_PreparedStatementObject(query);
	    	java.sql.ResultSet rs = null;
	    	rs = conn.executePreparedStatement(pso);
	    	try{
	    		while(rs.next()){
	    			gCostFull += new Double(PLCUtils.getDisplayDefaultDecimalFormat2(NIMUtils.getFeeSchedulePrice(new Integer(1), new Double(1.00), ssZIP,rs.getString(1),rs.getString(2),myEO2.getNIM3_Encounter().getDateOfService()))) * new Double(rs.getString(3));
	    		}
	    		System.out.println("List: " + gCostFull);
	    	}catch(Exception e){
	    		System.out.println("List Error: " + gCostFull);
	    		e.printStackTrace();
	    	}finally{
	    		conn.closeAll();
	    	}
           %>
           <td align="center" valign="middle" class="tdHeader" >Payer Allow: <%=d_MyPayer_Cost%> State Fee Schedule: <%=gCostFull %></td>
          </tr>
         <tr>
           <td colspan="4" ><table width="100%" border="0" cellspacing="0" cellpadding="10">
<%
	double myDistanceD = -1;
    java.util.Vector<PracticeMaster_SearchResultsObject> vZIP = null;
    java.util.Iterator itZIP = null;
	if ( (myPayer_Cost==null&&(myCPTO2_dom.isHasEMG()||isNetDev)) || myEO2.isNextImageDirectCase())
	{
		myPayer_Cost = defaultMaxPrice;
		myMaxMACPrice = 800.00;
	}
	if (useNetDevCenter)
	{
		vZIP = new java.util.Vector();
		vZIP.add(new PracticeMaster_SearchResultsObject(myEO2.getNIM3_Encounter().getSeeNetDev_SelectedPracticeID()));
		searchNotes = "No search was performed: SeeNetDev_SelectedPracticeID has been entered";
	}
	else if (myPayer_Cost!=null)
	{
		//Double gCostFull = PLCUtils.getDisplayDefaultDecimalFormat2(NIMUtils.getFeeSchedulePrice(new Integer(1), new Double(1.00), tempUseZip,NIM3_Service.getCPT(),NIM3_Service.getCPTModifier(),myEO2.getNIM3_Encounter().getDateOfService()));
		//Double gFull = new Double(PLCUtils.getDisplayDefaultDecimalFormatCurrency(gCostFull));
			
			try
		{
			out.println("myMaxMACPrice "+myMaxMACPrice);
			ImagingCenterListObject myImagingCenterListObject = NIMUtils.getListOfCenters(ssZIP, iRange,myCPTO2_full, new com.winstaff.ContactAddressObject(ssAddress1,ssCity, ssState, ssZIP), myEO2.getNIM3_Encounter().getUniqueID(),myPayer_Cost, isNetDev, 	myMaxMACPrice, false);
			searchNotes = myImagingCenterListObject.getSearchNotes_Text("<hr>");
			vZIP = myImagingCenterListObject.getListOfCenters();
	//		java.util.Collections.sort(vZIP);
	//		itZIP = vZIP.iterator();
		}
		catch (Exception eeee)
		{
			%>
			<h2>No Matching Centers [E2]</h2>
			<%
			//out.print ("Failing Sort:" + eeee.toString());				
		}
	}
	int i2cnt=0;
	if (vZIP==null)
	{
		%>
        <h2>No Matching Centers</h2>
        <%
	}
	else 
	{
    for (int iVect=0; iVect<vZIP.size(); iVect++) 
	{
		i2cnt++;
		PracticeMaster_SearchResultsObject myPracM_SRO = (PracticeMaster_SearchResultsObject) vZIP.get(iVect);
		String sNewDistance = "<20 miles";
		try
		{
			myDistanceD = myPracM_SRO.getDistance();
			sNewDistance = myDF.format(myDistanceD);
			if (myDistanceD==0)
			{
				sNewDistance = "<20 miles";
			}

		}
		catch (Exception eeeeee)
		{
			sNewDistance = "<20 miles";
			myDistanceD = -1;
		}
		

		String theClass_ic = "tdBaseAlt2";
		String theClass_ic2 = "ndBaseAltDefault";
		if (i2cnt%2==0)
		{
			theClass_ic = "tdBase";
		}
			
		  if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==1)
		  {
			  theClass_ic2 = "ndBaseAltNegotiating";
		  }
		  else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==2)
		  {
			  theClass_ic2 = "ndBaseAltContracted";
		  }
		  else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==3)
		  {
			  theClass_ic2 = "ndBaseAltTarget";
		  }
		  else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==4)
		  {
			  theClass_ic2 = "ndBaseAltNoContract";
		  }
		  else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==5)
		  {
			  theClass_ic2 = "ndBaseAltSendContract";
		  }
		  else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==6)
		  {
			  theClass_ic2 = "ndBaseAltTalking";
		  }
		  else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==7)
		  {
			  theClass_ic2 = "ndBaseAltTargetMerge";
		  }
		  else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==8)
		  {
			  theClass_ic2 = "ndBaseAltContractedSelect";
		  }
		  else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==9)
		  {
			  theClass_ic2 = "ndBaseAltContractedStar";
		  }
		  else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==10)
		  {
			  theClass_ic2 = "ndBaseAltContractedStar";
		  }
		  else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==11)
		  {
			  theClass_ic2 = "ndBaseAltContracted";
		  }
			

		String phoneTemp2 = myPracM_SRO.practiceMaster.getOfficePhone().replaceAll("\\p{Punct}|\\s","");
		try
		{
			if (phoneTemp2!=null)
			{
				phoneTemp2 = phoneTemp2.substring(0,3) + "-" + phoneTemp2.substring(3,6) + "-" + phoneTemp2.substring(6,10);
			}
		}
		catch (Exception eeeeee) 
		{

		}

		String faxTemp2 = myPracM_SRO.practiceMaster.getOfficeFaxNo().replaceAll("\\p{Punct}|\\s","");
		try
		{
			if (faxTemp2!=null)
			{
				faxTemp2 = faxTemp2.substring(0,3) + "-" + faxTemp2.substring(3,6) + "-" + faxTemp2.substring(6,10);
			}
		}
		catch (Exception eeeeee) 
		{
			
		}
		
		Double dPrice = NIMUtils.getCPTGroupPayPriceforPracticeMaster(myPracM_SRO.practiceMaster, myCPTO2_full, PLCUtils.getToday());
		Double MacScore = new Double(-1);
		bltNIM3_NIDPromo nid = new bltNIM3_NIDPromo();
		bltNIM3_PayerMaster mypm = new bltNIM3_PayerMaster();
		bltPracticeMaster pm = new bltPracticeMaster();
		String sPrice = "$Unknown";
		if (myDistanceD!=-1&&dPrice!=null)
		{
			MacScore = NIMUtils.getMACScore(myDistanceD,dPrice, myPracM_SRO.practiceMaster, myPayer_Cost,250.0, false);
			if (myEO2.isNextImageDirectCase()){
				dPrice = NIDUtils.getNIDFinalPrice( myPracM_SRO.getCustomerPrice(), pm , mypm, nid  );
			}
			sPrice = PLCUtils.getDisplayDefaultDecimalFormatCurrency(dPrice);
		}
		
		%>
			  <tr class=<%=theClass_ic%>>
				<td>
                  <strong class="title"><%=myPracM_SRO.practiceMaster.getPracticeName()%></strong><br />
                
                
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="borderHighlightBlackDottedNoFont">
                  <tr>
                    <td width="40"> <%
                if (myPracM_SRO.practiceMaster.getIsBlueStar().intValue()==1)
                {
                    %>
                <img src="images/bluestar1.png" height="25"  />
                    <%
                }else{%>&nbsp;<%}%></td>
                    <td width="25%" nowrap="nowrap" class="<%=theClass_ic2%>"><strong><%if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==2){%>Contracted<%}else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==8){%>Contracted [SELECT]<%}else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==11){%>Contracted [IPA]<%}else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==9){%>Contracted [*]<%}else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==10){%>Contracted [V*]<%}else{%>Negotiating<%}%></strong></td>
                    <td width="65%" nowrap="nowrap" class="tdHeader">
<%
if (!useNetDevCenter)
{

	if (myEO2.isNextImageDirectCase()){
		%>
        NextImage Direct 
        <%
	}
%>
         Price: <strong>$<%=sPrice%></strong><br /><span class="tdBase">*% of FS: <%=myPracM_SRO.practiceMaster.getFeePercentage()%> </span>
<%
} else {
%>
           Using Assigned Center from NetDev

<%
}
%>
                    
                    
                    </td>
                    <td width="8%" align="right" nowrap="nowrap">MAC: <strong><%=Math.round(MacScore*100000)%></strong>&nbsp;</td>
                  </tr>
				</table>
				<%          
                {
                    bltNIM3_Modality_List        bltNIM3_Modality_List        =    new    bltNIM3_Modality_List(myPracM_SRO.practiceMaster.getPracticeID());
                
                //declaration of Enumeration
                    bltNIM3_Modality        working_bltNIM3_Modality;
                    ListElement         leCurrentElement;
                    java.util.Enumeration eList = bltNIM3_Modality_List.elements();
                    %>
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td width="60%" valign="top"> 
                <table width="100%" border="1" cellspacing="0" cellpadding="5">
                  <tr>
                    <td> <%if (useNetDevCenter||myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==2||myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==8||myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==10||myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==11){%><span class="titleSub1">Phone:  <strong><%=phoneTemp2%></span></strong><br />
                Fax:  <strong><%=faxTemp2%></strong>
                
                <%if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==10){%>
                <br /><span class="instructionsBig1">Note: This is a V-Star (V*) Center</span>
                <%}%>
                
               
                
                <%}if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==10){%>
                <span class="instructionsBig1"><input name="" class="inputButton_lg_Action1" onclick="this.disable;document.location='tNIM3_Encounter_sendAlert_preview.jsp?EDIT=email_sr_vstar';" value="Send V* Request" type="button" /></span>
                <%}
                
                if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==9){%>
                <br /><span class="instructionsBig1" style="background-color:black;color:red;display:inline-block">Note: Do not select this center</span>
                <%}
                
                else if (myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==2||myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==8||myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==10||myPracM_SRO.practiceMaster.getContractingStatusID().intValue()==11)
                {%>
                <%}else if (isNetDev) {%>
                <span class="instructionsBig1"><input name="" class="inputButton_lg_Action1" onclick="alert('You are NetDev!');" value="See NetDev" type="button" /></span>
                <%} else if (!useNetDevCenter) {%>
                <span class="instructionsBig1"><input name="" class="inputButton_lg_Action1" onclick="this.disable;document.location='tNIM3_Encounter_SeeND_report.jsp?EDIT=<%=myPracM_SRO.practiceMaster.getUniqueID()%>';" value="See NetDev" type="button" /></span>
                <%}%><br />
                                <%=myPracM_SRO.practiceMaster.getOfficeAddress1()%><br />
                                <%=myPracM_SRO.practiceMaster.getOfficeAddress2()%><br />
                            <%=myPracM_SRO.practiceMaster.getOfficeCity()%>, <%=new bltStateLI(myPracM_SRO.practiceMaster.getOfficeStateID()).getShortState()%> <%=myPracM_SRO.practiceMaster.getOfficeZIP()%><br />
                PracticeID: <%=myPracM_SRO.practiceMaster.getPracticeID()%><br />
                <br />
                <a href="http://maps.google.com/maps?q=<%=ssAddress1 + "+" + ssCity + "+" + ssState + "+" + ssZIP+"+to+"%><%=myPracM_SRO.practiceMaster.getOfficeAddress1()+"+"+myPracM_SRO.practiceMaster.getOfficeZIP()%>" target="_blank">View Map</a>
                </td>
                    <td><table border="1" bordercolor="CCCCCC" cellpadding="3" class=tdBase cellspacing="0" width="100%">
                  <%
                    int altCnt = 0;
                    if (eList.hasMoreElements())
                    {
                     while (eList.hasMoreElements())
                     {
                
                        altCnt++;
                        String theClass2 = "tdBase";
                        if (altCnt%2!=0)
                        {
                            theClass2 = "tdBaseAlt";
                        }
                        leCurrentElement    = (ListElement) eList.nextElement();
                        working_bltNIM3_Modality  = (bltNIM3_Modality) leCurrentElement.getObject();
                        working_bltNIM3_Modality.GroupSecurityInit(UserSecurityGroupID);
                        %>
                  <tr class=<%=theClass2%> > 
                
                              <%String theClassF = "textBase";%>
                 <td>
                   <%theClassF = "textBase";%>
                   <%if ((working_bltNIM3_Modality.isExpired("ModalityTypeID",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("ModalityTypeID"))){theClassF = "expiredFieldMain";}%>
                   <%if ( (working_bltNIM3_Modality.isRequired("ModalityTypeID"))&&(!working_bltNIM3_Modality.isComplete("ModalityTypeID")) ){theClassF = "requiredFieldMain";}%>
                   Modality: <strong><jsp:include page="../generic/tModalityTypeLIShort_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Modality.getModalityTypeID()%>" /></jsp:include></strong> <br>
                   
                   <%theClassF = "textBase";%>
                   <%if ((working_bltNIM3_Modality.isExpired("ModalityModelID",expiredDays))&&(working_bltNIM3_Modality.isExpiredCheck("ModalityModelID"))){theClassF = "expiredFieldMain";}%>
                   <%if ( (working_bltNIM3_Modality.isRequired("ModalityModelID"))&&(!working_bltNIM3_Modality.isComplete("ModalityModelID")) ){theClassF = "requiredFieldMain";}%>
                   Model:&nbsp;
                <strong>   <jsp:include page="../generic/tMRI_ModelLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=working_bltNIM3_Modality.getModalityModelID()%>" /></jsp:include></strong> </td>
                 </tr>
                  <%
                    }//end while
                       }//end of if
                  }
                 %>        
                </table>  

                <p><span class="tdHeader">Special Services:</span><br />
                      Arthrograms:<strong>
                  <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myPracM_SRO.practiceMaster.getDiagnosticMDArthrogram()%>" /></jsp:include>
                  </strong>
                  <br />
                      Myelograms:<strong>
                  <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myPracM_SRO.practiceMaster.getDiagnosticMDMyelogram()%>" /></jsp:include>
                  </strong>
                  <br />
                      Aging:<strong>
                  <jsp:include page="../generic/tYesNoLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=myPracM_SRO.practiceMaster.getDoesAging()%>" /></jsp:include>
                  </strong>
                    </p>  
                    
  <%
  if (isNetDev||isScheduler)
  {
	  %>
      Has MR: <%=myPracM_SRO.getModMR()%><br />
      MR Open: <%=myPracM_SRO.getOpenMR()%><br />
      MR Open Class: <%=myPracM_SRO.getOpenMR_Class()%><br />
      MR Trad: <%=myPracM_SRO.getTradMR()%><br />
      MR Trad Class: <%=myPracM_SRO.getTradMR_Class()%><br />

      <%
  }
  
  %>                   
                      </td>
                  </tr>
                </table>


              </td>
            <td width="40%" valign="top" class="tdHeader">Scheduling Notes:<br /><textarea class="borderHighlightBlackDottedNoFont" style="background-color:#EEE" cols="30" rows="9" readonly ><%=myPracM_SRO.practiceMaster.getScheduling_Notes()%></textarea></td>
          </tr>
        </table>



           </td>
				<td >
                
<%
if (!useNetDevCenter)
{
%>
         <hr /><span class="title"><%=sNewDistance%> miles</span> from above address<br /><br />

<%
} else {
%>
          

<%
}
%>
                
                
			    <strong>For distance to patient home address, please click on View Map<br /><br /><a href="http://maps.google.com/maps?q=<%=ssAddress1 + "+" + ssCity + "+" + ssState + "+" + ssZIP+"+to+"%><%=myPracM_SRO.practiceMaster.getOfficeAddress1()+"+"+myPracM_SRO.practiceMaster.getOfficeZIP()%>" target="_blank"><img src="images/icon_viewmap.gif" width="111" height="20" border="0" /></a></strong></td>
				<td><%if (!isPreSchedule){%><span class="tdHeader">Available Times:</span>
				  <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#000033">
                  <tr class=tdHeaderAlt>
                    <td>Day</td>
                    <td>Time</td>
                    <td>Action</td>
                  </tr>
                <%
				if (true)
				{
					searchDB2 mySS_time = new searchDB2();
					java.sql.ResultSet myRS_time = null;
					try
					{
						String mySQL_time = "SELECT * from tnim3_appointment where providerid = " + myPracM_SRO.practiceMaster.getPracticeID() + " AND appointmenttime > current_timestamp and appointmenttime <current_timestamp + INTERVAL '10 days' and iStatus=0 order by appointmenttime";
						//mySQL_time = "SELECT * from tnim_appointmentlu where  appointmenttime > current_timestamp and appointmenttime <current_timestamp + INTERVAL '7 days' and authorizationid=0";
						//out.println(mySQL_time);
						myRS_time = mySS_time.executeStatement(mySQL_time);
						int icnt=0;
						while(myRS_time!=null&&myRS_time.next())
						{
							icnt++;
							//bltNIM2_AppointmentLU myNALU = new bltNIM2_AppointmentLU(new Integer(myRS_time.getString("lookupid")));
							java.util.Date dbAppointment = dbdft.parse(myRS_time.getString("appointmenttime"));
							java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
							java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
							java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
							java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
							java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);

							String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
							String sAppointmentTimeOnly = displayDateTimeOnlySDF.format((dbAppointment));
							String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
							String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
							String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
							String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
							String sAppointmentHour = displayDateHour.format((dbAppointment));
							String theClass_time = "tdBase";
							if (icnt%2==0)
							{
								theClass_time = "tdBaseAlt";
							}
						%>
                          <tr class=<%=theClass_time%>>
                            <td><span class=tdHeader><%=sAppointmentTimeDayWeek%>&nbsp;<br />
                            <%=sAppointmentTimeMonth%>&nbsp;<%=sAppointmentTimeDay%>&nbsp;<%=sAppointmentTimeYear%></span></td>
                            <td nowrap="nowrap"><span class=tdHeader><%=sAppointmentTimeOnly%></span></td>
                            <td><a href="tImaging_schedule_sub.jsp?EDIT=sched&EDITID=<%=myRS_time.getString("appointmentid")%> "><img src="images/icon_schedule-me.gif" width="142" height="16" border="0" /></a></td>
                          </tr>
<%
						}
						if (icnt==0)
						{
						%>
                        <tr><td colspan="3">No Appointments Available</td></tr>
                        <%
						}
						%>
                        <tr><td colspan="3"><a class=linkBase href = "tNIM3_Appointment_main_NIM3_Appointment_ProviderID_form_create_PayerID.jsp?EDITID=<%=myPracM_SRO.practiceMaster.getPracticeID()%>&EDIT=new&KM=p&INTNext=yes">Add Appointment</a>						</td></tr>
                        <%


					}
					catch(Exception e)
					{
							out.println("ResultsSet:"+e);
					}
					finally
					{
							mySS_time.closeAll();
					}
						
				}
				%>
                </table>    <%}else{%>
                &nbsp;
                <%}%>            </td>
			  </tr>
         <%     
		}

	}//end for?
}

%>           
           
</table>
           
<hr />           

 <table width="95%" align="center" border="0" cellspacing="0" cellpadding="5" class="borderHighlightGreen">
  <tr>
    <td class="tdHeaderAlt">Search Notes:</td></tr>
  <tr>
    <td>
<%=searchNotes%>

</td>
  </tr>
</table>

           
           
           
           
           
           </td>
         </tr>
        </table>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


<script language="javascript">
hide_pws_Loading();
</script>
