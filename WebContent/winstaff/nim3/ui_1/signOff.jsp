




<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltPhysicianMaster,com.winstaff.stepVerify_PhysicianID" %>
<%/*
    filename: out\jsp\tPhysicianMaster_form.jsp
    JSP AutoGen on Mar/02/2002
*/%>

<%@ include file="../../generic/CheckLogin.jsp" %>

<%
//String thePLCID = (String)pageControllerHash.get("plcID");
String tnIncludeFN = "top-nav_PhysicianID.jsp?plcID="+thePLCID;
%>
<%@ include file="../../generic/generalDisplay.jsp" %>
  <%
//initial declaration of list class and parentID
    Integer        iPhysicianID        =    null;
    Integer        iUserID        =    null;
    boolean accessValid = true;
    // required for Type2
    String sKeyMasterReference = null;
   if (true)
   {

   if (pageControllerHash.containsKey("iPhysicianID")&&!pageControllerHash.containsKey("iHCOID")) 
   {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
   }
   else
   {
        accessValid = false;    
   }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","pro-file_Status.jsp");
    pageControllerHash.put("sStartPage","pro-file_Attestation.jsp");

    session.setAttribute("pageControllerHash",pageControllerHash);
stepVerify_PhysicianID mySV = new stepVerify_PhysicianID();
mySV.setPhysicianID(iPhysicianID);
bltPhysicianMaster        PhysicianMaster        =   new  bltPhysicianMaster (iPhysicianID);


%>
<script language=javascript>
if (<%=new Boolean((PhysicianMaster.getIsAttested().intValue()!=1&&PhysicianMaster.getIsAttestedPending().intValue()!=1)).booleanValue()%>&&confirm("Practitioner data for <%=PhysicianMaster.getFirstName()%> <%=PhysicianMaster.getLastName()%> has been modified and is not attested. You must re-attest every time data is modified.\n\nDo you want to attest now?\n\nClick OK to go to the Attestation Page\nClick CANCEL to sign-off\n"))
{
document.location="ui_init.jsp";
}
else 
{
document.location="signOff2.jsp";
}
</script>
<%

  }
  else
  {
%>
<script language=javascript>
document.location="signOff2.jsp";
</script>
<%

  }
}
else
{
%>
<script language=javascript>
document.location="signOff2.jsp";
</script>
<%
}
%>

