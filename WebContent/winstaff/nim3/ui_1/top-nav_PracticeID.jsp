


<%@page contentType="text/html" language="java" import="com.winstaff.bltStateLI, com.winstaff.bltPracticeMaster" %>

<%@ include file="../../generic/CheckLogin.jsp" %>

 
<%
    Integer        iPracticeID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iPracticeID")) 
    {
        iPracticeID        =    (Integer)pageControllerHash.get("iPracticeID");
        accessValid = true;
    }
	bltPracticeMaster pm = new bltPracticeMaster (iPracticeID);

%>
<link rel="stylesheet" href="ui_<%=thePLCID%>\style_PracticeID.css" type="text/css">
<script language="JavaScript">
<!--
function fwLoadMenus() {
  if (window.fw_menu_0) return;
  window.fw_menu_0 = new Menu("root",200,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_0.addMenuItem("Practice Status","location='practice_Status.jsp'");
  fw_menu_0.addMenuItem("Populate Forms","location='populateForms_PracticeID.jsp'");
  fw_menu_0.addMenuItem("Return to PRO-FILE Manager Home","location='pro-file-manager_Status.jsp'");
  fw_menu_0.addMenuItem("Sign Off","location='signOff.jsp'");
   fw_menu_0.fontWeight="bold";
   fw_menu_0.hideOnMouseOut=true;
  window.fw_menu_1 = new Menu("root",230,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_1.addMenuItem("1a. General Information","location='tPracticeMaster_form.jsp?EDIT=edit'");
  fw_menu_1.addMenuItem("1b. General Information","location='tPracticeMaster_form2.jsp?EDIT=edit'");
  fw_menu_1.addMenuItem("2. Allied Health Professionals","location='tAlliedHealthProfessional_main_AlliedHealthProfessional_PracticeID.jsp'");
  fw_menu_1.addMenuItem("3. Practitioners at this Practice","location='tPhysicianPracticeLU_main2_LU_PracticeID.jsp'");
  fw_menu_1.addMenuItem("4. Supplemental Forms","location='SupplementalData_main_PracticeID.jsp'");


   fw_menu_1.fontWeight="bold";
   fw_menu_1.hideOnMouseOut=true;
  window.fw_menu_2 = new Menu("root",161,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_2.addMenuItem("General Help","window.open('help_general.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
  fw_menu_2.addMenuItem("Frequently Asked Questions","window.open('help_faqs.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
  fw_menu_2.addMenuItem("Contact Us","window.open('ui_1/contact_us.jsp', '_blank','scrollbars=yes,status=yes, width=400,height=630,resizable=yes');");
  fw_menu_2.addMenuItem("About","window.open('ui_1/about.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
   fw_menu_2.fontWeight="bold";
   fw_menu_2.hideOnMouseOut=true;

  fw_menu_2.writeMenus();
} // fwLoadMenus()

//-->
</script>
<script language="JavaScript1.2" src="ui_1/fw_menu.js"></script>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor=#003333>
  <tr> 
    <td> 
      <script language="JavaScript1.2">fwLoadMenus();</script>
      <table border="0" cellpadding="0" cellspacing="0" width="700">
        <!-- fwtable fwsrc="top-nav2_PracticeID.png" fwbase="top-nav_PracticeID.jpg" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
        <tr> 
          <td><img src="ui_1/images/spacer.gif" width="135" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="92" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="102" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="7" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="59" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="7" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="77" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="216" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="1" border="0"></td>
        </tr>
        <tr> 
          <td colspan="9"><img name="topnav_PracticeID_r1_c1" src="ui_1/images/top-nav_PracticeID_r1_c1.jpg" width="700" height="66" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="66" border="0"></td>
        </tr>
        <tr> 
          <td rowspan="2"><img name="topnav_PracticeID_r2_c1" src="ui_1/images/top-nav_PracticeID_r2_c1.jpg" width="135" height="34" border="0"></td>
          <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_0,135,91);" ><img name="topnav_PracticeID_r2_c2" src="ui_1/images/top-nav_PracticeID_r2_c2.jpg" width="92" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_PracticeID_r2_c3" src="ui_1/images/top-nav_PracticeID_r2_c3.jpg" width="5" height="34" border="0"></td>
          <td><a href="tAdminPhysicianLU_main_PhysicianMaster_AdminID.jsp" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_1,231,91);" ><img name="topnav_PracticeID_r2_c4" src="ui_1/images/top-nav_PracticeID_r2_c4.jpg" width="102" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_PracticeID_r2_c5" src="ui_1/images/top-nav_PracticeID_r2_c5.jpg" width="7" height="34" border="0"></td>
          <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_2,341,91);" ><img name="topnav_PracticeID_r2_c6" src="ui_1/images/top-nav_PracticeID_r2_c6.jpg" width="59" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_PracticeID_r2_c7" src="ui_1/images/top-nav_PracticeID_r2_c7.jpg" width="7" height="34" border="0"></td>
          <td><a href="signOff.jsp"><img name="topnav_PracticeID_r2_c8" src="ui_1/images/top-nav_PracticeID_r2_c8.jpg" width="77" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_PracticeID_r2_c9" src="ui_1/images/top-nav_PracticeID_r2_c9.jpg" width="216" height="34" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="25" border="0"></td>
        </tr>
        <tr> 
          <td><img name="topnav_PracticeID_r3_c2" src="ui_1/images/top-nav_PracticeID_r3_c2.jpg" width="92" height="9" border="0"></td>
          <td><img name="topnav_PracticeID_r3_c4" src="ui_1/images/top-nav_PracticeID_r3_c4.jpg" width="102" height="9" border="0"></td>
          <td><img name="topnav_PracticeID_r3_c6" src="ui_1/images/top-nav_PracticeID_r3_c6.jpg" width="59" height="9" border="0"></td>
          <td><img name="topnav_PracticeID_r3_c8" src="ui_1/images/top-nav_PracticeID_r3_c8.jpg" width="77" height="9" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="9" border="0"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table width="700" border="1" cellspacing="0" cellpadding="0" bordercolor="#999999">
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="1" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
              <tr>
                <td class=tdHeaderAlt nowrap>Practice:</td>
                <td class=tdHeader nowrap width="99%">&nbsp;<%=pm.getPracticeName()%> (<%=pm.getOfficeCity()%>, <%=new bltStateLI(pm.getOfficeStateID()).getLongState()%> <%=pm.getOfficeZIP()%>) [ID:&nbsp;<%=pm.getPracticeID()%>]</td>
                <td class=tdHeader nowrap><a href="practice_Status.jsp">Practice Home</a></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
	</table>
<br>
