

<%@page contentType="text/html" language="java" import="com.winstaff.bltStateLI, com.winstaff.bltPhysicianMaster" %>

<%@ include file="../../generic/CheckLogin.jsp" %>
 
<%
    Integer        iPhysicianID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
	bltPhysicianMaster pm = new bltPhysicianMaster(iPhysicianID);

    String plcID = request.getParameter("plcID");
    if ((plcID==null)||(plcID.equalsIgnoreCase("null")))
    {
        plcID="";
    }
    else
    {
        plcID="_"+plcID;
    }
%>
<link rel="stylesheet" href="ui<%=plcID%>\style_PhysicianID.css" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
 var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
   var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
   if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function fwLoadMenus() {
  if (window.fw_menu_0) return;
  window.fw_menu_0 = new Menu("root",200,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_0.addMenuItem("Pro-File Status","location='pro-file_Status.jsp'");
  fw_menu_0.addMenuItem("HCO Authorization","location='Physician_HCOAuth.jsp'");
  fw_menu_0.addMenuItem("Populate Forms","location='populateForms.jsp'");
  fw_menu_0.addMenuItem("Reports","location='reports_PhysicianID.jsp'");
  fw_menu_0.addMenuItem("Document Management","location='tDocumentManagement_main_DocumentManagement_PhysicianID.jsp'");
<%
if ((CurrentUserAccount.getAccessType().intValue()==3)||(CurrentUserAccount.getAccessType().intValue()==4))
{
%>
  fw_menu_0.addMenuItem("Attestation","location='pro-file_Attestation.jsp'");
<%
}
else
{
System.out.println("CHECKING:"+CurrentUserAccount.getAccessType());
}
%>
  fw_menu_0.addMenuItem("Attestation Accounts","location='tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp'");
<%
if (pageControllerHash.containsKey("iAdminID"))
{
%>
  fw_menu_0.addMenuItem("Return to PRO-FILE Manager Home","location='pro-file-manager_Status.jsp'");
<%
}
else
{
%>
<%
}
%>
  fw_menu_0.addMenuItem("Sign Off","location='signOff.jsp'");
   fw_menu_0.fontWeight="bold";
   fw_menu_0.hideOnMouseOut=true;
  window.fw_menu_1 = new Menu("root",200,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;1. Identifying Information","location='tPhysicianMaster_form.jsp?EDIT=edit'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;2. Practice Information","location='tPhysicianPracticeLU_main_LU_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;3. Licensure/Registration Current","location='tLicenseRegistration_main_LicenseRegistration_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;4. Licensure/Registration Past","location='tLicenseRegistrationPast_main_LicenseRegistrationPast_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;5. Specialty/Certifications","location='tBoardCertification_main_BoardCertification_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;6. Other Certification","location='tOtherCertification_main_OtherCertification_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;7. Education","location='tProfessionalEducation_main_ProfessionalEducation_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;8. Training","location='tExperience_main_Experience_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;9. Work History/Experience","location='tWorkHistory_main_WorkHistory_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("10. Professional Liability Coverage","location='tProfessionalLiability_main_ProfessionalLiability_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("11. Hospital/Facility Affiliations","location='tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("12. Peer References","location='tPeerReference_main_PeerReference_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("13. Professional Associations","location='tProfessionalSociety_main_ProfessionalSociety_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("14. Disclosure Questions","location='tAttestR_form2.jsp'");
  fw_menu_1.addMenuItem("15. Malpractice Claims History","location='tMalpractice_main_Malpractice_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("16. Continuing Education","location='tContinuingEducation_main_ContinuingEducation_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("17. Supplemental Data","location='SupplementalData_main_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("18. Additional Notes","location='tAdditionalInformation_main_AdditionalInformation_PhysicianID.jsp'");
   fw_menu_1.fontWeight="bold";
   fw_menu_1.hideOnMouseOut=true;
  window.fw_menu_2 = new Menu("root",108,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_2.addMenuItem("View/Edit Events","location='events-note.jsp'");
  fw_menu_2.addMenuItem("Event Reports","location='events-note.jsp'");
   fw_menu_2.fontWeight="bold";
   fw_menu_2.hideOnMouseOut=true;
  window.fw_menu_3 = new Menu("root",161,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_3.addMenuItem("General Help","window.open('help_general.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
  fw_menu_3.addMenuItem("Frequently Asked Questions","window.open('help_faqs.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
  fw_menu_3.addMenuItem("Contact Us","window.open('ui_1/contact_us.jsp', '_blank','scrollbars=yes,status=yes, width=400,height=630,resizable=yes');");
  fw_menu_3.addMenuItem("About","window.open('ui_1/about.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
   fw_menu_3.fontWeight="bold";
   fw_menu_3.hideOnMouseOut=true;

  fw_menu_3.writeMenus();
} 

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}
//-->
</script>
<script language="JavaScript1.2" src="ui_1/fw_menu.js"></script>
<body onLoad="MM_preloadImages('ui_1/ui_1/images/top-nav_r2_c2_f2.gif','ui_1/ui_1/images/top-nav_r2_c4_f2.gif','ui_1/ui_1/images/top-nav_r2_c6_f2.gif','ui_1/images/top-nav_r2_c8_f2.gif')" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor=#003366>
  <tr> 
    <td> 
      <script language="JavaScript1.2">fwLoadMenus();</script>
      <table border="0" cellpadding="0" cellspacing="0" width="700">
        <!-- fwtable fwsrc="top-nav2_PhysicianID.png" fwbase="top-nav_PhysicianID.jpg" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
        <tr> 
          <td><img src="ui_1/images/spacer.gif" width="133" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="24" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="24" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="27" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="17" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="2" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="28" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="26" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="26" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="20" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="26" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="26" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="26" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="24" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="2" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="3" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="21" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="24" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="14" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="7" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="26" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="23" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="21" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="4" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="111" height="1" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="1" border="0"></td>
        </tr>
        <tr> 
          <td colspan="28"><img name="topnav_PhysicianID_r1_c1" src="ui_1/images/top-nav_PhysicianID_r1_c1.jpg" width="700" height="60" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="60" border="0"></td>
        </tr>
        <tr> 
          <td rowspan="3"><img name="topnav_PhysicianID_r2_c1" src="ui_1/images/top-nav_PhysicianID_r2_c1.jpg" width="133" height="60" border="0"></td>
          <td colspan="4"><a href="pro-file_Status.jsp" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_0,134,86);" ><img name="topnav_PhysicianID_r2_c2" src="ui_1/images/top-nav_PhysicianID_r2_c2.jpg" width="92" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_PhysicianID_r2_c6" src="ui_1/images/top-nav_PhysicianID_r2_c6.jpg" width="5" height="36" border="0"></td>
          <td colspan="5"><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_1,230,86);" ><img name="topnav_PhysicianID_r2_c7" src="ui_1/images/top-nav_PhysicianID_r2_c7.jpg" width="102" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_PhysicianID_r2_c12" src="ui_1/images/top-nav_PhysicianID_r2_c12.jpg" width="5" height="36" border="0"></td>
          <td colspan="4"><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_2,337,86);" ><img name="topnav_PhysicianID_r2_c13" src="ui_1/images/top-nav_PhysicianID_r2_c13.jpg" width="102" height="25" border="0"></a></td>
          <td rowspan="2" colspan="2"><img name="topnav_PhysicianID_r2_c17" src="ui_1/images/top-nav_PhysicianID_r2_c17.jpg" width="5" height="36" border="0"></td>
          <td colspan="3"><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_3,444,86);" ><img name="topnav_PhysicianID_r2_c19" src="ui_1/images/top-nav_PhysicianID_r2_c19.jpg" width="59" height="25" border="0"></a></td>
          <td rowspan="2"><img name="topnav_PhysicianID_r2_c22" src="ui_1/images/top-nav_PhysicianID_r2_c22.jpg" width="5" height="36" border="0"></td>
          <td colspan="4"><a href="signOff.jsp"><img name="topnav_PhysicianID_r2_c23" src="ui_1/images/top-nav_PhysicianID_r2_c23.jpg" width="77" height="25" border="0"></a></td>
          <td rowspan="2" colspan="2"><img name="topnav_PhysicianID_r2_c27" src="ui_1/images/top-nav_PhysicianID_r2_c27.jpg" width="115" height="36" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="25" border="0"></td>
        </tr>
        <tr> 
          <td colspan="4"><img name="topnav_PhysicianID_r3_c2" src="ui_1/images/top-nav_PhysicianID_r3_c2.jpg" width="92" height="11" border="0"></td>
          <td colspan="5"><img name="topnav_PhysicianID_r3_c7" src="ui_1/images/top-nav_PhysicianID_r3_c7.jpg" width="102" height="11" border="0"></td>
          <td colspan="4"><img name="topnav_PhysicianID_r3_c13" src="ui_1/images/top-nav_PhysicianID_r3_c13.jpg" width="102" height="11" border="0"></td>
          <td colspan="3"><img name="topnav_PhysicianID_r3_c19" src="ui_1/images/top-nav_PhysicianID_r3_c19.jpg" width="59" height="11" border="0"></td>
          <td colspan="4"><img name="topnav_PhysicianID_r3_c23" src="ui_1/images/top-nav_PhysicianID_r3_c23.jpg" width="77" height="11" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="11" border="0"></td>
        </tr>
        <tr> 
          <td><a href="tPhysicianMaster_form.jsp?EDIT=edit"><img name="topnav_PhysicianID_r4_c2" src="ui_1/images/top-nav_PhysicianID_r4_c2.jpg" width="24" height="24" border="0" alt="1. Identifying Information"></a></td>
          <td><a href="tPhysicianPracticeLU_main_LU_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c3" src="ui_1/images/top-nav_PhysicianID_r4_c3.jpg" width="24" height="24" border="0" alt="2. Practice Information"></a></td>
          <td><a href="tLicenseRegistration_main_LicenseRegistration_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c4" src="ui_1/images/top-nav_PhysicianID_r4_c4.jpg" width="27" height="24" border="0" alt="3. Licensure/Registration"></a></td>
          <td colspan="3"><a href="tLicenseRegistrationPast_main_LicenseRegistrationPast_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c5" src="ui_1/images/top-nav_PhysicianID_r4_c5.jpg" width="24" height="24" border="0" alt="4. License/Registration Past"></a></td>
          <td><a href="tBoardCertification_main_BoardCertification_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c8" src="ui_1/images/top-nav_PhysicianID_r4_c8.jpg" width="28" height="24" border="0" alt="5. Specialty/Certifications"></a></td>
          <td><a href="tOtherCertification_main_OtherCertification_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c9" src="ui_1/images/top-nav_PhysicianID_r4_c9.jpg" width="26" height="24" border="0" alt="6. Other Certification"></a></td>
          <td><a href="tProfessionalEducation_main_ProfessionalEducation_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c10" src="ui_1/images/top-nav_PhysicianID_r4_c10.jpg" width="26" height="24" border="0" alt="7.  Education"></a></td>
          <td colspan="2"><a href="tExperience_main_Experience_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c11" src="ui_1/images/top-nav_PhysicianID_r4_c11.jpg" width="25" height="24" border="0" alt="8. Training"></a></td>
          <td><a href="tWorkHistory_main_WorkHistory_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c13" src="ui_1/images/top-nav_PhysicianID_r4_c13.jpg" width="26" height="24" border="0" alt="9. Work History/Experience"></a></td>
          <td><a href="tProfessionalLiability_main_ProfessionalLiability_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c14" src="ui_1/images/top-nav_PhysicianID_r4_c14.jpg" width="26" height="24" border="0" alt="10. Professional Liability Coverage"></a></td>
          <td><a href="tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c15" src="ui_1/images/top-nav_PhysicianID_r4_c15.jpg" width="26" height="24" border="0" alt="11. Hospital/Facility Affiliations"></a></td>
          <td colspan="2"><a href="tPeerReference_main_PeerReference_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c16" src="ui_1/images/top-nav_PhysicianID_r4_c16.jpg" width="26" height="24" border="0" alt="12. Peer References"></a></td>
          <td colspan="2"><a href="tProfessionalSociety_main_ProfessionalSociety_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c18" src="ui_1/images/top-nav_PhysicianID_r4_c18.jpg" width="24" height="24" border="0" alt="13. Professional Associations"></a></td>
          <td><a href="tAttestR_form2.jsp"><img name="topnav_PhysicianID_r4_c20" src="ui_1/images/top-nav_PhysicianID_r4_c20.jpg" width="24" height="24" border="0" alt="14. Disclosure Questions"></a></td>
          <td colspan="3"><a href="tMalpractice_main_Malpractice_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c21" src="ui_1/images/top-nav_PhysicianID_r4_c21.jpg" width="26" height="24" border="0" alt="15. Malpractice Information"></a></td>
          <td><a href="tContinuingEducation_main_ContinuingEducation_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c24" src="ui_1/images/top-nav_PhysicianID_r4_c24.jpg" width="26" height="24" border="0" alt="16. Continuing Education"></a></td>
          <td><a href="SupplementalData_main_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c25" src="ui_1/images/top-nav_PhysicianID_r4_c25.jpg" width="23" height="24" border="0" alt="17. Supplemental Data"></a></td>
          <td colspan="2"><a href="tAdditionalInformation_main_AdditionalInformation_PhysicianID.jsp"><img name="topnav_PhysicianID_r4_c26" src="ui_1/images/top-nav_PhysicianID_r4_c26.jpg" width="25" height="24" border="0" alt="18. Additional Notes"></a></td>
          <td><img name="topnav_PhysicianID_r4_c28" src="ui_1/images/top-nav_PhysicianID_r4_c28.jpg" width="111" height="24" border="0"></td>
          <td><img src="ui_1/images/spacer.gif" width="1" height="24" border="0"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table width="700" border="1" cellspacing="0" cellpadding="0" bordercolor="#999999">
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="1" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
              <tr>
                <td class=tdHeaderAlt nowrap>Practitioner:</td>
                <td class=tdHeader nowrap width="99%">&nbsp;<%=pm.getLastName()%>, 
                  <%=pm.getFirstName()%></td>
                <td class=tdHeader nowrap>ID:&nbsp;<%=pm.getPhysicianID()%></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
