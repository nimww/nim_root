<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltAdminMaster,com.winstaff.bltCompanyAdminLU,com.winstaff.bltCompanyUserAccountLU,com.winstaff.bltCompanyAdminLU_List_LU_CompanyID,com.winstaff.bltPracticeMaster,com.winstaff.bltPhysicianMaster,com.winstaff.searchDB2,com.winstaff.bltCompanyAdminLU,com.winstaff.bltUserAccount" %>
<%/*
    filename: out\jsp\tUserAccount_form_sub.jsp
    JSP AutoGen on Mar/19/2002
*/%>



<%

//initial declaration of list class and parentID
    boolean accessValid = false;
        {
            accessValid = true;
        }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateSDF2 = new java.text.SimpleDateFormat("MMM/dd/yyyy");
      java.text.SimpleDateFormat displayDateSDF3 = new java.text.SimpleDateFormat("MMMM dd yyyy");

//initial declaration of list class and parentID

    bltUserAccount        UserAccount        =    new    bltUserAccount();

String testChangeID = "0";





try
{
    java.util.Date testObj = null ;
    try
    {
        testObj = (displayDateSDF1.parse(request.getParameter("UniqueCreateDate"))) ;
    }
    catch(Exception sdf1)
    {

        try
        {
            testObj = (displayDateSDF2.parse(request.getParameter("UniqueCreateDate"))) ;
        }
        catch(Exception sdf2)
        {

            try
            {
                testObj = (displayDateSDF3.parse(request.getParameter("UniqueCreateDate"))) ;
            }
            catch(Exception sdf3)
            {
                if (request.getParameter("UniqueCreateDate").equalsIgnoreCase("today")) 
                {
                    testObj = new java.util.Date() ;
                }
                else if (request.getParameter("UniqueCreateDate").equalsIgnoreCase("current")) 
                {
                    testObj = displayDateSDF1.parse("01/02/1800") ;
                }
                else if (request.getParameter("UniqueCreateDate").equalsIgnoreCase("na")) 
                {
                    testObj = displayDateSDF1.parse("01/03/1800") ;
                }
                else
                {
                    testObj = displayDateSDF1.parse("01/01/1800") ;
                }
            }

        }

    }

    if ( !UserAccount.getUniqueCreateDate().equals(testObj)  )
    {
         UserAccount.setUniqueCreateDate( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    try
    {
        testObj = (displayDateSDF1.parse(request.getParameter("UniqueModifyDate"))) ;
    }
    catch(Exception sdf1)
    {

        try
        {
            testObj = (displayDateSDF2.parse(request.getParameter("UniqueModifyDate"))) ;
        }
        catch(Exception sdf2)
        {

            try
            {
                testObj = (displayDateSDF3.parse(request.getParameter("UniqueModifyDate"))) ;
            }
            catch(Exception sdf3)
            {
                if (request.getParameter("UniqueModifyDate").equalsIgnoreCase("today")) 
                {
                    testObj = new java.util.Date() ;
                }
                else if (request.getParameter("UniqueModifyDate").equalsIgnoreCase("current")) 
                {
                    testObj = displayDateSDF1.parse("01/02/1800") ;
                }
                else if (request.getParameter("UniqueModifyDate").equalsIgnoreCase("na")) 
                {
                    testObj = displayDateSDF1.parse("01/03/1800") ;
                }
                else
                {
                    testObj = displayDateSDF1.parse("01/01/1800") ;
                }
            }

        }

    }

    if ( !UserAccount.getUniqueModifyDate().equals(testObj)  )
    {
         UserAccount.setUniqueModifyDate( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !UserAccount.getUniqueModifyComments().equals(testObj)  )
    {
         UserAccount.setUniqueModifyComments( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount UniqueModifyComments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("StartPage")) ;
    if ( !UserAccount.getStartPage().equals(testObj)  )
    {
         UserAccount.setStartPage( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount StartPage not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AccountType")) ;
    if ( !UserAccount.getAccountType().equals(testObj)  )
    {
         UserAccount.setAccountType( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount AccountType not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReferenceID")) ;
    if ( !UserAccount.getReferenceID().equals(testObj)  )
    {
         UserAccount.setReferenceID( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ReferenceID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AccessType")) ;
    if ( !UserAccount.getAccessType().equals(testObj)  )
    {
         UserAccount.setAccessType( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount AccessType not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Status")) ;
    if ( !UserAccount.getStatus().equals(testObj)  )
    {
         UserAccount.setStatus( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount Status not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LogonUserName")) ;
    if ( !UserAccount.getLogonUserName().equals(testObj)  )
    {
         UserAccount.setLogonUserName( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount LogonUserName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LogonUserPassword")) ;
    if ( !UserAccount.getLogonUserPassword().equals(testObj)  )
    {
         UserAccount.setLogonUserPassword( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount LogonUserPassword not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttestKeyword1")) ;
    if ( !UserAccount.getAttestKeyword1().equals(testObj)  )
    {
         UserAccount.setAttestKeyword1( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount AttestKeyword1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttestKeyword2")) ;
    if ( !UserAccount.getAttestKeyword2().equals(testObj)  )
    {
         UserAccount.setAttestKeyword2( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount AttestKeyword2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactFirstName")) ;
    if ( !UserAccount.getContactFirstName().equals(testObj)  )
    {
         UserAccount.setContactFirstName( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactLastName")) ;
    if ( !UserAccount.getContactLastName().equals(testObj)  )
    {
         UserAccount.setContactLastName( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !UserAccount.getContactEmail().equals(testObj)  )
    {
         UserAccount.setContactEmail( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactAddress1")) ;
    if ( !UserAccount.getContactAddress1().equals(testObj)  )
    {
         UserAccount.setContactAddress1( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactAddress2")) ;
    if ( !UserAccount.getContactAddress2().equals(testObj)  )
    {
         UserAccount.setContactAddress2( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactCity")) ;
    if ( !UserAccount.getContactCity().equals(testObj)  )
    {
         UserAccount.setContactCity( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ContactStateID")) ;
    if ( !UserAccount.getContactStateID().equals(testObj)  )
    {
         UserAccount.setContactStateID( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactZIP")) ;
    if ( !UserAccount.getContactZIP().equals(testObj)  )
    {
         UserAccount.setContactZIP( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactZIP not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SecretQuestion")) ;
    if ( !UserAccount.getSecretQuestion().equals(testObj)  )
    {
         UserAccount.setSecretQuestion( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount SecretQuestion not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("SecretAnswer")) ;
    if ( !UserAccount.getSecretAnswer().equals(testObj)  )
    {
         UserAccount.setSecretAnswer( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount SecretAnswer not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactPhone")) ;
    if ( !UserAccount.getContactPhone().equals(testObj)  )
    {
         UserAccount.setContactPhone( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !UserAccount.getComments().equals(testObj)  )
    {
         UserAccount.setComments( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount Comments not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("CreditCardFullName")) ;
    if ( !UserAccount.getCreditCardFullName().equals(testObj)  )
    {
         UserAccount.setCreditCardFullName( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount CreditCardFullName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardNumber")) ;
    if ( !UserAccount.getCreditCardNumber().equals(testObj)  )
    {
         UserAccount.setCreditCardNumber( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount CreditCardNumber not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardType")) ;
    if ( !UserAccount.getCreditCardType().equals(testObj)  )
    {
         UserAccount.setCreditCardType( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount CreditCardType not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardExpMonth")) ;
    if ( !UserAccount.getCreditCardExpMonth().equals(testObj)  )
    {
         UserAccount.setCreditCardExpMonth( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount CreditCardExpMonth not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CreditCardExpYear")) ;
    if ( !UserAccount.getCreditCardExpYear().equals(testObj)  )
    {
         UserAccount.setCreditCardExpYear( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount CreditCardExpYear not set. this is ok-not an error");
}





try
{
    String testObj = new String(request.getParameter("BillingComments")) ;
    if ( !UserAccount.getBillingComments().equals(testObj)  )
    {
         UserAccount.setBillingComments( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
     out.println("UserAccount BillingComments not set. this is ok-not an error");
}






// If an edit, update information; if new, append information
if (testChangeID.equalsIgnoreCase("1"))
{
	UserAccount.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {

//START-verify this create

boolean isVerified = false;
//if (UserAccount.getUniqueID().intValue()==0)
{
	isVerified = true;
}

//END-verify this create

if (isVerified)
{
	searchDB2 mySDB = new searchDB2();
	String mySQL  = "select * from tUserAccount where (LogonUserName = '"+request.getParameter("LogonUserName")+"' )";
	java.sql.ResultSet myRS = mySDB.executeStatement(mySQL);
	if (!myRS.next())
	{
		String P1 = new String (request.getParameter("LogonUserPassword1"));
		String P2 = new String (request.getParameter("LogonUserPassword2"));
		com.winstaff.password.Encrypt myEnc = new com.winstaff.password.Encrypt();
		if (   myEnc.checkPasswords(P1,P2)  )
		{
			//create Admin Stuff
			bltAdminMaster am = new bltAdminMaster();
			am.setUniqueCreateDate(new java.util.Date());
			am.setUniqueModifyDate(new java.util.Date());
			am.setUniqueModifyComments("New Admin Account");
			am.setName(request.getParameter("Name"));
			am.setAddress1(request.getParameter("ContactAddress1"));
			am.setAddress2(request.getParameter("ContactAddress2"));
			am.setCity(request.getParameter("ContactCity"));
			am.setStateID(new Integer(request.getParameter("ContactStateID")));
			am.setZIP(request.getParameter("ContactZIP"));
			am.setAlertEmail(request.getParameter("ContactEmail"));
			am.setPhone(request.getParameter("ContactPhone"));
			am.setContactFirstName(request.getParameter("ContactFirstName"));
			am.setContactLastName(request.getParameter("ContactLastName"));
			am.setContactAddress1(request.getParameter("ContactAddress1"));
			am.setContactEmail(request.getParameter("ContactEmail"));
			am.setContactAddress2(request.getParameter("ContactAddress2"));
			am.setContactCity(request.getParameter("ContactCity"));
			am.setContactStateID(new Integer(request.getParameter("ContactStateID")));
			am.setContactZIP(request.getParameter("ContactZIP"));
			am.setContactPhone(request.getParameter("ContactPhone"));
			am.setAlertDays(new Integer("30"));
			am.commitData();
			Integer iAdminID = am.getUniqueID();


			//set User Account Stuff
			UserAccount.setAccessType(new Integer("2"));
			UserAccount.setStatus(new Integer("2"));
			UserAccount.setStartPage("pro-file-manager_Status.jsp");
			UserAccount.setAccountType("AdminID");
			UserAccount.setReferenceID( iAdminID   );














			UserAccount.setLogonUserPassword(myEnc.getMD5Base64(P1));
			UserAccount.setUniqueCreateDate(new java.util.Date());
			UserAccount.setUniqueModifyDate(new java.util.Date());
			UserAccount.setUniqueModifyComments("New Admin Account");
			UserAccount.setSecurityGroupID(new Integer("2"));
			UserAccount.setGenericSecurityGroupID(new Integer("1"));

			//set PLC ID for PCMG
			UserAccount.setPLCID(new Integer("1"));
			UserAccount.commitData();
			//set GSN User Link
			Integer        iCompanyID        =   new Integer( 1 );
			bltCompanyUserAccountLU        working_bltCompanyUserAccountLU = new bltCompanyUserAccountLU();
			working_bltCompanyUserAccountLU.setCompanyID(iCompanyID);
			working_bltCompanyUserAccountLU.setUserID(UserAccount.getUniqueID());
			working_bltCompanyUserAccountLU.commitData();
			//set GSN Admin Link
			bltCompanyAdminLU        working_bltCompanyAdminLU = new bltCompanyAdminLU();
			working_bltCompanyAdminLU.setCompanyID(iCompanyID);
			working_bltCompanyAdminLU.setAdminID(am.getUniqueID());
			working_bltCompanyAdminLU.commitData();
			iCompanyID = new Integer(1);
			if (iCompanyID.intValue()>1)
			{
				//set Company User Link
				working_bltCompanyUserAccountLU = new bltCompanyUserAccountLU();
				working_bltCompanyUserAccountLU.setUniqueCreateDate(new java.util.Date());
				working_bltCompanyUserAccountLU.setUniqueModifyDate(new java.util.Date());
				working_bltCompanyUserAccountLU.setUniqueModifyComments("New Admin Account");
				working_bltCompanyUserAccountLU.setCompanyID(iCompanyID);
				working_bltCompanyUserAccountLU.setUserID(UserAccount.getUniqueID());
				working_bltCompanyUserAccountLU.commitData();
				//set Company Admin Link
				working_bltCompanyAdminLU = new bltCompanyAdminLU();
				working_bltCompanyAdminLU.setUniqueCreateDate(new java.util.Date());
				working_bltCompanyAdminLU.setUniqueModifyDate(new java.util.Date());
				working_bltCompanyAdminLU.setUniqueModifyComments("New Admin Account");
				working_bltCompanyAdminLU.setCompanyID(iCompanyID);
				working_bltCompanyAdminLU.setAdminID(iAdminID);
				working_bltCompanyAdminLU.commitData();
			}
		        out.print("success");
		        response.sendRedirect("tUserAccount_form_signUp_success.jsp");
		}
		else
		{
		%>
		<script langauge=javascript>
			document.write();
			alert("Your Password is invalid.  Please try again.\nNote, for security reasons you may need to re-enter your passwords and keywords.");
			history.go(-1);
			</script>
		<%
		}
    mySDB.closeAll();


	}
	else
	{
	%>
		<script langauge=javascript>
		alert("Your User Name is invalid or already taken.  Please try again.");
		history.go(-1);
		</script>
	<%
	}
}






    }
}



  }
  else
  {
   out.println("illegal");
  }
  %>
