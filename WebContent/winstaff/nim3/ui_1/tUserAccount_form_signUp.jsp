<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltUserAccount" %>
<%/*

    filename: out\jsp\tUserAccount_form.jsp
    JSP AutoGen on Mar/25/2002
*/%>
<%@ include file="../../generic/generalDisplay.jsp" %>

<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<link rel="stylesheet" href="style_PhysicianID.css" type="text/css">



    
<script language="JavaScript">
<!--
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') {
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (val<min || max<val) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>

<title>PRO-FILE Register</title>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table cellpadding=5 cellspacing=0 border=0 width=700>
  <tr> 
    <td colspan="2"><img src=images/winstaff-blue-logo.gif></td>
  </tr>
  <tr> 
    <td width=10>&nbsp;</td>
    <td valign="top"> <br>
      <%
//initial declaration of list class and parentID
    boolean accessValid = false;
    // required for Type2
    {
        accessValid = true;    
		}
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");


//initial declaration of list class and parentID

//this is changed manually to allow a company to create user accounts.  This one is trickier, because data collection must come before accounts can be created
    bltUserAccount        UserAccount        =    new    bltUserAccount();


//fields
        %>
      <span class=tdHeader> WIN|Staff Account Registration</span> 
      <p class=instructions>Create a User Name account and a password. Once you 
        have selected your user name and password you will be asked to enter some 
        personal information to complete the setup of your account. There will 
        also be one security question that will help the GSN (Global Services 
        Network) department verify who you are if you ever have questions on your 
        account.</p>
      <form method=POST action="tUserAccount_form_signUp_sub.jsp" name="tUserAccount_form1">
        <table border = 1 bordercolor="#333333" width="100%" cellpadding="5" cellspacing="0" bgcolor="#CCCCCC" >
          <tr> 
            <td> 
              <table width="100%" border="0" cellspacing="0" cellpadding="2">
                <tr class=tdBase> 
                  <td colspan="3" class=instructions>The Admin Group User is the 
                    individual or entity that oversees the account(s) of one or 
                    more practitioners within a group or practice.</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="3"><b>Admin Group Name</b> 
                    <input type=text size="50" name="Name">
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdHeaderAlt> 
                  <td colspan="3">User Information</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2"><b>Logon User Name</b> 
                    <input type=text size="10" name="LogonUserName" value="<%=UserAccount.getLogonUserName()%>">
                  </td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=instructions> 
                  <td colspan="3">Create Password<span class="requiredFields">*</span><span class="greentextRegular"> 
                    (<b>Must be at least eight (8) characters long</b>, may contain 
                    numbers (0-9) and upper and lowercase letters (A-Z, a-z), 
                    but no spaces. Make sure it is difficult for others to guess!) 
                    </span></td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2"><b>Logon User Password:</b>&nbsp; 
                    <input type="password" name="LogonUserPassword1">
                  </td>
                  <td><b>Confirm:</b> 
                    <input type="password" name="LogonUserPassword2">
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=instructions> 
                  <td colspan="3">Pick a secret question(Choose a question only 
                    you know the answer to and that has nothing to do with your 
                    password)</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2"><b>Secret Question:</b> 
                    <input type=text size="37" name="SecretQuestion">
                  </td>
                  <td><b>Answer:</b> 
                    <input type=text size="37" name="SecretAnswer">
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdHeaderAlt> 
                  <td colspan="3">Contact Information</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2"> 
                    <p ><b>First Name</b> 
                      <input type=text size="25" name="ContactFirstName" value="<%=UserAccount.getContactFirstName()%>">
                    </p>
                  </td>
                  <td> 
                    <p  class=tdBase><b> Last Name</b> 
                      <input type=text size="25" name="ContactLastName" value="<%=UserAccount.getContactLastName()%>">
                    </p>
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2"><b>Phone</b> 
                    <input type=text size="25" name="ContactPhone" value="<%=UserAccount.getContactPhone()%>">
                  </td>
                  <td><b>Email</b> 
                    <input type=text size="37" name="ContactEmail" value="<%=UserAccount.getContactEmail()%>">
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdHeaderAlt> 
                  <td colspan="3">Billing Information</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="3"> 
                    <p  ><b>Full Name (as it appears on credit card)</b> 
                      <input   type=text size="50" name="CreditCardFullName">
                    </p>
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2"> 
                    <p  class=tdBase ><b>Address</b> 
                      <input type=text size="50" name="ContactAddress1" value="<%=UserAccount.getContactAddress1()%>" maxlength="s">
                    </p>
                  </td>
                  <td> 
                    <p  class=tdBase><b>Address 2</b> 
                      <input type=text size="10" name="ContactAddress2" value="<%=UserAccount.getContactAddress2()%>">
                    </p>
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td> 
                    <p ><b>City</b> 
                      <input type=text size="15" name="ContactCity" value="<%=UserAccount.getContactCity()%>">
                    </p>
                  </td>
                  <td><b>State </b> 
                    <select name="ContactStateID" >
                      <jsp:include page="../../generic/tStateLILong.jsp" flush="true" > 
                      <jsp:param name="CurrentSelection" value="<%=UserAccount.getContactStateID()%>" />
                      </jsp:include>
                    </select>
                  </td>
                  <td><b>ZIP</b> 
                    <input type=text size="5" name="ContactZIP" value="<%=UserAccount.getContactZIP()%>">
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2"><b>Credit Card Type</b> 
                    <select name="CreditCardType">
                      <option selected>Visa</option>
                      <option>Mastercard</option>
                      <option>American Express</option>
                      <option>Discover</option>
                    </select>
                  </td>
                  <td><b>Credit Card Number</b> 
                    <input   type=text size="20" name="CreditCardNumber" maxlength="16">
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td><b>Expiration Month</b> 
                    <input   type=text size="4" name="CreditCardExpMonth" maxlength="2">
                  </td>
                  <td><b>Expiration Year</b> 
                    <input   type=text size="6" name="CreditCardExpYear" maxlength="4">
                  </td>
                  <td>
                  </td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=tdBase> 
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr class=instructions> 
                  <td colspan="3">Your credit card will be billed annually in 
                    the month you enter the practitioner record for the PRO-FILE 
                    subscription in the amount of $90 per practitioner linked 
                    to this Admin User Account.<br>
                  </td>
                  <td>&nbsp;</td>
                </tr>
              </table>
              <p class=requiredField> I hereby certify that I am an authorized 
                representative of the practitioner(s) I manage through this PRO-FILE 
                system. I acknowledge that all the information is entered and 
                maintained by me, and, is accurate and true as directed by the 
                practitioner(s) I manage. Information submitted to healthcare 
                organizations by WIN/Staff on my behalf is in the format and content 
                that I entered and I will not hold WIN/Staff liable for any direct, 
                indirect, or consequential damages caused by inaccurate or incomplete 
                information.</p>
              <p> 
                <input type=hidden value="sParentReturnPage">
                <input style="font-family: Arial; font-size: 10px; background-color: #C0C0C0"  type=Submit value=Submit name=Submit onClick="MM_validateForm('Name','','R','LogonUserName','','R','SecretQuestion','','R','SecretAnswer','','R','ContactFirstName','','R','ContactLastName','','R','ContactAddress1','','R','ContactCity','','R','ContactZIP','','R','ContactPhone','','R','ContactEmail','','R','CreditCardFullName','','R','CreditCardNumber','','RisNum','CreditCardExpMonth','','RisNum','CreditCardExpYear','','RisNum','LogonUserPassword1','','R','LogonUserPassword2','','R');return document.MM_returnValue">
              </p>
            </td>
          </tr>
        </table >
      </form>
      <%
  }
  else
  {
   out.println("illegal");
  }

%>
    </td>
  </tr>
</table>
<table width="500" border="0" cellspacing="0" cellpadding="0" bgcolor=#FFFFFF>
  <tr>
    <td bgcolor="#FFFFFF"><img src="images/bot-nav_PhysicianID.gif" ></td>
  </tr>
</table>

