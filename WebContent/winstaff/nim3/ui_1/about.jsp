<html>
<head>
<title>about.jpg</title>
<meta http-equiv="Content-Type" content="text/html;">
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Mon Mar 18 16:44:52 GMT-0800 (Pacific Standard Time) 2002-->
<link rel="stylesheet" href="style_PhysicianID.css" type="text/css">
<script language="JavaScript">
<!--
function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}
//-->
</script>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table class=tdBase border="0" cellpadding="0" cellspacing="0" width="400">
<!-- fwtable fwsrc="about.png" fwbase="about.jpg" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->

  <tr>
   <td colspan="4"><img name="about_r1_c1" src="images/about_r1_c1.jpg" width="400" height="10" border="0"></td>
   <td><img src="images/spacer.gif" width="1" height="10" border="0"></td>
  </tr>
  <tr>
   <td rowspan="2" colspan="2"><img name="about_r2_c1" src="images/about_r2_c1.jpg" width="349" height="117" border="0"></td>
    <td><a href="#" onClick="MM_callJS('window.close()')"><img name="about_r2_c3" src="images/about_r2_c3.jpg" width="40" height="17" border="0"></a></td>
   <td rowspan="2"><img name="about_r2_c4" src="images/about_r2_c4.jpg" width="11" height="117" border="0"></td>
   <td><img src="images/spacer.gif" width="1" height="17" border="0"></td>
  </tr>
  <tr>
   <td><img name="about_r3_c3" src="images/about_r3_c3.jpg" width="40" height="100" border="0"></td>
   <td><img src="images/spacer.gif" width="1" height="100" border="0"></td>
  </tr>
  <tr>
    <td valign="top"><img name="about_r4_c1" src="images/about_r4_c1.jpg" width="57" height="212" border="0"></td>
   <td rowspan="2" colspan="3" valign="top">
      <p class="title">WIN|Staff PRO-FILE Version 3.1b</p>
      <p class="tdBase">WIN/Staff PRO-FILE is a web-based application system enabling 
        practitioners and healthcare organizations (HCOs) to reference a single 
        common credentialing application database. Practitioners update required 
        information and HCOs access application information via secured web access. 
        Our unique, proprietary security technology places information control 
        in the hands of the practitioner. HCOs control the receipt of applications, 
        replacing the current practice dependent on mail delivery following the 
        completion of paper forms by busy practitioner staffs. </p>
      <p class="tdBase">While some companies offer online application products, 
        they still rely on paper forms for practitioners to review, sign, and 
        send via mail to healthcare organizations. WIN/Staff PRO-FILE provides 
        a complete end-to-end electronic solution by delivering practitioner applications, 
        signatures, attestations, and copies of credentials electronically to 
        healthcare organizations. The information may then be electronically imported 
        to a healthcare organization's credentialing software, using WIN/Staff 
        ENTERPRISE, or another product. Both practitioners and healthcare organizations 
        benefit with significant time and cost savings. <br>
        <br>
        Practitioner Benefits: </p>
      <ul  class=tdBase>
        <li>Single location for storage and maintenance of credentialing information 
        </li>
        <li>Automatic notification of credential renewals, ensuring up-to-date 
          information </li>
        <li>Elimination of valuable time used processing repetitive paperwork 
        </li>
        <li>More staff time for patient care and office management needed previously 
          for credential application processing </li>
        <li>Savings in time, money and headache </li>
      </ul>
    </td>
   <td><img src="images/spacer.gif" width="1" height="212" border="0"></td>
  </tr>
  <tr>
   <td valign="top"></td>
   <td><img src="images/spacer.gif" width="1" height="161" border="0"></td>
  </tr>
  <tr>
   <td><img src="images/spacer.gif" width="57" height="1" border="0"></td>
   <td><img src="images/spacer.gif" width="292" height="1" border="0"></td>
   <td><img src="images/spacer.gif" width="40" height="1" border="0"></td>
   <td><img src="images/spacer.gif" width="11" height="1" border="0"></td>
   <td><img src="images/spacer.gif" width="1" height="1" border="0"></td>
  </tr>
</table>
</body>
</html>