<%@page contentType="text/html" language="java" import="com.winstaff.stepVerify_PhysicianID,com.winstaff.bltStateLI, com.winstaff.bltPhysicianMaster" %>

<%@ include file="../../generic/CheckLogin.jsp" %>
 
<%
    Integer        iPhysicianID        =    null;
    Integer        iHCOLUID        =    null;
    boolean accessValid = false;
    boolean isHCO = false;
    if (pageControllerHash.containsKey("iPhysicianID")) 
    {
        iPhysicianID        =    (Integer)pageControllerHash.get("iPhysicianID");
        accessValid = true;
    }
	bltPhysicianMaster pm = new bltPhysicianMaster(iPhysicianID);

    if (pageControllerHash.containsKey("iHCOLUID")) 
    {
        iHCOLUID        =    (Integer)pageControllerHash.get("iHCOLUID");
        isHCO = true;
    }


    String plcID = request.getParameter("plcID");
    if ((plcID==null)||(plcID.equalsIgnoreCase("null")))
    {
        plcID="";
    }
    else
    {
        plcID="_"+plcID;
    }
%>
<link rel="stylesheet" href="ui<%=plcID%>\style_PhysicianID.css" type="text/css">
<html>
<head>
<title>PRO-File Version 3.1b</title>
<meta http-equiv="Content-Type" content="text/html;">
<!-- Fireworks 4.0  Dreamweaver 4.0 target.  Created Tue Jan 21 15:44:58 GMT-0800 (Pacific Standard Time) 2003-->
<script language="JavaScript">
<!--
function fwLoadMenus() {
  if (window.fw_menu_0) return;
  window.fw_menu_0 = new Menu("root",200,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_0.addMenuItem("Practitioner Home","location='pro-file_Status.jsp'");
  fw_menu_0.addMenuItem("Options...","location='Physician_options.jsp'");
  fw_menu_0.addMenuItem("Additional Sections:","location='#'");
  fw_menu_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;HCO Authorization","location='Physician_HCOAuth.jsp'");
  fw_menu_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Populate Forms","location='populateForms.jsp'");
  fw_menu_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;CME Manager","location='CME-Manager_PhysicianID.jsp'");
  fw_menu_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;CV Builder","location='CV-Builder_PhysicianID.jsp'");
  fw_menu_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Document Management","location='tDocumentManagement_PhysicianID.jsp'");
  fw_menu_0.addMenuItem("&nbsp;&nbsp;&nbsp;&nbsp;Reports","location='reports_PhysicianID.jsp'");
<%
if (pageControllerHash.containsKey("iAdminID"))
{
%>
  fw_menu_0.addMenuItem("Return to PRO-FILE Manager Home","location='pro-file-manager_Status.jsp'");
<%
}
else
{
%>
<%
}
%>
  fw_menu_0.addMenuItem("Sign Off","location='signOff.jsp'");
   fw_menu_0.fontWeight="bold";
   fw_menu_0.hideOnMouseOut=true;
  window.fw_menu_1 = new Menu("root",200,14,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;1. Identifying Information","location='tPhysicianMaster_form.jsp?EDIT=edit'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;2. Covering Physicians","location='tCoveringPhysicians_main_CoveringPhysicians_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;3. Licensure/Registration","location='tLicenseRegistration_main_LicenseRegistration_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;4. Specialty/Certifications","location='tBoardCertification_main_BoardCertification_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;5. Other Certification","location='tOtherCertification_main_OtherCertification_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;6. Education","location='tProfessionalEducation_main_ProfessionalEducation_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;7. Training","location='tExperience_main_Experience_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;8. Work History/Experience","location='tWorkHistory_main_WorkHistory_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("&nbsp;&nbsp;9. Professional Liability Coverage","location='tProfessionalLiability_main_ProfessionalLiability_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("10. Hospital/Facility Affiliations","location='tFacilityAffiliation_main_FacilityAffiliation_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("11. Peer References","location='tPeerReference_main_PeerReference_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("12. Professional Associations","location='tProfessionalSociety_main_ProfessionalSociety_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("13. Disclosure Questions","location='tAttestR_form2.jsp'");
  fw_menu_1.addMenuItem("14. Malpractice Claims History","location='tMalpractice_main_Malpractice_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("15. Professional Misconduct History","location='tProfessionalMisconduct_main_ProfessionalMisconduct_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("16. Continuing Education","location='tContinuingEducation_main_ContinuingEducation_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("17. Managed Care Plans","location='tManagedCarePlan_main_ManagedCarePlan_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("18. Supplemental Data","location='SupplementalData_main_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("19. HCO Authorization","location='Physician_HCOAuth.jsp'");
  fw_menu_1.addMenuItem("20. Practice Information","location='tPhysicianPracticeLU_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("21. Attestation Account","location='tPhysicianUserAccountLU_main2_UserAccount_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("22. Attestation","location='pro-file_Attestation.jsp'");
  fw_menu_1.addMenuItem("23. Attached Documents","location='tDocumentManagement_PhysicianID.jsp'");
  fw_menu_1.addMenuItem("24. Application Checklist","location='FinishedCheckList_PhysicianID.jsp'");
   fw_menu_1.fontWeight="bold";
   fw_menu_1.hideOnMouseOut=true;
  window.fw_menu_2 = new Menu("root",160,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_2.addMenuItem("Document Management","location='tDocumentManagement_PhysicianID.jsp'");
  fw_menu_2.addMenuItem("Search Documents","location='tDocumentManagement_query_PhysicianID.jsp?DocumentID=&DocumentTypeID=0&DocumentName=&DateOfExpiration=&Archived=2&orderBy=DocumentID&startID=0&maxResults=50&Submit2=Submit'");
   fw_menu_2.fontWeight="bold";
   fw_menu_2.hideOnMouseOut=true;
  window.fw_menu_3 = new Menu("root",161,17,"Arial, Helvetica, sans-serif",10,"#000000","#993333","#ffffff","#cccccc");
  fw_menu_3.addMenuItem("General Help","window.open('help_general.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
  fw_menu_3.addMenuItem("Frequently Asked Questions","window.open('help_faqs.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
  fw_menu_3.addMenuItem("Contact Us","window.open('ui_1/contact_us.jsp', '_blank','scrollbars=yes,status=yes, width=400,height=630,resizable=yes');");
  fw_menu_3.addMenuItem("About","window.open('ui_1/about.jsp', '_blank','scrollbars=yes,status=yes, width=630,height=500,resizable=yes');");
   fw_menu_3.fontWeight="bold";
   fw_menu_3.hideOnMouseOut=true;

  fw_menu_3.writeMenus();
} // fwLoadMenus()

//-->
</script>
<script language="JavaScript1.2" src="ui_1/fw_menu.js"></script>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" bgcolor="#ffffff">
<script language="JavaScript1.2">fwLoadMenus();</script>
<table border="0" cellpadding="0" cellspacing="0" width="700">
<!-- fwtable fwsrc="top-nav2_PhysicianID.png" fwbase="top-nav_PhysicianID.jpg" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->

  <tr>
   <td colspan="11"><img name="topnav_PhysicianID_r1_c1" src="ui_1/images/top-nav_PhysicianID_r1_c1.jpg" width="700" height="60" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="1" height="60" border="0"></td>
  </tr>
  <tr>
   <td rowspan="2"><img name="topnav_PhysicianID_r2_c1" src="ui_1/images/top-nav_PhysicianID_r2_c1.jpg" width="133" height="30" border="0"></td>
   <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_0,134,85);" ><img name="topnav_PhysicianID_r2_c2" src="ui_1/images/top-nav_PhysicianID_r2_c2.jpg" width="92" height="25" border="0"></a></td>
   <td rowspan="2"><img name="topnav_PhysicianID_r2_c3" src="ui_1/images/top-nav_PhysicianID_r2_c3.jpg" width="5" height="30" border="0"></td>
   <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_1,232,85);" ><img name="topnav_PhysicianID_r2_c4" src="ui_1/images/top-nav_PhysicianID_r2_c4.jpg" width="102" height="25" border="0"></a></td>
   <td rowspan="2"><img name="topnav_PhysicianID_r2_c5" src="ui_1/images/top-nav_PhysicianID_r2_c5.jpg" width="5" height="30" border="0"></td>
   <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_2,338,85);" ><img name="topnav_PhysicianID_r2_c6" src="ui_1/images/top-nav_PhysicianID_r2_c6.jpg" width="102" height="25" border="0"></a></td>
   <td rowspan="2"><img name="topnav_PhysicianID_r2_c7" src="ui_1/images/top-nav_PhysicianID_r2_c7.jpg" width="5" height="30" border="0"></td>
   <td><a href="#" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_3,445,85);" ><img name="topnav_PhysicianID_r2_c8" src="ui_1/images/top-nav_PhysicianID_r2_c8.jpg" width="59" height="25" border="0"></a></td>
   <td rowspan="2"><img name="topnav_PhysicianID_r2_c9" src="ui_1/images/top-nav_PhysicianID_r2_c9.jpg" width="5" height="30" border="0"></td>
    <td><a href="signOff.jsp"><img name="topnav_PhysicianID_r2_c10" src="ui_1/images/top-nav_PhysicianID_r2_c10.jpg" width="77" height="25" border="0"></a></td>
   <td rowspan="2"><img name="topnav_PhysicianID_r2_c11" src="ui_1/images/top-nav_PhysicianID_r2_c11.jpg" width="115" height="30" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="1" height="25" border="0"></td>
  </tr>
  <tr>
   <td><img name="topnav_PhysicianID_r3_c2" src="ui_1/images/top-nav_PhysicianID_r3_c2.jpg" width="92" height="5" border="0"></td>
   <td><img name="topnav_PhysicianID_r3_c4" src="ui_1/images/top-nav_PhysicianID_r3_c4.jpg" width="102" height="5" border="0"></td>
   <td><img name="topnav_PhysicianID_r3_c6" src="ui_1/images/top-nav_PhysicianID_r3_c6.jpg" width="102" height="5" border="0"></td>
   <td><img name="topnav_PhysicianID_r3_c8" src="ui_1/images/top-nav_PhysicianID_r3_c8.jpg" width="59" height="5" border="0"></td>
   <td><img name="topnav_PhysicianID_r3_c10" src="ui_1/images/top-nav_PhysicianID_r3_c10.jpg" width="77" height="5" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="1" height="5" border="0"></td>
  </tr>
  <tr >
    <td colspan=15>
      <table width="700" border="1" cellspacing="0" cellpadding="0" bordercolor="#999999">
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="1" bordercolor="#CCCCCC" bgcolor="#FFFFFF">
              <tr>
                <td class=tdHeaderAlt nowrap>Practitioner:</td>
                <td class=tdHeader nowrap width="99%">&nbsp;<%=pm.getLastName()%>, 
                  <%=pm.getFirstName()%> [ID: <%=pm.getPhysicianID()%>]</td>
                <td class=tdHeader nowrap><a href="pro-file_Status.jsp">Practitioner Home</a>&nbsp;&nbsp;<a href="FinishedCheckList_PhysicianID.jsp">Application Checklist</a></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
   <td><img src="ui_1/images/spacer.gif" width="133" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="92" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="102" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="102" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="59" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="5" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="77" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="115" height="1" border="0"></td>
   <td><img src="ui_1/images/spacer.gif" width="1" height="1" border="0"></td>
  </tr>
</table>
