<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="    com.winstaff.Encrypt.*, com.winstaff.*" %>
<%/*
    filename: out\jsp\tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp
    Created on Jan/06/2009
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%
//initial declaration of list class and parentID
	String myUserName="";
	String myRoute="";
	String myPassword="";
	String mySP="";
	boolean isPassed = false;
	String ip = request.getRemoteAddr();
	String host = request.getRemoteHost();
	Integer UID = null;
	Integer iCaseID = null;
try
{
	myUserName = request.getParameter("LogonUserName");
	myPassword = request.getParameter("LogonUserPassword");
	mySP = request.getParameter("ScanPass");
	com.winstaff.password.Encrypt myEnc = new com.winstaff.password.Encrypt();
	
	searchDB2 mySS = new searchDB2();
	String mySQL = "select tUserAccount.UserId, tUserAccount.LogonUserPassword from tUserAccount where UPPER(tUserAccount.LogonUserName)= UPPER('" + myUserName + "') AND tUserAccount.LogonUserPassword = '" +  myEnc.getMD5Base64(myPassword) + "' AND tUserAccount.Status>0";
//	out.println (mySQL);
	java.sql.ResultSet myRS = mySS.executeStatement(mySQL);
	
if (myRS!=null&&myRS.next())
{
	UID = new Integer(myRS.getString("UserID"));
//	iCaseID = new Integer(myRS.getString("CaseID"));
	//out.println("UID = " + UID);
	isPassed = true;
}
else
{
		isPassed = false;
}



mySS.closeAll();

}
catch(Exception e)
{
out.println("Query Failed: " + e);
}


if (!isPassed)
{
	bltUATransaction myUTA = new bltUATransaction();
	myUTA.setTransactionDate(PLCUtils.getNowDate(false));
	myUTA.setActionID(new Integer(2));
	myUTA.setComments("Failed Outside View [" + myUserName + "] - host: " + "[" + host + "]");
	myUTA.setRemoteIP(ip);
	myUTA.setUniqueCreateDate(PLCUtils.getNowDate(false));
	myUTA.commitData();
//	response.sendRedirect("Outside_View_report.jsp");
		   %>
           <script language="javascript">
		   alert ("Invalid Password or ScanPass\n\nPlease Try Again\n\n\If you are still having trouble, please contact and NextImage Support Representative at 888-318-5111");
		   history.go(-1);
           </script>
           <%
}
else
{
	bltUATransaction myUTA = new bltUATransaction();
	myUTA.setTransactionDate(PLCUtils.getNowDate(false));
	myUTA.setActionID(new Integer(1));
	myUTA.setComments("Successful Outside View - host: " + "[" + host + "]");
	myUTA.setRemoteIP(ip);
	myUTA.setUserID(UID);
	myUTA.setUniqueCreateDate(PLCUtils.getNowDate(false));
	myUTA.commitData();
	try
	{

		java.util.Hashtable pageControllerHash = new java.util.Hashtable();
		bltUserAccount UA = new bltUserAccount(UID);
		pageControllerHash.put("i"+UA.getAccountType(),UA.getReferenceID());
		pageControllerHash.put("iCurrentUserID",UA.getUserID());
		pageControllerHash.put("plcID",String.valueOf(UA.getPLCID()));
//		out.println("<Br>PLCID" + UA.getPLCID());
		UA.setPLCID(new Integer(200));
//		out.println("<Br>PLCID_after" + UA.getPLCID());
		java.text.SimpleDateFormat umcD = new java.text.SimpleDateFormat("MM/dd/yyyy h:mm a");
		pageControllerHash.put("UserLogonDescription",UA.getLogonUserName()+"["+UA.getUserID()+"]["+umcD.format(PLCUtils.getNowDate(false))+"]");
		pageControllerHash.put("signOffPageRoute","ui_"+UA.getPLCID()+"/signOff.jsp");
		String sStart = UA.getStartPage();

		if (myRoute ==null||myRoute.equalsIgnoreCase("null")||myRoute.equalsIgnoreCase(""))
		{
		}
		else
		{
			sStart = myRoute;
		}
		pageControllerHash.put("sStartPage",sStart);
		pageControllerHash.put("UserSecurityGroupID",UA.getSecurityGroupID());
		pageControllerHash.put("iGenericSecurityGroupID",UA.getGenericSecurityGroupID());
//		out.println("<Br>SecGID" + UA.getSecurityGroupID());
		bltSecurityGroupMaster mySG = new bltSecurityGroupMaster(UA.getSecurityGroupID());
		pageControllerHash.put("isShowAudit",mySG.getShowAuditID());
//set document
		searchDB2 mySS = new searchDB2();



String mySQL = "select tNIM3_Encounter.EncounterID from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid where tNIM3_Encounter.ScanPass = '" + mySP + "' and (tNIM3_CaseAccount.AdjusterID  = " + UA.getUserID()  + " OR tNIM3_CaseAccount.CaseAdministratorID  = " + UA.getUserID()  + ")";
if (UA.getAccountType().equalsIgnoreCase("PhysicianID"))
{
	mySQL = "select tNIM3_Encounter.EncounterID from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid where tNIM3_Encounter.ScanPass = '" + mySP + "' and tNIM3_Referral.ReferringPhysicianID= " + UA.getUserID()  + "";
}
//		out.println (mySQL);
		java.sql.ResultSet myRS = mySS.executeStatement(mySQL);
		Integer iEncounterID  = null;
		if (myRS!=null&&myRS.next())
		{
			iEncounterID  = new Integer(myRS.getString("EncounterID"));
			bltNIM3_Encounter	NIM3_Encounter        =    new    bltNIM3_Encounter(iEncounterID);
			bltNIM3_Referral NIMR = new bltNIM3_Referral(NIM3_Encounter.getReferralID());
			bltNIM3_CaseAccount NCA = new bltNIM3_CaseAccount(NIMR.getCaseID());
			bltNIM3_Appointment myNALU = new bltNIM3_Appointment(NIM3_Encounter.getAppointmentID());
			bltPracticeMaster myPracM = new bltPracticeMaster(myNALU.getProviderID());
//			bltICMaster myICM = new bltICMaster (myNALU.getProviderID());
			bltUserAccount myUA = new bltUserAccount(new Integer(NCA.getAdjusterID()));
			bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(myUA.getPayerID());
			bltUserAccount myRefDr = new bltUserAccount (NIMR.getReferringPhysicianID());

			java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
			java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
            java.text.SimpleDateFormat displayDateDay = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDay);
            java.text.SimpleDateFormat displayDateDayWeek = new java.text.SimpleDateFormat(PLCUtils.String_displayDateDayWeek);
            java.text.SimpleDateFormat displayDateMonth = new java.text.SimpleDateFormat(PLCUtils.String_displayDateMonth);
            java.text.SimpleDateFormat displayDateYear = new java.text.SimpleDateFormat(PLCUtils.String_displayDateYear);
            java.text.SimpleDateFormat displayDateHour = new java.text.SimpleDateFormat(PLCUtils.String_displayDateHour);
			java.util.Date dbAppointment = myNALU.getAppointmentTime();
			String sAppointmentTime = displayDateTimeSDF.format((dbAppointment));
			String sAppointmentTimeDay = displayDateDay.format((dbAppointment));
			String sAppointmentTimeDayWeek = displayDateDayWeek.format((dbAppointment));
			String sAppointmentTimeMonth = displayDateMonth.format((dbAppointment));
			String sAppointmentTimeYear = displayDateYear.format((dbAppointment));
			String sAppointmentHour = displayDateHour.format((dbAppointment));

			bltNIM3_Document working_bltNIM3_Document  = new bltNIM3_Document(NIM3_Encounter.getReportFileID());
			pageControllerHash.put("sFileName",working_bltNIM3_Document.getFileName());
			pageControllerHash.put("sDownloadName", com.winstaff.password.RandomString.generateString(4).toLowerCase() + Math.round(Math.random()*1000)+com.winstaff.password.RandomString.generateString(4).toLowerCase() + "_" + working_bltNIM3_Document.getFileName());
			pageControllerHash.put("bDownload", new Boolean(true));
			session.setAttribute("pageControllerHash",pageControllerHash);
			%>
           <link href="ui_200/style_PayerID.css" rel="stylesheet" type="text/css">
           
           <title>NextImage View Report</title><table width="800" border="0" align="left" cellpadding="0" cellspacing="0">
             <tr>
               <td>&nbsp;</td>
             </tr>
             <tr>
               <td><table border="0" align="center" cellpadding="5" cellspacing="0">
                 <tr>
                   <td colspan="2" nowrap>Welcome <strong><%=UA.getContactFirstName()%>&nbsp;<%=UA.getContactLastName()%></strong> </td>
                 </tr>



                 <tr>
                   <td nowrap>ScanPass: </td>
                   <td><strong><%=mySP%></strong></td>
                 </tr>
                 <tr>
                   <td nowrap>Insurance Carrier: </td>
                   <td><strong><%=myPM.getPayerName()%></strong></td>
                 </tr>
                 <tr>
                   <td nowrap>Adjuster: </td>
                   <td><strong><%=myUA.getContactFirstName() + " " + myUA.getContactLastName()%></strong></td>
                 </tr>
                 <tr>
                   <td nowrap>Patient: </td>
                   <td><strong><%=NCA.getPatientFirstName() + " "  + NCA.getPatientLastName()%></strong></td>
                 </tr>
                 <tr>
                   <td nowrap>Claim Number: </td>
                   <td><strong><%=NCA.getCaseClaimNumber()%></strong></td>
                 </tr>
                 <tr>
                   <td nowrap>Date of Injury: </td>
                   <td><strong><%=displayDateSDF.format(NCA.getDateOfInjury())%></strong></td>
                 </tr>
                 <tr>
                   <td nowrap>Referring Doctor: </td>
                   <td><strong><%=myRefDr.getContactFirstName() + " " + myRefDr.getContactLastName()%></strong></td>
                 </tr>
                 <%
                if ( !myRefDr.getContactPhone().equalsIgnoreCase(""))
                {
				%>
                 <tr>
                   <td nowrap>Phone: </td>
                   <td><strong><%=myRefDr.getContactPhone()%></strong></td>
                 </tr>
                 <%
                }
                if ( !myRefDr.getContactPhone().equalsIgnoreCase(""))
                {
				%>
                 <tr>
                   <td nowrap>Fax: </td>
                   <td><strong><%=myRefDr.getContactFax()%></strong></td>
                 </tr>
                 <%
                }
				%>
                 <tr>
                   <td nowrap>Procedures: </td>
                   <td>
                   <%
						searchDB2 mySS_cpt = new searchDB2();
						java.sql.ResultSet myRS_cpt = null;;
						try
						{
							String mySQL_cpt = ("select * from tNIM3_Service where EncounterID = " + NIM3_Encounter.getEncounterID()  + " order by uniquecreatedate");
							myRS_cpt = mySS_cpt.executeStatement(mySQL_cpt);
							int endCount = 0;
							int cnt=0;
							int cnt2=0;
							while (myRS_cpt!=null&&myRS_cpt.next())
							{
								bltNIM3_Service working_bltNIM3_Service  =  new bltNIM3_Service(new Integer(myRS_cpt.getString("ServiceID")));
								if ( !working_bltNIM3_Service.getCPT().equalsIgnoreCase(""))
								{
									out.print("&nbsp;&nbsp;&nbsp;CPT: " + working_bltNIM3_Service.getCPT());
									out.print("<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Description: " + NIMUtils.getCPTText(working_bltNIM3_Service.getCPT()));
									out.print("<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Body Party: " + working_bltNIM3_Service.getCPTBodyPart());
								}
							}
						}
						catch(Exception e)
						{
					//		out.println("ResultsSet:"+e);
						}
						mySS_cpt.closeAll();
                   
                   %></td>
                 </tr>
                 <tr>
                   <td nowrap>Appointment Time: </td>
                   <td><strong><%=sAppointmentTimeDayWeek + " - " + sAppointmentTime%></strong></td>
                 </tr>
                 <tr>
                   <td nowrap>Provider/Location: </td>
                   <td><strong><%=myPracM.getPracticeName()%></strong>
				</td>
                 </tr>
                 <tr>
                   <td nowrap>ScanPass: </td>
                   <td><strong><%=mySP%></strong></td>
                 </tr>
                 <tr>
                   <td>&nbsp;</td>
                   <td>&nbsp;</td>
                 </tr>
                 <tr>
                   <td colspan="2" align="center">
                     <input type="button" class="title" onClick="document.location='fileRetrieve.jsp'" value="View Report" />
                </td>
                 </tr>
                 <tr>
                   <td colspan="2" align="center">&nbsp;</td>
                 </tr>
                 <tr>
                   <td colspan="2" align="center">If you have any questions,<br>
                   please contact a NextImage<br>
                   Support Representative at<br><strong>(888) 318 - 5111</strong></td>
                 </tr>
        
               </table></td>
             </tr>
             <tr>
               <td align="center"><%String bnIncludeFN = "ui_200\\bot-nav_sched.jsp";
%><jsp:include page="<%=bnIncludeFN%>" flush="true" ></jsp:include></td>
             </tr>
           </table>




           <%
			

		}
		else
		{
		   %>
           <script language="javascript">
		   alert ("Invalid Password or ScanPass\n\nPlease Try Again\n\n\If you are still having trouble, please contact and NextImage Support Representative at 888-318-5111");
//		   history.go(-1);
           </script>
           <%
		}
	}
	catch(Exception e)
	{
		out.println(e);
		   %>
           <script language="javascript">
		   alert ("Invalid Password or ScanPass\n\nPlease Try Again\n\n\If you are still having trouble, please contact and NextImage Support Representative at 888-318-5111");
		   history.go(-1);
           </script>
           <%

	}

}

%>
<br>
<br>

