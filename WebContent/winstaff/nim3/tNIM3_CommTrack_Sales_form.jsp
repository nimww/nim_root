<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltDocumentManagement,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement, com.winstaff.PLCUtils, com.winstaff.*, com.winstaff.SecurityCheck, com.winstaff.ConfigurationMessages, com.winstaff.bltNIM3_CommTrack" %>

<link rel="stylesheet" type="text/css" href="../app/js/anytime/anytime.css" />
    <script type="text/javascript" src="../app/js/prototype/prototype-1.6.0.3.js" ></script>
    <script type="text/javascript" src="../app/js/anytime/anytime.js" ></script>

<%/*

    filename: out\jsp\tNIM3_CommTrack_form.jsp
    Created on Nov/16/2011
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_CaseID.jsp?plcID="+thePLCID;
%>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td>



<%
//initial declaration of list class and parentID
    Integer        iCommTrackID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    Integer        iLink_UserID        =    null;
    if ( request.getParameter( "Link_UserID" ) != null )
    {
    	iLink_UserID        =    new Integer(request.getParameter ("Link_UserID"));
    }
    else
    {
    	iLink_UserID        =    new Integer(0);
    }
    Integer        iEncounterID        =    null;
    if ( request.getParameter( "EncounterID" ) != null )
    {
    	iEncounterID        =    new Integer(request.getParameter ("EncounterID"));
    }
    else
    {
    	iEncounterID        =    new Integer(0);
    }
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iCommTrackID")) 
    {
        iCommTrackID        =    (Integer)pageControllerHash.get("iCommTrackID");
        accessValid = true;
        if (iCommTrackID.intValue() != iEDITID.intValue())
        {
        	accessValid = false;
        }
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("newsales")&&isReporter )
    {
			accessValid = true;
    }
  //page security
  if (accessValid)
  {
	java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
	java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);	java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_CommTrack_form.jsp?EDIT=edit&EDITID=" + iEDITID);
//initial declaration of list class and parentID

    bltNIM3_CommTrack        NIM3_CommTrack        =    null;

    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("edit") )
    {
        NIM3_CommTrack        =    new    bltNIM3_CommTrack(iCommTrackID,UserSecurityGroupID);
    }
    else if ( request.getParameter( "EDIT" ).equalsIgnoreCase("newsales") )
    {
        NIM3_CommTrack        =    new    bltNIM3_CommTrack(UserSecurityGroupID, true);
		NIM3_CommTrack.setLink_UserID(iLink_UserID);
    }

//fields
        %>
        <form action="tNIM3_CommTrack_form_sub.jsp" name="tNIM3_CommTrack_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
    %>
        <input type="hidden" name="EDITID" value = "<%=iEDITID%>" >  
        <input type="hidden" name="Link_UserID" value = "<%=iLink_UserID%>" >  
        <input type="hidden" name="EncounterID" value = "<%=iEncounterID%>" >  

          <%  String theClass ="tdBase";%>
<br />
        <table width=100% border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 class=tableBase>
         <tr>
           <td class=tableColor><span class="title">Interaction</span>
             <table cellpadding=0 cellspacing=0 width=100%>
    
            <%
            if ( (NIM3_CommTrack.isRequired("CommTypeID",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("CommTypeID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("CommTypeID",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("CommTypeID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
       <%
            if ((NIM3_CommTrack.isWrite("CommTypeID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Type&nbsp;</b></p></td><td valign=top><p><select name="CommTypeID" ><jsp:include page="../generic/tCommTrackTypeLILong_Sales.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_CommTrack.getCommTypeID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CommTypeID&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("CommTypeID")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_CommTrack.isRead("CommTypeID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Type</b></p></td><td valign=top><p><jsp:include page="../generic/tCommTrackTypeLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=NIM3_CommTrack.getCommTrackID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CommTypeID&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("CommTypeID")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("CommStart",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("CommStart")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("CommStart",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("CommStart",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>

    <%
            if ((NIM3_CommTrack.isWrite("CommStart",UserSecurityGroupID)))
            {
					String dNAD = "today";
					if (NIM3_CommTrack.getCommStart().before(NIMUtils.getBeforeTime()))
					{
						
					}
					else
					{
						dNAD = displayDateTimeSDF.format(NIM3_CommTrack.getCommStart());
					}
				
				
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start Date:</b></p></td><td valign=top><p>
                        
                            <input maxlength=40  type=text size="20" name="CommStart" id="CommStart"   value="<%=dNAD%>"   >   
                            
                            <script type="text/javascript">
    new ATWidget( "CommStart", { format: "%m/%d/%Y %h:%i %p" } );
                           </script>
                            
                        
<br />
Duration: <select name="ssDuration">
  <option value="5">5 min</option>
  <option value="10">10 min</option>
  <option value="15">15 min</option>
  <option value="30">30 min</option>
  <option value="60">1 hour</option>
  <option value="120">2 hours</option>
  <option value="300">5 hours</option>
</select>
                        
                        
                        
                        &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CommStart&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("CommStart")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_CommTrack.isRead("CommStart",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Start&nbsp;Date</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=displayDateTimeSDF.format(NIM3_CommTrack.getCommStart())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CommStart&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("CommStart")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (NIM3_CommTrack.isRequired("MessageSubject",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("MessageSubject")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("MessageSubject",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("MessageSubject",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_CommTrack.isWrite("MessageSubject",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b>Subject&nbsp;</b></p></td><td valign=top><p>
                         <input name="MessageSubject" type="text" onkeydown="textAreaStop(this,200)" value="<%=NIM3_CommTrack.getMessageSubject()%>" size="40" maxlength="200" />
                         &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageSubject&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageSubject")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_CommTrack.isRead("MessageSubject",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b>Subject&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getMessageSubject()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageSubject&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageSubject")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (NIM3_CommTrack.isRequired("MessageText",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("MessageText")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("MessageText",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("MessageText",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((NIM3_CommTrack.isWrite("MessageText",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b>Notes&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,5000)" rows="12" name="MessageText" cols="40"><%=NIM3_CommTrack.getMessageText()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageText&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageText")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_CommTrack.isRead("MessageText",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b>Notes&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getMessageText()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=MessageText&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("MessageText")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (NIM3_CommTrack.isRequired("Comments",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("Comments")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("Comments",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("Comments",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_CommTrack.isWrite("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b><%=NIM3_CommTrack.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,200)" rows="2" name="Comments" cols="40" maxlength=200><%=NIM3_CommTrack.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(NIM3_CommTrack.isRead("Comments",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top> <p class=<%=theClass%> ><b><%=NIM3_CommTrack.getEnglish("Comments")%>&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("Comments")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (NIM3_CommTrack.isRequired("Link_UserID",UserSecurityGroupID))&&(!NIM3_CommTrack.isComplete("Link_UserID")) )
            {
                theClass = "requiredField";
            }
            else if ((NIM3_CommTrack.isExpired("Link_UserID",expiredDays))&&(NIM3_CommTrack.isExpiredCheck("Link_UserID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(NIM3_CommTrack.isWrite("Link_UserID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Link_UserID&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="5" name="Link_UserID" value="<%=NIM3_CommTrack.getLink_UserID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Link_UserID&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("Link_UserID")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((NIM3_CommTrack.isRead("Link_UserID",UserSecurityGroupID)))
            {
                        %>
                       <tr><td valign=top><p class=<%=theClass%> ><b>Link_UserID&nbsp;</b></p></td><td valign=top><p><%=NIM3_CommTrack.getLink_UserID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Link_UserID&amp;sTableName=tNIM3_CommTrack&amp;sRefID=<%=NIM3_CommTrack.getCommTrackID()%>&amp;sFieldNameDisp=<%=NIM3_CommTrack.getEnglish("Link_UserID")%>&amp;sTableNameDisp=tNIM3_CommTrack','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>

			<tr><td colspan="2">&nbsp;</td></tr>
			<%if (CurrentUserAccount.getUserID() == 18920){ %>
			<tr><td colspan="2"><strong>Write Email</strong></td></tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr><td><strong>Email To:</strong></td><td><input name="EmailTo" value="<%=new bltUserAccount(NIM3_CommTrack.getLink_UserID()).getContactEmail() %>" readonly size="55" ></td></tr>
			<tr><td><strong>Email From:</strong></td><td><input name="EmailFrom" value="<%=CurrentUserAccount.getContactEmail()%>" readonly size="55" ></td></tr>
			
			<tr><td><strong>Email Subject:</strong></td><td><input name="EmailSubject" type="text" onkeydown="textAreaStop(this,200)" size="40" maxlength="200" /></td></tr>
			<tr><td><strong>Email Body:</strong></td><td><textarea onKeyDown="textAreaStop(this,5000)" rows="12" name="EmailBody" cols="40"></textarea></td></tr>
			<%} %>


            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <p><input name=Submit type=Submit class="inputButton_md_Create" onClick = "this.disabled=true;submit();" value="Save"></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_CaseID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>