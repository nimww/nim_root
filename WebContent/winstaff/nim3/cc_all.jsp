<%@page contentType="text/html" language="java" import="com.winstaff.*,java.util.*"%>
<%@ include file="../generic/CheckLogin.jsp"%>

<%!

/**
*
* return 3 = email attachment, 4 = fax, 1 = no alerts
*/
public static int getPref(bltUserAccount ua, int scanpass_type ){
	if (scanpass_type == 1){
		return ua.getComm_Alerts2_LevelUser();
	} else if (scanpass_type == 2){
		return ua.getComm_Alerts_LevelUser();
	} else if (scanpass_type == 3){
		return ua.getComm_Report_LevelUser();
	}
	return 1;
	
}

public static boolean validateEmail(int userid){
	bltUserAccount ua = new bltUserAccount(userid);
	if(ua.getContactEmail().contains("DECLINE")){
	return false;		
	}
	return true;
}
public static String getAlert(int scanpass_type, int pref, int recepientsID){
	/* recepientsID
	1 "adj", 
 	2"ncm", 
	3"md",
	4"caseAdmin",
	5"caseAdmin2",
	6"caseAdmin3",
	7"caseAdmin4" */
	
	if (scanpass_type ==1){
		if (recepientsID ==1){
			if (pref==3){
				return "email_rc_adj";
			} else return "fax_rc_adj";
		} else if (recepientsID ==2){
			if (pref==3){
				return "email_rc_ncm";
			} else return "fax_rc_ncm";
		} else if (recepientsID ==3){
			if (pref==3){
				return "email_rc_refdr";
			} else return "fax_rc_refdr";
		} else if (recepientsID ==4){
			if (pref==3){
				return "email_rc_adm";
			} else return "fax_rc_adm";
		} else if (recepientsID ==5){
			if (pref==3){
				return "email_rc_adm2";
			} else return "fax_rc_adm2";
		} else if (recepientsID ==6){
			if (pref==3){
				return "email_rc_adm3";
			} else return "fax_rc_adm3";
		} else if (recepientsID ==7){
			if (pref==3){
				return "email_rc_adm4";
			} else return "fax_rc_adm4";
		}
	} else if (scanpass_type == 2){
		if (recepientsID ==1){
			if (pref==3){
				return "email_sp_adj";
			} else return "fax_sp_adj";
		} else if (recepientsID ==2){
			if (pref==3){
				return "email_sp_ncm";
			} else return "fax_sp_ncm";
		} else if (recepientsID ==3){
			if (pref==3){
				return "email_sp_refdr";
			} else return "fax_sp_refdr";
		} else if (recepientsID ==4){
			if (pref==3){
				return "email_sp_adm";
			} else return "fax_sp_adm";
		} else if (recepientsID ==5){
			if (pref==3){
				return "email_sp_adm2";
			} else return "fax_sp_adm2";
		} else if (recepientsID ==6){
			if (pref==3){
				return "email_sp_adm3";
			} else return "fax_sp_adm3";
		} else if (recepientsID ==7){
			if (pref==3){
				return "email_sp_adm4";
			} else return "fax_sp_adm4";
		}
	} else if (scanpass_type == 3){
		if (recepientsID ==1){
			if (pref==3){
				return "emaila_rp_adj";
			} else return "fax_rp_adj";
		} else if (recepientsID ==2){
			if (pref==3){
				return "emaila_rp_ncm";
			} else return "fax_rp_ncm";
		} else if (recepientsID ==3){
			if (pref==3){
				return "emaila_rp_refdr";
			} else return "fax_rp_refdr";
		} else if (recepientsID ==4){
			if (pref==3){
				return "emaila_rp_adm";
			} else return "fax_rp_adm";
		} else if (recepientsID ==5){
			if (pref==3){
				return "emaila_rp_adm2";
			} else return "fax_rp_adm2";
		} else if (recepientsID ==6){
			if (pref==3){
				return "emaila_rp_adm3";
			} else return "fax_rp_adm3";
		} else if (recepientsID ==7){
			if (pref==3){
				return "emaila_rp_adm4";
			} else return "fax_rp_adm4";
		}
	}
	return "no";
}


public static String getRecepients (int recepientsID, NIM3_EncounterObject2 eo2, int scanpass_type){
	/* recepientsID
	1 "adj", 
 	2"ncm", 
	3"md",
	4"caseAdmin",
	5"caseAdmin2",
	6"caseAdmin3",
	7"caseAdmin4" */
	
	String returnValue = "no";
	switch (recepientsID){
	case 1 : 
		if(eo2.getNIM3_CaseAccount().getAdjusterID() > 0 && getPref(eo2.getCase_Adjuster(), scanpass_type)!=1){
			returnValue = getAlert(scanpass_type, getPref(eo2.getCase_Adjuster(), scanpass_type),recepientsID ); break;
		} else break;
		
	case 2 : 
		if(eo2.getNIM3_CaseAccount().getNurseCaseManagerID() > 0 && getPref(eo2.getCase_NurseCaseManager(), scanpass_type)!=1){
			returnValue = getAlert(scanpass_type, getPref(eo2.getCase_NurseCaseManager(), scanpass_type),recepientsID ); break;
		} else break;
	
	case 3 : 
		if(eo2.getNIM3_Referral().getReferringPhysicianID() > 0 && getPref(eo2.getReferral_ReferringDoctor(), scanpass_type)!=1){
			returnValue = getAlert(scanpass_type, getPref(eo2.getReferral_ReferringDoctor(), scanpass_type),recepientsID ); break;
		} else break;
		
	case 4 : 
		if(eo2.getNIM3_CaseAccount().getCaseAdministratorID() > 0 && getPref(eo2.getCase_Admin(), scanpass_type)!=1){
			returnValue = getAlert(scanpass_type, getPref(eo2.getCase_Admin(), scanpass_type),recepientsID ); break;
		} else break;
		
	case 5 : 
		if(eo2.getNIM3_CaseAccount().getCaseAdministrator2ID() > 0 && getPref(eo2.getCase_Admin2(), scanpass_type)!=1){
			returnValue = getAlert(scanpass_type, getPref(eo2.getCase_Admin2(), scanpass_type),recepientsID ); break;
		} else break;
		
	case 6 : 
		if(eo2.getNIM3_CaseAccount().getCaseAdministrator3ID() > 0 && getPref(eo2.getCase_Admin3(), scanpass_type)!=1){
			returnValue = getAlert(scanpass_type, getPref(eo2.getCase_Admin3(), scanpass_type),recepientsID ); break;
		} else break;
		
	case 7 : 
		if(eo2.getNIM3_CaseAccount().getCaseAdministrator4ID() > 0 && getPref(eo2.getCase_Admin4(), scanpass_type)!=1){
			returnValue = getAlert(scanpass_type, getPref(eo2.getCase_Admin4(), scanpass_type),recepientsID ); break;
		} else break;
	} 
	
	return returnValue;
}

%>

<%	
	/*
	 scanpass_type:
		1 - rc
		2 - sp
		3 - rp
	*/
	

	List<String> list_of_id = new ArrayList<String>();

	int scanpass_type = new Integer(request.getParameter("scanpass_type"));
	int eID = new Integer(request.getParameter("eID"));

	NIM3_EncounterObject2 eo2 = new NIM3_EncounterObject2(eID, "");

		for (int x = 1; x<8;x++){
			String tmp = getRecepients(x,eo2,scanpass_type);
			if(!tmp.equals("no")){
				
				list_of_id.add(tmp);
			}
		}
		if (scanpass_type==2){
			list_of_id.add("fax_sp_ic");
		}
		for (String sFlag : list_of_id) {
			NIMUtils.sendAlertEmail(eID, sFlag, "", false, CurrentUserAccount.getContactFirstName() + " " + CurrentUserAccount.getContactLastName());
		}

	//NIMUtils.sendAlertEmail(iEncounterID,sFlag,text,customEmail,CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName());
%>