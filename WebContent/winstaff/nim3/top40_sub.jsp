<%@page import="com.winstaff.*"%>
<html>
<head>
<title>Top 40</title>
<link href="https://netdna.bootstrapcdn.com/bootswatch/2.3.1/flatly/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>Scanpass</th>
				<th>Receive Date</th>
				<th>Status</th>
				<th>Payer Name</th>

			</tr>
		</thead>
		<tbody>
			<%!public String translateValue(String param) {
		String value = "";

		if (param.equals("four")) {
			value = " - '4 month'::interval";
		} else if (param.equals("three")) {
			value = " - '3 month'::interval";
		} else if (param.equals("two")) {
			value = " - '2 month'::interval";
		} else if (param.equals("one")) {
			value = " - '1 month'::interval";
		}

		return value;
	}%>
			<%
				String query = "select \"Encounter_ScanPass\",\"Referral_ReceiveDate\", \"Encounter_IsCourtesy\", \"Encounter_Status\",  \"Encounter_StatusID\",\"PayerID\", \"PayerName\", \"Parent_PayerID\"  from \"vMQ4_Encounter_NoVoid_BP\"  where \"Referral_ReceiveDate_iMonth\"=extract(month from now() " + translateValue(request.getParameter("view"))
						+ ") and \"Referral_ReceiveDate_iYear\"=2013 and \"Parent_PayerID\" = " + request.getParameter("pid") + " and \"Encounter_IsCourtesy\" != 1";

				searchDB2 conn = new searchDB2();
				java.sql.ResultSet rs = conn.executeStatement(query);

				while (rs.next()) {
			%>
			<tr>
				<td><%=rs.getString("Encounter_ScanPass")%></td>
				<td><%=rs.getString("Referral_ReceiveDate")%></td>
				<td><%=rs.getString("Encounter_Status")%></td>
				<td><%=rs.getString("PayerName")%></td>
			</tr>
			<%
				}

				conn.closeAll();
			%>
		</tbody>
	</table>

</body>
</html>