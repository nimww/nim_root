



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*    " %>
<%/*
    filename: out\jsp\tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_create.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPayerID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
    Integer iPID = new Integer(0);
    String sCN = null;
    Integer iAID = new Integer(0);
	String editType = "";
	if (request.getParameter("CN")!=null&&request.getParameter("PID")!=null)
	{
		iPID = new Integer(request.getParameter("PID"));
		sCN = request.getParameter("CN");
	}
	else if (request.getParameter("CN")!=null&&request.getParameter("AID")!=null)
	{
		iAID = new Integer(request.getParameter("AID"));
		bltUserAccount uaAdj = new bltUserAccount(iAID);
		iPID = uaAdj.getPayerID();
		sCN = request.getParameter("CN");
	}
	else
	{
		//createID = new Integer(0);
		accessValid = false;
	}
  //page security
  if (accessValid&&iPID.intValue()>0)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
        }
        if (request.getParameter("EDIT")!=null)
        {
            editType = (request.getParameter("EDIT"));
        }

//declaration of Enumeration
    bltNIM3_CaseAccount        working_bltNIM3_CaseAccount = new bltNIM3_CaseAccount();
    working_bltNIM3_CaseAccount.setPayerID(iPID);
    working_bltNIM3_CaseAccount.setAdjusterID(iAID);
    working_bltNIM3_CaseAccount.setCaseClaimNumber(sCN);
    working_bltNIM3_CaseAccount.setCaseStatusID(new Integer(1));
	if (CurrentUserAccount.getAccountType().equalsIgnoreCase("PhysicianID"))
	{
//	    working_bltNIM3_CaseAccount.setPayerID(new Integer(2));    //Hard Coded for POMCO
	}
    working_bltNIM3_CaseAccount.setUniqueCreateDate(PLCUtils.getNowDate(false));
    working_bltNIM3_CaseAccount.setUniqueModifyDate(PLCUtils.getNowDate(false));
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltNIM3_CaseAccount.setUniqueModifyComments(""+UserLogonDescription);
    }

            working_bltNIM3_CaseAccount.commitData();
//validate across all forms of creation
working_bltNIM3_CaseAccount.setCaseCode("CC" +  working_bltNIM3_CaseAccount.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
			String tempAN = working_bltNIM3_CaseAccount.getAuditNotes() + "\n[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nCreated";
			working_bltNIM3_CaseAccount.setAuditNotes(tempAN);
			working_bltNIM3_CaseAccount.setCaseStatusID(new Integer(1));
			working_bltNIM3_CaseAccount.setAssignedToID(CurrentUserAccount.getUserID());
            working_bltNIM3_CaseAccount.commitData();
			if (CurrentUserAccount.getAccountType().equalsIgnoreCase("PhysicianID"))
			{
				bltNIM3_Referral  NIMR = new bltNIM3_Referral();
				NIMR.setCaseID(working_bltNIM3_CaseAccount.getUniqueID());
				NIMR.setReferringPhysicianID(CurrentUserAccount.getUniqueID());
				tempAN = "[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nCreated";
				NIMR.setAuditNotes(tempAN);
				NIMR.commitData();
			}
			String newExpress = working_bltNIM3_CaseAccount.getUniqueID().toString();		
			if(editType.equalsIgnoreCase("newexpress"))
			{
				bltNIM3_Referral  NIMR = new bltNIM3_Referral();
				NIMR.setCaseID(working_bltNIM3_CaseAccount.getUniqueID());
				NIMR.setReferralTypeID(new Integer(3));
				//NIMR.setReferralStatusID(new Integer(1));
				NIMR.setAttendingPhysicianID(new Integer(52));
				tempAN = "[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nCreated (express)";
				NIMR.setAuditNotes(tempAN);
				NIMR.commitData();
				pageControllerHash.put("iReferralID",NIMR.getUniqueID());

				bltNIM3_Encounter  NIME = new bltNIM3_Encounter();

				NIME.setReferralID(NIMR.getUniqueID());
				NIME.setEncounterStatusID(new Integer(1));
				NIME.setEncounterTypeID(new Integer(9));
//				NIME.setReferringPhysicianID(CurrentUserAccount.getUniqueID());
				tempAN = "[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nCreated (express)";
				NIME.setAuditNotes(tempAN);
				NIME.commitData();
				NIME.setScanPass("SP" +  NIME.getUniqueID() + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());								
				NIME.commitData();
				newExpress = NIME.getUniqueID().toString();
				pageControllerHash.put("iEncounterID",NIME.getUniqueID());

			}
			pageControllerHash.put("iCaseID",working_bltNIM3_CaseAccount.getUniqueID());
			//pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
			session.setAttribute("pageControllerHash",pageControllerHash);
			//Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
String myName = (String)myParameterPassList.nextElement();
String myS = (String) request.getParameter(myName);
parameterPassString+="&"+myName + "=" + myS;
}
			String targetRedirect = "tNIM3_CaseAccount_form.jsp?nullParam=null"+parameterPassString    ;

			if(editType.equalsIgnoreCase("newexpress"))
			{
				targetRedirect = "tNIM3_CaseAccount_express1_form.jsp?EDITID="+newExpress+"&routePageReference=sParentReturnPage&EDIT=edit"+parameterPassString    ;
			}
			else
			{
				targetRedirect = "tNIM3_CaseAccount_form.jsp?EDITID="+newExpress+"&routePageReference=sParentReturnPage&EDIT=edit"+parameterPassString    ;
			}
			response.sendRedirect(targetRedirect);

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>





