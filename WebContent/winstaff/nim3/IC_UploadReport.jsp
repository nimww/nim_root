<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.* " %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_icid.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
    <table cellpadding=0 cellspacing=0 border=0  >
    <tr><td width=10>&nbsp;</td><td>
  <%
	//initial declaration of list class and parentID
    boolean accessValid = false;
    Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
    String ENCID = null;
	NIM3_EncounterObject myEO = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isIC&&request.getParameter("EDITID")!=null) 
    {
		ENCID = request.getParameter("EDITID");
		if (NIMUtils.isICIDAuthorizedToUploadReport(CurrentUserAccount.getReferenceID(),new Integer(ENCID)))
		{
			myEO = new NIM3_EncounterObject(new Integer(ENCID));
	        accessValid = true;
		}
    }
  //page security
  if (accessValid)
  {
%>
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td class="tdHeader">Report - Upload  for Patient: <%=myEO.NIM3_CaseAccount.getPatientLastName()%>, <%=myEO.NIM3_CaseAccount.getPatientFirstName()%></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><form action="IC_UploadReport_sub.jsp" method="post" enctype="multipart/form-data">
        <p>
          <input type="file" name="fileData"/>
          <input type="hidden" name="EDIT" value="<%=ENCID%>"/>
        </p>
        <p>
<input type="submit" onClick="this.disabled=true;submit();" value="Upload" />          
</p>
      </form>      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>Please submit document as a PDF. </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>All information is secured through SSL Encryption.</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    </table>
<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>