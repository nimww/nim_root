<%@page import="java.io.*,com.winstaff.Top40Report.*,com.winstaff.*, java.util.*;"%>
<!doctype>
<html>
<head>
<title>Top 40</title>
<link href="https://netdna.bootstrapcdn.com/bootswatch/2.3.1/flatly/bootstrap.min.css" rel="stylesheet">
</head>

<body>
	<%
		java.util.Calendar calendar = java.util.Calendar.getInstance();

		calendar.add(java.util.Calendar.MONTH, -4);
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("MMMM");

		String fourmonthago = sdf.format(calendar.getTime());
		calendar.add(java.util.Calendar.MONTH, 1);

		String threemonthago = sdf.format(calendar.getTime());
		calendar.add(java.util.Calendar.MONTH, 1);

		String twomonthago = sdf.format(calendar.getTime());
		calendar.add(java.util.Calendar.MONTH, 1);

		String onemonthago = sdf.format(calendar.getTime());
		calendar.add(java.util.Calendar.MONTH, 1);

		String thismonth = sdf.format(calendar.getTime());
		calendar.add(java.util.Calendar.MONTH, 1);
		
		List<Top40Model> top = Top40Report.getTop40();
	%>
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>Payer Name</th>
				<th><%=fourmonthago%></th>
				<th><%=threemonthago%></th>
				<th><%=twomonthago%></th>
				<th><%=onemonthago%></th>
				<th><%=thismonth%></th>
				<th><%=thismonth%> Projected</th>
				<th>Change</th>
				<th>Trailing 3 months</th>
			</tr>
		</thead>
		<tbody>
			<%
				for(int x = 0; x < 40; x++){
			%>
			<tr>
				<td><%=top.get(x).getPayername()%></td>
				<td><a href="top40_sub.jsp?pid=<%=top.get(x).getPayerid()%>&view=four"><%=top.get(x).getFourmonthago()%></a></td>
				<td><a href="top40_sub.jsp?pid=<%=top.get(x).getPayerid()%>&view=three"><%=top.get(x).getThreemonthago()%></a></td>
				<td><a href="top40_sub.jsp?pid=<%=top.get(x).getPayerid()%>&view=two"><%=top.get(x).getTwomonthago()%></a></td>
				<td><a href="top40_sub.jsp?pid=<%=top.get(x).getPayerid()%>&view=one"><%=top.get(x).getLastmonth()%></a></td>
				<td><a href="top40_sub.jsp?pid=<%=top.get(x).getPayerid()%>&view=last"><%=top.get(x).getThismonth()%></a></td>
				<td><%=top.get(x).getProjected()%></td>
				<td><%=top.get(x).getChange()%></td>
				<td><%=top.get(x).getTrailing()%></td>
			</tr>
			<%
				}
			%>
			<tr>
				<td><strong>TOTAL TOP 40</strong></td>
				<td><strong><%=Top40Report.getTopNumber(top,40,Top40Report.t4)%></strong></td>
				<td><strong><%=Top40Report.getTopNumber(top,40,Top40Report.t3)%></strong></td>
				<td><strong><%=Top40Report.getTopNumber(top,40,Top40Report.t2)%></strong></td>
				<td><strong><%=Top40Report.getTopNumber(top,40,Top40Report.t1)%></strong></td>
				<td><strong><%=Top40Report.getTopNumber(top,40,Top40Report.t0)%></strong></td>
				<td><strong><%=Top40Report.getTopNumber(top,40,Top40Report.p)%></strong></td>
				<td><strong><%=String.format("%.1f",((float)Top40Report.getTopNumber(top,40,Top40Report.p)/(float)Top40Report.getTopNumber(top,40,Top40Report.t1))*100-100)%>%</strong></td>
				<td><strong><%=String.format("%.1f",((float)Top40Report.getTopNumber(top,40,Top40Report.p)/(((float)Top40Report.getTopNumber(top,40,Top40Report.t1)+(float)Top40Report.getTopNumber(top,40,Top40Report.t2)+(float)Top40Report.getTopNumber(top,40,Top40Report.t3))/3))*100-100)%>%</strong></td>
			</tr>
			<tr>
				<td>Total Top 10</td>
				<td><%=Top40Report.getTopNumber(top,10,Top40Report.t4)%></td>
				<td><%=Top40Report.getTopNumber(top,10,Top40Report.t3)%></td>
				<td><%=Top40Report.getTopNumber(top,10,Top40Report.t2)%></td>
				<td><%=Top40Report.getTopNumber(top,10,Top40Report.t1)%></td>
				<td><%=Top40Report.getTopNumber(top,10,Top40Report.t0)%></td>
				<td><%=Top40Report.getTopNumber(top,10,Top40Report.p)%></td>
				<td><%=String.format("%.1f",((float)Top40Report.getTopNumber(top,10,Top40Report.p)/(float)Top40Report.getTopNumber(top,10,Top40Report.t1))*100-100)%>%</td>
				<td><%=String.format("%.1f",((float)Top40Report.getTopNumber(top,10,Top40Report.p)/(((float)Top40Report.getTopNumber(top,10,Top40Report.t1)+(float)Top40Report.getTopNumber(top,10,Top40Report.t2)+(float)Top40Report.getTopNumber(top,10,Top40Report.t3))/3))*100-100)%>%</td>
			</tr>
			<tr>
				<td>Total Top 25</td>
				<td><%=Top40Report.getTopNumber(top,25,Top40Report.t4)%></td>
				<td><%=Top40Report.getTopNumber(top,25,Top40Report.t3)%></td>
				<td><%=Top40Report.getTopNumber(top,25,Top40Report.t2)%></td>
				<td><%=Top40Report.getTopNumber(top,25,Top40Report.t1)%></td>
				<td><%=Top40Report.getTopNumber(top,25,Top40Report.t0)%></td>
				<td><%=Top40Report.getTopNumber(top,25,Top40Report.p)%></td>
				<td><%=String.format("%.1f",((float)Top40Report.getTopNumber(top,25,Top40Report.p)/(float)Top40Report.getTopNumber(top,25,Top40Report.t1))*100-100)%>%</td>
				<td><%=String.format("%.1f",((float)Top40Report.getTopNumber(top,25,Top40Report.p)/(((float)Top40Report.getTopNumber(top,25,Top40Report.t1)+(float)Top40Report.getTopNumber(top,25,Top40Report.t2)+(float)Top40Report.getTopNumber(top,25,Top40Report.t3))/3))*100-100)%>%</td>
			</tr>
			<tr>
				<td><strong>Net Monthly Volume</strong></td>
				<td><strong><%=Top40Report.getTopNumber(top,top.size(),Top40Report.t4)%></strong></td>
				<td><strong><%=Top40Report.getTopNumber(top,top.size(),Top40Report.t3)%></strong></td>
				<td><strong><%=Top40Report.getTopNumber(top,top.size(),Top40Report.t2)%></strong></td>
				<td><strong><%=Top40Report.getTopNumber(top,top.size(),Top40Report.t1)%></strong></td>
				<td><strong><%=Top40Report.getTopNumber(top,top.size(),Top40Report.t0)%></strong></td>
				<td><%=Top40Report.getTopNumber(top,top.size(),Top40Report.p)%></td>
				<td><strong><%=String.format("%.1f",((float)Top40Report.getTopNumber(top,top.size(),Top40Report.p)/(float)Top40Report.getTopNumber(top,top.size(),Top40Report.t1))*100-100)%>%</strong></td>
				<td><strong><%=String.format("%.1f",((float)Top40Report.getTopNumber(top,top.size(),Top40Report.p)/(((float)Top40Report.getTopNumber(top,top.size(),Top40Report.t1)+(float)Top40Report.getTopNumber(top,top.size(),Top40Report.t2)+(float)Top40Report.getTopNumber(top,top.size(),Top40Report.t3))/3))*100-100)%>%</strong></td>
			</tr>
		</tbody>
	</table>
</body>
</html>