
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td><hr></td></tr>
    <tr><td width=10>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM_Encounter")%>



<%
//initial declaration of list class and parentID
   Integer        iEncounterID        =    null;
   java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
   java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat("MM/dd/yyyy");
   boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
		String sFlag = request.getParameter("EDIT");
		emailType_V3 myETSend = NIMUtils.getAlertEmail(iEncounterID, sFlag);
		myETSend.cc = new java.util.Vector();
		myETSend.cc.add("scottellis@gmail.com");
		myETSend.cc.add("scott.ellis@gmail.com");
		myETSend.cc.add("server@gmail.com");
		myETSend.setTo("scott@nextimagemedical.com");
//		myETSend.setTo("18006375164@efaxsend.com");
//		myETSend.setTo("18588479135@efaxsend.com");
		myETSend.setSubject("DEV TESTING: " + myETSend.getSubject());
//		myETSend.setFrom("andy@nextimagemedical.com");
		myETSend.setFrom("server@nextimagemedical.com");
		myETSend.isSendMail_Direct2();
											 
out.print(sFlag);

%>
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td><h1> Alert Sent to: <%=myETSend.getTo()%> </h1></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><script language="javascript">//window.close();</script></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr> 
    </table>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
