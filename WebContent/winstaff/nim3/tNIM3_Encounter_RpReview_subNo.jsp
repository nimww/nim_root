<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.* " %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />
      
      
      
  <%
//initial declaration of list class and parentID
    Integer        iReferralID        =    null;
    Integer        iEncounterID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);

   if (iSecurityCheck.intValue()!=0)
   {
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
	    iEncounterID = (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
    else if (pageControllerHash.containsKey("iReferralID")) 
    {
	    iReferralID = (Integer)pageControllerHash.get("iReferralID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
	  String myReportType = request.getParameter("EDIT");
	  bltNIM3_Encounter myE = new bltNIM3_Encounter(iEncounterID);
	  //if (myE.getEncounterStatusID().intValue()==8)
	  {
		  myE.setEncounterStatusID(new Integer(11));
	  }
	  bltNIM3_CommTrack myCT = new bltNIM3_CommTrack();
	  myCT.setReferralID(myE.getReferralID());
	  myCT.setCommStart(PLCUtils.getNowDate(false));
	  myCT.setCommEnd(PLCUtils.getNowDate(false));
	  myCT.setAlertStatusCode(new Integer(1));
	  myCT.setEncounterID(myE.getEncounterID());
	  myCT.setMessageName(CurrentUserAccount.getContactLastName() + " " + CurrentUserAccount.getContactFirstName() + " [" + CurrentUserAccount.getLogonUserName() + " ("+CurrentUserAccount.getUserID()+") ]");
	  if (myE.getTimeTrack_ReqRpReview().after(new java.util.Date(1)))
	  {
		  myCT.setMessageText("Escalated Report-Review/Approval Requested on: " + PLCUtils.getNowDate(false) + " || " + "Previous Approval by User (" +  myE.getTimeTrack_ReqRpReview_UserID() + ") on: " + myE.getTimeTrack_ReqRpReview() );
	  }
	  else
	  {
		  myCT.setMessageText("Escalated Report-Review/Approval Requested on: " + PLCUtils.getNowDate(false) + " || " + "No Prior Approval" );
	  }
	  myCT.commitData();
	  myCT.setUniqueModifyComments(UserLogonDescription);
	  myE.setTimeTrack_ReqRpReview(PLCUtils.getSubDate(""));
	  myE.setTimeTrack_ReqRpReview_UserID(new Integer(0));
	  myE.setUniqueModifyComments(UserLogonDescription);
	  myE.commitData();
	  %>
      <script language="javascript">
	  window.close();
	  </script>
      <%
	  
//out.println(iAuthID);
%>
<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>
