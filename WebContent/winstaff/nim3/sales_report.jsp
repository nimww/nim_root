<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
<%
String payerName = "";
String adjuster = "";
String dateReferred = "";
String payerNameClause = "";
String adjusterClause = "";
String dateReferredClause = " and \"Referral_ReceiveDate\" > '2012-01-01' ";
boolean isFirst = false;

if (request.getParameter("payerName") != null){
	payerName = request.getParameter("payerName");
	if ((!request.getParameter("payerName").equals("0"))){payerNameClause = " and \"PayerName\" = '"+payerName+"' ";}
}
if (request.getParameter("adjuster") != null){
	adjuster = request.getParameter("adjuster");
	if ((!request.getParameter("adjuster").equals("0"))){adjusterClause = " and \"UA_Adjuster_UserID\" = '"+adjuster+"' ";}
}
if (request.getParameter("dateReferred") != null && request.getParameter("dateReferred") != "" ){
	dateReferred = request.getParameter("dateReferred");
	dateReferredClause = " and \"Referral_ReceiveDate\" > '"+dateReferred+"' ";
}
try{
if (request.getParameter("isFirst").equals("true")){
	isFirst = true;
}
}
catch(Exception e){}


%>
</head>

<body>
<%String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?iDelay=-1&plcID="+thePLCID;%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

	<%boolean accessValid = false;
	Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
	if (iSecurityCheck.intValue()!=0)
	{
		if (isScheduler) 
		{
			accessValid = true;
		}
		
		//page security
		if (accessValid)
		{%>
		<%!
		public String optionGen(int type, String selected){
			String returnString = "<option value=\"0\">Show All</option>";
			try{
			searchDB2 connection = new searchDB2();
			String query = "";
			switch(type){
				case 1: query = "select payername, payerid from tnim3_payermaster where lower(payername) not SIMILAR to 'zz%|0|rem%|' and parentpayerid !=0 order by payername"; 
				break;
				case 2: query = "select contactfirstname||' '||contactlastname||', '||payername \"contact\", userid from tuseraccount inner join tnim3_payermaster on tnim3_payermaster.payerid = tuseraccount.payerid where lower(accounttype) like 'adjuster%' and lower(contactfirstname) not similar to '|xx%|rem%| |tbd|zz%|archive%|test%' and lower(contactlastname) not similar to '|xx%|rem%| |tbd|zz%|archive%|test%' order by contactfirstname"; 
				break;
			}
			java.sql.ResultSet results = connection.executeStatement(query);
			
			while (results.next())
			{
				switch(type){
				case 1: 
				if(results.getString("payername").equals(selected)){
				returnString +="<option value=\""+results.getString("payername")+"\" selected>"+results.getString("payername")+"</option>";	
				}
				else {returnString +="<option value=\""+results.getString("payername")+"\">"+results.getString("payername")+"</option>";}
				
				
				break;
				case 2: 
				if(results.getString("userid").equals(selected)){
				returnString +="<option value=\""+results.getString("userid")+"\" selected>"+results.getString("contact")+"</option>";	
				}
				else {returnString +="<option value=\""+results.getString("userid")+"\">"+results.getString("contact")+"</option>";}
				
				break;
				}
			}
			connection.closeAll();
			
			return returnString;
			}
			catch(Exception e){return returnString;}
		}
		
		%> 	
		<form action="sales_report.jsp" method="post">
        	Payer Name: 
            <select name="payerName">
                <%=optionGen(1, payerName)%>
            </select>
            <br>
            Adjuster:  
            <select name="adjuster">
                <%=optionGen(2, adjuster)%>
            </select>
            <br>
            Date Referred (yyyy-mm-dd): 
            <input type="text" name="dateReferred" value="<%=dateReferred%>">
            <input name="isFirst" type="hidden" value="true">
        	<input type="submit">
        </form>	
        
		<% if (isFirst){%>	
		<table style="border:1px solid">
        	<tr>
        	<th style="border:1px solid">Payer Name</th>
        	<th style="border:1px solid">Employer</th>
            <th style="border:1px solid">Parent Payer ID</th>
            <th style="border:1px solid">Assigned To</th>
            <th style="border:1px solid">ScanPass</th>
            <th style="border:1px solid">Adjuster</th>
            <th style="border:1px solid">AdjusterPhone</th>
            <th style="border:1px solid">Referral Method</th>
            <th style="border:1px solid">Is Stat</th>
            <th style="border:1px solid">Rec Date</th>
            <th style="border:1px solid">Sent Rp To Adj</th>
            <th style="border:1px solid">Type</th>
            <th style="border:1px solid">Claim  Number</th>
            <th style="border:1px solid">Date Scheduled</th>
            <th style="border:1px solid">Time To Schedule</th>
            <th style="border:1px solid">DOS</th>
            <th style="border:1px solid">Time To MR</th>
            <th style="border:1px solid">Status</th>
            <th style="border:1px solid">Time Track</th>
            <th style="border:1px solid">Referral ID</th>
            <th style="border:1px solid">Referral Source</th>
            <th style="border:1px solid">Referral Source Payor</th>
            <th style="border:1px solid">Referral Source Phone</th>
            <th style="border:1px solid">NCM</th>
            <th style="border:1px solid">NCM Email</th>
            <th style="border:1px solid">NCM Phone</th>
            <th style="border:1px solid">Case Admin First Name</th>
            <th style="border:1px solid">Case Admin Last Name</th>
            <th style="border:1px solid">Case Admin 2</th>
            <th style="border:1px solid">Case Admin 3</th>
            <th style="border:1px solid">Physician Name</th>
            <th style="border:1px solid">MD Payer Group</th>
            <th style="border:1px solid">Physician Phone</th>
            <th style="border:1px solid">Service first CPT Body Part</th>
            <th style="border:1px solid">IC</th>
            <th style="border:1px solid">IC City</th>
            <th style="border:1px solid">IC State</th>
            <th style="border:1px solid">Courtesy</th>
            <th style="border:1px solid">Retro</th>
            <th style="border:1px solid">Pricing Structure</th>
            <th style="border:1px solid">Void</th>
            </tr>
            <%
			
			String query = "select \"PayerName\", case when caseaccount.employerlocationid != 0 then emploc.contactfirstname||' '||emploc.contactlastname else '' end as \"EmployerLocation\", caseaccount.employername \"Employer\", parentpayer.parentpayerid \"ParentPayerID\", \"UA_AssignedTo_FirstName\"||' '||\"UA_AssignedTo_LastName\" \"AssignedTo\", scanpass \"ScanPass\", \"UA_Adjuster_FirstName\"||' '||\"UA_Adjuster_LastName\" \"Adjuster\", adj.contactphone \"AdjusterPhone\", case when referral.referralmethod = '' then '' else referral.referralmethod end as \"ReferralMethod\", case when isstat = 1 then 'Yes' else '' end as \"IsStat\", to_char(\"Referral_ReceiveDate\", 'mm/dd/yyyy HH24:mi' )\"RecDate\", case when cast(encounter.senttoadj as VARCHAR) like '1800%' then '--'  else to_char(encounter.senttoadj, 'mm/dd/yyyy HH24:mi') end \"SentRpToAdj\", \"Encounter_Type\" \"Type\", \"CaseClaimNumber\" \"ClaimNumber\", to_char(appointment.uniquecreatedate, 'mm/dd/yyyy') \"DateScheduled\", case when extract(day from (appointment.uniquecreatedate - \"Referral_ReceiveDate\")) = 1 then extract(day from (appointment.uniquecreatedate - \"Referral_ReceiveDate\"))||' day' else extract(day from (appointment.uniquecreatedate - \"Referral_ReceiveDate\"))||' days'   end \"TimeToSchedule\", to_char(\"Appointment_AppointmentTime\", 'mm/dd/yyyy HH24:mi') \"DOS\", case when extract(day from (encounter.senttoadj - \"Appointment_AppointmentTime\")) < 0 then '--' else extract(day from (encounter.senttoadj - \"Appointment_AppointmentTime\"))||' days'  end \"TimeToMR\", \"Encounter_Status\" \"Status\", case when extract(day from (encounter.senttoadj - \"Referral_ReceiveDate\")) < 0 then '--' when extract(day from (encounter.senttoadj - \"Referral_ReceiveDate\")) < 1 then extract(day from (encounter.senttoadj - \"Referral_ReceiveDate\"))||' day'  else extract(day from (encounter.senttoadj - \"Referral_ReceiveDate\"))||' days'  end \"TimeTrack\", main.\"ReferralID\", case when referral.referralsourceother = 0 then case when refsource.userid = 0 then '' else refsource.contactfirstname||' '||refsource.contactlastname end else referralsourceotherdesc end as \"ReferralSource\", case when  refsource.userid= 0 then ''  else refsourcepayer.payername  end as \"ReferralSourcePayor\",  case when refsource.userid= 0 then ''  else refsource.contactphone  end as \"ReferralSourcePhone\", case when \"UA_NCM_UserID\" = 0 then '' else \"UA_NCM_FirstName\"||' '||\"UA_NCM_LastName\" end as \"NCM\", case when \"UA_NCM_UserID\" = 0 then '' else \"UA_NCM_Email\" end as \"NCM_Email\", case when \"UA_NCM_UserID\" = 0 then '' else ncm.contactphone end as \"NCM_Phone\", case when caseaccount.caseadministratorid = 0 then '' else caseadmin_ua.contactfirstname end as \"CaseAdminFirstName\", case when caseaccount.caseadministratorid = 0 then '' else caseadmin_ua.contactlastname end as \"CaseAdminLastName\", case when caseaccount.caseadministrator2id = 0 then '' else caseadmin2_ua.contactfirstname||' '||caseadmin2_ua.contactlastname end as \"CaseAdmin2\", case when caseaccount.caseadministrator3id = 0 then '' else caseadmin3_ua.contactfirstname||' '||caseadmin3_ua.contactlastname end as \"CaseAdmin3\", \"UA_ReferringDr_FirstName\"||' '||\"UA_ReferringDr_LastName\" \"PhysicianName\", mdPayer.payername as \"MD Payer group\", refdr.contactphone \"PhysicianPh\", \"Service_first_CPTBodyPart\", \"Practice_Name\" \"IC\", \"Practice_City\" \"IC_City\", \"Practice_State\" \"IC_State\", case when encounter.iscourtesy = 1 then 'Yes' else '' end as \"Courtesy\", case when encounter.isretro = 1 then 'Yes' else '' end as \"Retro\", encounter.pricing_structure as \"Pricing Structure\", case when encounter.encounterstatusid = 5 then 'Yes' else '' end as \"Void\"  from \"vMQ4_Encounter_Void_BP\" as main left join tnim3_appointment as appointment on appointment.initialencounterid = main.\"EncounterID\"  left join tnim3_encounter as encounter on encounter.encounterid = main.\"EncounterID\"  left join tnim3_referral as referral on referral.referralid = main.\"ReferralID\" left join tnim3_caseaccount as caseaccount on caseaccount.caseid = main.\"CaseID\" left join tuseraccount as caseadmin_ua on caseadmin_ua.userid = caseaccount.caseadministratorid left join tuseraccount as caseadmin2_ua on caseadmin2_ua.userid = caseaccount.caseadministrator2id left join tuseraccount as caseadmin3_ua on caseadmin3_ua.userid = caseaccount.caseadministrator3id left join tuseraccount as refdr on refdr.userid = main.\"UA_ReferringDr_UserID\" left join tuseraccount as adj on adj.userid = main.\"UA_Adjuster_UserID\" left join tuseraccount as refsource on refsource.userid = referral.referredbycontactid left join tuseraccount as emploc on emploc.userid = caseaccount.employerlocationid left join tnim3_payermaster as refsourcepayer on refsourcepayer.payerid = refsource.payerid left join tnim3_payermaster as mdPayer on mdPayer.payerid = refdr.payerid left join tuseraccount as ncm on ncm.userid = main.\"UA_NCM_UserID\" left join tnim3_payermaster as parentpayer on parentpayer.payerid = caseaccount.payerid  where appointment.istatus = 1"+dateReferredClause+"order by main.\"EncounterID\"";
			//out.print(query);
			searchDB2 connection = new searchDB2();
			java.sql.ResultSet results = connection.executeStatement(query);
			
			while (results.next())
			{%>
				<tr>
                <td style="border:1px solid"><%=results.getString("PayerName")%></td>
				<td style="border:1px solid"><%=results.getString("Employer")%></td>
				<td style="border:1px solid"><%=results.getString("ParentPayerID")%></td>
				<td style="border:1px solid"><%=results.getString("AssignedTo")%></td>
				<td style="border:1px solid"><%=results.getString("ScanPass")%></td>
				<td style="border:1px solid"><%=results.getString("Adjuster")%></td>
				<td style="border:1px solid"><%=results.getString("AdjusterPhone")%></td>
                <td style="border:1px solid"><%=results.getString("ReferralMethod")%></td>
				<td style="border:1px solid"><%=results.getString("IsStat")%></td>
				<td style="border:1px solid"><%=results.getString("RecDate")%></td>
				<td style="border:1px solid"><%=results.getString("SentRpToAdj")%></td>
				<td style="border:1px solid"><%=results.getString("Type")%></td>
				<td style="border:1px solid"><%=results.getString("ClaimNumber")%></td>
				<td style="border:1px solid"><%=results.getString("DateScheduled")%></td>
				<td style="border:1px solid"><%=results.getString("TimeToSchedule")%></td>
				<td style="border:1px solid"><%=results.getString("DOS")%></td>
				<td style="border:1px solid"><%=results.getString("TimeToMR")%></td>
				<td style="border:1px solid"><%=results.getString("Status")%></td>
				<td style="border:1px solid"><%=results.getString("TimeTrack")%></td>
				<td style="border:1px solid"><%=results.getString("ReferralID")%></td>
				<td style="border:1px solid"><%=results.getString("ReferralSource")%></td>
				<td style="border:1px solid"><%=results.getString("ReferralSourcePayor")%></td>
				<td style="border:1px solid"><%=results.getString("ReferralSourcePhone")%></td>
				<td style="border:1px solid"><%=results.getString("NCM")%></td>
				<td style="border:1px solid"><%=results.getString("NCM_Email")%></td>
				<td style="border:1px solid"><%=results.getString("NCM_Phone")%></td>
				<td style="border:1px solid"><%=results.getString("CaseAdminFirstName")%></td>
                <td style="border:1px solid"><%=results.getString("CaseAdminLastName")%></td>
				<td style="border:1px solid"><%=results.getString("CaseAdmin2")%></td>
				<td style="border:1px solid"><%=results.getString("CaseAdmin3")%></td>
				<td style="border:1px solid"><%=results.getString("PhysicianName")%></td>
                <td style="border:1px solid"><%=results.getString("MD Payer Group")%></td>
				<td style="border:1px solid"><%=results.getString("PhysicianPh")%></td>
				<td style="border:1px solid"><%=results.getString("Service_first_CPTBodyPart")%></td>
				<td style="border:1px solid"><%=results.getString("IC")%></td>
				<td style="border:1px solid"><%=results.getString("IC_City")%></td>
				<td style="border:1px solid"><%=results.getString("IC_State")%></td>
				<td style="border:1px solid"><%=results.getString("Courtesy")%></td>
				<td style="border:1px solid"><%=results.getString("Retro")%></td>
				<td style="border:1px solid"><%=results.getString("Pricing Structure")%></td>
                <td style="border:1px solid"><%=results.getString("Void")%></td>
                </tr>
			<%}
			connection.closeAll();
			%>
            
           
        
        
        </table>	
		<%} %>	
			

		<%}//if (accessValid)
		else
		{
			out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");//if (accessvaild)
		}
	}//if (iSecurityCheck.intValue()!=0)
	else
	{
	out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");// if (iSecurityCheck.intValue()!=0)
	}%>

</body>
</html>
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>