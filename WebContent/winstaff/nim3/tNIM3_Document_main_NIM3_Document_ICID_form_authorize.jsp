<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    filename: out\jsp\tNIM3_Document_main_NIM3_Document_CaseID_form_authorize.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
boolean accessValid = false;
boolean isPass = false;

Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
if (iSecurityCheck.intValue()!=0)
{
  if (isIC) 
  {
      accessValid = true;
  }
 //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            //out.println(requestID);
        }
		
		bltNIM3_Document working_bltNIM3_Document = NIMUtils.getRxForICID(CurrentUserAccount.getReferenceID(),new Integer(requestID));
		if (working_bltNIM3_Document!=null)
		{
			pageControllerHash.put("iDocumentID",working_bltNIM3_Document.getDocumentID());
			pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
			pageControllerHash.put("sFileName",working_bltNIM3_Document.getFileName());
			pageControllerHash.put("sDownloadName", com.winstaff.password.RandomString.generateString(4).toLowerCase() + Math.round(Math.random()*1000)+com.winstaff.password.RandomString.generateString(4).toLowerCase() + "_" + working_bltNIM3_Document.getFileName());
			pageControllerHash.put("bDownload", new Boolean(true));
			session.setAttribute("pageControllerHash",pageControllerHash);
			response.sendRedirect("fileRetrieve.jsp");
		}
		else
		{
			out.println("Security Error");
			DebugLogger.println("SECURITY:[host:"+REMOTE_USER_host+"][ip:"+REMOTE_USER_ip+"]:WEB:tNIM3_Document_main_NIM3_Document_ICID_form_authorize.jsp:Unauthorized View Attempt[ICID="+CurrentUserAccount.getReferenceID()+"|RxFileID="+requestID+"]");
		}

  }
  else
  {
   out.println("illegal");
  }
}
else
{
out.println("Your Security Level does not permit you to View this.");
}
%>




