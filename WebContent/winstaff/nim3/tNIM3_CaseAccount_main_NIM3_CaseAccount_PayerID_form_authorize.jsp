<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*    " %>
<%/*
    filename: out\jsp\tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iPayerID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
		//iPayerID = CurrentUserAccount.getPayerID();
        accessValid = true;
    }
	else if (isScheduler)
	{
        iPayerID        =    new Integer(0);
        accessValid = true;
	}
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }



searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;
	String mySQL = "null";
	String mySQL2 = "null";

try
{
	if (isScheduler)
	{	
		mySQL = "select caseID from tNIM3_CaseAccount where PayerID >0 AND caseID = " + requestID;
	}
	else if ((isAdjuster||isProvider)&&request.getParameter("EDIT").equalsIgnoreCase("viewpayer")){
		mySQL = "select caseID from tNIM3_CaseAccount where caseID = " + requestID;
	}
	else if (isAdjuster3)
	{	
		//mySQL = "select caseID from tNIM3_CaseAccount where PayerID = " + iPayerID + " AND caseID = " + requestID;
		mySQL = "select caseID from tNIM3_CaseAccount where caseID = " + requestID;
	}
	else if (isAdjuster2)
	{	
		mySQL = "select caseID from tNIM3_CaseAccount where PayerID = " + iPayerID + " AND  ( tNIM3_CaseAccount.adjusterid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.adjusterid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) 	OR tNIM3_CaseAccount.nursecasemanagerid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.nursecasemanagerid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) OR tNIM3_CaseAccount.caseadministratorid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministratorid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " )  or tNIM3_CaseAccount.caseadministrator2id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator2id in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) or tNIM3_CaseAccount.caseadministrator3id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator3id in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " )  or tNIM3_CaseAccount.caseadministrator4id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator4id in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " )  ) AND caseID = " + requestID;
	}
	else if (isAdjuster)
	{	
		mySQL = "select caseID from tNIM3_CaseAccount where (tNIM3_CaseAccount.adjusterid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.nursecasemanagerid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministratorid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator2id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator3id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator4id = " + CurrentUserAccount.getUserID() + ") AND  caseID = " + requestID;
	}
	else if (isProvider3)
	{	
		mySQL = "select tNIM3_CaseAccount.caseid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid where tNIM3_Referral.referringphysicianid in (select userid from tUserAccount where payerid = " + iPayerID + ") and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") AND tNIM3_CaseAccount.caseID = " + requestID;
	}
	else if (isProvider)
	{	
//		mySQL = "select caseid from tNIM3_DualProvider_v_Payer where (ReferringID = " + iPayerID + " OR AttendingID = " + iPayerID + ") AND caseID = " + requestID;
		mySQL = "select tNIM3_CaseAccount.caseid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid where tNIM3_Referral.referringphysicianid in (" + CurrentUserAccount.getUserID() + ") and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") AND tNIM3_CaseAccount.caseID = " + requestID;
	}

//out.println(mySQL);
	myRS = mySS.executeStatement(mySQL);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

try
{
	if (myRS!=null&&myRS.next())
	{
			bltNIM3_CaseAccount working_bltNIM3_CaseAccount  =  new bltNIM3_CaseAccount(new Integer(myRS.getString("caseid") ) );
			pageControllerHash.put("iCaseID",working_bltNIM3_CaseAccount.getCaseID());
			pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
			session.setAttribute("pageControllerHash",pageControllerHash);
			//Parameter Pass Code here
			String parameterPassString ="";
			java.util.Enumeration myParameterPassList = request.getParameterNames();
			while (myParameterPassList.hasMoreElements())
			{
				String myName = (String)myParameterPassList.nextElement();
				String myS = (String) request.getParameter(myName);
				parameterPassString+="&"+myName + "=" + myS;
			}
			String targetRedirect = "tNIM3_CaseAccount_form.jsp?nullParam=null"+parameterPassString    ;
			if (request.getParameter("EDIT").equalsIgnoreCase("del"))
			{
				targetRedirect = "tNIM3_CaseAccount_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
			}
			else if (request.getParameter("EDIT").equalsIgnoreCase("openflow")&&isScheduler)
			{
				targetRedirect = DataControlUtils.getOpenFlow("tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize") + "?snull=null" +parameterPassString    ;
			}
			else if (request.getParameter("EDIT").equalsIgnoreCase("viewpayer"))
			{
				targetRedirect = "tNIM3_Referral_main_NIM3_Referral_CaseID_4Payer.jsp"   ;
			}
			else if (request.getParameter("EDIT").equalsIgnoreCase("quickview")&&isScheduler)
			{
				targetRedirect = "tNIM3_Encounter_QuickView.jsp?snull=null" +parameterPassString    ;
			}
			else if (request.getParameter("EDIT").equalsIgnoreCase("viewcomm"))
			{
				targetRedirect = "tNIM3_CommTrack_frame.jsp"    ;
			}
			else if (request.getParameter("EDIT").equalsIgnoreCase("viewreports"))
			{
				targetRedirect = "tNIM3_CaseAccount_reports.jsp"    ;
			}
			else if (request.getParameter("EDIT").equalsIgnoreCase("openflowexpress"))
			{
				targetRedirect = ("tNIM3_CaseAccount_Express.jsp?EDITID=" + working_bltNIM3_CaseAccount.getCaseID())   ;
			}
			response.sendRedirect(targetRedirect);
	}
    else
    {
	   out.println("invalid where query");
    }
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}
mySS.closeAll();



  }
  else
  {
   out.println("illegal");
  }
}
else
{
out.println("Your Security Level does not permit you to View this.");
}
%>




