<%@page import="java.io.*,com.winstaff.*, java.util.*, java.text.SimpleDateFormat,com.google.gson.Gson;"%>
<%
String caseClaimNumber = "";
String NIM3_schedulerID = "";
String NIM3_adjusterID="";
String NIM3_ncmID="";
String NIM3_ADMIN_1_ID = "";
String NIM3_ADMIN_2_ID = "";
String NIM3_ADMIN_3_ID = "";
String NIM3_ADMIN_4_ID = "";
String casenotes = "";
String patientfirst = "";
String patientlast = "";
String patientaddress = "";
String patientaddress2 = "";
String patientcity = "";
String patientstate = "";
String patientzip = "";
String patientemail = "";
String patientphone = "";
String patientcellphone = "";
String patientdob = "";
String patientssn = "";
String patientavailability = "";
String employername = "";
String employerfax = "";
String employerphone = "";
String employercontact = "";
String claustrophobic = "";
String height = "";
String weight = "";
String gender = "";
String doi = "";
String onmeds = "";
String onmedsdescreption = "";
String metalinbody = "";
String metalinbodydescreption = "";
String previousimage = "";
String previousimagedescreption = "";
String pregnant = "";
String pregnantdescreption = "";
String INJURY_DESCRIPTION = "";
String cid = "";
String rf_id = "";
String en_id = "";
String schedulerfullname = "";
try{
	caseClaimNumber = request.getParameter("caseClaimNumber");
	NIM3_adjusterID = request.getParameter("NIM3_adjusterID");
	NIM3_schedulerID = request.getParameter("NIM3_schedulerID");
	NIM3_ncmID = request.getParameter("NIM3_ncmID");
	NIM3_ADMIN_1_ID = request.getParameter("NIM3_ADMIN_1_ID");
	NIM3_ADMIN_2_ID = request.getParameter("NIM3_ADMIN_2_ID");
	NIM3_ADMIN_3_ID = request.getParameter("NIM3_ADMIN_3_ID");
	NIM3_ADMIN_4_ID = request.getParameter("NIM3_ADMIN_4_ID");
	casenotes = request.getParameter("casenotes");
	
	patientfirst = request.getParameter("patientfirst");
	 patientlast = request.getParameter("patientlast");
	 patientaddress = request.getParameter("patientaddress");
	 patientaddress2 = request.getParameter("patientaddress2");
	 patientcity = request.getParameter("patientcity");
	 patientstate = request.getParameter("patientstate");
	 patientzip = request.getParameter("patientzip");
	 patientemail = request.getParameter("patientemail");
	 patientphone = request.getParameter("patientphone");
	 patientcellphone = request.getParameter("patientcellphone");
	 patientdob = request.getParameter("patientdob");
	 patientssn = request.getParameter("patientssn");
	employername = request.getParameter("employername");
	employerfax = request.getParameter("employerfax");
	employerphone = request.getParameter("employerphone");
	employercontact = request.getParameter("employercontact");
	claustrophobic = request.getParameter("claustrophobic");
	 height = request.getParameter("height");
	 weight = request.getParameter("weight");
	 gender = request.getParameter("gender");
	 doi = request.getParameter("doi");
	 onmeds = request.getParameter("onmeds");
	 onmedsdescreption = request.getParameter("onmedsdescreption");
	 metalinbody = request.getParameter("metalinbody");
	 metalinbodydescreption = request.getParameter("metalinbodydescreption");
	 previousimage = request.getParameter("previousimage");
	 previousimagedescreption = request.getParameter("previousimagedescreption");
	 pregnant = request.getParameter("pregnant");
	 pregnantdescreption = request.getParameter("pregnantdescreption");
	 INJURY_DESCRIPTION = request.getParameter("INJURY_DESCRIPTION");
	 cid = request.getParameter("cid");
	 rf_id = request.getParameter("rf_id");
	 en_id = request.getParameter("en_id");
	 schedulerfullname = request.getParameter("schedulerfullname");
	 bltNIM3_CaseAccount ca = new bltNIM3_CaseAccount(new Integer(cid));
	 bltNIM3_CommTrack ct = new bltNIM3_CommTrack();	
	 SimpleDateFormat date = new SimpleDateFormat();
	 java.util.Date now = new java.util.Date();
	 try{
			if(caseClaimNumber != null && !caseClaimNumber.trim().isEmpty()){
				ca.setCaseClaimNumber(caseClaimNumber);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			if(NIM3_adjusterID != null && !NIM3_adjusterID.trim().isEmpty()){
				ca.setAdjusterID(new Integer(NIM3_adjusterID));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		/* try{
 			if(PAYER_NOTES != null && !PAYER_NOTES.trim().isEmpty()){
 				ca.setComments(PAYER_NOTES);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		} */
		try{
 			if(casenotes != null && !casenotes.trim().isEmpty()){
 				ca.setComments(casenotes);
 			}
		}catch(Exception e){
			ca.setComments("Case Created Via N4 ");
		}
 		
 		try{
 			if(patientfirst != null && !patientfirst.trim().isEmpty()){
 				ca.setPatientFirstName(patientfirst);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(patientlast != null && !patientlast.trim().isEmpty()){
 				ca.setPatientLastName(patientlast);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(patientaddress != null && !patientaddress.trim().isEmpty()){
 				ca.setPatientAddress1(patientaddress);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(patientaddress2 != null && !patientaddress2.trim().isEmpty()){
 				ca.setPatientAddress2(patientaddress2);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(patientcity != null && !patientcity.trim().isEmpty()){
 				ca.setPatientCity(patientcity);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(patientstate != null && !patientstate.trim().isEmpty()){
 				ca.setPatientStateID(new Integer(patientstate));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(patientzip != null && !patientzip.trim().isEmpty()){
 				ca.setPatientZIP(patientzip);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(patientemail != null && !patientemail.trim().isEmpty()){
 				ca.setPatientEmail(patientemail);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(patientphone != null && !patientphone.trim().isEmpty()){
 				ca.setPatientHomePhone(patientphone);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(patientcellphone != null && !patientcellphone.trim().isEmpty()){
 				ca.setPatientCellPhone(patientcellphone);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		
 		try{
 			if(patientdob != null && !patientdob.trim().isEmpty()){
 				ca.setPatientDOB(date.parse(patientdob));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		 try{
 			if(patientssn != null && !patientssn.trim().isEmpty()){
 				ca.setPatientSSN(patientssn);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(employername != null && !employername.trim().isEmpty()){
 				ca.setEmployerName(employername);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(employerphone != null && !employerphone.trim().isEmpty()){
 				ca.setEmployerPhone(employerphone);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(employerfax != null && !employerfax.trim().isEmpty()){
 				ca.setEmployerFax(employerfax);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		
 		try{
 			if(INJURY_DESCRIPTION != null && !INJURY_DESCRIPTION.trim().isEmpty()){
 				ca.setInjuryDescription(INJURY_DESCRIPTION);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(claustrophobic != null && !claustrophobic.trim().isEmpty()){
 				ca.setPatientIsClaus(new Integer(claustrophobic));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(height != null && !height.trim().isEmpty()){
 				ca.setPatientHeight(height);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(weight != null && !weight.trim().isEmpty()){
 				ca.setPatientWeight(new Integer(weight));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(gender != null && !gender.trim().isEmpty()){
 				ca.setPatientGender(gender);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(doi != null && !doi.trim().isEmpty()){
 				ca.setPatientDOB(date.parse(doi));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(onmeds != null && !onmeds.trim().isEmpty()){
 				ca.setIsCurrentlyOnMeds(new Integer(onmeds));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(onmedsdescreption != null && !onmedsdescreption.trim().isEmpty()){
 				ca.setIsCurrentlyOnMedsDesc(onmedsdescreption);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(metalinbody != null && !metalinbody.trim().isEmpty()){
 				ca.setPatientHasMetalInBody(new Integer(metalinbody));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(metalinbodydescreption != null && !metalinbodydescreption.trim().isEmpty()){
 				ca.setPatientHasMetalInBodyDesc(metalinbodydescreption);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(previousimage != null && !previousimage.trim().isEmpty()){
 				ca.setPatientHasPreviousMRIs(new Integer(previousimage));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(previousimagedescreption != null && !previousimagedescreption.trim().isEmpty()){
 				ca.setPatientHasPreviousMRIsDesc(previousimagedescreption);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(pregnant != null && !pregnant.trim().isEmpty()){
 				ca.setPatientIsPregnant(new Integer(pregnant));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(pregnantdescreption != null && !pregnantdescreption.trim().isEmpty()){
 				ca.setPatientIsPregnantDesc(pregnantdescreption);
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(NIM3_ncmID != null && !NIM3_ncmID.trim().isEmpty()){
 				ca.setNurseCaseManagerID(new Integer(NIM3_ncmID));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(NIM3_ADMIN_1_ID != null && !NIM3_ADMIN_1_ID.trim().isEmpty()){
 				ca.setCaseAdministratorID(new Integer(NIM3_ADMIN_1_ID));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(NIM3_ADMIN_2_ID != null && !NIM3_ADMIN_2_ID.trim().isEmpty()){
 				ca.setCaseAdministrator2ID(new Integer(NIM3_ADMIN_2_ID));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(NIM3_ADMIN_3_ID != null && !NIM3_ADMIN_3_ID.trim().isEmpty()){
 				ca.setCaseAdministrator3ID(new Integer(NIM3_ADMIN_3_ID));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		try{
 			if(NIM3_ADMIN_4_ID != null && !NIM3_ADMIN_4_ID.trim().isEmpty()){
 				ca.setCaseAdministrator4ID(new Integer(NIM3_ADMIN_4_ID));
 			}
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 	
 		ca.setUniqueModifyDate(PLCUtils.getNowDate(false));
 		ca.setUniqueModifyComments("Modified via N4 by " + ca.getAssignedToID());
 		ca.commitData();
 		
 		
 		ct.setUniqueCreateDate(now);
		ct.setCaseID(new Integer(cid));//FINISH SETTING COMMTRACKS
		ct.setReferralID(new Integer(rf_id));
		ct.setEncounterID(new Integer(en_id));
		ct.setIntUserID(new Integer(ca.getCaseID()));
		ct.setMessageCompany("Update ");
		ct.setMessageSubject("Encounter Modification");
		ct.setMessageName(NIM3_schedulerID + " Modified Case" + " N4 Generated.");
		ct.setMessageText("Generated via N4 " + now);
		ct.setCommStart(new Date());
		ct.setCommEnd(new Date());
		ct.commitData();
		out.println("Please wait saving case data loading page.."+cid);
			%>
		 	<script>
				document.location.href = "http://localhost:8080/n4/ca/<%=cid%>/<%=en_id%>";
			</script>
		 	<% 
 		
}catch(Exception e){
	e.printStackTrace();
}
%>