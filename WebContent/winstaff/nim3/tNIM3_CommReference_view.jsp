<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*

    filename: out\jsp\tNIM3_CommReference_form.jsp
    Created on Jun/24/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_1/style.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>

<p class="title">Comm Reference Viewer</p>

<%
//initial declaration of list class and parentID
    Integer        iCommReferenceID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iCommReferenceID        =    new Integer(request.getParameter ("EDITID"));
        accessValid = true;
    }
  //page security
  if (isScheduler&&accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

//initial declaration of list class and parentID

    bltNIM3_CommReference        NIM3_CommReference        =    null;
	NIM3_CommReference        =    new    bltNIM3_CommReference(iCommReferenceID,UserSecurityGroupID);
	bltEmailTransaction myET = null;
	if (NIM3_CommReference.getCRTypeID()==1)
	{
		myET = new bltEmailTransaction(NIM3_CommReference.getRefID());
	%>
    <table border="1" cellspacing="0" cellpadding="10">
  <tr class="tdHeaderAlt">
    <td>CommReferenceID:<%=NIM3_CommReference.getUniqueID()%></td>
  </tr>
  <tr class="tdBaseAlt3">
    <td>RefID:<%=NIM3_CommReference.getRefID()%></td>
  </tr>
  <tr>
    <td> <pre style="font-size:14"><%=myET.getEmailBody()%></pre></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>

   
   
    <%
	}

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>