<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tNIM3_CommTrack_main_NIM3_CommTrack_CaseID_form_create.jsp
    Created on Jun/24/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    Integer        iEncounterID        =    null;
    Integer        iReferralID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
		if (pageControllerHash.containsKey("iEncounterID")) 
		{
			iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
			
		}
		if (pageControllerHash.containsKey("iReferralID")) 
		{
			iReferralID        =    (Integer)pageControllerHash.get("iReferralID");
		}
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
        }

//declaration of Enumeration
    bltNIM3_CommTrack        working_bltNIM3_CommTrack = new bltNIM3_CommTrack();
    working_bltNIM3_CommTrack.setCaseID(iCaseID);
	if (iEncounterID!=null)
	{
	    working_bltNIM3_CommTrack.setEncounterID(iEncounterID);
	}
	if (iReferralID!=null)
	{
	    working_bltNIM3_CommTrack.setReferralID(iReferralID);
	}
	bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
	working_bltNIM3_CommTrack.setMessageName(CurrentUserAccount.getLogonUserName());
	working_bltNIM3_CommTrack.setMessageCompany(myPM.getPayerName());
    working_bltNIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
    working_bltNIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
    working_bltNIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
    working_bltNIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
    working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
	
    if (pageControllerHash.containsKey("UserLogonDescription"))
    {
        String UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
        working_bltNIM3_CommTrack.setUniqueModifyComments(""+UserLogonDescription);
    }

            working_bltNIM3_CommTrack.commitData();
        {
            {
                pageControllerHash.put("iCommTrackID",working_bltNIM3_CommTrack.getUniqueID());
                pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
                session.setAttribute("pageControllerHash",pageControllerHash);
                //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
                String targetRedirect = "tNIM3_CommTrack_form_easy.jsp?nullParam=null"+parameterPassString    ;

                {
                    targetRedirect = "tNIM3_CommTrack_form_easy.jsp?EDITID="+working_bltNIM3_CommTrack.getUniqueID()+"&routePageReference=sParentReturnPage&EDIT=edit"+parameterPassString    ;
                }
                response.sendRedirect(targetRedirect);
            }
        }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>





