<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%/*

    filename: out\jsp\tUserAccount_form.jsp
    Created on Jun/26/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>


    <link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
<table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>



<%
//initial declaration of list class and parentID
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (true) 
    {
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
    pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID

bltUserAccount UserAccount        =    CurrentUserAccount;

//fields
        %>
        <form action="tUserAccount_form_settings_sub.jsp" name="tUserAccount_form1" method="POST">

          <%  String theClass ="tdBase";%>
        <table  border=1 bordercolor=#333333 cellpadding=3 cellspacing=0 >
         <tr>
          <td class="title" >User Settings</td></tr>
         <tr>
           <td >
            <table cellpadding=2 cellspacing=0>


            <%
            if ( (UserAccount.isRequired("AccountType",UserSecurityGroupID))&&(!UserAccount.isComplete("AccountType")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("AccountType",expiredDays))&&(UserAccount.isExpiredCheck("AccountType",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("AccountType",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Account Type&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="AccountType" value="<%=UserAccount.getAccountType()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AccountType&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("AccountType")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("AccountType",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>AccountType&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getAccountType()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=AccountType&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("AccountType")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>




            <%
            if ( (UserAccount.isRequired("ReferenceID",UserSecurityGroupID))&&(!UserAccount.isComplete("ReferenceID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ReferenceID",expiredDays))&&(UserAccount.isExpiredCheck("ReferenceID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("ReferenceID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>ReferenceID&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="ReferenceID" value="<%=UserAccount.getReferenceID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferenceID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ReferenceID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(UserAccount.isRead("ReferenceID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>ReferenceID&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getReferenceID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReferenceID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ReferenceID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("LogonUserName",UserSecurityGroupID))&&(!UserAccount.isComplete("LogonUserName")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("LogonUserName",expiredDays))&&(UserAccount.isExpiredCheck("LogonUserName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("LogonUserName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>User name&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="LogonUserName" value="<%=UserAccount.getLogonUserName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LogonUserName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("LogonUserName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("LogonUserName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Username&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getLogonUserName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=LogonUserName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("LogonUserName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactFirstName",UserSecurityGroupID))&&(!UserAccount.isComplete("ContactFirstName")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactFirstName",expiredDays))&&(UserAccount.isExpiredCheck("ContactFirstName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactFirstName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b> First Name&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="40" name="ContactFirstName" value="<%=UserAccount.getContactFirstName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactFirstName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactFirstName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactFirstName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact First Name&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getContactFirstName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactFirstName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactFirstName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactLastName",UserSecurityGroupID))&&(!UserAccount.isComplete("ContactLastName")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactLastName",expiredDays))&&(UserAccount.isExpiredCheck("ContactLastName",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactLastName",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b> Last Name&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="40" name="ContactLastName" value="<%=UserAccount.getContactLastName()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactLastName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactLastName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactLastName",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Last Name&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getContactLastName()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactLastName&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactLastName")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactEmail",UserSecurityGroupID))&&(!UserAccount.isComplete("ContactEmail")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactEmail",expiredDays))&&(UserAccount.isExpiredCheck("ContactEmail",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact/Alert E-mail&nbsp;</b></p></td><td valign=top><p><input name="ContactEmail" type=text value="<%=UserAccount.getContactEmail()%>" size="40" maxlength="75">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactEmail",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact E-mail&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getContactEmail()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactEmail&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactEmail")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactAddress1",UserSecurityGroupID))&&(!UserAccount.isComplete("ContactAddress1")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactAddress1",expiredDays))&&(UserAccount.isExpiredCheck("ContactAddress1",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("ContactAddress1",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Address&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ContactAddress1" value="<%=UserAccount.getContactAddress1()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactAddress1&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactAddress1")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(UserAccount.isRead("ContactAddress1",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Address&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getContactAddress1()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactAddress1&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactAddress1")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactAddress2",UserSecurityGroupID))&&(!UserAccount.isComplete("ContactAddress2")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactAddress2",expiredDays))&&(UserAccount.isExpiredCheck("ContactAddress2",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("ContactAddress2",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Address 2&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="ContactAddress2" value="<%=UserAccount.getContactAddress2()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactAddress2&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactAddress2")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(UserAccount.isRead("ContactAddress2",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Address 2&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getContactAddress2()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactAddress2&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactAddress2")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactCity",UserSecurityGroupID))&&(!UserAccount.isComplete("ContactCity")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactCity",expiredDays))&&(UserAccount.isExpiredCheck("ContactCity",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("ContactCity",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact City&nbsp;</b></p></td><td valign=top><p><input maxlength="30" type=text size="80" name="ContactCity" value="<%=UserAccount.getContactCity()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactCity&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactCity")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(UserAccount.isRead("ContactCity",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact City&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getContactCity()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactCity&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactCity")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactStateID",UserSecurityGroupID))&&(!UserAccount.isComplete("ContactStateID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactStateID",expiredDays))&&(UserAccount.isExpiredCheck("ContactStateID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("ContactStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Contact State&nbsp;</b></p></td><td valign=top><p><select   name="ContactStateID" ><jsp:include page="../generic/tStateLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getContactStateID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactStateID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactStateID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(UserAccount.isRead("ContactStateID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Contact State&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tStateLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getContactStateID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactStateID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactStateID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("ContactProvince",UserSecurityGroupID))&&(!UserAccount.isComplete("ContactProvince")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactProvince",expiredDays))&&(UserAccount.isExpiredCheck("ContactProvince",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("ContactProvince",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact Province, District, State&nbsp;</b></p></td><td valign=top><p><input maxlength="100" type=text size="80" name="ContactProvince" value="<%=UserAccount.getContactProvince()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactProvince&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactProvince")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(UserAccount.isRead("ContactProvince",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Province, District, State&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getContactProvince()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactProvince&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactProvince")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactZIP",UserSecurityGroupID))&&(!UserAccount.isComplete("ContactZIP")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactZIP",expiredDays))&&(UserAccount.isExpiredCheck("ContactZIP",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("ContactZIP",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Contact ZIP&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="ContactZIP" value="<%=UserAccount.getContactZIP()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactZIP&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactZIP")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(UserAccount.isRead("ContactZIP",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact ZIP&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getContactZIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactZIP&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactZIP")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactCountryID",UserSecurityGroupID))&&(!UserAccount.isComplete("ContactCountryID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactCountryID",expiredDays))&&(UserAccount.isExpiredCheck("ContactCountryID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("ContactCountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Contact Country&nbsp;</b></p></td><td valign=top><p><select   name="ContactCountryID" ><jsp:include page="../generic/tCountryLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getContactCountryID()%>" /></jsp:include></select>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactCountryID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactCountryID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(UserAccount.isRead("ContactCountryID",UserSecurityGroupID)))
            {
                        %>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Contact Country&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/tCountryLILong_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getContactCountryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactCountryID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactCountryID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("ContactPhone",UserSecurityGroupID))&&(!UserAccount.isComplete("ContactPhone")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactPhone",expiredDays))&&(UserAccount.isExpiredCheck("ContactPhone",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactPhone",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Direct Phone</b></p></td><td valign=top><p><input maxlength="50" type=text size="40" name="ContactPhone" value="<%=UserAccount.getContactPhone()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactPhone&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactPhone")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactPhone",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Contact Phone (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getContactPhone()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactPhone&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactPhone")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactFax",UserSecurityGroupID))&&(!UserAccount.isComplete("ContactFax")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactFax",expiredDays))&&(UserAccount.isExpiredCheck("ContactFax",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactFax",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Fax</b></p></td><td valign=top><p><input maxlength="50" type=text size="40" name="ContactFax" value="<%=UserAccount.getContactFax()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactFax&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactFax")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactFax",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Fax (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getContactFax()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactFax&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactFax")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("ContactMobile",UserSecurityGroupID))&&(!UserAccount.isComplete("ContactMobile")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("ContactMobile",expiredDays))&&(UserAccount.isExpiredCheck("ContactMobile",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if ((UserAccount.isWrite("ContactMobile",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>Mobile</b></p></td><td valign=top><p><input maxlength="50" type=text size="40" name="ContactMobile" value="<%=UserAccount.getContactMobile()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactMobile&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactMobile")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("ContactMobile",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Mobile (XXX-XXX-XXXX)&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getContactMobile()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ContactMobile&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("ContactMobile")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>


            <%
            if ( (UserAccount.isRequired("TaxID",UserSecurityGroupID))&&(!UserAccount.isComplete("TaxID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("TaxID",expiredDays))&&(UserAccount.isExpiredCheck("TaxID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("TaxID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>TaxID&nbsp;</b></p></td><td valign=top><p><input maxlength="50" type=text size="80" name="TaxID" value="<%=UserAccount.getTaxID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TaxID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("TaxID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if (false&&(UserAccount.isRead("TaxID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>TaxID&nbsp;</b></p></td><td valign=top><p><%=UserAccount.getTaxID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TaxID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("TaxID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



            <%
            if ( (UserAccount.isRequired("PayerID",UserSecurityGroupID))&&(!UserAccount.isComplete("PayerID")) )
            {
                theClass = "requiredField";
            }
            else if ((UserAccount.isExpired("PayerID",expiredDays))&&(UserAccount.isExpiredCheck("PayerID",UserSecurityGroupID)))
            {
                theClass = "expiredField";
            }
            else
            {
                theClass = "tdBase";
            }
                %>
            <%
            if (false&&(UserAccount.isWrite("PayerID",UserSecurityGroupID)))
            {
                        %>
                     <tr><td valign=top><p class=<%=theClass%> ><b>PayerID&nbsp;</b></p></td><td valign=top><p><input maxlength="20" type=text size="80" name="PayerID" value="<%=UserAccount.getPayerID()%>">&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("PayerID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else if ((UserAccount.isRead("PayerID",UserSecurityGroupID)))
            {
                        %>
                         <tr><td valign=top><p class=<%=theClass%> ><b>Group&nbsp;</b></p></td><td valign=top><p><%=new bltNIM3_PayerMaster(UserAccount.getPayerID()).getPayerName()%> (<%=UserAccount.getPayerID()%>)&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PayerID&amp;sTableName=tUserAccount&amp;sRefID=<%=UserAccount.getUserID()%>&amp;sFieldNameDisp=<%=UserAccount.getEnglish("PayerID")%>&amp;sTableNameDisp=tUserAccount','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <%
            }
            else
            {
                        %>

                        <%
            }
            %>



<%
if (true)
{
	%>
            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr>
              <td>&nbsp;</td>
              <td><table border="1" cellspacing="0" cellpadding="3">
                <tr class="tdHeaderAlt">
                  <td>&nbsp;</td>
                  <td>Reports</td>
                  <td>Schedule Notification</td>
                  <td>Other Notifications</td>
                </tr>
                <tr class="tdHeaderAlt">
                  <td>Notify me when...</td>
                  <td><select name="Comm_Report_LevelUser" id="AlertDays">
                    <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Report_LevelUser()%>" />                  
                      </jsp:include>
                  </select></td>
                  <td><select name="Comm_Alerts_LevelUser" id="AlertDays">
                    <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Alerts_LevelUser()%>" />                  
                      </jsp:include>
                  </select></td>
                  <td><select name="Comm_Alerts2_LevelUser" id="AlertDays"><jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Alerts2_LevelUser()%>" /></jsp:include></select></td>
                  </tr>
                  <% if (false){%>
                <tr class="tdHeaderAlt">
                  <td>If I supervise anyone, alert me of their cases when...</td>
                  <td><select name="Comm_Report_LevelManager" id="AlertDays">
                    <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Report_LevelManager()%>" />                  
                      </jsp:include>
                  </select></td>
                  <td><select name="Comm_Alerts_LevelManager" id="AlertDays">
                    <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Alerts_LevelManager()%>" />                  
                      </jsp:include>
                  </select></td>
                  <td><select name="Comm_Alerts2_LevelManager" id="AlertDays"><jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Alerts2_LevelManager()%>" /></jsp:include></select></td>
                </tr>
                <tr class="tdHeaderAlt">
                  <td>If I manage an office, alert me of all cases when...</td>
                  <td><select name="Comm_Report_LevelBranch" id="AlertDays">
                    <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Report_LevelBranch()%>" />                  
                      </jsp:include>
                    </select></td>
                  <td><select name="Comm_Alerts_LevelBranch" id="AlertDays">
                    <jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" >
                      <jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Alerts_LevelBranch()%>" />                  
                      </jsp:include>
                  </select></td>
                  <td><select name="Comm_Alerts2_LevelBranch" id="AlertDays"><jsp:include page="../generic/tUAAlertsLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UserAccount.getComm_Alerts2_LevelBranch()%>" /></jsp:include></select></td>
                </tr>
                <% }%>
              </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><strong>Change your password:</strong></td>
              <td><label>
                <strong>Current</strong> Password:
<input name="CurrentPassword" type="password" id="CurrentPassword" value="" size="20" maxlength="40">
              </label></td>
            </tr>
            <tr>
              <td rowspan="2" class="instructions"> Note: Your new password<br>
 must be a length of 8 or <br>
 greater.  Passwords
are<br>
case-sensitive</td>
              <td><strong>New</strong>  Password:
                <input name="NewPassword1" type="password" id="NewPassword1" value="" size="20" maxlength="40"></td>
            </tr>
            <tr>
              <td>Verify  <strong>New</strong> Password:
                <input name="NewPassword2" type="password" id="NewPassword2" value="" size="20" maxlength="40"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2"><span class="tdBase">To update your Username and/or Email, please email us here <a href="mailto:orders@nextimagemedical.com">orders@nextimagemedical.com</a> or call us at <strong>888-318-5111</strong></span></td>
              </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <%
}
%>
            
            </table>
        <%if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {%>
            <input type=hidden name=routePageReference value="sParentReturnPage">
            <p><input name=Submit type=Submit class="inputButton_md_Create" onClick = "this.disabled=true;submit();" value="Save" ></p>
        <%}%>
        </td></tr></table>
        </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>



<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>