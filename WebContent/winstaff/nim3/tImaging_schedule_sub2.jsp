<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="java.text.DecimalFormat ,com.winstaff.*" %>
<%/*

    filename: out\jsp\tNIM3_Encounter_form.jsp
    Created on May/28/2008
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>





<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}

    pageControllerHash.put("sLocalChildReturnPage","tNIM3_Encounter_form.jsp");
//initial declaration of list class and parentID
	NIM3_EncounterObject2 myEO2 = new NIM3_EncounterObject2(iEncounterID,UserLogonDescription);

		   Integer iAppointmentID = null;
if (request.getParameter("EDITID")!=null)
{
	iAppointmentID = new Integer(request.getParameter("EDITID"));
}
bltNIM3_Appointment myNALU = new bltNIM3_Appointment(iAppointmentID);
//myNALU.setAuthorizationID(iEncounterID);
myNALU.setiStatus(new Integer(1));
myNALU.setAppointmentConfirmation("schd_" + com.winstaff.password.RandomString.generateString(2).toUpperCase() + Math.round(Math.random()*100)+com.winstaff.password.RandomString.generateString(2).toUpperCase());
myNALU.setUniqueModifyComments(UserLogonDescription);
myNALU.setScheduler_UserID(CurrentUserAccount.getUserID());

myNALU.setInitialEncounterID(myEO2.getNIM3_Encounter().getEncounterID());
myNALU.commitData();

myEO2.getNIM3_Encounter().setAppointmentID(myNALU.getAppointmentID());
myEO2.getNIM3_Encounter().setDateOfService(myNALU.getAppointmentTime());


//test for Case Sched Logic
if (myEO2.getNIM3_Encounter().getTimeTrack_ReqSched_UserID().intValue()==0)
{
	myEO2.getNIM3_Encounter().setTimeTrack_ReqSched(PLCUtils.getNowDate(true));
	myEO2.getNIM3_Encounter().setTimeTrack_ReqSched_UserID(CurrentUserAccount.getUserID());
}

//test for Case Sched Logic
if (myEO2.getNIM3_Encounter().getTimeTrack_ReqInitialAppointment_UserID().intValue()==0)
{
	myEO2.getNIM3_Encounter().setTimeTrack_ReqInitialAppointment(myNALU.getAppointmentTime());
	myEO2.getNIM3_Encounter().setTimeTrack_ReqInitialAppointment_UserID(CurrentUserAccount.getUserID());
}


myEO2.getNIM3_Encounter().setUniqueModifyComments(UserLogonDescription);
myEO2.getNIM3_Encounter().commitData();
bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
bltNIM3_CommTrack        working_bltNIM3_CommTrack = new bltNIM3_CommTrack();
working_bltNIM3_CommTrack.setUniqueModifyComments("Appointment Schedule Set [ULD:"+UserLogonDescription+"]");
working_bltNIM3_CommTrack.setEncounterID(myEO2.getNIM3_Encounter().getEncounterID());
working_bltNIM3_CommTrack.setReferralID(myEO2.getNIM3_Referral().getReferralID());
working_bltNIM3_CommTrack.setCaseID(myEO2.getNIM3_Referral().getCaseID());
working_bltNIM3_CommTrack.setMessageName(CurrentUserAccount.getLogonUserName());
working_bltNIM3_CommTrack.setMessageCompany(myPM.getPayerName());
working_bltNIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
working_bltNIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
working_bltNIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
working_bltNIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
working_bltNIM3_CommTrack.setUniqueModifyComments(""+UserLogonDescription);
working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
String theMessage = "Appointment Schedule Set [new ApptID:" + myNALU.getAppointmentID() + "]\nNotes: " + myNALU.getComments();
theMessage += "\n\n---System Generated Notes:";
theMessage += "\nCase Currently Assigned to: " + new bltUserAccount(myEO2.getNIM3_CaseAccount().getAssignedToID()).getLogonUserName() + " [" + myEO2.getNIM3_CaseAccount().getAssignedToID() + "]";
theMessage += NIMUtils.getAppointmentDetails(myNALU.getAppointmentID(), false);
working_bltNIM3_CommTrack.setMessageText(theMessage);
working_bltNIM3_CommTrack.commitData();


%>         <script language="javascript">
		 alert("Your Appointment has been saved.  No Emails have been sent out");
//		 document.location='tNIM3_Encounter_main_NIM3_Encounter_ReferralID.jsp';
		 //document.location='tNIM3_Referral_main_NIM3_Referral_CaseID.jsp';
		 window.close();
		 </script>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table>


