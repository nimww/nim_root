<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    filename: tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

    <table cellpadding=0 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM3_CaseAccount")%>



<%
//initial declaration of list class and parentID
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler) 
    {
        accessValid = true;
    }
    else if (pageControllerHash.containsKey("iAdjusterID")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        iAdjusterID        =    (Integer)pageControllerHash.get("iAdjusterID");
		isAdjuster = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID2")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster2 = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID3")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster3 = true;
        accessValid = true;
	}
    else if (isProvider&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
	}
	
	
  //page security
  if (accessValid)
  {
	  java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy");
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");
	  pageControllerHash.put("sINTNext","tNIM3_CaseAccount_PayerID_query.jsp");
	  session.setAttribute("pageControllerHash",pageControllerHash);

%>

<script type="text/javascript">
window.onload=function() {
document.getElementById("search-again").focus();
}
<%
try{
if (request.getParameter("ssSearchMe").equalsIgnoreCase("check_run")){%>
	document.location.href= 'check_run.jsp';
<%}
}
catch(Exception e){} %>
</script> 

<%
try
{

String mySearchMe = "";
String mySearchMe2 = "";
String mySearchDICOM = "";
int startID = 0;
int defaultMax = 25;
int maxResults = defaultMax;
boolean firstDisplay = false;

try
{
	maxResults = Integer.parseInt(request.getParameter("maxResults"));
	startID = Integer.parseInt(request.getParameter("startID"));
	mySearchMe = request.getParameter("ssSearchMe").toLowerCase().trim();
	mySearchMe2 = request.getParameter("ssSearchMe2").toLowerCase().trim();
}
catch (Exception e)
{
	maxResults = defaultMax;
	startID = 0;
	firstDisplay = true;
	mySearchMe = "";
	mySearchMe2 = "";
}
try
{
	mySearchDICOM = request.getParameter("ssSearchDICOM").toLowerCase();
}
catch (Exception e)
{
	mySearchDICOM = "";
}

boolean searchDICOM = false;
if (mySearchDICOM.equalsIgnoreCase("Y"))
{
	searchDICOM = true;
}
try{ 
  if (request.getParameter("ssSearchMe").equalsIgnoreCase("print_rem")){
  	firstDisplay = true;
  } 
}catch(Exception E){}

try{
	if (!request.getParameter("ssSearchMe2").equalsIgnoreCase("print_rem")){
	mySearchMe = DataControlUtils.fixApostrophe(mySearchMe);
} 
}catch(Exception E){}
if (firstDisplay)
{%>
	<script type="text/javascript">
		function search_main(){
			var temp = $.trim(document.forms["searchform"]["temp"].value);
			
			if (temp.indexOf(',') !== -1){
				var temparr = temp.replace(/\s/g, '').split(",");
				document.searchform.ssSearchMe.value = temparr[0];
				document.searchform.ssSearchMe2.value = temparr[1];
			}
			else if(temp.indexOf(':') !== -1){
				var temparr = temp.split(":");
				document.searchform.ssSearchMe.value = temparr[0];
				document.searchform.ssSearchMe2.value = temparr[1];
			}
			else if(temp.indexOf(' ') !== -1){
				var temparr = temp.split(" ");
				document.searchform.ssSearchMe2.value = temparr[0];
				document.searchform.ssSearchMe.value = temparr[1];
			}
			else{
				document.searchform.ssSearchMe.value = temp;
				document.searchform.ssSearchMe2.value = 'undefined';
			}
		}
		function print_rem(){
			var temp = $.trim(document.getElementById("temp").value).split("\u000A");
			var value = "";
			
			 for (x = 0; x<temp.length;x++){
				value += "\'"+$.trim(temp[x])+"\'";
				if(x+1<temp.length){
					value+=", ";
				}
			}
			document.searchform.ssSearchMe.value = value;
			
		}
		$(document).ready(function(){
			document.getElementById("quick-main").focus();
		});
		
	</script>
	
    <form method="post" action="tNIM3_CaseAccount_PayerID_query.jsp" name="searchform" id="searchform">
    <table border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td>&nbsp;</td>
    <td colspan="2" class="title">Quick Search</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Enter <strong>ScanPass</strong>, <strong>Claim #</strong> Or <strong>Patient Last Name<br></strong>
    <strong>Last Name, First Name</strong><br>
    <strong>First Name Last Name</strong> <br>
    <strong>ic: imageCenter</strong> e.g. 'ic: park place mri'<br>
    <strong>dr: doctorLastName</strong> e.g. 'dr: wieseltier'<br>
    
    &nbsp;</td>
    <td>&nbsp;
    <%try{ %>
    <%if (request.getParameter("ssSearchMe").equalsIgnoreCase("print_rem")){%>
      <input name="ssSearchMe2" type="text" value="print_rem" style="display:none"/>
      <textarea id="temp" style="margin: 2px;width: 272px;height: 163px;" onchange="print_rem()"></textarea>
      <input name="ssSearchMe" type="text" style="display:none"/>
      <%} %></td>
      <%}catch(Exception E){%>
    	  <input name="temp" type="text" onchange="search_main()" id="quick-main" autocomplete="off"/>
          <input name="ssSearchMe" type="text" style="display:none"/>
          <input name="ssSearchMe2" type="text" style="display:none"/>
     <% }%>
  </tr>
  <tr >
    <td></td>
    <td>&nbsp;</td>
    <script type="text/javascript">
    	$(document).ready(function(){
    		$("#questionIcon").hover(
    			function(){
    				$("#searchTooltip").css('display','block');
    			},
    			function(){
    				$("#searchTooltip").css('display','none');
    			}
    		);
    	});
    </script>
    <td style="position:relative;"><input name="input" type="submit" class="inputButton_md_Default" value="Search" />
    <img id="questionIcon" src="images/question-icon.png" style="position:relative;top:10px;left:10px;">
    <img id="searchTooltip" src="images/search_tooltip.png" style="position: absolute;left: -13px;top: -119px;display:none;"></td>
  </tr>
</table>

    <input type="hidden" name="maxResults" value=<%=maxResults%> />
    <input type="hidden" name="startID" value=<%=startID%> />
    <input type="hidden" name="ssSearchDICOM" value=<%=mySearchDICOM%> />
    <br />
  
    </form>
    <%
}
else
{



%>


<table width=100% border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
<tr>
<td>

<form name="selfForm" id="selfForm"  method="POST" action="tNIM3_CaseAccount_PayerID_query.jsp">
  <table border=0 cellspacing=1 width='100%' cellpadding=2>
    <tr > 
      <td colspan=2 nowrap="nowrap" class=tdHeader>
        Search Cases/Patients Here:<br />
  </td>
      <td colspan=1 align=right nowrap="nowrap" class=tdHeader>
  <div id="dPrev"></div>
  </td>
      <td colspan=1 class=tdHeader> 
  <div id="dNext"></div>
        
        </td>
      <td class=tdHeader colspan=1> 
	<%
	if (searchDICOM)
	{
		%>
          <input class="inputButton_md_Default" type="button" name="Button" value="Search Again" id="search-again" onclick="document.location='tNIM3_CaseAccount_PayerID_query.jsp?ssSearchDICOM=Y';" align="middle">
          <%
	}
	else
	{
		%>
          <input class="inputButton_md_Default" type="button" name="Button" value="Search Again" id="search-again" onclick="document.location='tNIM3_CaseAccount_PayerID_query.jsp';" align="middle">
          <%
	}
	%>
        </td>
    </tr>
    <tr class=tdHeader> 
      <td colspan=1>Action</td>
      <td colspan=1> 
        Carrier
        </td>
        <%if (!mySearchMe2.equalsIgnoreCase("print_rem")){ %>
      <td colspan=1> 
        Claim Number
        </td>
        <%} %>
      <td colspan=1> 
        First Name
        </td>
      <td colspan=1> 
        Last Name
        </td>
      <td colspan=1> 
        ScanPass
        </td>
      <td colspan=1> 
Type
        </td>
      <td colspan=1> 
DOS
        </td>
        
        <%if (mySearchMe.equalsIgnoreCase("ic")){%>
        <td colspan=1> 
Imaging Center
        </td>
        <%}
        else if (mySearchMe.equalsIgnoreCase("dr")){%>
        <td colspan=1> 
Referring Dr
        </td>
	    <%}
        else if (mySearchMe2.equalsIgnoreCase("print_rem")){%>
        <td colspan=1> 
Pay Out
        </td>
        <td colspan=1> 
Remittance
        </td>
	    <%}
	    else if (mySearchMe.equalsIgnoreCase("ch")){%>
        <td colspan=1> 
Check1
        </td>
        <td colspan=1> 
Check2
        </td>
        <td colspan=1> 
Check3
        </td>
        
      <%}%>
      
	<%
	if (searchDICOM)
	{
		out.print("<td align=center>");
		out.print("DICOM");
		out.print("</td>");
	}
	%>
    </tr>
<%



String myWhere = "where (tNIM3_CaseAccount.PayerID >0 ";
boolean theFirst = false;

try
{
	if (!mySearchMe.equalsIgnoreCase(""))
	{
		if (!theFirst) { myWhere+=" and ";} //Enabled pattern matching before and after search entry, not case sensitive -AJ 05/20/2015
		if (mySearchMe.indexOf("%")>=0 && mySearchMe2.equalsIgnoreCase("undefined")){myWhere += "CaseClaimNumber ILIKE '%" + mySearchMe + "%' OR PatientLastName ILIKE '%" + mySearchMe +"%' OR ScanPass ILIKE '%" + mySearchMe +"%' OR PatientFirstName ILIKE '" + mySearchMe2 +"%'";}
		else if (mySearchMe.indexOf("=")==0 && mySearchMe2.equalsIgnoreCase("undefined")){myWhere += "lower(CaseClaimNumber) = '" + mySearchMe.substring(1, mySearchMe.length())+"' OR lower(PatientLastName) = '" + mySearchMe.substring(1, mySearchMe.length())+"' OR lower(PatientFirstName) = '" + mySearchMe2.substring(1, mySearchMe.length())+"' OR lower(ScanPass) = '" + mySearchMe.substring(1, mySearchMe.length())+"'";}
		else if (mySearchMe.length()<=3 && mySearchMe2.equalsIgnoreCase("undefined")){myWhere += "CaseClaimNumber ILIKE '%" + mySearchMe +"%' OR PatientLastName ILIKE '%" + mySearchMe +"%' OR ScanPass ILIKE '%" + mySearchMe +"%'";}
		else if (mySearchMe2.equalsIgnoreCase("print_rem")) {myWhere += "lower(ScanPass) in ("+mySearchMe+")";}
		else if (!mySearchMe2.equalsIgnoreCase("undefined")) {myWhere += "PatientLastName ILIKE '%" + mySearchMe +"%' AND PatientFirstName ILIKE '"+mySearchMe2+"%'";}
		else {myWhere += "CaseClaimNumber ILIKE '%" + mySearchMe + "%' OR PatientLastName ILIKE '%" + mySearchMe +"%' OR ScanPass ILIKE '%" + mySearchMe +"%'";}
		theFirst = false;
	}
	myWhere += ")";
	theFirst=false;
	//System.out.println(myWhere);
	if (theFirst||myWhere.equalsIgnoreCase(")"))
	{
		myWhere = "";
	}
	

}
catch(Exception e)
{
out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
	String mySQL = "null";
	String mySQL2 = "null";
	if (isScheduler&&searchDICOM)
	{	
//		mySQL = "select * from tNIM3_CaseAccount " + myWhere + " AND tNIM3_CaseAccount.PayerID > 0  order by case when tNIM3_CaseAccount.PayerID=50 then 1 when (tNIM3_CaseAccount.CaseClaimNumber='') then 2 when (tNIM3_CaseAccount.CaseStatusID=3) then 3 when (true) then 4 END, patientfirstname LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
		mySQL = "select tNIM3_CaseAccount.CaseID, tNIM3_Encounter.EncounterID, tNIM3_Service.ServiceID from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid INNER JOIN tNIM3_Service on tNIM3_Service.encounterid = tNIM3_Encounter.encounterid " + myWhere + " AND (tNIM3_Service.DICOM_ShowImage=1)  order by case when tNIM3_CaseAccount.PayerID=50 then 1 when (tNIM3_CaseAccount.CaseClaimNumber='') then 2 when (tNIM3_CaseAccount.CaseStatusID=3) then 3 when (true) then 4 END, patientfirstname LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
	}
	else if (isScheduler && mySearchMe.equalsIgnoreCase("ch"))
	{	
		mySQL = "select tNIM3_CaseAccount.CaseID, tNIM3_Encounter.EncounterID from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid where paidtoprovidercheck1number like '%"+ mySearchMe2.toLowerCase() +"%' or paidtoprovidercheck2number like '%"+ mySearchMe2.toLowerCase() +"%' or paidtoprovidercheck3number like '%"+ mySearchMe2.toLowerCase() +"%' order by encounterid desc LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
	}
	else if (isScheduler && mySearchMe.equalsIgnoreCase("ic"))
	{	
//		mySQL = "select * from tNIM3_CaseAccount " + myWhere + " AND tNIM3_CaseAccount.PayerID > 0  order by case when tNIM3_CaseAccount.PayerID=50 then 1 when (tNIM3_CaseAccount.CaseClaimNumber='') then 2 when (tNIM3_CaseAccount.CaseStatusID=3) then 3 when (true) then 4 END, patientfirstname LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
		mySQL = "select tNIM3_CaseAccount.CaseID, tNIM3_Encounter.EncounterID from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid inner join tnim3_appointment on tnim3_appointment.initialencounterid = tnim3_encounter.encounterid INNER JOIN tpracticemaster on tpracticemaster.practiceid = tnim3_appointment.providerid where lower(practicename) like '%"+ mySearchMe2.toLowerCase() +"%' order by encounterid desc LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
	}
	else if (isScheduler && mySearchMe.equalsIgnoreCase("dr"))
	{	
//		mySQL = "select * from tNIM3_CaseAccount " + myWhere + " AND tNIM3_CaseAccount.PayerID > 0  order by case when tNIM3_CaseAccount.PayerID=50 then 1 when (tNIM3_CaseAccount.CaseClaimNumber='') then 2 when (tNIM3_CaseAccount.CaseStatusID=3) then 3 when (true) then 4 END, patientfirstname LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
		mySQL = "select tNIM3_CaseAccount.CaseID, tNIM3_Encounter.EncounterID from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid inner join tuseraccount on tuseraccount.userid = tnim3_referral.referringphysicianid where lower(contactlastname) like '%"+ mySearchMe2.toLowerCase() +"%' order by encounterid desc LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
	}
	else if (mySearchMe2.equalsIgnoreCase("print_rem"))
	{	
//		mySQL = "select * from tNIM3_CaseAccount " + myWhere + " AND tNIM3_CaseAccount.PayerID > 0  order by case when tNIM3_CaseAccount.PayerID=50 then 1 when (tNIM3_CaseAccount.CaseClaimNumber='') then 2 when (tNIM3_CaseAccount.CaseStatusID=3) then 3 when (true) then 4 END, patientfirstname LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
		mySQL = "select tNIM3_CaseAccount.CaseID, tNIM3_Encounter.EncounterID from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid " + myWhere+" order by lower(scanpass) = "+mySearchMe.replaceAll(", \'"," desc, lower(scanpass) = \'")+" desc";
	}
	else if (isScheduler)
	{	
//		mySQL = "select * from tNIM3_CaseAccount " + myWhere + " AND tNIM3_CaseAccount.PayerID > 0  order by case when tNIM3_CaseAccount.PayerID=50 then 1 when (tNIM3_CaseAccount.CaseClaimNumber='') then 2 when (tNIM3_CaseAccount.CaseStatusID=3) then 3 when (true) then 4 END, patientfirstname LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
		mySQL = "select tNIM3_CaseAccount.CaseID, tNIM3_Encounter.EncounterID from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid " + myWhere + "  order by case when tNIM3_CaseAccount.PayerID=50 then 1 when (tNIM3_CaseAccount.CaseClaimNumber='') then 2 when (tNIM3_CaseAccount.CaseStatusID=3) then 3 when (true) then 4 END, patientfirstname LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
	}
	else if (isAdjuster3)
	{	
		mySQL = "select * from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid " + myWhere + " AND tNIM3_CaseAccount.PayerID =" + iPayerID + "   and tNIM3_Encounter.encounterstatusid in (1,6, 9)  order by case when tNIM3_CaseAccount.PayerID=50 then 1 when (tNIM3_CaseAccount.CaseClaimNumber='') then 2 when (tNIM3_CaseAccount.CaseStatusID=3) then 3 when (true) then 4 END, patientfirstname LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
	}
	else if (isAdjuster2)
	{	
		mySQL = "select * from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid " + myWhere + " AND tNIM3_CaseAccount.PayerID =" + iPayerID + "   and (tNIM3_CaseAccount.adjusterid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.adjusterid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) OR tNIM3_CaseAccount.nursecasemanagerid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.nursecasemanagerid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) OR tNIM3_CaseAccount.caseadministratorid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministratorid in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) OR tNIM3_CaseAccount.caseadministrator2id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator2id in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) OR tNIM3_CaseAccount.caseadministrator3id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator3id in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) OR tNIM3_CaseAccount.caseadministrator4id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator4id in (select userid from tUserAccount where payerid=" + iPayerID + " AND managerid=" + CurrentUserAccount.getUserID() + " ) ) and tNIM3_Encounter.encounterstatusid in (1,6, 9)   order by case when tNIM3_CaseAccount.PayerID=50 then 1 when (tNIM3_CaseAccount.CaseClaimNumber='') then 2 when (tNIM3_CaseAccount.CaseStatusID=3) then 3 when (true) then 4 END, patientfirstname LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
	}
	else if (isAdjuster)
	{	
		mySQL = "select * from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid " + myWhere + " AND (tNIM3_CaseAccount.adjusterid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.nursecasemanagerid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministratorid = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator2id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator3id = " + CurrentUserAccount.getUserID() + " OR tNIM3_CaseAccount.caseadministrator4id = " + CurrentUserAccount.getUserID() + ") and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+")  order by case when tNIM3_CaseAccount.PayerID=50 then 1 when (tNIM3_CaseAccount.CaseClaimNumber='') then 2 when (tNIM3_CaseAccount.CaseStatusID=3) then 3 when (true) then 4 END, patientfirstname LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
	}
	else if (isProvider3)
	{
//		mySQL = "select tNIM3_Encounter.encounterid from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  where tNIM3_Referral.referringphysicianid in (select userid from tUserAccount where payerid = " + iPayerID + ") and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by referraldate desc LIMIT 10";
		mySQL = "select * from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  " + myWhere + " AND (tNIM3_Referral.referringphysicianID = " + CurrentUserAccount.getUserID() + " OR tNIM3_Referral.referringphysicianid in (select userid from tUserAccount where payerid = " + iPayerID + ") )  and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by patientfirstname  LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
	}
	else if (isProvider2)
	{
		mySQL = "select tNIM3_CaseAccount.caseID, tNIM3_Referral.receivedate from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid  " + myWhere + " AND (tNIM3_Referral.referringphysicianID = " + CurrentUserAccount.getUserID() + " OR tNIM3_Referral.referringphysicianID in (select userid from tUserAccount where managerid=" + CurrentUserAccount.getUserID() + " and managerid>0 ) )  and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by patientfirstname  LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
	}
	else if (isProvider)
	{	
		mySQL = "select * from tNIM3_CaseAccount INNER JOIN tNIM3_Referral on tNIM3_Referral.caseid = tNIM3_CaseAccount.caseid INNER JOIN tNIM3_Encounter on tNIM3_Encounter.referralid = tNIM3_Referral.referralid " + myWhere + " AND    (tNIM3_Referral.referringphysicianid = " + CurrentUserAccount.getUserID() + " ) and tNIM3_Encounter.encounterstatusid in ("+NIMUtils.getSQL_Encounters_Active()+") order by  patientfirstname LIMIT " + (maxResults+1) + " OFFSET " + startID + "";
	}

out.println("<!-- hello"+mySQL+"-->");
	myRS = mySS.executeStatement(mySQL);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
	NIM3_EncounterObject2 myEO2 = null;
	bltNIM3_Service working_bltNIM3_Service  =  new bltNIM3_Service();
	if (searchDICOM&&isScheduler)
	{
		working_bltNIM3_Service  =  new bltNIM3_Service(new Integer(myRS.getString("ServiceID")));
	}
	
	try
	{
		myEO2  =  new NIM3_EncounterObject2(new Integer(myRS.getString("EncounterID")),"loading");
	}
	catch (Exception NullEnc)
	{
	}
	cnt++;
	//if (cnt>=startID&&cnt<=startID+maxResults)
	if (mySearchMe2.equalsIgnoreCase("print_rem")){
		maxResults=1000000000;
	}
if (cnt<=maxResults)
{
	cnt2++;
	
	boolean isVoid = false;
	String myClassTR = "tdBase";
	String myClassText = "tdBase";
	if (cnt2%2!=0)
	{
		myClassTR = "tdBaseAlt";
	}
	if (isScheduler&&myEO2!=null&&myEO2.getNIM3_CaseAccount().getCaseClaimNumber().equalsIgnoreCase(""))
	{
		myClassTR = "requiredFieldMain";
	}
	else if (isScheduler&&myEO2!=null&&(myEO2.getNIM3_Encounter().getEncounterStatusID()==2||myEO2.getNIM3_Encounter().getEncounterStatusID()==3||myEO2.getNIM3_Encounter().getEncounterStatusID()==5||myEO2.getNIM3_Encounter().getEncounterStatusID()==10) )
	{
		isVoid = true;
		myClassText = "tdBaseFade1 textVoid";
	}
	else if (isScheduler&&myEO2!=null&&(myEO2.getNIM3_Encounter().getEncounterStatusID()==8||myEO2.getNIM3_Encounter().getEncounterStatusID()==11||myEO2.getNIM3_Encounter().getEncounterStatusID()==12 )  )
	{
		myClassTR = "tdBaseAlt_Action2";
	}
	out.print("<tr class="+myClassTR+">");
	
/*	else if (false)
	{
		out.print("<td><input class=\"inputButton_md_Default\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=edit&EDITID=" + working_bltNIM3_CaseAccount.getCaseID() + "&KM=p'\" value=\"Edit\">");
//	out.print("&nbsp;<input class=\"inputButton_md_Default\" value=\"Msg\"  type=button onClick = \"this.disabled=false;modalPost('PatientAccountID', modalWin('tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=viewcomm&EDITID=" + working_bltNIM3_CaseAccount.getCaseID() + "&KM=p','MsgWindow','dialogWidth:900px;dialogHeight:400px','status=yes,scrollbars=yes,resizable=yes,width=800,height=400'))\">");
		out.print("&nbsp;<input class=\"inputButton_md_Default\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=viewreports&EDITID=" + working_bltNIM3_CaseAccount.getCaseID() + "&KM=p'\" value=\"Reports\"></td>");
	}
*/

	if (myEO2!=null&&myEO2.getNIM3_CaseAccount().getPayerID().intValue()==50&&isScheduler)
	{
		out.print("<td colspan=1><input class=\"inputButton_md_Action1\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=" + myEO2.getNIM3_CaseAccount().getCaseID() + "&KM=p'\" value=\"[NEW]Submitted Case\"></td>");
	}
	else if (myEO2!=null&&isVoid&&isScheduler)
	{
		out.print("<td colspan=1><input class=\"inputButton_md_Stop\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=" + myEO2.getNIM3_CaseAccount().getCaseID() + "&SelectedEncounterID=" + myEO2.getNIM3_Encounter().getEncounterID() + "&KM=p'\" value=\"Voided\">");
	}
	else if (myEO2!=null&&!myEO2.getNIM3_CaseAccount().isComplete()&&isScheduler)
	{
		out.print("<td colspan=1><input class=\"inputButton_md_Action1\"  type=button onClick = \"this.disabled=true;alert('This case is missing critical information\\n\\nClick OK to be taken to the Case Edit screen in order to complete this data.  The Referral Dashboard for this case may not be available until all of this information is complete.');document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=" + myEO2.getNIM3_CaseAccount().getCaseID() + "&SelectedEncounterID=" + myEO2.getNIM3_Encounter().getEncounterID() + "&KM=p'\" value=\"Open\">");
	}
	else if (myEO2!=null&&isScheduler)
	{
	out.print("<td colspan=1><input class=\"inputButton_md_Default\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=" + myEO2.getNIM3_CaseAccount().getCaseID() + "&SelectedEncounterID=" + myEO2.getNIM3_Encounter().getEncounterID() + "&KM=p'\" value=\"Open\"></td>");
	}
	else if (myEO2!=null&&(isAdjuster||isProvider))
	{
	out.print("<td align=center  ><input class=\"inputButton_md_Default\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=viewpayer&EDITID=" + myEO2.getNIM3_CaseAccount().getCaseID() + "&KM=p'\" value=\"View Details\"></td>");
	}
	out.print("<td>");
//	out.print("<strong>" + (new bltCaseAccountStatusLI (working_bltNIM3_CaseAccount.getCaseStatusID() ).getCaseStatusShort()+"</strong>"));
//	out.print("<br>");
	out.print("Carrier: <strong><span class=\""+myClassText+"\">" + myEO2.getNIM3_PayerMaster().getPayerName() + "</span></strong>");
	out.print("</td>");
	if (!mySearchMe2.equalsIgnoreCase("print_rem")){
	out.print("<td><span class=\""+myClassText+"\">");
	if (myEO2!=null&&myEO2.getNIM3_CaseAccount().getCaseClaimNumber().equalsIgnoreCase(""))
	{
		out.print("<img src=\"images/blink_missing.gif\"onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=edit&EDITID=" + myEO2.getNIM3_CaseAccount().getCaseID() + "&KM=p'\">");
	}
	else if (myEO2!=null)
	{
		out.print(myEO2.getNIM3_CaseAccount().getCaseClaimNumber() );
	}
	out.print("</span></td>");
	}
	out.print("<td><span class=\""+myClassText+"\">");
	out.print(myEO2.getNIM3_CaseAccount().getPatientFirstName());
	out.print("</span></td>");
	out.print("<td><span class=\""+myClassText+"\">");
	out.print(myEO2.getNIM3_CaseAccount().getPatientLastName());
	out.print("</span></td>");
	out.print("<td><strong><span class=\""+myClassText+"\">");
	out.print(myEO2.getNIM3_Encounter().getScanPass());
	out.print("</span></strong></td>");
	out.print("<td><span class=\""+myClassText+"\">");
	if (searchDICOM&&isScheduler)
	{
		out.print(working_bltNIM3_Service.getCPT() + "<br>" + myEO2.getEncounterType_Display());
	}
	else
	{
		out.print(myEO2.getEncounterType_Display());
	}
	out.print("</span></td>");
	if (searchDICOM)
	{
		out.print("<td align=center><span class=\""+myClassText+"\">");
		out.print("Y");
		out.print("</span></td>");
	}
	out.print("<td><span class=\""+myClassText+"\">");
	
	if (myEO2.getNIM3_Encounter().getDateOfService().toString().equals("Wed Jan 01 00:00:00 PST 1800")){
		out.print("No DOS");
	}
	else{
		out.println(displayDateTimeSDF.format(myEO2.getNIM3_Encounter().getDateOfService()));
	}
	
	out.print("</span></td>");
	
	if (mySearchMe.equalsIgnoreCase("ic")){
		
		out.print("<td><span class=\""+myClassText+"\">");
		out.print(myEO2.getAppointment_PracticeMaster().getPracticeName());
		out.print("</span></td>");
	}
	else if (mySearchMe.equalsIgnoreCase("dr")){
		out.print("<td><span class=\""+myClassText+"\">");
		out.print(myEO2.getReferral_ReferringDoctor().getContactFirstName()+" "+myEO2.getReferral_ReferringDoctor().getContactLastName());
		out.print("</span></td>");
	}
	else if (mySearchMe2.equalsIgnoreCase("print_rem")){
		out.print("<td>Expected out: <strong>"+myEO2.getBillingTotals().getProvider_AllowAmount()+"</strong><br>Actual out: <strong>"+myEO2.getBillingTotals().getProvider_ActualAmount()+"</strong></td>");
		out.print("<td>");
		if (myEO2.getNIM3_Encounter().getBillingHCFA_FromProvider_FileID()>0){
			out.print("<a href='tNIM3_Encounter_bill-rem_print.jsp?nullParam=null&EDITID="+myEO2.getNIM3_Encounter().getEncounterID()+"&KM=p&EDIT=print_rem_pro' target=\"_blank\" style=\"border: 1px solid;padding: 4px 8px;text-decoration: none;color: black;background: yellow;margin: 7px;\" onclick=\"this.style.backgroundColor='#ccc';\">Print</a>");
		}
		else{
			out.print("<a href='tNIM3_Encounter_bill-rem_print.jsp?nullParam=null&EDITID="+myEO2.getNIM3_Encounter().getEncounterID()+"&KM=p&EDIT=print_rem_pro' target=\"_blank\" style=\"border: 1px solid;padding: 4px 8px;text-decoration: none;color: black;background: red;margin: 7px;\" onclick=\"this.style.backgroundColor='#ccc';\">Print</a>");
		}
		out.print("</td>");
    }
	else if (mySearchMe.equalsIgnoreCase("ch")){
		out.print("<td><span class=\""+myClassText+"\">");
		out.print(myEO2.getNIM3_Encounter().getPaidToProviderCheck1Number());
		out.print("</span></td>");
		out.print("<td><span class=\""+myClassText+"\">");
		out.print(myEO2.getNIM3_Encounter().getPaidToProviderCheck2Number());
		out.print("</span></td>");
		out.print("<td><span class=\""+myClassText+"\">");
		out.print(myEO2.getNIM3_Encounter().getPaidToProviderCheck3Number());
		out.print("</span></td>");
	}
	out.print("</tr>");
}
   }
mySS.closeAll();
endCount = cnt;


if (startID>=endCount)
{
//startID = endCount-maxResults;

}

if (maxResults<=0)
{
maxResults=5;
}


if (startID<=0)
{
startID=0;
}


if (startID>0)
{
	%>
    	<script language="javascript">
        document.getElementById('dPrev').innerHTML='<a href="tNIM3_CaseAccount_PayerID_query.jsp?startID=<%=(startID-maxResults)%>&maxResults=<%=maxResults%>&ssSearchMe=<%=mySearchMe%>&ssSearchMe2=<%=mySearchMe2%>&ssSearchDICOM=<%=mySearchDICOM%>"><< previous <%=maxResults%></a> ';
        </script>
	<%
}
//if ((startID+maxResults)<endCount)
if (endCount>maxResults)
{
//out.println("[" + startID + "]");
%>
    	<script language="javascript">
        document.getElementById('dNext').innerHTML='<a href="tNIM3_CaseAccount_PayerID_query.jsp?startID=<%=(startID+maxResults)%>&maxResults=<%=maxResults%>&ssSearchMe=<%=mySearchMe%>&ssSearchMe2=<%=mySearchMe2%>&ssSearchDICOM=<%=mySearchDICOM%>"> next >> <%=maxResults%></a>         ';
        </script>
        <%
}




%>

</table> 


<%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>



  </table>


</td>
</tr>
</table>
</td>
</tr>
</table>      

    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

    </td></tr></table><br>
    
<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>