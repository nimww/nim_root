 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*, java.util.*" %>
 
<div class="container">

<%
searchDB2 conn = new searchDB2();

String query ="select getencounterstatus(en.encounterstatusid), ua_at.contactfirstname||' '||ua_at.contactlastname assigntedto, to_char(rf.uniquecreatedate,'mm/dd/yyyy hh:mmam') receivedate, pm.payername carrier, getEncounterType(en.encountertypeid) encountertype, ca.caseclaimnumber, en.scanpass, ca.patientfirstname||' '||ca.patientlastname patientname, ca.employername, to_char(ca.patientdob, 'mm/dd/yyyy') patientdob, to_char(ap.appointmenttime,'mm/dd/yyyy hh:mmam') appointment from tnim3_caseaccount ca INNER JOIN tnim3_referral rf on rf.caseid = ca.caseID LEFT JOIN tnim3_encounter en on en.referralid=rf.referralid LEFT JOIN tnim3_appointment ap on ap.appointmentid = en.appointmentid join tuseraccount ua_at on  ua_at.userid = ca.assignedtoid join tnim3_payermaster pm on pm.payerid = ca.payerid where (ca.PayerID >0  and  ( reportfileid>0 AND EncounterStatusID in (9) and SentTo_Bill_Pay<'2008-01-01') and EncounterStatusID in (0,1,4,7,8,9,11,12) and EncounterTypeID >=0) order by case when ca.PayerID=50 then 1 when (ca.CaseClaimNumber='') then 2 when (en.encounterstatusid=4) then 3 when (true) then 4 END, PatientLastName LIMIT 11 OFFSET 0";
query = "select md.contactfirstname||' '||md.contactlastname mdname, md.contactaddress1||' '||COALESCE(md.contactaddress2,'') mdaddress, md.contactcity mdcity, getState(md.contactstateid) mdstate, md.contactzip mdzip, md.usernpi mdnpi, md.contactphone mdphone, md.contactfax mdfax, to_char(ap.appointmenttime, 'mm/dd/yyyy hh:miam') appointment, pm.practicename pname, pm.officeaddress1||' '||COALESCE(pm.officeaddress2, '') paddress, pm.officecity pcity, getState(pm.officestateid) pstate, pm.officezip pzip, pm.npinumber pnpi, getServices(en.encounterid)  from tnim3_encounter en join tnim3_referral rf on rf.referralid = en.referralid join tuseraccount md on md.userid = rf.referringphysicianid join tnim3_appointment ap on ap.appointmentid = en.appointmentid join tpracticemaster pm on pm.practiceid = ap.providerid where encounterid = 13455";

query = "SELECT main.\"EncounterID\" FROM \"vMQ4_Encounter_NoVoid_BP\" main JOIN tnim3_caseaccount on tnim3_caseaccount.caseclaimnumber = main.\"CaseClaimNumber\" where main.\"Encounter_StatusID\" = '1' and main.\"Parent_PayerID\" = '307'";

java.sql.ResultSet rs = conn.executeStatement(query);

/* for (int x = 1; x < rs.getMetaData().getColumnCount()+1;x++){
	out.print("private String "+rs.getMetaData().getColumnName(x)+";<br>");
}
out.print("<br><br>");
for (int x = 1; x < rs.getMetaData().getColumnCount()+1;x++){
	out.print("rs.getString(\""+rs.getMetaData().getColumnName(x)+"\"),");
} */

ArrayList<NIM3_EncounterObject2> eo = new ArrayList<NIM3_EncounterObject2>();

while(rs.next()){
	eo.add(new NIM3_EncounterObject2(rs.getInt(1),""));
}

//\"DateOfInjury\", main.\"CaseClaimNumber\", main.\"PatientLastName\", main.\"Parent_PayerName\", main.\"PayerName\", tnim3_caseaccount.patientssn " +
out.print("<table>");
out.print("<tr><th>Date of Injury</th><th>Claim Number</th><th>Patient Last Name</th><th>Payer Name</th><th></th><th>SSN</th></tr>");
for(NIM3_EncounterObject2 ob : eo){
	out.print("<tr>");
	
	out.print("<td>"+ob.getNIM3_CaseAccount().getDateOfInjury()+"</td>");
	out.print("<td>"+ob.getNIM3_CaseAccount().getCaseClaimNumber()+"</td>");
	out.print("<td>"+ob.getNIM3_CaseAccount().getPatientLastName()+"</td>");
	out.print("<td>"+ob.getPayerFullName()+"</td>");
	out.print("<td>"+(ob.getNIM3_CaseAccount().getPatientSSN().length()==11 ?ob.getNIM3_CaseAccount().getPatientSSN().substring(7, 11):"")+(ob.getNIM3_CaseAccount().getPatientSSN().length()==9 && !ob.getNIM3_CaseAccount().getPatientSSN().equals("Not Given")?ob.getNIM3_CaseAccount().getPatientSSN().substring(5, 9):"")+"</td>");
	//out.print("<td>"+ob.getNIM3_CaseAccount().getPatientSSN()+"</td>");
	
	out.print("</tr>");
}
out.print("</table>");

%>


</div>

a