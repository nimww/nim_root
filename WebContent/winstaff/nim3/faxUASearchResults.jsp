<%@page language="java" contentType="application/json; charset=UTF-8" import="java.io.*,com.winstaff.*, java.util.*, java.sql.*,com.google.gson.Gson;"%>
<%!class uaResult {
		String value;
		int id;

		public uaResult(String value, int id) {
			this.value = value;
			this.id = id;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
	}%>
<%
	List<uaResult> uaResultList = new ArrayList<uaResult>();

	if (request.getParameter("term") != null && !request.getParameter("term").isEmpty()) {
		String search = request.getParameter("term");

		String query = "SELECT\n" +
				"	userid,\n" +
				"	ua.contactfirstname || ' ' || ua.contactlastname||' - '|| pm.payername \n" +
				"from tuseraccount ua\n" +
				"join tnim3_payermaster pm on pm.payerid = ua.payerid\n" +
				"where LOWER (\n" +
				"		ua.contactfirstname || ' ' || ua.contactlastname\n" +
				"	) ~ lower('" + search + "')\n" +
				"ORDER BY\n" +
				"	userid desc\n" +
				"limit 15";
		searchDB2 db = new searchDB2();

		ResultSet rs = db.executeStatement(query);

		try {
			while (rs.next()) {
				uaResultList.add(new uaResult(rs.getString(2), rs.getInt(1)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closeAll();
		}
	}
	response.setHeader("Access-Control-Allow-Origin", "*");
	out.println(new Gson().toJson(uaResultList));
%>
