<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: out\jsp\tNIM3_CommTrack_main_NIM3_CommTrack_CaseID_form_authorize.jsp
    Created on Jun/24/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iCaseID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
        Integer requestID = null;
        if (request.getParameter("EDITID")!=null)
        {
            requestID = new Integer(request.getParameter("EDITID"));
            out.println(requestID);
        }
    bltNIM3_CommTrack_List        bltNIM3_CommTrack_List        =    new    bltNIM3_CommTrack_List(iCaseID,"CommTrackID="+requestID,"");

//declaration of Enumeration
    bltNIM3_CommTrack        working_bltNIM3_CommTrack;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_CommTrack_List.elements();
    %>
    <%
    if (eList.hasMoreElements())
    {
        leCurrentElement    = (ListElement) eList.nextElement();
        working_bltNIM3_CommTrack  = (bltNIM3_CommTrack) leCurrentElement.getObject();
        pageControllerHash.put("iCommTrackID",working_bltNIM3_CommTrack.getCommTrackID());
        pageControllerHash.put("sKeyMasterReference",request.getParameter("KM"));
        session.setAttribute("pageControllerHash",pageControllerHash);
        //Parameter Pass Code here
String parameterPassString ="";
java.util.Enumeration myParameterPassList = request.getParameterNames();
while (myParameterPassList.hasMoreElements())
{
	String myName = (String)myParameterPassList.nextElement();
	String myS = (String) request.getParameter(myName);
	parameterPassString+="&"+myName + "=" + myS;
}
        String targetRedirect = "tNIM3_CommTrack_form.jsp?nullParam=null"+parameterPassString    ;
        if (request.getParameter("EDIT").equalsIgnoreCase("del"))
        {
            targetRedirect = "tNIM3_CommTrack_form_delete.jsp?routePageReference=sParentReturnPage"+parameterPassString    ;
        }
        else if (request.getParameter("EDIT").equalsIgnoreCase("openflow"))
        {
            targetRedirect = DataControlUtils.getOpenFlow("tNIM3_CommTrack_main_NIM3_CommTrack_CaseID_form_authorize")   ;
        }
        response.sendRedirect(targetRedirect);
    }
    else
    {
   out.println("invalid where query");
    }

  }
  else
  {
   out.println("illegal");
  }
}
else
{
out.println("Your Security Level does not permit you to View this.");
}
%>




