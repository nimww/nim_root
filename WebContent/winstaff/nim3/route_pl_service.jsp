<%@page contentType="text/html" language="java" import="java.text.DecimalFormat ,com.winstaff.*, com.winstaff.password.*,     org.apache.http.*" %><%/*

    filename: out\jsp\tNIM3_Encounter_form.jsp
    Created on May/28/2008
    Created by: Scott Ellis
*/%><%@ include file="../generic/CheckLogin.jsp" %><%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    Integer        iServiceID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")&&pageControllerHash.containsKey("iServiceID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        iServiceID        =    (Integer)pageControllerHash.get("iServiceID");
        accessValid = true;    
	}
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("M/d/yyyy HH:mm:ss");
      java.text.SimpleDateFormat displayDateTimePL = new java.text.SimpleDateFormat("M/d/yyyy");

//initial declaration of list class and parentID
    bltNIM3_Service        NIM3_Service        =    new    bltNIM3_Service(iServiceID,UserSecurityGroupID);
    bltNIM3_Encounter        NIM3_Encounter        =    new    bltNIM3_Encounter(iEncounterID,UserSecurityGroupID);
	bltNIM3_Referral NIMR = new bltNIM3_Referral(NIM3_Encounter.getReferralID());
	bltNIM3_CaseAccount NCA = new bltNIM3_CaseAccount(NIMR.getCaseID());

//fields


		out.print("SP: " + NIM3_Encounter.getScanPass());
		out.print("<hr>MRN: " + NIM3_Service.getDICOM_MRN());
		out.print("<hr>ACC: " + NIM3_Service.getDICOM_AccessionNumber());
		out.print("<hr>DOB: " + displayDateTimePL.format(NCA.getPatientDOB()));
		out.print("<hr>DOS: " + displayDateTimePL.format(NIM3_Encounter.getDateOfService()));
		out.print("<hr>PN: " + NIM3_Service.getDICOM_PN());
		out.print("<hr>TS: " + displayDateTimeSDF.format(PLCUtils.getNowDate(false)));
		out.print("<hr>");
		org.apache.http.impl.client.DefaultHttpClient httpclient = new org.apache.http.impl.client.DefaultHttpClient();
//		org.apache.http.client.methods.HttpPost httpost = new org.apache.http.client.methods.HttpPost("https://www.petlinq.com/PETLinQVr4/StudyView.aspx");
//		org.apache.http.client.methods.HttpPost httpost = new org.apache.http.client.methods.HttpPost("https://secure.nextimagemedical.com/winstaff/nim3/signIn_sub.jsp");
//		org.apache.http.client.methods.HttpGet httpget = new org.apache.http.client.methods.HttpGet("http://www.nextimagemedical.com");
//		org.apache.http.client.methods.HttpPost httpost = new org.apache.http.client.methods.HttpPost("https://www.petlinq.com/PETLinQ/StudyView.aspx");        
		org.apache.http.client.methods.HttpPost httpost = new org.apache.http.client.methods.HttpPost("https://www.RISLinQ.com/RISLinQ/StudyView.aspx");        
		
		java.util.List <org.apache.http.NameValuePair> nvps = new java.util.ArrayList <org.apache.http.NameValuePair>();
//		nvps.add(new org.apache.http.message.BasicNameValuePair("LogonUserName", "nim3payer"));
//		nvps.add(new org.apache.http.message.BasicNameValuePair("LogonUserPassword", "12345678"));
		nvps.add(new org.apache.http.message.BasicNameValuePair("UserId", "211110"));
/* 		nvps.add(new org.apache.http.message.BasicNameValuePair("UniqueKey", "DPNxtImgeM2PT01")); */
		nvps.add(new org.apache.http.message.BasicNameValuePair("UniqueKey", "2661884"));
		nvps.add(new org.apache.http.message.BasicNameValuePair("PatientName", NIM3_Service.getDICOM_PN()));
		nvps.add(new org.apache.http.message.BasicNameValuePair("DOB", displayDateTimePL.format(NCA.getPatientDOB())));
		nvps.add(new org.apache.http.message.BasicNameValuePair("DOS", displayDateTimePL.format(NIM3_Encounter.getDateOfService())));
		nvps.add(new org.apache.http.message.BasicNameValuePair("AccessionNumber", NIM3_Service.getDICOM_AccessionNumber()));
		nvps.add(new org.apache.http.message.BasicNameValuePair("MRNNumber", NIM3_Service.getDICOM_MRN()));
		nvps.add(new org.apache.http.message.BasicNameValuePair("TimeStamp", displayDateTimeSDF.format(PLCUtils.getNowDate(false))));
		httpost.setEntity(new org.apache.http.client.entity.UrlEncodedFormEntity(nvps, org.apache.http.protocol.HTTP.UTF_8));
		org.apache.http.HttpResponse response2 = httpclient.execute(httpost);
		org.apache.http.HttpEntity entity = response2.getEntity();
//		String myResponse = org.apache.http.util.EntityUtils.toString(entity);
//		replaceSubstring myRS =new replaceSubstring();
		
//        out.println(myRS.getReplacedSubstring(myResponse,",resizable=no",",resizable=yes");
		out.println(org.apache.http.util.EntityUtils.toString(entity));												
		



/*		org.apache.http.client.ResponseHandler<String> responseHandler = new org.apache.http.impl.client.BasicResponseHandler();
        String responseBody = httpclient.execute(httpost, responseHandler);
        out.println(responseBody);		
*/		
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>