<%@page contentType="text/html" language="java" import="com.winstaff.*"%>
<%
	PayerExceptionObject payerExceptionObject = new PayerExceptionObject();
%>
<!DOCTYPE html>
<html data-ng-app="">
<head>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
<style type="text/css">
.max-cell {
	max-width: 200px !important;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
</style>
</head>

<body data-ng-controller="userController">
	<div class="container">
		<h3>Payer Exception Application</h3>
		<div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Edit</th>
						<th>Payer Id</th>
						<th>Practice Id</th>
						<th>CPTs</th>
						<th>FS</th>
						<th>Percentage</th>
						<th>Fee Type</th>
					</tr>
				</thead>
				<tbody>
					<tr data-ng-repeat="payerException in payerExceptionList">
						<td>
							<button class="btn" data-ng-click="editPayerException(payerException.payerExceptionId)">
								<span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Edit
							</button>
						</td>
						<td>{{ payerException.payerId }}</td>
						<td>{{ payerException.practiceId }}</td>
						<td class="max-cell">{{ payerException.cpts }}</td>
						<td>{{ payerException.fs }}</td>
						<td>{{ payerException.percentage }}</td>
						<td>{{ payerException.feeType }}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div>
			<button class="btn btn-success" data-ng-click="editUser('new')">
				<span class="glyphicon glyphicon-user"></span> Create New Exception
			</button>
			<hr>

			<h3 data-ng-show="edit">Create New Exception:</h3>
			<h3 data-ng-hide="edit">Edit Exception:</h3>

			<form class="form-horizontal">
				<div class="form-group">
					<div class="col-sm-6">
						<label class="col-md-6 col-lg-2 ccontrol-label">PayerId</label>
						<div class="col-md-6 col-lg-4">
							<input type="text" data-ng-model="payerId" data-ng-disabled="!edit" placeholder="PayerId">
						</div>
						<label class="col-md-6 col-lg-2 ccontrol-label">PracticeID</label>
						<div class="col-md-6 col-lg-4">
							<input type="text" data-ng-model="practiceId" data-ng-disabled="!edit" placeholder="PracticeId">
						</div>
					</div>
					<div class="col-sm-6">
						<label class="col-md-6 col-lg-2 ccontrol-label">FS</label>
						<div class="col-md-6 col-lg-4">
							<input type="text" data-ng-model="fs" data-ng-disabled="!edit" placeholder="FS">
						</div>
						<label class="col-md-6 col-lg-2 ccontrol-label">Percentage</label>
						<div class="col-md-6 col-lg-4">
							<input type="text" data-ng-model="percentage" data-ng-disabled="!edit" placeholder="Percentage">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label">CPTs</label>
					<div class="col-sm-8">
						<textarea data-ng-model="cpts" placeholder="CPTs" class="form-control" placeholder="CPTs seperated by commas"></textarea>
					</div>
					<label class="col-sm-1 control-label">Fee Type</label>
					<div class="col-sm-2">
						<select class="form-control" data-ng-model="feeType">
							<option data-ng-repeat="feeType in feeTypes" value="{{feeType}}">{{feeType}}</option>

						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label">Insert Modals</label>
					<div class="col-sm-8">
						<button class="btn btn-danger">Clear</button>
						<button class="btn btn-primary">mr_wo</button>
						<button class="btn btn-primary">mr_w</button>
						<button class="btn btn-primary">mr_wwo</button>
						<button class="btn btn-warning">ct_wo</button>
						<button class="btn btn-warning">ct_w</button>
						<button class="btn btn-warning">ct_wwo</button>
					</div>

				</div>
			</form>

			<hr>
			<button class="btn btn-success" data-ng-disabled="error || incomplete">
				<span class="glyphicon glyphicon-save"></span> Save Changes
			</button>
		</div>
	</div>
	<script>
		function userController($scope) {

			$scope.payerExceptionList = <%=payerExceptionObject.getExceptionList()%>
			$scope.feeTypes = <%=payerExceptionObject.getFeeType()%>
			$scope.edit = true;
			$scope.error = false;
			$scope.incomplete = false;

			$scope.editPayerException = function(id) {
				$scope.payerId = $scope.payerExceptionList[id].payerId;
				$scope.practiceId = $scope.payerExceptionList[id].practiceId;
				$scope.fs = $scope.payerExceptionList[id].fs;
				$scope.percentage = $scope.payerExceptionList[id].percentage;
				$scope.cpts = $scope.payerExceptionList[id].cpts;
				$scope.feeType = $scope.payerExceptionList[id].feeType;
			};

		}
	</script>

</body>
</html>