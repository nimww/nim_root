<%@page language="java" import="com.winstaff.*"%>
<%/*

    filename: out\jsp\tNIM3_PayerMaster_form.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_report.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<body onLoad="document.getElementById('ContactLastName').focus();">
<%
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
  //page security
  if (isReporter)
  {
	  pageControllerHash.remove("iUserID_Reporter");
	  pageControllerHash.remove("iUserID");
	session.setAttribute("pageControllerHash",pageControllerHash);
	String savedURL = null;
    if (pageControllerHash.containsKey("URL_UserAccount_Sales_Query")) 
    {
        savedURL        =    (String)pageControllerHash.get("URL_UserAccount_Sales_Query");
		if (savedURL.equalsIgnoreCase("")){
			savedURL =null;
		}
	}

%>

<table border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <span class=title>Contact Search
        </span>
      <%


try
{

String myParentPayerID = "";
String myPrimaryContactID = "";
String myParentPayerName = "";
String myPayerID = "";
String myPayerName = "";
String myUserID = "";
String myLogonUserName = "";
String myContactLastName = "";
String myContactFirstName = "";
String myContactPhone = "";
String myContactEmail = "";
String myAccountType = "";
String orderBy = "firstname";
int startID = 0;
int maxResults = 50;

boolean firstDisplay = false;

try
{
	myPayerID = request.getParameter("PayerID").toLowerCase();
	myPrimaryContactID = request.getParameter("PrimaryContactID").toLowerCase();
	maxResults = Integer.parseInt(request.getParameter("maxResults"));
	startID = Integer.parseInt(request.getParameter("startID"));
	myParentPayerID = request.getParameter("ParentPayerID").toLowerCase();
	myParentPayerName = request.getParameter("ParentPayerName").toLowerCase();
	myPayerName = request.getParameter("PayerName").toLowerCase();
	myUserID = request.getParameter("UserID").toLowerCase();
	myLogonUserName = request.getParameter("LogonUserName").toLowerCase();
	myContactLastName = request.getParameter("ContactLastName").toLowerCase();
	myContactFirstName = request.getParameter("ContactFirstName").toLowerCase();
	myContactPhone = request.getParameter("ContactPhone").toLowerCase();
	myContactEmail = request.getParameter("ContactEmail").toLowerCase();
	myAccountType = request.getParameter("AccountType").toLowerCase();
	orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
	maxResults = 50;
	startID = 0;
	firstDisplay = true;
	myParentPayerID = "";
	myPrimaryContactID = "";
	myPayerID = "";
	myParentPayerName = "";
	myPayerName = "";
	myUserID = "";
	myLogonUserName = "";
	myContactLastName = "";
	myContactFirstName = "";
	myContactPhone = "";
	myContactEmail = "";
	myAccountType = "";
	orderBy = "ParentPayerID";
	myPayerID = request.getParameter("PayerID");
	if (myPayerID==null||myPayerID.equalsIgnoreCase("")){
		myPayerID = "";
	} else {
		myPayerID = myPayerID.toLowerCase();
		firstDisplay = false;
	}
}


if (firstDisplay&&savedURL==null)
{
%>



<table cellpadding=2 cellspacing=2>
<tr>
<td width=10>&nbsp;</td>

<td>
<form name="form1"  method="GET" action="tUserAccount_Sales_query.jsp">
  <table width="400" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#003333">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
        <tr>
         <td class=tdHeaderAlt>
         Primary Contact
         </td>
         <td>
                
<select name="PrimaryContactID"  id="PrimaryContactID" />
<option value="-1">All</option>
<option value="0">Unassigned/Empty</option>
    <%
{
		searchDB2 mySS_List = new searchDB2();
		java.sql.ResultSet myRS_List = null;;
		try
		{
			String mysql=("select Userid, Status, LogonUserName, contactfirstname, contactlastname from tUserAccount where AllowAsPrimaryContact =1 order by contactfirstname, contactlastname");  //matches iOpen Type = Payer Type
			myRS_List = mySS_List.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS_List!=null&&myRS_List.next())
			{
				if (new Integer(myRS_List.getString("UserID")).intValue()==CurrentUserAccount.getUserID())
				{
					%>
					<option selected value="<%=myRS_List.getString("UserID")%>"><%=myRS_List.getString("contactfirstname")%> <%=myRS_List.getString("contactlastname")%></option>
					<%
				}
				else
				{
					%>
					<option  value="<%=myRS_List.getString("UserID")%>"><%=myRS_List.getString("contactfirstname")%> <%=myRS_List.getString("contactlastname")%></option>
					<%
				}
			}
			mySS_List.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
%>
    </select>  
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Parent PayerID
         </td>
         <td>
         <input type=text name="ParentPayerID">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Parent Payer Name
         </td>
         <td>
         <input type=text name="ParentPayerName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         PayerID
         </td>
         <td>
         <input type=text name="PayerID">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Payer Name
         </td>
         <td>
         <input type=text name="PayerName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         ContactID
         </td>
         <td>
         <input type=text name="UserID">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Logon Name
         </td>
         <td>
         <input type=text name="LogonUserName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Last Name
         </td>
         <td>
         <input id="ContactLastName" type=text name="ContactLastName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         First Name
         </td>
         <td>
         <input type=text name="ContactFirstName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Phone
         </td>
         <td>
         <input type=text name="ContactPhone">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Email
         </td>
         <td>
         <input type=text name="ContactEmail">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Account Type
         </td>
         <td>
         <input type=text name="AccountType">
         </td>
        </tr>
            <tr>
            <td class=tdHeaderAlt><p>Sort by:</p></td>
            <td> 
              <select name=orderBy>
                <option value="Internal_PrimaryContact">Primary Contact</option>
                <option value="ParentPayerName">Parent Payer Name</option>
                <option value="PayerName">Payer Name</option>
                <option value="LogonUserName">Logon Name</option>
                <option selected="selected" value="ContactLastName">Last Name</option>
                <option value="ContactFirstName">First Name</option>
                <option value="ContactPhone">Phone</option>
                <option value="ContactEmail">Email</option>
                <option value="AccountType">Account Type</option>
              </select>
            </td>
          </tr>
          <tr bgcolor="#CCCCCC"> 
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td> 
              <input type=hidden name="startID" value=0>
              <input type=hidden name="maxResults" value=50>
              <input type="submit" name="Submit2" value="Submit">
            </td>
            <td>&nbsp;</td>
          </tr>
        </table>
</td>
    </tr>
  </table>
  </form>
  <%
}
else if (firstDisplay&&savedURL!=null)
{
pageControllerHash.remove("URL_UserAccount_Sales_Query");
session.setAttribute("pageControllerHash",pageControllerHash);


%>
			<script language="javascript">
		//    alert( '<%=savedURL%>');
			document.location = '<%=savedURL%>';
		
			</script>
        <%

}
else
{


db_PreparedStatementObject myDPSO = new db_PreparedStatementObject(null);String myWhere = "where (";
boolean theFirst = true;

try
{
if (!myParentPayerID.equalsIgnoreCase(""))
{
    if (!myParentPayerID.equalsIgnoreCase(""))
    {
        if (!theFirst) { myWhere+=" and ";}
        myWhere += "ParentPayer.PayerID = ?";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myParentPayerID));
        theFirst = false;
    }
}
if (!myPrimaryContactID.equalsIgnoreCase("")&&!myPrimaryContactID.equalsIgnoreCase("-1"))
{
    if (!myPrimaryContactID.equalsIgnoreCase(""))
    {
        if (!theFirst) { myWhere+=" and ";}
        myWhere += "tUserAccount.Internal_PrimaryContact = ?";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myPrimaryContactID));
        theFirst = false;
    }
}
if (!myParentPayerName.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myParentPayerName.indexOf("%")>=0)
    {
        myWhere += "lower(ParentPayer.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myParentPayerName));
    }
    else if (myParentPayerName.length()<=3)
    {
        myWhere += "lower(ParentPayer.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myParentPayerName + "%"));
    }
    else
    {
        myWhere += "lower(ParentPayer.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myParentPayerName + "%"));
    }
    theFirst = false;
}
if (!myPayerID.equalsIgnoreCase(""))
{
    if (!myPayerID.equalsIgnoreCase(""))
    {
        if (!theFirst) { myWhere+=" and ";}
        myWhere += "tnim3_payermaster.PayerID = ?";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myPayerID));
        theFirst = false;
    }
}
if (!myPayerName.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myPayerName.indexOf("%")>=0)
    {
        myWhere += "lower(tnim3_payermaster.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPayerName));
    }
    else if (myPayerName.length()<=3)
    {
        myWhere += "lower(tnim3_payermaster.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myPayerName + "%"));
    }
    else
    {
        myWhere += "lower(tnim3_payermaster.PayerName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myPayerName + "%"));
    }
    theFirst = false;
}
if (!myUserID.equalsIgnoreCase(""))
{
    if (!myUserID.equalsIgnoreCase(""))
    {
        if (!theFirst) { myWhere+=" and ";}
        myWhere += "tUserAccount.UserID = ?";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.INT_TYPE,myUserID));
        theFirst = false;
    }
}
if (!myLogonUserName.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myLogonUserName.indexOf("%")>=0)
    {
        myWhere += "lower(tUserAccount.LogonUserName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myLogonUserName));
    }
    else if (myLogonUserName.length()<=3)
    {
        myWhere += "lower(tUserAccount.LogonUserName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myLogonUserName + "%"));
    }
    else
    {
        myWhere += "lower(tUserAccount.LogonUserName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myLogonUserName + "%"));
    }
    theFirst = false;
}
if (!myContactLastName.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myContactLastName.indexOf("%")>=0)
    {
        myWhere += "lower(tUserAccount.ContactLastName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactLastName));
    }
    else if (myContactLastName.length()<=3)
    {
        myWhere += "lower(tUserAccount.ContactLastName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactLastName + "%"));
    }
    else
    {
        myWhere += "lower(tUserAccount.ContactLastName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactLastName + "%"));
    }
    theFirst = false;
}
if (!myContactFirstName.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myContactFirstName.indexOf("%")>=0)
    {
        myWhere += "lower(tUserAccount.ContactFirstName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactFirstName));
    }
    else if (myContactFirstName.length()<=3)
    {
        myWhere += "lower(tUserAccount.ContactFirstName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactFirstName + "%"));
    }
    else
    {
        myWhere += "lower(tUserAccount.ContactFirstName) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactFirstName + "%"));
    }
    theFirst = false;
}
if (!myContactPhone.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myContactPhone.indexOf("%")>=0)
    {
        myWhere += "lower(tUserAccount.ContactPhone) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactPhone));
    }
    else if (myContactPhone.length()<=3)
    {
        myWhere += "lower(tUserAccount.ContactPhone) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactPhone + "%"));
    }
    else
    {
        myWhere += "lower(tUserAccount.ContactPhone) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactPhone + "%"));
    }
    theFirst = false;
}
if (!myContactEmail.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myContactEmail.indexOf("%")>=0)
    {
        myWhere += "lower(tUserAccount.ContactEmail) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactEmail));
    }
    else if (myContactEmail.length()<=3)
    {

        myWhere += "lower(tUserAccount.ContactEmail) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myContactEmail + "%"));
    }
    else
    {
        myWhere += "lower(tUserAccount.ContactEmail) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myContactEmail + "%"));
    }
    theFirst = false;
}
if (!myAccountType.equalsIgnoreCase(""))
{
    if (!theFirst) { myWhere+=" and ";}
    if (myAccountType.indexOf("%")>=0)
    {
        myWhere += "lower(tUserAccount.AccountType) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myAccountType));
    }
    else if (myAccountType.length()<=3)
    {
        myWhere += "lower(tUserAccount.AccountType) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,myAccountType + "%"));
    }
    else
    {
        myWhere += "lower(tUserAccount.AccountType) LIKE lower(?)";
        myDPSO.addSQLParameter(new SQLParameterType(SQLParameterType.STRING_TYPE,"%" + myAccountType + "%"));
    }
    theFirst = false;
}
myWhere += ")";

//System.out.println(myWhere);
if (theFirst||myWhere.equalsIgnoreCase(")"))
{
    myWhere = "";
}


}
catch(Exception e)
{
    out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;
String mySQL = "SELECT (PCUA.ContactFirstName || ' ' || PCUA.ContactLastName) as \"PC_FullName\", tuseraccount.logonusername, tuseraccount.status,  tuseraccount.Internal_PrimaryContact, tuseraccount.accounttype, tuseraccount.userid, tuseraccount.contactfirstname, tuseraccount.contactlastname, tuseraccount.contactemail, tuseraccount.contactphone, tnim3_payermaster.payerid as PayerID, tnim3_payermaster.payername as PayerName, ParentPayer.payerid as ParentPayerID, ParentPayer.payername as ParentPayerName FROM tuseraccount Left JOIN tnim3_payermaster ON tuseraccount.payerid = tnim3_payermaster.payerid Left JOIN tnim3_payermaster AS ParentPayer ON tnim3_payermaster.parentpayerid = ParentPayer.payerid LEFT JOIN tUserAccount as PCUA on PCUA.userid = tUseraccount.Internal_PrimaryContact " + myWhere + " order by " + orderBy + " LIMIT " + (maxResults+1) + " OFFSET " + startID + "";


pageControllerHash.put("URL_UserAccount_Sales_Query",request.getRequestURL()+"?"+request.getQueryString());
session.setAttribute("pageControllerHash",pageControllerHash);

try
{
   myDPSO.setSQL(mySQL);
   //out.print(mySQL);
   myRS = mySS.executePreparedStatement(myDPSO);
}
catch(Exception e)
{
    out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

    int endCount = 0;

    int cnt=0;
    int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
        cnt++;
        if (cnt<=maxResults)
        {
            cnt2++;
            String myClass = "tdBase";
            if (cnt2%2==0)
            {
                myClass = "tdBaseAlt";
            }
			if (myRS.getInt("Status")==3){
				myClass = "tdBaseAltVoid tdBaseFade1";
			}
			
			myMainTable +="<tr  class="+myClass+"  onMouseOver=\"this.className='highlight'\" onMouseOut=\"this.className='"+myClass+"'\" >";
            myMainTable +="<td>";
			myMainTable +="<a class=tdHeader  href = 'gsn_nim3b/tUserAccount_form_authorize.jsp?EDIT=salesview&EDITID="+myRS.getString("UserID")+"'>View</a>";
            myMainTable +="</td>";
            myMainTable +="<td>"+cnt+"</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("PC_FullName")+"";
            myMainTable +="</td>";
            myMainTable +="<td>";
			myMainTable +="<a class=linkBase href = '#' onclick=\"document.getElementById('ParentPayerID').value ='"+myRS.getString("ParentPayerID")+"';document.getElementById('searchForm').submit();\" >";
            myMainTable +=myRS.getString("ParentPayerID")+"";
            myMainTable +="</a>";
            myMainTable +="</td>";
            myMainTable +="<td>";
			myMainTable +="<a class=linkBase href = '#' onclick=\"document.getElementById('ParentPayerName').value ='"+myRS.getString("ParentPayerName")+"';document.getElementById('searchForm').submit();\" >";
            myMainTable +=myRS.getString("ParentPayerName")+"";
            myMainTable +="</a>";
            myMainTable +="</td>";
            myMainTable +="<td>";
			myMainTable +="<a class=linkBase href = '#' onclick=\"document.getElementById('PayerID').value ='"+myRS.getString("PayerID")+"';document.getElementById('searchForm').submit();\" >";
            myMainTable +=myRS.getString("PayerID")+"";
            myMainTable +="</a>";
            myMainTable +="</td>";
            myMainTable +="<td>";
			myMainTable +="<a class=linkBase href = '#' onclick=\"document.getElementById('PayerName').value ='"+myRS.getString("PayerName")+"';document.getElementById('searchForm').submit();\" >";
            myMainTable +=myRS.getString("PayerName")+"";
            myMainTable +="</a>";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("UserID")+"&nbsp;";
            myMainTable +="</td>";
            myMainTable +="<td>";
			myMainTable +=myRS.getString("LogonUserName")+"&nbsp;";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("ContactLastName")+"&nbsp;";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("ContactFirstName")+"&nbsp;";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("ContactPhone")+"&nbsp;";
            myMainTable +="</td>";
            myMainTable +="<td>";
            myMainTable +=myRS.getString("ContactEmail")+"&nbsp;";
            myMainTable +="</td>";
            myMainTable +="<td>";
			myMainTable +="<a class=linkBase href = '#' onclick=\"document.getElementById('AccountType').value ='"+myRS.getString("AccountType")+"';document.getElementById('searchForm').submit();\" >";
            myMainTable +=myRS.getString("AccountType")+"&nbsp;";
            myMainTable +="</a>";
            myMainTable +="</td>";
            myMainTable +="</tr>";
        }
   }
    mySS.closeAll();
    endCount = cnt;

    if (maxResults<=0)
    {
        maxResults=10;
    }

    if (startID<=0)
    {
        startID=0;
    }




%>
  <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</p>
<table width=100% border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
<tr>
<td width=10>&nbsp;</td>
<td>
<table width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#003333">
<tr>
<td>

<form name="selfForm" id="searchForm" method="POST" action="tUserAccount_Sales_query.jsp">
  <table border=1 cellspacing=0 width='100%' cellpadding=3 bordercolor="#666666">
    <tr > 
      <td colspan=4 class=tdHeader><p>&nbsp;</p></td>
      <td colspan=1 class=tdBase align=right nowrap="nowrap">
        <%if (startID>0)
{%><a href="tUserAccount_Sales_query.jsp?startID=<%=(startID-maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&ParentPayerID=<%=myParentPayerID%>&ParentPayerName=<%=myParentPayerName%>&PayerID=<%=myPayerID%>&PayerName=<%=myPayerName%>&UserID=<%=myUserID%>&LogonUserName=<%=myLogonUserName%>&ContactLastName=<%=myContactLastName%>&ContactFirstName=<%=myContactFirstName%>&ContactPhone=<%=myContactPhone%>&ContactEmail=<%=myContactEmail%>&AccountType=<%=myAccountType%>"><< previous <%=maxResults%></a> 
          <%
}
else
{
%><p>&nbsp;</p><%
}
%></td>
      <td colspan=1 class=tdBase nowrap="nowrap"> 
        <%
if (cnt>cnt2)
{
%>
<a href="tUserAccount_Sales_query.jsp?startID=<%=(startID+maxResults)%>&maxResults=<%=maxResults%>&orderBy=<%=orderBy%>&ParentPayerID=<%=myParentPayerID%>&ParentPayerName=<%=myParentPayerName%>&PayerID=<%=myPayerID%>&PayerName=<%=myPayerName%>&UserID=<%=myUserID%>&LogonUserName=<%=myLogonUserName%>&ContactLastName=<%=myContactLastName%>&ContactFirstName=<%=myContactFirstName%>&ContactPhone=<%=myContactPhone%>&ContactEmail=<%=myContactEmail%>&AccountType=<%=myAccountType%>"> next >> <%=maxResults%></a>         
        <%
}
else
{
%><p>&nbsp;</p><%
}


}
catch(Exception e)
{
out.println("PrevNext:"+e);
}




try{

%>
      </td>
      <td colspan=1 class=tdHeader nowrap> 
<input type=hidden name=maxResults value=<%=maxResults%> >
<input type=hidden name=startID value=0 ><p>Sort:
<select name=orderBy>
      <option value=Internal_PrimaryContact <%if (orderBy.equalsIgnoreCase("Internal_PrimaryContact")){out.println(" selected");}%> >Primary Contact</option>
      <option value=ParentPayerName <%if (orderBy.equalsIgnoreCase("ParentPayerName")){out.println(" selected");}%> >Parent Payer Name</option>
      <option value=PayerName <%if (orderBy.equalsIgnoreCase("PayerName")){out.println(" selected");}%> >Payer Name</option>
      <option value=LogonUserName <%if (orderBy.equalsIgnoreCase("LogonUserName")){out.println(" selected");}%> >Logon Name</option>
      <option value=ContactLastName <%if (orderBy.equalsIgnoreCase("ContactLastName")){out.println(" selected");}%> >Last Name</option>
      <option value=ContactFirstName <%if (orderBy.equalsIgnoreCase("ContactFirstName")){out.println(" selected");}%> >First Name</option>
      <option value=ContactPhone <%if (orderBy.equalsIgnoreCase("ContactPhone")){out.println(" selected");}%> >Phone</option>
      <option value=ContactEmail <%if (orderBy.equalsIgnoreCase("ContactEmail")){out.println(" selected");}%> >Email</option>
      <option value=AccountType <%if (orderBy.equalsIgnoreCase("AccountType")){out.println(" selected");}%> >Account Type</option>
</select></p>
      </td> 
      <td class=tdHeader colspan=1> 
        <p  align="center"> 
          <input type="Submit" name="Submit" value="Submit" align="middle">
        </p>
      </td>
    </tr>
    <tr class=tdHeader> 
      <td  colspan=1 >&nbsp;     </td>
      <td  colspan=1 width=50>&nbsp;     </td>
      <td colspan=1>
      
      
<select name="PrimaryContactID"  id="PrimaryContactID" />
<option value="-1">All</option>
<option value="0" <%if (myPrimaryContactID.equalsIgnoreCase("0")) {%> selected <%}%> >Unassigned/Empty</option>
    <%
{
		searchDB2 mySS_List = new searchDB2();
		java.sql.ResultSet myRS_List = null;;
		try
		{
			String mysql=("select Userid,LogonUserName, status, contactfirstname, contactlastname from tUserAccount where AllowAsPrimaryContact =1 order by contactfirstname, contactlastname");  //matches iOpen Type = Payer Type
			myRS_List = mySS_List.executeStatement(mysql);
		}
		catch(Exception e)
		{
			out.println("ResultsSet:"+e);
		}
		try
		{
			while (myRS_List!=null&&myRS_List.next())
			{
				if (new Integer(myRS_List.getString("UserID")).intValue()==new Integer(myPrimaryContactID))
				{
					%>
					<option selected value="<%=myRS_List.getString("UserID")%>"><%=myRS_List.getString("contactfirstname")%> <%=myRS_List.getString("contactlastname")%></option>
					<%
				}
				else
				{
					%>
					<option  value="<%=myRS_List.getString("UserID")%>"><%=myRS_List.getString("contactfirstname")%> <%=myRS_List.getString("contactlastname")%></option>
					<%
				}
			}
			mySS_List.closeAll();
		}
		catch(Exception e)
		{
			out.println("While:"+e);
		}
}
%>
    </select>         
      
      <td colspan=1> 
        <input type="text" name="ParentPayerID"  id="ParentPayerID" value="<%=myParentPayerID%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="ParentPayerName"  id="ParentPayerName" value="<%=myParentPayerName%>" size="15">
      </td>
      <td colspan=1> 
        <input type="text" name="PayerID"  id="PayerID" value="<%=myPayerID%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="PayerName"  id="PayerName" value="<%=myPayerName%>" size="15">
      </td>
      <td colspan=1> 
        <input type="text" name="UserID" value="<%=myUserID%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="LogonUserName" value="<%=myLogonUserName%>" size="15">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactLastName" value="<%=myContactLastName%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactFirstName" value="<%=myContactFirstName%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactPhone" value="<%=myContactPhone%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="ContactEmail" value="<%=myContactEmail%>" size="15">
      </td>
      <td colspan=1> 
        <input type="text" name="AccountType"  id="AccountType" value="<%=myAccountType%>" size="10">
      </td>
    </tr>
    <tr class=tdHeaderAlt> 
    <td class=tdHeaderAlt nowrap="nowrap">Action</td>
    <td nowrap="nowrap" class="tdHeaderAlt">#</td>
    <td colspan=1 nowrap="nowrap" class="tdHeaderAlt"> Primary Contact </td>
    <td colspan=1 nowrap="nowrap" class="tdHeaderAlt"> 
      Parent PayerID
    </td>
      <td colspan=1 nowrap="nowrap" class="tdHeaderAlt"> 
        Parent Payer Name
      </td>
      <td colspan=1 nowrap="nowrap" class="tdHeaderAlt"> 
        PayerID
      </td>
      <td colspan=1 nowrap="nowrap" class="tdHeaderAlt"> 
        Payer Name
      </td>
      <td colspan=1 nowrap="nowrap" class="tdHeaderAlt"> 
        UserID
      </td>
      <td colspan=1 nowrap="nowrap" class="tdHeaderAlt"> 
        Logon Name
      </td>
      <td colspan=1 nowrap="nowrap" class="tdHeaderAlt"> 
        Last Name
      </td>
      <td colspan=1 nowrap="nowrap" class="tdHeaderAlt"> 
        First Name
      </td>
      <td colspan=1 nowrap="nowrap" class="tdHeaderAlt"> 
        Phone
      </td>
      <td colspan=1 nowrap="nowrap" class="tdHeaderAlt"> 
        Email
      </td>
      <td colspan=1 nowrap="nowrap" class="tdHeaderAlt"> 
        Account Type
      </td>
    </tr>
    <%=myMainTable%>



</table> 


<%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>



  </table>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table> 
    </td>
  </tr>
</table>





        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>





<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>