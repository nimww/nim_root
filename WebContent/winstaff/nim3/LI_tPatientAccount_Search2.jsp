<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    filename: tnim2_Authorization_main_NIM_Authorization_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>
<%@ include file="../generic/CheckLogin.jsp" %>

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>

<base target=_self>
<%
//initial declaration of list class and parentID
    Integer        iPayerID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat dbdft = new java.text.SimpleDateFormat(PLCUtils.String_dbdft);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

%>


<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td width=10>&nbsp;</td>
    <td> 
      <p class=title>Search Accounts<br>
<%
try
{

String myPatientID = "";
String myPatientSSN = "";
String myPatientFirstName = "";
String myPatientLastName = "";
String myPatientHomePhone = "";
String myPatientCellPhone = "";
String myPatientCity = "";
String myPatientStateID = "0";
String myPatientZIP = "";
String myPatientGender = "";
String myEmployerName = "";
String orderBy = "firstname";
int startID = 0;
int maxResults = 50;
int iOpenID = 0;
String sOpenField = "";

boolean firstDisplay = false;

try
{
sOpenField= (request.getParameter("ssOpenField"));
iOpenID= Integer.parseInt(request.getParameter("ssOpenID"));
maxResults = Integer.parseInt(request.getParameter("maxResults"));
startID = Integer.parseInt(request.getParameter("startID"));
//myPatientID = request.getParameter("PatientID").toLowerCase();
myPatientSSN = request.getParameter("PatientSSN").toLowerCase();
//myPatientFirstName = request.getParameter("PatientFirstName").toLowerCase();
//myPatientLastName = request.getParameter("PatientLastName").toLowerCase();
//myPatientHomePhone = request.getParameter("PatientHomePhone").toLowerCase();
//myPatientCellPhone = request.getParameter("PatientCellPhone").toLowerCase();
//myPatientCity = request.getParameter("PatientCity").toLowerCase();
//myPatientStateID = request.getParameter("PatientStateID").toLowerCase();
//myPatientZIP = request.getParameter("PatientZIP").toLowerCase();
//myPatientGender = request.getParameter("PatientGender").toLowerCase();
//myEmployerName = request.getParameter("EmployerName").toLowerCase();
//orderBy = request.getParameter("orderBy");
}
catch (Exception e)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myPatientID = "";
myPatientSSN = "";
myPatientFirstName = "";
myPatientLastName = "";
myPatientHomePhone = "";
myPatientCellPhone = "";
myPatientCity = "";
myPatientStateID = "0";
myPatientZIP = "";
myPatientGender = "";
myEmployerName = "";
orderBy = "PatientID";
}
if (orderBy == null)
{
maxResults = 50;
startID = 0;
firstDisplay = true;
myPatientID = "";
myPatientSSN = "";
myPatientFirstName = "";
myPatientLastName = "";
myPatientHomePhone = "";
myPatientCellPhone = "";
myPatientCity = "";
myPatientStateID = "0";
myPatientZIP = "";
myPatientGender = "";
myEmployerName = "";
orderBy = "PatientID";
}

if (sOpenField!=null&&sOpenField.indexOf("atientAccountID")>0)
{
	iOpenID = 1;
}
else
{
	iOpenID = 1;

//	out.println("Unrecognized Opening Field");
}

%>

<input class="inputButton_md_Create" onClick = "javascript:document.location = 'tNIM3_PatientAccount_create.jsp?EDIT=new&&EDITID=&ssOpenID=<%=iOpenID%>&ssOpenField=<%=sOpenField%>'" type="Button" name="Create" value="Create" align="middle">
<%
if (myPatientSSN.equalsIgnoreCase(""))
{
	firstDisplay= true;
}
else
{
	firstDisplay= false;
}

String myPatientSSNd = myPatientSSN;
myPatientSSN = com.winstaff.password.Encrypt.encrypt_DESEDE_String(myPatientSSN, DataControlUtils.ENC_KEY);

if (firstDisplay && iOpenID>0)
{
%>



<table width = 735 cellpadding=2 cellspacing=2>
<tr>
<td width=10>&nbsp;</td>
<td class=tdBase><p>&nbsp;</p>
</td>
</tr>
<tr>
<td width=10>&nbsp;</td>

<td>
<form name="form1" method="POST" action="LI_tPatientAccount_Search2.jsp" target="_self">
<input type="hidden" name="ssOpenID" value ="<%=iOpenID%>">
<input type="hidden" name="ssOpenField" value ="<%=sOpenField%>">
  <table width="400" border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#003333">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
        <tr>
         <td class=tdHeaderAlt>&nbsp;</td>
         <td>&nbsp;</td>
        </tr>
<!--        <tr>
         <td class=tdHeaderAlt>
         Employer</td>
         <td>
         <input name="EmployerName" type=text id="EmployerName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Last Name
         </td>
         <td>
         <input name="PatientLastName" type=text id="PatientLastName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         First Name
         </td>
         <td>
         <input name="PatientFirstName" type=text id="PatientFirstName">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Phone
         </td>
         <td>
         <input type=text name="PatientHomePhone">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         Cell
         </td>
         <td>
         <input name="PatientCellPhone" type=text id="PatientCellPhone">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         City
         </td>
         <td>
         <input type=text name="PatientCity">
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         State
         </td>
         <td>
              <select name="PatientStateID">
                <jsp:include page="../generic/tStateLILong.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="0" />
                </jsp:include>
              </select>
         </td>
        </tr>
        <tr>
         <td class=tdHeaderAlt>
         ZIP
         </td>
         <td>
         <input type=text name="PatientZIP">
         </td>
        </tr>
-->
        <tr>
         <td class=tdHeaderAlt>
         SSN
         </td>
         <td>
         <input name="PatientSSN" type=text id="PatientSSN">
         </td>
        </tr>
<!--        <tr>
         <td class=tdHeaderAlt>
         Gender</td>
         <td>
         <input name="PatientGender" type=text id="PatientGender">
         </td>
        </tr>
            <tr>
            <td class=tdHeaderAlt><p>Sort by:</p></td>
            <td> 
              <select name=orderBy>
                <option value="PatientID">ID</option>
                <option value="PatientSSN">Logon Name</option>
                <option value="PatientFirstName">Last Name</option>
                <option value="PatientLastName">Last Name</option>
                <option value="PatientHomePhone">Phone</option>
                <option value="PatientCellPhone">Email</option>
                <option value="PatientCity">Postal Code</option>
                <option value="PatientStateID">State</option>
                <option value="PatientZIP">Postal Code</option>
                <option value="PatientGender">Type</option>
                <option value="EmployerName">Ref ID</option>
              </select>
            </td>
          </tr>
-->
          <tr bgcolor="#CCCCCC"> 
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td> 
              <input type=hidden name="startID" value=0>
              <input type=hidden name="maxResults" value=50>
              <input type="submit" name="Submit2" value="Submit">
            </td>
            <td>&nbsp;</td>
          </tr>
        </table>
</td>
    </tr>
  </table>
  <p>&nbsp;</p>
  </form>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p> 
  <%
}
else if (iOpenID>0)
{


String myWhere = "";
if (iOpenID==1)
{
//	myWhere = "where (tNIM3_PatientAccount.PayerID = " + CurrentUserAccount.getReferenceID() + "  ";
	myWhere = "where ";
}
else if (iOpenID==2)
{
	//myWhere = "where (PatientGender in ('AdjusterID') ";
}
boolean theFirst = true;

try
{
if (!myPatientID.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
myWhere += "PatientID = '" + myPatientID +"'";
theFirst = false;
}
if (!myPatientSSN.equalsIgnoreCase(""))
{
if (!theFirst) { myWhere+=" and ";}
myWhere += "PatientSSN = '" + myPatientSSN+"'";
theFirst = false;
}
if (!myPatientFirstName.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myPatientFirstName.indexOf("%")>=0){myWhere += "lower(PatientFirstName) LIKE '" + myPatientFirstName +"'";}
else if (myPatientFirstName.indexOf("=")==0){myWhere += "lower(PatientFirstName) = '" + myPatientFirstName.substring(1, myPatientFirstName.length())+"'";}
else if (myPatientFirstName.length()<=3){myWhere += "lower(PatientFirstName) LIKE '" + myPatientFirstName +"%'";}
else {myWhere += "lower(PatientFirstName) LIKE '%" + myPatientFirstName +"%'";}
theFirst = false;
}
if (!myPatientLastName.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myPatientLastName.indexOf("%")>=0){myWhere += "lower(PatientLastName) LIKE '" + myPatientLastName +"'";}
else if (myPatientLastName.indexOf("=")==0){myWhere += "lower(PatientLastName) = '" + myPatientLastName.substring(1, myPatientLastName.length())+"'";}
else if (myPatientLastName.length()<=3){myWhere += "lower(PatientLastName) LIKE '" + myPatientLastName +"%'";}
else {myWhere += "lower(PatientLastName) LIKE '%" + myPatientLastName +"%'";}
theFirst = false;
}
if (!myPatientHomePhone.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myPatientHomePhone.indexOf("%")>=0){myWhere += "lower(PatientHomePhone) LIKE '" + myPatientHomePhone +"'";}
else if (myPatientHomePhone.indexOf("=")==0){myWhere += "lower(PatientHomePhone) = '" + myPatientHomePhone.substring(1, myPatientHomePhone.length())+"'";}
else if (myPatientHomePhone.length()<=3){myWhere += "lower(PatientHomePhone) LIKE '" + myPatientHomePhone +"%'";}
else {myWhere += "lower(PatientHomePhone) LIKE '%" + myPatientHomePhone +"%'";}
theFirst = false;
}
if (!myPatientCellPhone.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myPatientCellPhone.indexOf("%")>=0){myWhere += "lower(PatientCellPhone) LIKE '" + myPatientCellPhone +"'";}
else if (myPatientCellPhone.indexOf("=")==0){myWhere += "lower(PatientCellPhone) = '" + myPatientCellPhone.substring(1, myPatientCellPhone.length())+"'";}
else if (myPatientCellPhone.length()<=3){myWhere += "lower(PatientCellPhone) LIKE '" + myPatientCellPhone +"%'";}
else {myWhere += "lower(PatientCellPhone) LIKE '%" + myPatientCellPhone +"%'";}
theFirst = false;
}
if (!myPatientCity.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myPatientCity.indexOf("%")>=0){myWhere += "lower(PatientCity) LIKE '" + myPatientCity +"'";}
else if (myPatientCity.indexOf("=")==0){myWhere += "lower(PatientCity) = '" + myPatientCity.substring(1, myPatientCity.length())+"'";}
else if (myPatientCity.length()<=3){myWhere += "lower(PatientCity) LIKE '" + myPatientCity +"%'";}
else {myWhere += "lower(PatientCity) LIKE '%" + myPatientCity +"%'";}
theFirst = false;
}
if (!myPatientStateID.equalsIgnoreCase("0"))
{
if (!myPatientStateID.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
myWhere += "PatientStateID = " + myPatientStateID +"";
theFirst = false;
}
}
if (!myPatientZIP.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myPatientZIP.indexOf("%")>=0){myWhere += "lower(PatientZIP) LIKE '" + myPatientZIP +"'";}
else if (myPatientZIP.indexOf("=")==0){myWhere += "lower(PatientZIP) = '" + myPatientZIP.substring(1, myPatientZIP.length())+"'";}
else if (myPatientZIP.length()<=3){myWhere += "lower(PatientZIP) LIKE '" + myPatientZIP +"%'";}
else {myWhere += "lower(PatientZIP) LIKE '%" + myPatientZIP +"%'";}
theFirst = false;
}
if (!myPatientGender.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myPatientGender.indexOf("%")>=0){myWhere += "lower(PatientGender) LIKE '" + myPatientGender +"'";}
else if (myPatientGender.indexOf("=")==0){myWhere += "lower(PatientGender) = '" + myPatientGender.substring(1, myPatientGender.length())+"'";}
else if (myPatientGender.length()<=3){myWhere += "lower(PatientGender) LIKE '" + myPatientGender +"%'";}
else {myWhere += "lower(PatientGender) LIKE '%" + myPatientGender +"%'";}
theFirst = false;
}
if (!myEmployerName.equalsIgnoreCase(""))
{
if (true||!theFirst) { myWhere+=" and ";}
if (myEmployerName.indexOf("%")>=0){myWhere += "lower(EmployerName) LIKE '" + myEmployerName +"'";}
else if (myEmployerName.indexOf("=")==0){myWhere += "lower(EmployerName) = '" + myEmployerName.substring(1, myEmployerName.length())+"'";}
else if (myEmployerName.length()<=3){myWhere += "lower(EmployerName) LIKE '" + myEmployerName +"%'";}
else {myWhere += "lower(EmployerName) LIKE '%" + myEmployerName +"%'";}
theFirst = false;
}
//myWhere += ")";

//System.out.println(myWhere);
if (theFirst||myWhere.equalsIgnoreCase(")"))
{
//	myWhere = "where (PatientGender in ('AdjusterID','PhysicianID'))";
//	myWhere += ")";
}


}
catch(Exception e)
{
out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
String mysql=("select * from tNIM3_PatientAccount LEFT JOIN tNIM3_EmployerAccount ON tNIM3_PatientAccount.EmployerID  = tNIM3_EmployerAccount.EmployerID " + myWhere );
//out.print(mysql);
myRS = mySS.executeStatement(mysql);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

String myMainTable= " ";
try{

int endCount = 0;

int cnt=0;
int cnt2=0;
   if (myRS!=null&&myRS.next())
   {
cnt++;
if (cnt>=startID&&cnt<=startID+maxResults)
{
cnt2++;
bltNIM3_PatientAccount working_bltNIM3_PatientAccount  =  new bltNIM3_PatientAccount(new Integer(myRS.getString("PatientID")));
bltNIM3_EmployerAccount working_bltNIM3_EmployerAccount  =  new bltNIM3_EmployerAccount(working_bltNIM3_PatientAccount.getEmployerID());

String myClass = "tdBase";
if (cnt2%2==0)
{
myClass = "tdBaseAlt";
}
myMainTable +="<tr class="+myClass+">";
if (iOpenID>0)
{
//	myMainTable +="<td><input class=\"inputButton_md_Create\" onClick = \"javascript:window.returnValue = '" + myRS.getString("PatientID") + "';window.close();\" type=\"Button\" name=\"Edit\" value=\"Edit\" align=\"middle\"></td>";
	myMainTable +="<td><input class=\"inputButton_md_Create\" onClick = \"javascript:document.location='tNIM3_PatientAccount_authorize.jsp?EDIT=new&&EDITID=" + working_bltNIM3_PatientAccount.getPatientID() + "&ssOpenID=" + iOpenID + "&ssOpenField=" + sOpenField + "';\" type=\"Button\" name=\"Select\" value=\"Select\" align=\"middle\"></td>";
}
else
{
	myMainTable +="<td><a href=\"#\">Select Error</a></td>";
}


myMainTable +="<td>";
myMainTable +=working_bltNIM3_EmployerAccount.getEmployerName()+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable += working_bltNIM3_PatientAccount.getPatientFirstName()+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=working_bltNIM3_PatientAccount.getPatientLastName()+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=working_bltNIM3_PatientAccount.getPatientHomePhone()+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=working_bltNIM3_PatientAccount.getPatientCellPhone()+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=working_bltNIM3_PatientAccount.getPatientCity()+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=(new bltStateLI (new Integer(working_bltNIM3_PatientAccount.getPatientStateID()) ).getShortState()+"");
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=working_bltNIM3_PatientAccount.getPatientZIP()+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=working_bltNIM3_PatientAccount.getPatientSSN()+"";
myMainTable +="</td>";
myMainTable +="<td>";
myMainTable +=working_bltNIM3_PatientAccount.getPatientGender()+"";
myMainTable +="</td>";
myMainTable +="</tr>";
}
   }
   else
   {
	   %>
<br />       No Patients Found.   <a href="LI_tPatientAccount_Search2.jsp">Search Again</a>
	   <%
   }
mySS.closeAll();
endCount = cnt;



if (startID>=endCount)
{
startID = endCount-maxResults;

}

if (maxResults<=0)
{
maxResults=10;
}


if (startID<=0)
{
startID=0;
}


myPatientSSN = myPatientSSNd;


%>
  <script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</p>
<table width=900 border=0 cellpadding=0 cellspacing=0 bordercolor=#003333>
<tr>
<td width=10>&nbsp;</td>
<td>
<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
<tr>
<td>

<form name="selfForm" method="POST" action="LI_tPatientAccount_Search2.jsp">
  <table border=0 cellspacing=1 width='100%' cellpadding=2>
    <tr > 
      <td colspan=4 class=tdHeader><p>Search Here:</p></td>
      <td colspan=1 class=tdBase align=right>
        <%if (startID>0)
{%><a href="LI_tPatientAccount_Search2.jsp?startID=<%=(startID-maxResults)%>&amp;maxResults=<%=maxResults%>&amp;orderBy=<%=orderBy%>&amp;PatientID=<%=myPatientID%>&amp;PatientSSN=<%=myPatientSSN%>&amp;PatientFirstName=<%=myPatientFirstName%>&amp;PatientLastName=<%=myPatientLastName%>&amp;PatientHomePhone=<%=myPatientHomePhone%>&amp;PatientCellPhone=<%=myPatientCellPhone%>&amp;PatientCity=<%=myPatientCity%>&amp;PatientStateID=<%=myPatientStateID%>&amp;PatientZIP=<%=myPatientZIP%>&amp;PatientGender=<%=myPatientGender%>&amp;EmployerName=<%=myEmployerName%>"><< previous <%=maxResults%></a> 
          <%
}
else
{
%><p>&nbsp;</p><%
}
%></td>
      <td colspan=1 class=tdBase> 
        <%
if ((startID+maxResults)<endCount)
{
%>
<a href="LI_tPatientAccount_Search2.jsp?startID=<%=(startID+maxResults)%>&amp;maxResults=<%=maxResults%>&amp;orderBy=<%=orderBy%>&amp;PatientID=<%=myPatientID%>&amp;PatientSSN=<%=myPatientSSN%>&amp;PatientFirstName=<%=myPatientFirstName%>&amp;PatientLastName=<%=myPatientLastName%>&amp;PatientHomePhone=<%=myPatientHomePhone%>&amp;PatientCellPhone=<%=myPatientCellPhone%>&amp;PatientCity=<%=myPatientCity%>&amp;PatientStateID=<%=myPatientStateID%>&amp;PatientZIP=<%=myPatientZIP%>&amp;PatientGender=<%=myPatientGender%>&amp;EmployerName=<%=myEmployerName%>"> next >> <%=maxResults%></a>         
        <%
}
else
{
%><p>&nbsp;</p><%
}


}
catch(Exception e)
{
out.println("PrevNext:"+e);
}




try{

%>
      </td>
      <td colspan=3 class=tdHeader nowrap> 
      </td> 
      <td class=tdHeader colspan=1> 
      </td>
    </tr>
    <tr class=tdHeader> 
      <td  colspan=1 width=50>&nbsp; 

      </td>
      <td colspan=1> 
        <input type="text" name="EmployerName" value="<%=myEmployerName%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="PatientFirstName" value="<%=myPatientFirstName%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="PatientLastName" value="<%=myPatientLastName%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="PatientHomePhone" value="<%=myPatientHomePhone%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="PatientCellPhone" value="<%=myPatientCellPhone%>" size="10">
      </td>
      <td colspan=1> 
        <input type="text" name="PatientCity" value="<%=myPatientCity%>" size="10">
      </td>
      <td colspan=1> 
              <select name="PatientStateID">
                <jsp:include page="../generic/tStateLIShort.jsp" flush="true" > 
                <jsp:param name="CurrentSelection" value="<%=myPatientStateID%>" />
                </jsp:include>
              </select>
      </td>
      <td colspan=1> 
        <input type="text" name="PatientZIP" value="<%=myPatientZIP%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="PatientSSN" value="<%=myPatientSSN%>" size="5">
      </td>
      <td colspan=1> 
        <input type="text" name="PatientGender" value="<%=myPatientGender%>" size="5">
      </td>
    </tr>
    <tr class=tdHeader> 
    <td>Action</td>
      <td colspan=1> 
        Company
      </td>
      <td colspan=1>First</td>
      <td colspan=1> 
        Last</td>
      <td colspan=1> 
        Phone
      </td>
      <td colspan=1> 
        Cell
      </td>
      <td colspan=1>City</td>
      <td colspan=1> 
        State
      </td>
      <td colspan=1> 
        ZIP</td>
      <td colspan=1> 
        SSN</td>
      <td colspan=1> 
        Gender
      </td>
    </tr>
    <%=myMainTable%>



</table> 


<%

}
catch(Exception e)
{
out.println("Display:"+e);
}





}
}
catch (Exception e)
{
out.println("Error???:"+e);
System.out.println("Error:"+e);
}

%>



  </table>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>      <p>&nbsp;</p>
      <p>&nbsp;</p>
    </td>
  </tr>
</table>

<%



  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_ClearID.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>
