<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*    " %>
<%/*
    filename: out\jsp\tNIM3_CaseAccount_form_sub.jsp
    Created on May/14/2009
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%
try
{
//initial declaration of list class and parentID
    Integer        iCaseID        =    null;
    Integer        iEncounterID        =    null;
    Integer        iReferralID        =    null;
    Integer        iEDITID        =    null;
    if ( request.getParameter( "EDITID" ) != null )
    {
    	iEDITID        =    new Integer(request.getParameter ("EDITID"));
    }
    else
    {
    	iEDITID        =    new Integer(0);
    }
    boolean accessValid = false;

   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {

    if (pageControllerHash.containsKey("iCaseID")||pageControllerHash.containsKey("iEncounterID")||pageControllerHash.containsKey("iReferralID")) 
    {
        iCaseID        =    (Integer)pageControllerHash.get("iCaseID");
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        iReferralID        =    (Integer)pageControllerHash.get("iReferralID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
        if (iEncounterID.intValue() != iEDITID.intValue())
        {
			out.print("Error - Collision.  Do you have multiple windows open?");
        	accessValid = false;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);

//initial declaration of list class and parentID
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);


		bltNIM3_CommTrack serviceToCommTracks = new bltNIM3_CommTrack();
		serviceToCommTracks.setEncounterID(iEncounterID);
		serviceToCommTracks.setReferralID(myEO.NIM3_Encounter.getReferralID());
	  	 serviceToCommTracks.setCaseID(myEO.NIM3_Referral.getCaseID());
		 serviceToCommTracks.setMessageName(CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName());
		 bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(CurrentUserAccount.getPayerID());
		 serviceToCommTracks.setMessageCompany(myPM.getPayerName());
		 serviceToCommTracks.setUniqueCreateDate(PLCUtils.getNowDate(false));
		 serviceToCommTracks.setUniqueModifyDate(PLCUtils.getNowDate(false));
		 serviceToCommTracks.setCommStart(PLCUtils.getNowDate(false));
		 serviceToCommTracks.setCommEnd(PLCUtils.getNowDate(false));
		 serviceToCommTracks.setUniqueModifyComments(""+CurrentUserAccount.getLogonUserName());
		 serviceToCommTracks.setAlertStatusCode(new Integer(1));
		 String serviceToCommTracksMsg = "Service Update: <br>";
		 boolean isServiceChanged = false;
		

//    bltNIM3_CaseAccount        myEO.NIM3_CaseAccount        =    new bltNIM3_CaseAccount(iCaseID);
//    bltNIM3_Encounter        myEO.NIM3_Encounter        =    new bltNIM3_Encounter(iEncounterID);
//    bltNIM3_Referral        myEO.NIM3_Referral        =    new bltNIM3_Referral(iReferralID);
	boolean isReferralGo = false;


      String tCaseAccount_tempAN =	myEO.NIM3_CaseAccount.getAuditNotes();
	  String tCaseAccount_tempAN_Status = "";

String tCaseAccount_testChangeID = "0";


searchDB2 mySS = new searchDB2();
java.sql.ResultSet myRS = null;;
String payerIDFromAdj ="";
	String mysql=("select payerid from tuseraccount where userid ='"+request.getParameter("AdjusterID")+"'");
	myRS = mySS.executeStatement(mysql);



	while (myRS!=null&&myRS.next())
	{
		payerIDFromAdj = myRS.getString("payerid");
	}

mySS.closeAll();
try{
	if(myEO.getNIM3_ParentPayerMaster().getPayerID()==951){
		//myEO.NIM3_Encounter.setSentTo_ReqRec_RefDr(PLCUtils.getSubDate("n/a"));
		//myEO.NIM3_Encounter.setSentTo_SP_RefDr(PLCUtils.getSubDate("n/a"));
		//myEO.NIM3_Encounter.setSentToRefDr(PLCUtils.getSubDate("n/a"));
		myEO.NIM3_Encounter.setSentTo_ReqRec_Adj(PLCUtils.getSubDate("n/a"));
		myEO.NIM3_Encounter.setSentToAdj(PLCUtils.getSubDate("n/a"));
	}
}
catch(Exception e){}

try
{
    Integer testObj = new Integer(request.getParameter("PayerID")) ;
    if ( !myEO.NIM3_CaseAccount.getPayerID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPayerID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PayerID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CaseClaimNumber")) ;
    if ( !myEO.NIM3_CaseAccount.getCaseClaimNumber().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setCaseClaimNumber( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount CaseClaimNumber not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CaseCode")) ;
    if ( !myEO.NIM3_CaseAccount.getCaseCode().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setCaseCode( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount CaseCode not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("WCBNumber")) ;
    if ( !myEO.NIM3_CaseAccount.getWCBNumber().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setWCBNumber( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount WCBNumber not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CaseStatusID")) ;
    if ( !myEO.NIM3_CaseAccount.getCaseStatusID().equals(testObj)  )
    {

		 if (testObj.intValue() == 0 ) {tCaseAccount_tempAN_Status = ("NA");}
		 else if (testObj.intValue() == 1 ) {tCaseAccount_tempAN_Status = ("Open");}
		 else if (testObj.intValue() == 2 ) {tCaseAccount_tempAN_Status = ("Closed");}
		 else if (testObj.intValue() == 3 ) {tCaseAccount_tempAN_Status = ("Flag for Review");}
		 else if (testObj.intValue() == 4 ) {tCaseAccount_tempAN_Status = ("Void");}
		 else if (testObj.intValue() == 5 ) {tCaseAccount_tempAN_Status = ("Pending");}

		myEO.NIM3_CaseAccount.setCaseStatusID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount CaseStatusID not set. this is ok-not an error");
}

try
{
	Integer testObj2 = new Integer(payerIDFromAdj) ;
    Integer testObj = new Integer(request.getParameter("AdjusterID")) ;
    if ( !myEO.NIM3_CaseAccount.getAdjusterID().equals(testObj)  )
    { 
         if (!myEO.NIM3_CaseAccount.getAdjusterID().equals(0)){
	         bltNIM3_CommTrack working_bltNIM3_CommTrack = new bltNIM3_CommTrack();
			 working_bltNIM3_CommTrack.setEncounterID(iEncounterID);
		 	 working_bltNIM3_CommTrack.setReferralID(myEO.NIM3_Encounter.getReferralID());
		  	 working_bltNIM3_CommTrack.setCaseID(myEO.NIM3_Referral.getCaseID());
			 working_bltNIM3_CommTrack.setMessageName(CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName());
			 working_bltNIM3_CommTrack.setMessageCompany(myPM.getPayerName());
			 working_bltNIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
			 working_bltNIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
			 working_bltNIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
			 working_bltNIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
			 working_bltNIM3_CommTrack.setUniqueModifyComments(""+CurrentUserAccount.getLogonUserName());
			 working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
			 bltUserAccount ReassignedAccount = new bltUserAccount(testObj);
			 working_bltNIM3_CommTrack.setMessageText("Changed Adj to: " + new bltUserAccount(testObj).getContactFirstName()+" "+new bltUserAccount(testObj).getContactLastName()+" ["+testObj+"]<br>"+
			 "from: "+ new bltUserAccount(myEO.NIM3_CaseAccount.getAdjusterID()).getContactFirstName()+" "+new bltUserAccount(myEO.NIM3_CaseAccount.getAdjusterID()).getContactLastName()+" ["+myEO.NIM3_CaseAccount.getAdjusterID()+"]");
			 working_bltNIM3_CommTrack.commitData();
         }
		 
		 myEO.NIM3_CaseAccount.setAdjusterID( testObj,UserSecurityGroupID   );
		 tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount AdjusterID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AssignedToID")) ;
    if ( !myEO.NIM3_CaseAccount.getAssignedToID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setAssignedToID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
         bltNIM3_CommTrack working_bltNIM3_CommTrack = new bltNIM3_CommTrack();
		 working_bltNIM3_CommTrack.setEncounterID(iEncounterID);
	 	 working_bltNIM3_CommTrack.setReferralID(myEO.NIM3_Encounter.getReferralID());
	  	 working_bltNIM3_CommTrack.setCaseID(myEO.NIM3_Referral.getCaseID());
		 working_bltNIM3_CommTrack.setMessageName(CurrentUserAccount.getContactFirstName()+" "+CurrentUserAccount.getContactLastName());
		 working_bltNIM3_CommTrack.setMessageCompany(myPM.getPayerName());
		 working_bltNIM3_CommTrack.setUniqueCreateDate(PLCUtils.getNowDate(false));
		 working_bltNIM3_CommTrack.setUniqueModifyDate(PLCUtils.getNowDate(false));
		 working_bltNIM3_CommTrack.setCommStart(PLCUtils.getNowDate(false));
		 working_bltNIM3_CommTrack.setCommEnd(PLCUtils.getNowDate(false));
		 working_bltNIM3_CommTrack.setUniqueModifyComments(""+CurrentUserAccount.getLogonUserName());
		 working_bltNIM3_CommTrack.setAlertStatusCode(new Integer(1));
		 bltUserAccount ReassignedAccount = new bltUserAccount(testObj);
		 working_bltNIM3_CommTrack.setMessageText("Assigned case to: " + ReassignedAccount.getContactFirstName()+" "+ReassignedAccount.getContactLastName());
		 working_bltNIM3_CommTrack.commitData();		
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount AdjusterID not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("OccMedPatientID")) ;
    if ( !myEO.NIM3_CaseAccount.getOccMedPatientID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setOccMedPatientID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount OccMedPatientID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("EmployerName")) ;
    if ( !myEO.NIM3_CaseAccount.getEmployerName().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setEmployerName( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount EmployerName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("EmployerPhone")) ;
    if ( !myEO.NIM3_CaseAccount.getEmployerPhone().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setEmployerPhone( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount EmployerPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("EmployerPhone")) ;
    if ( !myEO.NIM3_CaseAccount.getEmployerPhone().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setEmployerPhone( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount EmployerPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("EmployerFax")) ;
    if ( !myEO.NIM3_CaseAccount.getEmployerFax().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setEmployerFax( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount EmployerFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PayerAccountIDNumber")) ;
    if ( !myEO.NIM3_CaseAccount.getPayerAccountIDNumber().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPayerAccountIDNumber( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PayerAccountIDNumber not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PatientIsClaus")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientIsClaus().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientIsClaus( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientIsClaus not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PatientIsHWRatio")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientIsHWRatio().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientIsHWRatio( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientIsHWRatio not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientFirstName")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientFirstName().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientFirstName( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientLastName")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientLastName().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientLastName( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientAccountNumber")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientAccountNumber().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientAccountNumber( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientAccountNumber not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("PatientExpirationDate"))) ;
    if ( !myEO.NIM3_CaseAccount.getPatientExpirationDate().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientExpirationDate( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientExpirationDate not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PatientIsActive")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientIsActive().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientIsActive( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientIsActive not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("PatientDOB"))) ;
    if ( !myEO.NIM3_CaseAccount.getPatientDOB().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientDOB( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientDOB not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientSSN")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientSSN().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientSSN( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientSSN not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientGender")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientGender().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientGender( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientGender not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientAddress1")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientAddress1().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientAddress1( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientAddress2")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientAddress2().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientAddress2( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientCity")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientCity().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientCity( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PatientStateID")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientStateID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientStateID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientZIP")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientZIP().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientZIP( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
		 
		 searchDB2 conn = new searchDB2();
         java.sql.ResultSet results = null;
         String query = "select * from zip_code inner join tstateli on tstateli.shortstate = zip_code.state_prefix where \"zip_code\" = '"+testObj+"' ";
		 results = conn.executeStatement(query);
		 String city = "";
		 int stateID = 0;
		 
		 while(results!=null&&results.next()){
			 city = results.getString("city");
			 stateID = results.getInt("stateid");
		 }

		 myEO.NIM3_CaseAccount.setPatientCity( city,UserSecurityGroupID);
		 myEO.NIM3_CaseAccount.setPatientStateID( stateID,UserSecurityGroupID);
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientZIP not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientHomePhone")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHomePhone().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHomePhone( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHomePhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientWorkPhone")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientWorkPhone().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientWorkPhone( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientWorkPhone not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("PatientCellPhone")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientCellPhone().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientCellPhone( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientCellPhone not set. this is ok-not an error");
}




try
{
    Integer testObj = new Integer(request.getParameter("JobID")) ;
    if ( !myEO.NIM3_CaseAccount.getJobID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setJobID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount JobID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("JobNotes")) ;
    if ( !myEO.NIM3_CaseAccount.getJobNotes().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setJobNotes( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount JobNotes not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("EmployerLocationID")) ;
    if ( !myEO.NIM3_CaseAccount.getEmployerLocationID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setEmployerLocationID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount EmployerLocationID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientEmail")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientEmail().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientEmail( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount PatientEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("MobilePhoneCarrier")) ;
    if ( !myEO.NIM3_CaseAccount.getMobilePhoneCarrier().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setMobilePhoneCarrier( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_CaseAccount MobilePhoneCarrier not set. this is ok-not an error");
}



try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateOfInjury"))) ;
    if ( !myEO.NIM3_CaseAccount.getDateOfInjury().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setDateOfInjury( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount DateOfInjury not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("InjuryDescription")) ;
    if ( !myEO.NIM3_CaseAccount.getInjuryDescription().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setInjuryDescription( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount InjuryDescription not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("tCaseAccount_LODID")) ;
    if ( !myEO.NIM3_CaseAccount.getLODID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setLODID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount LODID not set. this is ok-not an error");
}



try
{
    Integer testObj = new Integer(request.getParameter("NurseCaseManagerID")) ;
    if ( !myEO.NIM3_CaseAccount.getNurseCaseManagerID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setNurseCaseManagerID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount NurseCaseManagerID not set. this is ok-not an error");
}



try
{
    Integer testObj = new Integer(request.getParameter("CaseAdministratorID")) ;
    if ( !myEO.NIM3_CaseAccount.getCaseAdministratorID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setCaseAdministratorID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount CaseAdministratorID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CaseAdministrator2ID")) ;
    if ( !myEO.NIM3_CaseAccount.getCaseAdministrator2ID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setCaseAdministrator2ID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount CaseAdministrator2ID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CaseAdministrator3ID")) ;
    if ( !myEO.NIM3_CaseAccount.getCaseAdministrator3ID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setCaseAdministrator3ID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount CaseAdministrator3ID not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("CaseAdministrator4ID")) ;
    if ( !myEO.NIM3_CaseAccount.getCaseAdministrator4ID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setCaseAdministrator4ID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount CaseAdministrator4ID not set. this is ok-not an error");
}



try
{
    String testObj = new String(request.getParameter("PatientHasImplantsDesc")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHasImplantsDesc().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHasImplantsDesc( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHasImplantsDesc not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientHasMetalInBodyDesc")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHasMetalInBodyDesc().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHasMetalInBodyDesc( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHasMetalInBodyDesc not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientHasAllergiesDesc")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHasAllergiesDesc().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHasAllergiesDesc( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHasAllergiesDesc not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientHasRecentSurgeryDesc")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHasRecentSurgeryDesc().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHasRecentSurgeryDesc( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHasRecentSurgeryDesc not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientHasPreviousMRIsDesc")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHasPreviousMRIsDesc().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHasPreviousMRIsDesc( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHasPreviousMRIsDesc not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientHasKidneyLiverHypertensionDiabeticConditionsDesc")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHasKidneyLiverHypertensionDiabeticConditionsDesc().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHasKidneyLiverHypertensionDiabeticConditionsDesc( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHasKidneyLiverHypertensionDiabeticConditionsDesc not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientIsPregnantDesc")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientIsPregnantDesc().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientIsPregnantDesc( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientIsPregnantDesc not set. this is ok-not an error");
}






try
{
    Integer testObj = new Integer(request.getParameter("BillToContactID")) ;
    if ( !myEO.NIM3_CaseAccount.getBillToContactID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setBillToContactID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount BillToContactID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("URContactID")) ;
    if ( !myEO.NIM3_CaseAccount.getURContactID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setURContactID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount URContactID not set. this is ok-not an error");
}





try
{
    Integer testObj = new Integer(request.getParameter("PatientAccountID")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientAccountID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientAccountID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientAccountID not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("AttorneyFirstName")) ;
    if ( !myEO.NIM3_CaseAccount.getAttorneyFirstName().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setAttorneyFirstName( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount AttorneyFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttorneyLastName")) ;
    if ( !myEO.NIM3_CaseAccount.getAttorneyLastName().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setAttorneyLastName( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount AttorneyLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttorneyAddress1")) ;
    if ( !myEO.NIM3_CaseAccount.getAttorneyAddress1().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setAttorneyAddress1( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount AttorneyAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttorneyAddress2")) ;
    if ( !myEO.NIM3_CaseAccount.getAttorneyAddress2().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setAttorneyAddress2( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount AttorneyAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttorneyCity")) ;
    if ( !myEO.NIM3_CaseAccount.getAttorneyCity().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setAttorneyCity( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount AttorneyCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AttorneyStateID")) ;
    if ( !myEO.NIM3_CaseAccount.getAttorneyStateID().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setAttorneyStateID( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount AttorneyStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttorneyZIP")) ;
    if ( !myEO.NIM3_CaseAccount.getAttorneyZIP().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setAttorneyZIP( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount AttorneyZIP not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttorneyFax")) ;
    if ( !myEO.NIM3_CaseAccount.getAttorneyFax().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setAttorneyFax( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount AttorneyFax not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttorneyWorkPhone")) ;
    if ( !myEO.NIM3_CaseAccount.getAttorneyWorkPhone().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setAttorneyWorkPhone( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount AttorneyWorkPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttorneyCellPhone")) ;
    if ( !myEO.NIM3_CaseAccount.getAttorneyCellPhone().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setAttorneyCellPhone( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount AttorneyCellPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("tCaseAccount_Comments")) ;
    if ( !myEO.NIM3_CaseAccount.getComments().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setComments( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount Comments not set. this is ok-not an error");
}




try
{
    Integer testObj = new Integer(request.getParameter("PatientHasImplants")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHasImplants().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHasImplants( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHasImplants not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PatientHasMetalInBody")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHasMetalInBody().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHasMetalInBody( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHasMetalInBody not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PatientHasAllergies")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHasAllergies().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHasAllergies( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHasAllergies not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PatientHasRecentSurgery")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHasRecentSurgery().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHasRecentSurgery( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHasRecentSurgery not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PatientHasPreviousMRIs")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHasPreviousMRIs().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHasPreviousMRIs( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHasPreviousMRIs not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("IsCurrentlyOnMeds")) ;
    if ( !myEO.NIM3_CaseAccount.getIsCurrentlyOnMeds().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setIsCurrentlyOnMeds( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
}

try
{
    String testObj = new String(request.getParameter("IsCurrentlyOnMedsDesc")) ;
    if ( !myEO.NIM3_CaseAccount.getIsCurrentlyOnMedsDesc().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setIsCurrentlyOnMedsDesc( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
}

try
{
    Integer testObj = new Integer(request.getParameter("PatientHasKidneyLiverHypertensionDiabeticConditions")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHasKidneyLiverHypertensionDiabeticConditions().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHasKidneyLiverHypertensionDiabeticConditions( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHasKidneyLiverHypertensionDiabeticConditions not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PatientIsPregnant")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientIsPregnant().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientIsPregnant( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientIsPregnant not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PatientHeight")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientHeight().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientHeight( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientHeight not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PatientWeight")) ;
    if ( !myEO.NIM3_CaseAccount.getPatientWeight().equals(testObj)  )
    {
         myEO.NIM3_CaseAccount.setPatientWeight( testObj,UserSecurityGroupID   );
         tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_CaseAccount PatientWeight not set. this is ok-not an error");
}


%>


<%//start tReferral%>



<%

String tReferral_testChangeID = "0";

String tReferral_tempAN =	myEO.NIM3_Referral.getAuditNotes();
String tReferral_tempAN_Status = "";

try
{
    Integer testObj = new Integer(request.getParameter("ReferralTypeID")) ;
    if ( !myEO.NIM3_Referral.getReferralTypeID().equals(testObj)  )
    {
         myEO.NIM3_Referral.setReferralTypeID( testObj,UserSecurityGroupID   );
         tReferral_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Referral ReferralTypeID not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("ReferralStatusID")) ;
    if ( !myEO.NIM3_Referral.getReferralStatusID().equals(testObj)  )
    {
		 if (testObj.intValue() == 0 ) { tReferral_tempAN_Status = ("NA");}
		 else if (testObj.intValue() == 1 ) { tReferral_tempAN_Status = ("Approved");}
		 else if (testObj.intValue() == 2 ) { tReferral_tempAN_Status = ("Denied");}
		 else if (testObj.intValue() == 3 ) { tReferral_tempAN_Status = ("Flag for Review");}
		 else if (testObj.intValue() == 4 ) { tReferral_tempAN_Status = ("Closed");}
		 else if (testObj.intValue() == 5 ) { tReferral_tempAN_Status = ("Void");}
		 else if (testObj.intValue() == 6 ) { tReferral_tempAN_Status = ("Pending");}
		myEO.NIM3_Referral.setReferralStatusID( testObj,UserSecurityGroupID   );
		if (testObj.intValue() == 1&&myEO.NIM3_Encounter.getTimeTrack_ReqApproved().before(NIMUtils.getBeforeTime()))
		{
			myEO.NIM3_Encounter.setTimeTrack_ReqApproved(PLCUtils.getNowDate(true));
			myEO.NIM3_Encounter.setTimeTrack_ReqApproved_UserID(CurrentUserAccount.getUserID());
		}
         tReferral_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Referral ReferralStatusID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AttendingPhysicianID")) ;
    if ( !myEO.NIM3_Referral.getAttendingPhysicianID().equals(testObj)  )
    {
         myEO.NIM3_Referral.setAttendingPhysicianID( testObj,UserSecurityGroupID   );
         tReferral_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Referral AttendingPhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReferringPhysicianID")) ;
    if ( !myEO.NIM3_Referral.getReferringPhysicianID().equals(testObj)  )
    {
         myEO.NIM3_Referral.setReferringPhysicianID( testObj,UserSecurityGroupID   );
         tReferral_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Referral ReferringPhysicianID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("PreAuthorizationConfirmation")) ;
    if ( !myEO.NIM3_Referral.getPreAuthorizationConfirmation().equals(testObj)  )
    {
         myEO.NIM3_Referral.setPreAuthorizationConfirmation( testObj,UserSecurityGroupID   );
         tReferral_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Referral PreAuthorizationConfirmation not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ProviderPatientID")) ;
    if ( !myEO.NIM3_Referral.getProviderPatientID().equals(testObj)  )
    {
         myEO.NIM3_Referral.setProviderPatientID( testObj,UserSecurityGroupID   );
         tReferral_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Referral ProviderPatientID not set. this is ok-not an error");
}


//2011-06-08>>>>>MOVED TO REFERRAL
try
{
    Integer testObj = new Integer(request.getParameter("ReferredByContactID")) ;
    if ( !myEO.NIM3_Referral.getReferredByContactID().equals(testObj)  )
    {
         myEO.NIM3_Referral.setReferredByContactID( testObj,UserSecurityGroupID   );
         tReferral_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Referral ReferredByContactID not set. this is ok-not an error");
}

//2013-9-13 added it to CASE ACCOUNT -> vmq4 reads from caseaccount
try
{
    Integer testObj = new Integer(request.getParameter("ReferredByContactID")) ;
    if ( !myEO.NIM3_CaseAccount.getReferredByContactID().equals(testObj)  )
    {
    	myEO.NIM3_CaseAccount.setReferredByContactID(testObj,UserSecurityGroupID);
    	tCaseAccount_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("this is ok-not an error");
}
try
{
    String testObj = new String(request.getParameter("ReferralMethod")) ;
    if ( !myEO.NIM3_Referral.getReferralMethod().equals(testObj)  )
    {
         myEO.NIM3_Referral.setReferralMethod( testObj,UserSecurityGroupID   );
         tReferral_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Referral ReferralMethod not set. this is ok-not an error");
}



try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("AuthorizationDate"))) ;
    if ( !myEO.NIM3_Referral.getAuthorizationDate().equals(testObj)  )
    {
         myEO.NIM3_Referral.setAuthorizationDate( testObj,UserSecurityGroupID   );
         tReferral_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Referral AuthorizationDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ReceiveDate"))) ;
    if ( !myEO.NIM3_Referral.getReceiveDate().equals(testObj)  )
    {
         myEO.NIM3_Referral.setReceiveDate( testObj,UserSecurityGroupID   );
         tReferral_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Referral ReceiveDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("ReferralDate"))) ;
    if ( !myEO.NIM3_Referral.getReferralDate().equals(testObj)  )
    {
         myEO.NIM3_Referral.setReferralDate( testObj,UserSecurityGroupID   );
         tReferral_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Referral ReferralDate not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("tReferral_Comments")) ;
    if ( !myEO.NIM3_Referral.getComments().equals(testObj)  )
    {
         myEO.NIM3_Referral.setComments( testObj,UserSecurityGroupID   );
         tReferral_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Referral Comments not set. this is ok-not an error");
}



%>


<%//end tReferral%>

<%//start tEncounterService%>



<%


      String tEncounter_tempAN =	myEO.NIM3_Encounter.getAuditNotes();
	  String tEncounter_tempAN_Status = "";
	String tEncounter_Enc_testChangeID = "0";


try
{
    String testObj = new String(request.getParameter("tEncounter_Comments")) ;
    if ( !myEO.NIM3_Encounter.getComments().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setComments( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Encounter Comments not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateOfService"))) ;
    if ( !myEO.NIM3_Encounter.getDateOfService().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setDateOfService( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Encounter DateOfService not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("EncounterTypeID")) ;
    if ( !myEO.NIM3_Encounter.getEncounterTypeID().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setEncounterTypeID( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Encounter EncounterTypeID not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("EncounterStatusID")) ;
    if ( !myEO.NIM3_Encounter.getEncounterStatusID().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setEncounterStatusID( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Encounter EncounterStatusID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("VoidLeakReasonID")) ;
    if ( !myEO.NIM3_Encounter.getVoidLeakReasonID().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setVoidLeakReasonID( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Encounter VoidLeakReasonID not set. this is ok-not an error");
}
try
{
    Integer testObj = new Integer(request.getParameter("isVIP")) ;
    if ( !myEO.NIM3_Encounter.getisSTAT().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setisVIP( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Encounter isSTAT not set. this is ok-not an error");
}





try
{
    Integer testObj = new Integer(request.getParameter("isSTAT")) ;
    if ( !myEO.NIM3_Encounter.getisSTAT().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setisSTAT( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Encounter isSTAT not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("isCourtesy")) ;
    if ( !myEO.NIM3_Encounter.getisCourtesy().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setisCourtesy( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Encounter isCourtesy not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("isRetro")) ;
    if ( !myEO.NIM3_Encounter.getisRetro().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setisRetro( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Encounter isRetro not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("RequiresFilms")) ;
    if ( !myEO.NIM3_Encounter.getRequiresFilms().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setRequiresFilms( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Encounter RequiresFilms not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("RequiresHandCarryCD")) ;
    if ( !myEO.NIM3_Encounter.getRequiresHandCarryCD().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setRequiresHandCarryCD( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter RequiresHandCarryCD not set. this is ok-not an error");
}



try
{
    Integer testObj = new Integer(request.getParameter("RequiresOnlineImage")) ;
    if ( !myEO.NIM3_Encounter.getRequiresOnlineImage().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setRequiresOnlineImage( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter RequiresOnlineImage not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("HasBeenRescheduled")) ;
    if ( !myEO.NIM3_Encounter.getHasBeenRescheduled().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setHasBeenRescheduled( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Encounter HasBeenRescheduled not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("RequiresAgeInjury")) ;
    if ( !myEO.NIM3_Encounter.getRequiresAgeInjury().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setRequiresAgeInjury( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("myEO.NIM3_Encounter RequiresAgeInjury not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("PatientAvailability")) ;
    if ( !myEO.NIM3_Encounter.getPatientAvailability().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setPatientAvailability( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter PatientAvailability not set. this is ok-not an error");
}


try
{
    Integer testObj = new Integer(request.getParameter("Pricing_Structure")) ;
    if ( !myEO.NIM3_Encounter.getPricing_Structure().equals(testObj)  )
    {
         myEO.NIM3_Encounter.setPricing_Structure( testObj,UserSecurityGroupID   );
         tEncounter_Enc_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Encounter Pricing_Structure not set. this is ok-not an error");
}



//start 


    bltNIM3_Service_List        bltNIM3_Service_List        =    new    bltNIM3_Service_List(iEncounterID, "","ServiceID");

//declaration of Enumeration
    bltNIM3_Service        NIM3_Service;
    ListElement         leCurrentElement;
    java.util.Enumeration eList = bltNIM3_Service_List.elements();
    %>
    <%
    int iExpress=0;
boolean errorRouteMeTotal = false;
boolean isAllComplete = true;
    while (eList.hasMoreElements()||iExpress<=ConfigurationMessages.getExpressItemCount("tNIM3_Service"))
    {
       iExpress++;

      boolean isNewRecord= false;
      if (eList.hasMoreElements())
      {
        leCurrentElement    = (ListElement) eList.nextElement();
        NIM3_Service  = (bltNIM3_Service) leCurrentElement.getObject();
      }
      else
      {
        NIM3_Service  = new bltNIM3_Service();

		
        NIM3_Service.setEncounterID(iEncounterID);
//		NIM3_Service.setServiceStatusID(new Integer(1));
        isNewRecord= true;
      }
        NIM3_Service.GroupSecurityInit(UserSecurityGroupID);
        String theClass = "tdHeader";
        %>

        <%  {

String tService_testChangeID = "0";
String subserviceToCommTracksMsg = iExpress+": ";
try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueCreateDate_"+iExpress))) ;
    if ( !NIM3_Service.getUniqueCreateDate().equals(testObj)  )
    {
         NIM3_Service.setUniqueCreateDate( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("UniqueModifyDate_"+iExpress))) ;
    if ( !NIM3_Service.getUniqueModifyDate().equals(testObj)  )
    {
         NIM3_Service.setUniqueModifyDate( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments_"+iExpress)) ;
    if ( !NIM3_Service.getUniqueModifyComments().equals(testObj)  )
    {
         NIM3_Service.setUniqueModifyComments( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("EncounterID_"+iExpress)) ;
    if ( !NIM3_Service.getEncounterID().equals(testObj)  )
    {
         NIM3_Service.setEncounterID( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service EncounterID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ServiceTypeID_"+iExpress)) ;
    if ( !NIM3_Service.getServiceTypeID().equals(testObj)  )
    {
         NIM3_Service.setServiceTypeID( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ServiceTypeID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ServiceStatusID_"+iExpress)) ;
    if ( !NIM3_Service.getServiceStatusID().equals(testObj)  )
    {
         NIM3_Service.setServiceStatusID( testObj,UserSecurityGroupID   );
         tService_testChangeID = "1";
         String status = "None";
         if (testObj==1){
        	 status = "Active";
         } else if(testObj==5){
        	 status = "Void";
         }
         subserviceToCommTracksMsg += "("+status+") ";
         isServiceChanged = true;
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ServiceStatusID not set. this is ok-not an error");
}



try
{
    java.util.Date testObj = null ;
    testObj = (PLCUtils.getSubDate(request.getParameter("DateOfService_"+iExpress))) ;
    if ( !NIM3_Service.getDateOfService().equals(testObj)  )
    {
         NIM3_Service.setDateOfService( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service DateOfService not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AttendingPhysicianID_"+iExpress)) ;
    if ( !NIM3_Service.getAttendingPhysicianID().equals(testObj)  )
    {
         NIM3_Service.setAttendingPhysicianID( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service AttendingPhysicianID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReferringPhysicianID_"+iExpress)) ;

    if ( !NIM3_Service.getReferringPhysicianID().equals(testObj)  )
    {
         NIM3_Service.setReferringPhysicianID( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReferringPhysicianID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CPT_"+iExpress)) ;
    if ( !NIM3_Service.getCPT().equals(testObj)  )
    {
         NIM3_Service.setCPT( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
         subserviceToCommTracksMsg += testObj+" ";
         isServiceChanged = true;
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service CPT not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("CPTModifier_"+iExpress)) ;
    if ( !NIM3_Service.getCPTModifier().equals(testObj)  )
    {
         NIM3_Service.setCPTModifier( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service CPTModifier not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("CPTQty_"+iExpress)) ;
    if ( !NIM3_Service.getCPTQty().equals(testObj)  )
    {
         NIM3_Service.setCPTQty(testObj,UserSecurityGroupID   );
		 if (isNewRecord)
		 {
			 NIM3_Service.setCPTQty_Bill(testObj,UserSecurityGroupID );
			 NIM3_Service.setCPTQty_Pay(testObj,UserSecurityGroupID );
		 }
         tService_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service CPTQty not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("CPTBodyPart_"+iExpress)) ;
    if ( !NIM3_Service.getCPTBodyPart().equals(testObj)  )
    {
         NIM3_Service.setCPTBodyPart( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
         subserviceToCommTracksMsg +=testObj+" ";
         isServiceChanged = true;
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service CPTBodyPart not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("dCPT1_"+iExpress)) ;
    if ( !NIM3_Service.getdCPT1().equals(testObj)  )
    {
         NIM3_Service.setdCPT1( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service dCPT1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("dCPT2_"+iExpress)) ;
    if ( !NIM3_Service.getdCPT2().equals(testObj)  )
    {
         NIM3_Service.setdCPT2( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service dCPT2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("dCPT3_"+iExpress)) ;
    if ( !NIM3_Service.getdCPT3().equals(testObj)  )
    {
         NIM3_Service.setdCPT3( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service dCPT3 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("dCPT4_"+iExpress)) ;
    if ( !NIM3_Service.getdCPT4().equals(testObj)  )
    {
         NIM3_Service.setdCPT4( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service dCPT4 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("CPTText_"+iExpress)) ;
    if ( !NIM3_Service.getCPTText().equals(testObj)  )
    {
         NIM3_Service.setCPTText( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service CPTText not set. this is ok-not an error");
}

//remove bililng save 
if (false)
{
	
try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("BillAmount_"+iExpress))) ;
    if ( !NIM3_Service.getBillAmount().equals(testObj)  )
    {
         NIM3_Service.setBillAmount( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service BillAmount not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("AllowAmount_"+iExpress))) ;
    if ( !NIM3_Service.getAllowAmount().equals(testObj)  )
    {
         NIM3_Service.setAllowAmount( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service AllowAmount not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("ReceivedAmount_"+iExpress))) ;
    if ( !NIM3_Service.getReceivedAmount().equals(testObj)  )
    {
         NIM3_Service.setReceivedAmount( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReceivedAmount not set. this is ok-not an error");
}

try
{
    Double testObj = null ;
    testObj = (PLCUtils.getDecimalDefault(request.getParameter("PaidOutAmount_"+iExpress))) ;
    if ( !NIM3_Service.getPaidOutAmount().equals(testObj)  )
    {
         NIM3_Service.setPaidOutAmount( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service PaidOutAmount not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReportFileID_"+iExpress)) ;
    if ( !NIM3_Service.getReportFileID().equals(testObj)  )
    {
         NIM3_Service.setReportFileID( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReportFileID not set. this is ok-not an error");
}


}
//remove billing save

try
{
    String testObj = new String(request.getParameter("Comments_"+iExpress)) ;
    if ( !NIM3_Service.getComments().equals(testObj)  )
    {
         NIM3_Service.setComments( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service Comments not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AuditNotes_"+iExpress)) ;
    if ( !NIM3_Service.getAuditNotes().equals(testObj)  )
    {
         NIM3_Service.setAuditNotes( testObj,UserSecurityGroupID   );
         tService_testChangeID  = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service AuditNotes not set. this is ok-not an error");
}


try
{
    String testObj = new String(request.getParameter("DICOM_AccessionNumber_"+iExpress)) ;
    if ( !NIM3_Service.getDICOM_AccessionNumber().equals(testObj)  )
    {
         NIM3_Service.setDICOM_AccessionNumber( testObj,UserSecurityGroupID   );
         tService_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service DICOM_AccessionNumber not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("DICOM_ShowImage_"+iExpress)) ;
    if ( !NIM3_Service.getDICOM_ShowImage().equals(testObj)  )
    {
         NIM3_Service.setDICOM_ShowImage( testObj,UserSecurityGroupID   );
         tService_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service DICOM_ShowImage not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DICOM_MRN_"+iExpress)) ;
    if ( !NIM3_Service.getDICOM_MRN().equals(testObj)  )
    {
         NIM3_Service.setDICOM_MRN( testObj,UserSecurityGroupID   );
         tService_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service DICOM_MRN not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DICOM_PN_"+iExpress)) ;
    if ( !NIM3_Service.getDICOM_PN().equals(testObj)  )
    {
         NIM3_Service.setDICOM_PN( testObj,UserSecurityGroupID   );
         tService_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service DICOM_PN not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("DICOM_STUDYID_"+iExpress)) ;
    if ( !NIM3_Service.getDICOM_STUDYID().equals(testObj)  )
    {
         NIM3_Service.setDICOM_STUDYID( testObj,UserSecurityGroupID   );
         tService_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service DICOM_STUDYID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReadingPhysicianID_"+iExpress)) ;
    if ( !NIM3_Service.getReadingPhysicianID().equals(testObj)  )
    {
         NIM3_Service.setReadingPhysicianID( testObj,UserSecurityGroupID   );
         tService_testChangeID = "1";
    }

}
catch(Exception e)
{
     //out.println("NIM3_Service ReadingPhysicianID not set. this is ok-not an error");
}





boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (false&&request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tNIM3_Service","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (tService_testChangeID.equalsIgnoreCase("1"))
{

	NIM3_Service.setUniqueModifyDate(PLCUtils.getNowDate(false));
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    NIM3_Service.setUniqueModifyComments("EX:"+UserLogonDescription);
	    try
	    {
			if (NIM3_Service.getServiceStatusID().intValue()==0)
			{
				NIM3_Service.setServiceStatusID(new Integer(1));
			}
			
			if (NIM3_Service.getServiceTypeID().intValue()==0)
			{
				NIM3_Service.setServiceTypeID(new Integer(3));
			}

	        NIM3_Service.commitData();
	        serviceToCommTracksMsg += subserviceToCommTracksMsg+"<br>";

	        if (!NIM3_Service.isComplete())
	        {
	            isAllComplete = false;
	        }
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	        errorRouteMeTotal = true;
	    }
    }
}       }
    }//while

    if (isServiceChanged)
    {
    	serviceToCommTracks.setMessageText(serviceToCommTracksMsg);
    	serviceToCommTracks.commitData();
    	java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
		java.util.Date d = sdf.parse("1800-01-01");
    	if(myEO.NIM3_Encounter.getAppointmentID()==0){
    		myEO.NIM3_Encounter.setTimeTrack_ReqRxReview(d);
        	myEO.NIM3_Encounter.setTimeTrack_ReqRxReview_UserID(0);	
    	}
		
		
    }
 
//end 
	//start calc Billing...
//	out.print(NIMUtils.ReCalcServiceBilling(myEO.NIM3_Encounter.getEncounterID()));
	//end calc billing




//end tEncounterService




boolean bINT = false;
boolean bNextStep = false;
String sRefreshDoc = "refreshMe=true&";
              if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("yes") ) 
	            {
	                bINT = true;
	                sRefreshDoc = "";
	            }
              else if (request.getParameter("INTNext")!=null&&request.getParameter("INTNext").equalsIgnoreCase("next") ) 
	            {
	                bINT = true;
	                sINTNext = ConfigurationMessages.getInterviewLinkRaw("tNIM3_CaseAccount","next");
	                sRefreshDoc = "";
	            }

// If an edit, update information; if new, append information
boolean errorRouteMe = false;
if (true)
{

	myEO.NIM3_CaseAccount.setUniqueModifyDate(PLCUtils.getNowDate(false));
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    myEO.NIM3_CaseAccount.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
			tCaseAccount_tempAN +="\n[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nRecord Edit";
			if (!tCaseAccount_tempAN_Status.equalsIgnoreCase(""))
			{
				tCaseAccount_tempAN += "\n-Status:" + tCaseAccount_tempAN_Status;
			}
			
			myEO.NIM3_CaseAccount.setAuditNotes(tCaseAccount_tempAN);
	        myEO.NIM3_CaseAccount.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }


	myEO.NIM3_Referral.setUniqueModifyDate(PLCUtils.getNowDate(false));
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    myEO.NIM3_Referral.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
			tReferral_tempAN +="\n[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nRecord Edit";
			if (!tReferral_tempAN_Status.equalsIgnoreCase(""))
			{
				tReferral_tempAN += "\n-Status:" + tReferral_tempAN_Status;
			}
			
			myEO.NIM3_Referral.setAuditNotes(tReferral_tempAN);
	        myEO.NIM3_Referral.commitData();
	    }
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }



//	myEO.NIM3_Encounter.setUniqueModifyDate(PLCUtils.getNowDate(false));
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    myEO.NIM3_Encounter.setUniqueModifyComments(UserLogonDescription);
	    try
	    {
			tEncounter_tempAN +="\n[" + PLCUtils.getNowDate(false) + " - " + CurrentUserAccount.getLogonUserName() + " (" + CurrentUserAccount.getUserID() + ")]\nRecord Edit";
			if (!tEncounter_tempAN_Status.equalsIgnoreCase(""))
			{
				tEncounter_tempAN += "\n-Status:" + tEncounter_tempAN_Status;
			}
			
			myEO.NIM3_Encounter.setAuditNotes(tEncounter_tempAN);
			
			//test for Case Rec Logic
			if (myEO.NIM3_Encounter.getTimeTrack_ReqRec().before(new java.util.Date(1)))
			{
				myEO.NIM3_Encounter.setTimeTrack_ReqRec(myEO.NIM3_Referral.getReceiveDate());
				myEO.NIM3_Encounter.setTimeTrack_ReqRec_UserID(CurrentUserAccount.getUserID());
			}
			//test for Case Create Logic
			if (myEO.NIM3_Encounter.getTimeTrack_ReqCreated().before(new java.util.Date(1))&&NIMUtils.isCaseCreateCriticalDone(myEO.NIM3_Encounter.getEncounterID()))
			{
				myEO.NIM3_Encounter.setTimeTrack_ReqCreated(PLCUtils.getNowDate(true));
				myEO.NIM3_Encounter.setTimeTrack_ReqCreated_UserID(CurrentUserAccount.getUserID());
			}


			//test for Dominant Diag Type INT to override
			int myEncDomTest = 0;
			//if (myEO.NIM3_Encounter.getEncounterTypeID().intValue()==NIMUtils.ENCOUNTER_TYPE_OTHER_SINGLE||myEO.NIM3_Encounter.getEncounterTypeID().intValue()==NIMUtils.ENCOUNTER_TYPE_DIAG_OTHER||myEO.NIM3_Encounter.getEncounterTypeID().intValue()==NIMUtils.ENCOUNTER_TYPE_NONE)
			{
				myEncDomTest = NIMUtils.getDiagnosticStudyType_NIMINT(NIMUtils.getCPTGroupObject(myEO.NIM3_Encounter.getUniqueID()));
			}
			if (myEncDomTest!=0&&myEncDomTest!=myEO.NIM3_Encounter.getEncounterTypeID().intValue())
			{
				myEO.NIM3_Encounter.setEncounterTypeID(new Integer(myEncDomTest));
				String MyEncDomTest_Result = "";
				if (myEncDomTest==NIMUtils.ENCOUNTER_TYPE_MR)
				{
					MyEncDomTest_Result = "MR";
				}
				else if (myEncDomTest==NIMUtils.ENCOUNTER_TYPE_CT)
				{
					MyEncDomTest_Result = "CT";
				}
				else if (myEncDomTest==NIMUtils.ENCOUNTER_TYPE_EMG)
				{
					MyEncDomTest_Result = "EMG";
				}
				%>
			<script language=Javascript>
			alert("Your Encounter appears to be primarily of type: <%=MyEncDomTest_Result%> \n\nCurrent Types:\n  EMG\n  MR\n  CT");
	      </script>                
                <%
			}

			//test for Case Processed Logic
	        myEO.NIM3_Encounter.commitData();
			myEO.ProcessEncounter(CurrentUserAccount);
			

		}
	    catch (Exception e55)
	    {
	        errorRouteMe = true;
	    }
    }


}
String routePageReference = request.getParameter("routePageReference");
String nextPage=null;
if (routePageReference!=null)
{
        if (pageControllerHash.containsKey(routePageReference))
        {
            nextPage = (String)pageControllerHash.get(routePageReference);
        }
        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
        }
        else if (pageControllerHash.containsKey("sParentReturnPage"))
        {
            nextPage = (String)pageControllerHash.get("sParentReturnPage");
        }
}
              if (bINT ) 
	            {
	                nextPage = sINTNext;
	                if (nextPage==null||nextPage.equalsIgnoreCase("")||nextPage.equalsIgnoreCase("#"))
	                {
	                    nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
	                }
	            }
	nextPage = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp";
//if (isReferralGo)
if (myEO.NIM3_CaseAccount.getCaseStatusID().intValue()!=4)
{
	nextPage = "tNIM3_Referral_main_NIM3_Referral_CaseID.jsp";
}
//	nextPage = "tImaging_schedule.jsp";

	String sQUICKVIEW = "no";
	if ( request.getParameter( "QUICKVIEW" ) != null && request.getParameter( "QUICKVIEW" ).equalsIgnoreCase("yes") )
    {
        sQUICKVIEW        =    "yes";
    }



if (errorRouteMe)
{
	out.println(ConfigurationMessages.getMessage("FatalError")); 
}
else if (sQUICKVIEW.equalsIgnoreCase("yes")&&request.getParameter("SubmitType")!=null&&request.getParameter("SubmitType").equalsIgnoreCase("1"))
{
	    %><script language=Javascript>
	      document.location="tNIM3_CaseAccount_express1_svc_form.jsp?QUICKVIEW=yes&EDIT=nim3exp_svc&EDITID=<%=iEncounterID%>&KM=p";
	      </script><%
}
else if (request.getParameter("SubmitType")!=null&&request.getParameter("SubmitType").equalsIgnoreCase("1"))
{
	    %><script language=Javascript>
	      document.location="tNIM3_CaseAccount_express1_svc_form.jsp?QUICKVIEW=no&EDIT=nim3exp_svc&EDITID=<%=iEncounterID%>&KM=p";
	      </script><%
}
else if (sQUICKVIEW.equalsIgnoreCase("yes"))
{
	    %><script language=Javascript>
//			alert("Your Case/Patient data has been updated.  \n\nPlease click OK to Refresh the page you are on." );
			window.close();
	      </script><hr /><input value="Close" name="closeme" onClick="window.close();" type="button" /><%
}
else if (nextPage!=null)
{
	    %><script language=Javascript>
	      document.location="<%=nextPage%>";
	      </script><%
    //response.sendRedirect(nextPage+"?EDIT=edit");
}
        %>


  <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}

}
catch(NullPointerException e1234)
{
	out.println("Error - Likely due to Collision or Session Timeout<br><br>" + e1234);
}
catch(Exception e123)
{
	out.println("Error - Please report the following error to IT<br><br>" + e123);
}
  %>
