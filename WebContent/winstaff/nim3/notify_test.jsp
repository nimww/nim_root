<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="../generic/CheckLogin.jsp" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NIM3 Start</title>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css">
</head>

<body>
<%@ include file="../generic/generalDisplay.jsp" %>

      <%
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isScheduler) 
    {
        accessValid = true;
    }
    else if (pageControllerHash.containsKey("iAdjusterID")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID        =    (Integer)pageControllerHash.get("iAdjusterID");
		isAdjuster = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID2")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster2 = true;
        accessValid = true;
	}
    else if (pageControllerHash.containsKey("iAdjusterID3")&&pageControllerHash.containsKey("iPayerID")) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
//        iAdjusterID2        =    (Integer)pageControllerHash.get("iAdjusterID2");
		isAdjuster3 = true;
        accessValid = true;
	}
	
	
  //page security
	Integer iDelay = null;
	if (request.getParameter("iDelay")!=null&&!request.getParameter("iDelay").equalsIgnoreCase(""))
	{
		iDelay = new Integer(request.getParameter("iDelay"));
	}
  //page security
  if (iDelay!=null)
  {
	  %>
      <meta http-equiv="refresh" content="20;URL=top_notify.jsp" />
		Pausing
      <%
  }
  else if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");

	
		if (isScheduler)
	  {
	  %>
	<meta http-equiv="refresh" content="600">
      <table width="100%" border="0" cellspacing="0" cellpadding="1">
        <tr class="tdBaseAlt">
          <td width="50%" >Next Actions</td>
          <td align="center" class="inputButton_sm_Default"><%=PLCUtils.getDisplayDateWithTime(PLCUtils.getNowDate(false))%></td>
        </tr>
        <%
{				
	searchDB2 mySS = new searchDB2();
	java.sql.ResultSet myRS = null;
	try
	{
		String mySQL = "null";
//		if (isScheduler)
		{	
			mySQL = "select encounterid from tNIM3_Encounter INNER JOIN tNIM3_Referral on tNIM3_Referral.referralid = tNIM3_Encounter.referralid INNER JOIN tNIM3_CaseAccount on tNIM3_CaseAccount.caseid = tNIM3_Referral.caseid where Date(tNIM3_Encounter.NextActionDate)<=Date(Now()) and tNIM3_Encounter.NextActionDate>'2000-01-01' AND tNIM3_CaseAccount.assignedtoid = " + CurrentUserAccount.getUserID() + " order by tNIM3_Encounter.NextActionDate";
//			mySQL = "select encounterid from tNIM3_Encounter ";
		}
	//out.println(mySQL);
		myRS = mySS.executeStatement(mySQL);
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
	try
	{
	
		int cnt=0;
		int cnt2=0;
		int cnt3=0;
		while (myRS!=null&&myRS.next())
		{
			cnt++;
			Integer iEncounterID = new Integer(myRS.getString("encounterid"));
			NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);
			if (myEO.NIM3_Encounter.getNextActionDate().before(PLCUtils.getToday()))
			{
				cnt2++;
			}
			if (myEO.NIM3_Encounter.getNextActionDate().before(PLCUtils.getYesterday()))
			{
				cnt3++;
			}
		}
		String theClass2 = "tdBase";
		if (cnt3>0)
		{
			theClass2 = "tdBaseAlt_Action3";
		}
		else if (cnt2>0)
		{
			theClass2 = "tdBaseAlt_Action2";
		}
		%>
                <tr ><td class="<%=theClass2%>">Now:<%=cnt2%></td>
                  <td class="inputButton_sm_Default" rowspan="2" align="center" valign="top"><table width="50%" border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="center">&nbsp;</td>
                      <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_NAD," AND tNIM3_CaseAccount.assignedtoid = " + CurrentUserAccount.getUserID() + " "); 
	String theClass_Temp = "tdBase";
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);

		if (myEO.NIM3_Encounter.getNextActionDate().before(PLCUtils.getYesterday()))
		{
			theClass_Temp = "tdBaseAlt_Action3";
		}
		else if (myEO.NIM3_Encounter.getNextActionDate().before(PLCUtils.getToday()))
		{
			if (!theClass_Temp.equalsIgnoreCase("tdBaseAlt_Action3"))
			{
				theClass_Temp = "tdBaseAlt_Action2";
			}
		}
			
	}
	%>
                      <td align="center" class="<%=theClass_Temp%>"><%=myWorklist_Vector.size()%></td>
                      <%
}
				%>
                      
                    </tr>
                    <tr>
                      <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_NOT_SCHEDULED," AND tNIM3_CaseAccount.assignedtoid = " + CurrentUserAccount.getUserID() + " "); 
	String theClass_Temp = "tdBase";
	int myClassLevel = 0;
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);
		long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myEO.NIM3_Referral.getReceiveDate().getTime())/(60*60*1000);
		if (iHoursPassed<2&&myClassLevel==0)
		{
			theClass_Temp = "tdBaseFade1";
		    myClassLevel = 0;
		}
		else if (iHoursPassed<4&&myClassLevel<=1)
		{
			theClass_Temp = "tdBase";
		    myClassLevel = 1;
		}
		else if (iHoursPassed<24&&myClassLevel<=2)
		{
			theClass_Temp = "tdBaseAlt_Action1";
		    myClassLevel = 2;
		}
		else if (iHoursPassed<48&&myClassLevel<=3)
		{
			theClass_Temp = "tdBaseAlt_Action2";
		    myClassLevel = 3;
		}
		else if (myClassLevel<=4)
		{
			theClass_Temp = "tdBaseAlt_Action3";
		    myClassLevel = 4;
		}
	}
	%>
                      <td align="center" class="<%=theClass_Temp%>"><%=myWorklist_Vector.size()%></td>
                      <%
}
				%>
                      <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_REPORTS_DUE," AND tNIM3_CaseAccount.assignedtoid = " + CurrentUserAccount.getUserID() + " "); 
	String theClass_Temp = "tdBase";
	int myClassLevel = 0;
	%>
                      <td align="center">                      <%
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);
		long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myEO.NIM3_Referral.getReceiveDate().getTime())/(60*60*1000);
		

		
		
		if (iHoursPassed<4&&myClassLevel==0)
		{
			theClass_Temp = "tdBaseFade1";
		    myClassLevel = 0;
		}
		else if (iHoursPassed<8&&myClassLevel<=1)
		{
			theClass_Temp = "tdBase";
		    myClassLevel = 1;
		}
		else if (iHoursPassed<12&&myClassLevel<=2)
		{
			theClass_Temp = "tdBaseAlt_Action1";
		    myClassLevel = 2;
		}
		else if (iHoursPassed<24&&myClassLevel<=3)
		{
			theClass_Temp = "tdBaseAlt_Action2";
		    myClassLevel = 3;
		}
		else if (myClassLevel<=4)
		{
			theClass_Temp = "tdBaseAlt_Action3";
		    myClassLevel = 4;
		}
		
	%>
                      <%=myEO.NIM3_Encounter.getScanPass()%><hr>
                      <%
		
	}
	%>
                      </td>
                      <%
}
				%>
                      
                    </tr>
                    <tr>
                      <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_RX_REVIEW," AND tNIM3_CaseAccount.assignedtoid <> " + CurrentUserAccount.getUserID() + " "); 
	String theClass_Temp = "tdBase";
	int cnt4=0;
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iEncounterID = (Integer) myWorklist_Vector.elementAt(i);
		NIM3_EncounterObject myEO = new NIM3_EncounterObject(iEncounterID);
		int myClassLevel = 0;
		if (NIMUtils.isEncounterReadyToSchedule(iEncounterID,false))
		{
			cnt4++;
		
			long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myEO.NIM3_Referral.getReceiveDate().getTime())/(60*60*1000);
			
	
			
			if (iHoursPassed<2&&myClassLevel==0)
			{
				theClass_Temp = "tdBaseFade1";
				myClassLevel = 0;
			}
			else if (iHoursPassed<4&&myClassLevel<=1)
			{
				theClass_Temp = "tdBase";
				myClassLevel = 1;
			}
			else if (iHoursPassed<24&&myClassLevel<=2)
			{
				theClass_Temp = "tdBaseAlt_Action1";
				myClassLevel = 2;
			}
			else if (iHoursPassed<48&&myClassLevel<=3)
			{
				theClass_Temp = "tdBaseAlt_Action2";
				myClassLevel = 3;
			}
			else if (myClassLevel<=4)
			{
				theClass_Temp = "tdBaseAlt_Action3";
				myClassLevel = 4;
			}
		}
	}
	%>
                      <td align="center" class="<%=theClass_Temp%>"><%=cnt4%></td>
                      <%
}
				%> <%
{				
	java.util.Vector myWorklist_Vector = NIMUtils.getWorklist(NIMUtils.WORKLIST_COMMTRACK,""); 
	String theClass_Temp = "tdBase";
	int myClassLevel = 0;
	for (int i=0;i<myWorklist_Vector.size();i++)
	{
		Integer iCommTrackID = (Integer) myWorklist_Vector.elementAt(i);
		bltNIM3_CommTrack myCT = new bltNIM3_CommTrack(iCommTrackID);
		long iHoursPassed = (PLCUtils.getNowDate(false).getTime()-myCT.getCommStart().getTime())/(60*60*1000);
		
		if (iHoursPassed<1&&myClassLevel==0)
		{
			theClass_Temp = "tdBaseFade1";
		    myClassLevel = 0;
		}
		else if (iHoursPassed<3&&myClassLevel<=1)
		{
			theClass_Temp = "tdBase";
		    myClassLevel = 1;
		}
		else if (iHoursPassed<6&&myClassLevel<=2)
		{
			theClass_Temp = "tdBaseAlt_Action1";
		    myClassLevel = 2;
		}
		else if (iHoursPassed<12&&myClassLevel<=3)
		{
			theClass_Temp = "tdBaseAlt_Action2";
		    myClassLevel = 3;
		}
		else if (myClassLevel<=4)
		{
			theClass_Temp = "tdBaseAlt_Action3";
		    myClassLevel = 4;
		}
	}
	%>
                      <td align="center" class="<%=theClass_Temp%>"><%=myWorklist_Vector.size()%></td>
                      <%
}
				%>
                    </tr>
                  </table></td>
                </tr>
                <tr><td>Today:<%=cnt%> </td>
                </tr>
        <%
	}
	catch(Exception e)
	{
		out.println("ResultsSet:"+e);
	}
	
}
				%>
      </table>
<%
	  }

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

<h1></h1></body>
</html>
