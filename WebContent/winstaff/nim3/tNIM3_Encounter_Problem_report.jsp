<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*
    filename: tNIM_Encounter_main_NIM_Encounter_PatientID.jsp
    Created on May/14/2008
    Type: 1-n main class file
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
<%@ include file="../generic/generalDisplay.jsp" %>



<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_Clear.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

    <table cellpadding=5 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td><td>
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM_Encounter")%>



<%
//initial declaration of list class and parentID
    Integer        iEncounterID        =    null;
    java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");
    java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat("MM/dd/yyyy");
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iEncounterID")) 
    {
        iEncounterID        =    (Integer)pageControllerHash.get("iEncounterID");
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
		String sFlag = request.getParameter("EDIT");
		java.util.Vector myQuickText = new java.util.Vector();
		String myMessage = "";
		String myTitle = "";
		if (sFlag.equalsIgnoreCase("problem_leak"))
		{
			myTitle = "Leak: Already Scheduled";
			myMessage = "Your note will be copied to a manager";
		}
		else if (sFlag.equalsIgnoreCase("problem_bypassrx"))
		{
			myTitle = "Bypass Rx & Rx Review";
			myMessage = "Encounter status will remain unchanged. A Temporary Rx will be automatically uploaded and Rx will be systematically approved.";
		}
		else if (sFlag.equalsIgnoreCase("problem_noshow"))
		{
			myTitle = "Patient No Show (Appointment missed)";
			myMessage = "Encounter status will remain unchanged.  Appointment will be archived and noted in CommTrack.";
		}
		else if (sFlag.equalsIgnoreCase("problem_resched"))
		{
			myTitle = "Reschedule (Appointment not missed)";
			myMessage = "Note: Encounter status will remain unchanged.  PLEASE NOTE: If you are rescheduling at different center, you must let the existing center know of the cancellation.";
		}
		else if (sFlag.equalsIgnoreCase("problem_escalate"))
		{
			myTitle = "Escalate Case";
			myMessage = "Your note information will be CC'd to a manager.";
		}
		else if (sFlag.equalsIgnoreCase("problem_courtesy"))
		{
			myTitle = "Courtesy Schedule";
			myMessage = "Your note information will be CC'd to a manager.";
		}
		else if (sFlag.equalsIgnoreCase("problem_void"))
		{
			myTitle = "Void";
			myMessage = "";
		}
		else if (sFlag.equalsIgnoreCase("problem_reactive"))
		{
			myTitle = "Change Case to Active";
			myMessage = "";
		}
		else if (sFlag.equalsIgnoreCase("problem_close"))
		{
			myTitle = "Change Case to Closed";
			myMessage = "";
		}
		else if (sFlag.equalsIgnoreCase("problem_holdbill"))
		{
			myTitle = "Change Case to Flag: Hold Bill";
			myMessage = "";
		}
		else if (sFlag.equalsIgnoreCase("problem_billinginprogress"))
		{
			myTitle = "Change Case to Billing: In Progress";
		}
		else if (sFlag.equalsIgnoreCase("problem_tier2"))
		{
			myTitle = "Change Case to Tier 2 Pricing.  Please remember to go into Billing and enter the appropriate pricing for this case right away.";
		}
		else if (sFlag.equalsIgnoreCase("problem_tier3"))
		{
			myTitle = "Change Case to Tier 3 Pricing.  Please remember to go into Billing and enter the appropriate pricing for this case right away.";
		}
		else if (sFlag.equalsIgnoreCase("problem_removecourtesy"))
		{
			myTitle = "NetDev Only: Remove Courtesy Status.";
		}



%>
    </td></tr>
    <tr>
      <td>&nbsp;</td>
      <td><h1> Problem</h1></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="tdHeader">Problem Type: <%=myTitle%></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>Please describe the problem in as much detail as you can below:</td>
    </tr>



    <tr>
      <td>&nbsp;</td>
      <td><form id="form1" name="form1" method="post" action="tNIM3_Encounter_Problem_report_sub.jsp">
<%
		if (sFlag.equalsIgnoreCase("problem_void"))
		{
		%>
            Void Reason Code:  <select   name="iExtraCode" ><jsp:include page="../generic/tVoidLeakReasonLILong.jsp" flush="true" ><jsp:param name="CurrentSelection" value="" /></jsp:include></select><br />

            
		<%
		}
%>
<%
		if (sFlag.equalsIgnoreCase("problem_escalate"))
		{
		%>
            Problem with:  
            <select name="iExtraCode" >
            	<option value="1">Escalated</option>
            	<option value="2">AOI</option>
            	<option value="3">Attention</option>
            	<option value="4">Second Opinion</option>
            	<option value="5">Comparison</option>
            	<option value="6">Reread</option>
            </select><br />            
		<%
		}
%>
        <label>
          <textarea name="problemReport" id="problemReport" cols="70" rows="10"></textarea>
        </label>
        <br />
        <input name="" type="submit" class="inputButton_md_Create" value="Submit" />
        <input name="EDIT" type="hidden" value="<%=sFlag%>" />
      </form></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="right"><input type="button" class="inputButton_md_Stop" onclick="this.disabled=true;window.close();" value="Cancel" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr> 
    </table>

<%

  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

