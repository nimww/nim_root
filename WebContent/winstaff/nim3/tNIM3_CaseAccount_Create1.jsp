<!DOCTYPE HTML>
<%@page contentType="text/html" language="java" import="com.winstaff.*   " %>
<%/*
    filename: tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID.jsp
    Created on May/14/2009
    Type: 1-n main class file
    Created by: Scott Ellis
    Version: 4.0
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<link href="ui_200/style_sched.css" rel="stylesheet" type="text/css" />

<%
String tnIncludeFN = "ui_"+thePLCID+"\\top-nav_sched.jsp?plcID="+thePLCID;
%>
<jsp:include page="<%=tnIncludeFN%>" flush="true" ></jsp:include>
<%@ include file="../generic/generalDisplay.jsp" %>
<table cellpadding=5 cellspacing=0 border=0 width=<%=MasterTableWidth%> >
    <tr><td width=10>&nbsp;</td><td colspan="2">
    <%//ConfigurationMessages.getHTML("INTERVIEWTopControl","tNIM3_CaseAccount")%>



<%
//initial declaration of list class and parentID
    Integer        iPayerID        =    null;
    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   if (iSecurityCheck.intValue()!=0)
   {
    if (pageControllerHash.containsKey("iPayerID")&&request.getParameter("CN")!=null) 
    {
        iPayerID        =    (Integer)pageControllerHash.get("iPayerID");
        accessValid = true;
    }
    else if (isScheduler&&request.getParameter("CN")!=null) 
    {
        accessValid = true;
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      pageControllerHash.put("sParentReturnPage","tNIM3_CaseAccount_Encounter_PayerID_query.jsp");
//	  pageControllerHash.remove("iCaseID");
	  pageControllerHash.put("sINTNext","tNIM3_CaseAccount_Encounter_PayerID_query.jsp");
	  session.setAttribute("pageControllerHash",pageControllerHash);

	String myCaseClaimNumber = request.getParameter("CN");
%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script>
	$(document).ready(function(e) {
		$("#faded-bg").click(function(e) {
			$("#ameri-query, #faded-bg").fadeOut('fast');
			$("#ameri-query").html("");
		});
	});

	function search_main(){
		var query ="<%=myCaseClaimNumber%>";
		var windowwidth = ($(window).width()-640)/2;
		var windowheight =($(window).height()-480)/2;
		$("#ameri-query").html("<p>Search Results:</p><iframe src=\"broadspire_query.jsp?query="+query+"\" width=\"630\" height=\"480px\" id=\"ameriDbResults\"></iframe>");
		
		if ($(window).width() < 640){$("#ameri-query").css({left:0});}
		else $("#ameri-query").css({'left':windowwidth});
		
		if ($(window).height() < 480){$("#ameri-query").css({top:0});}
		else $("#ameri-query").css({'top':windowheight});
		
		$("#faded-bg").css({'width':$(window).width(),'height':$(window).height()});
		$("#ameri-query, #faded-bg").fadeIn('fast');
	}
	
	function selectClaimant (caseClaimNumber, patientLastName, patientFirstName, patientAddress1, patientAddress2, patientCity, patientState, patientZip, patientPhone, patientDOB, adjusterFirstName, adjusterLastName, casemanagerFirstName, casemanagerLastName, employer, patientDOI, patientGender, PatientSSN, stateid){
		$("input#clientClaimNum").val(caseClaimNumber);
		
		$("input#patientLastName").val(patientLastName);
		
		$("input#patientFirstName").val(patientFirstName);
		
		$("input#patientAddress1").val(patientAddress1);
		
		$("input#patientAddress2").val(patientAddress2);
		
		$("input#patientCity").val(patientCity);
		
		$("input#patientState").val(patientState);
		
		$("input#patientZip").val(patientZip);
		
		$("input#patientPhone").val(patientPhone);
		
		$("input#patientDOB").val(patientDOB);
		
		$("input#adjusterFirstName").val(adjusterFirstName);
		
		$("input#adjusterLastName").val(adjusterLastName);
		
		$("input#casemanagerFirstName").val(casemanagerFirstName);
		
		$("input#casemanagerLastName").val(casemanagerLastName);
		
		$("input#employer").val(employer);
		
		$("input#patientDOI").val(patientDOI);
		
		$("input#PatientStateID").val(stateid);
		$("input#PatientSSN").val(PatientSSN);
		$("input#patientGender").val(patientGender);
		
		$("#ameri-query, #faded-bg").fadeOut('fast');
		$("#ameri-query").html("");
		$("#broadspire_form").submit();
	}
</script>



<span class="title">Existing Cases:</span><br />
<span class="instructions">If one of the cases below is the case you are trying to create, then please click to review this case first.  You may be intending to create a new referral or encounter instead of a new case</span>.  
<table  border=1 cellpadding=2 cellspacing=0 bordercolor=#003333>
  <tr class="tdHeaderAlt">
  <td >Action</td>
  <td >Claim #</td>
  <td >Payer</td>
  <td >Patient</td>
  </tr>
<%
String myWhere = "where (tNIM3_CaseAccount.PayerID >0 ";
boolean theFirst = false;

try
{
    if (!myCaseClaimNumber.equalsIgnoreCase(""))
	{
		if (!theFirst) { myWhere+=" and ";}
		myWhere += "(levenshtein(lower(CaseClaimNumber),lower('" + myCaseClaimNumber +"'))<2 OR lower(CaseClaimNumber) like lower('%" + myCaseClaimNumber +"%')   )";
		theFirst = false;
	}

	myWhere += ")";
	
	theFirst=false;

}
catch(Exception e)
{
out.println("FFF:"+e);
}

searchDB2 mySS = new searchDB2();

java.sql.ResultSet myRS = null;;

try
{
	String mySQL = "null";
	mySQL = "select caseid from tNIM3_CaseAccount " + myWhere + " order by uniquecreatedate desc";
	//out.println(mySQL);
	myRS = mySS.executeStatement(mySQL);
}
catch(Exception e)
{
out.println("ResultsSet:"+e);
}

try
{

	int endCount = 0;
	
	int cnt=0;
	int cnt2=0;
   while (myRS!=null&&myRS.next())
   {
	bltNIM3_CaseAccount working_bltNIM3_CaseAccount  =  new bltNIM3_CaseAccount(new Integer(myRS.getString("CaseID")));
	bltNIM3_PayerMaster myPM = new bltNIM3_PayerMaster(working_bltNIM3_CaseAccount.getPayerID());
	cnt++;
	cnt2++;
	String myClass = "tdBase";
	if (cnt2%2==0)
	{
		myClass = "tdBaseAlt";
	}
	out.print("<tr class="+myClass+">");
	out.print("<td>");
	out.print("<input class=\"inputButton_md_Action1\"  type=button onClick = \"this.disabled=true;document.location ='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_authorize.jsp?EDIT=openflow&EDITID=" + working_bltNIM3_CaseAccount.getCaseID() + "&KM=p'\" value=\"Open\">");
	out.print("&nbsp;</td>");
	out.print("<td>");
	out.print("" + working_bltNIM3_CaseAccount.getCaseClaimNumber());
	out.print("&nbsp;</td>");
	out.print("<td>");
	out.print("" + myPM.getPayerName());
	out.print("&nbsp;</td>");
	out.print("<td>");
	out.print("" + working_bltNIM3_CaseAccount.getPatientFirstName() + " " + working_bltNIM3_CaseAccount.getPatientLastName());
	out.print("&nbsp;</td>");
	out.print("</tr>");
   }
mySS.closeAll();
endCount = cnt;
%>

</table> 


<%

}
catch(Exception e)
{
out.println("Display:"+e);
}

%>


    </td>
  </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="2" class="title">OR Continue to create a case</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td valign="top">
          <table border="1" cellspacing="0" cellpadding="10">
            <tr>
              <td align="center" valign="top">
                <form id="adjform" name="adjform" method="post" action="tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_create.jsp"> <input class="inputButton_lg_Create" name="AdjusterID2" type="button"   onClick="this.disabled=false;modalPost('AID', modalWin('LI_tUserAccount_Search.jsp?ssOpenField=AID','Search...','dialogWidth:1200px;dialogHeight:800px','status=yes,scrollbars=yes,resizable=yes,width=1200,height=800'));document.AdjusterID_IF.location='../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=' + document.getElementById('AID').value;if (document.getElementById('AID').value>0){ document.getElementById('adjform').submit();}" value="Select Adjuster..."   >
    <br />
    
                <div class="boxDr"><iframe name="AdjusterID_IF" frameborder="0" marginheight="0" marginwidth="0" height="100%" width="100%" scrolling="no" src="../generic/LI_tUserAccount_Search_translate.jsp?CurrentSelection=0"></iframe></div><input name="AID" id="AID" type="hidden" value="0"> <input type=hidden name="CN" value="<%=myCaseClaimNumber%>" /> <input type=hidden name="EDIT" value="newexpress" /></form>
              </td>
            </tr>
          </table>
      </td>
      <td valign="top"><p class="title">NextImage Direct</p>
      <p>If this is a NextImage Direct Case, please select the company that the patient is affiliated with. </p>
      <p>
        <input name="NID1" type="button" class="inputButton_md_Create" onclick="this.disabled=true;document.location='tNIM3_CaseAccount_main_NIM3_CaseAccount_PayerID_form_create.jsp?CN=<%=myCaseClaimNumber%>';" id="No Affiliation2" value="Patriot Health / CCG" />
      </p>
      <p>
        <input name="NID2" type="button" class="inputButton_md_Create" id="No Affiliation" value="Other Affiliation or No Affiliation" />
      </p></td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
        <td valign="top">
        	<p class="title">Broadspire Crawford Only</p>
            
            <%
			try{
				String query = "select * from tnim3_eligibilityreference where lower(caseclaimnumber) like lower('%"+myCaseClaimNumber+"%')";	
				int count = 0;
				myRS = mySS.executeStatement(query);
				
				while(myRS.next()){
					count++;
				}
				out.print("<br><strong>"+count+"</strong> matching claim numbers.<br>");
			}
			catch (Exception e){}
			
			%>
            <input type="button" value="Eligibility Match" class="inputButton_md_Create" onClick="search_main()"><br>
            <div id="ameri-query"></div>
            <div id="faded-bg"></div>
            <div style="display:none;">
            	<form action="broadspire_submit.jsp" id="broadspire_form">
                    <input type="text" id="clientClaimNum" name="clientClaimNum" readonly class="claimantInfo">
                    <input type="text" id="patientFirstName" name="patientFirstName" readonly class="claimantInfo">
                    <input type="text" id="patientLastName" name="patientLastName" readonly class="claimantInfo">
                    <input type="text" id="patientAddress1" name="patientAddress1" readonly class="claimantInfo">
                    <input type="text" id="patientAddress2" name="patientAddress2" readonly class="claimantInfo">
                    <input type="text" id="patientCity" name="patientCity" readonly class="claimantInfo">
                    <input type="text" id="patientState" name="patientState" readonly class="claimantInfo"> 
                    <input type="text" id="patientZip" name="patientZip" readonly class="claimantInfo">
                    <input type="text" id="patientPhone" name="patientPhone" readonly class="claimantInfo">
                    <input type="text" id="patientDOB" name="patientDOB" readonly class="claimantInfo">
                    <input type="text" id="adjusterFirstName" name="adjusterFirstName" readonly class="claimantInfo"> 
                    <input type="text" id="adjusterLastName" name="adjusterLastName" readonly class="claimantInfo">
                    <input type="text" id="casemanagerFirstName" name="casemanagerFirstName" readonly class="claimantInfo"> 
                    <input type="text" id="casemanagerLastName" name="casemanagerLastName" readonly class="claimantInfo">
                    <input type="text" id="employer" name="employer" readonly class="claimantInfo">
                    <input type="text" id="patientDOI" name="patientDOI" readonly class="claimantInfo">
                    <input type="text" id="patientGender"name="patientGender" readonly class="claimantInfo">
                    <input type="submit">
                </form>
            </div>
        </td>
    </tr>
</table>

    <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";
%>
<jsp:include page="<%=bnIncludeFN%>" flush="true" ><jsp:param name="plcID" value="<%=thePLCID%>"/></jsp:include>