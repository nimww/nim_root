<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="com.winstaff.*" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">

<title>NID Contest</title>

<!-- Core CSS - Include with every page -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="font-awesome/css/font-awesome.css" rel="stylesheet">

<!-- Page-Level Plugin CSS - Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

<!-- SB Admin CSS - Include with every page -->
<link href="css/sb-admin.css" rel="stylesheet">
<%@ include file="../generic/CheckLogin.jsp"%>
</head>

<% if(request.getParameter("id") != null){
	searchDB2 conn = new searchDB2();
	
	String query = "insert into nidvote values ( "+request.getParameter("id")+","+CurrentUserAccount.getUserID()+")";

	java.sql.ResultSet rs = conn.executeStatement(query);
	
	//out.print(query);
	out.print("<script>window.location.href = \"index.jsp\";</script>");
}
%>


<body>

	<div id="wrapper">


		<div id="page-wrapper" style="margin: 20px";>
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Vote For The Best Name!</h1>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<%
			searchDB2 conn = new searchDB2();
			String query = "select * from nidvote where schid = "
						+ CurrentUserAccount.getUserID();
				 boolean isVoted = false;
				java.sql.ResultSet rs = conn.executeStatement(query);
				while (rs.next()) {
					isVoted = true;
				}

				if (isVoted) {
			%>
			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">Complete</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">Thanks for voting!</div>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-6 -->
				<!-- /.col-lg-6 -->
			</div>

			<%
				} else {
			%>
			<div class="row">
				<div class="col-lg-6">
					<div class="panel panel-default">
						<div class="panel-heading">Name List</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">
								<form method="post">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>Vote</th>
												<th>NID Name</th>
											</tr>
										</thead>
										<tbody>
											<%
												conn = new searchDB2();
													query = "select * from nidnamecontest order by nidname";
													rs = conn.executeStatement(query);

													while (rs.next()) {
														out.print("<tr>");
														out.print("<td><input name='id' type='radio' value='"
																+ rs.getString("nidid") + "'></td>");
														out.print("<td>" + rs.getString("nidname") + "</td>");
														out.print("</tr>");
													}
											%>
										</tbody>
									</table>
									<input type="submit" value="Vote!" class="btn">
								</form>
							</div>
							<!-- /.table-responsive -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-6 -->
				<!-- /.col-lg-6 -->
			</div>
			<%
				}
			%>

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- Core Scripts - Include with every page -->
	<script src="js/jquery-1.10.2.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

	<!-- Page-Level Plugin Scripts - Tables -->
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

	<!-- SB Admin Scripts - Include with every page -->
	<script src="js/sb-admin.js"></script>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script>
		$(document).ready(function() {
			$('#dataTables-example').dataTable();
		});
	</script>

</body>

</html>
