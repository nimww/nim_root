<%

String CurrentSelection = "";
try
{
	CurrentSelection = request.getParameter ("CurrentSelection");
}
catch(Exception e)
{
}
if (CurrentSelection==null)
{
	CurrentSelection ="";
}
%>
<option <%if (CurrentSelection.equalsIgnoreCase("1") ) {out.println("selected");}%> value=1>Licensure</option>
<option <%if (CurrentSelection.equalsIgnoreCase("2") ) {out.println("selected");}%> value=2>Malpractice</option>
<option <%if (CurrentSelection.equalsIgnoreCase("3") ) {out.println("selected");}%> value=3>Professional Conduct</option>
<option <%if (CurrentSelection.equalsIgnoreCase("4") ) {out.println("selected");}%> value=4>Healthcare Organizations</option>
<option <%if (CurrentSelection.equalsIgnoreCase("5") ) {out.println("selected");}%> value=5>Health Status</option>
<option <%if (CurrentSelection.equalsIgnoreCase("6") ) {out.println("selected");}%> value=6>Other Disclosures</option>
<option <%if (CurrentSelection.equalsIgnoreCase("7") ) {out.println("selected");}%> value=7>Miscellaneous: Not Disclosure-Related</option>
<option <%if (CurrentSelection.equalsIgnoreCase("8") ) {out.println("selected");}%> value=8>Unassigned</option>
<option <%if (CurrentSelection.equalsIgnoreCase("9") ) {out.println("selected");}%> value=9>Attestation Statement</option>
<option <%if (CurrentSelection.equalsIgnoreCase("10") ) {out.println("selected");}%> value=10>Massachusetts Initial Credentialing Disclosures</option>
<option <%if (CurrentSelection.equalsIgnoreCase("11") ) {out.println("selected");}%> value=11>Massachusetts Recredentialing Disclosures</option>
