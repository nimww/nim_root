<%

String CurrentSelection = "";
try
{
	CurrentSelection = request.getParameter ("CurrentSelection");
}
catch(Exception e)
{
}
if (CurrentSelection==null)
{
	CurrentSelection ="";
}
%>
<option value=0> </option>
<option <%if (CurrentSelection.equalsIgnoreCase("0") ) {out.println("selected");}%> value=0>Please Select...</option>
<option value=0> </option>
<option <%if (CurrentSelection.equalsIgnoreCase("1") ) {out.println("selected");}%> value=1>---Chiropractor</option>
<option value=0> </option>
<option value=2>---Counselor</option>
<option <%if (CurrentSelection.equalsIgnoreCase("2") ) {out.println("selected");}%> value=2>Counselor, Mental Health</option>
<option <%if (CurrentSelection.equalsIgnoreCase("3") ) {out.println("selected");}%> value=3>Professional Counselor</option>
<option <%if (CurrentSelection.equalsIgnoreCase("4") ) {out.println("selected");}%> value=4>Professional Counselor, Alcohol</option>
<option <%if (CurrentSelection.equalsIgnoreCase("5") ) {out.println("selected");}%> value=5>Professional Counselor, Family/Marriage</option>
<option <%if (CurrentSelection.equalsIgnoreCase("6") ) {out.println("selected");}%> value=6>Professional Counselor, Substance Abuse</option>
<option <%if (CurrentSelection.equalsIgnoreCase("7") ) {out.println("selected");}%> value=7>Marriage and Family Therapist</option>
<option value=0> </option>
<option value=8>---Dental Service Provider</option>
<option <%if (CurrentSelection.equalsIgnoreCase("8") ) {out.println("selected");}%> value=8>Dentist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("9") ) {out.println("selected");}%> value=9>Dental Resident</option>
<option <%if (CurrentSelection.equalsIgnoreCase("10") ) {out.println("selected");}%> value=10>Dental Assistant</option>
<option <%if (CurrentSelection.equalsIgnoreCase("11") ) {out.println("selected");}%> value=11>Dental Hygienist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("12") ) {out.println("selected");}%> value=12>Denturist</option>
<option value=0> </option>
<option value=13>---Dietician/Nutritionist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("13") ) {out.println("selected");}%> value=13>Dietician</option>
<option <%if (CurrentSelection.equalsIgnoreCase("14") ) {out.println("selected");}%> value=14>Nutritionist</option>
<option value=0> </option>
<option value=15>---Emergency Medical Technician (EMT)</option>
<option <%if (CurrentSelection.equalsIgnoreCase("15") ) {out.println("selected");}%> value=15>EMT, Basic</option>
<option <%if (CurrentSelection.equalsIgnoreCase("16") ) {out.println("selected");}%> value=16>EMT, Cardiac/Critical Care</option>
<option <%if (CurrentSelection.equalsIgnoreCase("17") ) {out.println("selected");}%> value=17>EMT, Intermediate</option>
<option <%if (CurrentSelection.equalsIgnoreCase("18") ) {out.println("selected");}%> value=18>EMT, Paramedic</option>
<option value=0> </option>
<option value=19>---Eye and Vision Service Provider</option>
<option <%if (CurrentSelection.equalsIgnoreCase("19") ) {out.println("selected");}%> value=19>Ocularist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("20") ) {out.println("selected");}%> value=20>Optician</option>
<option <%if (CurrentSelection.equalsIgnoreCase("21") ) {out.println("selected");}%> value=21>Optometrist</option>
<option value=0> </option>
<option value=22>---Nurse/Advanced Practice Registered Nurse</option>
<option <%if (CurrentSelection.equalsIgnoreCase("22") ) {out.println("selected");}%> value=22>Registered (Professional) Nurse</option>
<option <%if (CurrentSelection.equalsIgnoreCase("23") ) {out.println("selected");}%> value=23>Nurse Anesthetist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("24") ) {out.println("selected");}%> value=24>Nurse Midwife</option>
<option <%if (CurrentSelection.equalsIgnoreCase("25") ) {out.println("selected");}%> value=25>Nurse Practitioner</option>
<option <%if (CurrentSelection.equalsIgnoreCase("26") ) {out.println("selected");}%> value=26>Licensed Practical or Vocational Nurse</option>
<option <%if (CurrentSelection.equalsIgnoreCase("27") ) {out.println("selected");}%> value=27>Clinical Nurse Specialist</option>
<option value=0> </option>
<option value=28>---Nurses Aide/Home Health Aide</option>
<option <%if (CurrentSelection.equalsIgnoreCase("28") ) {out.println("selected");}%> value=28>Nurses Aide</option>
<option <%if (CurrentSelection.equalsIgnoreCase("29") ) {out.println("selected");}%> value=29>Home Health Aide (Homemaker)</option>
<option value=0> </option>
<option value=30>---Pharmacy Service Provider</option>
<option <%if (CurrentSelection.equalsIgnoreCase("30") ) {out.println("selected");}%> value=30>Pharmacist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("31") ) {out.println("selected");}%> value=31>Pharmacy Intern</option>
<option <%if (CurrentSelection.equalsIgnoreCase("32") ) {out.println("selected");}%> value=32>Pharmacist, Nuclear</option>
<option <%if (CurrentSelection.equalsIgnoreCase("33") ) {out.println("selected");}%> value=33>Pharmacy Assistant</option>
<option <%if (CurrentSelection.equalsIgnoreCase("34") ) {out.println("selected");}%> value=34>Pharmacy Technician</option>
<option value=0> </option>
<option value=35>---Physician</option>
<option <%if (CurrentSelection.equalsIgnoreCase("35") ) {out.println("selected");}%> value=35>Physician (MD)</option>
<option <%if (CurrentSelection.equalsIgnoreCase("36") ) {out.println("selected");}%> value=36>Physician Intern/Resident (MD)</option>
<option <%if (CurrentSelection.equalsIgnoreCase("37") ) {out.println("selected");}%> value=37>Osteopathic Physician (DO)</option>
<option <%if (CurrentSelection.equalsIgnoreCase("38") ) {out.println("selected");}%> value=38>Osteopathic Physician Intern/Resident (DO)</option>
<option value=0> </option>
<option value=39>---Physician Assistant</option>
<option <%if (CurrentSelection.equalsIgnoreCase("39") ) {out.println("selected");}%> value=39>Physician Assistant, Allopathic</option>
<option <%if (CurrentSelection.equalsIgnoreCase("40") ) {out.println("selected");}%> value=40>Physician Assistant, Osteopathic</option>
<option value=0> </option>
<option value=41>---Podiatric Service Provider</option>
<option <%if (CurrentSelection.equalsIgnoreCase("41") ) {out.println("selected");}%> value=41>Podiatrist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("42") ) {out.println("selected");}%> value=42>Podiatric Assistant</option>
<option value=0> </option>
<option value=43>---Psychologist/Psychological Assistant</option>
<option <%if (CurrentSelection.equalsIgnoreCase("43") ) {out.println("selected");}%> value=43>Psychologist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("44") ) {out.println("selected");}%> value=44>School Psychologist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("45") ) {out.println("selected");}%> value=45>Psychological Assistant, Associate,Examiner</option>
<option value=0> </option>
<option value=46>---Rehabilitative, Respiratory and Restorative Service Provider</option>
<option <%if (CurrentSelection.equalsIgnoreCase("46") ) {out.println("selected");}%> value=46>Art/Recreation Therapist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("47") ) {out.println("selected");}%> value=47>Massage Therapist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("48") ) {out.println("selected");}%> value=48>Occupational Therapist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("49") ) {out.println("selected");}%> value=49>Occupational Therapy Assistant</option>
<option <%if (CurrentSelection.equalsIgnoreCase("50") ) {out.println("selected");}%> value=50>Physical Therapist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("51") ) {out.println("selected");}%> value=51>Physical Therapy Assistant</option>
<option <%if (CurrentSelection.equalsIgnoreCase("52") ) {out.println("selected");}%> value=52>Rehabilitation Therapist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("53") ) {out.println("selected");}%> value=53>Respiratory Therapist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("54") ) {out.println("selected");}%> value=54>Respiratory Therapy Technician</option>
<option value=0> </option>
<option <%if (CurrentSelection.equalsIgnoreCase("55") ) {out.println("selected");}%> value=55>---Social Worker</option>
<option value=0> </option>
<option value=56>---Speech, Language and Hearing Service Provider</option>
<option <%if (CurrentSelection.equalsIgnoreCase("56") ) {out.println("selected");}%> value=56>Audiologist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("57") ) {out.println("selected");}%> value=57>Speech/Language Pathologist</option>
<option value=0> </option>
<option value=58>---Technologist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("58") ) {out.println("selected");}%> value=58>Medical Technologist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("59") ) {out.println("selected");}%> value=59>Cytotechnologist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("60") ) {out.println("selected");}%> value=60>Nuclear Medicine Technologist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("61") ) {out.println("selected");}%> value=61>Radiation Therapy Technologist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("62") ) {out.println("selected");}%> value=62>Radiologic Technologist</option>
<option value=0> </option>
<option value=63>---Other Health Care Practitioner</option>
<option <%if (CurrentSelection.equalsIgnoreCase("63") ) {out.println("selected");}%> value=63>Acupuncturist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("64") ) {out.println("selected");}%> value=64>Athletic Trainer</option>
<option <%if (CurrentSelection.equalsIgnoreCase("65") ) {out.println("selected");}%> value=65>Homeopath</option>
<option <%if (CurrentSelection.equalsIgnoreCase("66") ) {out.println("selected");}%> value=66>Medical Assistant</option>
<option <%if (CurrentSelection.equalsIgnoreCase("67") ) {out.println("selected");}%> value=67>Midwife, Lay (Non-Nurse)</option>
<option <%if (CurrentSelection.equalsIgnoreCase("68") ) {out.println("selected");}%> value=68>Naturopath</option>
<option <%if (CurrentSelection.equalsIgnoreCase("69") ) {out.println("selected");}%> value=69>Orthotics/Prosthetics Fitter</option>
<option <%if (CurrentSelection.equalsIgnoreCase("70") ) {out.println("selected");}%> value=70>Perfusionist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("71") ) {out.println("selected");}%> value=71>Psychiatric Technician</option>
<option <%if (CurrentSelection.equalsIgnoreCase("72") ) {out.println("selected");}%> value=72>Other Health Care Practitioner - Not Classified, Specify in Sub-Type</option>
<option value=0> </option>
<option value=73>---Health Care Facility Administrator</option>
<option <%if (CurrentSelection.equalsIgnoreCase("73") ) {out.println("selected");}%> value=73>Adult Care Facility Administrator</option>
<option <%if (CurrentSelection.equalsIgnoreCase("74") ) {out.println("selected");}%> value=74>Hospital Administrator</option>
<option <%if (CurrentSelection.equalsIgnoreCase("75") ) {out.println("selected");}%> value=75>Long-Term Care Administrator</option>
<option value=0> </option>
<option value=76>---Other Occupation</option>
<option <%if (CurrentSelection.equalsIgnoreCase("76") ) {out.println("selected");}%> value=76>Accountant</option>
<option <%if (CurrentSelection.equalsIgnoreCase("77") ) {out.println("selected");}%> value=77>Bookkeeper</option>
<option <%if (CurrentSelection.equalsIgnoreCase("78") ) {out.println("selected");}%> value=78>Business Manager</option>
<option <%if (CurrentSelection.equalsIgnoreCase("79") ) {out.println("selected");}%> value=79>Business Owner</option>
<option <%if (CurrentSelection.equalsIgnoreCase("80") ) {out.println("selected");}%> value=80>Corporate Officer</option>
<option <%if (CurrentSelection.equalsIgnoreCase("81") ) {out.println("selected");}%> value=81>Insurance Agent</option>
<option <%if (CurrentSelection.equalsIgnoreCase("82") ) {out.println("selected");}%> value=82>Insurance Broker</option>
<option <%if (CurrentSelection.equalsIgnoreCase("83") ) {out.println("selected");}%> value=83>Researcher, Clinical</option>
<option <%if (CurrentSelection.equalsIgnoreCase("84") ) {out.println("selected");}%> value=84>Salesperson</option>
<option <%if (CurrentSelection.equalsIgnoreCase("85") ) {out.println("selected");}%> value=85>Other Occupation - Not Classified, Specify in Sub-Type</option>
