<%

String CurrentSelection = "";
try
{
	CurrentSelection = request.getParameter ("CurrentSelection");
}
catch(Exception e)
{
}
if (CurrentSelection==null)
{
	CurrentSelection ="";
}
%>
<option <%if (CurrentSelection.equalsIgnoreCase("0") ) {out.println("selected");}%> value=0>None</option>
<option <%if (CurrentSelection.equalsIgnoreCase("1") ) {out.println("selected");}%> value=1>Other: Single-Visit</option>
<option <%if (CurrentSelection.equalsIgnoreCase("2") ) {out.println("selected");}%> value=2>Other: Multiple-Visit</option>
<option <%if (CurrentSelection.equalsIgnoreCase("3") ) {out.println("selected");}%> value=3>Other</option>
<option <%if (CurrentSelection.equalsIgnoreCase("4") ) {out.println("selected");}%> value=4>Diag: MRI</option>
<option <%if (CurrentSelection.equalsIgnoreCase("5") ) {out.println("selected");}%> value=5>Diag: CT</option>
<option <%if (CurrentSelection.equalsIgnoreCase("6") ) {out.println("selected");}%> value=6>Diag: EMG</option>
<option <%if (CurrentSelection.equalsIgnoreCase("7") ) {out.println("selected");}%> value=7>Diag: NCV</option>
<option <%if (CurrentSelection.equalsIgnoreCase("8") ) {out.println("selected");}%> value=8>Diag: PET</option>
<option <%if (CurrentSelection.equalsIgnoreCase("9") ) {out.println("selected");}%> value=9>Diag: Other</option>
<option <%if (CurrentSelection.equalsIgnoreCase("10") ) {out.println("selected");}%> value=10>Lab</option>
