<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.bltCompanyUserAccountLU,com.winstaff.bltCompanyAdminLU_List_LU_CompanyID,com.winstaff.bltPracticeMaster,com.winstaff.bltPhysicianMaster,com.winstaff.searchDB2,com.winstaff.bltCompanyAdminLU,java.util.Vector,java.util.Enumeration,com.winstaff.ListElement,com.winstaff.bltUserAccount" %>
<%/*
    filename: out\jsp\tUserAccount_form_sub.jsp
    JSP AutoGen on Mar/19/2002
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>


<%

//initial declaration of list class and parentID
    Integer        iCompanyID        =    null;
    boolean accessValid = false;
    if (pageControllerHash.containsKey("iCompanyID")) 
    {
        iCompanyID        =    (Integer)pageControllerHash.get("iCompanyID");
        if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
        {
            accessValid = true;
        }
    }
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
      java.text.SimpleDateFormat displayDateSDF1 = new java.text.SimpleDateFormat("MM/dd/yyyy");
      java.text.SimpleDateFormat displayDateSDF2 = new java.text.SimpleDateFormat("MMM/dd/yyyy");
      java.text.SimpleDateFormat displayDateSDF3 = new java.text.SimpleDateFormat("MMMM dd yyyy");

//initial declaration of list class and parentID

    bltUserAccount        UserAccount        =    new    bltUserAccount();

String testChangeID = "0";





try
{
    java.util.Date testObj = null ;
    try
    {
        testObj = (displayDateSDF1.parse(request.getParameter("UniqueCreateDate"))) ;
    }
    catch(Exception sdf1)
    {

        try
        {
            testObj = (displayDateSDF2.parse(request.getParameter("UniqueCreateDate"))) ;
        }
        catch(Exception sdf2)
        {

            try
            {
                testObj = (displayDateSDF3.parse(request.getParameter("UniqueCreateDate"))) ;
            }
            catch(Exception sdf3)
            {
                if (request.getParameter("UniqueCreateDate").equalsIgnoreCase("today")) 
                {
                    testObj = new java.util.Date() ;
                }
                else if (request.getParameter("UniqueCreateDate").equalsIgnoreCase("current")) 
                {
                    testObj = displayDateSDF1.parse("01/02/1800") ;
                }
                else if (request.getParameter("UniqueCreateDate").equalsIgnoreCase("na")) 
                {
                    testObj = displayDateSDF1.parse("01/03/1800") ;
                }
                else
                {
                    testObj = displayDateSDF1.parse("01/01/1800") ;
                }
            }

        }

    }

    if ( !UserAccount.getUniqueCreateDate().equals(testObj)  )
    {
         UserAccount.setUniqueCreateDate( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount UniqueCreateDate not set. this is ok-not an error");
}

try
{
    java.util.Date testObj = null ;
    try
    {
        testObj = (displayDateSDF1.parse(request.getParameter("UniqueModifyDate"))) ;
    }
    catch(Exception sdf1)
    {

        try
        {
            testObj = (displayDateSDF2.parse(request.getParameter("UniqueModifyDate"))) ;
        }
        catch(Exception sdf2)
        {

            try
            {
                testObj = (displayDateSDF3.parse(request.getParameter("UniqueModifyDate"))) ;
            }
            catch(Exception sdf3)
            {
                if (request.getParameter("UniqueModifyDate").equalsIgnoreCase("today")) 
                {
                    testObj = new java.util.Date() ;
                }
                else if (request.getParameter("UniqueModifyDate").equalsIgnoreCase("current")) 
                {
                    testObj = displayDateSDF1.parse("01/02/1800") ;
                }
                else if (request.getParameter("UniqueModifyDate").equalsIgnoreCase("na")) 
                {
                    testObj = displayDateSDF1.parse("01/03/1800") ;
                }
                else
                {
                    testObj = displayDateSDF1.parse("01/01/1800") ;
                }
            }

        }

    }

    if ( !UserAccount.getUniqueModifyDate().equals(testObj)  )
    {
         UserAccount.setUniqueModifyDate( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount UniqueModifyDate not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("UniqueModifyComments")) ;
    if ( !UserAccount.getUniqueModifyComments().equals(testObj)  )
    {
         UserAccount.setUniqueModifyComments( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount UniqueModifyComments not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("PLCID")) ;
    if ( !UserAccount.getPLCID().equals(testObj)  )
    {
         UserAccount.setPLCID( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount PLCID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("StartPage")) ;
    if ( !UserAccount.getStartPage().equals(testObj)  )
    {
         UserAccount.setStartPage( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount StartPage not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AccountType")) ;
    if ( !UserAccount.getAccountType().equals(testObj)  )
    {
         UserAccount.setAccountType( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount AccountType not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ReferenceID")) ;
    if ( !UserAccount.getReferenceID().equals(testObj)  )
    {
         UserAccount.setReferenceID( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ReferenceID not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("AccessType")) ;
    if ( !UserAccount.getAccessType().equals(testObj)  )
    {
         UserAccount.setAccessType( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount AccessType not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("Status")) ;
    if ( !UserAccount.getStatus().equals(testObj)  )
    {
         UserAccount.setStatus( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount Status not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LogonUserName")) ;
    if ( !UserAccount.getLogonUserName().equals(testObj)  )
    {
         UserAccount.setLogonUserName( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount LogonUserName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("LogonUserPassword")) ;
    if ( !UserAccount.getLogonUserPassword().equals(testObj)  )
    {
         UserAccount.setLogonUserPassword( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount LogonUserPassword not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttestKeyword1")) ;
    if ( !UserAccount.getAttestKeyword1().equals(testObj)  )
    {
         UserAccount.setAttestKeyword1( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount AttestKeyword1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("AttestKeyword2")) ;
    if ( !UserAccount.getAttestKeyword2().equals(testObj)  )
    {
         UserAccount.setAttestKeyword2( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount AttestKeyword2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactFirstName")) ;
    if ( !UserAccount.getContactFirstName().equals(testObj)  )
    {
         UserAccount.setContactFirstName( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactFirstName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactLastName")) ;
    if ( !UserAccount.getContactLastName().equals(testObj)  )
    {
         UserAccount.setContactLastName( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactLastName not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactEmail")) ;
    if ( !UserAccount.getContactEmail().equals(testObj)  )
    {
         UserAccount.setContactEmail( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactEmail not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactAddress1")) ;
    if ( !UserAccount.getContactAddress1().equals(testObj)  )
    {
         UserAccount.setContactAddress1( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactAddress1 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactAddress2")) ;
    if ( !UserAccount.getContactAddress2().equals(testObj)  )
    {
         UserAccount.setContactAddress2( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactAddress2 not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactCity")) ;
    if ( !UserAccount.getContactCity().equals(testObj)  )
    {
         UserAccount.setContactCity( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactCity not set. this is ok-not an error");
}

try
{
    Integer testObj = new Integer(request.getParameter("ContactStateID")) ;
    if ( !UserAccount.getContactStateID().equals(testObj)  )
    {
         UserAccount.setContactStateID( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactStateID not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactZIP")) ;
    if ( !UserAccount.getContactZIP().equals(testObj)  )
    {
         UserAccount.setContactZIP( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactZIP not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("ContactPhone")) ;
    if ( !UserAccount.getContactPhone().equals(testObj)  )
    {
         UserAccount.setContactPhone( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount ContactPhone not set. this is ok-not an error");
}

try
{
    String testObj = new String(request.getParameter("Comments")) ;
    if ( !UserAccount.getComments().equals(testObj)  )
    {
         UserAccount.setComments( testObj   );
         testChangeID = "1";
    }

}
catch(Exception e)
{
//     out.println("UserAccount Comments not set. this is ok-not an error");
}

// If an edit, update information; if new, append information
if (testChangeID.equalsIgnoreCase("1"))
{
	UserAccount.setUniqueModifyDate(new java.util.Date());
	String UserLogonDescription = "n/a";
	if (pageControllerHash.containsKey("UserLogonDescription"))
	{
	    UserLogonDescription = (String)pageControllerHash.get("UserLogonDescription");
	}
    //if ((CurrentUserAccount.getAccessType().intValue()==2)||(CurrentUserAccount.getAccessType().intValue()==4))
    {
	    UserAccount.setUniqueModifyComments("Modified by: "+UserLogonDescription);

//START-verify this create

boolean isVerified = false;
//if (UserAccount.getUniqueID().intValue()==0)
{
	isVerified = true;
}

//END-verify this create

if (isVerified)
{
	searchDB2 mySDB = new searchDB2();
	String mySQL  = "select * from tUserAccount where (LogonUserName = '"+request.getParameter("LogonUserName")+"' )";
	java.sql.ResultSet myRS = mySDB.executeStatement(mySQL);
	if (!myRS.next())
	{
		String P1 = new String (request.getParameter("LogonUserPassword1"));
		String P2 = new String (request.getParameter("LogonUserPassword2"));
		com.winstaff.password.Encrypt myEnc = new com.winstaff.password.Encrypt();
		if (   myEnc.checkPasswords(P1,P2)  )
		{
			UserAccount.setLogonUserPassword(myEnc.getMD5Base64(P1));
			UserAccount.commitData();
			bltCompanyUserAccountLU        working_bltCompanyUserAccountLU = new bltCompanyUserAccountLU();
			working_bltCompanyUserAccountLU.setCompanyID(iCompanyID);
			working_bltCompanyUserAccountLU.setUserID(UserAccount.getUniqueID());
			working_bltCompanyUserAccountLU.commitData();
			String routePageReference = request.getParameter("routePageReference");
			String nextPage=null;
			if (routePageReference!=null)
			{
			        if (pageControllerHash.containsKey(routePageReference))
			        {
					nextPage = (String)pageControllerHash.get(routePageReference);
			        }
			        else if (pageControllerHash.containsKey("sLocalChildReturnPage"))
			        {
			            nextPage = (String)pageControllerHash.get("sLocalChildReturnPage");
			        }
			}
			if (nextPage!=null)
			{
			    response.sendRedirect(nextPage+"?EDIT=edit");
			}
		}
		else
		{
		%>
			<script langauge=javascript>
			document.write();
			alert("Your Password is invalid.  Please try again.\nNote, for security reasons you may need to re-enter your passwords and keywords.");
			history.go(-1);
			</script>
		<%
		}

	}
	else
	{
	%>
		<script langauge=javascript>
		alert("Your User Name is invalid or already taken.  Please try again.");
		history.go(-1);
		</script>
	<%
	}
}






    }
}



  }
  else
  {
   out.println("illegal");
  }
  %>
