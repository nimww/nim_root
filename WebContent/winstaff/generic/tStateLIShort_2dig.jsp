<%

String CurrentSelection = "";
try
{
	CurrentSelection = request.getParameter ("CurrentSelection");
}
catch(Exception e)
{
}
if (CurrentSelection==null)
{
	CurrentSelection ="";
}
%>
<option <%if (CurrentSelection.equalsIgnoreCase("") ) {out.println("selected");}%> value="">All</option>
<option <%if (CurrentSelection.equalsIgnoreCase("AL") ) {out.println("selected");}%> value=AL>AL</option>
<option <%if (CurrentSelection.equalsIgnoreCase("AK") ) {out.println("selected");}%> value=AK>AK</option>
<option <%if (CurrentSelection.equalsIgnoreCase("AZ") ) {out.println("selected");}%> value=AZ>AZ</option>
<option <%if (CurrentSelection.equalsIgnoreCase("AR") ) {out.println("selected");}%> value=AR>AR</option>
<option <%if (CurrentSelection.equalsIgnoreCase("CA") ) {out.println("selected");}%> value=CA>CA</option>
<option <%if (CurrentSelection.equalsIgnoreCase("CO") ) {out.println("selected");}%> value=CO>CO</option>
<option <%if (CurrentSelection.equalsIgnoreCase("CT") ) {out.println("selected");}%> value=CT>CT</option>
<option <%if (CurrentSelection.equalsIgnoreCase("DE") ) {out.println("selected");}%> value=DE>DE</option>
<option <%if (CurrentSelection.equalsIgnoreCase("DC") ) {out.println("selected");}%> value=DC>DC</option>
<option <%if (CurrentSelection.equalsIgnoreCase("FL") ) {out.println("selected");}%> value=FL>FL</option>
<option <%if (CurrentSelection.equalsIgnoreCase("GA") ) {out.println("selected");}%> value=GA>GA</option>
<option <%if (CurrentSelection.equalsIgnoreCase("GU") ) {out.println("selected");}%> value=GU>GU</option>
<option <%if (CurrentSelection.equalsIgnoreCase("HI") ) {out.println("selected");}%> value=HI>HI</option>
<option <%if (CurrentSelection.equalsIgnoreCase("ID") ) {out.println("selected");}%> value=ID>ID</option>
<option <%if (CurrentSelection.equalsIgnoreCase("IL") ) {out.println("selected");}%> value=IL>IL</option>
<option <%if (CurrentSelection.equalsIgnoreCase("IN") ) {out.println("selected");}%> value=IN>IN</option>
<option <%if (CurrentSelection.equalsIgnoreCase("IA") ) {out.println("selected");}%> value=IA>IA</option>
<option <%if (CurrentSelection.equalsIgnoreCase("KS") ) {out.println("selected");}%> value=KS>KS</option>
<option <%if (CurrentSelection.equalsIgnoreCase("KY") ) {out.println("selected");}%> value=KY>KY</option>
<option <%if (CurrentSelection.equalsIgnoreCase("LA") ) {out.println("selected");}%> value=LA>LA</option>
<option <%if (CurrentSelection.equalsIgnoreCase("ME") ) {out.println("selected");}%> value=ME>ME</option>
<option <%if (CurrentSelection.equalsIgnoreCase("MD") ) {out.println("selected");}%> value=MD>MD</option>
<option <%if (CurrentSelection.equalsIgnoreCase("MA") ) {out.println("selected");}%> value=MA>MA</option>
<option <%if (CurrentSelection.equalsIgnoreCase("MI") ) {out.println("selected");}%> value=MI>MI</option>
<option <%if (CurrentSelection.equalsIgnoreCase("MN") ) {out.println("selected");}%> value=MN>MN</option>
<option <%if (CurrentSelection.equalsIgnoreCase("MS") ) {out.println("selected");}%> value=MS>MS</option>
<option <%if (CurrentSelection.equalsIgnoreCase("MO") ) {out.println("selected");}%> value=MO>MO</option>
<option <%if (CurrentSelection.equalsIgnoreCase("MT") ) {out.println("selected");}%> value=MT>MT</option>
<option <%if (CurrentSelection.equalsIgnoreCase("NE") ) {out.println("selected");}%> value=NE>NE</option>
<option <%if (CurrentSelection.equalsIgnoreCase("NV") ) {out.println("selected");}%> value=NV>NV</option>
<option <%if (CurrentSelection.equalsIgnoreCase("NH") ) {out.println("selected");}%> value=NH>NH</option>
<option <%if (CurrentSelection.equalsIgnoreCase("NJ") ) {out.println("selected");}%> value=NJ>NJ</option>
<option <%if (CurrentSelection.equalsIgnoreCase("NM") ) {out.println("selected");}%> value=NM>NM</option>
<option <%if (CurrentSelection.equalsIgnoreCase("NY") ) {out.println("selected");}%> value=NY>NY</option>
<option <%if (CurrentSelection.equalsIgnoreCase("NC") ) {out.println("selected");}%> value=NC>NC</option>
<option <%if (CurrentSelection.equalsIgnoreCase("ND") ) {out.println("selected");}%> value=ND>ND</option>
<option <%if (CurrentSelection.equalsIgnoreCase("OH") ) {out.println("selected");}%> value=OH>OH</option>
<option <%if (CurrentSelection.equalsIgnoreCase("OK") ) {out.println("selected");}%> value=OK>OK</option>
<option <%if (CurrentSelection.equalsIgnoreCase("OR") ) {out.println("selected");}%> value=OR>OR</option>
<option <%if (CurrentSelection.equalsIgnoreCase("PA") ) {out.println("selected");}%> value=PA>PA</option>
<option <%if (CurrentSelection.equalsIgnoreCase("PR") ) {out.println("selected");}%> value=PR>PR</option>
<option <%if (CurrentSelection.equalsIgnoreCase("RI") ) {out.println("selected");}%> value=RI>RI</option>
<option <%if (CurrentSelection.equalsIgnoreCase("SC") ) {out.println("selected");}%> value=SC>SC</option>
<option <%if (CurrentSelection.equalsIgnoreCase("SD") ) {out.println("selected");}%> value=SD>SD</option>
<option <%if (CurrentSelection.equalsIgnoreCase("TN") ) {out.println("selected");}%> value=TN>TN</option>
<option <%if (CurrentSelection.equalsIgnoreCase("TX") ) {out.println("selected");}%> value=TX>TX</option>
<option <%if (CurrentSelection.equalsIgnoreCase("UT") ) {out.println("selected");}%> value=UT>UT</option>
<option <%if (CurrentSelection.equalsIgnoreCase("VT") ) {out.println("selected");}%> value=VT>VT</option>
<option <%if (CurrentSelection.equalsIgnoreCase("VA") ) {out.println("selected");}%> value=VA>VA</option>
<option <%if (CurrentSelection.equalsIgnoreCase("VI") ) {out.println("selected");}%> value=VI>VI</option>
<option <%if (CurrentSelection.equalsIgnoreCase("WA") ) {out.println("selected");}%> value=WA>WA</option>
<option <%if (CurrentSelection.equalsIgnoreCase("WV") ) {out.println("selected");}%> value=WV>WV</option>
<option <%if (CurrentSelection.equalsIgnoreCase("WI") ) {out.println("selected");}%> value=WI>WI</option>
<option <%if (CurrentSelection.equalsIgnoreCase("WY") ) {out.println("selected");}%> value=WY>WY</option>
