<%

String CurrentSelection = "";
try
{
	CurrentSelection = request.getParameter ("CurrentSelection");
}
catch(Exception e)
{
}
if (CurrentSelection==null)
{
	CurrentSelection ="";
}
%>
<%if (CurrentSelection.equalsIgnoreCase("0") ) {out.println("NA");}%>
<%if (CurrentSelection.equalsIgnoreCase("42") ) {out.println("Traditional: 0.7T");}%>
<%if (CurrentSelection.equalsIgnoreCase("33") ) {out.println("Traditional: 1.0T");}%>
<%if (CurrentSelection.equalsIgnoreCase("34") ) {out.println("Traditional: 1.5T");}%>
<%if (CurrentSelection.equalsIgnoreCase("35") ) {out.println("Traditional: 3.0T");}%>
<%if (CurrentSelection.equalsIgnoreCase("36") ) {out.println("Traditional(Wide): 1.5T");}%>
<%if (CurrentSelection.equalsIgnoreCase("37") ) {out.println("Traditional(Wide): 3.0T");}%>
<%if (CurrentSelection.equalsIgnoreCase("43") ) {out.println("Open: 0.2T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("38") ) {out.println("Open: 0.3T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("44") ) {out.println("Open: 0.35T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("39") ) {out.println("Open: 0.7T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("41") ) {out.println("Open: 1.5T");}%>
<%if (CurrentSelection.equalsIgnoreCase("45") ) {out.println("Standup: 0.6T");}%>
<%if (CurrentSelection.equalsIgnoreCase("46") ) {out.println("CT - 4 Slices");}%>
<%if (CurrentSelection.equalsIgnoreCase("47") ) {out.println("CT - 6 Slices");}%>
<%if (CurrentSelection.equalsIgnoreCase("48") ) {out.println("CT - 8 Slices");}%>
<%if (CurrentSelection.equalsIgnoreCase("49") ) {out.println("CT - 16 Slices");}%>
<%if (CurrentSelection.equalsIgnoreCase("50") ) {out.println("CT - 24 Slices");}%>
<%if (CurrentSelection.equalsIgnoreCase("51") ) {out.println("CT - 32 Slices");}%>
<%if (CurrentSelection.equalsIgnoreCase("52") ) {out.println("CT - 64 Slices");}%>
<%if (CurrentSelection.equalsIgnoreCase("53") ) {out.println("CT - 160 Slices");}%>
<%if (CurrentSelection.equalsIgnoreCase("54") ) {out.println("CT - 256 Slices");}%>
<%if (CurrentSelection.equalsIgnoreCase("55") ) {out.println("CT - 320 Slices");}%>
<%if (CurrentSelection.equalsIgnoreCase("56") ) {out.println("CT - 640 Slices");}%>
<%if (CurrentSelection.equalsIgnoreCase("40") ) {out.println("MRI Generic .6T Stand Up");}%>
<%if (CurrentSelection.equalsIgnoreCase("2") ) {out.println("GE - Ovation - 0.35T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("3") ) {out.println("GE - Contour - 0.5T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("4") ) {out.println("GE - Open Speed - 0.7T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("5") ) {out.println("GE - Horizon - 1T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("6") ) {out.println("GE - Signa LX - 1.5T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("7") ) {out.println("GE - Optima - 1.5T - Super");}%>
<%if (CurrentSelection.equalsIgnoreCase("8") ) {out.println("GE - Signa LX 3T - 3T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("9") ) {out.println("Siemens - Open Viva - 0.2T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("10") ) {out.println("Siemens - Concerto - 0.2T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("11") ) {out.println("Siemens - C - 0.35T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("12") ) {out.println("Siemens - Harmony - 1T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("13") ) {out.println("Siemens - Symphony - 1.5T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("14") ) {out.println("Siemens - Essenza - 1.5T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("15") ) {out.println("Siemens - Avanto - 1.5T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("16") ) {out.println("Siemens - Espree - 1.5T - Super");}%>
<%if (CurrentSelection.equalsIgnoreCase("17") ) {out.println("Siemens - Trio - 3T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("18") ) {out.println("Siemens - Verio - 3T - Super");}%>
<%if (CurrentSelection.equalsIgnoreCase("19") ) {out.println("Philips - Panorama - 0.6T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("20") ) {out.println("Philips - Intera - 1.5T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("21") ) {out.println("Philips - Eclipse - 1.5T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("22") ) {out.println("Philips  - Acheiva - 1.5T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("23") ) {out.println("Philips - Acheiva - 3T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("24") ) {out.println("Toshiba - Vantage - 1.5T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("25") ) {out.println("Toshiba - Vantage Atlas - 1.5T - Traditional");}%>
<%if (CurrentSelection.equalsIgnoreCase("26") ) {out.println("Toshiba - Vantage Titan - 1.5T - Super");}%>
<%if (CurrentSelection.equalsIgnoreCase("27") ) {out.println("Hitachi - Airis - 0.3T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("28") ) {out.println("Hitachi - Airis II - 0.3T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("29") ) {out.println("Hitachi - MRP 7000 - 0.3T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("30") ) {out.println("Hitachi - Altaire - 0.7T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("31") ) {out.println("Hitachi - Oasis - 1.2T - Open");}%>
<%if (CurrentSelection.equalsIgnoreCase("32") ) {out.println("Hitachi - Echelon - 1.5T - Traditional");}%>
