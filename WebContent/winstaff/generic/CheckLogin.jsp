<%@page  import="com.winstaff.*" %><%
Integer UserSecurityGroupID = null;
String sINTNext = null;
Integer isShowAuditI = null;
boolean isShowAudit=false;
boolean isQuickMode=true;

String userAgent = request.getHeader("user-agent");
boolean isUserAndroid = false;
if (userAgent.toUpperCase().indexOf("ANDROID")>=0)
{
	isUserAndroid = true;
}

Integer iCurrentUserID = null;
bltUserAccount CurrentUserAccount = null;
bltPracticeMaster CurrentUser_PracticeMaster = null;
java.util.Hashtable pageControllerHash = null;
String thePLCID =null;

boolean isBasic = false;
//boolean isNIM = false;
	String REMOTE_USER_ip = request.getRemoteAddr();
	String REMOTE_USER_host = request.getRemoteHost();
	
	boolean isAccounting = false;
	boolean isGenAdmin = false;
	boolean isProvider = false;
	boolean isProvider2 = false;
	boolean isProvider3 = false;
	boolean isIC = false;
	boolean isAdjuster = false;
	boolean isAdjuster2 = false;
	boolean isAdjuster3 = false;
	boolean isPayer = false;
	boolean isScheduler = false;
	boolean isScheduler2 = false;
	boolean isScheduler3 = false;
	boolean isScheduler4 = false;
	boolean isReporter = false;

if (session.getAttribute("pageControllerHash")!=null)
{
	pageControllerHash = (java.util.Hashtable)session.getAttribute("pageControllerHash");
	if (pageControllerHash.containsKey("iCurrentUserID"))
	{
		iCurrentUserID = (Integer)pageControllerHash.get("iCurrentUserID");
		CurrentUserAccount = new bltUserAccount(iCurrentUserID);
		thePLCID = (String)pageControllerHash.get("plcID");
	}
	else
	{
		//out.println("ERROR -No User");
	}
	if (pageControllerHash.containsKey("isBasic"))
	{
		
		isBasic = ((Boolean)pageControllerHash.get("isBasic")).booleanValue();
	}
	else
	{
		//out.println("ERROR -No User");
	}
	if (CurrentUserAccount.getUserID()==18920 || CurrentUserAccount.getUserID()==18538 || CurrentUserAccount.getUserID()==19852){
		//True for Lizzie, Silvia, Paul
		//Use to disable few items they do not need as it slows load time. Enable few bits of info that are billing related. 
		isAccounting =true;
	}
	if (CurrentUserAccount.getAccountType().equalsIgnoreCase("PhysicianID")) 
	{
		isProvider=true;
	}
	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("PhysicianID2")) 
	{
		isProvider=true;
		isProvider2=true;
	}
	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("PhysicianID3")) 
	{
		isProvider=true;
		isProvider2=true;
		isProvider3=true;
	}
	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("AdjusterID")) 
	{
		isAdjuster=true;
	}
	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("AdjusterID2")) 
	{
		isAdjuster=true;
		isAdjuster2=true;
	}
	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("AdjusterID3")) 
	{
		isAdjuster=true;
		isAdjuster2=true;
		isAdjuster3=true;
		
	}
	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("PayerAdminID")) 
	{
		isPayer=true;
	}
	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("SchedulerID")) 
	{
		isScheduler=true;
		isReporter=true;
	}
	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("SchedulerID2")) 
	{
		isScheduler=true;
		isScheduler2=true;
		isReporter=true;
	}
	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("SchedulerID3")) 
	{
		isScheduler=true;
		isScheduler2=true;
		isScheduler3=true;
		isReporter=true;
	}
	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("SchedulerID4")) 
	{
		isScheduler=true;
		isScheduler2=true;
		isScheduler3=true;
		isReporter=true;
		isScheduler4=true;
	}
	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("GenAdminID")) 
	{
		isGenAdmin = true;
		isScheduler=true;
		isScheduler2=true;
		isScheduler3=true;
		isReporter=true;
	}
	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("ReporterID")) 
	{
		isReporter=true;
	}
	else if (CurrentUserAccount.getAccountType().equalsIgnoreCase("ICID")) 
	{
		isIC=true;
		CurrentUser_PracticeMaster = new bltPracticeMaster(CurrentUserAccount.getReferenceID());
	}
	

UserSecurityGroupID = (Integer) pageControllerHash.get("UserSecurityGroupID");

sINTNext = (String) pageControllerHash.get("sINTNext");

isShowAuditI = (Integer) pageControllerHash.get("isShowAudit");
isShowAudit=false;
if (isShowAuditI.intValue()==1)
{
   isShowAudit = true;
}


}
else
{
	out.println("ERROR - Not Authenticated");
}

/* remove ste 2010-05-12
navigationTracer navTrace = null;
if (pageControllerHash.containsKey("navTrace"))
{
	navTrace = (navigationTracer)pageControllerHash.get("navTrace");
}
else
{
	navTrace = new navigationTracer();

}
*/


//System.out.println("Version 4.0 beta");
%>