<p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px;" >
<%
Integer CurrentSelection = new Integer(0);
try
{
	CurrentSelection = new Integer(request.getParameter ("CurrentSelection"));
	if (CurrentSelection!=null&&CurrentSelection.intValue()>0)
	{
		com.winstaff.bltUserAccount myUA = new com.winstaff.bltUserAccount (new Integer(CurrentSelection));
		com.winstaff.bltNIM3_PayerMaster myPA = new com.winstaff.bltNIM3_PayerMaster (myUA.getPayerID());
		if (myUA.getAccountType().equalsIgnoreCase("PhysicianID")||myUA.getAccountType().equalsIgnoreCase("SchedulerID")||myUA.getAccountType().equalsIgnoreCase("SchedulerID2")||myUA.getAccountType().equalsIgnoreCase("AdjusterID")||myUA.getAccountType().equalsIgnoreCase("AdjusterID2")||myUA.getAccountType().equalsIgnoreCase("AdjusterID3")||myUA.getAccountType().equalsIgnoreCase("PayerAdminID"))
		{
				%><table border="0" cellspacing="0" cellpadding="3">
                  <tr style="font-family: Arial, Helvetica, sans-serif;font-size:24px;color:#000">
                    <td colspan="2">Contact Details</td>
                  </tr>
                  <tr>
                    <td>Name</td>
                    <td><strong><%=myUA.getContactFirstName()%>&nbsp;<%=myUA.getContactLastName()%></strong>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>Payer/Group</td>
                    <td><strong><%=myPA.getPayerName()%></strong>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>Phone</td>
                    <td><strong><%=myUA.getContactPhone()%></strong>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>Fax</td>
                    <td><strong><%=myUA.getContactFax()%></strong>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td><strong><%=myUA.getContactEmail()%></strong>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>UserName</td>
                    <td><strong><%=myUA.getLogonUserName()%></strong>&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="2"><hr />Address (if on file)</td>
                  </tr>
                  <tr>
                    <td colspan="2"><strong><%=myUA.getCompanyName()%></strong><br />
<%=myUA.getContactAddress1()%>&nbsp;<%=myUA.getContactAddress2()%><br />
<%=myUA.getContactCity()%>, <%=(new com.winstaff.bltStateLI(myUA.getContactStateID())).getShortState()%> <%=myUA.getContactZIP()%></td>
                  </tr>
                </table>
		<%
		}
	}
}
catch(Exception e)
{
}%>