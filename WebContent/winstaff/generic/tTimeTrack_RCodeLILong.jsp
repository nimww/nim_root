<%

String CurrentSelection = "";
try
{
	CurrentSelection = request.getParameter ("CurrentSelection");
}
catch(Exception e)
{
}
if (CurrentSelection==null)
{
	CurrentSelection ="";
}
%>
<option <%if (CurrentSelection.equalsIgnoreCase("0") ) {out.println("selected");}%> value=0>None </option>
<option <%if (CurrentSelection.equalsIgnoreCase("1") ) {out.println("selected");}%> value=1>Int- Fax Delay</option>
<option <%if (CurrentSelection.equalsIgnoreCase("2") ) {out.println("selected");}%> value=2>Int- ND - Busy</option>
<option <%if (CurrentSelection.equalsIgnoreCase("3") ) {out.println("selected");}%> value=3>Int- ND - Contract</option>
<option <%if (CurrentSelection.equalsIgnoreCase("4") ) {out.println("selected");}%> value=4>Int- Sched - Busy</option>
<option <%if (CurrentSelection.equalsIgnoreCase("5") ) {out.println("selected");}%> value=5>Int- Sched - Other</option>
<option <%if (CurrentSelection.equalsIgnoreCase("6") ) {out.println("selected");}%> value=6>Int- CPT Error</option>
<option <%if (CurrentSelection.equalsIgnoreCase("7") ) {out.println("selected");}%> value=7>Int- ICD Error</option>
<option <%if (CurrentSelection.equalsIgnoreCase("8") ) {out.println("selected");}%> value=8>Ext- Patient - Comm</option>
<option <%if (CurrentSelection.equalsIgnoreCase("9") ) {out.println("selected");}%> value=9>Ext- Patient - Resched</option>
<option <%if (CurrentSelection.equalsIgnoreCase("10") ) {out.println("selected");}%> value=10>Ext- Patient - NoShow</option>
<option <%if (CurrentSelection.equalsIgnoreCase("11") ) {out.println("selected");}%> value=11>Ext- Patient - Other</option>
<option <%if (CurrentSelection.equalsIgnoreCase("12") ) {out.println("selected");}%> value=12>Ext- Submission - Incomplete</option>
<option <%if (CurrentSelection.equalsIgnoreCase("13") ) {out.println("selected");}%> value=13>Ext- Submission - Delayed RX</option>
<option <%if (CurrentSelection.equalsIgnoreCase("14") ) {out.println("selected");}%> value=14>Ext- Submission - Invalid</option>
<option <%if (CurrentSelection.equalsIgnoreCase("15") ) {out.println("selected");}%> value=15>Ext- Approval - UR Comm</option>
<option <%if (CurrentSelection.equalsIgnoreCase("16") ) {out.println("selected");}%> value=16>Ext- Approval - UR Delay</option>
<option <%if (CurrentSelection.equalsIgnoreCase("17") ) {out.println("selected");}%> value=17>Ext- Provider - Resch</option>
<option <%if (CurrentSelection.equalsIgnoreCase("18") ) {out.println("selected");}%> value=18>Ext- Provider - Poor Exam</option>
<option <%if (CurrentSelection.equalsIgnoreCase("19") ) {out.println("selected");}%> value=19>Ext- Provider - Radiologist</option>
<option <%if (CurrentSelection.equalsIgnoreCase("20") ) {out.println("selected");}%> value=20>Ext- Provider - Other</option>
<option <%if (CurrentSelection.equalsIgnoreCase("21") ) {out.println("selected");}%> value=21>Ext- Billing - CPT Error</option>
<option <%if (CurrentSelection.equalsIgnoreCase("22") ) {out.println("selected");}%> value=22>Ext- Billing - ICD Error</option>
