<%

String CurrentSelection = "";
try
{
	CurrentSelection = request.getParameter ("CurrentSelection");
}
catch(Exception e)
{
}
if (CurrentSelection==null)
{
	CurrentSelection ="";
}
%>
<%
if (CurrentSelection.equalsIgnoreCase("0") ) {
%>
<span class="label label-warning"><i class="icon-question-sign icon-white"></i> Unknown</span>
<%
} else if (CurrentSelection.equalsIgnoreCase("2") ) {
%>
<span class="label label-important"><i class="icon-fire icon-white"></i> No</span>
<%
} else if (CurrentSelection.equalsIgnoreCase("1") ) {
%>
<span class="label label-success"><i class="icon-ok icon-white"></i> Yes</span>
<%
} else if (CurrentSelection.equalsIgnoreCase("3") ) {
%>
<span class="label label-inverse"><i class="icon-minus-sign icon-white"></i> NA</span>
<%
}
%>
