<%

String CurrentSelection = "";
try
{
	CurrentSelection = request.getParameter ("CurrentSelection");
}
catch(Exception e)
{
}
if (CurrentSelection==null)
{
	CurrentSelection ="";
}
%>
<option <%if (CurrentSelection.equalsIgnoreCase("0") ) {out.println("selected");}%> value=0>NA</option>
<option <%if (CurrentSelection.equalsIgnoreCase("-1") ) {out.println("selected");}%> value=0>------ MRI</option>
<option <%if (CurrentSelection.equalsIgnoreCase("33") ) {out.println("selected");}%> value=33>Traditional: 1.0T</option>
<option <%if (CurrentSelection.equalsIgnoreCase("34") ) {out.println("selected");}%> value=34>Traditional: 1.5T</option>
<option <%if (CurrentSelection.equalsIgnoreCase("35") ) {out.println("selected");}%> value=35>Traditional: 3.0T</option>
<option <%if (CurrentSelection.equalsIgnoreCase("36") ) {out.println("selected");}%> value=36>Traditional(Wide): 1.5T</option>
<option <%if (CurrentSelection.equalsIgnoreCase("37") ) {out.println("selected");}%> value=37>Traditional(Wide): 3.0T</option>
<option <%if (CurrentSelection.equalsIgnoreCase("43") ) {out.println("selected");}%> value=43>Open: 0.2T</option>
<option <%if (CurrentSelection.equalsIgnoreCase("38") ) {out.println("selected");}%> value=38>Open: 0.3T</option>
<option <%if (CurrentSelection.equalsIgnoreCase("44") ) {out.println("selected");}%> value=44>Open: 0.35T</option>
<option <%if (CurrentSelection.equalsIgnoreCase("39") ) {out.println("selected");}%> value=39>Open: 0.7T</option>
<option <%if (CurrentSelection.equalsIgnoreCase("41") ) {out.println("selected");}%> value=41>Open: 1.5T</option>
<option <%if (CurrentSelection.equalsIgnoreCase("40") ) {out.println("selected");}%> value=40>Standup:  0.6T</option>
<option <%if (CurrentSelection.equalsIgnoreCase("-1") ) {out.println("selected");}%> value=0>------ CT</option>
<option <%if (CurrentSelection.equalsIgnoreCase("46") ) {out.println("selected");}%> value=46>CT - 4 Slices</option>
<option <%if (CurrentSelection.equalsIgnoreCase("47") ) {out.println("selected");}%> value=47>CT - 6 Slices</option>
<option <%if (CurrentSelection.equalsIgnoreCase("48") ) {out.println("selected");}%> value=48>CT - 8 Slices</option>
<option <%if (CurrentSelection.equalsIgnoreCase("49") ) {out.println("selected");}%> value=49>CT - 16 Slices</option>
<option <%if (CurrentSelection.equalsIgnoreCase("50") ) {out.println("selected");}%> value=50>CT - 24 Slices</option>
<option <%if (CurrentSelection.equalsIgnoreCase("51") ) {out.println("selected");}%> value=51>CT - 32 Slices</option>
<option <%if (CurrentSelection.equalsIgnoreCase("52") ) {out.println("selected");}%> value=52>CT - 64 Slices</option>
<option <%if (CurrentSelection.equalsIgnoreCase("53") ) {out.println("selected");}%> value=53>CT - 160 Slices</option>
<option <%if (CurrentSelection.equalsIgnoreCase("54") ) {out.println("selected");}%> value=54>CT - 256 Slices</option>
<option <%if (CurrentSelection.equalsIgnoreCase("55") ) {out.println("selected");}%> value=55>CT - 320 Slices</option>
<option <%if (CurrentSelection.equalsIgnoreCase("56") ) {out.println("selected");}%> value=56>CT - 640 Slices</option>
<option <%if (CurrentSelection.equalsIgnoreCase("-1") ) {out.println("selected");}%> value=0>------ Legacy MRI</option>
<option <%if (CurrentSelection.equalsIgnoreCase("1") ) {out.println("selected");}%> value=1>GE - Profile - 0.2T - Open</option>
<option <%if (CurrentSelection.equalsIgnoreCase("2") ) {out.println("selected");}%> value=2>GE - Ovation - 0.35T - Open</option>
<option <%if (CurrentSelection.equalsIgnoreCase("3") ) {out.println("selected");}%> value=3>GE - Contour - 0.5T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("4") ) {out.println("selected");}%> value=4>GE - Open Speed - 0.7T - Open</option>
<option <%if (CurrentSelection.equalsIgnoreCase("5") ) {out.println("selected");}%> value=5>GE - Horizon - 1T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("6") ) {out.println("selected");}%> value=6>GE - Signa LX - 1.5T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("7") ) {out.println("selected");}%> value=7>GE - Optima - 1.5T - Super</option>
<option <%if (CurrentSelection.equalsIgnoreCase("8") ) {out.println("selected");}%> value=8>GE - Signa LX 3T - 3T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("9") ) {out.println("selected");}%> value=9>Siemens - Open Viva - 0.2T - Open</option>
<option <%if (CurrentSelection.equalsIgnoreCase("10") ) {out.println("selected");}%> value=10>Siemens - Concerto - 0.2T - Open</option>
<option <%if (CurrentSelection.equalsIgnoreCase("11") ) {out.println("selected");}%> value=11>Siemens - C - 0.35T - Open</option>
<option <%if (CurrentSelection.equalsIgnoreCase("12") ) {out.println("selected");}%> value=12>Siemens - Harmony - 1T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("13") ) {out.println("selected");}%> value=13>Siemens - Symphony - 1.5T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("14") ) {out.println("selected");}%> value=14>Siemens - Essenza - 1.5T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("15") ) {out.println("selected");}%> value=15>Siemens - Avanto - 1.5T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("16") ) {out.println("selected");}%> value=16>Siemens - Espree - 1.5T - Super</option>
<option <%if (CurrentSelection.equalsIgnoreCase("17") ) {out.println("selected");}%> value=17>Siemens - Trio - 3T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("18") ) {out.println("selected");}%> value=18>Siemens - Verio - 3T - Super</option>
<option <%if (CurrentSelection.equalsIgnoreCase("19") ) {out.println("selected");}%> value=19>Philips - Panorama - 0.6T - Open</option>
<option <%if (CurrentSelection.equalsIgnoreCase("20") ) {out.println("selected");}%> value=20>Philips - Intera - 1.5T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("21") ) {out.println("selected");}%> value=21>Philips - Eclipse - 1.5T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("22") ) {out.println("selected");}%> value=22>Philips  - Acheiva - 1.5T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("23") ) {out.println("selected");}%> value=23>Philips - Acheiva - 3T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("24") ) {out.println("selected");}%> value=24>Toshiba - Vantage - 1.5T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("25") ) {out.println("selected");}%> value=25>Toshiba - Vantage Atlas - 1.5T - Traditional</option>
<option <%if (CurrentSelection.equalsIgnoreCase("26") ) {out.println("selected");}%> value=26>Toshiba - Vantage Titan - 1.5T - Super</option>
<option <%if (CurrentSelection.equalsIgnoreCase("27") ) {out.println("selected");}%> value=27>Hitachi - Airis - 0.3T - Open</option>
<option <%if (CurrentSelection.equalsIgnoreCase("28") ) {out.println("selected");}%> value=28>Hitachi - Airis II - 0.3T - Open</option>
<option <%if (CurrentSelection.equalsIgnoreCase("29") ) {out.println("selected");}%> value=29>Hitachi - MRP 7000 - 0.3T - Open</option>
<option <%if (CurrentSelection.equalsIgnoreCase("30") ) {out.println("selected");}%> value=30>Hitachi - Altaire - 0.7T - Open</option>
<option <%if (CurrentSelection.equalsIgnoreCase("31") ) {out.println("selected");}%> value=31>Hitachi - Oasis - 1.2T - Open</option>
<option <%if (CurrentSelection.equalsIgnoreCase("32") ) {out.println("selected");}%> value=32>Hitachi - Echelon - 1.5T - Traditional</option>
