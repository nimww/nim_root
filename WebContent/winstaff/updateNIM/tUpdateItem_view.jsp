<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tUpdateItem_form.jsp
    Created on Nov/10/2004
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>

<%@ include file="../generic/generalDisplay.jsp" %>
<link href="../nim3/ui_200/style_sched.css" rel="stylesheet" type="text/css">

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
      <table width="100%" border="0" cellspacing="0" height="50">
        <tr>
          <td background="title1.jpg">&nbsp;</td>
        </tr>
      </table>
      <input name=Submit2 type=button class="inputButton_md_Default" value="Return to Maintenance" onClick="document.location='../nim3/Maint_Home.jsp';">
<br>
<table cellpadding=0 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td>
    <td>
      <%
//initial declaration of list class and parentID
    Integer        iUpdateItemID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = new Integer(2);
   if (iSecurityCheck.intValue()!=0&&isScheduler2)
   {
    bltUpdateItem        UpdateItem        =    null;
	boolean isEdit=false;
    if (request.getParameter("EDITID")!=null) 
    {
        iUpdateItemID        =    new Integer( request.getParameter("EDITID"));
		isEdit=true;
        accessValid = true;    
		UpdateItem        =    new    bltUpdateItem(iUpdateItemID,UserSecurityGroupID);
	}
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tUpdateItem_form.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID



//fields
	  String theClass ="tdBase";
	  
	  %>
        <table border=1 bordercolor=#333333 cellpadding=0 cellspacing=0 >
         <tr>
           <td >
            <table width=100% border="1" cellpadding=10 cellspacing=0>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Item ID:</b></p>
                  </td><td valign=top><p><%=UpdateItem.getUpdateItemID()%></p></td></tr>


                        <tr>
                          <td valign=top><strong>Ticket Number:</strong></td>
                          <td valign=top><%=UpdateItem.getTicketNumber()%></td>
                        </tr>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Submission Date&nbsp;(mm/dd/yyyy):&nbsp;</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UpdateItem.getSubmissionDate())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=SubmissionDate&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("SubmissionDate")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Category</b></p>
                  </td><td valign=top><p><jsp:include page="../generic/tUpdateItemCategory_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UpdateItem.getCategoryID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=CategoryID&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("CategoryID")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Release Version</b></p>
                  </td>
                  <td valign=top>
                    <p><b><%=UpdateItem.getReleaseVersion()%></b>&nbsp;
                      <%if (isShowAudit){%>
                      <a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ReleaseVersion&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("ReleaseVersion")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a>
                      <%}%>
                    </p>
                  </td>
                </tr>
                         <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Priority</b></p>
                  </td><td valign=top><p><%=UpdateItem.getPriorityID()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=PriorityID&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("PriorityID")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <tr><td valign=top>
                    <p class=<%=theClass%> ><b>Status</b></p>
                  </td><td valign=top class="borderHighlightGreen"><p><jsp:include page="../generic/tUpdateItemStatus_translate.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=UpdateItem.getStatusID()%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=StatusID&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("StatusID")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                       <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Item Title</b></p>
                  </td><td valign=top><p><%=UpdateItem.getItemTitle()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ItemTitle&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("ItemTitle")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                       <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Description</b></p>
                  </td><td valign=top><p><%=UpdateItem.getItemDescription()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ItemDescription&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("ItemDescription")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                       <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Development Comments</b></p>
                  </td><td valign=top><p><%=UpdateItem.getDeveloperComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DeveloperComments&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("DeveloperComments")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                       <tr>
                  <td valign=top> 
                    <p class=<%=theClass%> ><b>Tester Comments&nbsp;</b></p>
                  </td><td valign=top><p><%=UpdateItem.getTesterComments()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=TesterComments&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("TesterComments")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Tested On Development<br>
                        (mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UpdateItem.getDateTestedOnDevelopment())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateTestedOnDevelopment&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("DateTestedOnDevelopment")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Tested On QA<br>
                        (mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UpdateItem.getDateTestedOnQA())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateTestedOnQA&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("DateTestedOnQA")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                        <tr><td valign=top><p class=<%=theClass%> ><b>Date Tested On Production<br>
                        (mm/dd/yyyy):&nbsp;</b></p></td><td valign=top><p><jsp:include page="../generic/DateTypeConvert_Form.jsp" flush="true" ><jsp:param name="CurrentSelection" value="<%=dbdf.format(UpdateItem.getDateTestedOnProduction())%>" /></jsp:include>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=DateTestedOnProduction&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("DateTestedOnProduction")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                       <tr><td valign=top> <p class=<%=theClass%> ><b>Requested By:</b></p></td><td valign=top><p><%=UpdateItem.getRequesterInitials()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=RequesterInitials&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("RequesterInitials")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                         <tr><td valign=top><p class=<%=theClass%> ><b>RemoteIP&nbsp;</b></p></td><td valign=top><p><%=UpdateItem.getRemoteIP()%>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=RemoteIP&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("RemoteIP")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                       <tr><td valign=top> <p class=<%=theClass%> ><b>Additional Commments&nbsp;</b></p></td><td valign=top><pre><%=UpdateItem.getComments()%></pre>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("Comments")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></td></tr>



            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        </td></tr></table>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</td></tr></table>

