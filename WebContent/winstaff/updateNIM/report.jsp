<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*, com.winstaff.*" errorPage="" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>NIM3 Start</title>
<style type="text/css">
<!--
th 
{
	font-family: Verdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #FFF;
	background-color: #036;
}

  .normal { background-color: #FFFFFF }
  .tdAlt { background-color: #CCCCCC }
  .highlight { background-color: #8888FF }	
</style>


td 
{
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
}

.tdAlt 
{
	background:#EEE;
}

-->
</style>
</head>

<body>
<%@ include file="../generic/CheckLogin.jsp" %>

<%@ include file="../generic/generalDisplay.jsp" %>

<link href="../nim3/ui_200/style_sched.css" rel="stylesheet" type="text/css">
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
      <table width="100%" border="0" cellspacing="0" height="50">
        <tr>
          <td background="title1.jpg">&nbsp;</td>
        </tr>
      </table>
      <input name=Submit2 type=button class="inputButton_md_Default" value="Return to Maintenance" onClick="document.location='../nim3/Maint_Home.jsp';">
<br>

<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <td>
<%
java.util.Vector myList = new java.util.Vector();


com.winstaff.AutoReportObject myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("List All Pending Items");
myARO.setsSQL("SELECT tupdateitem.submissiondate AS \"Request_Date\",tupdateitem.priorityid AS \"Priority\",tupdateitemstatusli.updateitemstatustext AS \"Status\",tupdateitem.itemtitle AS \"Title\",tupdateitem.requesterinitials AS \"Req_By\" FROM tupdateitem INNER JOIN tupdateitemstatusli ON tupdateitemstatusli.updateitemstatusid = tupdateitem.statusid where statusid in (1,10, 2) ORDER BY priorityid ASC, submissiondate desc");
myARO.setDisplayChart(false);
myARO.setHideLabel(false);
myARO.setChartLabelField("Priority");
myARO.setChartDataField("Priority");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_PIECHART);
myList.add(myARO);


myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("List All Request Items");
myARO.setsSQL("SELECT tupdateitem.submissiondate AS \"Request_Date\",tupdateitem.priorityid AS \"Priority\",tupdateitemstatusli.updateitemstatustext AS \"Status\",tupdateitem.itemtitle AS \"Title\",tupdateitem.requesterinitials AS \"Req_By\" FROM tupdateitem INNER JOIN tupdateitemstatusli ON tupdateitemstatusli.updateitemstatusid = tupdateitem.statusid where ticketnumber>'' ORDER BY priorityid ASC, submissiondate desc");
myARO.setDisplayChart(false);
myARO.setHideLabel(false);
myARO.setChartLabelField("Priority");
myARO.setChartDataField("Priority");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_PIECHART);
myList.add(myARO);




myARO = new com.winstaff.AutoReportObject();
myARO.setTitle("Priority Report");
myARO.setsSQL("SELECT tupdateitem.priorityid, count(*) from tupdateitem group by priorityid order by priorityid" );
myARO.setDisplayChart(true);
myARO.setHideLabel(false);
myARO.setChartLabelField("priorityid");
myARO.setChartDataField("count");
myARO.setChartType(com.winstaff.AutoReportObject.CHART_PIECHART);
myList.add(myARO);





    boolean accessValid = false;
   Integer iSecurityCheck = SecurityCheck.CheckItem("nim1", UserSecurityGroupID);
   Integer iPayerID = null;
   Integer iAdjusterID = null;
   if (iSecurityCheck.intValue()!=0)
   {
    if (isReporter) 
    {
        accessValid = true;
    }
	if (accessValid)
	{
		try
		{
			String myRID = request.getParameter("rid");
			
			String sTitle = "";
			String sSQL = "";
			com.winstaff.AutoReportObject theARO = null;
			java.util.Vector myLables = new java.util.Vector();
			java.util.Vector myDatas = new java.util.Vector();
			int myLabel = -1;
			int myData = -1;
			%>
			<form action="report.jsp" method="GET" id="autoform">
			<select name="rid" onChange="document.getElementById('autoform').submit();">
			  <option value="-1">Please Select</option>
			<%
			for (int i5=0;i5<myList.size();i5++)
			{
				com.winstaff.AutoReportObject tempARO = (com.winstaff.AutoReportObject) myList.elementAt(i5);
				String myValue = i5+"";
				if (tempARO.getAutoReportCode()!=null)
				{
					myValue = tempARO.getAutoReportCode();
				}
				if (myRID.equalsIgnoreCase(myValue))
				{
					%><option selected value="<%=myValue%>"><%=tempARO.getTitle()%></option><%
					theARO = (com.winstaff.AutoReportObject) myList.elementAt(i5);
					sSQL = theARO.getsSQL();
					sTitle = theARO.getTitle();
				}
				else
				{
					%><option value="<%=myValue%>"><%=tempARO.getTitle()%></option><%
				}
			}
			%>
			</select>
Last Updated: <%=PLCUtils.getDisplayDateWithTime(PLCUtils.getNowDate(false))%>
			<h1><%=sTitle%></h1>
	  <%
				searchDB2 mySS = new searchDB2();
			//	String mySQL = ("SELECT LogonUserName, UniqueModifyDate from tUserAccount");
//				out.print(theARO.getPreparedStatementObject().getSQL());
				java.sql.ResultSet result = null;
				java.sql.ResultSetMetaData rsmd = null;    
				result = mySS.executePreparedStatement(theARO.getPreparedStatementObject());
				int columns=0;
				try 
				{
				   rsmd = result.getMetaData();
				   columns = rsmd.getColumnCount();
				}
				catch (SQLException e) 
				{
					out.println("Error occurred " + e);
				}
				
%>

<select name="search1">
<%
					for (int i=1; i<=columns; i++) 
					{
						 if (theARO.isHideLabel()&&rsmd.getColumnLabel(i).equalsIgnoreCase(theARO.getChartLabelField()))
						 {
						 }
						 else
						 {
						 	out.write("<option value="+ rsmd.getColumnLabel(i) +">" + rsmd.getColumnLabel(i) + "</option>");
						 }
					}
%>
</select> : <input name="value1" >
<br>
<select name="search2">
<%
					for (int i=1; i<=columns; i++) 
					{
						 if (theARO.isHideLabel()&&rsmd.getColumnLabel(i).equalsIgnoreCase(theARO.getChartLabelField()))
						 {
						 }
						 else
						 {
						 	out.write("<option>" + rsmd.getColumnLabel(i) + "</option>");
						 }
					}
%>
</select> : <input name="value2" >
<br>
<select name="search3">
<%
					for (int i=1; i<=columns; i++) 
					{
						 if (theARO.isHideLabel()&&rsmd.getColumnLabel(i).equalsIgnoreCase(theARO.getChartLabelField()))
						 {
						 }
						 else
						 {
						 	out.write("<option>" + rsmd.getColumnLabel(i) + "</option>");
						 }
					}
%>
</select> : <input name="value3" >

<input name="" type="submit" value="Submit">
</form>
            


<%				
				
				
					if (theARO.isDisplayChart())
					{
					   %>

            <hr> <div id="visualization" 
     style="width: 800px; height: 300px;">
    </div><hr>
    <%
					}
					%>
			<table cellspacing="3" border="1">
			  <tr>
			  <% // write out the header cells containing the column labels
				 try 
				 {
					for (int i=1; i<=columns; i++) 
					{
						 if (theARO.isHideLabel()&&rsmd.getColumnLabel(i).equalsIgnoreCase(theARO.getChartLabelField()))
						 {
						 }
						 else
						 {
						 	out.write("<th>" + rsmd.getColumnLabel(i) + "</th>");
						 }
						 if (rsmd.getColumnLabel(i).equalsIgnoreCase(theARO.getChartLabelField()))
						 {
							 myLabel = i;
						 }
						 if (rsmd.getColumnLabel(i).equalsIgnoreCase(theARO.getChartDataField()))
						 {
							 myData = i;
						 }
																							  
					}
			  %>
			  </tr>
<% // now write out one row for each entry in the database table
			  int i5=0;
					while (result.next()) 
					{
						i5++;
						if (i5%2==0)
						{
						   out.write("<tr class=tdAlt   onMouseOver=\"this.className='highlight'\" onMouseOut=\"this.className='tdAlt'\" >");
						}
						else
						{
						   out.write("<tr class=normal onMouseOver=\"this.className='highlight'\" onMouseOut=\"this.className='normal'\" >");
						}
					   for (int i=1; i<=columns; i++) 
					   {
						 if (theARO.isHideLabel()&&i==myLabel)
						 {
						 }
						 else
						 {
							 out.write("<td>&nbsp;" + result.getString(i) + "</td>");
						 }
						 if (i==myLabel)
						 {
							 myLables.add(result.getString(i));
						 }
						 if (i==myData)
						 {
							 myDatas.add(result.getString(i));
						 }
					   }
					   out.write("</tr>");
					}
//google chart example	
					if (theARO.isDisplayChart())
					{
					   %>
						<script type="text/javascript" 
                         src="https://www.google.com/jsapi">
                        </script>
                        <script type="text/javascript">
                          google.load('visualization', '1', 
                            {packages: ['<%=theARO.getChartType()%>']});
                        </script>
                        <script type="text/javascript">
                          function drawVisualization() {
                            // Create and populate the data table.
                            var data = new google.visualization.DataTable();
                            data.addColumn('string', 'Label');
                            data.addColumn('number', 'Total');
                            data.addRows([
							<%
							   for (int i6=0; i6<myLables.size(); i6++) 
							   {
								   String tempLabel = (String) myLables.elementAt(i6);
								   String tempData = (String) myDatas.elementAt(i6);
									 %>['<%=tempLabel%>',{v:<%=tempData%>}]<%
									 if (i6!=myLables.size()-1)
									 {
										 out.print(", ");
									 }
									 
							   }
							%>  
                            ]);
                          
                            // Create and draw the visualization.
							<%
							if (theARO.getChartType().equalsIgnoreCase(com.winstaff.AutoReportObject.CHART_PIECHART))
							{%>new google.visualization.PieChart(<%
							}
							else if (theARO.getChartType().equalsIgnoreCase(com.winstaff.AutoReportObject.CHART_BARCHART))
							{%>new google.visualization.BarChart(<%
							}
							else if (theARO.getChartType().equalsIgnoreCase(com.winstaff.AutoReportObject.CHART_COLCHART))
							{%>new google.visualization.ColumnChart(<%
							}%>
                              document.getElementById('visualization')).
                                draw(data, {is3D:true});
                          }
                          google.setOnLoadCallback(drawVisualization);
                        </script>                       
                       <%
					}
			
					// close the connection and the statement
					mySS.closeAll();
				 } // end of the try block
				 catch (SQLException e) 
				 {
					out.println("Error " + e);
				 }
				 // ensure everything is closed
		 } // end of the try block
		 catch (Exception e55) 
		 {
			out.println("<hr>Error Main: " + e55);
		 }
	}
	else
	{
		out.print ("no access");
	}
   }
   else
   {
		out.print ("no sec");
   }

   %>
</table>

</td>
  </tr>
</table>

</body>
</html>

<%String bnIncludeFN = "ui_"+thePLCID+"\\bot-nav_sched.jsp";

%>
