<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<%@page contentType="text/html" language="java" import="com.winstaff.*" %>
<%/*

    filename: out\jsp\tUpdateItem_form.jsp
    Created on Nov/10/2004
    Created by: Scott Ellis
*/%>

<%@ include file="../generic/CheckLogin.jsp" %>
<%@ include file="../generic/generalDisplay.jsp" %>

<link href="../nim3/ui_200/style_sched.css" rel="stylesheet" type="text/css">
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
      <table width="100%" border="0" cellspacing="0" height="50">
        <tr>
          <td background="title1.jpg">&nbsp;</td>
        </tr>
      </table>
      <input name=Submit2 type=button class="inputButton_md_Default" value="Return to Maintenance" onClick="document.location='../nim3/Maint_Home.jsp';">
<br>
<table cellpadding=0 cellspacing=0 border=0 width=100% >
    <tr><td width=10>&nbsp;</td>
    <td><br>
      <hr noshade>
      <b><span class="instructions">Make sure to click 'update' to save your changes 
      before leaving this page!</span></b> 
      <%
//initial declaration of list class and parentID
    Integer        iUpdateItemID        =    null;
    boolean accessValid = false;
    // required for Type2
    String sKeyMasterReference = null;

   Integer iSecurityCheck = new Integer(2);
   if (iSecurityCheck.intValue()!=0&&isScheduler2)
   {
    bltUpdateItem        UpdateItem        =    null;
	boolean isEdit=false;
    if ( request.getParameter( "EDIT" ).equalsIgnoreCase("request") )
    {
	    pageControllerHash.remove("iUpdateItemID");
        accessValid = true;    
        UpdateItem        =    new    bltUpdateItem(UserSecurityGroupID, true);
	}
	if (UpdateItem.getReleaseVersion().equalsIgnoreCase(""))
	{
		UpdateItem.setReleaseVersion("TBD");
	}
  //page security
  if (accessValid)
  {
      java.text.SimpleDateFormat dbdf = new java.text.SimpleDateFormat(PLCUtils.String_dbdf);
      java.text.SimpleDateFormat displayDateSDF = new java.text.SimpleDateFormat(PLCUtils.String_displayDateSDF1);
      java.text.SimpleDateFormat displayDateTimeSDF = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

    pageControllerHash.put("sLocalChildReturnPage","tUpdateItem_form.jsp");
    session.setAttribute("pageControllerHash",pageControllerHash);

//initial declaration of list class and parentID



//fields
        %>
      <form action="tUpdateItem_form_sub.jsp" name="tUpdateItem_form1" method="POST">
<%
    if ( request.getParameter( "EDIT" ) != null )
    {
    %>
        <input type="hidden" name="EDIT" value = "<%=request.getParameter( "EDIT" )%>" >  
    <%
    }
%>
          <%  String theClass ="tdBase";%>
<input name=Submit type=Submit class="inputButton_lg_Create" value="Submit Request" >
        <table border=1 bordercolor=#333333 cellpadding=0 cellspacing=0>
         <tr><td class=tableColor>
            <table cellpadding=10 cellspacing=0 width=100%>
                       <tr class="tdBaseAltGreen"><td valign=top>
                    <p class=<%=theClass%> ><b>Item Title</b></p>
                  </td><td valign=top><p>
                      <input name="ItemTitle" type="text" onKeyDown="textAreaStop(this,200)" value="<%=UpdateItem.getItemTitle()%>" size="80" maxlength="200">
                      &nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ItemTitle&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("ItemTitle")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                       <tr class="tdBaseAltGreen"><td valign=top>
                    <p class=<%=theClass%> ><b>Description</b><br>
                      4000 Characters or less
                    </p>
                    <p class=<%=theClass%> >For reports, please make sure to include all the fields that you want in the report.</p>
                  </td  ><td valign=top><p><textarea onKeyDown="textAreaStop(this,4000)" rows="8" name="ItemDescription" cols="60"><%=UpdateItem.getItemDescription()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=ItemDescription&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("ItemDescription")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>
                       <tr class="tdBaseAltRed">
                         <td valign=top><p>Mark Urgent</p>
                         <p>Note: Urgent implies that we should halt all other requests/updates to push this through as fast as possible. </p>
                         <p>This should be used primarily for any significant bugs that affect workflow or the ability to process a case. </p></td>
                         <td valign=top><label>
                           <select name="ssUrgent" id="ssUrgent">
                             <option value="Yes">Yes</option>
                             <option value="No" selected>No</option>
                           </select>
                         </label></td>
                       </tr>
                       <tr>
                         <td valign=top>Is this needed for a specific meeting, date, time? If so please indicate:</td>
                         <td valign=top><label>
                           <input name="ssTimeFrame" type="text" id="ss" size="40" maxlength="80">
                         </label></td>
                       </tr>
                       <tr>
                         <td valign=top>If this is related to a specific client or branch or user, please indicate here:</td>
                         <td valign=top><input name="ssClient" type="text" id="ss2" size="40" maxlength="80"></td>
                       </tr>
                       <tr>
                         <td valign=top>Format:<br>
                         For reports, please indicate a file format: Excel, PDF, etc.</td>
                         <td valign=top><label>
                           <select name="ssFileFormat" id="ssFileFormat">
                             <option value="NA" selected>NA</option>
                             <option value="Excel">Excel</option>
                             <option value="PDF">PDF</option>
                             <option value="Other">Other</option>
                           </select>
                         </label></td>
                       </tr>
                       <tr><td valign=top><p class=<%=theClass%> ><b>Please indicate any other information</b></p></td><td valign=top><p><textarea onKeyDown="textAreaStop(this,4000)" rows="6" name="Comments" cols="60"><%=UpdateItem.getComments()%></textarea>&nbsp;<%if (isShowAudit){%><a href="#" onClick="window.open('auditView_Field.jsp?sFieldName=Comments&amp;sTableName=tUpdateItem&amp;sRefID=<%=UpdateItem.getUpdateItemID()%>&amp;sFieldNameDisp=<%=UpdateItem.getEnglish("Comments")%>&amp;sTableNameDisp=tUpdateItem','AuditField','scrollbars=yes,resizable=yes,width=350,height=300');return false;"><img align=middle border=0 src=images/icon_audit.gif></a><%}%></p></td></tr>

            <tr><td width=40%>&nbsp;</td><td width=60%>&nbsp;</td></tr>
            </table>
        </td></tr></table>
      </form>
        <%
  }
  else
  {
   out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORIllegal")+"</p>");
  }
}
else
{
out.println("<p class=instructions>"+ConfigurationMessages.getMessage("ERRORSecurityNoAccess")+"</p>");
}
%>

</td>
  </tr>
</table>

